<?php
/*
PUBLIC FUNCTION:
- add(intWarehouseID,intSalesmanID,strLoadingDate,strDischargeDate,intStatus,strDescription,strDate)
- searchBySalesman(strSalesmanName)
- getCount()
- getItems(intStartNo,intPerPage)
- getItemByID(intID)
- editByID(intID,intStatus,strDischargeDate)
- editByID2(intID,strDescription,intStatus,strDischargeDate)
- deleteByID(intID)
- getUserHistory(intUserID,intPerPage)
- getLastDay(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Mkanvas extends JW_Model {

public function __construct() {
	parent::__construct();

	$this->initialize('kanvas');
}

public function add($intWarehouseID,$intSalesmanID,$strLoadingDate,$strDischargeDate,$intStatus,$strDescription,$strDate) {
	return $this->dbInsert(array(
		'kanv_branch_id' => $this->session->userdata('intBranchID'),
		'kanv_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
		'kanv_salesman_id' => $intSalesmanID,
		'kanv_loading_date' => $strLoadingDate,
		'kanv_discharge_date' => $strDischargeDate,
		'kanv_status' => $intStatus,
		'kanv_description' => $strDescription,
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
	));
}

public function searchBySalesman($strSalesmanName) {
	$strSalesmanName = urldecode($strSalesmanName);

	$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_status, sals_name, sals_address, sals_phone, kanv_code
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
WHERE (sals_name LIKE '%$strSalesmanName%')
ORDER BY k.cdate DESC");

	if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id');
	return $this->getNumRows();
}

public function getItems($intStartNo = -1, $intPerPage = -1) {
	if ($intStartNo == -1 || $intPerPage == -1)
		$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_status, sals_name, sals_address, sals_phone,kanv_code
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
ORDER BY k.cdate,kanv_code DESC");
	else
		$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_status, sals_name, sals_address, sals_phone,kanv_code
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
ORDER BY k.cdate,kanv_code DESC LIMIT $intStartNo, $intPerPage");

	if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID = 0) {
	if ($intID == 0)
		$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_description, kanv_status, sals_name,
	sals_address, sals_phone,kanv_code,s.id as sals_id, ware_name, sals_code,
	kanv_loading_date,kanv_discharge_date, w.id as ware_id
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
LEFT JOIN jw_warehouse AS w ON kanv_warehouse_id = w.id
ORDER BY k.cdate DESC LIMIT 0,1");
	else
		$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_description, kanv_status, sals_name,
	sals_address, sals_phone,kanv_code,s.id as sals_id, ware_name, sals_code,
	kanv_loading_date,kanv_discharge_date, w.id as ware_id
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
LEFT JOIN jw_warehouse AS w ON kanv_warehouse_id = w.id
WHERE k.id = $intID");

	if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function editByID($intID,$intStatus,$strDischargeDate) {
	return $this->dbUpdate(array(
			'kanv_status' => $intStatus,
			'kanv_discharge_date' => $strDischargeDate),
		"id = $intID");
}

public function editByID2($intID,$strDescription,$intStatus,$strDischargeDate) {
	return $this->dbUpdate(array(
			'kanv_description' => $strDescription,
			'kanv_discharge_date' => $strDischargeDate,
			'kanv_status' => $intStatus),
		"id = $intID");
}

public function deleteByID($intID) {
	$status = $this->dbSelect('kanv_status',"id = $intID");
	if($status['kanv_status']='0' || $status['kanv_status']='1'){
		$this->setQuery("DELETE FROM kanvas_item WHERE kavi_kanvas_id = $intID");
		return $this->dbDelete("id = $intID");
	}
	else
		return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT k.id, k.cdate AS kanv_date, kanv_status, sals_name, sals_address
FROM kanvas AS k
LEFT JOIN jw_salesman AS s ON kanv_salesman_id = s.id
WHERE (k.cby = $intUserID OR k.mby = $intUserID)
ORDER BY k.cdate DESC LIMIT 0, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getLastDay($intID) {
	$this->setQuery(
"SELECT max(kavi_day) as kanv_day
FROM kanvas_item
WHERE kavi_kanvas_id = $intID");

	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->kanv_day;
	else return 0;
}

}