<?php
/**
 * 
 */
class Mtransout extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('transaction_out');
	}

	public function getChartOfAccount()
    {
        $this->setQuery("
            SELECT id, acco_code, acco_name FROM jw_account
            ");

        if($this->getNumRows() > 0 ) return $this->getQueryResult('Array');
        else return false;
    }

    public function getPurchaseInvoiceBySupplier($intSupplierID)
    {
    	$this->initialize("purchase_invoice");
    	$this->dbSelect("", "pinv_supplier_id = '$intSupplierID'");

    	if($this->getNumRows() > 0 ) return $this->getQueryResult('Array');
        else return false;
    }

    public function getAllTransaction($strType, $intStartNo = -1, $intPerPage = -1)
    {    	
    	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "txou_date DESC, txou.id DESC";
        else $strOrderBy = "txou_date DESC, txou.id DESC LIMIT $intStartNo, $intPerPage";

  //       $this->setQuery(
  //           "
  //            SELECT id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS txod_totalbersih,  txod_ppn, txod_type, reff_id, supp_name, cdate, mdate, txou_status_trans, txod_description FROM
		// (
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih,  txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH') as tabelDPH
		// LEFT JOIN downpayment as dp ON tabelDPH.dphi_reff_id = dp.id
		// LEFT JOIN jw_supplier as supp ON dp.dpay_supplier_id = supp.id
		// WHERE dphi_reff_type = 'DP' GROUP BY id)
		// UNION ALL
		// (SELECT  tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON pi.pinv_supplier_id = supp.id
		// WHERE dphi_reff_type = 'PI' GROUP BY id)
		// UNION ALL
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description, 
		// 	txod.txod_supp_id FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// -- LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON tabelDPH.txod_supp_id = supp.id
		// WHERE dphi_reff_type = 'NKS' GROUP BY id)
		// UNION ALL 
		// (SELECT tabelNKS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNKS.cdate AS cdate, tabelNKS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id , txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NKS' ORDER BY txou.cdate, txou.mdate) as tabelNKS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNKS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelNDS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNDS.cdate AS cdate, tabelNDS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NDS' ORDER BY txou.cdate, txou.mdate) as tabelNDS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelPGL.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, kont_name, tabelPGL.cdate AS cdate, tabelPGL.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, SUM(txod_totalbersih) as txod_totalbersih, txod_amount, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'PGL' GROUP BY txou_code ORDER BY txou.cdate, txou.mdate) as tabelPGL
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN kontrak as kont ON txod_ref_id = kont.id
		// )
		// ) as unionTable
		// WHERE txou_code LIKE '$strType%' AND txou_status_trans > ".STATUS_DELETED."
		// GROUP BY id	ORDER BY $strOrderBy
		// 	"
  //       );

        switch ($strType) {
        	case 'PHU': $strListOfType = "DPH"; break;
        	case 'NKS': $strListOfType = "NKS"; break;
        	case 'PGL': $strListOfType = "PGL"; break;
        }

        $this->setQuery("SELECT txou.id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, supp_name, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out txou
			LEFT JOIN transaction_out_detail txod ON txou.id = txod.txod_txou_id
			LEFT JOIN jw_supplier supp ON txod_supp_id = supp.id
		WHERE txod_type IN ('$strListOfType') AND txou_status_trans > ".STATUS_DELETED."
		GROUP BY txou.id ORDER BY $strOrderBy");        
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getCount($strType) {        
  //       $this->setQuery(
  //           "
  //           SELECT id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS  txod_totalbersih,  txod_ppn, txod_type, reff_id, supp_name, cdate, mdate, txou_status_trans, txod_description FROM
		// (
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih,  txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH') as tabelDPH
		// LEFT JOIN downpayment as dp ON tabelDPH.dphi_reff_id = dp.id
		// LEFT JOIN jw_supplier as supp ON dp.dpay_supplier_id = supp.id
		// WHERE dphi_reff_type = 'DP' GROUP BY id)
		// UNION ALL
		// (SELECT  tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON pi.pinv_supplier_id = supp.id
		// WHERE dphi_reff_type = 'PI' GROUP BY id)
		// UNION ALL
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description, 
		// 	txod.txod_supp_id FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// -- LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON tabelDPH.txod_supp_id = supp.id
		// WHERE dphi_reff_type = 'NKS' GROUP BY id)
		// UNION ALL 
		// (SELECT tabelNKS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNKS.cdate AS cdate, tabelNKS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id , txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NKS' ORDER BY txou.cdate, txou.mdate) as tabelNKS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNKS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelNDS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNDS.cdate AS cdate, tabelNDS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NDS' ORDER BY txou.cdate, txou.mdate) as tabelNDS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelPGL.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, kont_name, tabelPGL.cdate AS cdate, tabelPGL.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, SUM(txod_totalbersih) as txod_totalbersih, txod_amount, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'PGL' GROUP BY txou_code ORDER BY txou.cdate, txou.mdate) as tabelPGL
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN kontrak as kont ON txod_ref_id = kont.id
		// )
		// ) as unionTable
		// WHERE txou_code LIKE '$strType%' AND txou_status_trans > ".STATUS_DELETED."
		// GROUP BY id
		// 	"
  //       );       
    	switch ($strType) {
        	case 'PHU': $strListOfType = "DPH"; break;
        	case 'NKS': $strListOfType = "NKS"; break;
        	case 'PGL': $strListOfType = "PGL"; break;
        }

        $this->setQuery("SELECT txou.id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, supp_name, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out txou
			LEFT JOIN transaction_out_detail txod ON txou.id = txod.txod_txou_id
			LEFT JOIN jw_supplier supp ON txod_supp_id = supp.id
		WHERE txod_type IN ('$strListOfType') AND txou_status_trans > ".STATUS_DELETED."
		GROUP BY txou.id ORDER BY txou_date DESC, txou.id ASC");
		
        return $this->getNumRows();
    }


	public function getTransactionByID($intID)
	{
		$this->initialize('transaction_out_detail');
		$this->dbSelect("txod_type, txod_ref_id", "txod_txou_id=$intID");
		$arrRefData = $this->getNextRecord("Array");
		
		$this->setQuery("
			SELECT tbUnion.id, `date`, txod_ppn, txod_pph, txou_description, code, txod_txou_id, refID, refType, acco_id, acco_code, acco_name, metode, txou_ref_giro, txou_jatuhtempo, tbUnion.cby, tbUnion.supp_id, supp_name, txou_status_trans FROM 
			( 
				(SELECT tbDPH.id as id, tbDPH.`date` as `date`, txod_ppn, txod_pph, tbDPH.txou_description, tbDPH.code as code, txod_txou_id, tbDPH.refID as refID, tbDPH.refType as refType, tbDPH.acco_id as acco_id, tbDPH.metode as metode, pinv_supplier_id AS supp_id, txou_ref_giro, txou_jatuhtempo, tbDPH.cby, txou_status_trans
				FROM 
					(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn, txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType,txou.txou_acco_id as acco_id,txou.txou_metode as metode, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.txou_ref_giro, txou.txou_jatuhtempo, txou.cby, txou_status_trans
					FROM transaction_out AS txou 
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
					JOIN daftar_pembayaran_hutang AS dph ON txod_ref_id = dph.id 
					JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id 
					WHERE txod.txod_type = 'DPH') as tbDPH 
				JOIN purchase_invoice AS pi ON pi.id = dphi_reff_id 
				WHERE dphi_reff_type = 'PI')
			UNION ALL 
			(SELECT tbDPH_DP.id as id, tbDPH_DP.`date` as `date`, txod_ppn, txod_pph, tbDPH_DP.txou_description, tbDPH_DP.code as code, txod_txou_id, tbDPH_DP.refID as refID, tbDPH_DP.refType as refType, tbDPH_DP.acco_id as acco_id, tbDPH_DP.metode as metode, dpay_supplier_id AS supp_id, txou_ref_giro, txou_jatuhtempo, tbDPH_DP.cby, txou_status_trans 
				FROM 
					(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn, txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType,txou.txou_acco_id as acco_id,txou.txou_metode as metode, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.txou_ref_giro, txou.txou_jatuhtempo, txou.cby, txou_status_trans
					FROM transaction_out AS txou 
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
					JOIN daftar_pembayaran_hutang AS dph ON txod_ref_id = dph.id 
					JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id 
					WHERE txod.txod_type = 'DPH') as tbDPH_DP 
				JOIN downpayment AS dp ON dp.id = dphi_reff_id 
				WHERE dphi_reff_type = 'DP')
			UNION ALL 
			(SELECT tbDPH_NKS.id as id, tbDPH_NKS.`date` as `date`, tbDPH_NKS.txod_ppn, tbDPH_NKS.txod_pph, tbDPH_NKS.txou_description, tbDPH_NKS.code as code, tbDPH_NKS.txod_txou_id, tbDPH_NKS.refID as refID, tbDPH_NKS.refType as refType, tbDPH_NKS.acco_id as acco_id, tbDPH_NKS.metode as metode, txod_supp_id AS supp_id, txou_ref_giro, txou_jatuhtempo, tbDPH_NKS.cby, txou_status_trans
			FROM 
				(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn, txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType,txou.txou_acco_id as acco_id,txou.txou_metode as metode, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.txou_ref_giro, txou.txou_jatuhtempo, txou.cby, txou_status_trans  
				FROM transaction_out AS txou 
				LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
				JOIN daftar_pembayaran_hutang AS dph ON txod_ref_id = dph.id 
				JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id 
				WHERE txod.txod_type = 'DPH') as tbDPH_NKS 
			JOIN transaction_out_detail AS txod ON txod.txod_txou_id = dphi_reff_id 
			WHERE dphi_reff_type = 'NKS')
			UNION ALL 
				(SELECT tbNotaKredit.id as id, tbNotaKredit.`date` as `date`, txod_ppn, txod_pph, tbNotaKredit.txou_description, tbNotaKredit.code as code, txod_txou_id, tbNotaKredit.refID as refID, tbNotaKredit.refType as refType, tbNotaKredit.acco_id as acco_id, tbNotaKredit.metode as metode, supp.id as supp_id,txou_ref_giro, txou_jatuhtempo, tbNotaKredit.cby, txou_status_trans  
				FROM 
					(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn,txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType, txou.txou_acco_id as acco_id, txou.txou_metode as metode, txod.txod_supp_id, txou.txou_ref_giro, txou.txou_jatuhtempo, txou.cby, txou_status_trans 
					FROM transaction_out AS txou 
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
					WHERE txod_type = 'NKS') AS tbNotaKredit
				LEFT JOIN kontrak AS kont ON tbNotaKredit.refID = kont.id 
				LEFT JOIN jw_supplier AS supp ON txod_supp_id = supp.id)
			UNION ALL 
				(SELECT tbNotaDebet.id as id, tbNotaDebet.`date` as `date`, txod_ppn, txod_pph, tbNotaDebet.txou_description, tbNotaDebet.code as code, txod_txou_id, tbNotaDebet.refID as refID, tbNotaDebet.refType as refType, tbNotaDebet.acco_id as acco_id, tbNotaDebet.metode as metode, txod_supp_id as supp_id, txou_ref_giro, txou_jatuhtempo, tbNotaDebet.cby, txou_status_trans
				FROM 
					(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn,txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType, txou.txou_acco_id as acco_id, txou.txou_metode as metode, txou.txou_ref_giro, txou.txou_jatuhtempo, txod_supp_id, txou.cby, txou_status_trans    
					FROM transaction_out AS txou 
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
					WHERE txod_type = 'NDS') AS tbNotaDebet
				-- LEFT JOIN purchase_invoice AS pi ON tbNotaDebet.refID = pi.id 
				-- LEFT JOIN jw_supplier AS supp ON pi.pinv_supplier_id = supp.id
				)
			UNION ALL
				(SELECT tbPGL.id as id, tbPGL.`date` as `date`, txod_ppn, txod_pph, tbPGL.txou_description, tbPGL.code as code, txod_txou_id, tbPGL.refID as refID, tbPGL.refType as refType, tbPGL.acco_id as acco_id, tbPGL.metode as metode, kont.id as supp_id, txou_ref_giro, txou_jatuhtempo, tbPGL.cby, txou_status_trans 
				FROM 
					(SELECT txou.id as id, txou.txou_date as `date`, txod_ppn,txod_pph, txou.txou_description as txou_description, txou_code as code, txod_txou_id, txod.txod_ref_id as refID, txod.txod_type as refType, txou.txou_acco_id as acco_id, txou.txou_metode as metode, txou.txou_ref_giro, txou.txou_jatuhtempo, txou.cby, txou_status_trans 
					FROM transaction_out AS txou 
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id 
					WHERE txod_type = 'PGL') AS tbPGL
				LEFT JOIN kontrak AS kont ON tbPGL.refID = kont.id) 
			) as tbUnion
			LEFT JOIN jw_supplier AS supp ON tbUnion.supp_id = supp.id
			LEFT JOIN jw_account AS acco ON tbUnion.acco_id = acco.id
			WHERE txod_txou_id = '$intID' GROUP BY code
		");
		
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}

	public function getDetailTransaction($intID)
	{
		$this->initialize('transaction_out_detail');
		$this->dbSelect("txod_type, txod_ref_id", "txod_txou_id=$intID");
		$arrRefData = $this->getNextRecord("Array");

		$select = "*";
		$join = '';
		$groupby = '';
		$where = '';
		switch ($arrRefData['txod_type']) {
			case 'DPH': 
				#$select = "SUM(txod_amount) as txod_amount";
				$strTableName = "daftar_pembayaran_hutang"; 
				$join = "LEFT JOIN $strTableName ON txod_ref_id = $strTableName.id";
				$where .= " AND dphu_status > ".STATUS_DELETED;
				# $groupby = "GROUP BY txou_code";
			break;
			case 'NKS': 
			case 'NDS':
				$select = "txod.*, k.kont_name";				
				// $strTableName = "purchase_invoice"; 
				// $join = " LEFT JOIN jw_customer AS cust ON txod_ref_id = cust.id ";
				// $join .= " LEFT JOIN purchase_invoice_item AS pi_item ON pi.id = pi_item.pinvit_purchase_invoice_id";
				// $join .= " LEFT JOIN jw_product as prod ON pi_item.pinvit_product_id = prod.id";
				// $join .= " LEFT JOIN purchase as p ON pi.pinv_purchase_id = p.id";
				// $join .= " LEFT JOIN purchase_order as po ON p.prch_po_id = po.id";
				$join .= " LEFT JOIN kontrak as k ON txod.txod_ref_id = k.id";				
				// $groupby = "GROUP BY txou_code";
			break;
			case 'PGL':
				$select = "txod.id, txod_acco_id, txod_ref_id, txod_amount, txod_totalbersih, txod_description";
				$join = " LEFT JOIN kontrak AS kont ON txod_ref_id = kont.id";
			break;		
		}
		// $sql = "SELECT * FROM $strTableName WHERE $strTableName.id = ".$arrRefData['txod_ref_id'];		
		// $this->setQuery($sql);
		$this->setQuery("
			SELECT $select FROM transaction_out as txou 
			LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id	
			$join WHERE txou.id = $intID $where $groupby;
		");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function addTransOut($arrDataTransaksi)
	{
		return $this->dbInsert(array(
			'txou_code' => $arrDataTransaksi['txou_code'],
			'txou_date' => $arrDataTransaksi['txou_date'],
			'txou_acco_id' => $arrDataTransaksi['txou_coa'],
			'txou_metode' => $arrDataTransaksi['txou_metode'],
			'txou_ref_giro' => (isset($arrDataTransaksi['txou_ref_giro']) ? $arrDataTransaksi['txou_ref_giro'] : 0 ),
			'txou_jatuhtempo' => (isset($arrDataTransaksi['txou_jatuhtempo']) ? str_replace("/", "-", $arrDataTransaksi['txou_jatuhtempo']) : "0" ),
			'txou_description' => (isset($arrDataTransaksi['txou_description']) ? $arrDataTransaksi['txou_description'] : "0" ),
			'txou_status_trans' => (isset($arrDataTransaksi['txou_status_trans']) ? $arrDataTransaksi['txou_status_trans'] : STATUS_FINISHED ),
		));
	}

	public function updateTransOut($arrDataTransaksi, $id)
	{		
		return $this->dbUpdate(array(			
			'txou_date' => $arrDataTransaksi['txou_date'],
			'txou_acco_id' => $arrDataTransaksi['txou_coa'],
			'txou_metode' => (isset($arrDataTransaksi['txou_metode']) ? $arrDataTransaksi['txou_metode'] : 0 ),
			'txou_ref_giro' => (isset($arrDataTransaksi['txou_ref_giro']) ? $arrDataTransaksi['txou_ref_giro'] : 0 ),
			'txou_jatuhtempo' => (isset($arrDataTransaksi['txou_jatuhtempo']) ? $arrDataTransaksi['txou_jatuhtempo'] : "0" ),
			'txou_description' => (isset($arrDataTransaksi['txou_description']) ? $arrDataTransaksi['txou_description'] : null )
		), "id='$id'");
	}

	public function deleteTransout($id)
	{
		return $this->dbUpdate(
			array(
				'txou_status_trans' => STATUS_DELETED
			), "id='$id'"
		);
	}

	public function copyToJournal()
	{
		# code...
	}

	public function copyToLaporan($arrDataLaporan)
	{
		// $this->setQuery("
		// 	INSERT INTO rhlaporanhutang (`nomormhsupplier`, `biaya`, `jenis`, `tanggal`) VALUES ()
		// ");
		$this->initialize('rhlaporanhutang');		
		return $this->dbInsert(array(
			'nomormhsupplier' => $arrDataLaporan['nomormhsupplier'],
			'biaya' => $arrDataLaporan['biaya'],
			'jenis' => $arrDataLaporan['jenis'],
			'tanggal' => $arrDataLaporan['tanggal'],
			'transaksi_nomor' => $arrDataLaporan['transaksi_nomor'],
			'transaksi_kode' => $arrDataLaporan['transaksi_kode'],
			'transaksi_tanggal' => $arrDataLaporan['transaksi_tanggal'],
			'jatuh_tempo' => $arrDataLaporan['jatuh_tempo'],
			'total' => $arrDataLaporan['total'],			
			'keterangan' => $arrDataLaporan['keterangan']		
		));
	}

	public function searchBy($strType, $arrSearchData)
	{				
		$strWhere = '';
		if ($arrSearchData['dateStart'] != '' && $arrSearchData['dateEnd'] == '') $strWhere .= " AND txou_date = '".str_replace("/", "-",$arrSearchData['dateStart'])."'";
		if ($arrSearchData['dateEnd'] != '') $strWhere .= " AND (txou_date BETWEEN '".str_replace("/", "-",$arrSearchData['dateStart'])."' AND '".str_replace("/", "-",$arrSearchData['dateEnd'])."')";
		if ($arrSearchData['strSearchKey'] != '') $strWhere .= " AND txou_code LIKE '%".$arrSearchData['strSearchKey']."%'";
		if ($arrSearchData['strSearchOther'] != '') { 
			if ($strType == 'PGL') $strWhere .= " AND txod_description LIKE '%".$arrSearchData['strSearchOther']."%'";
			else $strWhere .= " AND (txod_description LIKE '%".$arrSearchData['strSearchOther']."%' OR supp_name LIKE '%".$arrSearchData['strSearchOther']."%')";			
		}

		// $this->setQuery(
  //           "
  //           SELECT id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS txod_totalbersih,  txod_ppn, txod_type, reff_id, supp_name, cdate, mdate, txou_status_trans, txod_description FROM
		// (
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih,  txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH') as tabelDPH
		// LEFT JOIN downpayment as dp ON tabelDPH.dphi_reff_id = dp.id
		// LEFT JOIN jw_supplier as supp ON dp.dpay_supplier_id = supp.id
		// WHERE dphi_reff_type = 'DP' GROUP BY id)
		// UNION ALL
		// (SELECT  tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description
		// 	FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON pi.pinv_supplier_id = supp.id
		// WHERE dphi_reff_type = 'PI' GROUP BY id)
		// UNION ALL
		// (SELECT tabelDPH.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dphi_reff_id as reff_id, supp_name, tabelDPH.cdate AS cdate, tabelDPH.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT  txou.id as id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, dph_item.dphi_reff_type, dph_item.dphi_reff_id, txou.cdate, txou.mdate, txou_status_trans, txod_description, 
		// 	txod.txod_supp_id FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	LEFT JOIN daftar_pembayaran_hutang as dph ON txod.txod_ref_id = dph.id
		// 	LEFT JOIN daftar_pembayaran_hutang_item as dph_item ON dph.id = dph_item.dphi_dphu_id
		// 	WHERE txod_type = 'DPH' ORDER BY txou.cdate, txou.mdate) as tabelDPH
		// -- LEFT JOIN purchase_invoice as pi ON tabelDPH.dphi_reff_id = pi.id
		// LEFT JOIN jw_supplier as supp ON tabelDPH.txod_supp_id = supp.id
		// WHERE dphi_reff_type = 'NKS' GROUP BY id)
		// UNION ALL 
		// (SELECT tabelNKS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNKS.cdate AS cdate, tabelNKS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id , txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NKS' ORDER BY txou.cdate, txou.mdate) as tabelNKS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNKS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelNDS.id AS id, txou_code, txou_date, SUM(txod_amount) AS txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, supp_name, tabelNDS.cdate AS cdate, tabelNDS.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'NDS' ORDER BY txou.cdate, txou.mdate) as tabelNDS
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN jw_supplier as supp ON txod_supp_id = supp.id GROUP BY txou_code
		// )
		// UNION ALL 
		// (SELECT tabelPGL.id AS id, txou_code, txou_date, txod_amount, txod_totalbersih, txod_ppn, txod_type, txod_ref_id as ref_id, kont_name, tabelPGL.cdate AS cdate, tabelPGL.mdate AS mdate, txou_status_trans, txod_description FROM 
		// 	(SELECT txou.id AS id, txou_code, txou_date, SUM(txod_totalbersih) as txod_totalbersih, txod_amount, txod_ppn, txod_ref_id, txod_type, txod_supp_id, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out as txou
		// 	LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
		// 	WHERE txod_type = 'PGL' GROUP BY txou_code ORDER BY txou.cdate, txou.mdate) as tabelPGL
		// -- LEFT JOIN purchase_invoice as pi ON tabelNDS.txod_ref_id = pi.id
		// LEFT JOIN kontrak as kont ON txod_ref_id = kont.id
		// )
		// ) as unionTable
		// WHERE txou_code LIKE '$strType%' AND txou_status_trans > ".STATUS_DELETED." $strWhere
		// GROUP BY id
		// ORDER BY txou_date DESC, id ASC		
		// 	"
  //       );

		switch ($strType) {
        	case 'PHU': $strListOfType = "DPH"; break;
        	case 'NKS': $strListOfType = "NKS"; break;
        	case 'PGL': $strListOfType = "PGL"; break;
        }

        $this->setQuery("SELECT txou.id, txou_code, txou_date, SUM(txod_amount) as txod_amount, SUM(txod_totalbersih) AS txod_totalbersih, txod_ppn, txod_type, supp_name, txou.cdate, txou.mdate, txou_status_trans, txod_description FROM transaction_out txou
			LEFT JOIN transaction_out_detail txod ON txou.id = txod.txod_txou_id
			LEFT JOIN jw_supplier supp ON txod_supp_id = supp.id
		WHERE txod_type IN ('$strListOfType') AND txou_status_trans > ".STATUS_DELETED." $strWhere
		GROUP BY txou.id ORDER BY txou_date DESC, txou.id ASC");
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
	}

	public function rollbackAfterDelete($intTransOutID)
	{
		$this->setQuery("SELECT txod_type, txod_ref_id FROM transaction_out_detail WHERE txod_txou_id = '$intTransOutID'");

		if($this->getNumRows() > 0 ) $result = $this->getQueryResult('Array');
		else $result = false;

		foreach ($result as $value){
		
			$type = $value['txod_type'];
			$id = $value['txod_ref_id'];

			switch ($type){
				case 'DPH':
					$strTableName = "daftar_pembayaran_hutang";
					$strFieldName = "dphu_status";
					$intRollbackStatus = STATUS_APPROVED;
					break;			
			}
			
			$result = $this->setQuery("UPDATE $strTableName SET $strFieldName = $intRollbackStatus WHERE id = '$id'");

		}

		$this->_restartTerbayar($intTransOutID);

		return $result;
		
	}

	private function _restartTerbayar($intTransOutID)
	{
		$this->setQuery("SELECT txod_ref_id FROM transaction_out_detail WHERE txod_txou_id = '$intTransOutID' AND txod_type = 'DPH'");

		if($this->getNumRows() > 0 ) $result = $this->getQueryResult('Array');
		else return false;

		foreach ($result as $value) {
			$this->setQuery("SELECT dphi_reff_type, dphi_reff_id FROM daftar_pembayaran_hutang_item WHERE dphi_dphu_id = ".$value['txod_ref_id']);

			if($this->getNumRows() > 0 ) $itemDPH = $this->getQueryResult('Array');
			else return false;

			foreach ($itemDPH as $item) {
				switch ($item['dphi_reff_type']) {
					case 'PI': $strTableName = "purchase_invoice"; $strFieldName['id'] = "id"; $strFieldName['terbayar'] = "pinv_terbayar"; $strFieldName['sisa'] = "pinv_sisa_bayar"; $strFieldName['final'] = "pinv_finaltotal"; break;
					case 'NKS': $strTableName = "transaction_out_detail"; $strPre = "txod"; $strFieldName['id'] = "txod_txou_id"; $strFieldName['terbayar'] = "txod_terbayar"; $strFieldName['sisa'] = "txod_sisa_bayar"; $strFieldName['final'] = "txod_totalbersih"; break;
					case 'DP': $strTableName = "downpayment"; $strPre = "dpay"; $strFieldName['id'] = "id"; $strFieldName['terbayar'] = "dpay_terbayar"; $strFieldName['sisa'] = "dpay_sisa_bayar"; $strFieldName['final'] = "dpay_amount_final"; break;				
				}

				//Recalculate Terbayar di PHU
				$this->setQuery("SELECT COALESCE(SUM(txod_totalbersih),0) AS totalTerbayar
					FROM transaction_out AS txou
					LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id
					WHERE txod.txod_type = 'DPH' AND txod.txod_ref_id IN 
						( 
							SELECT dph.id FROM daftar_pembayaran_hutang AS dph 
							LEFT JOIN daftar_pembayaran_hutang_item AS dphi ON dph.id = dphi.dphi_dphu_id 
							WHERE dphi.dphi_reff_type = '".$item['dphi_reff_type']."' AND dph.dphu_status != ".STATUS_DELETED." AND dphi.dphi_reff_id = ".$item['dphi_reff_id']."
						)
					AND txou.txou_status_trans != ".STATUS_DELETED
				);
				// End of Recalculate

				$totalTerbayar = (Float) $this->getNextRecord('Array')["totalTerbayar"];				
				
				$this->setQuery("UPDATE $strTableName SET ".$strFieldName['terbayar']." = ".$totalTerbayar.", ".$strFieldName['sisa']." = `".$strFieldName['final']."` - ".$totalTerbayar."  WHERE ".$strFieldName['id']."=".$item['dphi_reff_id']);
			}
						
		}		

	}

}