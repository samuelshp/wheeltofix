<?php
/*
PUBLIC FUNCTION:
- getTaxInvoiceByID(intID)
- getAllInvoice(strKeyword)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsByCustomerID(intID,strDate)
- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
- getItemByID(intID)
- getPrintDataByID(intID)
- getUserHistory(intUserID,intPerPage)
- getCount()
- getDateMark(strFrom,strTo)
- getCustomers(strFrom,strTo)
- getStockSold(intProductID,intWarehouseID,strLastStockUpdate)
- searchByCustomer(strCustomerName)
- add(intOutletID, intCustomerID,intSalesmanID,intSuppID,intWarehouseID,strDescription,intStatus,strDate)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intStatus)
- editStatusByID(intID,intStatus)
- editAfterPrint(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Minvoiceretur extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();
	$this->initialize('invoice_retur');
}

public function getTaxInvoiceByID($intID) {
	$this->setQuery(
'SELECT invo_tax
FROM invoice AS i
WHERE id ='.$intID.'');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAllInvoice($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (invr_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%')";
	} else $strWhere = '';

	$this->setQuery(
'SELECT i.id,invr_code,cust_name,invr_status,i.cdate,cust_address,cust_city
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invr_salesman_id = s.id
WHERE invr_status IN (0,2)'.$strWhere.'
ORDER BY i.id DESC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invr_code
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
ORDER BY i.cdate DESC, i.id DESC LIMIT $intStartNo, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDate($strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invr_code
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
WHERE DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByCustomerID($intID,$strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invr_code
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
WHERE invr_customer_id = $intID AND DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');

	$this->setQuery(
"SELECT ii.id, cust_name, cust_address, cust_city, i.cdate AS invr_date, invr_status, inri_quantity
FROM invoice_retur AS i
INNER JOIN invoice_retur_item AS ii ON inri_invoice_retur_id = i.id
LEFT JOIN jw_customer AS c ON inri_customer_id = c.id
WHERE inri_product_id = $intProductID AND invr_warehouse_id = $intWarehouseID AND i.cdate > '$strLastStockUpdate'");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	if($intID == '') $strWhere = "ORDER BY i.cdate DESC, i.id DESC LIMIT 0,1";
	else $strWhere = "
WHERE i.id = $intID
ORDER BY i.cdate DESC, i.id DESC";
	
	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_description,invr_progress, invr_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invr_code,sals_code, sals_name,invr_customer_id as cust_id,cust_outlettype,cust_markettype,invr_supplier_id AS supp_id,cust_internal
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
LEFT JOIN jw_salesman AS sl on invr_salesman_id = sl.id
$strWhere");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}
    
public function getPrintDataByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY i.cdate DESC, i.id DESC LIMIT 0,1";
    else $strWhere = "WHERE i.id = $intID";
	
	$this->setQuery(
"SELECT sals_name,sals_code, ware_name
FROM invoice_retur AS i
LEFT JOIN jw_salesman AS s ON invr_salesman_id = s.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_status, cust_name, cust_address, cust_city,invr_code
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
WHERE (i.cby = $intUserID OR i.mby = $intUserID)
ORDER BY i.cdate DESC, i.id DESC LIMIT 0, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id');

	return $this->getNumRows();
}

public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as invr_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'cdate ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCustomers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT invr_customer_id,DATE(cdate) AS invo_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'invr_customer_id ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockSold($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0;

	$this->setQuery(
"SELECT inri_quantity
FROM invoice_retur AS i
INNER JOIN invoice_retur_item AS ii ON inri_invoice_retur_id = i.id
WHERE inri_product_id = $intProductID AND invr_warehouse_id = $intWarehouseID AND invr_status IN (2,3) AND i.cdate > '$strLastStockUpdate'");

	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['inri_quantity'];

	return $intTotalStock;
}

public function searchByCustomer($strCustomerName, $arrSearchDate, $intStatus) {
	$strWhere = '';
    if(!empty($strCustomerName)) {
        $strCustomerName = urldecode($strCustomerName);
        $strWhere .= " AND (cust_name LIKE '%{$strCustomerName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (i.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (i.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (invr_status = {$intStatus})";

	$this->setQuery(
"SELECT i.id, i.cdate AS invr_date, invr_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invr_code
FROM invoice_retur AS i
LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invr_warehouse_id = w.id
WHERE i.id > 0{$strWhere}
ORDER BY i.cdate DESC, i.id  DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intCustomerID,$intSalesmanID,$intSuppID,$intWarehouseID,$strDescription,$intStatus,$strDate) {
	return $this->dbInsert(array(
		'invr_customer_id' => $intCustomerID,
		'invr_salesman_id' => $intSalesmanID,
		'invr_supplier_id' => $intSuppID,
		'invr_branch_id' => $this->session->userdata('intBranchID'),
		'invr_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
		'invr_description' => $strDescription,
		'invr_status' => $intStatus,
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
	));
}

public function editByID($intID,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
			'invr_progress' => $strProgress,
			'invr_status' => $intStatus),
		"id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
			'invr_description' => $strDescription,
			'invr_progress' => $strProgress,
			'invr_status' => $intStatus),
		"id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'invr_status' => $intStatus),"id = $intID");
}

public function editAfterPrint($intID) {
	return $this->dbUpdate(array(
		'invr_status' => '2'),"id = $intID AND invr_status = '3'");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */