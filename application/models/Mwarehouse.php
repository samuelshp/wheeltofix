<?php
/*
PUBLIC FUNCTION:
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Mwarehouse extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('jw_warehouse');
}

public function getAllData($strKeyword = '') {
	$strKeyword = urldecode($strKeyword);

    if(!empty($strKeyword)) $strWhere = " AND (ware_name LIKE '%$strKeyword%')";
    else $strWhere = '';

    $this->setQuery(
"SELECT id,ware_name,ware_stock_warning
FROM jw_warehouse
WHERE ware_status > 1$strWhere
ORDER BY ware_name ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,ware_name',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */