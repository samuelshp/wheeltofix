<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Euangmasuknoncustomer extends Eloquent 
{
    protected $table = 'uang_masuk_non_customer';
    public $timestamps = false;

    public function account()
    {
        return $this->belongsTo(Eaccount::class);
    }
}