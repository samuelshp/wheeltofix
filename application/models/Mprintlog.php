<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intRepayment)

PRIVATE FUNCTION:
- __construct()	
*/

class Mprintlog extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();
    $this->initialize('print_log');
}

public function add($strInvoiceNumber, $strType, $intTransID, $strTemplateCode) {
    return $this->dbInsert(array(
        'prlo_invoice_number' => $strInvoiceNumber,
        'prlo_type' => $strType,
        'prlo_transaction_id' => $intTransID,
        'prlo_template_code' => $strTemplateCode,
    ));
}

public function getCount($strInvoiceNumber) {
    $this->setQuery(
'SELECT id
FROM print_log AS p
WHERE p.prlo_invoice_number = "'.$strInvoiceNumber.'"');

    return $this->getNextRecord();
}

public function getInvoiceNumber($strType,$intTransID) {
    $this->setQuery(
'SELECT prlo_invoice_number
FROM print_log AS p
WHERE p.prlo_type = "'.$strType.'" AND p.prlo_transaction_id = '.$intTransID.'');
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getLastTemplate($strType, $intTransID) {
    $this->dbSelect('prlo_template_code', "prlo_type = '$strType' AND prlo_transaction_id = $intTransID", 'cdate DESC', 1);
    if($this->getNumRows() > 0) return $this->getNextRecord('Object')->prlo_template_code;
    return false;
}

}

/* End of File */