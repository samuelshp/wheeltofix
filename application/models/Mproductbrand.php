<?php

class Mproductbrand extends JW_Model {

public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_product_brand');
}

public function getAllItems($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strWhere = " AND prob_title LIKE '%$strKeyword%'";
	} else {
		$strWhere = '';
	}
	$this->dbSelect('', "prob_status > 1$strWhere");
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('', "id = $intID");
	return $this->getNextRecord('Array');
}

}