<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

$date = date('Y/m/d H:i:s');

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>


<div class="col-xs-12">
    <!-- <?php
    //$strSearchAction = site_url('daftar_pembayaran_hutang/browse', NULL, FALSE);
    //include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearckodeuang.php'); ?> -->
    <!-- <form name="searchForm" method="post" action="<?=site_url('daftar_pembayaran_hutang', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
            <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search your keyword here.." />
            <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form> -->
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;">
        <a href="<?=site_url('daftar_pembayaran_hutang', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover main-list-table">
            <thead>
                <tr>
                	<th>No. DPH</th>
                    <th>Tanggal DPH</th>                	
                    <th>Amount</th>
                    <th class="action">Action</th>      
                </tr>
            </thead>
            <tbody>
                <?php
                // Display data in the table
                if(!empty($arrDaftarPembayaranHutang)):
                    $i=0;
                    foreach($arrDaftarPembayaranHutang as $e):$i++ ?>
                        <tr style="text-align:center;">
                            <td><?=$e['dphu_code'] //diisi field nomor pb ?></td> 
                            <td><?=formatDate2($e['dphu_date'],'d F Y')?></td>                            
                            <td><?=setPrice($e['dphu_amount'])//diisi field amount  ?></td>
                            <td class="action">
                                <a href="<?=site_url('daftar_pembayaran_hutang/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>
                            </a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    else: ?>
                        <tr class="info"><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
                    endif; ?>
                </tbody>
            </table>
        </div>        
    </div>
