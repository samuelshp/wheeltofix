<?php

class Mreportinvoice extends JW_Model {

public $_intWarehouseID;

public function __construct() {
	parent::__construct();	
	$this->_intWarehouseID = 1;
}

public function getItemsDetailJualPerBarangByCustomer($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invo_code ASC";
    else $strOrderBy = "invo_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, c.cust_name,it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountDetailJualPerBarangByCustomer() {
    /*$this->setQuery(
"SELECT iv.invo_code
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)");*/
	$this->setQuery("SELECT iv.invo_code FROM invoice iv");

    return $this->getNumRows();
}

public function searchDetailJualPerBarangByCustomer($productID,$txtStart,$txtEnd) {

    $productID = urldecode($productID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, c.cust_name,it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
WHERE p.id = $productID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invo_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getItemsDetailJualPerCustomerByBarang($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invo_code ASC";
    else $strOrderBy = "invo_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, p.prod_title, it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountDetailJualPerCustomerByBarang() {
    /*$this->setQuery(
"SELECT iv.invo_code
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)");*/
	$this->setQuery("SELECT iv.invo_code FROM invoice iv");

    return $this->getNumRows();
}

public function searchDetailJualPerCustomerByBarang($custID,$txtStart,$txtEnd) {

    $custID = urldecode($custID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, p.prod_title, it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
WHERE c.id = $custID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invo_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsDetailJualPerSalesmanByBarang($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invo_code ASC";
    else $strOrderBy = "invo_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, p.prod_title, it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountDetailJualPerSalesmanByBarang() {
	/*$this->setQuery(
"SELECT iv.invo_code
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)");*/
	$this->setQuery("SELECT iv.invo_code FROM invoice iv");

    return $this->getNumRows();
}

public function searchDetailJualPerSalesmanByBarang($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invo_code, iv.cdate, p.prod_title, it.invi_quantity1,it.invi_quantity2,
	it.invi_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.invi_price,
	it.invi_discount1,it.invi_discount2,it.invi_discount3
FROM invoice iv
	INNER JOIN invoice_item it ON (it.invi_invoice_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.invi_product_id)
	LEFT JOIN jw_salesman s ON (s.id = iv.invo_salesman_id)
WHERE s.id = $salsID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invo_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsDetailJualPerSalesmanByCustomer($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invo_code ASC";
    else $strOrderBy = "invo_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invo_code,iv.cdate,c.cust_code,c.cust_name,iv.invo_grandtotal,iv.invo_discount,iv.invo_tax
FROM invoice iv
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountDetailJualPerSalesmanByCustomer() {
    /*$this->setQuery(
"SELECT iv.invo_code
FROM invoice iv
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)");*/
	$this->setQuery("SELECT iv.invo_code FROM invoice iv");

    return $this->getNumRows();
}

public function searchDetailJualPerSalesmanByCustomer($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
    
    $this->setQuery(
"SELECT iv.invo_code,iv.cdate,c.cust_code,c.cust_name,iv.invo_grandtotal,iv.invo_discount,iv.invo_tax
FROM invoice iv
	LEFT JOIN jw_customer c ON (c.id = iv.invo_customer_id)
	LEFT JOIN jw_salesman s ON (s.id = iv.invo_salesman_id)
WHERE s.id = $salsID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invo_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsRekapJualPerSalesmanByCustomer($intStartNo = -1, $intPerPage = -1, $salsID = 0) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "cust_code,cust_name ASC";
    else $strOrderBy = "cust_code,cust_name ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT i.invo_customer_id, cust_code, cust_name, cust_city,
	sum(i.invo_grandtotal) as 'total',
	sum(ii.invi_quantity1) as 'qty1', sum(ii.invi_quantity2) as 'qty2', sum(ii.invi_quantity3) as 'qty3'
FROM invoice i
   INNER JOIN invoice_item ii ON (ii.invi_invoice_id = i.id)
   LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
WHERE i.invo_salesman_id = $salsID AND invo_status in (2,3)
GROUP BY i.invo_customer_id, cust_code, cust_name, cust_city
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountRekapJualPerSalesmanByCustomer($salsID = 0) {
    /*$this->setQuery(
"SELECT i.invo_customer_id
FROM invoice i
   INNER JOIN invoice_item ii ON (ii.invi_invoice_id = i.id)
   LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
WHERE i.invo_salesman_id = $salsID AND invo_status in (2,3)
GROUP BY i.invo_customer_id, cust_code, cust_name, cust_city");*/
    $this->setQuery(
"SELECT i.invo_customer_id
FROM invoice i
WHERE i.invo_salesman_id = $salsID AND invo_status in (2,3)
GROUP BY i.invo_customer_id");

    return $this->getNumRows();

}

public function searchRekapJualPerSalesmanByCustomer($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT i.invo_customer_id, cust_code, cust_name, cust_city,
	sum(i.invo_grandtotal) as 'total',
	sum(ii.invi_quantity1) as 'qty1', sum(ii.invi_quantity2) as 'qty2', sum(ii.invi_quantity3) as 'qty3'
FROM invoice i
   INNER JOIN invoice_item ii ON (ii.invi_invoice_id = i.id)
   LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
WHERE i.invo_salesman_id = $salsID AND invo_status in (2,3) AND i.cdate BETWEEN '$txtStart' AND '$txtEnd'
GROUP BY i.invo_customer_id, cust_code, cust_name, cust_city");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsRekapJualPerCustomerByBarang($intStartNo = -1, $intPerPage = -1, $custID = 0) {
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "prod_title ASC";
    else $strOrderBy = "prod_title ASC LIMIT $intStartNo, $intPerPage";
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,invi_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id
ORDER BY $strOrderBy");

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountRekapJualPerCustomerByBarang() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

   /*$this->setQuery(
"SELECT prod_code
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");*/
   $this->setQuery(
"SELECT trhi_product_id
FROM transaction_history th
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");

    return $this->getNumRows();
}

public function searchRekapJualPerCustomerByBarang($custID,$txtStart,$txtEnd) {
    $custID = urldecode($custID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,invi_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
	  INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3) AND trhi_date BETWEEN '$txtStart' AND '$txtEnd'
    AND invo_customer_id = $custID
GROUP BY th.trhi_product_id
ORDER BY prod_title");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsRekapJualPerSalesmanByBarang($intStartNo = -1, $intPerPage = -1, $salsID = 0) {
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "prod_title ASC";
    else $strOrderBy = "prod_title ASC LIMIT $intStartNo, $intPerPage";
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,invi_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
	  INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3) 
	AND i.invo_salesman_id = $salsID
GROUP BY th.trhi_product_id
ORDER BY $strOrderBy");

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountRekapJualPerSalesmanByBarang() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    /*$this->setQuery(
"SELECT prod_code
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");*/
    $this->setQuery(
"SELECT trhi_product_id
FROM transaction_history th
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");

    return $this->getNumRows();

}

public function searchRekapJualPerSalesmanByBarang($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,invi_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
	  INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)
WHERE th.trhi_type = 'invoice' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3) AND trhi_date BETWEEN '$txtStart' AND '$txtEnd'
    AND invo_salesman_id = $salsID
GROUP BY th.trhi_product_id
ORDER BY prod_title");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

// patricklipesik begin
public function searchInvoicePerKasir($txtStart,$txtEnd,$userID) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d').' 00:00:00';
	$txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d').' 23:59:59';
	$userID = urldecode($userID);
	$strWhere = "";
	if(!empty($userID))
		$strWhere = "AND (iv.cby = ".$userID.")"; // OR iv.mby = ".$userID."
	
	$this->setQuery(
"	SELECT iv.id, iv.invo_code, iv.cdate, iv.invo_grandtotal, iv.invo_discount, iv.invo_tax, iv.invo_payment, iv.invo_status, iv.invo_locked,
	c.cust_code, c.cust_name,
	a.adlg_login
	FROM invoice iv
	LEFT JOIN jw_customer c ON c.id = iv.invo_customer_id
	LEFT JOIN adm_login a ON a.id = iv.cby
	WHERE iv.invo_status IN (2,3)
	AND iv.invo_branch_id = ".$branchID."
	AND iv.cdate BETWEEN '".$txtStart."' AND '".$txtEnd."'
	".$strWhere."
	ORDER BY iv.invo_code");
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}
public function getCountInvoicePerKasir() {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	$this->setQuery(
"	SELECT iv.id, iv.invo_code, iv.cdate, iv.invo_grandtotal, iv.invo_discount, iv.invo_tax,
	c.cust_code, c.cust_name,
	a.adlg_login
	FROM invoice iv
	LEFT JOIN jw_customer c ON c.id = iv.invo_customer_id
	LEFT JOIN adm_login a ON a.id = iv.cby
	WHERE iv.invo_status IN (2,3)
	AND iv.invo_branch_id = ".$branchID."
	AND iv.cdate BETWEEN '".$txtStart."' AND '".$txtEnd."'");
	
    return $this->getNumRows();
}
public function getItemsInvoicePerKasir($intStartNo = -1,$intPerPage = -1) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	if($intStartNo == -1 || $intPerPage == -1)
		$strLimit = "";
	else
		$strLimit = " LIMIT ".$intStartNo.", ".$intPerPage;
	
	$this->setQuery(
"	SELECT iv.id, iv.invo_code, iv.cdate, iv.invo_grandtotal, iv.invo_discount, iv.invo_tax, iv.invo_payment, iv.invo_status, iv.invo_locked,
	c.cust_code, c.cust_name,
	a.adlg_login
	FROM invoice iv
	LEFT JOIN jw_customer c ON c.id = iv.invo_customer_id
	LEFT JOIN adm_login a ON a.id = iv.cby
	WHERE iv.invo_status IN (2,3)
	AND iv.invo_branch_id = ".$branchID."
	AND iv.cdate BETWEEN '".$txtStart."' AND '".$txtEnd."'
	ORDER BY iv.invo_code
	".$strLimit);
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}

public function searchPenjualanRekap($txtStart,$txtEnd,$custName,$salsName) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d').' 00:00:00';
	$txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d').' 23:59:59';
	$custName = urldecode($custName);
	if(empty($custName))
		$custName = "%";
	else
		$custName = "%".$custName."%";
	$salsName = urldecode($salsName);
	if(empty($salsName))
		$salsName = "%";
	else
		$salsName = "%".$salsName."%";
	
	$this->setQuery(
"	SELECT a.id, a.invo_code AS faktur, a.cdate AS tanggal, a.invo_jatuhtempo AS jatuhtempo,
	a.invo_grandtotal AS total, a.invo_description AS keterangan,
	b.cust_name AS customer, b1.kcmt_title AS kecamatan, b2.kota_title AS kota,
	c.sals_name AS salesman, d.ware_name AS gudang, e.supp_name AS supplier
	FROM invoice a
	LEFT JOIN jw_customer b ON a.invo_customer_id = b.id
	LEFT JOIN jw_kecamatan b1 ON b.cust_kecamatan_id = b1.id
	LEFT JOIN jw_kota b2 ON b.cust_kota_id = b2.id
	LEFT JOIN jw_salesman c ON a.invo_salesman_id = c.id AND c.sals_name LIKE '".$salsName."'
	JOIN jw_warehouse d ON a.invo_warehouse_id = d.id
	LEFT JOIN jw_supplier e ON a.invo_supplier_id = e.id
	WHERE a.invo_status IN (2,3)
	AND a.invo_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'
	AND b.cust_name LIKE '".$custName."'
	ORDER BY a.invo_code, a.cdate");
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}
public function getCountPenjualanRekap() {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	$this->setQuery(
"	SELECT a.id, a.invo_code AS faktur, a.cdate AS tanggal, a.invo_jatuhtempo AS jatuhtempo,
	a.invo_grandtotal AS total, a.invo_description AS keterangan,
	b.cust_name AS customer, b1.kcmt_title AS kecamatan, b2.kota_title AS kota,
	c.sals_name AS salesman, d.ware_name AS gudang, e.supp_name AS supplier
	FROM invoice a
	LEFT JOIN jw_customer b ON a.invo_customer_id = b.id
	LEFT JOIN jw_kecamatan b1 ON b.cust_kecamatan_id = b1.id
	LEFT JOIN jw_kota b2 ON b.cust_kota_id = b2.id
	LEFT JOIN jw_salesman c ON a.invo_salesman_id = c.id
	JOIN jw_warehouse d ON a.invo_warehouse_id = d.id
	LEFT JOIN jw_supplier e ON a.invo_supplier_id = e.id
	WHERE a.invo_status IN (2,3)
	AND a.invo_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'");
	
    return $this->getNumRows();
}
public function getItemsPenjualanRekap($intStartNo = -1,$intPerPage = -1) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	if($intStartNo == -1 || $intPerPage == -1)
		$strLimit = "";
	else
		$strLimit = " LIMIT ".$intStartNo.", ".$intPerPage;
	
	$this->setQuery(
"	SELECT a.id, a.invo_code AS faktur, a.cdate AS tanggal, a.invo_jatuhtempo AS jatuhtempo,
	a.invo_grandtotal AS total, a.invo_description AS keterangan,
	b.cust_name AS customer, b1.kcmt_title AS kecamatan, b2.kota_title AS kota,
	c.sals_name AS salesman, d.ware_name AS gudang, e.supp_name AS supplier
	FROM invoice a
	LEFT JOIN jw_customer b ON a.invo_customer_id = b.id
	LEFT JOIN jw_kecamatan b1 ON b.cust_kecamatan_id = b1.id
	LEFT JOIN jw_kota b2 ON b.cust_kota_id = b2.id
	LEFT JOIN jw_salesman c ON a.invo_salesman_id = c.id
	JOIN jw_warehouse d ON a.invo_warehouse_id = d.id
	LEFT JOIN jw_supplier e ON a.invo_supplier_id = e.id
	WHERE a.invo_status IN (2,3)
	AND a.invo_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'
	ORDER BY a.invo_code, a.cdate
	".$strLimit);
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}

}