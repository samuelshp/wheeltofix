<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intRepayment)

PRIVATE FUNCTION:
- __construct() 
*/

class Mrepayment extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();
    $this->initialize('repayment');
}

public function add($intID, $intType,$txtDate,$intPurchaseID=0,$intAccountID=0,$strBankID='',$txtBG='',$strDescription='',$intStatus) {
    return $this->dbInsert(array(
        'repa_header_id' => $intID,
        'repa_type' => $intType,
        'repa_date' => $txtDate,
        'repa_purchase_id' => $intPurchaseID,
        'repa_account_id' => $intAccountID,
        'repa_bank_id' => $strBankID,
        'repa_bg_number' => $txtBG,
        'repa_description' => $strDescription,
        'repa_status' => $intStatus,
    ));
}


public function getAllRepayment($strKeyword = '',$intPerPage = 0,$intStartNo = 0) {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " WHERE reha_code LIKE '%$strKeyword%' OR supp_name LIKE '%$strKeyword%'";
    } else $strWhere = '';

    if($intPerPage > 0) $strLimit = "LIMIT $intStartNo,$intPerPage";
    else $strLimit = "LIMIT 0,18446744073709551615";

    $this->setQuery(
'        SELECT 
        p.id,
        reha_code,
        supp_name,supp_address,supp_city,supp_phone,
        repa_code,repa_date,repa_type,repa_description,repa_bank_id,repa_bg_number,repa_status,
        acco_code,acco_name,
        prch_grandtotal,pur.cdate AS prch_date
        FROM repayment AS p
        LEFT JOIN repayment_header AS rh ON repa_header_id = rh.id
        LEFT JOIN purchase AS pur ON repa_purchase_id = pur.id
        LEFT JOIN jw_account AS ac ON repa_account_id = ac.id
        LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
        '.$strWhere.'
        ORDER BY p.cdate DESC, p.id DESC
        '.$strLimit);

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount() {
    $this->dbSelect('id');

    return $this->getNumRows();
}

public function getStatusRepaymentByID($intID) {
    $this->setQuery(
'SELECT repa_status
FROM repayment AS p
WHERE p.id = '.$intID.'');
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function editByID($intID,$intStatus='') {
    return $this->dbUpdate(array(
            'repa_status' => $intStatus),
        "id = $intID");
}

public function editByID2($intID,$strDescription,$intStatus) {
    return $this->dbUpdate(array(
            'repa_description' => $strDescription,
            'repa_status' => $intStatus),
        "id = $intID");
}

public function getHeaderIDByID($intID) {

    $this->setQuery(
'SELECT r.id,repa_header_id
FROM repayment AS r
WHERE r.id = '.$intID.'
ORDER BY r.id DESC');

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataHeaderByID($intID) {

    $this->setQuery(
'SELECT rh.id, reha_code,rh.cdate AS reha_date
FROM repayment_header AS rh
WHERE rh.id = '.$intID.'
ORDER BY rh.id DESC');

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataByID($intID) {

    $this->setQuery(
'SELECT 
r.id, repa_code, repa_type, repa_date, repa_bank_id,repa_bg_number,repa_description, prch_code,prch_grandtotal,supp_name
FROM repayment AS r
LEFT JOIN purchase AS p ON repa_purchase_id = p.id
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
WHERE repa_header_id = '.$intID.'
ORDER BY r.cdate DESC, r.id ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function autoExpired($intDay=1) {
    // Tunai
    $this->setQuery(
"UPDATE repayment SET repa_status = 3 WHERE DATE_ADD(cdate,INTERVAL {$intDay} DAY) <= CURRENT_TIMESTAMP AND repa_status = 2 AND repa_type = 4");
    // BG
    $this->setQuery(
"UPDATE repayment SET repa_status = 3 WHERE repa_date <= CURRENT_TIMESTAMP AND repa_status = 2 AND repa_type = 6");
    return true;
}

}

/* End of File */