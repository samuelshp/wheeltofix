<?php
/*
PUBLIC FUNCTION:
- getAllacceptance2dItemByPOID(intPOID)
- getAllacceptance2dBonusItemByPOID(intPOID)
- getItemsByacceptance2ID(intacceptance2ID,strMode)
- getBonusItemsByacceptance2ID(intacceptance2ID,strMode)
- getLatestPrice(intProductID)
- add(intacceptance2ID,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3,unit1Item,unit2Item,unit3Item,intPrice,intDiscount1,intDiscount2,intDiscount3,flagBonus,flagExist)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- deleteByacceptance2ID(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Macceptanceitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('acceptance_item');
}

public function getAllacceptance2dItemByPOID($intPOID) {
    $this->setQuery(
"SELECT  acit_product_id, acit_quantity1, acit_quantity2, acit_quantity3
FROM acceptance_item AS pi
LEFT JOIN acceptance AS p ON pi.acit_acceptance_id = p.id
WHERE p.acce_po_id = $intPOID AND p.acce_status = '".STATUS_APPROVED."' AND pi.acit_bonus='0'
ORDER BY pi.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllacceptance2dBonusItemByPOID($intPOID) {
    $this->setQuery(
"SELECT  acit_product_id, acit_quantity1, acit_quantity2, acit_quantity3
FROM acceptance_item AS pi
LEFT JOIN acceptance AS p ON pi.acit_acceptance_id = p.id
WHERE p.acce_po_id = $intPOID AND p.acce_status ='".STATUS_APPROVED."' AND pi.acit_bonus='1'
ORDER BY pi.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsByAcceptanceID($intacceptanceID,$strMode = 'ALL') {
//     $this->setQuery(
// "SELECT p.id, acit_product_id, pro.prod_title, pu.prch_code, po.idKontrak, pu.cdate, p.acit_quantity1, acit_quantity2, acit_quantity3, acit_price, acit_arrived1, acit_arrived2, acit_arrived3, acit_product_id, prod_code,prod_title,acit_description,acit_unit1,acit_unit2,acit_unit3,acit_exist,prod_code,acit_subtotal, prod_conv1, prod_conv2, prod_conv3, p.acit_purchaseid, p.acit_location
// FROM acceptance_item AS p
// LEFT JOIN jw_product AS pro ON pro.id = p.acit_product_id
// LEFT JOIN purchase AS pu ON pu.id = p.acit_purchaseid
// LEFT JOIN purchase_order as po ON po.id = pu.prch_po_id
// WHERE acit_acceptance_id =  $intacceptanceID AND acit_bonus = '0'
// ORDER BY p.id ASC");

    $this->setQuery(
        "SELECT ai.id, ai.acit_quantity1, poi.proi_description, k.kont_name, ai.acit_quantity_bayar, ai.acit_quantity_pb, pi.purchi_terterima, pi.prci_quantity1 - pi.purchi_terterima as jum_purch, pi.prci_price, ai.acit_description, ai.acit_acceptance_id as acc_id, p.prch_code, js.supp_name, sk.job as kont_job, p.cdate, ai.acit_quantity1 as jumlah_diterima, pi.prci_quantity1-pi.purchi_terterima as jumlah_purchase, jp.prod_title, ai.acit_arrived1, po.idKontrak, po.idSubKontrak, jp.id as product_id_baru, p.id as purchase_id_baru, pi.id as purchase_item_id, ai.acit_purchase_id, ju_pb.unit_title as satuan_pb, ju_terima.unit_title as satuan_terima, ju_bayar.unit_title as satuan_bayar,	SUM(ai.acit_quantity1 * pi.prci_price) as grandTotal
        FROM acceptance_item as ai
        LEFT JOIN jw_product as jp ON jp.id = ai.acit_product_id
        LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
        LEFT JOIN purchase_item as pi ON pi.id = ai.acit_purchase_item_id
        LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
        LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id AND sk.id = po.idSubKontrak
        LEFT JOIN jw_supplier as js ON js.id = p.prch_supplier_id
        LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
        LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
        LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
        WHERE ai.acit_acceptance_id = $intacceptanceID
        GROUP BY ai.id"
    );
    
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getDataPurchase($accId, $purchaseId, $productId){
    $this->setQuery(
        "SELECT pi.prci_quantity1 - SUM(`acit_quantity1`) as jml_purch_now 
        FROM `acceptance_item` as ai
        LEFT JOIN purchase_item as pi ON pi.prci_purchase_id = $purchaseId AND pi.prci_product_id = $productId
        WHERE `acit_purchase_id` = $purchaseId AND `acit_acceptance_id` < $accId AND `acit_product_id` = $productId"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSumRow($accId){
    $this->setQuery(
        "SELECT COUNT(*) as jml_row 
        FROM `acceptance_item` as ai
        WHERE `acit_acceptance_id` = $accId "
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function editItemsById($qty_terima_baru, $keterangan_terima_baru, $acit_item_id){
    // $this->setQuery(
    //     "UPDATE acceptance_item SET acit_arrived1 = $qty_terima_baru, acit_location = '$lokasi_terima_baru'
    //     WHERE id = $acit_item_id "
    // );
    // return true;

    //echo "<script>alert($prod_id $purch_id $purchi_id);</script>";

    $_CI =& get_instance();
    $_CI->load->model('Macceptanceitem');
    $_CI->Macceptanceitem->dbUpdate(array(
        'acit_quantity1' => $qty_terima_baru,
        'acit_description' => $keterangan_terima_baru),
        "id = $acit_item_id");

    // $this->initialize('acceptance_item');
    // return $this->dbUpdate(array(
    //     'acit_quantity1' => $qty_terima_baru,
    //     'acit_description' => $keterangan_terima_baru),
    // "id = $acit_item_id");
}

public function editItem($purchi_id, $qty_lama){

    $_CI =& get_instance();
    $_CI->load->model('Mpurchaseitem');
    $_CI->Mpurchaseitem->dbUpdate(array(
        'prci_quantity1' => $qty_lama),
        "id = $purchi_id");
    
    // $this->initialize('purchase_item');
    
    // return $this->dbUpdate(array(
    //     'prci_quantity1' => $qty_lama),
    // "id = $purchi_id");
}

public function getBonusItemsByAcceptanceID($intacceptanceID,$strMode = 'ALL') {
    $this->setQuery(
"SELECT p.id,pro.id as product_id, acit_quantity1, acit_quantity2, acit_quantity3, acit_product_id, prob_title, proc_title, prod_title,acit_description,acit_unit1,acit_unit2,acit_unit3,proc_title,acit_exist,prod_code, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS pro
LEFT JOIN acceptance_item AS p ON pro.id = p.acit_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE acit_acceptance_id =  $intacceptanceID AND acit_bonus = '1'
ORDER BY p.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getLatestPrice($intProductID) {
	$this->setQuery("SELECT prod_currency FROM jw_product WHERE id = $intProductID");
	
	if($this->getNumRows() > 0) $intCurrencyID = $this->getNextRecord('Object')->prod_currency;
	else return false;

	$this->dbSelect('acit_price',"acit_product_id = $intProductID",'cdate DESC',1);
	
	if($this->getNumRows() > 0) return formatPriceCurrency($this->getNextRecord('Object')->acit_price,'BASE',$intCurrencyID);
	else return false;
}

public function getLastProductPrice($intProductID, $intCount = 5) {
    $this->setQuery(
"SELECT acit_price, a.cdate, supp_name
FROM acceptance_item AS ai
LEFT JOIN acceptance AS a ON a.id = ai.acit_acceptance_id
LEFT JOIN jw_supplier AS s ON s.id = a.acce_supplier_id
WHERE ai.acit_product_id = {$intProductID}
ORDER BY a.cdate DESC, a.id DESC
LIMIT 0, {$intCount}");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function getMaxItemsByAcceptedItemIDAndProdID($intPurchaseID,$intProductID) {

    $this->setQuery(
"    SELECT prci_quantity1, prci_quantity2, prci_quantity3,prci_arrived1, prci_arrived2, prci_arrived3, prci_unit1,prci_unit2,prci_unit3
    FROM purchase_item AS p
    WHERE p.prci_purchase_id =  $intPurchaseID AND p.prci_product_id = $intProductID AND  p.prci_bonus ='0'"
    );

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getMaxBonusItemsByAcceptedItemIDAndProdID($intPurchaseID,$intProductID) {

    $this->setQuery(
"    SELECT prci_quantity1, prci_quantity2, prci_quantity3,prci_arrived1, prci_arrived2, prci_arrived3, prci_unit1,prci_unit2,prci_unit3
    FROM purchase_item AS p
    WHERE p.prci_purchase_id =  $intPurchaseID AND p.prci_product_id = $intProductID AND  p.prci_bonus ='1'"
    );

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function add($intacceptanceID,$intProductID,$strProductDescription,$intArrived1,$intPurchaseId,$location, $qty_pb, $purchase_id_item, $qty_bayar) {
	// Tambahan untuk membuat cdate item sama dengan cdate header
    // Tujuannya supaya di transaction history juga cdatenya sama
    $this->initialize('acceptance_item');
	$this->setQuery("SELECT cdate FROM acceptance WHERE id = $intacceptanceID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';
    //$intPurchaseId=0;
    //echo "<script type='text/javascript'>alert('Masuk sini ".$strProductDescription." ".$intQuantity1." ".$intArrived1." ');</script>";
	return $this->dbInsert(array(
		'acit_acceptance_id' => $intacceptanceID,
		'acit_product_id' => $intProductID,
		'acit_description' => $strProductDescription,
        'acit_quantity1' => str_replace(['.',','],['','.'],$intArrived1),
        'acit_quantity_pb' => str_replace(['.',','],['','.'],$qty_pb),
        'acit_quantity_bayar' => str_replace(['.',','],['','.'],$qty_bayar),
        'acit_purchase_item_id' => $purchase_id_item,
        'acit_purchase_id' => $intPurchaseId,
        'acit_location' => $location,
		'cdate' => $strCDate));
}

public function updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete){
    $_CI =& get_instance();
    $_CI->load->model('Mpurchaseitem');
    $_CI->Mpurchaseitem->dbUpdate(array(
        'purchi_terterima' => 0),
        "id = $purchase_item_id_delete");
    // $this->initialize('purchase_item');
    // return $this->dbUpdate(array(
    //     'purchi_terterima' => 0),
    // "id = $purchase_item_id_delete");
}

public function updateDataTerima($purchase_id_item, $terterima_baru){
    $_CI =& get_instance();
    $_CI->load->model('Mpurchaseitem');
    $_CI->Mpurchaseitem->dbUpdate(array(
        'purchi_terterima' => $terterima_baru),
        "id = $purchase_id_item");
    // $this->initialize('purchase_item');
    // return $this->dbUpdate(array(
    //     'purchi_terterima' => $terterima_baru),
    // "id = $purchase_id_item");
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
            'acit_quantity1' => $intQuantity1,
            'acit_quantity2' => $intQuantity2,
            'acit_quantity3' => $intQuantity3,
            'acit_price' => $intPrice,
            'acit_discount1' => $intDiscount1,
            'acit_discount2' => $intDiscount2,
            'acit_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByAcceptanceID($intID) {
	return $this->dbDelete("acit_acceptance_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("acit_acceptance_id = $intID");
}

public function deleteByAcitID($intID) {
    return $this->dbDelete("id = $intID");
}

public function updateJumlahBarang($ai_id, $jmlBaru, $jmlBayar, $qty_pb){
    return $this->dbUpdate(array(
        'acit_quantity1' => $jmlBaru,
        'acit_quantity_bayar' => $jmlBayar,
        'acit_quantity_pb' => $qty_pb),
    "id = $ai_id");
}

public function getAllAccItem($acc_id){
    $this->setQuery(
        "SELECT id, acit_terpi
        FROM acceptance_item
        WHERE acit_acceptance_id = $acc_id");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getReportAcceptanceDetail($acceptanceID){
    $this->setQuery(
        "SELECT prod_title, acit_quantity_pb, acit_quantity1, acit_quantity_bayar, j1.unit_title as 'sat_terima', j2.unit_title as 'sat_pb', j3.unit_title as 'sat_bayar', pi.prci_price
        FROM acceptance_item ai
        LEFT JOIN jw_product jp ON ai.acit_product_id = jp.id
        LEFT JOIN jw_unit j1 ON j1.id = jp.satuan_terima_id
        LEFT JOIN jw_unit j2 ON j2.id = jp.satuan_pb_id
        LEFT JOIN jw_unit j3 ON j3.id = jp.satuan_bayar_id
        LEFT JOIN purchase_item pi ON pi.id = ai.acit_purchase_item_id
        WHERE acit_acceptance_id = $acceptanceID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount($id){
    $this->dbSelect('id', "acit_acceptance_id = $id");
	return $this->getNumRows();
}

public function updateTerPi($acit_id, $terpi_baru){
    return $this->dbUpdate(array(
        'acit_terpi' => $terpi_baru),
    "id = $acit_id");
}

public function recalculateTerterima($purchase_id_lol, $prodId, $purchase_id_item){
    $this->setQuery(
        "SELECT COALESCE(SUM(acit_quantity_pb),0) as jumlah, COALESCE(SUM(acit_quantity1),0) as jumlah2, op_item.prci_quantity_pb, op_item.prci_quantity_terima
        FROM acceptance_item as ai
        LEFT JOIN acceptance as a ON a.id = ai.acit_acceptance_id
        JOIN purchase_item as op_item ON acit_purchase_item_id = op_item.id
        WHERE acit_product_id = $prodId AND acit_purchase_item_id = $purchase_id_item AND a.acce_status >= 2");

    if($this->getNumRows() > 0) {
        $result = $this->getNextRecord('Array');
        if ($result['jumlah'] <= $result['prci_quantity_pb'] || $result['jumlah2'] <= $result['prci_quantity_terima']) return $result;
        else return false;        
    } else return false;
}


}

/* End of File */