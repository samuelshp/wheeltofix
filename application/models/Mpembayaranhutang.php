<?php

class Mpembayaranhutang extends JW_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->initialize('transaction_out');
    }

    public function getAllTransaction()
    {
        $this->setQuery(
            "SELECT txou.id, txou_code, txou_date, SUM(txod_amount) as txod_amount FROM transaction_out as txou 
            LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
            LEFT JOIN daftar_pembayaran_hutang as dph ON dph.id = 
                (SELECT id FROM daftar_pembayaran_hutang WHERE id IN 
                    (SELECT id FROM daftar_pembayaran_hutang_item as dphi WHERE dphi_pi_id IN
                        (SELECT pi.id FROM purchase_invoice as pi LEFT JOIN jw_supplier as supp ON pi.pinv_supplier_id = supp.id)
                    )
                )
            GROUP BY txou_code"
        );
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    
    public function getDetail($intSupplierID)
    {
        $this->setQuery("
            SELECT dph.id, dph.dphu_code, dph.dphu_amount FROM daftar_pembayaran_hutang AS dph
            LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
            LEFT JOIN purchase_invoice AS pi ON dph_item.dphi_reff_id = pi.id
            LEFT JOIN jw_supplier AS supp ON pi.pinv_supplier_id = supp.id
            WHERE pi.pinv_supplier_id = '$intSupplierID' AND dph.dphu_status = 0
            GROUP BY dphu_code
        ");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
    }

    public function getDPH($intSupplier)
    {
        # code...
    }

    // public function getDetailDPH($intPI)
    // {
    //     $this->setQuery("
    //         SELECT pi.cdate, pi.pinv_code, pi.pinv_grandtotal FROM daftar_pembayaran_hutang AS dph
    //         LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
    //         LEFT JOIN purchase_invoice AS pi ON dph_item.dphi_pi_id = pi.id
    //         WHERE pi.id = '$intPI'
    //     ");

    //     if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    //     else return false;
    // }
    
    
}


?>