<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Esubkontraknonmaterial extends Eloquent 
{
    protected $table = 'subkontrak_nonmaterial';
    public $timestamps = false;

    public function subkontrak()
    {
        return $this->belongsTo(Esubkontrak::class);
    }
}