<?php
/*
PUBLIC FUNCTION:

PRIVATE FUNCTION:
- __construct()
*/

class Mpemakaianitem extends JW_Model {

    // Constructor
    public function __construct() {
        parent::__construct();

        $this->initialize('pemakaian_item');
    }

    public function addItem($intID, $arrPostData){
	    for($i = 0; $i < count($arrPostData['productID']); $i++) {
	        $subkontrakMaterialID = $arrPostData['subkontrakMaterialID'][$i];
	        $prodID = $arrPostData['productID'][$i];
	        $qty = $arrPostData['qtyTotal'][$i];

	        if(!empty($prodID)){
	            $intInsert = $this->dbInsert(array(
	                'pemakaian_id' => $intID,
	                'subkontrak_material_id' => $subkontrakMaterialID,
	                'material' => $prodID,
	                'qty' => $qty,
	            ));
	        }
	    }

	    return $intInsert;
	}

	public function getItem($intID)
	{
		$this->setQuery("SELECT q.subkontrak_material_id, q.material, q.qty as qty_total, jp.prod_title, ju.unit_title, t.qty as jumlah_awal, SUM(ts.trst_quantity) as gudang_qty
		FROM pemakaian_item q 
		JOIN pemakaian as p ON p.id = q.pemakaian_id
		JOIN jw_product as jp ON jp.id = q.material
		JOIN jw_unit as ju ON ju.id = jp.prod_satuan_id
		JOIN subkontrak_material as t ON t.id = q.subkontrak_material_id
		JOIN transaction_stock ts ON ts.trst_product_id = q.material AND ts.trst_warehouse_id = p.warehouse_id 
		WHERE q.pemakaian_id = ".$intID);

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function updateItem($intID, $arrPostData)
	{
		$this->dbDelete("pemakaian_id = $intID");
		
		for($i = 0; $i < count($arrPostData['productID']); $i++) {
	        $subkontrakMaterialID = $arrPostData['subkontrakMaterialID'][$i];
	        $prodID = $arrPostData['productID'][$i];
	        $qty = $arrPostData['qtyTotal'][$i];

	        if(!empty($prodID)){
	            $intInsert = $this->dbInsert(array(
	                'pemakaian_id' => $intID,
	                'subkontrak_material_id' => $subkontrakMaterialID,
	                'material' => $prodID,
	                'qty' => $qty,
	            ));
	        }
	    }

	    return $intInsert;
	}
}
/* End of File */