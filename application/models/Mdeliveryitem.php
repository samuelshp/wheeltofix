<?php

/*
PUBLIC FUNCTION:
- getInvoiceIDByDeliveryItemID(intDeitID)
- getInvoiceIDByDeliveryID(intDelID)
- getAllDeliverydItemByPOID(intPOID)
- getItemsByDeliveryID(intDeliveryID,strMode)
- getLatestPrice(intProductID)
- add(intDeliveryID,invoiceID)
- deleteByDeliveryID(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdeliveryitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	
	$this->initialize('delivery_item');
}

public function getInvoiceIDByDeliveryItemID($intDeitID) {
    $this->setQuery(
"SELECT pi.id,deit_invoice_id
FROM delivery_item AS pi
WHERE pi.id = $intDeitID");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getInvoiceIDByDeliveryID($intDelID) {
    $this->setQuery(
"SELECT pi.id,deit_invoice_id,invo_discount
FROM delivery_item AS pi
LEFT JOIN invoice AS i ON i.id = deit_invoice_id
WHERE deit_delivery_id = $intDelID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsByDeliveryID($intDeliveryID,$strMode = 'ALL') {
	if($strMode == 'COUNT_PRICE')
        $this->dbSelect('deit_invoice_id',"deit_delivery_id = $intDeliveryID");
	else
        $this->setQuery(
"SELECT i.cdate AS invo_date, cust_name, cust_address, cust_city,deit_invoice_id,di.id,invo_status,invo_discount,i.id AS invo_id, invo_grandtotal,invo_code,invo_payment
FROM delivery_item AS di
LEFT JOIN invoice AS i ON deit_invoice_id = i.id
LEFT JOIN jw_customer AS c ON c.id = i.invo_customer_id
WHERE deit_delivery_id = $intDeliveryID
ORDER BY di.id ASC");
        //$this->dbSelect('',"deit_delivery_id = $intDeliveryID");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intDeliveryID,$invoiceID) {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM delivery WHERE id = $intDeliveryID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';
	
	return $this->dbInsert(array(
		'deit_delivery_id' => $intDeliveryID,
		'deit_invoice_id' => $invoiceID,
		'cdate' => $strCDate));
}


public function deleteByDeliveryID($intID) {
	return $this->dbDelete("deit_delivery_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function deleteByInvoiceID($intID) {
    return $this->dbDelete("deit_invoice_id = $intID");
}

}

/* End of File */