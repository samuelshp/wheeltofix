<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intPayment)

PRIVATE FUNCTION:
- __construct()	
*/

class Maccountadjustment extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();
    $this->initialize('accountadjustment');
}

public function add($strDescription,$strDate,$intStatus) {
    return $this->dbInsert(array(
        'acad_description' => $strDescription,
        'acad_status' => $intStatus,
        'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d')
    ));
}

public function getAllPerkiraan() {
    $this->setQuery(
'SELECT a.id,acco_code,acco_name
FROM jw_account AS a
ORDER BY a.acco_code, a.acco_name ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllAdjustment($strKeyword = '') {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " WHERE acad_code LIKE '%$strKeyword%'";
    } else $strWhere = '';

    $this->setQuery(
'SELECT a.id, acad_code, acad_description, acad_status
FROM accountadjustment AS a
'.$strWhere.'
ORDER BY a.acad_code DESC,a.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount() {
    $this->dbSelect('id');

    return $this->getNumRows();
}

public function editByID($intID,$intStatus='') {
    return $this->dbUpdate(array(
            'acad_status' => $intStatus),
        "id = $intID");
}

public function editByID2($intID,$strDescription,$intStatus) {
    return $this->dbUpdate(array(
            'acad_description' => $strDescription,
            'acad_status' => $intStatus),
        "id = $intID");
}

public function getStatusAdjustmentByID($intID) {
    $this->setQuery(
'SELECT acad_status
FROM accountadjustment AS p
WHERE p.id = '.$intID.'');
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataHeaderByID($intID) {

    $this->setQuery(
'SELECT rh.id, pahe_code,rh.cdate AS pahe_date
FROM payment_header AS rh
WHERE rh.id = '.$intID.'
ORDER BY rh.id DESC');

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataByHeaderID($intID) {

    $this->setQuery(
'SELECT p.id,pahe_code,cust_name,sals_name,paym_status,cust_address,cust_city, invo_payment, invo_jatuhtempo,sals_address,sals_phone,cust_phone,paym_date,paym_type,paym_description,invo_grandtotal,paym_bank_id,paym_bg_number,invo_code,
invo_jualkanvas
FROM payment AS p
LEFT JOIN invoice AS i ON paym_invoice_id = i.id
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON paym_salesman_id = s.id
LEFT JOIN payment_header AS pa ON paym_header_id = pa.id
WHERE paym_header_id = '.$intID.'
ORDER BY p.id ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function autoExpired($intDay=1){
    $this->setQuery(
"UPDATE accountadjustment SET acad_status = 3 WHERE DATE_ADD(cdate,INTERVAL {$intDay} DAY) <= CURRENT_TIMESTAMP AND acad_status = 2");
    return true;
}

}

/* End of File */