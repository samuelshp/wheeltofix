<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intRepayment)

PRIVATE FUNCTION:
- __construct()	
*/

class Mpaymentheader extends JW_Model {

// Constructor
    public function __construct() {
        parent::__construct();
        $this->initialize('payment_header');
    }

    public function add($strDate) {
        return $this->dbInsert(array(
            'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d')
        ));
    }

}

/* End of File */