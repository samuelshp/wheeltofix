<?php
/*
PUBLIC FUNCTION:
- getAllPurchasedItemByPOID(intPOID)
- getAllPurchasedBonusItemByPOID(intPOID)
- getItemsByPurchaseID(intPurchaseID,strMode)
- getBonusItemsByPurchaseID(intPurchaseID,strMode)
- getLatestPrice(intProductID)
- add(intPurchaseID,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3,unit1Item,unit2Item,unit3Item,intPrice,intDiscount1,intDiscount2,intDiscount3,flagBonus,flagExist)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- deleteByPurchaseID(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mpurchaseitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('purchase_item');
}

public function UpdateAcceptedItem($intPOID,$intProductID,$intPurchased1,$intPurchased2,$intPurchased3,$intBonus) {
    $this->setQuery(
"        UPDATE purchase_item
        SET prci_arrived1 = prci_arrived1 + $intPurchased1,
         prci_arrived2 = prci_arrived2 + $intPurchased2,
         prci_arrived3 = prci_arrived3 + $intPurchased3
         WHERE
         prci_purchase_id= $intPOID AND
         prci_product_id =$intProductID AND
         prci_bonus = $intBonus"
    );
    return true;

}

public function updatePurchaseItemAfterDelAcc($prci_update, $purch_item_id){
    // $this->setQuery(
    //     "UPDATE purchase_item
    //         SET prci_quantity1 = $prci_update, prci_arrived1 = 0
    //     WHERE prci_purchase_id = $purch_id AND prci_product_id = $prod_id"
    // );
    // return true;

    //$prci_update = $prci_quantity1+$prci_arrived1;
    return $this->dbUpdate(array(
        'purchi_terterima' => $prci_update),
    "id = $purch_item_id");
}

public function getIdOfPurchaseItem($prod_id, $purchase_id){
    $this->setQuery(
    "SELECT id
    FROM purchase_item
    WHERE prci_product_id = $prod_id AND prci_purchase_id = $purchase_id");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function UpdateArrivedItemView($ID,$idItem,$intDif1,$intDif2,$intDif3,$intBonus) {

    $this->setQuery(
"    UPDATE purchase_item
    SET prci_arrived1 = prci_arrived1 + $intDif1,
     prci_arrived2 = prci_arrived2 + $intDif2,
     prci_arrived3 = prci_arrived3 + $intDif3
     WHERE
     prci_purchase_id= '$ID' AND prci_product_id= '$idItem' AND prci_bonus =$intBonus"
    );
    return true;

}

public function getAllPurchaseItem($intStartNo = -1,$intPerPage = -1, $previledge = 0){

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "pi.cdate DESC";
    else $strOrderBy = "pi.cdate DESC, pi.id DESC LIMIT $intStartNo, $intPerPage";
    
    if($previledge == 0){

        $this->setQuery(
            "SELECT pi.id, pu.id as purchase_id, poi.proi_description, js.supp_name, pu.cdate, k.kont_name, pu.prch_code, jc.cust_name, pi.prci_description, po.idKontrak, k.kont_name, sk.job as kont_job, pi.prci_quantity_pb, jp.prod_title, ju_bayar.unit_title as title_bayar, ju_pb.unit_title as title_pb, ju_terima.unit_title as title_terima, pi.prci_quantity_pb - pi.purchi_terterima as qty_tampil, pi.purchi_terterima
            FROM purchase_item as pi
            LEFT JOIN purchase as pu ON pu.id = pi.prci_purchase_id
            LEFT JOIN purchase_order as po ON po.id = prch_po_id
            LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
            LEFT JOIN kontrak as k ON k.id = po.idKontrak
            LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
            LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
            LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
            LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
            LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
            LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
            LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
            WHERE pu.prch_status IN (".STATUS_WAITING_FOR_FINISHING.")
            GROUP BY pi.id
            ORDER BY $strOrderBy"
        );//nanti tambah ambil satu field lagi untuk conversi unitnya
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }
    else{
        $this->setQuery(
            "SELECT pi.id, pu.id as purchase_id, poi.proi_description, js.supp_name, pu.cdate, k.kont_name, pu.prch_code, jc.cust_name, pi.prci_description, po.idKontrak, k.kont_name, sk.job as kont_job, pi.prci_quantity_pb, jp.prod_title, ju_bayar.unit_title as title_bayar, ju_pb.unit_title as title_pb, ju_terima.unit_title as title_terima, pi.prci_quantity1 - pi.purchi_terterima as qty_tampil, pi.purchi_terterima
            FROM purchase_item as pi
            LEFT JOIN purchase as pu ON pu.id = pi.prci_purchase_id
            LEFT JOIN purchase_order as po ON po.id = prch_po_id
            LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
            LEFT JOIN kontrak as k ON k.id = po.idKontrak
            LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
            LEFT JOIN subkontrak_team as skt ON skt.subkontrak_id = sk.id
            LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
            LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
            LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
            LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
            LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
            LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
            WHERE pi.statusClosed <> 1 AND pu.prch_status IN (".STATUS_WAITING_FOR_FINISHING.") AND skt.name = $previledge
            GROUP BY pi.id
            ORDER BY $strOrderBy"
        );//nanti tambah ambil satu field lagi untuk conversi unitnya
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }
}

public function getCount(){
    $this->dbSelect('id');
	return $this->getNumRows();
}

public function getAllPurchaseItemById($idPurchase){
    $this->setQuery(
        "SELECT pi.id, pu.id as purchase_id, po.idSubKontrak, poi.proi_description, js.supp_name, pu.cdate, k.kont_name, pu.prch_code, jc.cust_name, pi.prci_description, po.idKontrak, k.kont_name, sk.job as kont_job, pi.prci_quantity1, pi.prci_quantity_pb, jp.prod_title, ju_bayar.unit_title as title_bayar, ju_pb.unit_title as title_pb, ju_terima.unit_title as title_terima, pi.prci_quantity1 - pi.purchi_terterima as qty_tampil, pi.purchi_terterima, pi.prci_product_id, prci_price
        FROM purchase_item as pi
        LEFT JOIN purchase as pu ON pu.id = pi.prci_purchase_id
        LEFT JOIN purchase_order as po ON po.id = prch_po_id
        LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
        LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
        LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
        LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
        LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
        LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
        LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
        WHERE pi.statusClosed <> 1 AND pu.prch_status IN (".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") AND pi.id = $idPurchase
        GROUP BY pi.id"
    );//nanti tambah ambil satu field lagi untuk conversi unitnya
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchaseItemById2($idPurchase){
    $this->setQuery(
        "SELECT pi.id, pu.id as purchase_id, po.idSubKontrak, poi.proi_description, js.supp_name, pu.cdate, k.kont_name, pu.prch_code, jc.cust_name, pi.prci_description, po.idKontrak, k.kont_name, sk.job as kont_job, pi.prci_quantity1, pi.prci_quantity_pb, jp.prod_title, ju_bayar.unit_title as title_bayar, ju_pb.unit_title as title_pb, ju_terima.unit_title as title_terima, pi.prci_quantity1 - pi.purchi_terterima as qty_tampil, pi.purchi_terterima, pi.prci_product_id, pi.purchi_terterima_bpb, pi.prci_quantity_terima
        FROM purchase_item as pi
        LEFT JOIN purchase as pu ON pu.id = pi.prci_purchase_id
        LEFT JOIN purchase_order as po ON po.id = prch_po_id
        LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
        LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
        LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
        LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
        LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
        LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
        LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
        WHERE pi.statusClosed <> 1 AND pu.prch_status IN (".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") AND pu.id = $idPurchase
        GROUP BY pi.id"
    );//nanti tambah ambil satu field lagi untuk conversi unitnya
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchaseItemBySupplier($nama_supp){
    $strOrderBy = "pi.cdate DESC, pi.id DESC";
    $this->setQuery(
        "SELECT pi.id, pu.id as purchase_id, js.supp_name, pu.cdate, k.kont_name, pu.prch_code, jc.cust_name, pi.prci_description, po.idKontrak, k.kont_name, sk.job as kont_job, pi.prci_quantity_pb, jp.prod_title, ju_bayar.unit_title as title_bayar, ju_pb.unit_title as title_pb, ju_terima.unit_title as title_terima, pi.prci_quantity_pb - pi.purchi_terterima as qty_tampil, pi.purchi_terterima
        FROM purchase_item as pi
        LEFT JOIN purchase as pu ON pu.id = pi.prci_purchase_id
        LEFT JOIN purchase_order as po ON po.id = prch_po_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
        LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
        LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
        LEFT JOIN jw_unit as ju_bayar ON ju_bayar.id = jp.satuan_bayar_id
        LEFT JOIN jw_unit as ju_pb ON ju_pb.id = jp.satuan_pb_id
        LEFT JOIN jw_unit as ju_terima ON ju_terima.id = jp.satuan_terima_id
        LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
        WHERE pi.statusClosed <> 1 AND pu.prch_status = ".STATUS_WAITING_FOR_FINISHING." AND js.supp_name LIKE '%$nama_supp%'
        GROUP BY pi.id
        ORDER BY $strOrderBy"
    );//nanti tambah ambil satu field lagi untuk conversi unitnya
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchasedItemByPOID($intPOID) {
    $this->setQuery(
"SELECT  prci_product_id, prci_quantity1, prci_quantity2, prci_quantity3
FROM purchase_item AS pi
LEFT JOIN purchase AS p ON pi.prci_purchase_id = p.id
WHERE p.prch_po_id = $intPOID AND p.prch_status ='".STATUS_APPROVED."' AND pi.prci_bonus='0'
ORDER BY pi.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchasedBonusItemByPOID($intPOID) {
    $this->setQuery(
"SELECT  prci_product_id, prci_quantity1, prci_quantity2, prci_quantity3
FROM purchase_item AS pi
LEFT JOIN purchase AS p ON pi.prci_purchase_id = p.id
WHERE p.prch_po_id = $intPOID AND p.prch_status ='".STATUS_APPROVED."' AND pi.prci_bonus='1'
ORDER BY pi.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
    $this->setQuery(
"SELECT p.id,pro.id as product_id, prci_proi_id, prci_quantity1, prci_quantity2, prci_quantity3, prci_arrived1, prci_arrived2, prci_arrived3, prci_price, prci_discount1, prci_discount2, prci_discount3, prci_product_id, prob_title, proc_title, prod_code,prod_title,prci_description,prci_unit1,prci_unit2,prci_unit3,proc_title,prci_exist,prod_code,prci_subtotal, prod_conv1, prod_conv2, prod_conv3, prci_quantity_pb, prci_product_id
FROM jw_product AS pro
LEFT JOIN purchase_item AS p ON pro.id = p.prci_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE prci_purchase_id =  $intPurchaseID AND prci_bonus = '0' AND p.statusClosed != 1
ORDER BY p.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}


public function GetItemsByPurchaseID2($intPurchaseID) {
    $this->setQuery(
    "SELECT pi.id, prci_proi_id, pi.cdate, poi.proi_description, jp.prod_title, ju.unit_title as 'sat_pb', ju2.unit_title as 'sat_bayar', pi.prci_quantity1, pi.prci_product_id, pi.prci_quantity_pb, pi.prci_price
    FROM purchase_item AS pi 
    LEFT JOIn purchase as p ON p.id = pi.prci_purchase_id
    LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id
    LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id 
    LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id
    LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_bayar_id
    WHERE pi.prci_purchase_id = $intPurchaseID
    GROUP BY pi.id
    ORDER BY jp.prod_title"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getBonusItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
    $this->setQuery(
"SELECT p.id,pro.id as product_id, prci_quantity1, prci_quantity2, prci_quantity3, prci_arrived1, prci_arrived2, prci_arrived3, prci_product_id, prob_title, proc_title, prod_title,prci_description,prci_unit1,prci_unit2,prci_unit3,proc_title,prci_exist,prod_code, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS pro
LEFT JOIN purchase_item AS p ON pro.id = p.prci_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE prci_purchase_id =  $intPurchaseID AND prci_bonus = '1'
ORDER BY p.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getLatestPrice($intProductID) {
    $this->setQuery("SELECT prod_currency FROM jw_product WHERE id = $intProductID");
    
    if($this->getNumRows() > 0) $intCurrencyID = $this->getNextRecord('Object')->prod_currency;
    else return false;

    $this->dbSelect('prci_price',"prci_product_id = $intProductID",'cdate DESC',1);
    
    if($this->getNumRows() > 0) return formatPriceCurrency($this->getNextRecord('Object')->prci_price,'BASE',$intCurrencyID);
    else return false;
}

public function recalculateTotalByPOIID($intPOIID, $prodId) {
	$this->setQuery("SELECT SUM(prci_quantity_pb) as total, SUM(prci_quantity_terima) as total2 FROM purchase p, purchase_item pi WHERE pi.prci_purchase_id = p.id AND prch_status >= 2 AND prci_proi_id = $intPOIID AND prci_product_id = $prodId");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}


public function add($intPurchaseID,$intProductID,$intProiID,$intQuantityPB,$intQuantity1,$intQuantity2,$intQuantity3,$unit1Item,$unit2Item,$unit3Item,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3,$flagBonus,$flagExist,$accoID,$qtyTerima) {
    // Tambahan untuk membuat cdate item sama dengan cdate header
    // Tujuannya supaya di transaction history juga cdatenya sama
    $this->setQuery("SELECT cdate FROM purchase WHERE id = $intPurchaseID");
    if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
    else $strCDate = '';

    $intInsertID = $this->dbInsert(array(
        'prci_purchase_id' => $intPurchaseID,
        'prci_product_id' => $intProductID,
        'prci_proi_id' => $intProiID,
        'prci_quantity_pb' => $intQuantityPB,
        'prci_quantity_terima' => $qtyTerima,
        'prci_quantity1' => $intQuantity1,
        'prci_quantity2' => $intQuantity2,
        'prci_quantity3' => $intQuantity3,
        'prci_unit1' => $unit1Item,
        'prci_unit2' => $unit2Item,
        'prci_unit3' => $unit3Item,
        'prci_price' => $intPrice,
        'prci_discount1' => $intDiscount1,
        'prci_discount2' => $intDiscount2,
        'prci_discount3' => $intDiscount3,
        'prci_exist' => $flagExist,
        'prci_bonus' => $flagBonus,
        'prci_acco_id' => $accoID,
        'cdate' => $strCDate));

    $_CI =& get_instance();
    $_CI->load->model('Mproduct');
    $arrProduct = $_CI->Mproduct->getItemByID($intProductID);
    if(!empty($arrProduct['prod_profit_margin'])) {
        $intNewPrice = $intPrice * ($arrProduct['prod_profit_margin'] + 100) / 100;
        $_CI->Mproduct->updatePriceByID($intNewPrice, $intProductID);
    }

    return $intInsertID;
}

public function update($intPurchaseItemID,$intQuantityPB,$intQuantity1,$intPrice) {

    $this->dbUpdate(array(
        'prci_quantity_pb' => $intQuantityPB,
        'prci_quantity1' => $intQuantity1,
        'prci_price' => $intPrice),
        "id=$intPurchaseItemID");

}

public function updateJumlahBarang($jumlah_baru, $jumlah_penerimaan, $prodId, $purchase_item_id){
    
    return $this->dbUpdate(array(
        'purchi_terterima' => $jumlah_penerimaan),
    "id = $purchase_item_id");
}

public function UpdateTerterima($intPurchaseItemID, $qty_baru, $qty_baru2){
    $this->dbSelect('prci_purchase_id, prci_quantity_pb, prci_quantity_terima', "id = $intPurchaseItemID");
    $intPurchaseID = $this->getNextRecord('Object')->prci_purchase_id;

    $intUpdated = $this->dbUpdate(array(
        'purchi_terterima' => $qty_baru,
        'purchi_terterima_bpb' => $qty_baru2),
    "id = $intPurchaseItemID");

    $this->checkAutoClose($intPurchaseID);
    return $intUpdated;
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
            'prci_quantity1' => $intQuantity1,
            'prci_quantity2' => $intQuantity2,
            'prci_quantity3' => $intQuantity3,
            'prci_price' => $intPrice,
            'prci_discount1' => $intDiscount1,
            'prci_discount2' => $intDiscount2,
            'prci_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByPurchaseID($intID) {
	return $this->dbDelete("prci_purchase_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function checkAutoClose($intPurchaseID) {
    $arrData = $this->getAllPurchaseItemById2($intPurchaseID);
    $bolClosed = TRUE; $bolSearched = FALSE;
    foreach($arrData as $e) {
        $bolSearched = TRUE;
        if(floatval($e['prci_quantity_pb']) > floatval($e['purchi_terterima']) || floatval($e['prci_quantity_terima']) > floatval($e['purchi_terterima_bpb'])) {
            $bolClosed = FALSE; break;
        }
    }

    if ($bolClosed === FALSE && $bolSearched) {
        $_CI =& get_instance();
        $_CI->load->model('Mpurchase');
        $_CI->Mpurchase->dbUpdate(array(
            'prch_status' => STATUS_WAITING_FOR_FINISHING),
            "id = $intPurchaseID");
    }

    if($bolClosed && $bolSearched) {
        $_CI =& get_instance();
        $_CI->load->model('Mpurchase');
        $_CI->Mpurchase->dbUpdate(array(
            'prch_status' => STATUS_FINISHED),
            "id = $intPurchaseID");        
    }   

    return $bolClosed;
}

public function getTerterima($purch_item_id, $product_id){
    $this->dbSelect('purchi_terterima','id = '.$purch_item_id.' AND prci_product_id = '.$product_id.' ');
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

}

/* End of File */