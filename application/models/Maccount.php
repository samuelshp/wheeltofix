<?php
/*
PUBLIC FUNCTION:
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Maccount extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_account');
}

public function getItemByID($intID) {
	$this->setQuery('SELECT * FROM jw_account WHERE id = '.$intID);
	if($this->getNumRows() > 0)
		return $this->getNextRecord('Array');
	else
		return false;
}

public function getAllAccount($strKeyword = '', $strWhere2 = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (acco_name LIKE '%$strKeyword%' OR acco_code LIKE '%$strKeyword%')";
	} elseif(!empty($strWhere2)) {
		$strWhere = $strWhere2;
	} else $strWhere = '';

	$this->setQuery("SELECT * FROM jw_account WHERE acco_status > 1 $strWhere ORDER BY acco_code ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByCode($strCode) {
	$this->setQuery("SELECT * FROM jw_account WHERE acco_status > 1 AND acco_code = '$strCode'");

	if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
	else return false;
}

public function getAllDetailAccount($strKeyword = '', $strWhere2 = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (acco_name LIKE '%$strKeyword%' OR acco_code LIKE '%$strKeyword%')";
	} elseif(!empty($strWhere2)) {
		$strWhere = $strWhere2;
	} else $strWhere = '';

	$this->setQuery("SELECT * FROM jw_account WHERE acco_status > 1 and acco_detail = 2 $strWhere ORDER BY acco_code ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

}

/* End of File */