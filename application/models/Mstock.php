<?php
/*
PUBLIC FUNCTION:
- getDynamicCode(strTableName,intID)
- getQuantityByProductID(intProductID,intWarehouseID)
- getHistoryByProductID(intProductID,intWarehouseID)
- calibrate(intStartNo,intPerPage)

PRIVATE FUNCTION:
- 

*/

class Mstock extends JW_Model {

// Constructor
public function __construct() { 
    parent::__construct(); 
}

public function getDynamicCode($strTableName,$intID) {
    if($strTableName == 'purchase') $strTableName = 'acceptance'; // This is needed because stock counted in acceptance but called 'purchase' in transaction_history type
    $strFieldName = ''; $strItemFieldName = '';
    if($strTableName == 'purchase_order') {
        $strFieldName = 'pror_code';
        $strItemFieldName = 'proi_purchase_order_id';
    } else if($strTableName == 'acceptance') {
        $strFieldName = 'acce_code';
        $strItemFieldName = 'acit_acceptance_id';
    } else if($strTableName == 'adjustment' || $strTableName == 'mutasi_in' || $strTableName == 'mutasi_out' || $strTableName == 'opname') {
        $strFieldName = 'ajst_code';
        $strTableName = 'adjustment';
        $strItemFieldName = 'adit_adjustment_id';
    } else if($strTableName == 'delivery') {
        $strFieldName = 'deli_code';
        $strItemFieldName = 'deit_delivery_id';
    } else if($strTableName == 'delivery_back') {
        $strFieldName = 'deba_code';
        $strItemFieldName = 'dbit_deba_id';
    } else if($strTableName == 'invoice') {
        $strFieldName = 'invo_code';
        $strItemFieldName = 'invi_invoice_id';
    } else if($strTableName == 'invoice_retur') {
        $strFieldName = 'invr_code';
        $strItemFieldName = 'inri_invoice_retur_id';
    } else if($strTableName == 'kanvas' || $strTableName == 'kanvas_muat' || $strTableName == 'kanvas_turun') {
        $strFieldName = 'kanv_code';
        $strTableName = 'kanvas';
        $strItemFieldName = 'kavi_kanvas_id';
    } else if($strTableName == 'purchase') {
        $strFieldName = 'prch_code';
        $strItemFieldName = 'prci_purchase_id';
    } else if($strTableName == 'purchase_retur') {
        $strFieldName = 'prcr_code';
        $strItemFieldName = 'prri_purchase_retur_id';
    }

    if(!(empty($strFieldName))){
        $this->setQuery(
'            SELECT t.id AS header_id,'.$strFieldName.' AS code
            FROM '.$strTableName.' AS t
            INNER JOIN '.$strTableName.'_item AS ti ON t.id = ti.'.$strItemFieldName.'
            WHERE ti.id = '.$intID.'
            ');
        
        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    } 

    return false;
}

public function getQuantityByProductID($intProductID,$intWarehouseID = 0) {
    if(empty($intProductID)) return 0;
    if(empty($intWarehouseID)) $intWarehouseID = 1;
    
    /*$this->setQuery(
'SELECT SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS totalqty
FROM transaction_history
WHERE trhi_type in ("purchase","purchase_retur","invoice","invoice_retur","adjustment","opname","mutasi_in","mutasi_out","kanvas_muat","kanvas_turun")
    AND trhi_product_id = '.$intProductID.'
    AND trhi_warehouse_id = '.$intWarehouseID.'
    AND trhi_status IN (2,3)');
    
    $intTotalQty = $this->getNextRecord('Object')->totalqty;*/
    $this->setQuery("SELECT trst_quantity AS totalqty FROM transaction_stock WHERE trst_product_id = $intProductID AND trst_warehouse_id = $intWarehouseID");
    if($this->getNumRows() > 0) $intTotalQty = $this->getNextRecord('Object')->totalqty;
    else $intTotalQty = 0;

    if(!empty($intTotalQty)) return $intTotalQty;
    else return 0;
}
    
public function getHistoryByProductID($intProductID,$intWarehouseID = 0) {
    if(empty($intWarehouseID)) $intWarehouseID = 1;
    
    $this->setQuery(
'SELECT *
FROM transaction_history
WHERE trhi_type in ("purchase","purchase_retur","invoice","invoice_retur","adjustment","opname","mutasi_in","mutasi_out","kanvas_muat","kanvas_turun")
    AND trhi_product_id = '.$intProductID.'
    AND trhi_warehouse_id = '.$intWarehouseID.'
    AND trhi_status IN (2,3)
ORDER BY trhi_date DESC');
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function calibrate($intStartNo,$intPerPage) {
    $this->setQuery("SELECT id FROM jw_product ORDER BY id ASC LIMIT $intStartNo,$intPerPage");
    $arrProduct = $this->getQueryResult('Array');
    $this->setQuery("SELECT id FROM jw_warehouse ORDER BY id ASC");
    $arrWarehouse = $this->getQueryResult('Array');

    foreach($arrProduct as $e) foreach($arrWarehouse as $e2)
        $this->setQuery("CALL func_count_stock(".$e['id'].",".$e2['id'].",NULL)");

}

}

/* End of File */