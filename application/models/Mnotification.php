<?php

class Mnotification extends JW_Model
{	
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->initialize('notification');
	}

	public function add($arrPostData)
	{						
		$this->load->model('Mdbvo', 'Madmlogin');
		$this->Madmlogin->initialize('adm_login');
		$this->Madmlogin->dbSelect('', "id = '".$arrPostData['noti_user_id']."' OR adlg_login = '".$arrPostData['noti_user_id']."'");
		$arrMember = $this->Madmlogin->getNextRecord('Array');
			
		$dbInsert = $this->dbInsert(
			array(
				'noti_user_id' => $arrPostData['noti_user_id'],
				'noti_title' => $arrPostData['noti_title'],
				'noti_content' => isset($arrPostData['noti_content']) ? $arrPostData['noti_content'] : "-",
				'noti_status' => isset($arrPostData['noti_status']) ? $arrPostData['noti_status'] : 2
			)
		);

		if(!empty($arrMember['adlg_onesignal_id']) && $dbInsert) {			
			// $return = $this->email->sendPushNotification($arrPostData['noti_title'], $arrPostData['noti_content'], $arrMember['adlg_onesignal_id']);	
		}

		return $dbInsert;		
	}
}