<?php
/*
PUBLIC FUNCTION:
- getItemsByInvoiceID(intInvoiceID,strMode)
- getAllLastSaleItems(strKeyword)
- add(intInvoiceReturID,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3, intUnit1,intUnit2,intUnit3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- deleteByInvoiceID(intID)

PRIVATE FUNCTION:
- __construct()

 *********************************************/

class Minvoicereturitem extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('invoice_retur_item');
}

public function getItemsByInvoiceReturID($intInvoiceReturID,$strMode = 'ALL') {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,inri_quantity,inri_price,inri_discount',"inri_invoice_retur_id = $intInvoiceReturID");
    else $this->dbSelect('',"inri_invoice_retur_id = $intInvoiceReturID");*/

    $this->setQuery(
"SELECT p.id, inri_quantity1, inri_quantity2, inri_quantity3, inri_price, inri_discount1, inri_discount2, inri_discount3, inri_product_id, prob_title, proc_title, prod_code,prod_title,inri_description,inri_unit1,inri_unit2,inri_unit3,prod_hpp
FROM jw_product AS pro
LEFT JOIN invoice_retur_item AS p ON pro.id = p.inri_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE inri_invoice_retur_id =  $intInvoiceReturID AND inri_bonus ='0'
ORDER BY p.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function getItemsBonusByInvoiceReturID($intInvoiceReturID,$strMode = 'ALL') {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,inri_quantity,inri_price,inri_discount',"inri_invoice_retur_id = $intInvoiceReturID");
    else $this->dbSelect('',"inri_invoice_retur_id = $intInvoiceReturID");*/

    $this->setQuery(
"SELECT p.id, inri_quantity1, inri_quantity2, inri_quantity3, inri_product_id, prob_title, proc_title, prod_code,prod_title,inri_description,inri_unit1,inri_unit2,inri_unit3
FROM jw_product AS pro
LEFT JOIN invoice_retur_item AS p ON pro.id = p.inri_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE inri_invoice_retur_id =  $intInvoiceReturID AND inri_bonus ='1'
ORDER BY p.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function getAllLastSaleItems($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (prod_title LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%' OR proc_title LIKE '%$strKeyword%')";
	} else $strWhere = '';

	$this->setQuery(
'SELECT p.id, proc_title, prob_title, prod_title, prod_price, prod_currency, invi_price, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN (
SELECT invi_price,MAX(cdate),invi_product_id
FROM invoice_item
GROUP BY invi_product_id
) AS pi ON invi_product_id = p.id
WHERE prod_status > 1'.$strWhere.'
ORDER BY proc_title ASC, prod_title ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intInvoiceReturID,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice='0',$intDiscount1='0',$intDiscount2='0',$intDiscount3='0',$intBonus='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM invoice_retur WHERE id = $intInvoiceReturID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'inri_invoice_retur_id' => $intInvoiceReturID,
		'inri_product_id' => $intProductID,
		'inri_description' => $strProductDescription,
		'inri_quantity1' => $intQuantity1,
		'inri_quantity2' => $intQuantity2,
		'inri_quantity3' => $intQuantity3,
		'inri_unit1' => $intUnit1,
		'inri_unit2' => $intUnit2,
		'inri_unit3' => $intUnit3,
		'inri_price' => $intPrice,
		'inri_discount1' => $intDiscount1,
		'inri_discount2' => $intDiscount2,
		'inri_discount3' => $intDiscount3,
        'inri_bonus' => $intBonus,
		'cdate' => $strCDate));
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
			'inri_quantity1' => $intQuantity1,
			'inri_quantity2' => $intQuantity2,
			'inri_quantity3' => $intQuantity3,
			'inri_price' => $intPrice,
			'inri_discount1' => $intDiscount1,
			'inri_discount2' => $intDiscount2,
			'inri_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByInvoiceID($intID) {
	return $this->dbDelete("invi_invoice_retur_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

}

/* End of File */