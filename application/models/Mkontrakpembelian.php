<?php
/*
PUBLIC FUNCTION:
- getAllCustomer(strKeyword)
- getAllCustomerWithSupplierBind(strKeyword,intSupplier)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mkontrakpembelian extends JW_Model {

    // Constructor
    public function __construct() { 
    	parent::__construct();
    	
    	$this->initialize('kontrak_pembelian');
    }

    public function getAllKontrakPembelian($strKeyword = '', $strOrder = '') {
        if(!empty($strKeyword)) {
            $strKeyword = urldecode($strKeyword);
            $strWhere = "WHERE kopb_code LIKE '%$strKeyword%' OR prod_title LIKE '%$strKeyword%' OR supp_name LIKE '%$strKeyword%'";
        } else $strWhere = '';
        if(empty($strOrder)) $strOrder = 'kopb_date DESC';

        $this->setQuery(
        "SELECT kp.id, kopb_code, kopb_date, kopb_supplier_id, kopb_product_id, kopb_amount, kopb_qty, kopb_po, kopb_description, prod_title, supp_name, kont_name
        FROM kontrak_pembelian AS kp
        LEFT JOIN kontrak AS k on k.id = kopb_proyek_id
        LEFT JOIN jw_product AS prod on prod.id = kopb_product_id
        LEFT JOIN jw_supplier AS supp on supp.id = kopb_supplier_id
        $strWhere
        ORDER BY $strOrder");


        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getKontrakPembelianByID($intID)
    {
        $this->dbSelect('',"id = $intID");
        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;
    }

    public function getDataAutoComplete($intSupplierID) {
        $this->setQuery(
        "SELECT kp.id, kopb_code, kopb_date, jp.prod_title, kopb_supplier_id, kopb_product_id, kopb_amount, kopb_description, supp_name
        FROM kontrak_pembelian AS kp
        LEFT JOIN jw_supplier AS supp on supp.id = kopb_supplier_id
        LEFT JOIN jw_product AS jp on jp.id = kp.kopb_product_id
        LEFT JOIN purchase_order_item AS proi on proi_product_id = jp.id
        WHERE kopb_qty > kopb_po AND supp.id = $intSupplierID
        ORDER BY kopb_code DESC");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getDataBySupplierID($intSupplierID) {
        $this->setQuery(
        "SELECT id, kopb_code, kopb_date, kopb_supplier_id, kopb_amount, kopb_description
        FROM kontrak_pembelian 
        WHERE kopb_supplier_id = $intSupplierID
        ORDER BY kopb_code DESC");


        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getLastKPCode($code) {
    	$this->setQuery(
        "SELECT SUBSTRING(kopb_code,10,4) as code
        FROM kontrak_pembelian
        WHERE kopb_code like '$code%'
        ORDER BY kopb_code DESC");

        if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
        else return false;
    }

    public function add($strKPCode, $intProyekID, $strDate, $intSupplierID, $intProductID, $doubleQty, $intPrice, $doubleAmount, $doublePO, $strDescription) {
        return $this->dbInsert(array(
            'kopb_code' => $strKPCode,
            'kopb_proyek_id' => $intProyekID,
            'kopb_date' => $strDate,
            'kopb_supplier_id' => $intSupplierID,
            'kopb_product_id' => $intProductID,
            'kopb_qty' => $doubleQty,
            'kopb_price' => $intPrice,
            'kopb_amount' => $doubleAmount,
            'kopb_po' => $doublePO,
            'kopb_description' => $strDescription,
            'cdate' => date('Y-m-d H:i')
        ));
    }

    public function update($intID, $strDate, $intProyekID, $intSupplierID, $intProductID, $doubleQty, $intPrice, $doubleAmount, $doublePO, $strDescription)
    {               
        return $this->dbUpdate(array(
            'kopb_date' => $strDate,
            'kopb_proyek_id' => $intProyekID,
            'kopb_supplier_id' => $intSupplierID,
            'kopb_product_id' => $intProductID,
            'kopb_qty' => $doubleQty,
            'kopb_price' => $intPrice,
            'kopb_amount' => $doubleAmount,
            'kopb_po' => $doublePO,
            'kopb_description' => $strDescription,
        ),"id = $intID");
    }

    public function updatePO($intID,$qty) {
        $this->setQuery("UPDATE kontrak_pembelian SET
            kopb_po = kopb_po + $qty
            WHERE id = $intID");
    }

    public function updateMinusPO($intID,$qty) {
        $this->setQuery("UPDATE kontrak_pembelian SET
            kopb_po = kopb_po - $qty
            WHERE id = $intID");
    }

}

/* End of File */