<?php
/*
PUBLIC FUNCTION:
- GetAllPromoByDate(date)

PRIVATE FUNCTION:
- __construct()
*/
class Mpromo extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('promo');
}

public function GetAllPromoByDate($date='',$strProduct='',$strBrand='') {
	$strWhere = '';
	$arrWhere = array();
	if(!empty($strProduct)) {
		$arrWhere[] = "prom_product_id IN ($strProduct)";
	}
	if(!empty($strBrand)) {
		$arrWhere[] = "prom_brand_id IN ($strBrand)";
	}
	if(!empty($arrWhere)) {
		$strWhere .= ' AND ('.implode(' OR ', $arrWhere).')';
	}
	$this->setQuery(
"SELECT p.id,prom_title,prom_product_id,prom_bonus_product_id,prom_brand_id,prom_bonus_brand_id,prom_syarat,prom_bonus_qty,prom_discount,prom_syarat2,prom_bonus_qty2,prom_discount2,prom_syarat3,prom_bonus_qty3,prom_discount3,prom_include_note
FROM promo AS p
WHERE p.prom_start <= '$date' AND p.prom_end >= '$date'{$strWhere}");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

}