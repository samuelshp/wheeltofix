<?php
/**
 * 
 */
class Mdebt extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();		
	}

	public function getDetailDPH($intSupplierID)
    {
       $this->setQuery("
            SELECT TBUNION.id, dphu_code,dphu_amount,supp_id, dphu_status FROM
			(
			-- Untuk reff ke PI
			SELECT dph.id, dph.dphu_code, dph.dphu_amount, pi.pinv_supplier_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN purchase_invoice AS pi ON dph_item.dphi_reff_id = pi.id
			WHERE dph_item.dphi_reff_type = 'PI' AND (pinv_status <= ".STATUS_FINISHED." AND pinv_status != ".STATUS_DELETED.")
			UNION ALL
			-- Untuk reff ke NKS
			SELECT dph.id, dph.dphu_code, dph.dphu_amount, txod.txod_supp_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN transaction_out AS txou ON dph_item.dphi_reff_id = txou.id
			LEFT JOIN transaction_out_detail AS txod ON dph_item.dphi_reff_id = txod.txod_txou_id
			WHERE (dph_item.dphi_reff_type = 'NKS' OR dph_item.dphi_reff_type = 'NDS') AND (txou_status_trans < ".STATUS_FINISHED." AND txou_status_trans != ".STATUS_DELETED.") 
			UNION ALL
			-- Untuk reff ke DP
			SELECT dph.id, dph.dphu_code, dph.dphu_amount, dpay_supplier_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			JOIN downpayment AS dp ON dph_item.dphi_reff_id = dp.id
			WHERE dph_item.dphi_reff_type = 'DP' AND (dpay_status <= ".STATUS_FINISHED." AND dpay_status != ".STATUS_DELETED.")
			)
			AS TBUNION
			LEFT JOIN jw_supplier AS supp ON TBUNION.supp_id = supp.id
			WHERE supp_id = '$intSupplierID' AND dphu_status = ".STATUS_OUTSTANDING."
			GROUP BY dphu_code
        ");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
    }

    public function updateTerbayar($arrFilter)
    {    	
    	foreach ($arrFilter as $filterValue) {
            
            $intDPHid = explode("-", $filterValue)[2];            
	    	
	    	$this->initialize("daftar_pembayaran_hutang_item");
	    	$this->dbSelect("dphi_reff_id, dphi_reff_type, dphi_amount_rencanabayar","dphi_dphu_id = '$intDPHid'");

	    	if($this->getNumRows() > 0) $items = $this->getQueryResult('Array');
	    	else $items = false;

	    	if (!empty($items)) {
	    		foreach ($items as $item) {
		    		
		    		switch ($item['dphi_reff_type']) {
			    		case 'PI': $strTableName = 'purchase_invoice'; $strPre = 'pinv'; $strCustomColumn['total'] = 'finaltotal'; $strCustomColumn['id'] = 'id'; $intTax = 0; break;
			    		case 'DP': $strTableName = 'downpayment'; $strPre = 'dpay'; $strCustomColumn['total'] = 'amount_final'; $strCustomColumn['id'] = 'id'; $intTax = "(dpay_amount*dpay_pph/100)"; break;
			    		case 'NDS':
			    		case 'NKS': $strTableName = 'transaction_out_detail'; $strPre = 'txod'; $strCustomColumn['total'] = 'totalbersih'; $strCustomColumn['id'] = 'txod_txou_id'; $intTax = 0;break;
			    	}

			    	$intUpdate = $this->setQuery("UPDATE $strTableName SET ".$strPre."_terbayar = ".$strPre."_terbayar + ".$item['dphi_amount_rencanabayar']." + ".$intTax.", ".$strPre."_sisa_bayar = ".$strPre."_".$strCustomColumn['total']." - ".$strPre."_terbayar WHERE ".$strCustomColumn['id']." = ".$item['dphi_reff_id']);
		    	}
		    	$intUpdate = true;	    	
	    	}else{
	    		$intUpdate = false;
	    	}

    	}

    	return $intUpdate;
      	
    }  

    public function getMasterPengeluaranLain()
    {
    	$this->initialize('jw_jenis_pengeluaran');
    	$this->dbSelect("*");

    	if($this->getNumRows() > 0 ) return $this->getQueryResult('Array');
    	else return false;
    }
}