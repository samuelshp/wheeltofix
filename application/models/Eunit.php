<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Eunit extends Eloquent 
{
    protected $table = 'jw_unit';
    public $timestamps = false;

    public function material_units()
    {
        return $this->hasMany(Ematerialunit::class, 'munit_unit_id');
    }
}