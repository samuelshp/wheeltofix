<?php
/*
PUBLIC FUNCTION:
- getName(intIDUnit1)
- getConversion(intIDUnit1)
- getComparisonUnits(intIDUnit1,intIDUnit2)
- getUnitsForCalculation(intProductID)
- getUnits(intProductID)
- getBaseUnit(intProductID)
- convertToBaseQuantity(intQty1,intQty2,intQty3,intProductID)
- convertToSeparatedQuantity(intQty,intProductID)

PRIVATE FUNCTION:
- __construct()	
*/

class Munit extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_product');
}

public function getName($intIDUnit1) {
    $intProductID = substr($intIDUnit1,0,-1);
    $intUnitID = substr($intIDUnit1,-1);
    
    switch($intUnitID) {
        case '1': $strUnitSelect = 'prod_unit1'; break;
        case '2': $strUnitSelect = 'prod_unit2'; break;
        case '3': $strUnitSelect = 'prod_unit3'; break;
    }
    
    $this->setQuery(
"SELECT $strUnitSelect AS unit_title
FROM jw_product AS p
WHERE p.id = $intProductID");
    
    if($this->getNumRows() > 0) return  $this->getNextRecord('Object')->unit_title;
    else return false;
}

public function getConversion($intIDUnit1) {
    if(empty($intIDUnit1)) return false;
    
    $intProductID = substr($intIDUnit1,0,-1);
    $intUnitID = substr($intIDUnit1,-1);
    switch($intUnitID) {
        case '1': $strConvSelect = 'prod_conv1'; break;
        case '2': $strConvSelect = 'prod_conv2'; break;
        case '3': $strConvSelect = 'prod_conv3'; break;
    }
    
    $this->setQuery(
"SELECT $intIDUnit1 AS id,$strConvSelect AS unit_conversion
FROM jw_product AS p
WHERE p.id = $intProductID");
    
    if($this->getNumRows() > 0) return  $this->getQueryResult('Array');
    else return false;
}
    
public function getComparisonUnits($intIDUnit1,$intIDUnit2) {
    if(empty($intIDUnit1) || empty($intIDUnit2)) return false;
    
    // Unit 1
    $intProductID = substr($intIDUnit1,0,-1);
    $intUnitID = substr($intIDUnit1,-1);
    
    switch($intUnitID) {
        case '1': $strConvSelect = 'prod_conv1'; break;
        case '2': $strConvSelect = 'prod_conv2'; break;
        case '3': $strConvSelect = 'prod_conv3'; break;
    }

    $this->setQuery(
"SELECT $intIDUnit1 AS id,$strConvSelect AS unit_conversion
FROM jw_product AS p
WHERE p.id = $intProductID");
    
    if($this->getNumRows() > 0) $arrData[] = $this->getNextRecord('Array');
    
    // Unit 2
    $intProductID = substr($intIDUnit2,0,-1);
    $intUnitID = substr($intIDUnit2,-1);
    
    switch($intUnitID) {
        case '1': $strConvSelect = 'prod_conv1'; break;
        case '2': $strConvSelect = 'prod_conv2'; break;
        case '3': $strConvSelect = 'prod_conv3'; break;
    }

    $this->setQuery(
"SELECT $intIDUnit2 AS id,$strConvSelect AS unit_conversion
FROM jw_product AS p
WHERE p.id = $intProductID");
    
    if($this->getNumRows() > 0) $arrData[] = $this->getNextRecord('Array');
    
    return $arrData;
}

    
public function getUnitsForCalculation($intProductID) {
    if(empty($intProductID)) return false;
    
    $this->setQuery(
"SELECT id,prod_conv1,prod_unit1,prod_conv2,prod_unit2,prod_conv3,prod_unit3
FROM jw_product
WHERE id = $intProductID");
    
    if($this->getNumRows() > 0) {
        $arrTempData = $this->getNextRecord('Array');
        $arrData[] = array('id' => $intProductID.'1', 'unit_conversion' => $arrTempData['prod_conv1'], 'unit_title' => $arrTempData['prod_unit1']);
        $arrData[] = array('id' => $intProductID.'2', 'unit_conversion' => $arrTempData['prod_conv2'], 'unit_title' => $arrTempData['prod_unit2']);
        $arrData[] = array('id' => $intProductID.'3', 'unit_conversion' => $arrTempData['prod_conv3'], 'unit_title' => $arrTempData['prod_unit3']);
        
        return $arrData;

    } else return false;
}

public function getUnits($intProductID) {
    $this->setQuery(
"SELECT id,prod_unit1,prod_unit2,prod_unit3
FROM jw_product
WHERE id = $intProductID");
    
    if($this->getNumRows() > 0) {
        $arrTempData = $this->getNextRecord('Array');
        $arrData[] = array('id' => $intProductID.'1', 'unit_title' => $arrTempData['prod_unit1']);
        $arrData[] = array('id' => $intProductID.'2', 'unit_title' => $arrTempData['prod_unit2']);
        $arrData[] = array('id' => $intProductID.'3', 'unit_title' => $arrTempData['prod_unit3']);
        
        return $arrData;

    } else return false;
}

public function getBaseUnit($intProductID) {
    $this->setQuery("SELECT prod_unit3 AS unit_title FROM jw_product WHERE id = $intProductID");
    
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->unit_title;
	else return false;
}

public function convertToBaseQuantity($intQty1,$intQty2,$intQty3,$intProductID) {
    $this->dbSelect('prod_conv1,prod_conv2,prod_conv3',"id = $intProductID");
    if($this->getNumRows() > 0) {
        $arrConversion = $this->getNextRecord('Array');
        $intConv1 = $arrConversion['prod_conv1'];
        $intConv2 = $arrConversion['prod_conv2'];
        $intConv3 = $arrConversion['prod_conv3'];

        return ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);

    } else return false;
}

public function convertToSeparatedQuantity($intQty,$intProductID) {
    $this->dbSelect('prod_conv1,prod_conv2,prod_conv3',"id = $intProductID");

    if($this->getNumRows() > 0) {
        $arrConversion = $this->getNextRecord('Array');
        $intConv1 = $arrConversion['prod_conv1'];
        $intConv2 = $arrConversion['prod_conv2'];
        $intConv3 = $arrConversion['prod_conv3'];

        $intTempQty = $intQty;

        if(!empty($intConv1) && $intConv1 > 1) {
            $intQty1 = intval($intTempQty / $intConv1);
            $intTempQty %= $intConv1;
            
        } else $intQty1 = 0;

        if(!empty($intConv2) && $intConv1 > 1) {
            $intQty2 = intval($intTempQty / $intConv2);
            $intTempQty %= $intConv2;

        } else $intQty2 = 0;

        $intQty3 = $intTempQty;

        return array('intQty1' => $intQty1,'intQty2' => $intQty2,'intQty3' => $intQty3);

    } else false;
}

}

/* End of File */