<?php

class Mdeposit extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();
	$this->initialize('deposit');
}

public function getCount() {
	$this->dbSelect('id');

	return $this->getNumRows();
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT d.id, d.cdate AS depo_date, depo_price, depo_status, cust_name, cust_address, cust_city, cust_phone
FROM deposit AS d
LEFT JOIN jw_customer AS c ON depo_customer_id = c.id
ORDER BY d.cdate DESC, d.id DESC LIMIT $intStartNo, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function searchByCustomer($strCustomerName, $arrSearchDate, $intStatus) {
	$strWhere = '';
    if(!empty($strCustomerName)) {
        $strCustomerName = urldecode($strCustomerName);
        $strWhere .= " AND (cust_name LIKE '%{$strCustomerName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (d.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (d.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (depo_status = {$intStatus})";

	$this->setQuery(
"SELECT d.id, d.cdate AS depo_date, depo_price, depo_status, cust_name, cust_address, cust_city, cust_phone
FROM deposit AS d
LEFT JOIN jw_customer AS c ON depo_customer_id = c.id
WHERE d.id > 0{$strWhere}
ORDER BY d.cdate DESC, d.id  DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intCustomerID,$intPrice) {
	return $this->dbInsert(array(
		'depo_customer_id' => $intCustomerID,
		'depo_price' => $intPrice,
		'depo_status' => 2,
		'cdate' => date('Y-m-d H:i')
	));
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */