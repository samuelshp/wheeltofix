<?php

class Mgiro extends JW_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getAllData($type)
	{

		if ($type == 'in') {
			$this->setQuery(
				"SELECT * FROM
				(SELECT umcu_code AS `code`, umcu_date AS `date`, umcu_cust_id, umcu_type AS `metode`, umcu_ref_giro AS `ref_giro`, 
				umcu_jatuhtempo AS `jatuhtempo`, umcu_status_giro as status_giro, SUM(umcu_amount) AS total FROM uang_masuk_customer
				GROUP BY code
				UNION ALL
				SELECT txin_code AS `code`, txin_date AS `date`, txin_cust_id, txin_metode AS `metode`, 
				txin_ref_giro AS `ref_giro`, txin_jatuhtempo AS `jatuhtempo`,txin_status_giro as status_giro,
				SUM(txid_totalbersih) AS total
				FROM transaction_in AS txin
				LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
				GROUP BY code) AS tbUnion
				WHERE metode = 3 AND status_giro = ".STATUS_OUTSTANDING."
				ORDER BY `date` DESC"
			);
		}else{
			$this->setQuery(
				"SELECT * FROM
				(SELECT txou_code AS `code`, txou_date AS `date`, txod_amount, txou_metode AS `metode`, txou_ref_giro AS `ref_giro`, txou_jatuhtempo  AS `jatuhtempo`, txou_status_giro as status_giro, SUM(txod_totalbersih) AS total FROM transaction_out as txou LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
					GROUP BY txou_code
				) as tbUnion
				WHERE metode = 3 AND status_giro = ".STATUS_OUTSTANDING
			);
		}
		

		if ($this->getNumRows() > 0 ) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllApproved($type)
	{
		if ($type == "in") {
			$this->setQuery(
				"SELECT * FROM
				(SELECT id, umcu_code AS `code`, umcu_date AS `date`, umcu_cust_id, umcu_type AS `metode`, umcu_ref_giro AS `ref_giro`, 
				umcu_jatuhtempo AS `jatuhtempo`, umcu_status_giro as status_giro, SUM(umcu_amount) AS total, appgirodate FROM uang_masuk_customer
				GROUP BY code
				UNION ALL
				SELECT txin.id as id, txin_code AS `code`, txin_date AS `date`, txin_cust_id, txin_metode AS `metode`, 
				txin_ref_giro AS `ref_giro`, txin_jatuhtempo AS `jatuhtempo`,txin_status_giro as status_giro,
				SUM(txid_totalbersih) AS total, appgirodate
				FROM transaction_in AS txin
				LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
				GROUP BY code) AS tbUnion
				WHERE metode = 3 AND status_giro > ".STATUS_OUTSTANDING."
				ORDER BY `date` DESC"
			);
		}else{
			$this->setQuery(
				"SELECT * FROM
				(SELECT txou.id, txou_code as code, txou_date as `date`, SUM(txod_amount) AS total, txou_metode AS `metode`, txou_ref_giro AS `ref_giro`, txou_jatuhtempo AS `jatuhtempo`, txou_status_giro as status_giro, txou.appgirodate FROM transaction_out as txou
				LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
				GROUP BY code
				) as tbUnion
				WHERE metode = 3 AND status_giro > ".STATUS_OUTSTANDING."  ORDER BY `date` DESC"
			);
		}

		if ($this->getNumRows() > 0 ) return $this->getQueryResult('Array');
		else return false;
	}

	public function getCount() {  
		$this->initialize('uang_masuk_customer');
        $this->dbSelect('id');
        return $this->getNumRows();
    }

    public function getDetail($strGiroType, $intID)
    {
    	switch ($strGiroType) {
    		case 'in':    			
				$this->setQuery("SELECT * FROM
				(SELECT id, umcu_code AS `code`, umcu_date AS `date`, umcu_cust_id, umcu_type AS `metode`, umcu_ref_giro AS `ref_giro`, 
				umcu_jatuhtempo AS `jatuhtempo`, umcu_status_giro as status_giro, SUM(umcu_amount) AS total, appgirodate FROM uang_masuk_customer
				GROUP BY code
				UNION ALL
				SELECT txin.id as id, txin_code AS `code`, txin_date AS `date`, txin_cust_id, txin_metode AS `metode`, 
				txin_ref_giro AS `ref_giro`, txin_jatuhtempo AS `jatuhtempo`,txin_status_giro as status_giro,
				SUM(txid_totalbersih) AS total, appgirodate
				FROM transaction_in AS txin
				LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
				GROUP BY code) AS tbUnion
				WHERE metode = 3 AND status_giro > ".STATUS_OUTSTANDING." AND id = '$intID'
				ORDER BY `date` DESC");
    			break;
    
    		case 'out':				
				$this->setQuery("SELECT * FROM
				(SELECT txou.id, txou_code as code, txou_date as `date`, SUM(txod_amount) AS total, txou_metode AS `metode`, txou_ref_giro AS `ref_giro`, txou_jatuhtempo AS `jatuhtempo`, txou_status_giro as status_giro, txou.appgirodate FROM transaction_out as txou
				LEFT JOIN transaction_out_detail as txod ON txou.id = txod.txod_txou_id
				GROUP BY code
				) as tbUnion
				WHERE metode = 3 AND status_giro > ".STATUS_OUTSTANDING." AND id = '$intID'
				ORDER BY `date` DESC");
				break;		

    	}    	
    	
    	if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
    	else return false;
    }

	public function approve($arrApproval)
	{
		
		foreach ($arrApproval['approval_date'] as $key => $value){
			
			if ($value != '') {	
				$item = explode("-", $arrApproval['approval_type'][$key]);	
				$code = $item[0];
				$status = $item[1];
				$codeType = substr($code, 0,3);
				switch ($codeType) {
					case 'UMC': $strTableName = 'uang_masuk_customer'; $pre = 'umcu'; break;
					case 'PML':						
					case 'PPI': $strTableName = 'transaction_in'; $pre = 'txin'; break;
					case 'PHU': $strTableName = 'transaction_out'; $pre = 'txou'; break;
					case 'PGL': $strTableName = 'transaction_out'; $pre = 'txou'; break;
					case 'NKS': $strTableName = 'transaction_out'; $pre = 'txou'; break;
					case 'NDS': $strTableName = 'transaction_out'; $pre = 'txou'; break;
				}
				$strFieldCode = $pre."_code";
				$strApprovalDate = str_replace("/", "-", $arrApproval['approval_date'][$key	]);
				$this->initialize($strTableName);
				$intStatusUpdate =  $this->dbUpdate(array(
					$pre.'_status_giro' => $status,
					'appgirodate' => $strApprovalDate
				), "$strFieldCode = '$code'");
			}			

		}

		return $intStatusUpdate;		
	}

	public function updateApproval($arrApproval)
	{				
		$item = explode("-", $arrApproval['approval_type']);	
		$code = $item[0];
		$status = $item[1];
		$codeType = substr($code, 0,3);		
		switch ($codeType) {
			case 'UMC': $strTableName = 'uang_masuk_customer'; $pre = 'umcu'; break;						
			case 'PML':
			case 'PPI': $strTableName = 'transaction_in'; $pre = 'txin'; break;			
			case 'PHU': $strTableName = 'transaction_out'; $pre = 'txou'; break;
			case 'PGL': $strTableName = 'transaction_out'; $pre = 'txou'; break;
			case 'NKS': $strTableName = 'transaction_out'; $pre = 'txou'; break;
			case 'NDS': $strTableName = 'transaction_out'; $pre = 'txou'; break;
		}
		$strFieldCode = $pre."_code";
		$strApprovalDate = str_replace("/", "-", $arrApproval['approval_date']);
		$this->initialize($strTableName);		
		return $this->dbUpdate(array(
			$pre.'_status_giro' => $status,
			'appgirodate' => $strApprovalDate
		), "$strFieldCode = '$code'");		
	}
}