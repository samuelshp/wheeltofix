<?php

class Msubkontrakmaterial extends JW_Model {

// Constructor
public function __construct() { 
    parent::__construct();
    $this->initialize('subkontrak_material');
}

public function getSubkontrakMaterialBySubkontrakID($id)
{
    $this->setQuery(
        "SELECT
            subkontrak_material.id AS id,
            jw_product.id AS product_id,
            jw_product.prod_title AS prod_title,
            subkontrak_material.qty AS qty,
            subkontrak_material.keterangan AS keterangan,
            subkontrak_material.hpp AS hpp,
            subkontrak_material.qty AS qty,
            subkontrak_material.jumlah AS jumlah,
            subkontrak_material.subkontrak_terpb AS subkontrak_terpb,
            jw_unit.unit_title AS satuan_bayar
        FROM subkontrak_material
        LEFT JOIN jw_product ON subkontrak_material.material = jw_product.id
        INNER JOIN jw_unit ON jw_product.satuan_bayar_id = jw_unit.id
        WHERE subkontrak_material.status != 1
            AND subkontrak_material.subkontrak_id = $id
        ORDER BY jw_product.prod_title
        "
    );
    if($this->getNumRows() > 0)
        return $this->getQueryResult('Array');
    else
        return [];
}

public function getAvailableSubstitusi($id)
{
    $this->setQuery(
        "SELECT 
            subkontrak_material.id, 
            jw_product.prod_title
        FROM subkontrak_material
        JOIN jw_product ON subkontrak_material.material = jw_product.id
        WHERE subkontrak_material.status != 1
            AND subkontrak_material.subkontrak_id = $id
        ORDER BY jw_product.prod_title
        "
    );
    if($this->getNumRows() > 0)
        return $this->getQueryResult('Array');
    else
        return [];
}

public function add($arrDataMaterial, $intSubkontrakID)
{    
    $intCount = count($arrDataMaterial['material']);
    $i = 0;
    while($i < $intCount){
        $dbInsert = $this->dbInsert(array(
            'subkontrak_id' => $intSubkontrakID,
            'material'  => $arrDataMaterial['material'][$i],
            'qty' => str_replace(".","",$arrDataMaterial['qty'][$i]),
            'keterangan' => $arrDataMaterial['keterangan'][$i],
            'substitusi' => $arrDataMaterial['substitusi'][$i],
            'hpp' => str_replace(".","",$arrDataMaterial['hpp'][$i]),
            'jumlah' => str_replace(".","",$arrDataMaterial['jumlah'][$i]),
            'subkontrak_terpb' => str_replace(".","",$arrDataMaterial['subkontrak_terpb'][$i])
        ));
        if($dbInsert) $i++;
        else return false;
    }        
    return true;    
}

public function update($rowID,$subkontrak_id, $material, $qty, $keterangan, $substitusi, $hpp, $jumlah, $subkontrak_terpb)
{
    if(empty($rowID))
        $this->dbInsert(array(
            'subkontrak_id' => $subkontrak_id,
            'material' => $material,
            'qty' => $qty,
            'keterangan' => $keterangan,
            'substitusi' => $substitusi,
            'hpp'   => $hpp,
            'jumlah' => $jumlah,
            'subkontrak_terpb' => $subkontrak_terpb
        ));
        else
        $this->dbUpdate(array(
            'subkontrak_id' => $subkontrak_id,
            'material' => $material,
            'qty' => $qty,
            'keterangan' => $keterangan,
            'substitusi' => $substitusi,
            'hpp'   => $hpp,
            'jumlah' => $jumlah,
            'subkontrak_terpb' => $subkontrak_terpb
        ), "id= '$rowID'");
        // $this->setQuery("REPLACE INTO subkontrak_team (`id`, `subkontrak_id`, `name`, `jabatan_id`, `mby`, `mdate`, `status`) VALUES ($row_id, $subkontrak_id, $pegawaiID ,$jabatan_id ,$mby,NOW(), $status)");        

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
}

public function updateTerBpb($idsubkon,$prodId,$jml_baru){
    return $this->dbUpdate(array(
            'subkontrak_terbpb' => $jml_baru
        ),"subkontrak_id = $idsubkon AND material = $prodId");
    // $this->setQuery(
    //     "UPDATE subkontrak_material SET subkontrak_terbpb = $jml_baru WHERE subkontrak_id = $idsubkon AND material = $prodId
    //     "
    // );
    // if($this->getNumRows() > 0)
    //     return $this->getQueryResult('Array');
    // else
    //     return [];
}

public function checkBahanToSubkontrakMaterial($subkon_id, $idprod){
    $this->setQuery(
        "SELECT COUNT(skm.subkontrak_id) as jumlah_row, skm.subkontrak_id, skm.material, sk.kontrak_id, jp.prod_title, ju.unit_title
        FROM subkontrak_material as skm
        LEFT JOIN subkontrak as sk ON sk.id = $subkon_id
        LEFT JOIN jw_product as jp ON jp.id = $idprod
        LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id
        WHERE skm.material = $idprod AND skm.subkontrak_id = $subkon_id"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteItem($id) {
    return $this->dbUpdate(array(
            'status' => 1
        ),"id = $id");
}

}