<?php
/*
*/

class Mpick_list extends JW_Model {

// Constructor
public function __construct() 
{ 
	parent::__construct(); 
	$this->initialize('Pick_list');
}

public function getAllPicklist(){
	$this->setQuery(
		'SELECT *
		FROM pick_list p
		LEFT JOIN invoice_order o ON p.id = o.id
		LEFT JOIN jw_warehouse j ON p.id = j.id'
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

// public function add($arrData)
// {
// 	return $this->dbInsert(array(
// 		'id' => generateTransactionCode($arrData['txtDateBuat'],'','picklist'),
// 		'pcli_kode' => $arrData['']
// 		'pcli_so_id' => $arrData['intSOid'],
// 		'pcli_warehouse_id' => $arrData['intWHid'],
// 		'pcli_product_id' => $arrData['intProductId'],
// 		'pcli_qty' => $arrData['intQty'],
// 		'pcli_qty_display' => $arrData['intQdis'],
// 		'pcli_qty_processed' => $arrData['intQpro'],
// 		'pcli_qty_processed_display' => $arrData['intQproDis'],
// 		'pcli_status' => $arrData['intStatus'],
// 		'pcli_cby' => $arrData['intDibuat'],
// 		'pcli_cdate' => $arrData['txtDateBuat'],
// 		'pcli_mby' => $arrData['intDiubah'],
// 		'pcli_mdate' => $arrData['txtDateUbah']
// 	));
// }

}

/* End of File */