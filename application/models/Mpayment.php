<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intPayment)

PRIVATE FUNCTION:
- __construct() 
*/

class Mpayment extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();
    $this->initialize('payment');
}

public function add($intHeaderID,$intType,$intSalesman,$txtDate='',$intInvoiceID=0,$intAccountID=0,$strBankID='',$txtBG='',$strDescription='',$intStatus) {
    return $this->dbInsert(array(
        'paym_header_id' => $intHeaderID,
        'paym_type' => $intType,
        'paym_salesman_id' => $intSalesman,
        'paym_date' => $txtDate,
        'paym_invoice_id' => $intInvoiceID,
        'paym_account_id' => $intAccountID,
        'paym_bank_id' => $strBankID,
        'paym_bg_number' => $txtBG,
        'paym_description' => $strDescription,
        'paym_status' => $intStatus
        ));
}

public function getAllPayment($strKeyword = '',$intPerPage = 0,$intStartNo = 0) {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " WHERE paym_code LIKE '%$strKeyword%' OR pahe_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%'";
    } else $strWhere = '';

    if($intPerPage > 0) $strLimit = "LIMIT $intStartNo,$intPerPage";
    else $strLimit = "LIMIT 0,18446744073709551615";

    $this->setQuery(
'        SELECT 
            p.id,pahe_code,
            cust_name,cust_address,cust_city,cust_phone,
            sals_name,sals_address,sals_phone,
            invo_payment,invo_jatuhtempo,invo_grandtotal,invo_jualkanvas,
            acco_code,acco_name,
            paym_code,paym_type,paym_description,paym_bank_id,paym_bg_number,paym_date,paym_status
        FROM payment AS p
        LEFT JOIN invoice AS i ON paym_invoice_id = i.id
        LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
        LEFT JOIN jw_salesman AS s ON paym_salesman_id = s.id
        LEFT JOIN jw_account AS ac ON paym_account_id = ac.id
        LEFT JOIN payment_header AS pa ON paym_header_id = pa.id
        '.$strWhere.'
        ORDER BY p.cdate DESC, p.id DESC
        '.$strLimit);

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount() {
    $this->dbSelect('id');

    return $this->getNumRows();
}

public function getStatusPaymentByID($intID) {
    $this->setQuery(
'        SELECT paym_status
        FROM payment AS p
        WHERE p.id = '.$intID.'');
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function editByID($intID,$intStatus='') {
    return $this->dbUpdate(array(
        'paym_status' => $intStatus),
    "id = $intID");
}

public function editByID2($intID,$strDescription,$intStatus) {
    return $this->dbUpdate(array(
        'paym_description' => $strDescription,
        'paym_status' => $intStatus),
    "id = $intID");
}

public function getHeaderIDByID($intID) {

    $this->setQuery(
'        SELECT r.id,paym_header_id
        FROM payment AS r
        WHERE r.id = '.$intID.'
        ORDER BY r.id DESC');

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataHeaderByID($intID) {

    $this->setQuery(
'        SELECT rh.id, pahe_code,rh.cdate AS pahe_date
        FROM payment_header AS rh
        WHERE rh.id = '.$intID.'
        ORDER BY rh.id DESC');

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getPrintDataByHeaderID($intID) {

    $this->setQuery(
'        SELECT
            p.id,pahe_code,
            cust_name,cust_address,cust_city,cust_phone,
            sals_name,sals_address,sals_phone,
            invo_code,invo_payment,invo_jatuhtempo,invo_grandtotal,invo_jualkanvas,
            paym_code,paym_date,paym_type,paym_description,paym_bank_id,paym_bg_number,paym_status
        FROM payment AS p
        LEFT JOIN invoice AS i ON paym_invoice_id = i.id
        LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
        LEFT JOIN jw_salesman AS s ON paym_salesman_id = s.id
        LEFT JOIN payment_header AS pa ON paym_header_id = pa.id
        WHERE paym_header_id = '.$intID.'
        ORDER BY p.cdate DESC, p.id ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function autoExpired($intDay=1) {
    // Tunai
    $this->setQuery(
"UPDATE payment SET paym_status = 3 WHERE DATE_ADD(cdate,INTERVAL {$intDay} DAY) <= CURRENT_TIMESTAMP AND paym_status = 2 AND paym_type = 4");
    // BG
    $this->setQuery(
"UPDATE payment SET paym_status = 3 WHERE paym_date <= CURRENT_TIMESTAMP AND paym_status = 2 AND paym_type = 6");
    return true;
}

}

/* End of File */