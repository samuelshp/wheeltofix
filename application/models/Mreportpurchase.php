<?php

class Mreportpurchase extends JW_Model {

public $_intWarehouseID;

public function __construct() {
	parent::__construct();	
	$this->_intWarehouseID = 1;
}

/*
public function getItemsPurchasePerSupplier($intStartNo = -1,$intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "supp_code ASC";
	else $strOrderBy = "supp_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT id as supp_id, supp_code, supp_name
FROM jw_supplier 
ORDER BY supp_code ASC    
    ");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;    
    
}

public function getItemsPurchasePerSupplierDetail() {
    
    $this->setQuery(
"SELECT p.prch_supplier_id, p.cdate, b.prod_code, b.prod_title, i.prci_description, i.prci_quantity1,
	i.prci_quantity2, i.prci_quantity3, i.prci_price, i.prci_discount1, i.prci_discount2,
    i.prci_unit1, i.prci_unit2, i.prci_unit3,
	i.prci_discount3, i.prci_bonus 
FROM purchase_item i
	INNER JOIN purchase p ON (p.id = i.prci_purchase_id)
	LEFT JOIN jw_product b ON (b.id = i.prci_product_id)
ORDER BY p.prch_supplier_id, p.cdate, b.prod_title");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false; 
    
}

public function getCountPurchasePerSupplier() {
    
    $this->setQuery(
"SELECT id as supp_id, supp_code, supp_name
FROM jw_supplier");
    
    return $this->getNumRows();
}    
*/

public function getCountPurchasePerSupplierBySupplier() {
/*    $this->setQuery(
"SELECT p.prch_supplier_id
FROM purchase_item i
	INNER JOIN purchase p ON (p.id = i.prci_purchase_id)
	LEFT JOIN jw_supplier s ON (s.id = p.prch_supplier_id)
	LEFT JOIN jw_product b ON (b.id = i.prci_product_id)");*/
	$this->setQuery("SELECT i.id FROM purchase_item i");

    return $this->getNumRows();
}

public function getItemsPurchasePerSupplierBySupplier($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.prch_supplier_id, p.cdate, b.prod_title";
    else $strOrderBy = "p.prch_supplier_id, p.cdate, b.prod_title LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT p.prch_supplier_id, p.cdate, b.prod_code, b.prod_title, i.prci_description, i.prci_quantity1,
	i.prci_quantity2, i.prci_quantity3, i.prci_price, i.prci_discount1, i.prci_discount2,
    i.prci_unit1, i.prci_unit2, i.prci_unit3,
	i.prci_discount3, i.prci_bonus 
FROM purchase_item i
	INNER JOIN purchase p ON (p.id = i.prci_purchase_id)
	LEFT JOIN jw_supplier s ON (s.id = p.prch_supplier_id)
	LEFT JOIN jw_product b ON (b.id = i.prci_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function searchPurchasePerSupplierBySupplier($suppID,$txtStart,$txtEnd) {
    
    $suppID = urldecode($suppID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT p.prch_supplier_id, p.cdate, b.prod_code, b.prod_title, i.prci_description, i.prci_quantity1,
	i.prci_quantity2, i.prci_quantity3, i.prci_price, i.prci_discount1, i.prci_discount2,
    i.prci_unit1, i.prci_unit2, i.prci_unit3,
	i.prci_discount3, i.prci_bonus 
FROM purchase_item i
	INNER JOIN purchase p ON (p.id = i.prci_purchase_id)
	LEFT JOIN jw_supplier s ON (s.id = p.prch_supplier_id)
	LEFT JOIN jw_product b ON (b.id = i.prci_product_id)
WHERE s.id = $suppID AND p.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY p.prch_supplier_id, p.cdate, b.prod_title");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;  
    
}



public function getCountDetailBeliPerBarangBySupplier() {
    /*$this->setQuery(
"SELECT pu.prch_code
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)");*/
	$this->setQuery("SELECT pu.prch_code FROM purchase pu");

    return $this->getNumRows();
}

public function searchDetailBeliPerBarangBySupplier($productID,$txtStart,$txtEnd) {

    $productID = urldecode($productID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT pu.prch_code,pu.cdate,supp_name,p.prod_title, it.prci_quantity1,it.prci_quantity2,
	it.prci_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.prci_price,
	it.prci_discount1,it.prci_discount2,it.prci_discount3
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)
WHERE s.id = $productID AND pu.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY prch_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getItemsDetailBeliPerSupplierByBarang($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "prch_code ASC";
    else $strOrderBy = "prch_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT pu.prch_code,pu.cdate,p.prod_title, it.prci_quantity1,it.prci_quantity2,
	it.prci_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.prci_price,
	it.prci_discount1,it.prci_discount2,it.prci_discount3
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountDetailBeliPerSupplierByBarang() {
    /*$this->setQuery(
"SELECT pu.prch_code
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)");*/
	$this->setQuery("SELECT pu.prch_code FROM purchase pu");

    return $this->getNumRows();
}

public function searchDetailBeliPerSupplierByBarang($suppID,$txtStart,$txtEnd) {

    $suppID = urldecode($suppID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT pu.prch_code,pu.cdate,p.prod_title, it.prci_quantity1,it.prci_quantity2,
	it.prci_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.prci_price,
	it.prci_discount1,it.prci_discount2,it.prci_discount3
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)
WHERE s.id = $suppID AND pu.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY prch_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsDetailBeliPerBarangBySupplier($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "prch_code ASC";
    else $strOrderBy = "prch_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT pu.prch_code,pu.cdate,supp_name,p.prod_title, it.prci_quantity1,it.prci_quantity2,
	it.prci_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.prci_price,
	it.prci_discount1,it.prci_discount2,it.prci_discount3
FROM purchase pu
	INNER JOIN purchase_item it ON (it.prci_purchase_id = pu.id)
	LEFT JOIN jw_supplier s ON (s.id = pu.prch_supplier_id)
	LEFT JOIN jw_product p ON (p.id = it.prci_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getItemsRekapBeliPerSupplierByBarang($intStartNo = -1, $intPerPage = -1, $suppID = 0) {
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "prod_title ASC";
    else $strOrderBy = "prod_title ASC LIMIT $intStartNo, $intPerPage";
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,prci_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN purchase_item ui ON (ui.id = th.trhi_transaction_id)
WHERE th.trhi_type = 'purchase' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id
ORDER BY $strOrderBy");

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountRekapBeliPerSupplierByBarang() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    /*$this->setQuery(
"SELECT prod_code
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN purchase_item ui ON (ui.id = th.trhi_transaction_id)
WHERE th.trhi_type = 'purchase' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");*/
    $this->setQuery(
"SELECT trhi_product_id
FROM transaction_history th
WHERE th.trhi_type = 'purchase' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3)
GROUP BY th.trhi_product_id");

    return $this->getNumRows();
}

public function searchRekapBeliPerSupplierByBarang($suppID,$txtStart,$txtEnd) {

    $suppID = urldecode($suppID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT prod_code,prod_title,trhi_quantity1,trhi_quantity2,trhi_quantity3,
	p.prod_unit1,p.prod_unit2,p.prod_unit3,prci_subtotal
FROM transaction_history th
	LEFT JOIN jw_product p ON (p.id = th.trhi_product_id)
	LEFT JOIN purchase_item ui ON (ui.id = th.trhi_transaction_id)
	   INNER JOIN purchase u ON (u.id = ui.prci_purchase_id)
WHERE th.trhi_type = 'purchase' 
	AND trhi_warehouse_id = $intWarehouseID AND trhi_status in (2,3) AND trhi_date BETWEEN '$txtStart' AND '$txtEnd'
    AND prch_supplier_id = $suppID
GROUP BY th.trhi_product_id
ORDER BY prod_title");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function searchPembelianRekap($txtStart,$txtEnd,$suppName) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d').' 00:00:00';
	$txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d').' 23:59:59';
	$suppName = urldecode($suppName);
	if(empty($suppName))
		$suppName = "%";
	else
		$suppName = "%".$suppName."%";
	
	$this->setQuery(
"	SELECT a.id, a.acce_code AS faktur, a.cdate AS tanggal,
	a.acce_grandtotal AS total, a.acce_description AS keterangan,
	b.supp_name AS supplier, b.supp_city AS kota,
	c.ware_name AS gudang
	FROM acceptance a
	LEFT JOIN jw_supplier b ON a.acce_supplier_id = b.id AND b.supp_name LIKE '".$suppName."'
	JOIN jw_warehouse c ON a.acce_warehouse_id = c.id
	WHERE a.acce_status IN (2,3)
	AND a.acce_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'
	ORDER BY a.acce_code, a.cdate");
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}
public function getCountPembelianRekap() {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	$this->setQuery(
"	SELECT a.id, a.acce_code AS faktur, a.cdate AS tanggal,
	a.acce_grandtotal AS total, a.acce_description AS keterangan,
	b.supp_name AS supplier, b.supp_city AS kota,
	c.ware_name AS gudang
	FROM acceptance a
	LEFT JOIN jw_supplier b ON a.acce_supplier_id = b.id
	JOIN jw_warehouse c ON a.acce_warehouse_id = c.id
	WHERE a.acce_status IN (2,3)
	AND a.acce_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'");
	
    return $this->getNumRows();
}
public function getItemsPembelianRekap($intStartNo = -1,$intPerPage = -1) {
	$branchID = $this->session->userdata('intBranchID');
	$txtStart = date('Y-m-d').' 00:00:00';
	$txtEnd = date('Y-m-d').' 23:59:59';
	
	if($intStartNo == -1 || $intPerPage == -1)
		$strLimit = "";
	else
		$strLimit = " LIMIT ".$intStartNo.", ".$intPerPage;
	
	$this->setQuery(
"	SELECT a.id, a.acce_code AS faktur, a.cdate AS tanggal,
	a.acce_grandtotal AS total, a.acce_description AS keterangan,
	b.supp_name AS supplier, b.supp_city AS kota,
	c.ware_name AS gudang
	FROM acceptance a
	LEFT JOIN jw_supplier b ON a.acce_supplier_id = b.id
	JOIN jw_warehouse c ON a.acce_warehouse_id = c.id
	WHERE a.acce_status IN (2,3)
	AND a.acce_branch_id = '".$branchID."'
	AND a.cdate >= '".$txtStart."'
	AND a.cdate <= '".$txtEnd."'
	ORDER BY a.acce_code, a.cdate
	".$strLimit);
	
	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}

}