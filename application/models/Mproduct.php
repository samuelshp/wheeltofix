<?php
/*
PUBLIC FUNCTION:
- getAllItems(strKeyword)
- getAllItemsPerSupplierIDForPO(intSupplierID,strKeyword)
- getAllItemsPerSupplierID(intSupplierID,intOutletType,strKeyword)
- getAllData()
- getNonServiceData()
- getItems(intStartNo,intPerPage)
- getItemByID(intID)
- getTitleByID(intID)
- getTypeByID(intID)
- getPriceByID(intID)
- getAllID()
- getAvailabilityByBarcodeID(intBarcodeID)
- getCount()
- searchByTitle(strKeyword)
- getBrandNameById(intBrandID)
- getAllItemAutoCompleteByPromo(strAllowedBrand,strKeyword)

PRIVATE FUNCTION:
- __construct()
*/

class Mproduct extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_product');
}

public function getStockLimitItems() {
	$this->setQuery(
"SELECT p.id, prod_code, proc_title, prob_title, prod_title, prod_price, prod_currency, prod_hpp, prod_type, prod_conv1, prod_conv2, prod_conv3, prod_stocklimit
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN jw_warehouse AS w ON w.ware_stock_warning > 1
LEFT JOIN transaction_stock AS trst ON trst_product_id = p.id AND trst_warehouse_id = w.id
WHERE p.prod_type = 1 AND trst_quantity <= prod_stocklimit AND prod_status > 1
ORDER BY proc_title ASC, prod_title ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllItemWithSatuan() {
	$this->setQuery(
"SELECT p.id, prod_code, prod_title, prod_price, prod_currency, prod_hpp, unit_title
FROM jw_product AS p
LEFT JOIN jw_unit AS u on u.id = p.satuan_bayar_id
ORDER BY prod_title ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllItems($strKeyword = '', $strUnselect = '', $strFilter = 'Product') {
	$strKeyword = urldecode($strKeyword);

	$strWhere = '';
	if(!empty($strKeyword)) {
		if($strFilter == 'Product')
	    	$strWhere = " AND (prod_title LIKE '%$strKeyword%' OR prod_barcode LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%' OR proc_title LIKE '%$strKeyword%' OR prod_code LIKE '%$strKeyword%')";
		elseif($strFilter == 'Category')
	    	$strWhere = " AND (proc_title LIKE '%$strKeyword%')";
		elseif($strFilter == 'Brand')
	    	$strWhere = " AND (prob_title LIKE '%$strKeyword%')";
	    elseif($strFilter == 'In_ID')
	    	$strWhere = " AND p.id IN (".str_replace('-', ',', $strKeyword).")";
	}

    if(!empty($strUnselect)) $strWhereUnselect = " AND p.id NOT IN (".str_replace('-', ',', $strUnselect).")";
    else $strWhereUnselect = '';

    $this->setQuery(
"SELECT p.id, prod_code, proc_title, prob_title, prod_title, prod_price, prod_currency, prci_price, prod_hpp, prod_type, prod_conv1, prod_conv2, prod_conv3, prod_stocklimit, prod_rak
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN (
	SELECT prci_price,pii.prci_product_id
	FROM purchase_item AS pii
	INNER JOIN (
		SELECT MAX(cdate) AS maxcdate,prci_product_id
		FROM purchase_item
		GROUP BY prci_product_id
	) AS pij ON pij.maxcdate = pii.cdate AND pij.prci_product_id = pii.prci_product_id
	WHERE prci_bonus = 0
    GROUP BY prci_product_id
) AS pi ON prci_product_id = p.id
WHERE prod_status > 1{$strWhere}{$strWhereUnselect}
ORDER BY proc_title ASC, prod_title ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

// public function getAllItemsPerSupplierIDForPO($intSupplierID = 0,$strKeyword = '',$intInternal= 0) {
public function getAllItemsPerSupplier($intSupplierID = 0,$strKeyword = '',$intInternal= 0) {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
	
        $strWhere = " AND (prod_title LIKE '%$strKeyword%' OR prod_barcode LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%' OR proc_title LIKE '%$strKeyword%' OR prod_code LIKE '%$strKeyword%')";
    }
    else $strWhere = '';

    if($intSupplierID != 0 && getSetting('JW_FILTER_PRODUCT_BY_SUPPLIER') == 'yes') {
        $strWhere = $strWhere.' AND (prod_supplier_id = '.$intSupplierID.')';
    }

    $this->setQuery(
"SELECT pu.id as purchase_id, p.id, proc_title, pi.prci_description, pi.prci_unit1, pi.prci_quantity1, pu.prch_code, pu.cdate, po.idKontrak, prob_title, prod_title, prod_description, prod_code,prod_type, prod_conv1, prod_conv2, prod_conv3, prod_rak
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN (
	SELECT prci_price,pii.prci_product_id,cdate, prci_purchase_id, prci_description, prci_unit1, prci_quantity1
	FROM purchase_item AS pii
	INNER JOIN (
		SELECT MAX(cdate) AS maxcdate,prci_product_id
		FROM purchase_item
		GROUP BY prci_product_id
	) AS pij ON pij.maxcdate = pii.cdate AND pij.prci_product_id = pii.prci_product_id
	WHERE prci_bonus = 0
	GROUP BY prci_product_id
) AS pi ON prci_product_id = p.id
LEFT JOIN purchase AS pu ON pu.prch_po_id = pi.prci_purchase_id
LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
WHERE prod_status > 1$strWhere
ORDER BY proc_title ASC, prod_title ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllItemsPerSupplierID($intSupplierID=0,$intOutletType=1,$strKeyword = '') {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
	
        $strWhere = " AND (prod_title LIKE '%$strKeyword%' OR prod_barcode LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%' OR proc_title LIKE '%$strKeyword%' OR prod_code LIKE '%$strKeyword%')";
    }
    else $strWhere = '';

    if($intSupplierID != 0 && getSetting('JW_FILTER_PRODUCT_BY_SUPPLIER') == 'yes')
        $strWhere = $strWhere.' AND (prod_supplier_id = '.$intSupplierID.')';

    if(getSetting('JW_ROUND_INVOICE_PRODUCT_PRICE') == 'yes') $strPriceQuery = 'CEIL(prod_price / 100) * 100 AS prod_price';
    else $strPriceQuery = 'prod_price';

    $this->setQuery(
"SELECT DISTINCT p.id, proc_title, prob_title, prod_title, prod_description, {$strPriceQuery}, prod_pricemodern, prod_currency,pb.id AS id_brand,prod_type, prod_conv1, prod_conv2, prod_conv3, prod_rak,
    sd.skds_discount1,sd.skds_discount2,sd.skds_discount3,prod_hpp,prod_code
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN skema_diskon AS sd ON (sd.skds_supplier_id = prod_supplier_id AND sd.skds_outlettype = $intOutletType)
WHERE prod_status > 1$strWhere
ORDER BY proc_title ASC, prod_title ASC");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllData() {
	$this->setQuery(
"SELECT p.id, prod_category_id, proc_title, prod_type, prob_title, prod_title, prod_price, prod_currency, prci_price,prod_type, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN (
	SELECT prci_price,pii.prci_product_id
	FROM purchase_item AS pii
	INNER JOIN (
		SELECT MAX(cdate) AS maxcdate,prci_product_id
		FROM purchase_item
		GROUP BY prci_product_id
	) AS pij ON pij.maxcdate = pii.cdate AND pij.prci_product_id = pii.prci_product_id
	WHERE prci_bonus = 0
) AS pi ON pi.prci_product_id = p.id
WHERE prod_status > 1
ORDER BY prob_title ASC, prod_title ASC");
	
	if($this->getNumRows() > 0) {
		$arrData = $this->getQueryResult('Array');
		
		for($i = 0; $i < count($arrData); $i++) {
			$this->setQuery("SELECT id,unit_title,unit_conversion FROM jw_unit WHERE unit_category_id = '".$arrData[$i]['prod_category_id']."' AND unit_status > 1");
			$arrData[$i]['arrUnit'] = $this->getQueryResult('Array');
		}
		
		return $arrData;
	} else return false;
}

public function getNonServiceData() {
	$this->setQuery(
"SELECT p.id, prod_category_id, proc_title, prod_type, prob_title, prod_title, prod_price, prod_currency,prod_type, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
WHERE prod_status > 1 AND prod_type = 1
ORDER BY prob_title ASC, prod_title ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getServiceData() {
	$this->setQuery(
"SELECT p.id, prod_category_id, proc_title, prod_type, prob_title, prod_title, prod_price, prod_currency, prod_type, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
WHERE prod_status > 1 AND prod_type = 2
ORDER BY prob_title ASC, prod_title ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT p.id, proc_title, prob_title, prod_title, prod_price, prod_currency, prci_price, prod_hpp, prod_type, prod_conv1, prod_conv2, prod_conv3, prod_stocklimit, prod_rak
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN (
	SELECT prci_price,pii.prci_product_id
	FROM purchase_item AS pii
	INNER JOIN (
		SELECT MAX(cdate) AS maxcdate,prci_product_id
		FROM purchase_item
		GROUP BY prci_product_id
	) AS pij ON pij.maxcdate = pii.cdate AND pij.prci_product_id = pii.prci_product_id
	WHERE prci_bonus = 0
) AS pi ON prci_product_id = p.id
WHERE prod_status > 1
ORDER BY proc_title ASC, prod_title ASC
LIMIT $intStartNo,$intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->setQuery(
"SELECT p.id,prod_code,proc_title, prob_title, prod_title, prod_price, prod_currency, prod_conv1, prod_conv2, prod_conv3, prod_unit1, prod_unit2, prod_unit3, prod_type, prod_profit_margin, u.unit_title
FROM jw_product AS p
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN jw_unit as u ON p.satuan_bayar_id = u.id
WHERE p.id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getTitleByID($intID) {
	$this->setQuery(
"SELECT prod_title,prob_title,proc_title,prod_type
FROM jw_product AS p
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
WHERE p.id = $intID");
	if($this->getNumRows() > 0){
		$arrData = $this->getNextRecord('Array');
		return formatProductName('',$arrData['prod_title'],$arrData['prob_title'],$arrData['proc_title']);
	} else return false;
}

public function getTypeByID($intID) {
	$this->dbSelect('prod_type',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->prod_type;
	else return false;
}

public function getPriceByID($intID) {
	$this->dbSelect('prod_price,prod_currency',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getAllID() {
	$this->dbSelect('id',"prod_status > 1");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAvailabilityByBarcodeID($intBarcodeID) {
	$intID = decodeBarcodeNumber($intBarcodeID);
	
	$this->dbSelect('id',"id = $intID AND prod_status > 1");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->id;
	else return false;
}

public function getBahanByNamaPekerjaan($namaPekerjaan){
	$this->setQuery(
		"SELECT sm.keterangan, p.prod_status, p.prod_title, p.prod_title, sm.qty
		FROM kontrak_material as sm
		LEFT JOIN jw_product as p
		ON sm.material = p.id
		WHERE sm.material = "
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id',"prod_status > 1");
	
	return $this->getNumRows();
}

public function searchByTitle($strKeyword) {
	$strKeyword = urldecode($strKeyword);

	$this->setQuery(
"SELECT p.id, proc_title, prob_title, prod_title, prod_price, prod_currency, prci_price, prod_hpp, prod_type, prod_conv1, prod_conv2, prod_conv3, prod_stocklimit, prod_rak
FROM jw_product AS p
LEFT JOIN (
	SELECT prci_price,pii.prci_product_id
	FROM purchase_item AS pii
	INNER JOIN (
		SELECT MAX(cdate) AS maxcdate,prci_product_id
		FROM purchase_item
		GROUP BY prci_product_id
	) AS pij ON pij.maxcdate = pii.cdate AND pij.prci_product_id = pii.prci_product_id
	WHERE prci_bonus = 0
) AS pi ON prci_product_id = p.id
LEFT JOIN jw_product_category AS pc ON prod_category_id = pc.id
LEFT JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
WHERE prod_status > 1 AND (prod_title LIKE '%$strKeyword%' OR prod_barcode LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%')
ORDER BY proc_title ASC, prod_title ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getBrandNameById($intBrandID) {
    $this->setQuery(
"SELECT p.id, prob_title
FROM jw_product_brand AS p
WHERE p.id =$intBrandID
ORDER BY prob_title ASC");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getAllItemAutoCompleteByPromo($strAllowedBrand='0',$strKeyword='') {
	$strWhereAllowedBrand = '';
	if(!empty($strAllowedBrand)){
		$strWhereAllowedBrand=' AND pb.id IN ('.$strAllowedBrand.')';
	}
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		
		$strWhere = " AND (prod_title LIKE '%$strKeyword%' OR prod_barcode LIKE '%$strKeyword%' OR prob_title LIKE '%$strKeyword%' OR proc_title LIKE '%$strKeyword%' OR prod_code LIKE '%$strKeyword%')";
	} else $strWhere = '';

	$this->setQuery(
'SELECT p.id, proc_title, prob_title, prod_title, prod_description, prod_price, prod_currency, pb.id AS brand_id, prod_code, prod_type, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS p
INNER JOIN jw_product_category AS pc ON prod_category_id = pc.id
INNER JOIN jw_product_brand AS pb ON prod_brand_id = pb.id
WHERE prod_status > 1'.$strWhere.$strWhereAllowedBrand.'
ORDER BY proc_title ASC, prod_title ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCodeAvailable($txtData='') {
    $this->dbSelect('prod_title',"prod_code = '$txtData'");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function updatePriceByID($intPrice, $intProductID) {
	$this->dbUpdate(array(
		'prod_price' => $intPrice,
	), "id = $intProductID");
}

}

/* End of File */