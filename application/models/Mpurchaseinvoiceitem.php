<?php
/*
PUBLIC FUNCTION:
- getItemsByInvoiceID(intInvoiceID,strMode)
- getBonusItemsByInvoiceID(intInvoiceID,strMode)
- add(intInvoiceID,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3, intUnit1,intUnit2,intUnit3,intPrice,intDiscount1,intDiscount2,intDiscount3,flagBonus)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- deleteByInvoiceID(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mpurchaseinvoiceitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	
    //$this->initialize('invoice_item');
    $this->initialize('purchase_invoice_item');
}

public function getLatestPriceByCustomer($intProductID,$intCustomerID) {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,invi_quantity,invi_price,invi_discount',"invi_invoice_id = $intInvoiceID");
    else $this->dbSelect('',"invi_invoice_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, invi_price,invi_discount1,invi_discount2,invi_discount3
FROM invoice_item AS i
LEFT JOIN invoice AS vo ON vo.id = i.invi_invoice_id
WHERE invo_customer_id =  $intCustomerID AND invi_product_id = $intProductID AND invi_bonus = '0'
ORDER BY i.cdate DESC, i.id DESC");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return array();
}

public function getItemsByInvoiceID($intInvoiceID,$strMode = 'ALL') {
	/*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,invi_quantity,invi_price,invi_discount',"invi_invoice_id = $intInvoiceID");
	else $this->dbSelect('',"invi_invoice_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, invi_quantity1, invi_quantity2, invi_quantity3, invi_price, invi_discount1, invi_discount2, invi_discount3, invi_product_id, prob_title, proc_title, prod_type, prod_code,prod_title,invi_description,invi_unit1,invi_unit2,invi_unit3,invi_product_id AS product_id,prod_brand_id,prod_hpp,invo_grandtotal, prod_conv1, prod_conv2, prod_conv3, prod_rak
FROM jw_product AS pro
LEFT JOIN invoice_item AS i ON pro.id = i.invi_product_id
LEFT JOIN invoice AS vo ON vo.id = i.invi_invoice_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE invi_invoice_id =  $intInvoiceID AND invi_bonus = '0'
ORDER BY i.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return array();
}

public function getBonusItemsByInvoiceID($intInvoiceID,$strMode = 'ALL') {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,invi_quantity,invi_price,invi_discount',"invi_invoice_id = $intInvoiceID");
    else $this->dbSelect('',"invi_invoice_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, invi_quantity1, invi_quantity2, invi_quantity3, invi_price, invi_discount1, invi_discount2, invi_discount3, invi_product_id, prob_title, proc_title, prod_code,prod_title,invi_description,invi_unit1,invi_unit2,invi_unit3,invi_product_id AS product_id,prod_brand_id,prod_code, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS pro
LEFT JOIN invoice_item AS i ON pro.id = i.invi_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE invi_invoice_id =  $intInvoiceID AND invi_bonus = '1'
ORDER BY i.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function getLastProductPrice($intProductID, $intCount = 5) {
    $this->setQuery(
"SELECT invi_price, i.cdate, invo_customer_name
FROM invoice_item AS ii
LEFT JOIN invoice AS i ON i.id = ii.invi_invoice_id
WHERE ii.invi_product_id = {$intProductID}
ORDER BY i.cdate DESC, i.id DESC
LIMIT 0, {$intCount}");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function add($intInvoiceID,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice='0',$intDiscount1='0',$intDiscount2='0',$intDiscount3='0',$flagBonus='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM invoice WHERE id = $intInvoiceID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'invi_invoice_id' => $intInvoiceID,
		'invi_product_id' => $intProductID,
		'invi_description' => $strProductDescription,
		'invi_quantity1' => $intQuantity1,
        'invi_quantity2' => $intQuantity2,
        'invi_quantity3' => $intQuantity3,
        'invi_unit1' => $intUnit1,
        'invi_unit2' => $intUnit2,
        'invi_unit3' => $intUnit3,
		'invi_price' => $intPrice,
		'invi_discount1' => $intDiscount1,
        'invi_discount2' => $intDiscount2,
        'invi_discount3' => $intDiscount3,
        'invi_bonus' => $flagBonus,
		'cdate' => $strCDate
    ));
}

public function addBaru($intPurchaseInvoiceId, $ai_id, $bahan, $desc,$qty_bayar, $qty_pengkali, $harga_satuan, $sum_perbarang,$cby,$cdate){
    $this->initialize('purchase_invoice_item');
    return $this->dbInsert(array(
        'pinvit_purchase_invoice_id' => $intPurchaseInvoiceId, 
        'pinvit_acit_id' => $ai_id,
        'pinvit_product_id' => $bahan, 
        'pinvit_description' => $desc,
        'pinvit_quantity' => $qty_bayar, 
        'pinvit_quantity_pengkali' => $qty_pengkali, 
        'pinvit_price' => $harga_satuan, 
        'pinvit_subtotal' => $sum_perbarang,
        // 'pinvit_price' => str_replace(['.',','],['','.'],$harga_satuan), 
        // 'pinvit_subtotal' => str_replace(['.',','],['','.'],$sum_perbarang),
        'cby' => $cby,
        'cdate' => $cdate
    ));
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
		'invi_quantity1' => $intQuantity1,
        'invi_quantity2' => $intQuantity2,
        'invi_quantity3' => $intQuantity3,
		'invi_price' => $intPrice,
		'invi_discount1' => $intDiscount1,
        'invi_discount2' => $intDiscount2,
        'invi_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByInvoiceID($intID) {
	return $this->dbDelete("invi_invoice_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}

public function updateTerPI($ai_id, $terpi_baru){
    $_CI =& get_instance();
    $_CI->load->model('Macceptanceitem');
    $_CI->Macceptanceitem->dbUpdate(array(
        'acit_terpi' => $terpi_baru),
        "id = $ai_id");
  //   $this->initialize('acceptance_item');
  //   return $this->dbUpdate(array(
		// 'acit_terpi' => $terpi_baru),
		// "id = $ai_id");
}

public function getAllQtyPurchInv($id){
    $this->setQuery(
        "SELECT pinvit_quantity, pinvit_product_id, id
        FROM purchase_invoice_item 
        WHERE pinvit_purchase_invoice_id = $id");
        
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function updatePurchaseInvoiceItemQty($qty, $pinvitId, $new_subtotal){
    return $this->dbUpdate(array(
        'pinvit_quantity_pengkali' => $qty,
        'pinvit_subtotal' => $new_subtotal),
		"id = $pinvitId");
}

}

/* End of File */