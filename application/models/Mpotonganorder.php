<?php
/*
PUBLIC FUNCTION:
- getDynamicCode(strTableName,intID)
- getQuantityByProductID(intProductID,intWarehouseID)
- getHistoryByProductID(intProductID,intWarehouseID)

PRIVATE FUNCTION:
- 
*/

class Mpotonganorder extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
    $this->initialize('potongan_order');
}

public function add($intInvoiceID,$intQty,$strName,$intNominal) {
    return $this->dbInsert(array(
        'poor_invoice_id' => $intInvoiceID,
        'poor_item_qty' => $intQty,
        'poor_name' => $strName,
        'poor_nominal' => $intNominal
    ));
}
public function getAllPotonganByInvoiceID($intInvoiceID) {

    $this->setQuery(
'SELECT poor_item_qty, poor_name, poor_nominal
FROM potongan_order
WHERE poor_invoice_order_id= '.$intInvoiceID.'');
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteByInvoiceID($intID) {
    return $this->dbDelete("poor_invoice_order_id = $intID");
}

}

/* End of File */