<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Esubkontrakmaterial extends Eloquent 
{
    protected $table = 'subkontrak_material';
    public $timestamps = false;

    public function subkontrak()
    {
        return $this->belongsTo(Esubkontrak::class, 'subkontrak_id');
    }

    public function bahan()
    {
        return $this->belongsTo(Ebahan::class, 'material');
    }
}