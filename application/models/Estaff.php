<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Estaff extends Eloquent 
{
    protected $table = 'adm_login';
    public $timestamps = false;

    public function kontrak()
    {
        return $this->hasMany(Ekontrak::class);
    }

    public function subkontrak_team()
    {
        return $this->belongsTo(Esubkontrakteam::class, 'name', 'id');
    }
}