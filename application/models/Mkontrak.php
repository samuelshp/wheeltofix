<?php

class Mkontrak extends JW_Model {

public function __construct() {
    parent::__construct();

    $this->initialize('kontrak');
}

public function getKontrak()
{
    $this->dbSelect('','', 'kont_name');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountActiveItems() {
    $this->setQuery("SELECT id
FROM kontrak AS t
WHERE status = ".STATUS_APPROVED." AND latest_rev = 1");

    return $this->getNumRows();
}

public function getActiveItems($intStartNo,$intPerPage) {
    $this->setQuery("SELECT t.*, c.cust_name, sk.job_value
FROM kontrak AS t
LEFT JOIN jw_customer AS c ON c.id = t.owner_id
LEFT JOIN (
    SELECT kontrak_id, SUM(job_value) AS job_value
    FROM subkontrak
    GROUP BY kontrak_id
) AS sk ON sk.kontrak_id = t.id
WHERE status = ".STATUS_APPROVED." AND latest_rev = 1
LIMIT $intStartNo, $intPerPage");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
    $this->setQuery(
"SELECT t.*, c.cust_name, sk.job_value, kont_name
FROM kontrak AS t
LEFT JOIN jw_customer AS c ON c.id = t.owner_id
LEFT JOIN (
    SELECT kontrak_id, SUM(job_value) AS job_value
    FROM subkontrak
    GROUP BY kontrak_id
) AS sk ON sk.kontrak_id = t.id
WHERE t.id = $intID");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getItemByName($strName) {
    $this->dbSelect('', "kont_name = '$strName'");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function add($arrData) {
    if(!$this->getItemByName($arrData['kont_name'])) return $this->dbInsert($arrData);

    $this->session->set_flashdata('strMessage','Duplicate Nama Proyek, Error: already exist!.');
    return false;
}

public function edit($intID, $arrData) {
    if($arrCheckData = $this->getItemByName($arrData['kont_name']) && $arrCheckData['id'] != $intID) {
        $this->session->set_flashdata('strMessage','Duplicate Nama Proyek, Error: already exist!.');
        return false;
    }

    return $this->dbUpdate($arrData, "id = $intID");

}

public function getKontrakByPurchaseId($id_purchase){
    $this->setQuery(
        "SELECT sk.id, po.idSubKontrak, sk.job, js.supp_name
        FROM purchase as p
        LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
        LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
        LEFT JOIN jw_supplier as js ON js.id = p.prch_supplier_id
        WHERE p.id = $id_purchase"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getKontrakByCC($intAdmLoginID)
{
    $this->setQuery("
        SELECT kont.id, kont.kont_name, skm.job, skm_team.jabatan_id FROM kontrak AS kont
        RIGHT JOIN subkontrak AS skm ON kont.id = skm.kontrak_id
        JOIN subkontrak_team AS skm_team ON skm_team.subkontrak_id = skm.id
        WHERE ".$intAdmLoginID." = 2 OR (skm_team.jabatan_id = 11 AND skm_team.name = $intAdmLoginID)
        GROUP BY kont.id
        ORDER BY kont_name
    ");

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getKontrakByInProject($inProject)
{
    $this->setQuery("
        SELECT k.id, k.kont_name, k.owner_id, k.kont_date
        FROM kontrak k INNER JOIN  subkontrak sk ON k.id = sk.kontrak_id
        WHERE sk.id in (".$inProject.") AND sk.status = ".STATUS_APPROVED." AND k.status = ".STATUS_APPROVED."
        GROUP BY k.id ORDER BY kont_name");

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

}