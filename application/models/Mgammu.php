<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mgammu extends CI_Model {
# Private Variables

function Mgammu() {}

function sendSMS($nomorHP, $smsN){
	$smsN = $this->config->item('jw_website_name').': '.$smsN;
	
	$DB1 = $this->load->database('gammu',TRUE);
	
	// $nomorHP = 08484565156,1516546565,61516165165;
	# $nomorHandPhone memecah data dari $nomorHP dalam bentuk array nomor handphone
	$nomorHandPhone = explode(",",$nomorHP);

	$noHp = $nomorHandPhone;
	# menghitung jumlah nomor yang akan dikirimi SMS
	$juml = count($noHp);
	for($i=0;$i<$juml;$i++){
		$qryStatus = "SHOW TABLE STATUS LIKE 'outbox'";
		$hasilQery = $DB1->query($qryStatus);
		$dataQuery = $hasilQery->row_array(0);
		
		# $newForSMS digunakan untuk menyimpan ID yang terbaru dari Primary Key yang ada di table outbox
		# ID ini nanti yang menghubungkan antara table "outbox" dengan table "outbox_multipart"
		$newForSMS = $dataQuery['Auto_increment'];

		$hp = $noHp[$i];

		# jika panjang pesan SMS kurang dari sama dengan 153 karakter
		# dalam gammu 153 karakter = 1 SMS
		if(strlen($smsN)<=153){
			 $DB1->query("INSERT INTO outbox (DestinationNumber, TextDecoded) VALUES ('".$hp."','".$smsN."')");
		}else{
			# pembulatan keatas berapa total SMS yang akan dikirimkan nantinya
			$jmlSMS = ceil(strlen($smsN)/153);
			# memecah pesan SMS per 153 karakter
			$pecah = str_split($smsN, 153);

			for($j=1;$j<=$jmlSMS;$j++){
				$udh = "050003A7".sprintf("%02s", $jmlSMS).sprintf("%02s", $j);
				$msg = $pecah[$j-1];
				if ($j==1){
					# pesan sms yang sudah dipecah per 153 karakter tadi
					# pecahan pertama disimpan dalam tabel outbox
					$querySend = "INSERT INTO outbox (DestinationNumber, UDH, TextDecoded, ID, MultiPart)
								  VALUES ('".$hp."', '$udh', '".$msg."', '$newForSMS', 'true')";
				}else{
					# pecahan selanjutnya disimpan pada tabel outbox_multipart
					$querySend = "INSERT INTO outbox_multipart(UDH, TextDecoded, ID, SequencePosition)
								  VALUES ('$udh', '".$msg."', '$newForSMS', '$j')";
				}
				$DB1->query($querySend);
			}
		}
	}
	
	$DB1->close();
}

}
# END Table class