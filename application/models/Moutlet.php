<?php
/*
PUBLIC FUNCTION:
- getAllOutlet(strKeyword)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Moutlet extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('jw_outlet');
}

public function getAllOutlet($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (outl_title LIKE '%$strKeyword%')";
	} else $strWhere = '';

	$this->setQuery(
"SELECT id, outl_code, outl_title, outl_address, outl_phone, outl_markettype
FROM jw_outlet
WHERE outl_status > 1$strWhere
ORDER BY outl_title ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAllData() {
	$this->dbSelect('id,outl_title,outl_address',"id > 0 AND outl_status > 1",'outl_name ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,outl_title,outl_address',"id = $intID");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */