<?php
/*
PUBLIC FUNCTION:
- getItemsByInvoiceID(intInvoiceID,strMode)
- getBonusItemsByInvoiceID(intInvoiceID,strMode)
- add(intInvoiceID,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3, intUnit1,intUnit2,intUnit3,intPrice,intDiscount1,intDiscount2,intDiscount3,flagBonus)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,intDiscount1,intDiscount2,intDiscount3)
- deleteByInvoiceID(intID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Minvoiceorderitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	
	$this->initialize('invoice_order_item');
}
public function UpdateSelledItem($intPOID,$intProductID,$intSelled1,$intSelled2,$intSelled3,$intBonus) {

    $this->setQuery(
"    UPDATE invoice_order_item
    SET inoi_selled1 = inoi_selled1 + $intSelled1,
     inoi_selled2 = inoi_selled2 + $intSelled2,
     inoi_selled3 = inoi_selled3 + $intSelled3
     WHERE
     inoi_invoice_order_id= $intPOID AND
     inoi_product_id =$intProductID AND
     inoi_bonus =$intBonus"
    );

    return true;

}

public function UpdateSelledItemVIew($ID,$idItem,$intDif1,$intDif2,$intDif3,$intBonus) {

    $this->setQuery(
"    UPDATE invoice_order_item
    SET inoi_selled1 = inoi_selled1 + $intDif1,
     inoi_selled2 = inoi_selled2 + $intDif2,
     inoi_selled3 = inoi_selled3 + $intDif3
     WHERE
     inoi_invoice_order_id= '$ID' AND inoi_product_id= '$idItem' AND inoi_bonus =$intBonus"
    );
    return true;

}
public function getLatestPriceByCustomer($intProductID,$intCustomerID) {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,inoi_quantity,inoi_price,inoi_discount',"inoi_invoice_order_id = $intInvoiceID");
    else $this->dbSelect('',"inoi_invoice_order_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, inoi_price,inoi_discount1,inoi_discount2,inoi_discount3
FROM invoice_order_item AS i
LEFT JOIN invoice_order AS vo ON vo.id = i.inoi_invoice_order_id
WHERE inor_customer_id =  $intCustomerID AND inoi_product_id = $intProductID AND inoi_bonus = '0'
ORDER BY i.cdate DESC, i.id DESC");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return array();
}

public function getItemsByInvoiceID($intInvoiceID,$strMode = 'ALL') {
	/*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,inoi_quantity,inoi_price,inoi_discount',"inoi_invoice_order_id = $intInvoiceID");
	else $this->dbSelect('',"inoi_invoice_order_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, inoi_selled1, inoi_selled2, inoi_selled3, inoi_quantity1, inoi_quantity2, inoi_quantity3, inoi_price, inoi_discount1, inoi_discount2, inoi_discount3, inoi_product_id, prob_title, proc_title, prod_code,prod_title,inoi_description,inoi_unit1,inoi_unit2,inoi_unit3,inoi_product_id AS product_id,prod_brand_id,prod_hpp,inoi_subtotal, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS pro
LEFT JOIN invoice_order_item AS i ON pro.id = i.inoi_product_id
LEFT JOIN invoice_order AS vo ON vo.id = i.inoi_invoice_order_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE inoi_invoice_order_id =  $intInvoiceID AND inoi_bonus = '0'
ORDER BY i.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return array();
}

public function getBonusItemsByInvoiceID($intInvoiceID,$strMode = 'ALL') {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,inoi_quantity,inoi_price,inoi_discount',"inoi_invoice_order_id = $intInvoiceID");
    else $this->dbSelect('',"inoi_invoice_order_id = $intInvoiceID");*/

    $this->setQuery(
"SELECT i.id, inoi_selled1, inoi_selled2, inoi_selled3, inoi_quantity1, inoi_quantity2, inoi_quantity3, inoi_price, inoi_discount1, inoi_discount2, inoi_discount3, inoi_product_id, prob_title, proc_title, prod_code,prod_title,inoi_description,inoi_unit1,inoi_unit2,inoi_unit3,inoi_product_id AS product_id,prod_brand_id,prod_code, prod_conv1, prod_conv2, prod_conv3
FROM jw_product AS pro
LEFT JOIN invoice_order_item AS i ON pro.id = i.inoi_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE inoi_invoice_order_id =  $intInvoiceID AND inoi_bonus = '1'
ORDER BY i.id ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return array();
}

public function add($intInvoiceID,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice='0',$intDiscount1='0',$intDiscount2='0',$intDiscount3='0',$flagBonus='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM invoice_order WHERE id = $intInvoiceID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'inoi_invoice_order_id' => $intInvoiceID,
		'inoi_product_id' => $intProductID,
		'inoi_description' => $strProductDescription,
		'inoi_quantity1' => $intQuantity1,
        'inoi_quantity2' => $intQuantity2,
        'inoi_quantity3' => $intQuantity3,
        'inoi_unit1' => $intUnit1,
        'inoi_unit2' => $intUnit2,
        'inoi_unit3' => $intUnit3,
		'inoi_price' => $intPrice,
		'inoi_discount1' => $intDiscount1,
        'inoi_discount2' => $intDiscount2,
        'inoi_discount3' => $intDiscount3,
        'inoi_bonus' => $flagBonus,
		'cdate' => $strCDate
    ));
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
		'inoi_quantity1' => $intQuantity1,
        'inoi_quantity2' => $intQuantity2,
        'inoi_quantity3' => $intQuantity3,
		'inoi_price' => $intPrice,
		'inoi_discount1' => $intDiscount1,
        'inoi_discount2' => $intDiscount2,
        'inoi_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByInvoiceID($intID) {
	return $this->dbDelete("inoi_invoice_order_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}
}

/* End of File */