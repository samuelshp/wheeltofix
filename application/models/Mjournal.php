<?php

class Mjournal extends JW_Model
{
	function __construct()
	{
		parent::__construct();
	}

	// POST RHLAPORANHUTANG
	public function insertHutangPI($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, nomorkontrak)
			SELECT  pinv_supplier_id, 'PI' Jenis, cdate, id, pinv_code, cdate, NULL, ROUND((pinv_subtotal - pinv_discount + pinv_costadd + (pinv_tax/100 * (pinv_subtotal - pinv_discount - pinv_dp))),2) calc, b.idkontrak
			FROM purchase_invoice a 
			LEFT JOIN 
			(
				SELECT pi.id nomorid, po.idkontrak 
				FROM purchase_invoice AS PI
				LEFT JOIN acceptance_item AS ai ON ai.acit_acceptance_id = pi.pinv_acceptance_id
				LEFT JOIN purchase AS p ON p.id = pi.pinv_purchase_id
				LEFT JOIN purchase_order AS po ON po.id = p.prch_po_id
				GROUP BY pi.id
			) b ON a.id = b.nomorid
			WHERE a.id = $intID");
		return true;
	}

	public function insertHutangUMSPI($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT a.pinv_supplier_id, 'UMS-PI' Jenis, a.cdate, d.id, d.dpay_code, a.cdate, NULL, ROUND(pinv_dp + (pinv_tax/100*(pinv_dp)),2), a.id, a.pinv_code, d.dpay_kontrak_id
			FROM purchase_invoice a
			inner JOIN purchase_invoice_history c ON a.id = pinvhis_pinv_id
			inner JOIN downpayment d ON c.pinvhis_dp = d.id
			WHERE a.id = $intID");
		return true;
	}

	public function insertHutangNKS($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT b.txod_supp_id, 'NKS', a.txou_date, a.id, txou_code, a.txou_date, txou_jatuhtempo, txod_totalbersih, 0, '', txod_ref_id
			FROM  transaction_out a 
			INNER JOIN transaction_out_detail b ON a.id = b.txod_txou_id 
			WHERE a.id = $intID");
		return true;
	}


	public function insertHutangUMS($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT e.dpay_supplier_id, 'UMS', a.txou_date, e.id, e.dpay_code, a.txou_date, a.txou_date, - dphi.dphi_amount_rencanabayar , a.id, a.txou_code, e.dpay_kontrak_id
			FROM transaction_out a 
			INNER JOIN transaction_out_detail b ON a.id = b.txod_txou_id
			INNER JOIN daftar_pembayaran_hutang dph ON b.txod_ref_id = dph.id
			INNER JOIN daftar_pembayaran_hutang_item dphi ON dph.id = dphi.dphi_dphu_id 
			INNER JOIN downpayment e ON dphi.dphi_reff_id = e.id
			WHERE dphi.id = $intID");
		return true;
	}

	public function insertHutangPIL($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT b.txod_supp_id, 'PIL', a.txou_date, e.id, e.pinv_code, a.txou_code, a.txou_date, - dphi.dphi_amount_rencanabayar,  b.id, a.txou_code, z.idkontrak
			FROM transaction_out a 
			INNER JOIN transaction_out_detail b ON a.id = b.txod_txou_id
			INNER JOIN daftar_pembayaran_hutang dph ON b.txod_ref_id = dph.id
			INNER JOIN daftar_pembayaran_hutang_item dphi ON dph.id = dphi.dphi_dphu_id 
			INNER JOIN purchase_invoice e ON dphi.dphi_reff_id = e.id
			LEFT JOIN 
			(
				SELECT pi.id nomorid, po.idkontrak 
				FROM purchase_invoice AS PI
				LEFT JOIN acceptance_item AS ai ON ai.acit_acceptance_id = pi.pinv_acceptance_id
				LEFT JOIN purchase AS p ON p.id = pi.pinv_purchase_id
				LEFT JOIN purchase_order AS po ON po.id = p.prch_po_id
				GROUP BY pi.id
			) z ON e.id = z.nomorid
			WHERE dphi.id = $intID");
		return true;
	}

	public function insertHutangNKSL($intID) {
		$this->setQuery("INSERT INTO rhlaporanhutang (nomormhsupplier, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT b.txod_supp_id, 'NKSL',  a.txou_date, e.id, a.txou_code, a.txou_date, a.txou_date, - dphi.dphi_amount_rencanabayar ,  b.id, a.txou_code, f.txod_ref_id
			FROM transaction_out a 
			INNER JOIN transaction_out_detail b ON a.id = b.txod_txou_id
			INNER JOIN daftar_pembayaran_hutang dph ON b.txod_ref_id = dph.id
			INNER JOIN daftar_pembayaran_hutang_item dphi ON dph.id = dphi.dphi_dphu_id 
			INNER JOIN transaction_out e ON dphi.dphi_reff_id = e.id
			INNER JOIN transaction_out_detail f ON e.id = f.txod_txou_id
			WHERE dphi.id = $intID");
		return true;
	}

	// POST RHLAPORANPIUTANG
	public function insertPiutangINV($intID) {
		$this->setQuery("INSERT INTO rhlaporanpiutang (nomormhcustomer, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
		SELECT c.owner_id, 'INV', a.invo_jatuhtempo, a.id, a.invo_code, a.invo_jatuhtempo, a.invo_jatuhtempo, a.invo_grandtotal, 0, '', c.id
		FROM  invoice a 
		INNER JOIN subkontrak b ON a.invo_subkontrak_id = b.id
		INNER JOIN kontrak c ON b.kontrak_id = c.id
		WHERE a.id = $intID");
		return true;
	}

	public function insertPiutangUMC($intID) {
		$this->setQuery("INSERT INTO rhlaporanpiutang (nomormhcustomer, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode)
			SELECT a.umcu_cust_id, 'UMC', a.umcu_date, a.id, a.umcu_code, a.umcu_date, a.umcu_date, -umcu_amount, 0, ''
			FROM uang_masuk_customer a 
			WHERE a.id = $intID");
		return true;
	}

	public function insertPiutangINVP($intID) {
		$this->setQuery("INSERT INTO rhlaporanpiutang (nomormhcustomer, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode, nomorkontrak)
			SELECT a.txin_cust_id, 'INVP', txin_date, c.id, c.invo_code, c.invo_jatuhtempo, c.invo_jatuhtempo, -d.invp_umcu_amount_used, a.id, a.txin_code, sk.kontrak_id
			FROM transaction_in a INNER JOIN transaction_in_detail b ON a.id = b.txid_txin_id 
			INNER JOIN invoice c ON b.txid_ref_id = c.id
			INNER JOIN invoice_paid d ON a.id = invp_txin_id AND c.id = invp_invo_id
			INNER JOIN uang_masuk_customer e ON d.invp_umcu_id = e.id
			INNER JOIN subkontrak sk ON c.invo_subkontrak_id = sk.id
			WHERE d.id = $intID");
		return true;
	}

	public function insertPiutangUMCP($intID) {
		$this->setQuery("INSERT INTO rhlaporanpiutang (nomormhcustomer, jenis, tanggal, transaksi_nomor, transaksi_kode, transaksi_tanggal, jatuh_tempo, total, pelunasan_nomor, pelunasan_kode)
			SELECT a.txin_cust_id, 'UMCP', txin_date, e.id, e.umcu_code,e.umcu_date, e.umcu_date, d.invp_umcu_amount_used, a.id, a.txin_code
			FROM transaction_in a
			INNER JOIN transaction_in_detail b ON a.id = b.txid_txin_id 
			INNER JOIN invoice c ON b.txid_ref_id = c.id
			INNER JOIN invoice_paid d ON a.id = invp_txin_id AND c.id = invp_invo_id
			INNER JOIN uang_masuk_customer e ON d.invp_umcu_id = e.id
			WHERE d.id = $intID");
		return true;
	}

	public function resetRHLaporan() {
		$this->setQuery("CALL insert_lap_hutang_piutang_20190618");
		return true;
	}

	public function postTransaction($transaction,$date) {
		$this->setQuery("CALL sp_post_".$transaction."(".formatDate2($date,'m').",".formatDate2($date,'Y').")");
		return true;
	}

	public function insertJournalMemorial($arrDataJournal)
	{

		$this->initialize('jurnal');
		
		$strCode = generateTransactionCode($arrDataJournal['jrnl_date'], '', 'jurnal_memorial');

		foreach ($arrDataJournal['jrnl_debet'] as $key => $value){
			$intDbInsert = $this->dbInsert(
				array(
					'jrnl_code' => $strCode,
					'jrnl_date' => str_replace("/", "-", $arrDataJournal['jrnl_date']),
					'jrnl_acco_id' => $arrDataJournal['jrnl_acco_id'][$key],
					'jrnl_ref_id' => $arrDataJournal['jrnl_ref_id'],					
					'jrnl_keterangan' => $arrDataJournal['jrnl_keterangan'][$key],
					'jrnl_debet' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_debet'][$key])),
					'jrnl_kredit' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_kredit'][$key])),
				)
			);			

			if (!$intDbInsert) return false;
		}

		return $intDbInsert;
	}

	public function insertJournalPenyesuaian($arrDataJournal)
	{

		$this->initialize('jurnal');
		
		$strCode = generateTransactionCode($arrDataJournal['jrnl_date'], '', 'jurnal_penyesuaian');

		foreach ($arrDataJournal['jrnl_debet'] as $key => $value){
			$intDbInsert = $this->dbInsert(
				array(
					'jrnl_code' => $strCode,
					'jrnl_date' => str_replace("/", "-", $arrDataJournal['jrnl_date']),
					'jrnl_acco_id' => $arrDataJournal['jrnl_acco_id'][$key],
					'jrnl_ref_id' => $arrDataJournal['jrnl_ref_id'],
					'jrnl_keterangan' => $arrDataJournal['jrnl_keterangan'][$key],
					'jrnl_debet' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_debet'][$key])),
					'jrnl_kredit' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_kredit'][$key])),
				)
			);			

			if (!$intDbInsert) return false;
		}

		return $intDbInsert;
	}

	public function insertJournalPersediaanAkhirTahun($arrDataJournal)
	{
		$this->initialize('jurnal');
		
		$strCode = generateTransactionCode($arrDataJournal['jrnl_date'], '', 'jurnal_persediaan_akhir_tahun');
		
		$intDbInsert = $this->dbInsert(
			array(
				'jrnl_code' => $strCode,
				'jrnl_date' => str_replace("/", "-", $arrDataJournal['jrnl_date']),
				'jrnl_type' => "PERSEDIAAN AKHIR TAHUN",
				'jrnl_ref_id' => $arrDataJournal['strProyek'],
				'jrnl_debet' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['intAmount'])),
			));

		if (!$intDbInsert) return false;		

		return $intDbInsert;
	}

	public function update($arrDataJournal, $intID)
	{
		$this->setQuery("SELECT id FROM jurnal WHERE jrnl_code = (SELECT jrnl_code FROM jurnal WHERE id = $intID) ");
		$otherID = $this->getQueryResult('Array');
		$this->initialize('jurnal');
		foreach ($arrDataJournal['jrnl_debet'] as $key => $value){
 			$intDbUpdate = $this->dbUpdate(
				array(					
					'jrnl_date' => str_replace("/", "-", $arrDataJournal['jrnl_date']),
					'jrnl_acco_id' => $arrDataJournal['jrnl_acco_id'][$key],
					'jrnl_ref_id' => $arrDataJournal['jrnl_ref_id'],
					'jrnl_keterangan' => $arrDataJournal['jrnl_keterangan'][$key],
					'jrnl_debet' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_debet'][$key])),
					'jrnl_kredit' => str_replace(",", ".", str_replace(".", "", $arrDataJournal['jrnl_kredit'][$key])),
				), "id = ".$otherID[$key]['id']
			);
		}

		return $intDbUpdate;
	}

	public function getAllJournal($strType, $intStartNo = -1, $intPerPage = -1)
    {    	    	
    	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "jrnl_date DESC, id DESC";
        else $strOrderBy = "jrnl_date DESC, id DESC LIMIT $intStartNo, $intPerPage";
        $join = ''; $strSelect = ''; $strGroupBy = "jrnl_code";
        switch ($strType) {
        	case 'PERSEDIAAN AKHIR TAHUN':
        		$join .= "LEFT JOIN kontrak ON t.jrnl_ref_id = kontrak.id";
        		$strSelect .= ", kontrak.id as kont_id, kont_name";
        		break;
        }

        $this->setQuery(
            "SELECT t.* $strSelect FROM jurnal as t $join WHERE jrnl_code LIKE '%$strType%' AND jrnl_status != ".STATUS_DELETED." GROUP BY $strGroupBy ORDER BY $strOrderBy"
        );
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getCount($strType)
    {
    	$this->setQuery( "SELECT * FROM jurnal WHERE jrnl_code LIKE '%$strType%' AND jrnl_status != ".STATUS_DELETED." GROUP BY jrnl_code");
        return $this->getNumRows();
    }

    public function getByID($intID)
    {
    	$this->initialize('jurnal');
    	$this->dbSelect("jrnl_code", "id = $intID");
    	$jrnl_code = $this->getNextRecord('Array')['jrnl_code'];
    	
    	$this->setQuery("SELECT * FROM jurnal WHERE jrnl_code = '$jrnl_code'");

    	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getByProjectID($intID)
    {
    	$this->setQuery("SELECT t.*, kont_name FROM jurnal as t LEFT JOIN kontrak ON t.jrnl_ref_id = kontrak.id WHERE jrnl_ref_id IS NOT NULL AND kontrak.id = $intID");

    	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function deleteJournalMemorial($strCode)
    {
    	$this->initialize('jurnal');    	
    	return $this->dbUpdate(array('jrnl_status' => STATUS_DELETED),"jrnl_code = '$strCode'");
    }

    public function searchBy($type, $arrSearch)
    {
    	$strWhere = "";
    	foreach ($arrSearch as $key => $value) {
    		if ($value != ''){
	    		if(strpos($key, "date")) {
	    			$strOperand = "=";
	    			$strLikeChar = null;
	    		}else{
	    			$strOperand = "LIKE";
	    			$strLikeChar = "%";
	    		}
	    		$strWhere .= " AND ".$key." ".$strOperand." '$strLikeChar".$value."$strLikeChar'";
    		}
    	}

    	$this->setQuery("SELECT * FROM jurnal WHERE jrnl_code LIKE '%$strType%' AND jrnl_status != '".STATUS_DELETED."' $strWhere GROUP BY jrnl_code ORDER BY jrnl_date");

    	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    	else return false;
    }

    public function postingTransaksi($arrData)
    {    	    	
    	return $this->setQuery("CALL sp_post_all('".$arrData['periode']."','".$arrData['tahun']."')");    	
    }
}