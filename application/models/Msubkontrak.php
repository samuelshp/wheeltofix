<?php

class Msubkontrak extends JW_Model {

// Constructor
public function __construct() { 
    parent::__construct();
    $this->initialize('subkontrak');
}

public function getCountActiveItems($intKontrakID = 0) {
    $strWhere = '';
    if(!empty($intKontrakID)) $strWhere .= " AND subkontrak.id = $intKontrakID";
    if(!empty($strKeyword)) $strWhere .= " AND (subkontrak.job LIKE '%$strKeyword%' OR kontrak.kont_name LIKE '%$strKeyword%')";
    else $strWhere = '';
    $this->setQuery("SELECT subkontrak.id
FROM subkontrak
LEFT JOIN kontrak ON subkontrak.kontrak_id = kontrak.id
WHERE subkontrak.status > 1$strWhere");

    return $this->getNumRows();
}

public function getActiveItems($intKontrakID = 0, $strKeyword = '', $intStartNo = 0, $intPerPage = 0) {
    $strWhere = '';
    if(!empty($intKontrakID)) $strWhere .= " AND subkontrak.id = $intKontrakID";
    if(!empty($strKeyword)) $strWhere .= " AND (subkontrak.job LIKE '%$strKeyword%' OR kontrak.kont_name LIKE '%$strKeyword%')";
    $this->setQuery("SELECT subkontrak.id, subkont_kode, subkontrak.kont_code, subkont_date, job, hpp, job_value, kont_name, kont_date, due_date, skmaterial.subkontrak_terpb, sktermin.due_date, login.adlg_name
FROM subkontrak
LEFT JOIN kontrak ON subkontrak.kontrak_id = kontrak.id
LEFT JOIN (
    SELECT subkontrak_id, SUM(subkontrak_terpb) AS subkontrak_terpb
    FROM subkontrak_material
    GROUP BY subkontrak_id
) skmaterial ON skmaterial.subkontrak_id = subkontrak.id
LEFT JOIN (
    SELECT subkontrak_id, due_date
    FROM subkontrak_termin
    WHERE status > 1
    GROUP BY subkontrak_id
    ORDER BY due_date ASC
) sktermin ON sktermin.subkontrak_id = subkontrak.id
LEFT JOIN (
    SELECT subkontrak_id, name
    FROM subkontrak_team
    WHERE jabatan_id = 9 AND status > 1
    GROUP BY subkontrak_id
    ORDER BY id ASC
) skteam ON skteam.subkontrak_id = subkontrak.id
LEFT JOIN (
    SELECT id, adlg_name
    FROM adm_login
) login ON login.id = skteam.name
WHERE subkontrak.status > 1$strWhere
ORDER BY kont_name ASC, subkontrak.id DESC
LIMIT $intStartNo, $intPerPage");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSubkontrak($id) {
    $this->setQuery("SELECT 
    subkontrak.id as subkont_id, subkontrak.kontrak_id, subkontrak.kont_code, subkontrak.job, subkontrak.hpp,  subkontrak.job_value, subkontrak.subkont_date, subkontrak.status, subkontrak.subkont_kode,
    kontrak.*, jw_customer.*
FROM subkontrak 
JOIN kontrak ON subkontrak.kontrak_id = kontrak.id
JOIN jw_customer ON kontrak.owner_id = jw_customer.id
WHERE subkontrak.id = $id");

    if($this->getNumRows() > 0)
        return $this->getNextRecord('Array');
    else
        return false;
}

public function getSubkontrakByKontrakID($id) {
    $this->setQuery("SELECT 
    subkontrak.id as subkont_id, subkontrak.kontrak_id, subkontrak.kont_code, subkontrak.job, subkontrak.hpp,  subkontrak.job_value, subkontrak.subkont_date, subkontrak.status, subkontrak.subkont_kode,
    kontrak.*, jw_customer.*
FROM subkontrak 
JOIN kontrak ON subkontrak.kontrak_id = kontrak.id
JOIN jw_customer ON kontrak.owner_id = jw_customer.id
WHERE kontrak.id = $id");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
    $this->setQuery('SELECT * FROM subkontrak WHERE id = '.$intID);

    if($this->getNumRows() > 0)
        return $this->getNextRecord('Array');
    else
        return false;
}

public function getSubkontrakMaterial($intID)
{
    $this->setQuery("SELECT prod.id, sk_m.id as id_sk, sk_m.qty, hpp, jumlah, sk_m.subkontrak_terpb, unit.unit_title, sk_m.keterangan, sk_m.substitusi
        FROM subkontrak_material as sk_m
    LEFT JOIN jw_product as prod
    ON sk_m.material = prod.id
    LEFT JOIN jw_unit as unit
    ON prod.satuan_bayar_id = unit.id
    WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSubkontrakNonMaterial($intID)
{
    $this->setQuery("SELECT * FROM subkontrak_nonmaterial WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSubkontrakHPPMaterial($intID)
{
    $this->setQuery("SELECT SUM(`jumlah`) as hpp_material FROM subkontrak_material WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getSubkontrakHPPNonMaterial($intID)
{
    $this->setQuery("SELECT SUM(`amount`) as hpp_nonmaterial FROM subkontrak_nonmaterial WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getSubkontrakTermin($intID)
{
    $this->setQuery("SELECT * FROM subkontrak_termin WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSubkontrakTeam($intID)
{
    $this->setQuery(
        "SELECT lg.id, lg.adlg_name, t.jabatan_id,j.tiny_data1 , t.id as rowID
        FROM subkontrak_team as t 
        LEFT JOIN adm_login as lg
        ON t.name = lg.id
        LEFT JOIN (SELECT tiny_key, tiny_data1 FROM tinydb WHERE tiny_category = 'admin_priviledge' ) as j
        ON t.jabatan_id = j.tiny_key
        WHERE subkontrak_id = $intID AND status = 2");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAvailableSubstitusi($intID)
{
    $this->setQuery(
        "SELECT subkontrak_material.id, jw_product.prod_title, jw_product.id as prod_id
        FROM subkontrak_material
        JOIN jw_product ON subkontrak_material.material = jw_product.id
        WHERE subkontrak_material.status != 1
            AND subkontrak_material.subkontrak_id = $intID
        ORDER BY jw_product.prod_title"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function add($arrSubkontrak)
{
    return $this->dbInsert(
        array(
            'kontrak_id' => $arrSubkontrak['intKontrakID'],            
            'job'   => $arrSubkontrak['strJob'],
            'hpp'   => $arrSubkontrak['intHPPTotal'],
            'job_value' => $arrSubkontrak['intJobValue'],            
            'subkont_date' => formatDate2(str_replace('/','-',$arrSubkontrak['strSubkontDate']),'Y-m-d'),
            'subkont_kode' => $arrSubkontrak['strSubkontKode']
        )
    );
}

public function updateSubKontrak($intKontrakID, $strJob, $intJobValue, $subkont_date, $subkont_kode, $id)
{
    return $this->dbUpdate(
        array(
            'kontrak_id' => $intKontrakID,
            'job' => $strJob,
            'job_value' => $intJobValue,
            'subkont_date' => $subkont_date, 
            'subkont_kode' => $subkont_kode
        ),
        "id = '$id'"
    );
}

public function getCopyTeam($intIDSubkontrak) {
    $this->setQuery("SELECT *
        FROM subkontrak_team skt
        LEFT JOIN subkontrak sk ON sk.id = skt.subkontrak_id
        WHERE sk.kontrak_id = (SELECT kontrak_id from subkontrak where id = $intIDSubkontrak)
        GROUP BY skt.name, skt.jabatan_id");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function checkTeam($intIDSubkontrak, $idTeam, $idJabatan) {
    $this->setQuery("SELECT * from subkontrak_team where subkontrak_id = $intIDSubkontrak AND name = $idTeam AND jabatan_id =  $idJabatan AND status = 2");

    return $this->getNumRows();
}
public function saveTeam($intIDSubkontrak, $idTeam, $idJabatan) {
    $this->setQuery("INSERT INTO subkontrak_team(subkontrak_id,name,jabatan_id,status) VALUES ($intIDSubkontrak,$idTeam,$idJabatan,2)");
}


}