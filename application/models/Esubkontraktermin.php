<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Esubkontraktermin extends Eloquent 
{
    protected $table = 'subkontrak_termin';
    public $timestamps = false;

    public function subkontrak()
    {
        return $this->belongsTo(Esubkontrak::class);
    }
}