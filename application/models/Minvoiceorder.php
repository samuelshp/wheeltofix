<?php
/*
PUBLIC FUNCTION:
- getTaxInvoiceByID(intID)
- getAllActiveInvoice(strKeyword)
- getAllActiveInvoicePerSalesman(intSalesmanID,strKeyword)
- getAllActiveInvoiceAndUnselect(strKeyword,strUnselect)
- getAllActiveInvoiceAndUnselectPerSalesman(intSalesmanID,strKeyword,strUnselect)
- getAllInvoice(strKeyword)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsByCustomerID(intID,strDate)
- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
- getItemsForFinance()
- getAccountReceivableItems()
- getItemByID(intID)
- getPrintDataByID(intID)
- getUserHistory(intUserID,intPerPage)
- getCount()
- getDateMark(strFrom,strTo)
- getCustomers(strFrom,strTo)
- getStockSold(intProductID,intWarehouseID,strLastStockUpdate)
- getHeaderBySaleOrderID(intSaleOrderID)
- getQuantityBySaleOrderID(intSaleOrderID,intProductID)
- searchByCustomer(strCustomerName)
- add(strInvoiceSOID,intOutletID,intCustomerID,intSalesmanID,intSuppID,intWarehouseID,strDescription,intTax,intStatus, strDate,intDisc,intTipeBayar,strJatuhTempo,intJualKanvas)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intDiscount,intStatus,intTax)
- editStatusByID(intID,intStatus)
- editAfterPrint(intID)
- deleteByID(intID)
- getCustomerCredit(intID)
- getFakturBelumLunas()

PRIVATE FUNCTION:
- __construct()	
*/

class Minvoiceorder extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('invoice_order');
}

public function getGrandTotalInvoiceByID($intID) {
    $this->setQuery(
'SELECT inor_grandtotal
FROM invoice_order AS i
WHERE id ='.$intID.'');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getTaxInvoiceByID($intID) {
    $this->setQuery(
'SELECT inor_tax
FROM invoice_order AS i
WHERE id ='.$intID.'');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getDataAutoComplete($strKeyword = '') {
    $strKeyword = urldecode($strKeyword);

    $this->setQuery(
"SELECT p.id,p.cdate,cust_name,cust_address,cust_city,cust_phone,inor_tax,inor_description,inor_customer_id,inor_code,inor_discount,w.id as ware_id,ware_name,s.id as cust_id,cust_outlettype,cust_limitkredit,inor_salesman_id,sals_name,sals_code
FROM invoice_order AS p
LEFT JOIN jw_salesman AS sl ON inor_salesman_id = sl.id
LEFT JOIN jw_customer AS s ON inor_customer_id = s.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
WHERE (p.inor_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR p.inor_description LIKE '%$strKeyword%')  AND p.inor_status IN (2,3)");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
public function getInvoiceDataForPrintByArray($strArrayID){
    $this->setQuery(
'SELECT i.id,inor_code,cust_name,inor_status,i.cdate,cust_address,cust_city,inor_exp_date,inor_grandtotal,
    inor_jualkanvas
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_salesman AS s ON inor_salesman_id = s.id
WHERE  i.id IN('.$strArrayID.')
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

    


public function getAllInvoice($strKeyword = '') {
    if(!empty($strKeyword)) {
		$strWhere = " AND (inor_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,inor_code,cust_name,sals_name,inor_status,i.cdate,cust_address,cust_city, inor_payment, inor_exp_date, inor_jualkanvas
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_salesman AS s ON inor_salesman_id = s.id
WHERE inor_status IN (0,2)'.$strWhere.'
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, inor_status, cust_name, cust_address, cust_city, cust_phone, ware_name, inor_code,inor_discount, inor_payment, inor_exp_date, inor_jualkanvas,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
ORDER BY i.cdate DESC, i.id DESC LIMIT $intStartNo, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDate($strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, inor_status, cust_name, cust_address, cust_city, cust_phone, ware_name, inor_code,inor_discount, inor_payment, inor_exp_date, inor_jualkanvas
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
WHERE DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByCustomerID($intID,$strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, inor_status, cust_name, cust_address, cust_city, cust_phone, ware_name, inor_code,inor_discount, inor_payment, inor_exp_date, inor_jualkanvas
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
WHERE inor_customer_id = $intID AND DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
	$this->setQuery(
"SELECT ii.id, cust_name, cust_address, cust_city, i.cdate AS inor_date, inor_status, inoi_quantity1
FROM invoice_order AS i
INNER JOIN invoice_order_item AS ii ON inoi_invoice_order_id = i.id
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
WHERE inoi_product_id = $intProductID AND inor_warehouse_id = $intWarehouseID AND i.cdate > '$strLastStockUpdate'
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForFinance() {
	$this->setQuery(
"SELECT i.id,cust_name AS name,i.cdate AS date
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
WHERE inor_status NOT IN (1,3) AND i.id NOT IN (
	SELECT fina_transaction_id FROM finance WHERE fina_type = 1
)
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAccountReceivableItems() {
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, cust_name, cust_address, cust_city, cust_phone, ware_name
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
WHERE inor_status IN (0,2)
ORDER BY i.cdate DESC, i.id DESC");
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	if($intID == '') $strWhere = "ORDER BY i.cdate DESC, i.id DESC LIMIT 0,1";
	else $strWhere = "
WHERE i.id = $intID
ORDER BY i.cdate DESC, i.id DESC";

    $this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, inor_description, inor_progress, inor_status, cust_name, cust_address, cust_city, cust_phone, ware_name, inor_code, sals_code, sals_name,inor_discount, inor_payment, inor_exp_date, inor_customer_id as cust_id,cust_outlettype,inor_supplier_id,w.id AS ware_id,cust_markettype,cust_internal,inor_jualkanvas,cust_limitkredit,inor_grandtotal,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp,inor_customer_id
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
LEFT JOIN jw_salesman AS sl on inor_salesman_id = sl.id
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}
    
public function getPrintDataByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY i.cdate DESC, i.id DESC LIMIT 0,1";
    else $strWhere = "WHERE i.id = $intID";
	
	$this->setQuery(
"SELECT inor_exp_date,inor_jualkanvas,inor_payment, sals_name,sals_code, ware_name, i.cdate AS inor_date, kota_title,kcmt_title
FROM invoice_order AS i
LEFT JOIN (
    SELECT cc.id,kota_title,kcmt_title
    FROM jw_customer AS cc
    LEFT JOIN jw_kota AS ck ON cc.cust_kota_id = ck.id
    LEFT JOIN jw_kecamatan AS ckc ON cc.cust_kecamatan_id = ckc.id
    ) AS c ON inor_customer_id = c.id
LEFT JOIN jw_salesman AS s ON inor_salesman_id = s.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_status, cust_name, cust_address, cust_city,inor_code
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
WHERE (i.cby = $intUserID OR i.mby = $intUserID)
ORDER BY i.cdate DESC, i.id DESC LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}


public function getCount() {
	$this->dbSelect('id');

	return $this->getNumRows();
}

public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as inor_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'cdate ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCustomers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT inor_customer_id,DATE(cdate) AS inor_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'inor_customer_id ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockSold($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0; 
	
	$this->setQuery(
"SELECT inoi_quantity
FROM invoice_order AS i
INNER JOIN invoice_order_item AS ii ON inoi_invoice_order_id = i.id
WHERE inoi_product_id = $intProductID AND inor_warehouse_id = $intWarehouseID AND inor_status IN (2,3) AND i.cdate > '$strLastStockUpdate'");
	
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['inoi_quantity'];
	
	return $intTotalStock;
}

public function getHeaderBySaleOrderID($intSaleOrderID) {
	$this->dbSelect('id,cdate AS inor_date',"inor_soid = $intSaleOrderID",'inor_date DESC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getQuantityBySaleOrderID($intSaleOrderID,$intProductID) {
	// Get invoice_order product quantity that involved with the sale order
	$this->setQuery(
"SELECT SUM(inoi_quantity) AS sum_qty
FROM invoice_order AS i
INNER JOIN invoice_order_item AS ii ON inoi_invoice_order_id = i.id
WHERE i.inor_soid = $intSaleOrderID AND inoi_product_id = $intProductID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->sum_qty;
	else false;
}

public function searchByCustomer($strCustomerName, $arrSearchDate, $intStatus) {
	$strWhere = '';
    if(!empty($strCustomerName)) {
        $strCustomerName = urldecode($strCustomerName);
        $strWhere .= " AND (cust_name LIKE '%{$strCustomerName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (i.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (i.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (inor_status = {$intStatus})";
	
	$this->setQuery(
"SELECT i.id, i.cdate AS inor_date, inor_tax, inor_status, cust_name, cust_address, cust_city, cust_phone, ware_name, inor_code,inor_discount, inor_payment, inor_exp_date, inor_jualkanvas,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp
FROM invoice_order AS i
LEFT JOIN jw_customer AS c ON inor_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON inor_warehouse_id = w.id
WHERE i.id > 0{$strWhere}
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intOutletID,$intCustomerID,$intSalesmanID,$intSuppID,$intWarehouseID,$strDescription,$intTax,$intStatus,$strDate,$intDisc,$intTipeBayar,$strExpDate,$intJualKanvas) {
	if(empty($intCustomerID)) $intCustomerID = 1;
	if(empty($strCustName)) {
        $this->setQuery("SELECT cust_name FROM jw_customer WHERE id = $intCustomerID");
        $arrCustomer = $this->getNextRecord('Array');
        $strCustName = $arrCustomer['cust_name'];
    }

	return $this->dbInsert(array(
        'inor_outlet_id' => $intOutletID,
        'inor_jualkanvas' => $intJualKanvas,
		'inor_customer_id' => $intCustomerID,
		'inor_customer_name' => $strCustName,
        'inor_salesman_id' => $intSalesmanID,
        'inor_supplier_id' => $intSuppID,
        'inor_branch_id' => $this->session->userdata('intBranchID'),
		'inor_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
		'inor_description' => $strDescription,
        'inor_discount' => $intDisc,
		'inor_tax' => $intTax,
        'inor_payment' => $intTipeBayar,
        'inor_exp_date' => formatDate2(str_replace('/','-',$strExpDate),'Y-m-d'),
		'inor_status' => $intStatus,
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
	));
}

public function editByID($intID,$strProgress='',$intStatus) {
    $arrUpdate = array('inor_status' => $intStatus);
    if(!empty($strProgress)) $arrUpdate['inor_progres'] = $strProgress;
	return $this->dbUpdate($arrUpdate,"id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intDiscount,$intStatus,$intTax) {
    return $this->dbUpdate(array(
            'inor_description' => $strDescription,
            'inor_progress' => $strProgress,
            'inor_discount' => $intDiscount,
            'inor_tax' => $intTax,
            'inor_status' => $intStatus),
        "id = $intID");
}

public function editByID3($intID,$dateArrive,$intPayment,$status) {
    return $this->dbUpdate(array(
            'inor_status' => $status,
            'inor_payment' => $intPayment),
        "id = $intID");
}

public function editByID4($intID,$dateArrive,$status) {
    return $this->dbUpdate(array(
            'inor_status' => $status),
        "id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'inor_status' => $intStatus),"id = $intID");
}

public function editAfterPrint($intID) {
	return $this->dbUpdate(array(
		'inor_status' => '2'),"id = $intID AND inor_status = '3'");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

/*
public function getFakturBelumLunas() {
    $this->setQuery(
"SELECT inor_code, i.cdate as invoice_order_date, s.sals_code, s.sals_name,
	o.outl_title
FROM invoice_order i
	LEFT JOIN jw_salesman s ON (s.id = i.inor_salesman_id)
	LEFT JOIN jw_outlet o ON (o.id = i.inor_outlet_id)
ORDER BY i.cdate DESC, i.id DESC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
*/

public function getCustomerCredit($intID) {
    $this->setQuery(
"SELECT i.id,inor_grandtotal
FROM invoice_order AS i
WHERE inor_status IN (2,3) AND inor_customer_id  = $intID AND i.id");
//  NOT IN ( SELECT clam_sales_id FROM claim WHERE clam_type = 2 )

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

}

/* End of File */