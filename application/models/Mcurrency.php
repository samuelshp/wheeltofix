<?php

class Mcurrency extends JW_Model {

# CONSTRUCTOR
public function __construct() { 
	parent::__construct(); 
	
	$this->initialize('jw_setting_currency');
}

# LIST
public function getItems() {
	$this->dbSelect();
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) {
		$arrData[$i]['stcu_rate_shown'] = setPrice($arrData[$i]['stcu_rate']);
	}
	
	return $arrData;
}

# DETAIL
public function getItemByCode($strCode) {
	if($strCode == 'BASE') $this->dbSelect('stcu_sign,stcu_thousand,stcu_decimal',"stcu_base = 2");
	else if($strCode == 'USED') $this->dbSelect('stcu_sign,stcu_thousand,stcu_decimal',"stcu_used = 2");
	else if($strCode == 'PAY') $this->dbSelect('stcu_sign,stcu_thousand,stcu_decimal',"stcu_pay = 2");
	else if(is_numeric($strCode)) $this->dbSelect('stcu_sign,stcu_thousand,stcu_decimal',"id = '$strCode'");
	else $this->dbSelect('stcu_sign,stcu_thousand,stcu_decimal',"stcu_sign = '$strCode'");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return FALSE;
}

public function getItemByID($intID) {
	return $this->getItemByCode(intval($intID));
}

}

/* End of File */