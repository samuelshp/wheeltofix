<?php

class Mperminbayar extends JW_Model {

    public function __construct() { 
        parent::__construct(); 
        $this->initialize('permintaan_pembayaran');
    }

    public function getAllPerminBayar($intStartNo,$intPerPage){
        $this->setQuery(
            "SELECT pp.id, pp.perbayar_code, pp.perbayar_amount, pp.cdate, k.kont_name, skn.kategori
            FROM `permintaan_pembayaran` as pp
            LEFT JOIN kontrak as k ON k.id = pp.`perbayar_kontrakid`
            LEFT JOIN subkontrak_nonmaterial as skn ON skn.id = pp.`perbayar_kategori`
            WHERE pp.perbayar_status = 3
            ORDER BY pp.cdate ASC, pp.id ASC LIMIT $intStartNo, $intPerPage");
            
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getAllPerminBayarById($id){
        $this->setQuery(
            "SELECT pp.id, pp.perbayar_ownerid, pp.perbayar_kontrakid, pp.perbayar_subkontrakid, pp.perbayar_code, pp.perbayar_amount, pp.cdate, k.kont_name, skn.kategori, pp.approvalMgr
            FROM `permintaan_pembayaran` as pp
            LEFT JOIN kontrak as k ON k.id = pp.`perbayar_kontrakid`
            LEFT JOIN subkontrak_nonmaterial as skn ON skn.id = pp.`perbayar_kategori`
            WHERE pp.id = $id
            ORDER BY pp.cdate ASC, pp.id ASC");
            
        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;
    }

    public function getCount(){
        $this->dbSelect('id');

	    return $this->getNumRows();
    }

    public function getAllOwner(){
        $this->initialize('jw_customer');

        $this->setQuery(
            "SELECT id, cust_name
            FROM jw_customer");
            
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getNamaProyekByOwner($idowner='', $id_pegawai_login = ''){
		if($id_pegawai_login == 1 || $id_pegawai_login == 5 || $id_pegawai_login == 2 || $id_pegawai_login == 6){
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM kontrak as k
				WHERE k.owner_id = $idowner
				GROUP BY k.kont_name"
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
		else{
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM subkontrak_team as skt
				LEFT JOIN subkontrak as sk ON sk.id = skt.subkontrak_id
				LEFT JOIN kontrak as k ON k.id = sk.kontrak_idid
				WHERE skt.name = $id_pegawai_login AND k.kont_code IS NOT NULL AND k.kont_name LIKE '%$nama_proyek%'
				GROUP BY sk.kontrak_id"
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
    }

    public function getNamaPekerjaan($kontrak_id){
        $this->setQuery(
            "SELECT job, id, kont_code
            FROM subkontrak
            WHERE kontrak_id = $kontrak_id
            ORDER BY job ASC"
        );
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getNamaKategori($id_subkontrak){
        $this->setQuery(
            "SELECT id, kategori, amount
            FROM subkontrak_nonmaterial
            WHERE subkontrak_id = $id_subkontrak
            GROUP BY kategori
            ORDER BY kategori ASC"
        );
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }
//perbayar status langsung approved aja, (tidak butuh approval = -1) 3 approved, 1 softdelete
    public function add($kontrak_id, $subkontrak_id, $owner_id, $kategori, $txtDateBuat, $pembuat ,$amount){
        return $this->dbInsert(array(
            'perbayar_code' => '',
			'perbayar_ownerid' => $owner_id,
            'perbayar_kontrakid' => $kontrak_id,
            'perbayar_subkontrakid' => $subkontrak_id,
			'perbayar_amount' => $amount,
			'perbayar_kategori' => $kategori,
			'cdate' => formatDate2(str_replace('/','-',$txtDateBuat),'Y-m-d H:i'),
            'cby' => $pembuat,
            'approvalMgr' => -1,
            'perbayar_status' => 3
        ));
    }

    public function update($arrPerminbayar, $id)
    {
        return $this->dbUpdate($arrPerminbayar, "id = $id");
    }

    public function delete($id)
    {        
        return $this->dbUpdate(
            array('approvalMgr' => -1, 'perbayar_status' => 1),
            "id = $id"
        );
    }

    public function approve($id)
    {
        return $this->dbUpdate(
            array('approvalMgr' => 3, 'perbayar_status' => 3),
            "id = $id"
        );
    }

    public function disapprove($id)
    {
        return $this->dbUpdate(
            array('approvalMgr' => 1),
            "id = $id"
        );
    }


    public function updatePerminBayarCode($no_perminbayar_baru, $new_id){
        return $this->dbUpdate(array(
			'perbayar_code' => $no_perminbayar_baru),
		"id = $new_id");
    }   

    public function searchBy($strCustomerName, $arrSearchDate, $intStatus)
    {
        $strWhere = '';
        if(!empty($strCustomerName)) {
            // $strCustomerName = urldecode($strCustomerName);
            // $strWhere .= " AND (cust_name LIKE '%{$strCustomerName}%')";
        }
        if(!empty($arrSearchDate[0])) {
            if(!empty($arrSearchDate[1])) $strWhere .= " AND (pp.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
            else $strWhere .= " AND (pp.cdate = '{$arrSearchDate[0]}')";
        }
        if($intStatus >= 0) $strWhere .= " AND (approvalMgr = {$intStatus})";
    
        $this->setQuery(
        "SELECT 
        pp.id, pp.perbayar_code, pp.perbayar_amount, pp.cdate, k.kont_name, skn.kategori
        FROM `permintaan_pembayaran` as pp
        LEFT JOIN kontrak as k ON k.id = pp.`perbayar_kontrakid`
        LEFT JOIN subkontrak_nonmaterial as skn ON skn.id = pp.`perbayar_kategori`
        -- LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
        -- LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
        WHERE pp.id > 0{$strWhere}
        ORDER BY pp.cdate DESC, pp.id DESC");
    
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }    

}

?>