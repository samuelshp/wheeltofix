<?php
/*
PUBLIC FUNCTION:
- getAllCustomer(strKeyword)
- getAllCustomerWithSupplierBind(strKeyword,intSupplier)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mcustomer extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('jw_customer');
}
//new
    public function Invoice_Order_GetAllCustomer($strKeyword = '',$intSupplier) {
        if(!empty($strKeyword)) {
            $strKeyword = urldecode($strKeyword);
            $strWhere = " AND (cust_name LIKE '%$strKeyword%' OR cust_code LIKE '%$strKeyword%')";
        } else $strWhere = '';

        $this->setQuery(
"SELECT id, cust_name, cust_address, cust_city,cust_phone,cust_outlettype,cust_limitkredit,cust_internal,cust_jatuhtempo,cust_markettype
FROM jw_customer
WHERE cust_status > 1 $strWhere
ORDER BY cust_name ASC");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function Invoice_Order_IsCustomerPaidAllPerSupplier($intCustomer = '',$intSupplier='') {

        $this->setQuery(
"    SELECT i.id
    FROM invoice AS i
    WHERE invo_supplier_id  = $intSupplier AND invo_customer_id = $intCustomer AND i.id NOT IN(
        SELECT paym_invoice_id
        FROM payment AS di
        WHERE paym_type IN (4,6)
    )");


        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function Invoice_Order_IsCustomerPaidAll($intCustomer = '') {

        $this->setQuery(
"    SELECT i.id
    FROM invoice AS i
    WHERE  invo_customer_id = $intCustomer AND i.id NOT IN(
        SELECT paym_invoice_id
        FROM payment AS di
        WHERE paym_type IN (4,6)
    )");


        if($this->getNumRows() > 0) return $this->getNumRows();
        else return false;
    }
//sampe sini
public function getAllCustomer($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (cust_name LIKE '%$strKeyword%' OR cust_code LIKE '%$strKeyword%')";
	} else $strWhere = '';

	$this->setQuery(
"SELECT id, cust_code, cust_name, cust_address, cust_city,cust_phone,cust_outlettype,cust_limitkredit,cust_internal,cust_jatuhtempo,cust_markettype
FROM jw_customer
WHERE cust_status > 1 $strWhere
ORDER BY cust_name ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAllCustomerWithSupplierBind($strKeyword = '',$intSupplier='') {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " AND (cust_name LIKE '%$strKeyword%' OR cust_code LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
"SELECT c.id, cust_name, cust_address, cust_city,cust_phone,cust_outlettype,cust_limitkredit,cust_deposit,cust_internal,cust_jatuhtempo,cust_markettype,cust_code
FROM jw_customer AS c
WHERE cust_status > 1 $strWhere /*AND c.id NOT IN (
    SELECT invo_customer_id
    FROM invoice AS i
    WHERE invo_supplier_id  = $intSupplier AND i.id NOT IN(
      SELECT paym_invoice_id
        FROM payment AS di
        WHERE paym_type IN (4,6) *//*invoice tunai atau bg*//*
    )
)*/
ORDER BY cust_name ASC");


    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

    public function isCustomerPaidAllPerSupplier($intCustomer = '',$intSupplier='') {

        $this->setQuery(
"    SELECT i.id
    FROM invoice AS i
    WHERE invo_supplier_id  = $intSupplier AND invo_customer_id = $intCustomer AND i.id NOT IN(
        SELECT paym_invoice_id
        FROM payment AS di
        WHERE paym_type IN (4,6)
    )");


        if($this->getNumRows() > 0) return $this->getNumRows();
        else return "0";
    }

public function getAllCustomerWithSupplierBindForRetur($strKeyword = '',$intSupplier='') {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " AND (cust_name LIKE '%$strKeyword%' OR cust_code LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
"SELECT c.id, cust_name, cust_address, cust_city,cust_phone,cust_outlettype,cust_limitkredit,cust_internal,cust_jatuhtempo,cust_markettype
FROM jw_customer AS c
WHERE cust_status > 1 $strWhere  AND c.id
ORDER BY cust_name ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllData() {
	$this->dbSelect('id,cust_name,cust_address,cust_city',"id > 0 AND cust_status > 1",'cust_name ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,cust_code,cust_name,cust_address,cust_city',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */