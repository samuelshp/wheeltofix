<?php

class Mproductcategory extends JW_Model {

public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_product_category');
}

public function getAllItems($strKeyword = '') {
	if(!empty($strKeyword)) {
		$strWhere = " AND proc_title LIKE '%$strKeyword%'";
	} else {
		$strWhere = '';
	}
	$this->dbSelect('', "proc_status > 1$strWhere");
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('', "id = $intID");
	return $this->getNextRecord('Array');
}

}