<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Epegawai extends Eloquent 
{
    protected $table = 'adm_login';
    public $timestamps = false;
}