<?php
/*
TODO : 
- jika ada muatan baru diinputkan pada hari selasa, maka pada hari rabu muatan tersebut tetap dianggap muatan baru pada hari rabu.
  padahal seharusnya muatan baru tsb menjadi saldo pada hari rabu.


PUBLIC FUNCTION:
- add(intKanvasID,intProductID,strProductDescription,intDay,intRecordType,intQuantity1,intQuantity2,intQuantity3,intUnit1,intUnit2,intUnit3)
- getItemsByKanvasID(intKanvasID,intRecType,strMode)
- getLatestSisaDay(intKanvasID)
- getItemsByKanvasIDByDay(intKanvasID,intRecType,intDay,strMode)
- getItemsByKanvasIDByDayForEditing(intKanvasID,intRecType,intDay,strMode)
- editByID(intID,intDay,intRecordType,intQuantity1,intQuantity2,intQuantity3)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()

*/

class Mkanvasitem extends JW_Model {

public function __construct() {
	parent::__construct();

	$this->initialize('kanvas_item');
}

public function add($intKanvasID,$intProductID,$strProductDescription,$intDay,$intRecordType,$intQuantity1='0',$intQuantity2='0', $intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM kanvas WHERE id = $intKanvasID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'kavi_kanvas_id' => $intKanvasID,
		'kavi_product_id' => $intProductID,
		'kavi_description' => $strProductDescription,
		'kavi_day' => $intDay,
		'kavi_record_type' => $intRecordType,
		'kavi_quantity1' => $intQuantity1,
		'kavi_quantity2' => $intQuantity2,
		'kavi_quantity3' => $intQuantity3,
		'kavi_unit1' => $intUnit1,
		'kavi_unit2' => $intUnit2,
		'kavi_unit3' => $intUnit3,
		'cdate' => $strCDate
	));
}

public function getItemsByKanvasID($intKanvasID,$intRecType,$strMode = 'ALL') {
	$this->setQuery(
"SELECT k.id,pro.id as product_id, kavi_quantity1, kavi_quantity2, kavi_quantity3,
	kavi_product_id, prob_title, proc_title, prod_title,kavi_description,
	kavi_unit1,kavi_unit2,kavi_unit3,proc_title,kavi_day,kavi_record_type,prod_code
FROM jw_product AS pro
LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE kavi_kanvas_id =  $intKanvasID AND kavi_record_type = $intRecType");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getLatestSisaDay($intKanvasID) {
	$this->setQuery(
"SELECT MAX(kavi_day) AS maxday
FROM kanvas_item ki
WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = 0");

	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->maxday;
	else return 0;
}

public function getItemsByKanvasIDByDay($intKanvasID,$intRecType,$intDay,$strMode = 'ALL') {
	/*
	 * kalo muatan sisa dan hari senin, maka pake query default
	 * jika muatan sisa dan hari selasa sampai sabtu maka pake query khusus menghitung stok hari sebelumnya.
     
     * retur tidak menambah stok
	 */
	if ($intRecType == 0 ) {
		if ($intDay > 1) {
            if ($intDay == 2) {
                $intDayBefore = 2;
            } else {
                $intDayBefore = $intDay;                
            }
			$this->setQuery(
"                SELECT 0 as id, kavi_product_id as product_id, prod_title, prob_title, proc_title, kavi_description,
                    kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
                    pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
                    func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 0) -
                    func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 1) -
                    func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 2)
                    as saldo,
                    0 as kavi_quantity1,
                    0 as kavi_quantity2,
                    0 as kavi_quantity3, pro.prod_code
                FROM kanvas_item AS k
                   LEFT JOIN jw_product AS pro ON pro.id = kavi_product_id
                   LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                   LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day < $intDay
                UNION
                SELECT k.id,pro.id as product_id, prod_title, prob_title, proc_title, kavi_description,
                    kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
                    pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
                    -1 as saldo,
                    kavi_quantity1, kavi_quantity2, kavi_quantity3, pro.prod_code
                FROM jw_product AS pro
                    LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
                    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay;");
		} elseif ($intDay == 0) {
			/*
			 * Muatan sisa yang ditampilkan di saldo ada muatan sisa hari terakhir yg tersimpan
			 */
			/*$intDay = $this->getLatestSisaDay($intKanvasID);
            
            if ($intDay == 1) {
                $intDay = 2;
                $intDayBefore = 2;
            } else {
                if ($intDay == 2) {
                    $intDayBefore = 2;
                } else {
                    $intDayBefore = $intDay;                
                }
            }*/
            $intDay = 6;
                
            
			$this->setQuery(
"                SELECT distinct 0 as id, kavi_product_id as product_id, prod_title, prob_title, proc_title, kavi_description,
                    kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,
                    pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
                    func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 0) -
                    func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 1) -
                    func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 2)
                    as saldo,
                    0 as kavi_quantity1,
                    0 as kavi_quantity2,
                    0 as kavi_quantity3, pro.prod_code
                FROM kanvas_item AS k
                   LEFT JOIN jw_product AS pro ON pro.id = kavi_product_id
                   LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                   LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id = $intKanvasID AND kavi_day < $intDay
                UNION
                SELECT k.id,pro.id as product_id, prod_title, prob_title, proc_title, kavi_description,
                    kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,
                    pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
                    -1 as saldo,
                    kavi_quantity1, kavi_quantity2, kavi_quantity3, pro.prod_code
                FROM jw_product AS pro
                    LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
                    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay;");
		} else {
		  $this->setQuery(
"                SELECT k.id,pro.id as product_id, kavi_quantity1, kavi_quantity2, kavi_quantity3,
                    kavi_product_id, prob_title, proc_title, prod_title,kavi_description,
                    prod_unit1, prod_unit2, prod_unit3,pro.prod_code,
                    kavi_unit1,kavi_unit2,kavi_unit3,proc_title,kavi_day,kavi_record_type, 0 as saldo
                FROM jw_product AS pro
                    LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
                    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay");
		};
	} else {
		if ($intDay == 0) {
			/*
			 * Saldo yang ditampilkan adalah total dari hari senin sampai sabtu
			 */
			$this->setQuery(
"                SELECT distinct kavi_product_id as product_id, prod_title, prob_title, proc_title, kavi_description,
                   kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,
                   pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
                   func_count_kanvas_saldo(kavi_kanvas_id, $intRecType, kavi_product_id) as saldo,
                   0 as kavi_quantity1, 0 as kavi_quantity2, 0 as kavi_quantity3, pro.prod_code
                FROM kanvas_item
                  LEFT JOIN jw_product AS pro ON pro.id = kavi_product_id
                  LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                  LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id

                WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType
                HAVING kavi_product_id <> 0");
		} else {
			$this->setQuery(
"                SELECT k.id,pro.id as product_id, prob_title, proc_title, prod_title,kavi_description,
                    kavi_unit1,kavi_unit2,kavi_unit3,prod_unit1, prod_unit2, prod_unit3,
                    pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3, -1 as saldo,
                    kavi_quantity1, kavi_quantity2, kavi_quantity3,kavi_product_id,
                    kavi_day,kavi_record_type, pro.prod_code
                FROM jw_product AS pro
                    LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
                    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
                    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
                WHERE kavi_kanvas_id =  $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay");
		}
	}

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByKanvasIDByDayForEditing($intKanvasID,$intRecType,$intDay,$strMode = 'ALL') {

    if ($intDay == 1) {
        $intDayBefore = $intDay;
    } else if ($intDay == 2) {
        $intDayBefore = 2;
        if ($intRecType == 0) {
            $intDayBefore = 1;
        }
    } else if ($intDay > 1) {
        $intDayBefore = $intDay/* - 1*/;
        if ($intRecType == 0) {
            $intDayBefore = $intDay - 1;
        }
    } else {
        $intDayBefore = 0;
    };

    if ($intRecType == 0) {
        $this->setQuery(
"        SELECT 0 as id, kavi_product_id as product_id, prod_title, prob_title, proc_title, kavi_description,
            kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
            pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
            func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 0) -
            func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 1) -
            func_count_kanvas_stock(kavi_kanvas_id, $intDay, kavi_product_id, 2)
            as saldo,
            0 as kavi_quantity1,
            0 as kavi_quantity2,
            0 as kavi_quantity3,
            $intDayBefore as kavi_day, kavi_record_type,pro.prod_code
        FROM kanvas_item AS k
            LEFT JOIN jw_product AS pro ON pro.id = kavi_product_id
            LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
            LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
        WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day < $intDay
        UNION
        SELECT k.id,pro.id as product_id, prod_title, prob_title, proc_title, kavi_description,
            kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
            pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
            -1 as saldo,
            kavi_quantity1, kavi_quantity2, kavi_quantity3, kavi_day, kavi_record_type,pro.prod_code
        FROM jw_product AS pro
            LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
            LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
            LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
        WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay;");
    } else {
        $this->setQuery(
"        SELECT k.id, kavi_product_id as product_id, prod_title, prob_title, proc_title, kavi_description,
            kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
            pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
            func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 0) -
            func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 1) -
            func_count_kanvas_stock(kavi_kanvas_id, $intDayBefore, kavi_product_id, 2)
            as saldo,
            0 as kavi_quantity1,
            0 as kavi_quantity2,
            0 as kavi_quantity3,
            kavi_day,kavi_record_type,pro.prod_code
        FROM kanvas_item AS k
            LEFT JOIN jw_product AS pro ON pro.id = kavi_product_id
            LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
            LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
        WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day < $intDay
        UNION
        SELECT k.id,pro.id as product_id, prod_title, prob_title, proc_title, kavi_description,
            kavi_unit1, kavi_unit2, kavi_unit3, prod_unit1, prod_unit2, prod_unit3,prod_code,
            pro.prod_conv1 as conv1, pro.prod_conv2 as conv2, pro.prod_conv3 as conv3,
            -1 as saldo,
            kavi_quantity1, kavi_quantity2, kavi_quantity3, kavi_day,kavi_record_type,pro.prod_code
        FROM jw_product AS pro
            LEFT JOIN kanvas_item AS k ON pro.id = k.kavi_product_id
            LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
            LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
        WHERE kavi_kanvas_id = $intKanvasID AND kavi_record_type = $intRecType AND kavi_day = $intDay;");
    }

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}
    
public function editByID($intID,$intDay,$intRecordType,$intQuantity1,$intQuantity2,$intQuantity3) {
	return $this->dbUpdate(array(
			'kavi_day' => $intDay,
			'kavi_record_type' => $intRecordType,
			'kavi_quantity1' => $intQuantity1,
			'kavi_quantity2' => $intQuantity2,
			'kavi_quantity3' => $intQuantity3),
		"id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}