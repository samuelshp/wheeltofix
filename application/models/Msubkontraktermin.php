<?php

class Msubkontraktermin extends JW_Model
{
    // Constructor
    public function __construct() { 
        parent::__construct();
        $this->initialize('subkontrak_termin');
    }

    public function add($arrDataTermin, $intSubkontrakID)
    {
        $intCount = count($arrDataTermin['jenis']);
        $i=0;
        while($i < $intCount) { 
            
            $dbInsert = $this->dbInsert(array(
                'subkontrak_id' => $intSubkontrakID,
                'jenis' => $arrDataTermin['jenis'][$i],
                'amount' => str_replace(".","", $arrDataTermin['termin_amount'][$i]),
                'terbayar' => str_replace(".","", $arrDataTermin['terbayar'][$i]),
                'terbayar_date' => formatDate2(str_replace('/','-',$arrDataTermin['terbayar_date'][$i]),'Y-m-d'),
                'due_date' => formatDate2(str_replace('/','-',$arrDataTermin['due_date'][$i]),'Y-m-d')
            ));

            if($dbInsert) $i++;
            else return false;
        }           
            // $toInsert['jenis'][$key] = array($jenis,$intAmount,$intTerbayar,$strDateTerbayar,$strDueDate,$subkontrak_id);
        
        return true;
        #$jenis,$intAmount,$intTerbayar,$strDateTerbayar,$strDueDate,$subkontrak_id
        // return $this->dbInsert(array(
        //     'subkontrak_id' => $subkontrak_id,
        //     'jenis' => $jenis,
        //     'amount'    => $intAmount,
        //     'terbayar'  => $intTerbayar,
        //     'terbayar_date' => formatDate2(str_replace('/','-',$strDateTerbayar),'Y-m-d'),
        //     'due_date'  => formatDate2(str_replace('/','-',$strDueDate),'Y-m-d')
        // ));
    }

    public function updateDuedate($id,$due_date) {
        return $this->dbUpdate(array(
            'due_date' => formatDate2(str_replace('/','-',$due_date),'Y-m-d')
        ),"id = $id");
    }

    public function update($id,$subkontrak_id, $jenis,$amount,$terbayar,$terbayar_date, $due_date, $mby)    
    {
        return $this->setQuery("REPLACE INTO subkontrak_termin (id, subkontrak_id, jenis, amount, terbayar, terbayar_date, due_date, mby, mdate) VALUES ($id, $subkontrak_id, '$jenis','$amount','$terbayar','$terbayar_date', '$due_date','$mby', 'NOW()')");        
        // if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        // else return false;
    }

    public function deleteItem($id)
    {
        return $this->dbUpdate(array(
            'status' => 1
        ),"id = $id");
    }
    
}


?>