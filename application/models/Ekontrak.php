<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ekontrak extends Eloquent 
{
    protected $table = 'kontrak';
    public $timestamps = false;

    public function owner()
    {
        return $this->belongsTo(Ecustomer::class);
    }

    public function subkontrak()
    {
        return $this->hasOne(Esubkontrak::class, 'kont_code', 'kont_code');
    }

    public function subkontraks()
    {   
        return $this->hasMany(Esubkontrak::class, 'kontrak_id');
    }

    public function getRelationships() {
        return [
            'belongsTo' => [
                'Estaff',
            ],
            'hasMany' => [
                'Ekontraktermin',
            ],
        ];
    }
}