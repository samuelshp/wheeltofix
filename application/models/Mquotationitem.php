<?php
/**
 * 
 */
class Mquotationitem extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('quotation_item');
	}

	public function getItem($intID)
	{
		$this->setQuery("SELECT q.*, prod.id as prod_id, prod_title, prod_price 
		FROM quotation_item q
		JOIN jw_product prod ON q.quoi_prod_id = prod.id
		JOIN jw_unit unit ON prod.prod_satuan_id = unit.id
		WHERE q.quoi_quot_id = ".$intID);

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function add($intQuotationID, $arrItem)
	{
		for ($i=0; $i < count($arrItem['idProduct']) ; $i++) { 
			$intInsertItem = $this->dbInsert(array(
				'quoi_quot_id' => $intQuotationID,
				'quoi_prod_id' => $arrItem['idProduct'][$i],
				'quoi_qty_display' => $arrItem['inputQty'][$i],
				'quoi_price' => $arrItem['inputHarga'][$i],
				'quoi_disc' => $arrItem['inputDsc'][$i],
				'quoi_subtotal' => $arrItem['inputSubtotal'][$i],
				'quoi_desc' => $arrItem['isiKeterangan'][$i]
			));			
		}

		return $intInsertItem;
		
	}

	public function update($arrItem, $intQuotationID)
	{
		$this->dbDelete("quoi_quot_id = $intQuotationID");
		
		for ($i=0; $i < count($arrItem['idProduct']) ; $i++) { 
			$intInsertItem = $this->dbInsert(array(
				'quoi_quot_id' => $intQuotationID,
				'quoi_prod_id' => $arrItem['idProduct'][$i],
				'quoi_qty_display' => $arrItem['inputQty'][$i],
				'quoi_price' => $arrItem['inputHarga'][$i],
				'quoi_disc' => $arrItem['inputDsc'][$i],
				'quoi_subtotal' => $arrItem['inputSubtotal'][$i],
				'quoi_desc' => $arrItem['isiKeterangan'][$i]
			));			
		}
	}

	public function closeItem($intItemID)
	{
		$this->dbUpdate(array('quoi_closed' => 1),"id=$intItemID");
	}

	public function reverseCloseItem($intItemID)
	{
		$this->dbUpdate(array('quoi_closed' => 0),"id=$intItemID");
	}
}