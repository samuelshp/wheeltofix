<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intPayment)

PRIVATE FUNCTION:
- __construct()	
*/

class Maccountadjustmentitem extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();
	$this->initialize('accountadjustment_item');
}

public function add($intHeaderID,$intPerkiraanID,$intType,$intTotal) {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM accountadjustment WHERE id = $intHeaderID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';
	
	return $this->dbInsert(array(
		'adem_header_id' => $intHeaderID,
		'adem_perkiraan_id' => $intPerkiraanID,
		'adem_type' => $intType,
		'adem_total' => $intTotal,
		'cdate' => $strCDate
	));
}

public function getAllAdjustmentItemDebit($intID) {
	$this->setQuery(
"SELECT ai.id,adem_type,acco_name,acco_code,adem_total
FROM  accountadjustment_item AS ai
LEFT JOIN jw_account AS a ON adem_perkiraan_id = a.id
WHERE ai.adem_header_id = $intID AND adem_type = '1'
ORDER BY ai.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAllAdjustmentItemKredit($intID) {
	$this->setQuery(
"SELECT ai.id,adem_type,acco_name,acco_code,adem_total
FROM  accountadjustment_item AS ai
LEFT JOIN jw_account AS a ON adem_perkiraan_id = a.id
WHERE ai.adem_header_id = $intID AND adem_type = '-1'
ORDER BY ai.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function deleteByHeaderID($intID) {
	return $this->dbDelete("adem_header_id = $intID");
}
}

/* End of File */