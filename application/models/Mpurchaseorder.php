<?php
/*
PUBLIC FUNCTION:
- getAllPOID()
- getSupplierData(intID) x
- getDataAutoComplete(txtData) x
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate) x
- getItemsBySupplierID(intID,strDate) x
- getAccountPayableItems()
- getItemsByProductID(intProductID,strLastStockUpdate)
- getItemsForFinance() x
- getUserHistory(intUserID,intPerPage) x
- getItemByID(intID) 
- getCount() x
- getDateMark(strFrom,strTo) x
- getSuppliers(strFrom,strTo) x
- getStockPurchased(intProductID,strLastStockUpdate) x
- searchBySupplier(strSupplierName) x
- add(intSupplierID,strDescription,intTax,intStatus,strDate,strEndDate,strExpDate,intDisc)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intDiscount,intStatus,intTax,intEdit)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

New function public :
- getKontrakDataFilterOwner()
- getKontrakDataNamaProyek()
- getKontrakDaftarBahan()
- getAllOwner()
- getProyekByOwner()
- getPekerjaanByNamaProyek()
- add()
- editByApprovalCostControl()
- editApprovalPM()
- deleteByID(id)

PRIVATE FUNCTION:
- __construct()
*/

class Mpurchaseorder extends JW_Model {

	// Constructor
	public function __construct() {
		parent::__construct();
		$this->initialize('purchase_order');
	}
	
	public function getAllPOID() {
		$this->setQuery("SELECT id, pror_supplier_id, cdate FROM purchase_order WHERE pror_status = '".STATUS_APPROVED."' Order By cdate DESC, id DESC");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getSupplierData($intID = 0) {
		$this->setQuery(
	"SELECT supp_name,supp_address,supp_city,supp_phone,pror_tax,pror_description
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE p.id = $intID");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	//New function

	public function searchBox($noPB='',$tglProyek=''){
		$tglProyek = str_replace('/','-', $tglProyek);
		if($noPB != ''){//berdasarkan pb
			$strWhere = "p.pror_code LIKE '%$noPB%' AND pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")";
		}
		else if($noPB !='' && $tglProyek != ''){
			$strWhere = "p.pror_code LIKE '%$noPB%' && p.cdate = '$tglProyek' AND pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")";
		}
		else{//berdasarkan kontrak
			$strWhere = "p.cdate LIKE '%$tglProyek%' AND pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")";
		}

		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.pror_code, p.cdate, k.kont_name, p.idKontrak, p.id, p.approvalCC, p.approvalPM, sk.job as kont_job, sk.subkont_kode, p.pror_status
			from purchase_order as p 
			LEFT JOIN kontrak as k ON k.id = p.idKontrak 
			LEFT JOIN subkontrak as sk ON sk.id = p.idSubKontrak
			WHERE $strWhere $whereInProject
			GROUP BY p.pror_code");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function updateCloseStatus($data = ''){
		for($i=0; $i<sizeof($data); $i++){
			$this->setQuery(
				"UPDATE subkontrak_material SET statusClosed = 1 WHERE id = $data[$i]"
			);
		}
	}

	public function updateKodePurchaseOrder($kode_pb, $id_pb){
		return $this->dbUpdate(array(
			'pror_code' => $kode_pb),
		"id = $id_pb");
	}

	public function getLatestID(){
		return $this->setQuery(
			"SELECT id FROM purchase_order WHERE ID = ( SELECT MAX(ID) FROM purchase_order)"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function editCCStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET approvalCC=3 WHERE pror_code = '$pror_code' "
		);
	}

	public function editPMStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET approvalPM=3 WHERE pror_code = '$pror_code' "
		);
	}

	public function editProrStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET pror_status=3 WHERE pror_code = '$pror_code' "
		);
	}

	public function editAccStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET pror_status=3 WHERE pror_code = '$pror_code' "
		);
	}

	public function editProrStatus2($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET pror_status=1 WHERE pror_code = '$pror_code' "
		);
	}

	public function uneditCCStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET approvalCC=1 WHERE pror_code = '$pror_code' "
		);
	}

	public function uneditPMStatus($pror_code=''){
		return $this->setQuery(
			"UPDATE purchase_order SET approvalPM=1 WHERE pror_code = '$pror_code' "
		);
	}

	public function updateJamApprovalPM($idpo, $date){
		return $this->dbUpdate(array(
			'acdate_pm' => $date),
		"id = $idpo");
	}

	public function updateJamApprovalCC($idpo, $date){
		return $this->dbUpdate(array(
			'acdate_cc' => $date),
		"id = $idpo");
	}

	public function updateOrangApprovalPM($idpo, $orang){
		return $this->dbUpdate(array(
			'aby_pm' => $orang),
		"id = $idpo");
	}

	public function updateOrangApprovalCC($idpo, $orang){
		return $this->dbUpdate(array(
			'aby_cc' => $orang),
		"id = $idpo");
	}

	public function getBahanByIdProduct($id_prod){
		$this->setQuery(
			"SELECT *, ju.unit_title as title_pb, ju2.unit_title as title_terima
			FROM jw_product as jp
			LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id
			LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_terima_id
			WHERE jp.id = $id_prod"
		);
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return $this->getQueryResult('Array');
	}

	public function getAllOwner($nameOwner='', $idKontrak=''){
		$nameOwner = urldecode($nameOwner);
		if($idKontrak == ''){
			$this->setQuery(
				"SELECT id, COUNT(id) as id_count, cust_code, cust_name from jw_customer
				where cust_name LIKE '%$nameOwner%' "
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
		else{
			$this->setQuery(
				"SELECT jc.id, COUNT(jc.id) as id_count, jc.cust_code, jc.cust_name from jw_customer as jc LEFT JOIN kontrak as k ON k.kont_code = '$idKontrak' where cust_name LIKE '%$nameOwner%' AND jc.id = k.owner_id"
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
		
	}

	public function getAllKontrak($orang=0){
		if($orang == 0){
			$this->setQuery(
			"SELECT * 
			from kontrak
			ORDER BY kont_name ASC
			");
		}
		else{
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM subkontrak_team as skt
				LEFT JOIN subkontrak as sk ON sk.id = skt.subkontrak_id
				LEFT JOIN kontrak as k ON k.id = sk.kontrak_id
				WHERE skt.name = $orang AND skt.status != ".STATUS_DELETED."
				GROUP BY sk.kontrak_id
                ORDER BY k.kont_name ASC"
			);
		}

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getNamaProyek($nama_proyek='', $id_pegawai_login = ''){
		if($id_pegawai_login == 1 || $id_pegawai_login == 5 || $id_pegawai_login == 2 || $id_pegawai_login == 6){
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM kontrak as k
				WHERE k.kont_name LIKE '%$nama_proyek%'
				GROUP BY k.kont_name"
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
		else{
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM subkontrak_team as skt
				LEFT JOIN subkontrak as sk ON sk.id = skt.subkontrak_id
				LEFT JOIN kontrak as k ON k.id = sk.kontrak_idid
				WHERE skt.name = $id_pegawai_login AND k.kont_code IS NOT NULL AND k.kont_name LIKE '%$nama_proyek%'
				GROUP BY sk.kontrak_id"
			);
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
		}
	}

	public function getNamaPekerjaan($kontrak_id){
		$this->setQuery(
			"SELECT job, id, kont_code
			FROM subkontrak
			WHERE kontrak_id = $kontrak_id
			ORDER BY job ASC"
		);
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getKontrakIdByNamaPekerjaan($namaPekerjaan, $ownerID){
		$this->setQuery(
			"SELECT id from kontrak WHERE kont_job = $namaPekerjaan AND owner_id = $ownerID"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllBahanAutoComplete($bahan){
		$this->setQuery(
			"SELECT jp.id, jp.prod_title, jp.prod_qty1, ju.unit_title, ju.unit_conversion
			from jw_product as jp
			left join jw_unit as ju on ju.id = jp.satuan_pb_id
			WHERE prod_title LIKE '%$bahan%'"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllBahanAC($bahan){
		$this->initialize('jw_product');
		$this->setQuery(
			"SELECT jp.id, jp.prod_title, ju.unit_title
			FROM jw_product as jp
			LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
			WHERE jp.prod_title LIKE '%$bahan%' "
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllProduct(){
		$this->initialize('jw_product');
		$this->setQuery(
			"SELECT *
			FROM jw_product
			ORDER BY prod_title ASC"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllBahanAutoComplete2($bahan, $subkon_id){
		$this->initialize('subkontrak_material');
		$this->setQuery(
			"SELECT k.id, sk.id as subkontrak_id, sk.kont_code, skm.material, jp.prod_title, SUM(skm.qty) - SUM(skm.subkontrak_terpb) as qty, ju.unit_title, skm.subkontrak_terpb, skm.qty as jumlah_awal, skm.keterangan, skm.statusClosed 
			FROM subkontrak_material as skm 
			LEFT JOIN jw_product as jp ON jp.id = skm.material 
			LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
			LEFT JOIN subkontrak as sk ON sk.id = skm.subkontrak_id
			LEFT JOIN kontrak as k ON k.kont_code = sk.kont_code
			WHERE jp.prod_title = '$bahan' AND skm.subkontrak_id = $subkon_id
			GROUP BY sk.kont_code, skm.material"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllBahanAutoCompleteById($bahan, $subkon_id){
		$this->initialize('subkontrak_material');
		$this->setQuery(
			"SELECT k.id, sk.id as subkontrak_id, sk.kont_code, skm.material, jp.prod_title, SUM(skm.qty) - SUM(skm.subkontrak_terpb) as qty, ju.unit_title, ju2.unit_title as title_terima, skm.subkontrak_terpb, skm.qty as jumlah_awal, skm.keterangan, skm.statusClosed 
			FROM subkontrak_material as skm 
			LEFT JOIN jw_product as jp ON jp.id = skm.material 
			LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
			LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_terima_id 
			LEFT JOIN subkontrak as sk ON sk.id = skm.subkontrak_id
			LEFT JOIN kontrak as k ON k.kont_code = sk.kont_code
			WHERE jp.id = $bahan AND skm.subkontrak_id = $subkon_id
			GROUP BY sk.kont_code, skm.material"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function checkAvailableCostControl($subkonid){
		$this->setQuery(
			"SELECT skt.name
			FROM subkontrak as sk
			LEFT JOIN subkontrak_team as skt ON skt.subkontrak_id = sk.id
			LEFT JOIN tinydb as t ON t.tiny_key = skt.jabatan_id AND t.tiny_category = 'admin_priviledge'
			WHERE sk.id = $subkonid AND skt.jabatan_id = 11
			GROUP BY skt.name
			"
		);
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function udpateStatusCC($idPurchaseOrder){
		return $this->dbUpdate(array(
			'approvalCC' => -1),
		"id = $idPurchaseOrder");
	}

	//New function
	
	public function getDataAutoComplete($intPBID) {
		$strKeyword = urldecode($strKeyword);
	
		$this->setQuery(
	"SELECT p.id,p.cdate, k.kont_name, k.kont_code, sk.job, c.cust_name
	FROM purchase_order AS p
	
	LEFT JOIN kontrak AS k ON k.id = p.idKontrak
	LEFT JOIN subkontrak AS sk ON k.id = p.idKontrak
	LEFT JOIN jw_customer c ON c.id = k.owner_id
	WHERE p.id = $intPBID ");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getDataByCustID($intCustID) {
		$this->setQuery(
	"SELECT p.id,p.cdate, k.kont_name, k.kont_code, sk.job as kont_job, cust_name
	FROM kontrak AS k
	LEFT JOIN purchase_order AS p ON p.idKontrak = k.id 
	LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
	LEFT JOIN jw_customer c ON c.id = k.owner_id
	WHERE approvalPM = 1 AND k.owner_id = $intCustID
	GROUP BY kont_name");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getDataByProjectID($intProjectID) {
	// 	$this->setQuery(
	// "SELECT p.id,p.cdate, k.kont_name, k.kont_code, sk.job as kont_job, cust_name
	// FROM kontrak AS k
	// LEFT JOIN purchase_order AS p ON p.idKontrak = k.id 
	// LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
	// LEFT JOIN jw_customer c ON c.id = k.owner_id
	// WHERE approvalPM = 1 AND k.id = $intProjectID
	// GROUP BY kont_name"); punya ko audi

		$this->setQuery(
			"SELECT p.id, poi.id as poi_id, p.cdate, sk.job as kont_job, p.pror_code, jp.prod_title, p.cdate
			FROM purchase_order as p
            LEFT JOIN purchase_order_item as poi ON poi.proi_purchase_order_id = p.id
            LEFT JOIN jw_product as jp ON jp.id = poi.proi_product_id
			LEFT JOIN subkontrak as sk ON sk.kontrak_id = p.idKontrak
			WHERE p.approvalPM = 3 AND poi.proi_quantity1 > poi.proi_terpurchase AND p.idKontrak = $intProjectID
            GROUP BY poi.id
            ORDER BY p.cdate, p.id, jp.prod_title ASC
			# bedanya hanya disini WHERE p.approvalPM = 3 AND poi.proi_quantity1 > poi.proi_terpurchase AND p.idKontrak = $intProjectID"
		);
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getItems($intStartNo = -1,$intPerPage = -1) {
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC, p.id DESC";
		else $strOrderBy = "p.cdate DESC, p.id DESC LIMIT $intStartNo, $intPerPage";
		
		$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date,pror_status, supp_name, supp_address, supp_city, supp_phone,pror_code,pror_grandtotal,pror_tax
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	ORDER BY $strOrderBy");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getItemsBaru($intStartNo = -1,$intPerPage = -1){
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC, p.pror_code DESC";
		else $strOrderBy = "p.cdate DESC, p.pror_code DESC LIMIT $intStartNo, $intPerPage";

		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.pror_code, p.cdate, k.kont_name, p.idKontrak, p.id, p.approvalCC, p.approvalPM, sk.job as kont_job, sk.subkont_kode, p.pror_status
			from purchase_order as p 
			LEFT JOIN kontrak as k ON k.id = p.idKontrak 
			LEFT JOIN subkontrak as sk ON sk.id = p.idSubKontrak
			WHERE p.pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject
            GROUP BY p.pror_code
			order by $strOrderBy"
			// "SELECT p.pror_code, p.cdate, p.idKontrak, p.id, p.approvalCC, p.approvalPM, sk.job as kont_job, pu.prch_code
			// from purchase_order as p
			// LEFT JOIN kontrak as k ON k.id = p.idKontrak 
			// LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id AND sk.kont_code = k.kont_code
			// LEFT JOIN purchase as pu ON pu.prch_po_id = p.id
			// order by $strOrderBy"
		);
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getItemsByDate($strDate) {
			$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date, pror_tax, pror_status, supp_name, supp_address, supp_city, supp_phone
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE DATE(p.cdate) = DATE('$strDate')
	ORDER BY p.cdate DESC, p.id DESC");
	
			if($this->getNumRows() > 0) return $this->getQueryResult('Array');
			else return false;
	}
	
	public function getItemsBySupplierID($intID,$strDate) {
		$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date, pror_tax, pror_status, supp_name, supp_address, supp_city, supp_phone
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE pror_supplier_id = $intID AND DATE(p.cdate) = DATE('$strDate')
	ORDER BY p.cdate DESC, p.id DESC");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getAccountPayableItems() {
		$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date, pror_tax, supp_name, supp_address, supp_city, supp_phone
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE pror_status IN (".STATUS_OUTSTANDING.",".STATUS_APPROVED.")
	ORDER BY p.cdate DESC, p.id DESC");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getItemsByProductID($intProductID,$strLastStockUpdate) {
		$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
		$this->setQuery(
	"SELECT pi.id, supp_name, supp_address, supp_city, p.cdate AS pror_date, pror_status, proi_quantity
	FROM purchase_order AS p
	INNER JOIN purchase_order_item AS pi ON proi_purchase_id = p.id
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE prci_product_id = $intProductID  AND p.cdate > '$strLastStockUpdate'");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getItemsForFinance() {
		$this->setQuery(
	"SELECT p.id,supp_name AS name,p.cdate AS date
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE pror_status NOT IN (".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.") AND p.id NOT IN (
	SELECT fina_transaction_id FROM finance WHERE fina_type = 2
	)");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getUserHistory($intUserID,$intPerPage) {
		$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date, pror_status, supp_name, supp_address, supp_city,pror_code
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE (p.cby = $intUserID OR p.mby = $intUserID)
	ORDER BY p.cdate DESC, p.id DESC LIMIT 0, $intPerPage");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getItemByID($intID = 0) {//terganti
		if($intID == 0) $strWhere = "ORDER BY po.cdate DESC, po.id DESC LIMIT 0,1";
		else $strWhere = "WHERE po.id = $intID";
	
		$this->setQuery(
			"SELECT po.id, po.pror_description, po.approvalCC, po.approvalPM, po.pror_status, po.pror_code, po.cdate, k.kont_name, jc.cust_name, sk.job, po.approvalCC, po.approvalPM, po.aby_pm, po.aby_cc, lgPR.adlg_name as adlg_name_pr, lgCC.adlg_name as adlg_name_cc, lgPM.adlg_name as adlg_name_pm, lg.adlg_name, k.kont_code, k.id as kontrak_id, po.idSubKontrak
			FROM purchase_order as po
			LEFT JOIN kontrak as k ON k.id = po.idKontrak
			LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
			LEFT JOIN purchase as p ON p.prch_po_id = po.id
			LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
			LEFT JOIN adm_login as lgCC ON po.aby_cc = lgCC.id
			LEFT JOIN adm_login as lgPM ON po.aby_pm = lgPM.id
			LEFT JOIN adm_login as lg ON po.cby = lg.id
			LEFT JOIN adm_login as lgPR ON p.cby = lgPR.id			
			$strWhere");
	
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}
	
	public function getCount() {
		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.pror_code, p.cdate, k.kont_name, p.idKontrak, p.id, p.approvalCC, p.approvalPM, sk.job as kont_job, sk.subkont_kode
			from purchase_order as p 
			LEFT JOIN kontrak as k ON k.id = p.idKontrak 
			LEFT JOIN subkontrak as sk ON sk.id = p.idSubKontrak
			WHERE p.pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject
            GROUP BY p.pror_code"
		);
		return $this->getNumRows();
	}
	
	public function getDateMark($strFrom,$strTo) {
		$this->dbSelect('DISTINCT DATE(cdate) as pror_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'pror_date ASC');
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getSuppliers($strFrom,$strTo) {
		$this->dbSelect('DISTINCT pror_supplier_id,DATE(cdate) AS pror_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'pror_supplier_id ASC');
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function getStockPurchased($intProductID,$strLastStockUpdate) {
		$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
		$intTotalStock = 0;
	
		$this->setQuery(
	"SELECT proi_quantity
	FROM purchase_order AS p
	INNER JOIN purchase_order_item AS pi ON proi_purchase_id = p.id
	WHERE proi_product_id = $intProductID AND pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.") AND p.cdate >= '$strLastStockUpdate'");
	
		$arrData = $this->getQueryResult('Array');
		for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['proi_quantity'];
	
		return $intTotalStock;
	}
	
	public function searchBySupplier($strSupplierName, $arrSearchDate, $intStatus) {
		$strWhere = '';
		if(!empty($strSupplierName)) {
			$strSupplierName = urldecode($strSupplierName);
			$strWhere .= " AND (supp_name LIKE '%{$strSupplierName}%')";
		}
		if(!empty($arrSearchDate[0])) {
			if(!empty($arrSearchDate[1])) $strWhere .= " AND (p.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
			else $strWhere .= " AND (p.cdate = '{$arrSearchDate[0]}')";
		}
		if($intStatus >= 0) $strWhere .= " AND (pror_status = {$intStatus})";
	
		$this->setQuery(
	"SELECT p.id, p.cdate AS pror_date, pror_tax, pror_status, supp_name, supp_address, supp_city, supp_phone, pror_code, pror_discount
	FROM purchase_order AS p
	LEFT JOIN jw_supplier AS s ON pror_supplier_id = s.id
	WHERE p.id > 0{$strWhere}
	ORDER BY p.cdate DESC, p.id DESC");
	
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	public function add($intSupplierID,$intWarehouseID,$strDescription,$intTax,$intStatus,$strDate,$strExpDate,$intDisc) {
		return $this->dbInsert(array(
			'pror_supplier_id' => $intSupplierID,
			'pror_branch_id' => $this->session->userdata('intBranchID'),
			'pror_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
			'pror_description' => $strDescription,
			'pror_discount' => $intDisc,
			'pror_tax' => $intTax,
			'pror_status' => $intStatus,
			'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i'),
			'pror_exp_date' => formatDate2(str_replace('/','-',$strExpDate),'Y-m-d'),
		));
	}

	public function addPO($txtNoPB, $txtDateBuat, $txtaDescription, $kontrakID, $codeKontrak, $creator){
		return $this->dbInsert(array(
			'pror_code' => $txtNoPB,
			'pror_description' => $txtaDescription,
			'cdate' => date_format(date_create($txtDateBuat." ".date('H:i:s')), 'Y-m-d H:i:s'),
			'idKontrak' => $kontrakID,
			'idSubKontrak' => $codeKontrak,
			'pror_status' => STATUS_APPROVED,
			'cby' => $creator
		));
	}
	
	public function editByID($intID,$strProgress,$intStatus) {
		return $this->dbUpdate(array(
			'pror_progress' => $strProgress,
			'pror_status' => $intStatus),
		"id = $intID");
	}
	
	public function editByID2($intID,$strDescription,$strProgress,$intDiscount,$intStatus,$intTax,$intEdit) {
		return $this->dbUpdate(array(
			'pror_description' => $strDescription,
			'pror_progress' => $strProgress,
			'pror_discount' => $intDiscount,
			'pror_tax' => $intTax,
			'pror_status' => $intStatus),
		"id = $intID");
	}
	
	public function editStatusByID($intID, $ganti) {
		return $this->dbUpdate(array(
			'pror_status' => $ganti),
		"id = $intID");
	}
	
	public function deleteByID($intID) {
		return $this->dbUpdate(array(
			'pror_status' => 1),
		"id = $intID");
		
		// $status = $this->dbSelect('pror_status',"id = $intID");
		// if($status['pror_status']='0' || $status['pror_status']='1'){
		// 	$this->setQuery("DELETE FROM purchase_order_item WHERE proi_purchase_order_id = $intID");
		// 	return $this->dbDelete("id = $intID");
			
		// } else return false;
	}

	public function getAllPMBySubKontrak($id_subkon){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $id_subkon AND jabatan_id = 9");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllCCBySubKontrak($id_subkon){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $id_subkon AND jabatan_id = 11");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllSMBySubKontrak($id_subkon){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $id_subkon AND jabatan_id = 10");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllPurchasingBySubKontrak($id_subkon){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $id_subkon AND jabatan_id = 2");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllLogistikBySubKontrak($id_subkon){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $id_subkon AND jabatan_id = 3");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function closePB($intID, $description){
		return $this->dbUpdate(array(
			'pror_status' => STATUS_FINISHED,
			'pror_progress' => $description),
		"id = $intID");
	}

	public function checkEditablePB($idPegawai, $idSubKontrak){
		$this->initialize('subkontrak_team');

		$this->dbSelect('name',"subkontrak_id = $idSubKontrak AND name = $idPegawai");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	
	}

/* End of File */