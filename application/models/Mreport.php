<?php

class Mreport extends JW_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function reportHutangRekap($strDateStart,$strDateEnd,$intSupplierID,$intKontrakID) {
		if($intSupplierID == 0) $intSupplierID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @sa = 0.00;");
		$this->setQuery("SET @beli = 0.00;");
		$this->setQuery("SET @bayar = 0.00;");
		$this->setQuery("SET @sakhir = 0.00;");
		$this->setQuery("
			SELECT *, @sa := @sa + saldo_awal, @beli := @beli + beli, @bayar := @bayar + bayar, @sakhir := @sakhir + saldo_akhir
			FROM (
				SELECT 
					br.id AS relasi_kode, br.supp_name AS relasi_nama, k.kont_name AS nama_proyek, 
					SUM(CASE WHEN a.tanggal < '$strDateStart' THEN a.total ELSE 0 END) AS saldo_awal,
					SUM(CASE WHEN a.tanggal >= '$strDateStart' AND a.tanggal <= '$strDateEnd' AND pelunasan_nomor = 0 THEN a.total ELSE 0 END) AS beli,
					SUM(CASE WHEN a.tanggal >= '$strDateStart' AND a.tanggal <= '$strDateEnd' AND pelunasan_nomor <> 0 THEN ABS(a.total) ELSE 0 END) AS bayar,
					SUM(a.total) AS saldo_akhir
				FROM rhlaporanhutang a
					JOIN jw_supplier br ON a.nomormhsupplier = br.id
					JOIN kontrak k ON a.nomorkontrak = k.id
				WHERE jenis NOT LIKE 'um%'
				AND tanggal <= '$strDateEnd'
				AND a.nomormhsupplier LIKE '$intSupplierID'
				AND k.id LIKE '$intKontrakID'
				GROUP BY a.nomormhsupplier, k.id
				ORDER BY nomormhsupplier
			) a
			UNION 
			SELECT '', '', 'TOTAL', @sa, @beli, @bayar, @sakhir, 0, 0, 0, 0;
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportHutangDetail($strDateEnd,$intSupplierID,$intKontrakID) {
		if($intSupplierID == 0) $intSupplierID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @sisa = 0.00;");
		$this->setQuery("
			SELECT *, @sisa := @sisa + sisa
			FROM ( 
				SELECT
					br.id AS relasi_kode, br.supp_name AS relasi_nama, k.kont_name AS nama_proyek, 
					DATE_FORMAT(a.transaksi_tanggal,'%d-%m-%Y') AS transaksi_tanggal,
					a.transaksi_kode,
					GROUP_CONCAT(IF(a.pelunasan_nomor=0,NULL,a.pelunasan_kode) SEPARATOR ', ') AS kode_pelunasan,
					a.keterangan,
					SUM(a.total) AS sisa,
					IFNULL(a.jatuh_tempo, a.transaksi_tanggal) AS jatuh_tempo,
					CONCAT(br.id,' - ',br.supp_name) AS grup
				FROM rhlaporanhutang a
					JOIN jw_supplier br ON a.nomormhsupplier = br.id
					join kontrak k on a.nomorkontrak = k.id
				WHERE
					a.jenis NOT LIKE 'um%' AND
					a.nomormhsupplier LIKE '$intSupplierID' AND
					a.tanggal <= '$strDateEnd' and
					k.id like '$intKontrakID'
				GROUP BY a.nomormhsupplier, a.transaksi_kode, k.id
				HAVING sisa <> 0
				ORDER BY br.supp_name, a.transaksi_tanggal, a.transaksi_kode
			) a
			UNION 
			SELECT '', '', 'TOTAL', NULL, NULL, NULL, NULL, @sisa, NULL, NULL, NULL; 
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportHutangKartu($strDateStart,$strDateEnd,$intSupplierID,$intKontrakID) {
		if($intSupplierID == 0) $intSupplierID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @total = 0.00;");
		$this->setQuery("
			SELECT gab.*, (@total := @total + gab.debit - gab.kredit) AS total
			FROM (
				SELECT
					2 AS urutan,
					a.jenis,
					a.tanggal,
					k.kont_name AS nama_proyek,
					a.transaksi_nomor,
					a.transaksi_kode,
					a.pelunasan_kode,
					a.keterangan,
					IF(a.total > 0, a.total, 0) AS debit,
					IF(a.total < 0, -1 * a.total, 0) AS kredit
				FROM rhlaporanhutang a
					JOIN kontrak k ON a.nomorkontrak = k.id
				WHERE
					a.jenis NOT LIKE 'umc%' AND
					a.nomormhsupplier LIKE '$intSupplierID' AND
					a.nomorkontrak LIKE '$intKontrakID' AND
					a.tanggal >= '$strDateStart' AND
					a.tanggal <= '$strDateEnd'
				UNION ALL
				SELECT
					1 AS urutan,
					'SALDO_AWAL' AS jenis,
					'$strDateStart' AS tanggal,
					a.kont_name AS nama_proyek,
					0 AS transaksi_nomor,
					'SA' AS transaksi_kode,
					'' AS pelunasan_kode,
					'Saldo Awal',
					IF(IFNULL(a.total, 0) >= 0 , IFNULL(a.total, 0), 0) AS debit,
					IF(IFNULL(a.total, 0) < 0, -1 * IFNULL(a.total, 0), 0) AS kredit
				FROM jw_supplier br
					JOIN (
						SELECT
							za.nomormhsupplier,
							k.kont_name,
							SUM(za.total) AS total
						FROM rhlaporanhutang za
							JOIN kontrak k ON za.nomorkontrak = k.id
						WHERE
							za.jenis NOT LIKE 'um%' AND
							za.nomormhsupplier LIKE '$intSupplierID' AND
							za.tanggal < '$strDateStart' AND
							za.nomorkontrak LIKE '$intKontrakID'
						GROUP BY za.nomormhsupplier
					) a ON a.nomormhsupplier = br.id
				WHERE br.id LIKE '$intSupplierID'
			) gab
			ORDER BY gab.urutan, gab.tanggal, gab.transaksi_nomor;
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportPiutangRekap($strDateStart,$strDateEnd,$intCustomerID,$intKontrakID) {
		if($intCustomerID == 0) $intCustomerID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @sa = 0.00;");
		$this->setQuery("SET @jual = 0.00;");
		$this->setQuery("SET @bayar = 0.00;");
		$this->setQuery("SET @sakhir = 0.00;");
		$this->setQuery("
			SELECT *, @sa := @sa + saldo_awal, @jual := @jual + jual, @bayar := @bayar + bayar, @sakhir := @sakhir + saldo_akhir
			FROM (
				SELECT 
					br.id AS relasi_kode, br.cust_name AS relasi_nama, k.kont_name AS nama_proyek, 
					SUM(CASE WHEN a.tanggal < '$strDateStart' THEN a.total ELSE 0 END) AS saldo_awal,
					SUM(CASE WHEN a.tanggal >= '$strDateStart' AND a.tanggal <= '$strDateEnd' AND pelunasan_nomor = 0 THEN a.total ELSE 0 END) AS jual,
					SUM(CASE WHEN a.tanggal >= '$strDateStart' AND a.tanggal <= '$strDateEnd' AND pelunasan_nomor <> 0 THEN ABS(a.total) ELSE 0 END) AS bayar,
					SUM(a.total) AS saldo_akhir
				FROM rhlaporanpiutang a
					JOIN jw_customer br ON a.nomormhcustomer = br.id
					join kontrak k on a.nomorkontrak = k.id
				WHERE jenis NOT LIKE 'umc%'
				AND tanggal <= '$strDateEnd'
				AND a.nomormhcustomer LIKE '$intCustomerID'
				AND k.id LIKE '$intKontrakID'
				GROUP BY a.nomormhcustomer, k.id
				ORDER BY nomormhcustomer
			) a
			UNION 
			SELECT '', '', 'TOTAL', @sa, @jual, @bayar, @sakhir, 0, 0, 0, 0;
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportPiutangDetail($strDateEnd,$intCustomerID,$intKontrakID) {
		if($intCustomerID == 0) $intCustomerID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @sisa = 0.00;");
		$this->setQuery("
			SELECT *, @sisa := @sisa + sisa
			FROM ( 
				SELECT
					br.id AS relasi_kode, br.cust_name AS relasi_nama, k.kont_name AS nama_proyek, 
					DATE_FORMAT(a.transaksi_tanggal,'%d-%m-%Y') AS transaksi_tanggal,
					a.transaksi_kode,
					GROUP_CONCAT(IF(a.pelunasan_nomor=0,NULL,a.pelunasan_kode) SEPARATOR ', ') AS kode_pelunasan,
					a.keterangan,
					SUM(a.total) AS sisa,
					IFNULL(a.jatuh_tempo, a.transaksi_tanggal) AS jatuh_tempo,
					CONCAT(br.id,' - ',br.cust_name) AS grup
				FROM rhlaporanpiutang a
					JOIN jw_customer br ON a.nomormhcustomer = br.id
					join kontrak k on a.nomorkontrak = k.id
				WHERE
					a.jenis NOT LIKE 'umc%' AND
					a.nomormhcustomer LIKE '$intCustomerID' AND
					a.tanggal <= '$strDateEnd' and
					k.id like '$intKontrakID'
				GROUP BY a.nomormhcustomer, a.transaksi_kode, k.id
				HAVING sisa <> 0
				ORDER BY br.cust_name, a.transaksi_tanggal, a.transaksi_kode
				) a
			UNION 
			SELECT '', '', 'TOTAL', NULL, NULL, NULL, NULL, @sisa, NULL, NULL, NULL; 
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportPiutangKartu($strDateStart,$strDateEnd,$intCustomerID,$intKontrakID) {
		if($intCustomerID == 0) $intCustomerID = '%';
		if($intKontrakID == 0) $intKontrakID = '%';
		$this->setQuery("SET @total = 0.00;");
		$this->setQuery("
			SELECT gab.*, (@total := @total + gab.debit - gab.kredit) AS total
			FROM (
				SELECT
					2 AS urutan,
					a.jenis,
					a.tanggal,
					k.kont_name AS nama_proyek,
					a.transaksi_nomor,
					a.transaksi_kode,
					a.pelunasan_kode,
					a.keterangan,
					IF(a.total > 0, a.total, 0) AS debit,
					IF(a.total < 0, -1 * a.total, 0) AS kredit
				FROM rhlaporanpiutang a
					JOIN kontrak k ON a.nomorkontrak = k.id
				WHERE
					a.jenis NOT LIKE 'umc%' AND
					a.nomormhcustomer LIKE '$intCustomerID' AND
					a.nomorkontrak LIKE '$intKontrakID' AND
					a.tanggal >= '$strDateStart' AND
					a.tanggal <= '$strDateEnd'
				UNION ALL
				SELECT
					1 AS urutan,
					'SALDO_AWAL' AS jenis,
					'$strDateStart' AS tanggal,
					a.kont_name AS nama_proyek,
					0 AS transaksi_nomor,
					'SA' AS transaksi_kode,
					'' AS pelunasan_kode,
					'Saldo Awal',
					IF(IFNULL(a.total, 0) >= 0 , IFNULL(a.total, 0), 0) AS debit,
					IF(IFNULL(a.total, 0) < 0, -1 * IFNULL(a.total, 0), 0) AS kredit
				FROM jw_customer br
					JOIN (
						SELECT
							za.nomormhcustomer,
							k.kont_name,
							SUM(za.total) AS total
						FROM rhlaporanpiutang za
							JOIN kontrak k ON za.nomorkontrak = k.id
						WHERE
							za.jenis NOT LIKE 'umc%' AND
							za.nomormhcustomer LIKE '$intCustomerID' AND
							za.tanggal < '$strDateStart' AND
							za.nomorkontrak LIKE '$intKontrakID'
						GROUP BY za.nomormhcustomer
					) a ON a.nomormhcustomer = br.id
				WHERE br.id LIKE '$intCustomerID'
			) gab
			ORDER BY gab.urutan, gab.tanggal, gab.transaksi_nomor;
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportAccounting($strAccountStart,$strAccountEnd,$strDateStart,$strDateEnd,$intMonth,$intYear,$type,$strTransactionCode) {
		$this->setQuery("CALL dbg_rp_jurnal_20190406('$strAccountStart', '$strAccountEnd', '$strDateStart', '$strDateEnd', $intMonth, $intYear, $type, '$strTransactionCode');");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportKasBank($strAccountStart,$strDateStart,$strDateEnd) {
		$this->setQuery("CALL rp_harian_20190408('$strAccountStart', '$strDateStart', '$strDateEnd');");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportBiayaProyek($strDateEnd) {
		$this->setQuery("CALL rp_biayaproyek('$strDateEnd');");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportBiayaPemasukanProyek($intKontrakID,$intBulan,$intTahun) {
		$this->setQuery("CALL rp_biayaproyek_detail($intKontrakID,$intBulan,$intTahun);");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportMutasiBiaya($strAccountStart,$strDateStart,$strDateEnd) {
		$this->setQuery("CALL rp_mutasi_account_biaya('$strAccountStart', '$strDateStart', '$strDateEnd');");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
	}

	public function reportPenerimaanPengeluaranProyek($strType,$strDateEnd,$idkontrak = "%", $jenis = "%")
	{
		switch ($strType) {
			case 'rekap':
				$this->setQuery("CALL rp_penerimaan_pengeluaran_proyek_20190531('$strDateEnd')");
				break;
			case 'detail':
				$this->setQuery("CALL rp_penerimaan_pengeluaran_proyek_detail_20190627('$strDateEnd','$idkontrak','$jenis')");
				break;
		}		

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function reportHutangBelumDifakturkan($strDateStart, $strDateEnd, $intProject, $intMode)
	{
		$this->setQuery("CALL rp_hutang_yang_belum_difakturkan_20190601('$strDateStart','$strDateEnd','$intProject', '$intMode')");

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

}