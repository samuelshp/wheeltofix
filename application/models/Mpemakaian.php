<?php
/*
PUBLIC FUNCTION:

PRIVATE FUNCTION:
- __construct()
*/

class Mpemakaian extends JW_Model {

	// Constructor
	public function __construct() {
		parent::__construct();
		$this->initialize('pemakaian');
	}

	public function add($arrPemakaian){
		return $this->dbInsert(array(
			'kontrak_id' => $arrPemakaian['kontrak_id'],
			'subkontrak_id' => $arrPemakaian['subkontrak_id'],
			'warehouse_id' => $arrPemakaian['warehouse_id'],
			'status' => STATUS_APPROVED,
		));
	}

	public function searchBox($proyek='',$tglProyek=''){
		$tglProyek = str_replace('/','-', $tglProyek);
		if($proyek !='' && $tglProyek != ''){
			$strWhere = "(k.kont_name LIKE '%$proyek%' OR sk.job LIKE '%$proyek%') && DATE(p.cdate) = '$tglProyek'";
		}else if($proyek != ''){ //berdasarkan proyek/kontrak
			$strWhere = "k.kont_name LIKE '%$proyek%' OR sk.job LIKE '%$proyek%'";
		}
		else{ //berdasarkan tanggal
			$strWhere = "DATE(p.cdate) LIKE '%$tglProyek%'";
		}

		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.cdate, k.kont_name, p.kontrak_id, p.id, sk.job as kont_job, sk.subkont_kode
			from pemakaian as p 
			LEFT JOIN kontrak as k ON k.id = p.kontrak_id 
			LEFT JOIN subkontrak as sk ON sk.id = p.subkontrak_id
			WHERE $strWhere $whereInProject");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getCount() {
		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.cdate, k.kont_name, p.kontrak_id, p.id, sk.job as kont_job, sk.subkont_kode
			from pemakaian as p 
			LEFT JOIN kontrak as k ON k.id = p.kontrak_id 
			LEFT JOIN subkontrak as sk ON sk.id = p.subkontrak_id
			WHERE p.status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject"
		);
		return $this->getNumRows();
	}

	public function getItems($intStartNo = -1,$intPerPage = -1){
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC";
		else $strOrderBy = "p.cdate DESC LIMIT $intStartNo, $intPerPage";

		$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    $whereInProject .= ")";

		$this->setQuery(
			"SELECT p.cdate, k.kont_name, p.kontrak_id, p.id, sk.job as kont_job, sk.subkont_kode, p.status
			from pemakaian as p 
			LEFT JOIN kontrak as k ON k.id = p.kontrak_id 
			LEFT JOIN subkontrak as sk ON sk.id = p.subkontrak_id
			WHERE p.status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.",".STATUS_REJECTED.") $whereInProject
			order by $strOrderBy"
		);
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getAllKontrak($orang=0){
		if($orang == 0){
			$this->setQuery(
			"SELECT * 
			from kontrak
			ORDER BY kont_name ASC
			");
		}
		else{
			$this->setQuery(
				"SELECT k.kont_name, k.id, k.kont_code
				FROM subkontrak_team as skt
				LEFT JOIN subkontrak as sk ON sk.id = skt.subkontrak_id
				LEFT JOIN kontrak as k ON k.id = sk.kontrak_id
				WHERE skt.name = $orang
				GROUP BY sk.kontrak_id
                ORDER BY k.kont_name ASC"
			);
		}

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getItem($intID)
	{
		$this->setQuery(
			"SELECT p.id, p.kontrak_id, k.kont_name as proyek, p.subkontrak_id, sk.job as kontrak, p.warehouse_id, t.ware_name as gudang, p.status
			FROM pemakaian as p 
			LEFT JOIN kontrak as k ON k.id = p.kontrak_id 
			LEFT JOIN subkontrak as sk ON sk.id = p.subkontrak_id
			LEFT JOIN jw_warehouse as t ON t.id = p.warehouse_id
			WHERE p.id = $intID 
			AND p.status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.",".STATUS_REJECTED.")"
		);

		if ($this->getNumRows() > 0 ) return $this->getNextRecord('Array');
		else return false;
	}

	public function delete($intID)
	{
		return $this->dbUpdate(array('status' => STATUS_DELETED), "id=$intID");
	}
}
/* End of File */