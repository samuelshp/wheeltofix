<?php

class Msubkontraknonmaterial extends JW_Model {

    // Constructor
    public function __construct() { 
        parent::__construct();
        $this->initialize('subkontrak_nonmaterial');
    }

    public function add($arrDataKategori, $intSubkontrakID)
    {
        $intCount = count($arrDataKategori['kategori']);
        $i = 0;
        while($i < $intCount) {
            $dbInsert = $this->dbInsert(array(
                'subkontrak_id' => $intSubkontrakID,
                'kategori'  => $arrDataKategori['kategori'][$i],
                'amount' => str_replace(".","",$arrDataKategori['amount'][$i])           
            ));
            if($dbInsert) $i++;
            else return false;
        }        
        return true;
    }

    public function update($row_id, $intSubkontrak_id, $strKategori, $intAmount)
    {
        if(empty($row_id)) {
            $this->dbInsert(array(
                'subkontrak_id' => $intSubkontrak_id,
                'kategori' => $strKategori,
                'amount' => $intAmount,            
            ));
        } else {
            $this->dbUpdate(array(
                'subkontrak_id' => $intSubkontrak_id,
                'kategori' => $strKategori,
                'amount' => $intAmount,
            ), "id= '$row_id'");    
        }
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function deleteItem($id)
    {
        return $this->dbUpdate(array(
            'status' => 1
        ),"id = $id");
    }

}