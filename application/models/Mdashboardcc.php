<?php
/*
PUBLIC FUNCTION:
- getAllCustomer(strKeyword)
- getAllCustomerWithSupplierBind(strKeyword,intSupplier)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdashboardcc extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('subkontrak_material');
}

public function getAllData($subkontrak_id){

    $this->setQuery(
        "SELECT jp.prod_title, sm.qty, jp.satuan_bayar_id, sm.keterangan, jp.prod_price
        FROM subkontrak_material as sm
        LEFT JOIN jw_product as jp ON jp.id = sm.material
        WHERE sm.subkontrak_id = $subkontrak_id"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllDataByNamaBahan($id_bahan){

    $this->setQuery(
        "SELECT jp.prod_title, sm.qty, jp.satuan_bayar_id, sm.keterangan, jp.prod_price
        FROM subkontrak_material as sm
        LEFT JOIN jw_product as jp ON jp.id = sm.material
        WHERE jp.id = $id_bahan"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllOwner(){
    $this->initialize('jw_customer');
    $this->setQuery(
        "SELECT * from jw_customer"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllBahan(){
    $this->initialize('jw_product');
    $this->setQuery(
        "SELECT * from jw_product"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getNamaProyekByOwner($idOwner){
    $this->initialize('kontrak');
    $this->setQuery(
        "SELECT * from kontrak where owner_id = $idOwner"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getNamaPekerjaanByIdProyek($idProyek){
    $this->initialize('subkontrak');
    $this->setQuery(
        "SELECT * from subkontrak where kontrak_id = $idProyek"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getProductByKontrakAndSubkontrak($kontrak,$subkontrak,$bahan){
    $this->initialize('subkontrak_material');
    if($bahan!=0){
        $this->setQuery(
            "SELECT skm.id, skm.qty, skm.material, skm.keterangan, skm.jumlah, jp.prod_title, ju1.unit_title as satuan_bayar, ju2.unit_title as satuan_pb, ju3.unit_title as satuan_terima
            from subkontrak_material as skm
            LEFT JOIN jw_product as jp on jp.id = skm.material
            LEFT JOIN jw_unit as ju1 ON ju1.id = jp.satuan_bayar_id
            LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_pb_id
            LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id
            where subkontrak_id = $subkontrak AND skm.material = $bahan
            GROUP BY skm.id"
        );
    }
    else{
        $this->setQuery(
            "SELECT skm.id, skm.qty, skm.material, skm.keterangan, skm.jumlah, jp.prod_title, ju1.unit_title as satuan_bayar, ju2.unit_title as satuan_pb, ju3.unit_title as satuan_terima
            from subkontrak_material as skm
            LEFT JOIN jw_product as jp on jp.id = skm.material
            LEFT JOIN jw_unit as ju1 ON ju1.id = jp.satuan_bayar_id
            LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_pb_id
            LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id
            where subkontrak_id = $subkontrak
            GROUP BY skm.id"
        );
    }
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getDetailProduct($idProduct, $subkontrak_id){
    $this->initialize('purchase_order');
    $this->setQuery(
        "SELECT SUM(pi.prci_quantity1) as prci_quantity1, pi.prci_price, pi.prci_subtotal, SUM(pii.pinvit_quantity) as pinvit_quantity, pii.pinvit_subtotal, pii.pinvit_price, po.id, p.id, if(pi.prci_price > 0, SUM(pii.pinvit_quantity*pi.prci_price), SUM(pii.pinvit_quantity*pii.pinvit_price)) as total, SUM(pi.prci_quantity1*pi.prci_price) as total2
        FROM purchase_order as po
        LEFT JOIN purchase_order_item as poi ON poi.proi_purchase_order_id = po.id
        LEFT JOIN purchase as p ON p.prch_po_id = po.id
        LEFT JOIN purchase_item as pi ON pi.prci_purchase_id = p.id AND pi.prci_product_id = $idProduct
        LEFT JOIN purchase_invoice as pi2 ON pi2.pinv_purchase_id = p.id
        LEFT JOIN purchase_invoice_item as pii ON pii.pinvit_product_id = $idProduct AND pii.pinvit_purchase_invoice_id = pi2.id
        WHERE po.idSubKontrak = $subkontrak_id AND p.id > 0 AND poi.proi_product_id = $idProduct OR pii.pinvit_subtotal >= 0 AND po.pror_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCostControlDataByProject($intSubkontrakID,$intProductID) {
    if($intProductID == 0) $intProductID = "%";
    $this->setQuery("CALL sp_dashboard_cc_20190613(" . $intSubkontrakID . ",'" . $intProductID . "')");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCostControlNonMaterial($intSubkontrakID, $intProductID)
{
    if($intProductID == 0) $intProductID = "%";
    $this->setQuery("CALL sp_dashboard_cc_non_material_20190924('" . $intSubkontrakID . "','" . $intProductID . "')");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}


}