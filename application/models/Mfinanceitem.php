<?php
/*
PUBLIC FUNCTION:
- getItemsByFinanceID(intFinanceID)
- add(intFinanceID,intDestination,strTitle,strDueDate,intPrice,intStatus)
- editByID(intID,intStatus)
- editByID2(intID,strDueDate,intPrice,intStatus)
- deleteByFinanceID(intFinanceID)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mfinanceitem extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('finance_item');
}

public function getItemsByFinanceID($intFinanceID) {
	$this->dbSelect('id,fini_to,fini_title,fini_duedate,fini_price,fini_status,cdate AS fini_date',"fini_finance_id = $intFinanceID");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intFinanceID,$intDestination,$strTitle,$strDueDate,$intPrice,$intStatus) {
	return $this->dbInsert(array(
		'fini_finance_id' => $intFinanceID,
		'fini_to' => $intDestination,
		'fini_title' => $strTitle,
		'fini_duedate' => formatDate2($strDueDate,'Y-m-d'),
		'fini_price' => $intPrice,
		'fini_status' => $intStatus));
}

public function editByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'fini_status' => $intStatus),"id = $intID");
}

public function editByID2($intID,$strDueDate,$intPrice,$intStatus) {
	return $this->dbUpdate(array(
		'fini_duedate' => formatDate2($strDueDate,'Y-m-d'),
		'fini_price' => $intPrice,
		'fini_status' => $intStatus),"id = $intID");
}

public function deleteByFinanceID($intFinanceID) {
	return $this->dbDelete("fini_finance_id = $intFinanceID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */