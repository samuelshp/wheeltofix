<?php
/**
 * 
 */
class Muangmasukcustomer extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('uang_masuk_customer');
	}

	public function getListOfData($intStartNo = -1, $intPerPage = -1)
	{
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "umc.id DESC";
		else $strOrderBy = "umc.id DESC LIMIT $intStartNo, $intPerPage";

		$this->setQuery("SELECT umc.id, umcu_code, umcu_date, cust_name, umcu_amount FROM uang_masuk_customer AS umc
			JOIN jw_customer AS cust ON umc.umcu_cust_id = cust.id
			WHERE umcu_status > ".STATUS_DELETED."
			ORDER BY $strOrderBy
		");

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getCount()
	{
		$this->dbSelect('id', "umcu_status > ".STATUS_DELETED);
		return $this->getNumRows();
	}	

	public function getDetail($intID)
	{
		$this->setQuery("
			SELECT umc.id, umcu_code, umcu_date,umcu_type, umcu_ref_giro, umcu_jatuhtempo, umcu_acco_id, umcu_cust_id, umcu_amount, umc.cby, cust_name, acco.acco_name FROM uang_masuk_customer AS umc
			LEFT JOIN jw_customer AS cust ON umc.umcu_cust_id = cust.id
			LEFT JOIN jw_account as acco ON umc.umcu_acco_id = acco.id
			WHERE umc.id = '$intID'
		");

		if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}

	public function add($arrData)
	{		
		return $this->dbInsert(array(
			'umcu_code' => $arrData['umcu_code'],
            'umcu_date' => str_replace("/", "-", $arrData['umcu_date']),
            'umcu_cust_id' => $arrData['umcu_cust_id'],
            'umcu_acco_id' => $arrData['umcu_acco_id'],
            'umcu_type' => $arrData['umcu_type'],
            'umcu_ref_giro' => $arrData['umcu_ref_giro'],
            'umcu_jatuhtempo' => str_replace("/", "-", $arrData['umcu_jatuhtempo']),
            'umcu_amount' => str_replace(",", ".", str_replace(".", "", $arrData['umcu_amount'])),              
		));
	}

	public function update($arrData, $intID)
	{
		return $this->dbUpdate(array(			
            'umcu_date' =>  str_replace("/", "-", $arrData['umcu_date']),
            'umcu_cust_id' => $arrData['umcu_cust_id'],
            'umcu_acco_id' => $arrData['umcu_acco_id'],
            'umcu_type' => $arrData['umcu_type'],
            'umcu_ref_giro' => $arrData['umcu_ref_giro'],
            'umcu_jatuhtempo' =>  str_replace("/", "-", $arrData['umcu_jatuhtempo']),
            'umcu_amount' => str_replace(",", ".", str_replace(".", "", $arrData['umcu_amount'])),
		), "id = '$intID'");
	}

	public function delete($intID)
	{		
		return $this->dbUpdate(
			array(
				'umcu_status' => STATUS_DELETED
			), "id='$intID'"
		);
	}

	public function searchBy($arrSearchData)
	{
		$strWhere = '';
		if ($arrSearchData['umcu_date_start'] != '' && $arrSearchData['umcu_date_end'] == '') $strWhere .= " AND umcu_date = '".str_replace("/", "-",$arrSearchData['umcu_date_start'])."'";
		if ($arrSearchData['umcu_date_end'] != '') $strWhere .= " AND (umcu_date BETWEEN '".str_replace("/", "-",$arrSearchData['umcu_date_start'])."' AND '".str_replace("/", "-",$arrSearchData['umcu_date_end'])."')";
		if ($arrSearchData['umcu_code'] != '') $strWhere .= " AND umcu_code LIKE '%".$arrSearchData['umcu_code']."%'";
		if ($arrSearchData['umcu_cust'] != '') $strWhere .= " AND (cust_name LIKE '%".$arrSearchData['umcu_cust']."%')";

		$this->setQuery("
			SELECT umcu.id, umcu_code, umcu_date, cust_name, umcu_amount FROM
			uang_masuk_customer as umcu			
			LEFT JOIN jw_customer AS cust ON umcu_cust_id = cust.id
			WHERE umcu_code LIKE '$strType%' AND umcu_status > ".STATUS_DELETED." $strWhere 
			GROUP BY umcu.id ORDER BY umcu_date DESC, umcu.id DESC			
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
}