<?php
namespace App\Models;

use \Closure as Closure;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class DBFacadesWrapper extends EloquentModel
{
    public static function table($table)
    {
        self::getConnectionResolver()->connection()->table($table);
    }

    public static function beginTransaction()
    {
        self::getConnectionResolver()->connection()->beginTransaction();
    }

    public static function commit()
    {
        self::getConnectionResolver()->connection()->commit();
    }

    public static function rollBack()
    {
        self::getConnectionResolver()->connection()->rollBack();
    }

    public static function transaction(Closure $callback)
    {
        self::beginTransaction();
        try
        {
                $result = $callback(__CLASS__);
                self::commit();
        }
        catch (\Exception $e)
        {
                self::rollBack();
                throw $e;
        }
        return $result;
    }

    public static function select($query, $bindings = [], $useReadPdo = true)
    {
        self::getConnectionResolver()->connection()->select($query, $bindings = [], $useReadPdo = true);
    }

    public static function raw($value)
    {
        self::getConnectionResolver()->connection()->raw($value);
    }
}