<?php
/*
PUBLIC FUNCTION:
- getDynamicCode(strTableName,intID)
- getQuantityByProductID(intProductID,intWarehouseID)
- getHistoryByProductID(intProductID,intWarehouseID)

PRIVATE FUNCTION:
- 
*/

class Mtax extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
}


public function getAllDataForTax($txtName='',$txtStart='',$txtEnd='',$intStartNo = -1,$intPerPage = -1) {
    if(!empty($txtStart) || !empty($txtEnd)){
        $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
        $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
        $txtDate = "AND tanggal BETWEEN '$txtStart' AND '$txtEnd'";
    }else{
        $txtDate='';
    }

    if($intStartNo == -1 || $intPerPage == -1){
        $strPage='';
    }else{
        $strPage=" LIMIT $intStartNo, $intPerPage";
    }
    if(!empty($txtName)) {
        $strWherePurchase = " AND (supp_name LIKE '%$txtName%')";
        $strWherePurchaseRetur = " AND (supp_name LIKE '%$txtName%')";
        $strWhereInvoice = " AND (cust_name LIKE '%$txtName%')";
        $strWhereInvoiceRetur = " AND (cust_name LIKE '%$txtName%')";
        $strWherePayment = " AND (sals_name LIKE '%$txtName%')";
        $strWhereRepayment = " AND (supp_name LIKE '%$txtName%')";
    } else {
        $strWherePurchase = '';
        $strWherePurchaseRetur = '';
        $strWhereInvoice = '';
        $strWhereInvoiceRetur = '';
        $strWherePayment = '';
        $strWhereRepayment = '';
    }
    $this->setQuery(
"SELECT id,tipe,numbertipe,kode,nama,total,tanggal,checked
FROM (
    (
      SELECT p1.id,'purchase' AS tipe,'1' AS numbertipe,s1.supp_name AS nama, p1.prch_code AS kode, p1.prch_grandtotal AS total, p1.cdate AS tanggal,p1.prch_taxin AS checked
      FROM purchase AS p1
      LEFT JOIN jw_supplier AS s1 ON s1.id = p1.prch_supplier_id
      WHERE 1=1 $txtDate $strWherePurchase
    )
    UNION
    (
      SELECT pr2.id,'purchase_retur' AS tipe,'2' AS numbertipe,s2.supp_name AS nama, pr2.prcr_code AS kode, pr2.prcr_grandtotal AS total, pr2.cdate AS tanggal,pr2.prcr_taxin AS checked
      FROM purchase_retur AS pr2
      LEFT JOIN jw_supplier AS s2 ON s2.id = pr2.prcr_supplier_id
      WHERE 1=1 $txtDate $strWherePurchaseRetur
    )
    UNION
    (
      SELECT i3.id,'invoice' AS tipe,'3' AS numbertipe,c3.cust_name AS nama, i3.invo_code AS kode, i3.invo_grandtotal AS total, i3.cdate AS tanggal,i3.invo_taxin AS checked
      FROM invoice as i3
      LEFT JOIN jw_customer AS c3 ON c3.id = i3.invo_customer_id
      WHERE 1=1 $txtDate $strWhereInvoice
    )
    UNION
    (
      SELECT ir4.id,'invoice_retur' AS tipe,'4' AS numbertipe,c4.cust_name AS nama, ir4.invr_code AS kode, ir4.invr_grandtotal AS total, ir4.cdate AS tanggal,ir4.invr_taxin AS checked
      FROM invoice_retur AS ir4
      LEFT JOIN jw_customer AS c4 ON c4.id = ir4.invr_customer_id
      WHERE 1=1 $txtDate $strWhereInvoiceRetur
    )
    UNION
    (
      SELECT pa5.id,'payment' AS tipe,'5' AS numbertipe,s5.sals_name AS nama, ph5.pahe_code AS kode, i5.invo_grandtotal AS total, pa5.cdate AS tanggal,pa5.paym_taxin AS checked
      FROM payment AS pa5
      LEFT JOIN  payment_header AS ph5 ON pa5.paym_header_id = ph5.id
      LEFT JOIN  jw_salesman AS s5 ON pa5.paym_salesman_id = s5.id
      LEFT JOIN  invoice AS i5 ON pa5.paym_invoice_id = i5.id
      WHERE 1=1 $txtDate $strWherePayment
    )
    UNION
    (
      SELECT r6.id,'repayment' AS tipe,'6' AS numbertipe,s6.supp_name AS nama, rh6.reha_code AS kode, p6.prch_grandtotal AS total, r6.cdate AS tanggal,r6.repa_taxin AS checked
      FROM repayment AS r6
      LEFT JOIN repayment_header AS rh6 ON r6.repa_header_id = rh6.id
      LEFT JOIN purchase AS p6 ON r6.repa_purchase_id = p6.id
      LEFT JOIN jw_supplier AS s6 ON s6.id = p6.prch_supplier_id
      WHERE 1=1 $txtDate $strWhereRepayment
    )
    UNION
    (
      SELECT ac7.id,'accountadjustment' AS tipe,'7' AS numbertipe,'account adjustment' AS nama, ac7.acad_code AS kode, ac7.acad_grandtotal AS total, ac7.cdate AS tanggal,ac7.acad_taxin AS checked
      FROM accountadjustment AS ac7
      WHERE 1=1 $txtDate
    )
) AS t
ORDER BY tanggal DESC,kode DESC ,id DESC $strPage");
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount() {
    $this->setQuery(
"SELECT SUM(c) AS c
FROM (
    SELECT COUNT(id) AS c,'purchase' AS tipe FROM purchase
    UNION
    SELECT COUNT(id) AS c,'purchase_retur' AS tipe FROM purchase_retur
    UNION
    SELECT COUNT(id) AS c,'invoice' AS tipe FROM invoice
    UNION
    SELECT COUNT(id) AS c,'invoice_retur' AS tipe FROM invoice_retur
    UNION
    SELECT COUNT(id) AS c,'payment' AS tipe FROM payment
    UNION
    SELECT COUNT(id) AS c,'repayment' AS tipe FROM repayment
    UNION
    SELECT COUNT(id) AS c,'accountadjustment' AS tipe FROM accountadjustment
) AS t
");
    return $this->getNextRecord('Object')->c;
}

public function setPurchaseTaxedOrNot($intID,$intStatus) {
    $this->initialize('purchase');
    $this->dbUpdate(array(
            'prch_taxin' => $intStatus),
        "id = $intID");
    return true;
}

public function setPurchaseReturTaxedOrNot($intID,$intStatus) {
    $this->initialize('purchase_retur');
    $this->dbUpdate(array(
            'prcr_taxin' => $intStatus),
        "id = $intID");
    return true;
}
public function setInvoiceTaxedOrNot($intID,$intStatus) {
    $this->initialize('invoice');
    $this->dbUpdate(array(
            'invo_taxin' => $intStatus),
        "id = $intID");
    return true;
}
public function setInvoiceReturTaxedOrNot($intID,$intStatus) {
    $this->initialize('invoice_retur');
    $this->dbUpdate(array(
            'invr_taxin' => $intStatus),
        "id = $intID");
    return true;
}
public function setPaymentTaxedOrNot($intID,$intStatus) {
    $this->initialize('payment');
    $this->dbUpdate(array(
            'paym_taxin' => $intStatus),
        "id = $intID");
    return true;
}
public function setRepaymentTaxedOrNot($intID,$intStatus) {
    $this->initialize('repayment');
    $this->dbUpdate(array(
            'repa_taxin' => $intStatus),
        "id = $intID");
    return true;
}
public function setAccountAdjustmentTaxedOrNot($intID,$intStatus) {
    $this->initialize('accountadjustment');
    $this->dbUpdate(array(
            'acad_taxin' => $intStatus),
        "id = $intID");
    return true;
}
}

/* End of File */