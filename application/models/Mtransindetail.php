<?php
/**
 * 
 */
class Mtransindetail extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('transaction_in_detail');
	}

	public function add($arrDataDetail, $bolFilterBy = false)
	{				
		if($bolFilterBy){
			foreach ($arrDataDetail['txid_filter_by'] as $i => $value){
				if ($value != '') {					
					$item = explode("-", $value);
					$id = $i;					
					$dbInsert = $this->dbInsert(array(				
						'txid_txin_id' => (isset($arrDataDetail['txid_txin_id'][$id]) ? $arrDataDetail['txid_txin_id'][$id] : $arrDataDetail['txid_txin_id']),	
						'txid_txin_pph' => (isset($arrDataDetail['txid_txin_pph'][$id]) ? str_replace(",", ".", $arrDataDetail['txid_txin_pph'][$id]) : 0),		
						'txid_amount' => (isset($arrDataDetail['txid_amount'][$id]) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_amount'][$id])) : str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_amount']))),
						'txid_totalbersih' => (isset($arrDataDetail['txid_totalbersih'][$id]) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_totalbersih'][$id])) : 0),
						'txid_type' => (isset($arrDataDetail['txid_type']) ? $arrDataDetail['txid_type'] : "-"),
						'txid_ref_id' => (isset($arrDataDetail['txid_ref_id']) ? $arrDataDetail['txid_ref_id'][$id] : $arrDataDetail['txid_ref_id']),
						'txid_ref_giro' => (isset($arrDataDetail['txid_ref_giro'][$id]) ? $arrDataDetail['txid_ref_giro'][$id] : $arrDataDetail['txid_ref_giro']),
						'txid_jatuhtempo' => (isset($arrDataDetail['txid_jatuhtempo'][$id]) ? str_replace("/", "-", $arrDataDetail['txid_jatuhtempo'][$id]) : str_replace("/", "-", $arrDataDetail['txid_jatuhtempo'])),
						'txid_description' => (isset($arrDataDetail['txid_description'][$id]) ?  $arrDataDetail['txid_description'][$id] : $arrDataDetail['txid_description'])				
					));
					if(!$dbInsert) return false;				
				}
			}
		}else{	
			$intCount = count($arrDataDetail['txid_amount']);
		    $i = 0;
		    while ($i < $intCount) {	    	
	       		$dbInsert = $this->dbInsert(array(		
					'txid_txin_id' => (isset($arrDataDetail['txid_txin_id'][$i]) ? $arrDataDetail['txid_txin_id'][$i] : $arrDataDetail['txid_txin_id']),	
					'txid_txin_pph' => (isset($arrDataDetail['txid_txin_pph'][$i]) ? str_replace(",", ".", $arrDataDetail['txid_txin_pph'][$i]) : 0),		
					'txid_amount' => (isset($arrDataDetail['txid_amount'][$i]) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_amount'][$i])) : str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_amount']))),
					'txid_totalbersih' => (isset($arrDataDetail['txid_totalbersih'][$i]) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_totalbersih'][$i])) : 0),
					'txid_type' => (isset($arrDataDetail['txid_type']) ? $arrDataDetail['txid_type'] : "-"),
					'txid_ref_id' => (isset($arrDataDetail['txid_ref_id']) ? $arrDataDetail['txid_ref_id'][$i] : $arrDataDetail['txid_ref_id']),
					'txid_acco_id' => (isset($arrDataDetail['txid_coa']) ? $arrDataDetail['txid_coa'][$i] : null),
					'txid_ref_giro' => (isset($arrDataDetail['txid_ref_giro'][$i]) ? $arrDataDetail['txid_ref_giro'][$i] : $arrDataDetail['txid_ref_giro']),
					'txid_jatuhtempo' => (isset($arrDataDetail['txid_jatuhtempo'][$i]) ? str_replace("/", "-", $arrDataDetail['txid_jatuhtempo'][$i]) : str_replace("/", "-", $arrDataDetail['txid_jatuhtempo'])),
					'txid_description' =>(isset($arrDataDetail['txid_description'][$i]) ?  $arrDataDetail['txid_description'][$i] : $arrDataDetail['txid_description'])				
				));

				if($dbInsert) $i++;
		        else return false;
	    	}
	    }
    	return $dbInsert;
	}

	public function update($arrDataDetail)
	{
		$intCount = count($arrDataDetail['txid_amount']);
	    $i = 0;
	    while ($i < $intCount) {	    	
       		$dbUpdate = $this->dbUpdate(array(				
       			'txid_txin_pph' => (isset($arrDataDetail['txid_txin_pph'][$i]) ? str_replace(",", ".", $arrDataDetail['txid_txin_pph'][$i]) : 0),		
				'txid_amount' => str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_amount'][$i])),		
				'txid_totalbersih' => str_replace(",", ".", str_replace(".", "", $arrDataDetail['txid_totalbersih'][$i])),	
				'txid_acco_id' => (isset($arrDataDetail['txid_coa']) ? $arrDataDetail['txid_coa'][$i] : null),
				'txid_ref_giro' => (isset($arrDataDetail['txid_ref_giro']) ?  $arrDataDetail['txid_ref_giro'] : null),
				'txid_jatuhtempo' => (isset($arrDataDetail['txid_jatuhtempo']) ? str_replace("/", "-", $arrDataDetail['txid_jatuhtempo']) : null),
				'txid_type' => $arrDataDetail['txid_type'],
				'txid_ref_id' => $arrDataDetail['txid_ref_id'][$i],
				'txid_description' =>(isset($arrDataDetail['txid_description']) ? $arrDataDetail['txid_description'][$i] : null)
			),"id='".$arrDataDetail['txid_txin_id'][$i]."'");

			if($dbUpdate) $i++;
	        else return false;
    	}		

    	return true;
	}

	public function delete($intID)
	{
		// return $this->dbDelete("id='$intID'");
		return true;
	}
}