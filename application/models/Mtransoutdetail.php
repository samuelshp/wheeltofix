<?php
/**
 * 
 */
class Mtransoutdetail extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('transaction_out_detail');
	}

	public function add($arrDataDetail, $bolFilterBy = false)
	{		
		if($bolFilterBy){			
			foreach ($arrDataDetail['txod_filter_by'] as $i => $value){
				$item = explode("-", $value);
				$id = $item[0];
				$dbInsert = $this->dbInsert(array(				
					'txod_txou_id' => $arrDataDetail['txod_txou_id'],			
					'txod_amount' => (isset($arrDataDetail['txod_amount']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_amount'][$id])) : 0),
					'txod_pph' => (isset($arrDataDetail['txod_pph']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_pph'][$id])) : 0),
					'txod_ppn' => (isset($arrDataDetail['txod_ppn']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_ppn'][$id])) : 0),
					'txod_totalbersih' => (isset($arrDataDetail['txod_totalbersih']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_totalbersih'][$id])) : 0),
					'txod_sisa_bayar' => (isset($arrDataDetail['txod_sisa_bayar']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_sisa_bayar'])) : 0),
					'txod_type' => (isset($arrDataDetail['txod_type']) ? $arrDataDetail['txod_type'] : null),
					'txod_ref_id' => (isset($arrDataDetail['txod_ref_id']) ? $arrDataDetail['txod_ref_id'][$id] : 0),
					'txod_supp_id' => (isset($arrDataDetail['txod_supp_id']) ? $arrDataDetail['txod_supp_id'] : 0),
					'txod_ref_giro' => (isset($arrDataDetail['txod_ref_giro']) ? $arrDataDetail['txod_ref_giro'][$id] : null),
					'txod_jatuhtempo' => (isset($arrDataDetail['txod_jatuhtempo']) ? str_replace("/", "-", $arrDataDetail['txod_jatuhtempo'][$id]) : null),
					'txod_acco_id' => (isset($arrDataDetail['txod_coa']) ? $arrDataDetail['txod_coa'][$id] : null),
					'txod_description' => (isset($arrDataDetail['txod_description']) ? $arrDataDetail['txod_description'][$id] : null),
				));
				if(!$dbInsert) return false;	
			}			
		}else{	
			$intCount = count($arrDataDetail['txod_amount']);
		    $i = 0;		    
		    while ($i < $intCount){
		    	$dbInsert = $this->dbInsert(array(				
					'txod_txou_id' => $arrDataDetail['txod_txou_id'],			
					'txod_amount' => (isset($arrDataDetail['txod_amount']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_amount'][$i])) : 0),
					'txod_pph' => (isset($arrDataDetail['txod_pph']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_pph'][$i])) : 0),
					'txod_ppn' => (isset($arrDataDetail['txod_ppn']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_ppn'][$i])) : 0),
					'txod_totalbersih' => (isset($arrDataDetail['txod_totalbersih']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_totalbersih'][$i])) : 0),
					'txod_sisa_bayar' => (isset($arrDataDetail['txod_sisa_bayar']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_sisa_bayar'][$i])) : 0),
					'txod_type' => (isset($arrDataDetail['txod_type']) ? $arrDataDetail['txod_type'] : null),
					'txod_ref_id' => (isset($arrDataDetail['txod_ref_id']) ? $arrDataDetail['txod_ref_id'][$i] : 0),
					'txod_supp_id' => (isset($arrDataDetail['txod_supp_id']) ? $arrDataDetail['txod_supp_id'] : 0),
					'txod_ref_giro' => (isset($arrDataDetail['txod_ref_giro'][$i]) ? $arrDataDetail['txod_ref_giro'][$i] : null),
					'txod_jatuhtempo' => (isset($arrDataDetail['txod_jatuhtempo'][$i]) ? str_replace("/", "-", $arrDataDetail['txod_jatuhtempo'][$i]) : null),
					'txod_acco_id' => (isset($arrDataDetail['txod_coa']) ? $arrDataDetail['txod_coa'][$i] : null),
					'txod_description' => (isset($arrDataDetail['txod_description']) ? $arrDataDetail['txod_description'][$i] : null),
				));
		    	if($dbInsert) $i++;
		        else return false;	    		
		    }
		}
    	return true;	
	}

	public function update($arrDataDetail, $intID)
	{		
		$intCount = count($arrDataDetail['txod_amount']);		
		$i = 0;		

		while ($i < $intCount) {
			if(isset($arrDataDetail['txod_id'][$i])){
				$dbUpdate = $this->dbUpdate(
					array(				
						'txod_amount' => (isset($arrDataDetail['txod_amount']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_amount'][$i])) : 0 ),
						'txod_pph' => (isset($arrDataDetail['txod_pph']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_pph'][$i])) : 0),
						'txod_ppn' => (isset($arrDataDetail['txod_ppn']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_ppn'][$i])) : 0),
						'txod_totalbersih' =>(isset($arrDataDetail['txod_totalbersih']) ?  str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_totalbersih'][$i])) : 0),
						'txod_sisa_bayar' => (isset($arrDataDetail['txod_sisa_bayar']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_sisa_bayar'][$i])) : 0),
						'txod_type' => (isset($arrDataDetail['txod_type']) ? $arrDataDetail['txod_type'] : 0),
						'txod_ref_id' => (isset($arrDataDetail['txod_ref_id']) ? $arrDataDetail['txod_ref_id'][$i] : 0),
						'txod_supp_id' => (isset($arrDataDetail['txod_supp_id']) ? $arrDataDetail['txod_supp_id'][$i] : null),
						'txod_ref_giro' => (isset($arrDataDetail['txod_ref_giro']) ? $arrDataDetail['txod_ref_giro'] : null),
						'txod_jatuhtempo' => (isset($arrDataDetail['txod_jatuhtempo']) ? date_format(date_create(str_replace("/", "-", $arrDataDetail['txod_jatuhtempo'])), "Y-m-d") : null),
						'txod_acco_id' => (isset($arrDataDetail['txod_coa']) ? $arrDataDetail['txod_coa'][$i] : null),
						'txod_description' => (isset($arrDataDetail['txod_description']) ? $arrDataDetail['txod_description'][$i] : null),
					), "txod_txou_id='$intID' AND id = ".$arrDataDetail['txod_id'][$i]
				);	
			}else{
				$dbUpdate = $this->dbInsert(
					array(	
						'txod_txou_id' => $intID,
						'txod_amount' => (isset($arrDataDetail['txod_amount']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_amount'][$i])) : 0 ),
						'txod_pph' => (isset($arrDataDetail['txod_pph']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_pph'][$i])) : 0),
						'txod_ppn' => (isset($arrDataDetail['txod_ppn']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_ppn'][$i])) : 0),
						'txod_totalbersih' =>(isset($arrDataDetail['txod_totalbersih']) ?  str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_totalbersih'][$i])) : 0),
						'txod_sisa_bayar' => (isset($arrDataDetail['txod_sisa_bayar']) ? str_replace(",", ".", str_replace(".", "", $arrDataDetail['txod_sisa_bayar'][$i])) : 0),
						'txod_type' => (isset($arrDataDetail['txod_type']) ? $arrDataDetail['txod_type'] : 0),
						'txod_ref_id' => (isset($arrDataDetail['txod_ref_id']) ? $arrDataDetail['txod_ref_id'][$i] : 0),
						'txod_supp_id' => (isset($arrDataDetail['txod_supp_id']) ? $arrDataDetail['txod_supp_id'][$i] : null),
						'txod_ref_giro' => (isset($arrDataDetail['txod_ref_giro']) ? $arrDataDetail['txod_ref_giro'] : null),
						'txod_jatuhtempo' => (isset($arrDataDetail['txod_jatuhtempo']) ? date_format(date_create(str_replace("/", "-", $arrDataDetail['txod_jatuhtempo'])), "Y-m-d") : null),
						'txod_acco_id' => (isset($arrDataDetail['txod_coa']) ? $arrDataDetail['txod_coa'][$i] : null),
						'txod_description' => (isset($arrDataDetail['txod_description']) ? $arrDataDetail['txod_description'][$i] : null),
					));
			}			

			if($dbUpdate) $i++;
		    else return false;
		}

		if (isset($arrDataDetail['txod_item_delete']) && $arrDataDetail['txod_item_delete'] != '') {
			foreach (explode(",", $arrDataDetail['txod_item_delete']) as $val) {
				$this->dbDelete("id=$val");
			}			
		}
		
		return true;
	}
}