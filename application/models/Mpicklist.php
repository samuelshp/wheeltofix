<?php

class Mpicklist extends JW_Model {
    private $_CI;

    public function __construct() { 
        parent::__construct(); 
        $this->initialize('pick_list');

        $this->load->model('Madmlinklist');
        $this->_CI=& get_instance();
    }

    public  function getAllPickList() {
        $this->setQuery(
            'SELECT p.id, pcls_code, pcls_status, pcli_product_id, pcli_qty, inor_code, ware_name, prod_title
            FROM pick_list AS p
            LEFT JOIN invoice_order AS inor ON pcls_so_id = inor.id
            LEFT JOIN pick_list_item AS itm ON p.id = itm.id
            LEFT JOIN jw_product AS o ON pcli_product_id = o.id
            LEFT JOIN jw_warehouse AS w ON pcls_warehouse_id = w.id
            WHERE pcls_status > -1'
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public  function getPickListDataByID($intID) {
        $this->setQuery(
            'SELECT p.id, pcls_code, pcls_status, pcli_product_id, pcli_qty, inor.id as inor_id, inor_code, ware_name, prod_title, unit_title, inoi_qty, inoi_qty_processed
            FROM pick_list AS p
            LEFT JOIN invoice_order AS inor ON pcls_so_id = inor.id
            LEFT JOIN invoice_order_item AS i ON inor.id = i.id
            LEFT JOIN pick_list_item AS itm ON p.id = itm.pcli_pick_list_id
            LEFT JOIN jw_product AS o ON pcli_product_id = o.id
            LEFT JOIN jw_unit AS u ON o.prod_satuan_id = u.id
            LEFT JOIN jw_warehouse AS w ON pcls_warehouse_id = w.id
            WHERE p.id = '.$intID.''
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function add($arrPostData, $arrItem) {

        return $this->dbInsert(array(
            'pcls_code' => generateTransactionCode($arrPostData['txtDate'], '', 'picklist'),
            'pcls_so_id' => $arrPostData['intSOID'],
            'pcls_warehouse_id' => $arrPostData['intWarehouseID'],
            'pcls_status' => $arrPostData['intStatus'],
            'pcls_date' => $arrPostData['txtDate']
        ));
    }

    public function delete($intID) {
        $this->_CI->load->model('Minvoiceorderitem');

        $del = $this->dbUpdate(array(
            'pcls_status' => STATUS_DELETED,
        ),
            'id = '.$intID);

        $this->_CI->load->model('Mpicklistitem');

        $this->setQuery(
            "SELECT pl.id, pcls_so_id, pcli_product_id
            FROM pick_list AS pl
            LEFT JOIN pick_list_item AS pli ON pl.id = pli.pcli_pick_list_id
            WHERE pl.id = ".$intID.""
        );

        if($this->getNumRows() > 0) $tmp = $this->getQueryResult('Array');
        else return false;

        for($i=0; $i < count($tmp); $i++) {
            $count = $this->_CI->Mpicklistitem->recalculate($tmp[$i]['pcls_so_id'], $tmp[$i]['pcli_product_id']);

            $a = $this->_CI->Minvoiceorderitem->updateProcessed($tmp[$i]['pcls_so_id'], $tmp[$i]['pcli_product_id'], $count['totalQty']);
        }

        return $del;
    }

    public function getCountData() {
        $this->setQuery(
            'SELECT p.id, pcls_code, pcls_status, pcli_product_id, pcli_qty, inor_code, ware_name, prod_title
            FROM pick_list AS p
            LEFT JOIN invoice_order AS inor ON pcls_so_id = inor.id
            LEFT JOIN pick_list_item AS itm ON p.id = itm.id
            LEFT JOIN jw_product AS o ON pcli_product_id = o.id
            LEFT JOIN jw_warehouse AS w ON pcls_warehouse_id = w.id
            WHERE pcls_status > -1'
        );

        return $this->getNumRows();
    }

    public function getCountSearch($intSOID = 0, $intPickListCode = '', $intDate = '', $intStartNo = 0, $intPerPage = 0) {
        $strWhere = '';

        if(!empty($intPickListCode)) $strWhere .= " AND (picklist.pcls_code LIKE '%$intPickListCode%')";

        if(!empty($intSOID)) $strWhere .= " AND picklist.pcls_so_id = $intSOID";

        if(!empty($intDate)) $strWhere .= " AND picklist.pcls_cdate = $intDate";

        $this->setQuery(
            "SELECT picklist.id
            FROM pick_list AS picklist
            LEFT JOIN jw_warehouse AS ware ON picklist.pcls_warehouse_id = ware.id
            WHERE pcls_status != ".STATUS_DELETED." $strWhere");
    
        return $this->getNumRows();
    }

    public function getSearch($intSOID = 0, $intPickListCode = '', $intDate = '', $intStartNo = 0, $intPerPage = 0) {
        $strWhere = '';

        if(!empty($intPickListCode)) $strWhere .= " AND (picklist.pcls_code LIKE '%$intPickListCode%')";

        if(!empty($intSOID)) $strWhere .= " AND picklist.pcls_so_id = $intSOID";

        if(!empty($intDate)) $strWhere .= " AND picklist.pcls_date = '$intDate'";

        $this->setQuery(
            "SELECT picklist.id, pcls_code, pcls_so_id, ware_name, inor_code
            FROM pick_list AS picklist
            LEFT JOIN jw_warehouse AS ware ON picklist.pcls_warehouse_id = ware.id
            LEFT JOIN invoice_order AS inor ON picklist.pcls_so_id = inor.id
            WHERE pcls_status != ".STATUS_DELETED." $strWhere
            LIMIT $intStartNo, $intPerPage");
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }
} 