<?php
/*
PUBLIC FUNCTION:
- getDynamicCode(strTableName,intID)
- getQuantityByProductID(intProductID,intWarehouseID)
- getHistoryByProductID(intProductID,intWarehouseID)

PRIVATE FUNCTION:
- 
*/

class Mpotongan extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
    $this->initialize('potongan');
}

public function add($intInvoiceID,$intQty,$strName,$intNominal) {
    return $this->dbInsert(array(
        'poto_invoice_id' => $intInvoiceID,
        'poto_item_qty' => $intQty,
        'poto_name' => $strName,
        'poto_nominal' => $intNominal
    ));
}
public function getAllPotonganByInvoiceID($intInvoiceID) {

    $this->setQuery(
'SELECT poto_item_qty, poto_name, poto_nominal
FROM potongan
WHERE poto_invoice_id= '.$intInvoiceID.'');
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function deleteByInvoiceID($intID) {
    return $this->dbDelete("poto_invoice_id = $intID");
}

}

/* End of File */