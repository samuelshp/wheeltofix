<?php
/*
PUBLIC FUNCTION:
- getAllSalesman(strKeyword,intSupplierID)
- getSalesmanKanvas(strKeyword)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Msalesman extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('jw_salesman');
}

public function getAllSalesman($strKeyword = '', $intSupplierID = '') {
$strKeyword = urldecode($strKeyword);

if(!empty($strKeyword)) $strWhere = " AND (sals_name LIKE '%$strKeyword%' OR sals_code LIKE '%$strKeyword%' )";
else $strWhere = '';
    
if(!empty($intSupplierID) || $intSupplierID!=0) $strWhere .= " AND (sals_supplier_id = $intSupplierID)";

$this->setQuery(
'SELECT id, sals_code, sals_name, sals_address, sals_phone
FROM jw_salesman
WHERE sals_status > 1'.$strWhere.'
ORDER BY sals_name ASC');

if($this->getNumRows() > 0) return $this->getQueryResult('Array');
else return false;
}


public function getSalesmanKanvas($strKeyword = '') {
    $strKeyword = urldecode($strKeyword);

    if(!empty($strKeyword)) $strWhere = " AND (sals_name LIKE '%$strKeyword%' OR sals_code LIKE '%$strKeyword%')";
    else $strWhere = '';

    $this->setQuery(
'SELECT s.id, sals_code, sals_name, sals_address, sals_phone, sals_jenis_id, tiny_data1
FROM jw_salesman s
   LEFT JOIN tinydb d ON (tiny_key = sals_jenis_id)
WHERE tiny_category = "salesman_type" AND tiny_data1 IN ("Motorist", "Canvas")
   AND sals_status > 1'.$strKeyword.'
ORDER BY sals_name ASC');

    if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllData() {
	$this->dbSelect('id,sals_name,sals_address',"id > 0 AND sals_status > 1",'sals_name ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,sals_code,sals_name,sals_address',"id = $intID");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */