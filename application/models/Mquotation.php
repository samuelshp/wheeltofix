<?php
/**
 * 
 */
class Mquotation extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('quotation');
	}

	public function getCount()
	{
		return $this->dbCount('quot_status=2');
	}

	public function getItems($intStartNo = -1,$intPerPage = -1)
	{
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "q.cdate DESC, q.id DESC";
		else $strOrderBy = "q.cdate DESC, q.id DESC LIMIT $intStartNo, $intPerPage";

	    // $whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
	    // if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
	    // $whereInProject .= ")";
		
		$this->setQuery(
			"SELECT q.id, q.cdate AS quot_date, quot_finaltotal, quot_status, cust_name, kont_name, quot_code
			FROM quotation AS q
			LEFT JOIN quotation_item as quoi ON q.id = quoi.quoi_quot_id
			LEFT JOIN kontrak as k ON q.quot_kont_id = k.id
			LEFT JOIN subkontrak as sk ON sk.id = q.quot_subkont_id			
			LEFT JOIN jw_customer AS cust ON cust.id = q.quot_cust_id
			WHERE quot_status >= ".STATUS_APPROVED." 
			GROUP BY q.id 
			ORDER BY $strOrderBy");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getItem($intID)
	{
		$strCustomSelect = "";
		$strCustomJoin = "";

		if (PS_SYSTEM_MODE == 'CONTRACTOR') {
			$strCustomSelect .= ",k.kont_name,sk.job,ware_name ";
			$strCustomJoin .= "JOIN kontrak as k ON q.quot_kont_id = k.id
			JOIN subkontrak as sk ON sk.id = q.quot_subkont_id			
			JOIN jw_warehouse AS ware ON ware.id = q.quot_ware_id";
		}

		$this->setQuery("
			SELECT q.*, cust_name $strCustomSelect FROM quotation q
			LEFT JOIN quotation_item quoi ON q.id = quoi_quot_id
			JOIN jw_customer AS cust ON cust.id = q.quot_cust_id
			$strCustomJoin
			WHERE q.id = $intID
		");

		if ($this->getNumRows() > 0 ) return $this->getNextRecord('Array');
		else return false;
	}

	public function add($arrData)
	{
		return $this->dbInsert(array(
			'quot_code' => generateTransactionCode($arrData['txtDateBuat'],'','quotation'),
			'quot_date' => $arrData['txtDateBuat'],
			'quot_exp_date' => $arrData['txtDateExpired'],
			'quot_cust_id' => $arrData['intCustID'],
			'quot_kont_id' => $arrData['selProyek'],
			'quot_subkont_id' => $arrData['selPekerjaan'],
			'quot_ware_id' => $arrData['selWarehouse'],
			'quot_description' => $arrData['txtaDescription'],
			'quot_tax' => $arrData['intTax'],
			'quot_finaltotal' => $arrData['intGrandTotal']
		));
	}

	public function update($intQuotationID, $arrData)
	{
		return $this->dbUpdate(array(
			'quot_description' => $arrData['txtaDescription'],
			'quot_tax' => $arrData['intTax'],
			'quot_finaltotal' => $arrData['intGrandTotal']
		),"id = $intQuotationID");
	}

	public function delete($intQuotationID)
	{
		return $this->dbUpdate(array('quot_status' => STATUS_DELETED), "id=$intQuotationID");
	}

	public function updateQtyProcessed($intQuotationID,$intProductID, $intQtyItemUsed)
	{
		$this->setQuery("UPDATE quotation_item SET quoi_qty_processed = $intQtyItemUsed, quoi_qty_display_processed = $intQtyItemUsed WHERE quoi_quot_id = $intQuotationID AND quoi_prod_id = $intProductID");

		$this->checkAutoClose($intQuotationID);
	}

	public function checkAutoClose($intQuotationID)
	{
		$bolSearched = FALSE;
    	$arrBolClosed = [];
		$this->setQuery("SELECT *, b.id as item_id FROM quotation a 
			LEFT JOIN quotation_item b ON a.id = b.quoi_quot_id
			WHERE a.id = $intQuotationID AND a.quot_status != ".STATUS_DELETED);

		if ($this->getNumRows() > 0)  $items = $this->getQueryResult('Array');
		else return false;

		$_CI =& get_instance();
	    $_CI->load->model('Mquotationitem');

		foreach ($items as $val) {
			$bolSearched = TRUE;
			if(($val['quoi_qty_processed'] >= $val['quoi_qty']) && ($val['quoi_qty_display_processed'] >= $val['quoi_qty_display'])){
				$_CI->Mquotationitem->closeItem($val['item_id']);
				array_push($arrBolClosed, TRUE);				
			}else{
				$_CI->Mquotationitem->reverseCloseItem($val['item_id']);
				array_push($arrBolClosed, FALSE);
			}
		}

		$bolClosed = (compareData(FALSE,$arrBolClosed,'notin')) ? TRUE : FALSE ;
	    if($bolClosed && $bolSearched) $intStatusClose = STATUS_FINISHED;        
	    else $intStatusClose = STATUS_WAITING_FOR_FINISHING;            

	    $this->dbUpdate(array(
	        'quot_status' => $intStatusClose),
	        "id = $intQuotationID");

	    return ($bolClosed && $bolSearched);

	}
}