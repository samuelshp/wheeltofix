<?php

class Msubkontrakteam extends JW_Model
{
    // Constructor
    public function __construct() { 
        parent::__construct();
        $this->initialize('subkontrak_team');
    }

    public function add($arrDataTeam, $intSubkontrakID)
    {
        $intCount = count($arrDataTeam['jabatan_id']);
        $i=0;
        while($i < $intCount) {             
            $dbInsert = $this->dbInsert(array(
                'subkontrak_id' => $intSubkontrakID,
                'name'  => $arrDataTeam['name'][$i],
                'jabatan_id'    => $arrDataTeam['jabatan_id'][$i]               
            ));
            if($dbInsert) $i++;
            else return false;
        }        
        return true;
    }

    public function updateDuedate($id,$due_date) {
        return $this->dbUpdate(array(
            'due_date' => $due_date
        ),"id = $id");
    }

    public function update($row_id, $subkontrak_id, $pegawaiID, $jabatan_id, $mby, $status)    
    {        
        if($row_id == '')
        return $this->dbInsert(array(
            'subkontrak_id' => $subkontrak_id,
            'name' => $pegawaiID,
            'jabatan_id' => $jabatan_id,
            'status' => $status            
        ));
        else
        return $this->dbUpdate(array(
            'subkontrak_id' => $subkontrak_id,
            'name' => $pegawaiID,
            'jabatan_id' => $jabatan_id,
            'status' => $status
        ), "id= '$row_id'");        
    }

    public function checkEditableBPB($idPegawai, $idSubKontrak){
    
        $this->setQuery(
            "SELECT skt.name
            from subkontrak_team as skt
            where skt.subkontrak_id = $idSubKontrak AND skt.name = $idPegawai"
        );
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function deleteItem($id) {
        return $this->dbUpdate(array(
            'status' => 1
        ),"id = $id");
    }
    
}


?>