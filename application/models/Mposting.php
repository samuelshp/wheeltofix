<?php
/*
PUBLIC FUNCTION:
- callFunctionPost(strPeriode)

PRIVATE FUNCTION:
- __construct()
*/

class Mposting extends JW_Model {

public function __construct() {
	parent::__construct();
}

public function callFunctionPost($strPeriod) {
	$strDateFirst = $strPeriod.'-01';
	$query = $this->db->query('SELECT LAST_DAY("'.$strDateFirst.'") AS last_day');
	$strDateLast = $query->row_array();
	$this->db->query('SET AUTOCOMMIT = OFF');
	$this->db->query('START TRANSACTION');
	$query = $this->db->query('CALL func_post_transaction ("'.$strDateFirst.'","'.$strDateLast['last_day'].'")');
	if($this->db->trans_status() === FALSE){
		$this->db->query('ROLLBACK');
		return array('status' => FALSE,'error_message' => 'Posting Transaksi '.$strPeriod.' gagal. Silahkan coba kembali setelah beberapa saat. Hubungi Administrator untuk bantuan.');
	} else {
		$this->db->query('COMMIT');
		return array('status' => TRUE,'success_message' => 'Posting Transaksi '.$strPeriod.' berhasil.');
	}
}

}