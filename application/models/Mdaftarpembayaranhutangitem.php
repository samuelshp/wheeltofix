<?php
class Mdaftarpembayaranhutangitem extends JW_Model {

    // Constructor
    public function __construct() {
        parent::__construct();
        $this->initialize('daftar_pembayaran_hutang_item');
    }

    public function getItemsByID($id)
    {
        $this->setQuery(
            "SELECT *
            FROM (                            
                (select dph_item.id as item_id, dphi_dphu_id, pinv_code as code, pi.cdate, (pinv_subtotal-pinv_discount-pinv_dp) AS pinv_subtotal, pinv_tax, pinv_totalbeforepph, pinv_pph as pph, pinv_finaltotal, dph_item.dphi_amount_rencanabayar, dphi_lunas, dphi_reff_type, dphi_reff_id, dphi_status, dphi_sisabayar from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join purchase_invoice as pi on dph_item.dphi_reff_id = pi.id
                left join jw_supplier as supp on pi.pinv_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'PI')
                UNION ALL
                (select 
                dph_item.id as item_id, dphi_dphu_id, txou_code as code, txou_date, txod_amount, txod_ppn, (txod_amount+(txod_amount*txod_ppn/100)) as txod_grandtotal, txod_pph as pph, txod_totalbersih, dph_item.dphi_amount_rencanabayar, dphi_lunas, dphi_reff_type, dphi_reff_id, dphi_status, dphi_sisabayar
                from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id   
                left join transaction_out as txou on txou.id = dph_item.dphi_reff_id
                left join transaction_out_detail as txod on txou.id = txod.txod_txou_id
                left join jw_supplier as supp on txod.txod_supp_id = supp.id
                where dph_item.dphi_reff_type = 'NKS' OR dph_item.dphi_reff_type = 'NDS')
                UNION ALL
                (select dph_item.id as item_id, dphi_dphu_id, dpay_code as code, dpay_date, dpay_amount, dpay_ppn, dpay_amount_ppn, dpay_pph as pph, dpay_amount_final, dph_item.dphi_amount_rencanabayar, dphi_lunas, dphi_reff_type, dphi_reff_id, dphi_status, dphi_sisabayar from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join downpayment as dp on dph_item.dphi_reff_id = dp.id
                left join jw_supplier as supp on dp.dpay_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'DP')
                ) tblUnion
            WHERE dphi_dphu_id = '$id' AND dphi_status > ".STATUS_DELETED
            
        );        

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }
    
    public function add($dphi_dphu_id, $dphi_reff_type, $dphi_pi_id, $dphi_amount_rencanabayar,$dphi_sisabayar, $dphi_twoweek_status, $dphi_lunas)
    {
        
        return $this->dbInsert(array(
            'dphi_dphu_id'=> $dphi_dphu_id,
            'dphi_reff_type' => $dphi_reff_type,
            'dphi_reff_id' => $dphi_pi_id,             
            'dphi_amount_rencanabayar' => $dphi_amount_rencanabayar,
            'dphi_sisabayar' => ($dphi_lunas == 1) ? 0 : $dphi_sisabayar,
            'dphi_2week_status' => $dphi_twoweek_status,
            'dphi_lunas' => $dphi_lunas
        ));
    }

    public function delete($id)
    {
        //hitung ulang amount total
        $this->setQuery("
            SELECT (dphu.dphu_amount-pi.pinv_grandtotal) AS amount_baru , dph_item.dphi_dphu_id
            FROM daftar_pembayaran_hutang_item as dph_item         
            LEFT JOIN daftar_pembayaran_hutang as dphu ON dph_item.dphi_dphu_id = dphu.id
            LEFT JOIN purchase_invoice as pi ON dph_item.dphi_pi_id = pi.id
            WHERE dph_item.id = '".$id."'
        ");
        
        $record = $this->getNextRecord('Array');

        //update grandtotal baru
        $this->initialize('daftar_pembayaran_hutang');
        $this->dbUpdate(array(
            'dphu_amount' => $record['amount_baru'],            
        ),"id='".$record['dphi_dphu_id']."'");

        $this->initialize('daftar_pembayaran_hutang_item');
        return $this->dbDelete("id = '$id'");
        //4914250
    }

    public function deleteItem($intItemID)
    {
        return $this->dbUpdate(array("dphi_status" => STATUS_DELETED),"id=$intItemID");
    }

    public function updateRencanaBayar($arrItem, $strID)
    {               
        return $this->dbUpdate(array(
            'dphi_amount_rencanabayar' => $arrItem['amount_rencanabayar'], 
            'dphi_sisabayar' => $arrItem['amount_sisabayar'],
            'dphi_lunas' => ($arrItem['lunas'] == "on" ? 1 : 0)
        ), 
        "id = $strID");        
    }

    public function updateTerbayarPI($intPinvID, $amount, $type)
    {
        // $this->initialize('purchase_invoice');
        if ($type == "add") {
            $stmt = "pinv_terbayar + $amount";
        }else{
            $stmt = "pinv_terbayar - $amount";
        }
        return $this->setQuery("UPDATE purchase_invoice SET pinv_terbayar = ($stmt) WHERE id = $intPinvID");
       // return  $this->dbUpdate(array(
       //          'pinv_terbayar' => $amount
       //  ),"id=$intPinvID");
    }

    public function updateStatusReferensi($arrDPHdibayar)
    {
        $result = FALSE;
        foreach ($arrDPHdibayar as $key => $value){
            $intDPHid = explode("-", $value)[2];            
            $this->setQuery("SELECT dphi_reff_type, dphi_reff_id, dphi_amount_rencanabayar, dphi_lunas FROM daftar_pembayaran_hutang_item WHERE dphi_dphu_id = '$intDPHid' ");

            if($this->getNumRows() > 0) $result = $this->getQueryResult('Array');
            
            foreach ($result as $item) {
                if ($item) {
                    switch ($item['dphi_reff_type']) {
                        case 'PI': $strTableName = 'purchase_invoice'; $strFieldStatus = 'pinv_status'; break;
                        case 'NKS': $strTableName = 'transaction_out'; $strFieldStatus = 'txou_status_trans'; break;
                        case 'DP': $strTableName = 'downpayment'; $strFieldStatus = 'dpay_status'; break;
                    }                    
                    $status = ($item['dphi_lunas'] == 1) ? STATUS_FINISHED : STATUS_WAITING_FOR_FINISHING;
                    $return = $this->setQuery("UPDATE ".$strTableName." SET ".$strFieldStatus." = ".$status." WHERE id = '".$item['dphi_reff_id']."'");
                    $return = $this->setQuery("UPDATE daftar_pembayaran_hutang SET dphu_status = ".STATUS_FINISHED." WHERE id = ".$intDPHid);

                }
            }

            $result = $return;
        }
        
        return $result;
        
    }

    public function updateStatus($type, $intID)
    {
        switch ($type) {
            case 'PI':
                $strTableName = 'purchase_invoice'; $strField = 'pinv_status';
                break;
            case 'NKS':
                $strTableName = 'transaction_out'; $strField = 'txou_status_trans';
                break;
            case 'DP':
                $strTableName = 'downpayment'; $strField = 'dpay_status';
                break;
        }

        return $this->setQuery("UPDATE $strTableName SET $strField = '".STATUS_WAITING_FOR_FINISHING."' WHERE id = '$intID'");
        
    }

    public function checkAvailabilityDp($intDp){        
        // $this->dbSelect('id,dphi_amount_rencanabayar',"dphi_reff_id = $intDp AND dphi_reff_type = 'DP' AND ");
        $this->setQuery("SELECT dph_item.id,dphi_amount_rencanabayar FROM daftar_pembayaran_hutang_item AS dph_item 
        JOIN daftar_pembayaran_hutang AS dph ON dph.id = dph_item.dphi_dphu_id
        WHERE dph_item.dphi_reff_type = 'DP' AND dph_item.dphi_reff_id = $intDp AND dph.dphu_status > ".STATUS_DELETED."
        GROUP BY dph_item.id");
        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;
    }
    
    public function checkPiInDph($idPi){
        $this->setQuery(
            "SELECT dphi.id,dphi_amount_rencanabayar FROM daftar_pembayaran_hutang_item AS dphi 
            JOIN daftar_pembayaran_hutang AS dph ON dph.id = dphi_dphu_id 
            WHERE dphi_reff_id = $idPi AND dphi_reff_type = 'PI'  AND dphu_status > ".STATUS_DELETED."
        ");
        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;
    }


}