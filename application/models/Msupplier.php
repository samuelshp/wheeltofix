<?php
/*
PUBLIC FUNCTION:
- getAllSuppliers(strKeyword)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Msupplier extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('jw_supplier');
}

//sampe sini
public function getAllSuppliers($strKeyword = '') {
	$strKeyword = urldecode($strKeyword);

    if(!empty($strKeyword)) $strWhere = " AND (supp_name LIKE '%$strKeyword%' OR supp_code LIKE '%$strKeyword%')";
    else $strWhere = '';

    $this->setQuery(
'SELECT jw.id, jw.supp_name, jw.supp_address, jw.supp_city, jw.supp_phone, jw.supp_internal, jw.supp_code
FROM jw_supplier as jw
WHERE supp_status > 1'.$strWhere.'
ORDER BY supp_name ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllData() {
	$this->dbSelect('id,supp_name,supp_address,supp_city',"supp_status > 1",'supp_name ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,supp_code,supp_name,supp_address,supp_city,supp_cp_name,supp_cp_phone',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */