<?php
/**
 * 
 */
class Mattachment extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('attachment');
	}

	public function upload($arrFileData)
	{
		return $this->dbInsert(array(
			'attc_type' => $arrFileData['attachment_type'],
			'attc_ref_id' => $arrFileData['ref_id'],
			'attc_content' => $arrFileData['path']			
		));
	}

	public function deleteItem($intID)
	{
		return $this->dbDelete('id = '.$intID);
	}

	public function deleteItems($strID)
	{
		return $this->dbDelete('id IN ('.$strID.')');
	}

	public function updateReferenceID($strAttachmentID, $intReferenceID)
	{

		return $this->dbUpdate(array(
			'attc_ref_id' => $intReferenceID
		), "id IN (".$strAttachmentID.")");
		
		// $arrAttachment = explode(",", $strAttachmentID);
		// foreach ($arrAttachment as $intID) {
			
		// }
	}

	public function getByItemType($strType, $intRefID = 0)
	{
		$strWhere = "attc_type = '".$strType."'";

		if (!empty($intRefID)) $strWhere .= "AND attc_ref_id = ".$intRefID;

		$this->dbSelect('id, attc_type, attc_content',$strWhere);

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getByItemID($intID)
	{
		$this->dbSelect('id, attc_type, attc_content', "id = ".$intID);
		if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}
}