<?php
/**
 * Madjustment.php
 *
 * @version	: 1.5
 * @author	: Victor Julian Lipesik (vjlipesik@gmail.com)
 * @date	: October 10, 2008 (January 5, 2012)
 *
 * This class is model to retrieve data from table adjustment
 *
 */

/********************************************
  PUBLIC FUNCTION:
	- getItems(intStartNo,intPerPage)
	- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
	- getItemByID(intID)
	- getStockAdjusted(intProductID,intWarehouseID,strLastStockUpdate)
	- getCount()
	- add(intProductID,intAtWarehouse,intAdjustmentType,intToWarehouse,intQuantity,strDescription,strDate)
	- editByID(intID,intStatus)
	- editByID2(intID,intQuantity,strDescription,intStatus)
	- deleteByID(intID)
	
  PRIVATE FUNCTION:
	
*********************************************/

class Madjustment extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('adjustment');
}

public function searchByWarehouse($strWareName, $arrSearchDate, $intStatus) {
	$strWhere = '';
	if(!empty($strWareName)) {
		$strWhere .= " AND (wfrom.ware_name LIKE '%$strWareName%' OR wto.ware_name LIKE '%$strWareName%')";
	}
	if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (a.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (a.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (ajst_status = {$intStatus})";

    $this->setQuery(
"SELECT a.id, a.cdate AS ajst_date, wfrom.ware_name AS from_ware, wto.ware_name AS to_ware, ajst_status, ajst_code
FROM adjustment AS a
LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
WHERE a.id > 0{$strWhere}
ORDER BY a.cdate DESC, a.id DESC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItems($intStartNo=-1,$intPerPage=-1) {
    if($intStartNo == -1 || $intPerPage == -1) $this->setQuery(
"SELECT a.id, a.cdate AS ajst_date, wfrom.ware_name AS from_ware, wto.ware_name AS to_ware, ajst_status,ajst_code
FROM adjustment AS a
LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
ORDER BY a.cdate DESC, a.id DESC");
    else $this->setQuery(
"SELECT a.id, a.cdate AS ajst_date, wfrom.ware_name AS from_ware, wto.ware_name AS to_ware, ajst_status,ajst_code
FROM adjustment AS a
LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
ORDER BY a.cdate DESC, a.id DESC LIMIT $intStartNo, $intPerPage");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
	// Cari penyesuaian pada gudang asli
	$this->setQuery(
"SELECT ajst_type,ajst_to,ajst_quantity,ajst_status,cdate AS ajst_date
FROM adjustment
WHERE ajst_product_id = $intProductID AND ajst_warehouse_id = $intWarehouseID AND cdate > '$strLastStockUpdate'");
	
	if($this->getNumRows() > 0) $arrData = $this->getQueryResult('Array');
	else $arrData = array();
	
	// Cari penyesuaian pada gudang tujuan, terjadi PENUKARAN warehouse_id menjadi _to, serta tipe harusnya pasti 3
	$this->setQuery(
"SELECT ajst_warehouse_id AS ajst_to,4 AS ajst_type,ajst_quantity,ajst_status,cdate AS ajst_date
FROM adjustment
WHERE ajst_product_id = $intProductID AND ajst_to = $intWarehouseID AND cdate > '$strLastStockUpdate'");
	
	if($this->getNumRows() > 0) $arrData = array_merge($arrData,$this->getQueryResult('Array'));
		
	if(!empty($arrData)) return $arrData;
	else return false;
}

public function getItemByID($intID) {
    if($intID == 0) $this->setQuery(
"    SELECT a.id, a.cdate AS ajst_date, ajst_code,wfrom.ware_name AS from_ware, wto.ware_name AS to_ware,ajst_description, ajst_progress, ajst_status,ajst_editable,wfrom.id AS from_id, wto.id AS to_id
    FROM adjustment AS a
    LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
    LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
    ORDER BY a.cdate DESC, a.id DESC LIMIT 0,1");
    else $this->setQuery(
"    SELECT a.id, a.cdate AS ajst_date, ajst_code,wfrom.ware_name AS from_ware, wto.ware_name AS to_ware,ajst_description, ajst_progress, ajst_status,ajst_editable,wfrom.id AS from_id, wto.id AS to_id
    FROM adjustment AS a
    LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
    LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
    WHERE a.id = $intID");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

    public function getUserHistory($intUserID,$intPerPage) {
        $this->setQuery(
"    SELECT a.id, a.cdate AS ajst_date, ajst_status, wfrom.ware_name AS from_ware, wto.ware_name AS to_ware,ajst_code
    FROM adjustment AS a
    LEFT JOIN jw_warehouse AS wfrom ON ajst_warehouse_id = wfrom.id
    LEFT JOIN jw_warehouse AS wto ON ajst_to = wto.id
    WHERE (a.cby = $intUserID OR a.mby = $intUserID)
    ORDER BY a.cdate DESC, a.id DESC LIMIT 0, $intPerPage");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

public function getStockAdjusted($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0;
	
	// Cari penyesuaian pada gudang asli	
	$this->setQuery(
"SELECT ajst_type,ajst_quantity
FROM adjustment
WHERE ajst_product_id = $intProductID AND ajst_warehouse_id = $intWarehouseID AND ajst_status IN (2,3) AND cdate >= '$strLastStockUpdate'");
	
	$arrData = $this->getQueryResult('Array');
	
	for($i = 0; $i < count($arrData); $i++) switch($arrData[$i]['ajst_type']) {
		case 1: $intTotalStock += $arrData[$i]['ajst_quantity']; break;
		case 2: $intTotalStock -= $arrData[$i]['ajst_quantity']; break;
		case 3: $intTotalStock -= $arrData[$i]['ajst_quantity']; break;
	}
	
	// Cari penyesuaian pada gudang tujuan
	$this->setQuery(
"SELECT ajst_quantity
FROM adjustment
WHERE ajst_product_id = $intProductID AND ajst_type = '3' AND ajst_to = $intWarehouseID AND ajst_status IN (2,3) AND DATE(cdate) > DATE('$strLastStockUpdate')");
	
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['ajst_quantity'];

	return $intTotalStock;
}

public function getCount() {
	$this->dbSelect('id');
	
	return $this->getNumRows();
}

public function add($intAtWarehouse,$intToWarehouse,$strDescription,$strDate) {
	return $this->dbInsert(array(
		'ajst_branch_id' => $this->session->userdata('intBranchID'),
		'ajst_warehouse_id' => !empty($intToWarehouse) ? $intAtWarehouse : $this->session->userdata('intWarehouseID'),
		'ajst_to' => $intToWarehouse,
		'ajst_description' => $strDescription,
		'ajst_status' => 0,
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
	));
}

public function editByID($intID,$txtProgress,$intStatus) {
	return $this->dbUpdate(array(
        'ajst_progress' => $txtProgress,
		'ajst_status' => $intStatus
		),"id = $intID");
}

public function editByID2($intID,$intWarehouse,$intWarehouseTo,$strDescription,$txtProgress,$intStatus,$intEditable) {
	return $this->dbUpdate(array(
		'ajst_warehouse_id' => $intWarehouse,
        'ajst_to' => $intWarehouseTo,
        'ajst_progress' => $txtProgress,
		'ajst_description' => $strDescription,
		'ajst_status' => $intStatus,
        'ajst_editable' => $intEditable
		),"id = $intID");
}

public function deleteByID($intID) {
    $status = $this->dbSelect('ajst_status',"id = $intID");
    if($status['ajst_status']='0' || $status['ajst_status']='1'){
        $this->setQuery("DELETE FROM adjustment_item WHERE adit_adjustment_id = $intID");
        return $this->dbDelete("id = $intID");
    }
    else
        return false;
}

public function autoExpired($intDay=1){
    $this->setQuery(
"UPDATE adjustment SET ajst_status = 0 WHERE DATE_ADD(cdate,INTERVAL {$intDay} DAY) <= CURRENT_TIMESTAMP AND ajst_status = 1");

    $this->setQuery(
"UPDATE adjustment SET ajst_status = 3 WHERE DATE_ADD(cdate,INTERVAL {$intDay} DAY) <= CURRENT_TIMESTAMP AND ajst_status = 2");
    return true;
}

}

/* End of File */