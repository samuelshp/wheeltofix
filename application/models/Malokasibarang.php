<?php

class Malokasibarang extends JW_Model {

// Constructor
    public function __construct() { 
        parent::__construct();
        $this->initialize('alokasi_barang_pembelian');
    }
    
    public function getAllData($intAdmLoginID = 0){
        
        if ($intAdmLoginID) $strWhere .= "WHERE skt.name = $intAdmLoginID AND skt.jabatan_id = 11";

        $this->setQuery(
            "SELECT subkont.id, kont.kont_name, subkont.job, cust.cust_name FROM alokasi_barang_pembelian AS albp
                JOIN subkontrak AS subkont ON albp.albp_subkon_id = subkont.id
                JOIN kontrak AS kont ON subkont.kontrak_id = kont.id
                JOIN jw_customer AS cust ON cust.id = kont.owner_id
                JOIN subkontrak_team as skt on skt.subkontrak_id = subkont.id
            $strWhere
            GROUP BY id"
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	    else return false;
    }

    public function getDetailBySubkontrak($intSubkontrakID)
    {
        $this->setQuery(
            "SELECT subkont.id, kont.kont_name, subkont.job FROM alokasi_barang_pembelian AS albp
                JOIN subkontrak AS subkont ON albp.albp_subkon_id = subkont.id
                JOIN kontrak AS kont ON subkont.kontrak_id = kont.id                
            WHERE albp_subkon_id = $intSubkontrakID
            "
        );

        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;   
    }

    public function getAlokasiDataPurchased($intSubkontrakID)
    {
        $this->setQuery(
            "SELECT albp.id as albp_id, albp_subkon_nonmaterial_id, op_item.id as op_item_id, prod_title, op.prch_code, op_item.prci_quantity1, unit.unit_title, op_item.prci_price, (op_item.prci_quantity1 * op_item.prci_price) AS totalBelumPPn, skm.id as skm_id, skm.kategori, op.cdate as prch_date, pb.pror_code FROM alokasi_barang_pembelian AS albp
                JOIN purchase_item AS op_item ON albp.albp_ref_id = op_item.id
                JOIN purchase AS op ON op.id = op_item.prci_purchase_id
                JOIN purchase_order AS pb ON op.prch_po_id = pb.id 
                JOIN jw_product as prod ON prod.id = op_item.prci_product_id 
                JOIN jw_unit AS unit ON prod.satuan_bayar_id = unit.id 
                LEFT JOIN subkontrak_nonmaterial AS skm ON skm.id = albp.albp_subkon_nonmaterial_id
            WHERE albp.albp_ref_type = 'prci' AND albp_subkon_id = $intSubkontrakID
            "
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;  
    }

    public function getAlokasiDataPengeluaran($intSubkontrakID)
    {
        $this->setQuery(
            "SELECT albp.id as albp_id, albp_subkon_nonmaterial_id, tbPGL.txod_id, txou_code, txou_date, txod_totalbersih, txod_description, skm.id as skm_id, skm.kategori FROM 
                (SELECT txod.txod_ref_id, txod.id as txod_id, txou_code, txou_date, txod_totalbersih, txod_description FROM transaction_out AS txou 
                LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id
                WHERE txod_type = 'PGL') 
                AS tbPGL 
            JOIN alokasi_barang_pembelian AS albp ON albp.albp_ref_id = tbPGL.txod_id
            JOIN subkontrak AS subkon ON subkon.id = albp.albp_subkon_id
            JOIN kontrak AS kont ON kont.id = subkon.kontrak_id
            LEFT JOIN subkontrak_nonmaterial AS skm ON skm.id = albp.albp_subkon_nonmaterial_id
            WHERE albp.albp_subkon_id = $intSubkontrakID AND albp.albp_ref_type = 'pgl'"
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;  
    }

    public function getSubkontrakNonMaterial($intSubkontrakID)
    {
        $this->setQuery("SELECT * FROM subkontrak_nonmaterial WHERE subkontrak_id=$intSubkontrakID");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function add($arrPostData)
    {
        $intSubkontrakID = $arrPostData['arrKontrak'];

        foreach ($arrPostData['purchaseAlocated'] as $i => $e) {
            if(!empty($e)){                
                $expld = explode("-", $e);

                $dbInsert = $this->dbInsert(array(
                    'albp_subkon_id' => $intSubkontrakID,
                    'albp_ref_type' => $expld[0],
                    'albp_ref_id' => $expld[1],
                    'albp_subkon_nonmaterial_id' => $expld[2]
                ));
            }
        }

        return $dbInsert;
        // return (in_array(null, $dbInsert, true) ? true : false);
        
    }

    public function update($arrPostData, $intSubkontrakID)
    {
        $arrPostData['purchaseAlocated'] = array_merge(
            isset($arrPostData['itemPembelianLain']) ? $arrPostData['itemPembelianLain'] : [], 
            isset($arrPostData['itemPengeluaranLain']) ? $arrPostData['itemPengeluaranLain'] : []
        );

        foreach ($arrPostData['purchaseAlocated'] as $e) {
            if (substr_count($e, "-")) {            
               $expld = explode("-", $e);
               $intNewSKMID = $expld[0];
               $intAlokasiID = $expld[1];

               $dbUpdate = $this->dbUpdate(array(
                    'albp_subkon_nonmaterial_id' => $intNewSKMID
               ),"id=$intAlokasiID AND albp_subkon_id = $intSubkontrakID");
            }
        }

        return $dbUpdate;        
    }
}