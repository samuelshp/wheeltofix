<?php
/*
PUBLIC FUNCTION:
- getSJByPurchaseID(intPurchaseID)
- add(intPurchaseID,strNewSJName,strNewSJDate)

PRIVATE FUNCTION:
- __construct()
*/

class Msuratjalan extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('suratjalan');
}

public function getSJByPurchaseID($intPurchaseID) {
	//$this->dbSelect('suja_name,suja_date','suja_prch_id=$intPurchaseID','suja_date');
	$this->setQuery("SELECT suja_name,suja_date FROM suratjalan WHERE suja_prch_id = $intPurchaseID Order By suja_date DESC ");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}
public function add($intPurchaseID,$strNewSJName,$strNewSJDate) {
	return $this->dbInsert(array(
		'suja_prch_id' => $intPurchaseID,
		'suja_name' => $strNewSJName,
		'suja_date'=>formatDate2(str_replace('/','-',$strNewSJDate),'Y-m-d')
	));
}

}

/* End of File */