<?php
/*
PUBLIC FUNCTION:
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Masset extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('jw_aset');
}

public function getAllData()
{
	$this->setQuery("SELECT *, COUNT(*) as `jumlah_aset` FROM jw_aset WHERE aset_status = 3 GROUP BY aset_name ORDER BY aset_date DESC");

	if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID)
{
	$this->setQuery("SELECT * FROM jw_aset WHERE id = $intID GROUP BY aset_name");
	if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function add($type, $arrItem)
{
	if ($type == 'pembelian') {
		return $this->dbInsert(array(
			'aset_code' => $arrItem['strCode'],
			'aset_date' => $arrItem['txtDateBuat'],
			'aset_name' => $arrItem['strBarang'],
			'aset_jumlah_unit' => $arrItem['intAmounQty'],
			'aset_jumlah_satuan' => $arrItem['strSatuan'],
			'aset_harga' => $arrItem['intAmount'],
			'aset_nilai_perolehan' => $arrItem['intAmountFinal'],
			'aset_nilai_perolehan_default' => $arrItem['intAmountDefault'],
			'aset_nilai_sisa' => $arrItem['intAmountPenyusutanSisa'],
			'aset_sisa_penyusutan' => $arrItem['intAmountPenyusutanSisa'],
			'aset_lama_penyusutan' => $arrItem['intLama'],
			'aset_supp_id' => $arrItem['intSupplier'],
			'aset_acc_id_aset' => $arrItem['intCOAasset'],
			'aset_acc_id_akumulasi' => $arrItem['intCOAakumulasi'],
			'aset_acc_id_beban' => $arrItem['intCOAbeban'],
			'aset_keterangan' => $arrItem['strKeterangan'],
			'aset_ppn' => $arrItem['intPPN'],
			'aset_grandtotal' => $arrItem['intgrandTotal'],
			'aset_status' => 3
		));
	}


}


}

/* End of File */