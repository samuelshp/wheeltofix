<?php

class Mshipping extends JW_Model {

private $_strCountSelectFields,$_strListSelectFields,$_strShortListSelectFields,$_strDetailSelectFields;

# CONSTRUCTOR
public function __construct() { 
	parent::__construct(); 
	
	$this->initialize('jw_shipping');
	$this->_strCountSelectFields		= 'id';
	$this->_strListSelectFields			= 'id,ship_title,ship_province,ship_city,ship_district,ship_currency,ship_value_reg,ship_value_oke,ship_value_yes,ship_estimation_reg,ship_estimation_oke';
	$this->_strShortListSelectFields	= $this->_strListSelectFields;
	$this->_strDetailSelectFields		= 'id,ship_title,ship_province,ship_city,ship_district,ship_currency,ship_value_reg,ship_value_oke,ship_value_yes,ship_estimation_reg,ship_estimation_oke';
}

# LIST
public function getItems() {
	$this->dbSelect($this->_strListSelectFields,"ship_status > 1");
	
	return $this->getQueryResult('Array');
}

public function searchByKey($strKey) {
	$this->setQuery(
"SELECT *
FROM (
	(
		SELECT id AS ship_id,CONCAT(ship_title,' REGULER') AS ship_title,ship_province,ship_city,ship_district,ship_currency,ship_value_reg AS ship_value,ship_estimation_reg AS ship_estimation,'reg' AS ship_type
		FROM jw_shipping
		WHERE ship_district LIKE '%$strKey%' AND ship_status > 1
	) UNION (
		SELECT id AS ship_id,CONCAT(ship_title,' OKE') AS ship_title,ship_province,ship_city,ship_district,ship_currency,ship_value_oke AS ship_value,ship_estimation_oke AS ship_estimation,'oke' AS ship_type
		FROM jw_shipping
		WHERE ship_district LIKE '%$strKey%' AND ship_status > 1
	) UNION (
		SELECT id AS ship_id,CONCAT(ship_title,' YES') AS ship_title,ship_province,ship_city,ship_district,ship_currency,ship_value_yes AS ship_value,'1' AS ship_estimation,'yes' AS ship_type
		FROM jw_shipping
		WHERE ship_district LIKE '%$strKey%' AND ship_status > 1
	)
) AS s
ORDER BY ship_province,ship_city,ship_district,ship_title");
	
	return $this->getQueryResult('Array');
}

public function getItemsByWeight($fProdWeightTotal) {
	$this->dbSelect($this->_strListSelectFields,"ship_status > 1 AND ((ship_weight_lowbound <= $fProdWeightTotal AND $fProdWeightTotal <= ship_weight_upbound) OR (ship_weight_lowbound <= $fProdWeightTotal AND ship_weight_upbound = 0))");
	
	return $this->getQueryResult('Array');
}

# DETAIL
public function getItemByID($intID) {
	$this->dbSelect($this->_strDetailSelectFields,"ship_status > 1 AND id = $intID");
	return $this->getNextRecord('Array');
}

}

/* End of File */