<?php
/**
 * 
 */
class Mtransin extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->initialize('transaction_in');
	}

	public function getAllTransaction($strType, $intStartNo = -1, $intPerPage = -1)
    {    	    	
    	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "txin.txin_date DESC, txin.id DESC";
        else $strOrderBy = "txin.txin_date DESC, txin.id DESC LIMIT $intStartNo, $intPerPage";

        $this->setQuery(
            "SELECT txin.id, txin_code, cust_name, txin_date, SUM(txid_amount) as txou_amount, SUM(txid_totalbersih) as txou_totalbersih, txid_description FROM
			transaction_in as txin
			LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
			LEFT JOIN jw_customer AS cust ON txin_cust_id = cust.id
			WHERE txin_code LIKE '$strType%' AND txin_status_trans >= ".STATUS_APPROVED."
			GROUP BY id ORDER BY $strOrderBy"
        );
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

	public function getCount($strType) {        
        $this->dbSelect('id', "txin_code LIKE '$strType%' AND txin_status_trans >= ".STATUS_APPROVED);        
        return $this->getNumRows();
    }

	public function addTransIn($arrDataTransaksi)
	{
		return $this->dbInsert(array(			
			'txin_code' => $arrDataTransaksi['txin_code'],
			'txin_cust_id' => (isset($arrDataTransaksi['txin_cust_id']) ? $arrDataTransaksi['txin_cust_id'] : 0),
			'txin_date' => str_replace("/", "-", $arrDataTransaksi['txin_date']),
			'txin_acco_id' => (isset($arrDataTransaksi['txin_acco_id']) ? $arrDataTransaksi['txin_acco_id'] : 0),
			'txin_metode' => (isset($arrDataTransaksi['txin_metode']) ? $arrDataTransaksi['txin_metode'] : 0),			
			'txin_status_trans' => STATUS_APPROVED
		));
	}

	public function UpdateTransIn($arrDataTransaksi, $intID)
	{
		return $this->dbUpdate(array(			
			'txin_cust_id' =>  (isset($arrDataTransaksi['txin_cust_id']) ? $arrDataTransaksi['txin_cust_id'] : 0),
			'txin_date' => $arrDataTransaksi['txin_date'],
			'txin_acco_id' => $arrDataTransaksi['txin_acco_id'],
			'txin_metode' => $arrDataTransaksi['txin_metode'],
			'txin_ref_giro' => (isset($arrDataTransaksi['txin_ref_giro']) ? $arrDataTransaksi['txin_ref_giro'] : null),
			'txin_jatuhtempo' => (isset($arrDataTransaksi['txin_jatuhtempo']) ? $arrDataTransaksi['txin_jatuhtempo'] : null)
		), "id=$intID");
	}

	public function deleteTransIn($intID)
	{
		return $this->dbUpdate(
			array(
				'txin_status_trans' => STATUS_DELETED
			), "id='$intID'"
		);
	}

	public function copyToJournal()
	{
		# code...
	}

	public function copyToLaporan()
	{
		# code...
	}

	public function getListOfData($strPreCode)
	{
		$this->setQuery("
			SELECT txin.id, txin_code, txin_date, cust_name, txid_amount FROM transaction_in as txin 
			LEFT JOIN transaction_in_detail as txid ON txin.id = txid.txid_txin_id
			LEFT JOIN jw_customer as cust ON txin.txin_cust_id = cust.id
			WHERE txin_code LIKE '".$strPreCode."%';
		");

		if ($this->getNumRows() > 0) return $this->getQueryResult("Array");
		else return false;
	}

	public function getTransactionByID($intID)
	{
		$this->setQuery("
			SELECT txin.id, txin_code as code, txin_date as `date`, txin_metode as metode, txin_ref_giro, txin_jatuhtempo, txin_acco_id as acco_id, txin_cust_id,txin_status_trans FROM transaction_in as txin 
			LEFT JOIN transaction_in_detail as txid ON txin.id = txid.txid_txin_id			
			WHERE txin.id = '$intID'					
		");

		if ($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}	


	public function getDetail($intID)
	{
		$this->initialize('transaction_in_detail');
		$this->dbSelect("txid_type, txid_ref_id", "txid_txin_id=$intID");
		$arrRefData = $this->getNextRecord("Array");

		$select = "*";
		$join = '';
		$groupby = '';
		switch ($arrRefData['txid_type']) {
			case 'PML':
				$select = "txid.id, txid_acco_id, txid_ref_id, txid_amount, txid_description";
				$join = " LEFT JOIN kontrak AS kont ON txid_ref_id = kont.id";
			break;		
			case 'PPI':
				$select = "inv.id,invo_code, inv.cdate, inv.invo_grandtotal, txid.txid_amount, txid.txid_totalbersih, SUM(invp.invp_umcu_amount_used) AS terbayar, txid.txid_txin_pph, umcu_code, umc.id as umcu_id,umcu_amount, invp.id as invp_id, invp.invp_umcu_amount_used, (umcu_amount - invp.invp_umcu_amount_used) as sisa_umc";
				$join = " LEFT JOIN invoice AS inv ON txid_ref_id = inv.id";
				$join .= " LEFT JOIN invoice_paid AS invp ON invp.invp_txin_id = txin.id";
				$join .= " LEFT JOIN uang_masuk_customer AS umc ON umc.id = invp.invp_umcu_id";
				$groupby = "GROUP BY inv.id";
			break;
		}
		// $sql = "SELECT * FROM $strTableName WHERE $strTableName.id = ".$arrRefData['txod_ref_id'];		
		// $this->setQuery($sql);
		$this->setQuery("
			SELECT $select FROM transaction_in as txin 
			LEFT JOIN transaction_in_detail as txid ON txin.id = txid.txid_txin_id	
			$join WHERE txin.id = $intID $groupby;
		");
		
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function searchBy($strType, $arrDataSearch)
	{		
		$strWhere = '';
		if(!empty($arrDataSearch['txin_date'])) $strWhere .= " AND txin_date = '".str_replace("/", "-", $arrDataSearch['txin_date'])."'";
		if(!empty($arrDataSearch['txin_code'])) $strWhere .= " AND txin_code LIKE '%".$arrDataSearch['txin_code']."%'";
		if(!empty($arrDataSearch['txin_customer'])) $strWhere .= " AND cust_name LIKE '%".$arrDataSearch['txin_customer']."%'";

		$this->setQuery("
			SELECT txin.id, txin_code, cust_name, txin_date, SUM(txid_amount) as txou_amount, SUM(txid_totalbersih) as txou_totalbersih, txid_description FROM
			transaction_in as txin
			LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
			LEFT JOIN jw_customer AS cust ON txin_cust_id = cust.id
			WHERE txin_code LIKE '$strType%' AND txin_status_trans > ".STATUS_DELETED." $strWhere 
			GROUP BY id ORDER BY txin.txin_date DESC, txin.id DESC			
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
}