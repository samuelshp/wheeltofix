<?php

class Mtinydbvo extends JW_Model {

private $_arrTinyDBData;
private $_strCategory;
private $_intCount;

# CONSTRUCTOR
public function __construct() {
    parent::__construct();

    $this->_arrTinyDBData = array();
    $this->_strCategory = '';
    $this->_intCount = 0;

    parent::initialize('tinydb');
}

public function initialize($strTableName,$strPrefix = '') {
    if(!empty($strPrefix)) $strTableName = $strPrefix.'-'.$strTableName;

    $this->dbSelect('tiny_key AS strKey,tiny_data1 AS strData,tiny_data2 AS strData2,tiny_data3 AS strData3',"tiny_category = '$strTableName'",'CAST(tiny_key AS UNSIGNED) ASC');

    if($this->getNumRows() > 0) {

        $this->_intCount = $this->getNumRows();
        $this->_strCategory = $strTableName;
        $this->setData($this->getQueryResult('Array'));

        return TRUE;

    } else return FALSE;
}

# GETTER

public function getCount() { return $this->_intCount; }

# Get all data in the array data
public function getAllData() {
    if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Mtinydbvo_getAllData_'.$this->_strCategory))
        $this->_arrTinyDBData = $mixResult;
    return $this->_arrTinyDBData;
}

# Get single data in the array data by key
public function getSingleData($strKey,$intDataNumber = 1) {
    switch ($intDataNumber) {
        case 2: $strDataType = 'strData2'; break;
        case 3: $strDataType = 'strData3'; break;
        default: $strDataType = 'strData'; break;
    }

    foreach($this->getAllData() as $e) if($e['strKey'] == $strKey) return $e[$strDataType];

    # If not found, maybe it's an alias
    if($intDataNumber == 1)
        foreach($this->getAllData() as $e) if($e['strData3'] == $strKey) return $e['strData'];

    return FALSE;
}

# SETTER

# Set the data variable
public function setData($arrTinyDBData) {
    $this->cache->{JW_CACHE_TYPE}->save('Mtinydbvo_getAllData_'.$this->_strCategory, $arrTinyDBData, JW_CACHE_EXP_1_DAY);
    $this->_arrTinyDBData = $arrTinyDBData;
}

# save the data
public function saveData($strFileName = '') {
    $this->cache->{JW_CACHE_TYPE}->delete('Mtinydbvo_getAllData_'.$this->_strCategory);

    foreach($this->getAllData() as $e) $this->dbUpdate(
        array(
            'tiny_data1' => (!empty($e['strData'])) ? $e['strData'] : '',
            'tiny_data2' => (!empty($e['strData2'])) ? $e['strData2'] : '',
            'tiny_data3' => (!empty($e['strData3'])) ? $e['strData3'] : ''
        ),"tiny_category = '".$this->_strCategory."' AND tiny_key = '".$e['strKey']."'");

    return TRUE;
}

# Set single data in the array data by key
public function setSingleData($strKey,$strData,$intDataNumber = 1) {

    switch ($intDataNumber) {
        case 2: $strDataType = 'strData2'; break;
        case 3: $strDataType = 'strData3'; break;
        default: $strDataType = 'strData'; break;
    }

    for($i = 0; $i < $this->_intCount; $i++) if($this->_arrTinyDBData[$i]['strKey'] == $strKey) {
        $this->_arrTinyDBData[$i][$strDataType] = $strData;
        return TRUE;
    }

    return FALSE;
}

}

/* End of File */