<?php

class Mapicustom extends JW_Model {

private $_CI;
public function __construct() {
	parent::__construct();
	$this->_CI =& get_instance();
	$this->_CI->load->model('Mdbvo', '_Mtable');
}

# <jw:sql_select> <jw:sql_table> <jw:sql_where> <jw:sql_order> <jw:sql_limit>

public function afterPost($strTable, $arrPostData, $strWhere = '', $intID = 0, &$bolAfterPost = true) {
	# Prepare data for processing	
	$arrAffectedData = array();
	switch($strTable) {
		case 'gps_log':
			if(!empty($intID)) $strWhere = 'gpsl_user_id = '.$arrData['gpsl_user_id']." AND cdate = '$intID'";
			if(!empty($strWhere)) $this->_CI->_Mtable->dbSelect("", $strWhere, 'cdate DESC');
		break; default:
			if(!empty($intID)) $strWhere = "id = $intID";
			if(!empty($strWhere)) $this->_CI->_Mtable->dbSelect("", $strWhere);
		break;
	}
	if(!empty($strWhere)) $arrAffectedData = $this->_CI->_Mtable->getQueryResult('Array');

	# Do something here
	if($strTable == 'purchase_order_item') {
		$this->_CI->load->model('Mpurchaseorderitem');
		foreach($arrAffectedData as $e) $this->_CI->Mpurchaseorderitem->checkAutoClose($e['proi_purchase_order_id'], $e['id']);

	} elseif($strTable == 'purchase_item') {
		$this->_CI->load->model('Mpurchaseitem');
		foreach($arrAffectedData as $e) $this->_CI->Mpurchaseitem->checkAutoClose($e['prci_purchase_id']);	
	} elseif ($strTable == 'acceptance_item') {
		$this->_CI->load->model('Macceptanceitem');
		$this->_CI->load->model('Msubkontrakmaterial');
		$this->_CI->load->model('Mpurchaseitem');		
		foreach($arrAffectedData as $e);		
		$intAccID = ($strTable == 'acceptance') ? $e['id'] : $e['acit_acceptance_id'];		
		$arrAccItem = $this->_CI->Macceptanceitem->getItemsByAcceptanceID($intAccID);
		if (count($arrAccItem) > 0) {				
			for($i=0;$i<count($arrAccItem);$i++){
	            $purchase_item_id = $this->_CI->Mpurchaseitem->getIdOfPurchaseItem($arrAccItem[$i]['product_id_baru'],$arrAccItem[$i]['acit_purchase_id']);
	            $purchaseItemID = ($strTable == 'acceptance') ? $purchase_item_id[0]['id'] : $arrAccItem[$i]['purchase_item_id'];
	            $new_terterima[$i] = $this->_CI->Macceptanceitem->recalculateTerterima($arrAccItem[$i]['acit_purchase_id'],$arrAccItem[$i]['product_id_baru'],$purchaseItemID);
	            if ($new_terterima[$i] !== FALSE) {      
		            $new_terterima[$i]['jumlah'] = ($new_terterima[$i]['jumlah'] == null) ? 0 : $new_terterima[$i]['jumlah'];
					$new_terterima[$i]['jumlah2'] = ($new_terterima[$i]['jumlah2'] == null) ? 0 : $new_terterima[$i]['jumlah2'];
		            $this->_CI->Msubkontrakmaterial->updateTerBpb($arrAccItem[$i]['idSubKontrak'],$arrAccItem[$i]['product_id_baru'],$new_terterima[$i]['jumlah2']);
		            $this->_CI->Mpurchaseitem->UpdateTerterima($purchaseItemID, $new_terterima[$i]['jumlah'], $new_terterima[$i]['jumlah2']);
	        	} else {
	        		$bolAfterPost = FALSE;
	        	}
	        }		
	    }
	} elseif($strTable == 'acceptance') {
		//Handle Decline		
		$this->_CI->load->model('Macceptanceitem');
		$this->_CI->load->model('Msubkontrakmaterial');
		$this->_CI->load->model('Mpurchaseitem');		
		foreach($arrAffectedData as $e);		
		$intAccID = $e['id'];
		$arrAccItem = $this->_CI->Macceptanceitem->getItemsByAcceptanceID($intAccID);		
		if (count($arrAccItem) > 0) {			
			for($i=0;$i<count($arrAccItem);$i++){
				if (!empty($arrAccItem[$i]['product_id_baru']) && !empty($arrAccItem[$I]['acit_purchase_id'])){
		            $purchase_item_id = $this->_CI->Mpurchaseitem->getIdOfPurchaseItem($arrAccItem[$i]['product_id_baru'],$arrAccItem[$i]['acit_purchase_id']);
		            $purchaseItemID = ($strTable == 'acceptance') ? $purchase_item_id[0]['id'] : $arrAccItem[$i]['purchase_item_id'];
		            $new_terterima[$i] = $this->_CI->Macceptanceitem->recalculateTerterima($arrAccItem[$i]['acit_purchase_id'],$arrAccItem[$i]['product_id_baru'],$purchaseItemID);
		            if ($new_terterima[$i] !== FALSE) {      
			            $new_terterima[$i]['jumlah'] = ($new_terterima[$i]['jumlah'] == null) ? 0 : $new_terterima[$i]['jumlah'];
						$new_terterima[$i]['jumlah2'] = ($new_terterima[$i]['jumlah2'] == null) ? 0 : $new_terterima[$i]['jumlah2'];
			            $this->_CI->Msubkontrakmaterial->updateTerBpb($arrAccItem[$i]['idSubKontrak'],$arrAccItem[$i]['product_id_baru'],$new_terterima[$i]['jumlah2']);
			            $this->_CI->Mpurchaseitem->UpdateTerterima($purchaseItemID, $new_terterima[$i]['jumlah'], $new_terterima[$i]['jumlah2']);
		        	} else {
		        		$bolAfterPost = FALSE;
		        	}	        	
				}	
	        }		
	    }	    

	}

	return $bolAfterPost;

}

public function itemGetConditionals($strTable, $strField, $strKey, &$arrWhere, &$arrOrder, &$bolAllowMultiDimension) {
	switch($strTable) {
		case 'product_category_per_supplier':
			$arrWhere[] = "s.$strField = '$strKey'";
			$arrOrder[] = 't.id ASC';
			$bolAllowMultiDimension = true;
		break; default:
			$arrWhere[] = "$strField = '$strKey'";
		break;
	}
}

public function detectTable($strTable, &$strGlobalTable, &$strCustomQuery) {
	switch($strTable) {		
		case 'invoice_list':
			$strCustomQuery = "SELECT t.id as invo_id, invo_code, invo_grandtotal, t.cdate as createAt, invo_grandtotal - invo_payment_value as sisaBayar FROM invoice as t
			LEFT JOIN subkontrak as subkont ON subkont.id = t.invo_subkontrak_id
			LEFT JOIN kontrak as kont ON kont.id = subkont.kontrak_id
			WHERE <jw:sql_where> AND t.invo_status > ".STATUS_DELETED." AND t.invo_status < ".STATUS_FINISHED."
			HAVING sisaBayar > 0
			ORDER BY t.id
			";
			break;
		case 'detail_dph':
			$strCustomQuery = "SELECT t.id as dph_id, dphu_code,dphu_amount,supp_id, dphu_status FROM (
				-- Untuk reff ke PI
				SELECT dph.id, dph.dphu_code, dph.dphu_amount, pi.pinv_supplier_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
				LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
				LEFT JOIN purchase_invoice AS pi ON dph_item.dphi_reff_id = pi.id
				WHERE dph_item.dphi_reff_type = 'PI' AND (pinv_status <= ".STATUS_FINISHED." AND pinv_status != ".STATUS_DELETED.")
				UNION ALL
				-- Untuk reff ke NKS
				SELECT dph.id, dph.dphu_code, dph.dphu_amount, txod.txod_supp_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
				LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
				LEFT JOIN transaction_out AS txou ON dph_item.dphi_reff_id = txou.id
				LEFT JOIN transaction_out_detail AS txod ON dph_item.dphi_reff_id = txod.txod_txou_id
				WHERE (dph_item.dphi_reff_type = 'NKS' OR dph_item.dphi_reff_type = 'NDS') AND (txou_status_trans < ".STATUS_FINISHED." AND txou_status_trans != ".STATUS_DELETED.") 
				UNION ALL
				-- Untuk reff ke DP
				SELECT dph.id, dph.dphu_code, dph.dphu_amount, dpay_supplier_id AS supp_id, dph.dphu_status FROM daftar_pembayaran_hutang AS dph
				LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
				JOIN downpayment AS dp ON dph_item.dphi_reff_id = dp.id
				WHERE dph_item.dphi_reff_type = 'DP' AND (dpay_status <= ".STATUS_FINISHED." AND dpay_status != ".STATUS_DELETED.")
				) AS t
				LEFT JOIN jw_supplier AS supp ON t.supp_id = supp.id
				WHERE <jw:sql_where> AND dphu_status = ".STATUS_APPROVED."
				GROUP BY dphu_code";
			break;
		case 'kontrak_customer_subkontrak':
			$strCustomQuery = "SELECT cust_name FROM kontrak AS t LEFT JOIN jw_customer AS cust ON t.owner_id = cust.id WHERE <jw:sql_where>";
			break;
		case 'bahan_subkontrak_material':
			$strCustomQuery = "SELECT t.subkontrak_id, t.material, sk.kontrak_id, jp.prod_title, ju.unit_title
	        FROM subkontrak_material as t
	        LEFT JOIN subkontrak as sk ON sk.id = t.subkontrak_id
	        LEFT JOIN jw_product as jp ON jp.id = t.material
	        LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id
	        WHERE <jw:sql_where>";
			break;
		case 'last_termin':
			$strCustomQuery = "SELECT t.id as subkontrak_termin_id,t.* FROM subkontrak_termin as t
			WHERE (due_date = '0000-00-00' OR due_date IS NULL) AND status = ".STATUS_APPROVED." AND <jw:sql_where>
			ORDER BY due_date ASC";
			break;
		case 'purchase_order_product':
			$strCustomQuery = "SELECT t.id as po_id, poi.id as poi_id, t.cdate as createAt, sk.job as kont_job, t.idKontrak as kont_id, t.pror_code, jp.prod_title
			FROM purchase_order as t
            LEFT JOIN purchase_order_item as poi ON poi.proi_purchase_order_id = t.id
            LEFT JOIN jw_product as jp ON jp.id = poi.proi_product_id
			LEFT JOIN subkontrak as sk ON sk.kontrak_id = t.idKontrak
			WHERE t.approvalPM = 3 AND poi.proi_quantity1 > poi.proi_terpurchase AND t.pror_status = ".STATUS_WAITING_FOR_FINISHING." AND <jw:sql_where>
            GROUP BY poi.id
            ORDER BY t.cdate, t.id, jp.prod_title ASC";
			break;
		case 'purchase_order_item_purchase':
			$strCustomQuery = "SELECT t.id as poi_id, t.cdate, po.idSubKontrak, jp.prod_title, ju.unit_title as 'sat_pb', ju2.unit_title as 'sat_bayar', ju3.unit_title as 'sat_terima', t.proi_description, t.proi_quantity1, t.proi_unit1, t.proi_quantity_terima, po.idSubKontrak, t.proi_product_id, t.proi_terpurchase, ju3.unit_title as title_terima, t.proi_terpurchase_bpb, t.proi_purchase_order_id
		    FROM purchase_order_item AS t
		    LEFT JOIn purchase_order as po ON po.id = t.proi_purchase_order_id
		    LEFT JOIN jw_product as jp ON jp.id = t.proi_product_id 
		    LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
		    LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_bayar_id 
		    LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id 
		    WHERE <jw:sql_where>
		    GROUP BY t.id ";
			break;
		case 'kontrak_pembelian_supplier':
			$strCustomQuery = "SELECT t.id as kp_id, kopb_code, kopb_date, jp.prod_title, kopb_supplier_id, kopb_product_id, kopb_amount, kopb_description, supp_name
	        FROM kontrak_pembelian AS t
	        LEFT JOIN jw_supplier AS supp on supp.id = kopb_supplier_id
	        LEFT JOIN jw_product AS jp on jp.id = t.kopb_product_id
	        LEFT JOIN purchase_order_item AS proi on proi_product_id = jp.id
	        WHERE kopb_qty > kopb_po AND <jw:sql_where>
	        ORDER BY kopb_code DESC";
			break;
		case 'acceptance_supplier_kontrak':
			$strCustomQuery = "SELECT t.acce_code, k.kont_name, p.prch_supplier_id, t.cdate as createAt, t.id as acc_id, js.supp_name, p.id as idOrderPembelian, k.id as idKontrak
			FROM acceptance as t
	        LEFT JOIN acceptance_item as ai On ai.acit_acceptance_id = t.id
			LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
	        LEFT JOIN jw_supplier as js ON js.id = p.prch_supplier_id
			LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
			LEFT JOIN kontrak as k ON k.id = po.idKontrak
			WHERE <jw:sql_where>
			";			
			break;
		case 'acceptance_detail':
			$strCustomQuery = "SELECT t.id as acci_id, jp.prod_title, po.idSubKontrak, poi.proi_quantity1, ju2.unit_title as title_pb, t.acit_quantity1, ju3.unit_title as title_terima, t.acit_description, t.acit_terpi, t.acit_location, ju.unit_title as title_bayar, t.acit_product_id, p.prch_po_id, p.id as idOrderPembelian, jp.id as idProduct, poi.proi_description, pi.prci_price, pi.id, t.acit_quantity_bayar, t.acit_acceptance_id
			FROM acceptance_item as t
	        LEFT JOIN purchase as p ON p.id = t.acit_purchase_id
			LEFT JOIN purchase_item as pi ON pi.id = t.acit_purchase_item_id AND prci_product_id = t.acit_product_id
			LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
			LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id AND poi.proi_purchase_order_id = p.prch_po_id AND poi.proi_product_id = t.acit_product_id
			LEFT JOIN jw_product as jp ON jp.id = t.acit_product_id
			LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_bayar_id
			LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_pb_id
			LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id
			WHERE <jw:sql_where>
	        group by t.id";
			break;
		case 'downpayment_supplier_kontrak':
			$strCustomQuery = "SELECT t.*, t.id as dp_id, kopb_code, supp.supp_name, k.kont_name
			FROM downpayment AS t
			LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
			LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
			LEFT JOIN kontrak as k on k.id = t.dpay_kontrak_id
			WHERE <jw:sql_where> AND dpay_terbayar > 0";
			break;
		case 'downpayment_detail':
			$strCustomQuery = "SELECT t.*, t.id as dp_id, kopb_code, supp_name, kont_name
            FROM downpayment AS t
            LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
            LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
            LEFT JOIN kontrak as k on k.id = t.dpay_kontrak_id
            LEFT JOIN purchase_invoice_history as pih on pih.pinvhis_dp = t.id
            WHERE <jw:sql_where> AND dpay_status in (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")
            GROUP BY t.id";
			break;
		case 'downpayment_used':
			$strCustomQuery = "SELECT SUM(pinvhis_dipakai) as total_terpakai, id as pinvhis_id, pinvhis_dp, pinvhis_supplier
            from purchase_invoice_history
            WHERE <jw:sql_where> AND pinvhis_status >= ".STATUS_APPROVED."";
			break;
		case 'purchase_invoice_list':		
			$strCustomQuery = "
			SELECT * FROM 
			(SELECT 
			t.id as pi_id, pinv_code, (pinv_subtotal - pinv_discount - pinv_dp)as pinv_grandtotal, pinv_tax, pinv_totalbeforepph, pinv_pph, pinv_finaltotal, (pinv_finaltotal-pinv_terbayar) as sisa_bayar, acc.cdate as createAt, pinv_supplier_id as supp_id, CONCAT('PI') as type, pinv_status as status
			FROM purchase_invoice AS t
			LEFT JOIN acceptance AS acc ON pinv_acceptance_id = acc.id					
			WHERE (t.pinv_finaltotal >= t.pinv_sisa_bayar) AND t.pinv_status < ".STATUS_FINISHED." AND t.pinv_status <> ".STATUS_DELETED."
			AND t.id NOT IN (
				SELECT aa.dphi_reff_id FROM daftar_pembayaran_hutang a
				JOIN daftar_pembayaran_hutang_item aa ON a.id = aa.dphi_dphu_id
				WHERE a.dphu_status = 2 AND aa.dphi_reff_type = 'PI'
			)
			UNION ALL
			SELECT txou.id, txou_code, txod_amount, txod_ppn, (txod_amount+(txod_amount*txod_ppn/100)) as txod_grandtotal,  txod_pph, txod_totalbersih, (txod_totalbersih - txod_terbayar) as sisa_bayar, txou_date, txod_supp_id as supp_id, CONCAT('NKS') as type, txou_status_trans as status
			FROM transaction_out AS txou
			LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id	
			WHERE txod_type = 'NKS' AND (txod_totalbersih >= 0 AND txod_sisa_bayar >= 0)
			UNION ALL
			SELECT dp.id, dpay_code, dpay_amount, dpay_ppn, dpay_amount_ppn, dpay_pph, dpay_amount_final, dpay_sisa_bayar, dp.dpay_date, dpay_supplier_id as supp_id, CONCAT('DP') as type, dpay_status as status FROM downpayment AS dp			
			WHERE (dpay_amount_final >= dp.dpay_sisa_bayar) AND dpay_sisa_bayar > 0 AND dp.id NOT IN (SELECT dphi_reff_id FROM daftar_pembayaran_hutang_item dphitem JOIN daftar_pembayaran_hutang dph ON dphitem.dphi_dphu_id = dph.id WHERE dphi_reff_type = 'DP' AND dph.dphu_status != ".STATUS_DELETED.")	
			) as t			
			WHERE <jw:sql_where> AND pinv_grandtotal > 0 AND sisa_bayar > 0 AND status > ".STATUS_DELETED."
			GROUP BY pinv_code
			";
			break;
		case 'alokasi_barang_pembelian_purchase':
			$strCustomQuery = "SELECT * FROM (SELECT t.prch_code, op_item.id AS op_item_id, prod.prod_title, op_item.prci_quantity1, unit.unit_title, op_item.prci_price, (op_item.prci_quantity1 * op_item.prci_price) AS totalBelumPPn, pb.idKontrak as idKontrak, pb.idSubKontrak as idSubKontrak, t.cdate as prch_date, pb.pror_code
			FROM purchase AS t 
				JOIN purchase_item AS op_item ON op_item.prci_purchase_id = t.id
				JOIN purchase_order AS pb ON t.prch_po_id = pb.id
				JOIN jw_product as prod ON prod.id = op_item.prci_product_id
				JOIN jw_unit AS unit ON prod.satuan_bayar_id = unit.id
			WHERE <jw:sql_where> AND op_item.prci_product_id NOT IN (SELECT material FROM subkontrak AS sk INNER JOIN subkontrak_material AS pb ON sk.id = pb.subkontrak_id WHERE sk.kontrak_id = pb.idKontrak AND sk.id = pb.idSubKontrak)) as tbPurchase WHERE tbPurchase.op_item_id NOT IN (SELECT albp_ref_id FROM alokasi_barang_pembelian WHERE albp_ref_type = 'prci')";
			break;
		case 'alokasi_barang_pembelian_pengeluaran':
			$strCustomQuery = "SELECT * FROM (SELECT txod.id as txod_id, txou_code, txou_date, txod_totalbersih, txod_description FROM transaction_out AS txou 
				LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id
			WHERE txod_type = 'PGL' AND <jw:sql_where>) as tbPgl WHERE tbPgl.txod_id NOT IN (SELECT albp_ref_id FROM alokasi_barang_pembelian WHERE albp_ref_type = 'pgl')";
			break;
		default:
			$strGlobalTable = $strTable;
			$strCustomQuery = '';
		break;
	}
}

public function detectInsert($strTable, $arrData) {
	switch($strTable) {
		case 'gps_log':
			$this->_CI->_Mtable->dbInsert($arrData);
			$this->_CI->_Mtable->setQuery("SELECT cdate FROM gps_log WHERE gpsl_user_id = '".$arrData['gpsl_user_id']."' ORDER BY cdate DESC");
			return $this->_CI->_Mtable->getNextRecord('Object')->cdate;
		case 'notification':			
			$this->_CI->load->model('Mnotification');
			return $this->_CI->Mnotification->add($arrData);
			break;
		break; default:
			return $this->_CI->_Mtable->dbInsert($arrData);
		break;
	}
}

}