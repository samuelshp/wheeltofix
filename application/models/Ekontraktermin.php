<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ekontraktermin extends Eloquent 
{
    protected $table = 'kontrak_termin';
    public $timestamps = false;

    public function kontrak()
    {
        return $this->belongsTo(Ekontrak::class);
    }
}