<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Esubkontrakteam extends Eloquent 
{
    protected $table = 'subkontrak_team';
    public $timestamps = false;

    public function staff()
    {
        return $this->belongsTo(Estaff::class, 'name');
    }


}