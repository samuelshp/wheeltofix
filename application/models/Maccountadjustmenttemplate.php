<?php

class Maccountadjustmenttemplate extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();
    $this->initialize('accountadjustment_template');
}

public function getAllItems() {
    $this->dbSelect('');
    return $this->getQueryResult('Array');
}

public function getItemByID($intID) {
    $this->dbSelect('', "id = $intID");
    $arrData = $this->getNextRecord('Array');
    $arrData['acte_setting'] = $arrData['acte_setting'];

    return $arrData;
}

public function add($strTemplateName, $arrTemplateSetting) {
    return $this->dbInsert(array(
        'acte_title' => $strTemplateName,
        'acte_setting' => json_encode($arrTemplateSetting),
    ));
}

public function delete($intID) {
    return $this->dbDelete("id = $intID");
}

}

/* End of File */