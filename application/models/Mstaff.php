<?php

class Mstaff extends JW_Model 
{
	protected $table = 'adm_login'; 

	# CONSTRUCTOR
	public function __construct() { 
		parent::__construct(); 
		
		$this->initialize('adm_login');
	}

	public function all()
	{
        $this->setQuery(
            "SELECT *
            FROM {$this->table}"
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
	}

	public function getCurrentActive() {
		$_CI =& get_instance();
		$intID = $_CI->session->userdata('strAdminID');
		$this->dbSelect('', "id = $intID");
		return $this->getNextRecord('Array');
	}

	public function getItemsByType($strType) {
		$this->dbSelect('', "adlg_priviledge = $strType AND adlg_status > 1");
		return $this->getQueryResult('Array');
	}

}

/* End of File */