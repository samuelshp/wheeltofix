<?php
/*
PUBLIC FUNCTION:
- add(intDeliveryID,strDate)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdeliveryback extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('deliveryback');
}

public function add($intDeliveryID,$strDate,$txtDescription,$status) {
    return $this->dbInsert(array(
        'deba_delivery_id' => $intDeliveryID,
        'deba_description' => $txtDescription,
        'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i'),
        'deba_status' => $status
    ));
}

public function searchByDeliverer($strDelivererName, $arrSearchDate, $intStatus) {
    $strWhere = '';
    if(!empty($strDelivererName)) {
        $strDelivererName = urldecode($strDelivererName);
        $strWhere .= " AND (sals_name LIKE '%{$strDelivererName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (d.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (d.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (deba_status = {$intStatus})";

    $this->setQuery(
"SELECT d.id, deba_code, deba_description, sals_name, sals_address, sals_phone,d.cdate, deba_status,deli_timereturn
FROM deliveryback AS d
LEFT JOIN delivery AS de ON deba_delivery_id = de.id
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
WHERE d.id > 0{$strWhere}
ORDER BY d.cdate DESC, d.id DESC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCount() {
    $this->dbSelect('id');
    return $this->getNumRows();
}

public function getItems($intStartNo = -1,$intPerPage = -1) {
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "d.cdate DESC, d.id DESC";
    else $strOrderBy = "d.cdate DESC, d.id DESC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT d.id, deba_code, deli_description, sals_name, sals_address, sals_phone,d.cdate, deba_status,deli_timereturn
FROM deliveryback AS d
LEFT JOIN delivery AS de ON deba_delivery_id = de.id
LEFT JOIN jw_warehouse AS w ON deli_froMwarehouse = w.id
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY d.cdate DESC, d.id DESC LIMIT 0,1";
    else $strWhere = "WHERE d.id = $intID";

    $this->setQuery(
"SELECT d.id,deli_code,deba_code, d.cdate AS deba_date,deli_timereturn, deba_description, deba_status,sals_code,sals_name,sals_address,sals_phone,deli_time
FROM deliveryback AS d
LEFT JOIN delivery as de ON deba_delivery_id = de.id
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
    $this->setQuery(
"SELECT a.id, deli_timereturn AS deba_date,deba_code, sals_name
FROM deliveryback AS a
LEFT JOIN delivery AS  de ON deba_delivery_id = de.id
LEFT JOIN jw_salesman AS s ON s.id = de.deli_salesman
WHERE (a.cby = $intUserID OR a.mby = $intUserID)
ORDER BY a.cdate DESC, a.id DESC LIMIT 0, $intPerPage");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function editByID2($intID,$strDescription,$intStatus) {
    return $this->dbUpdate(array(
            'deba_description' => $strDescription,
            'deba_status' => $intStatus),
        "id = $intID");
}

public function editByID($intID,$intStatus) {
    return $this->dbUpdate(array(
            'deba_status' => $intStatus),
        "id = $intID");
}

public function deleteByID($intID) {
    return $this->dbDelete("id = $intID");
}
}

/* End of File */