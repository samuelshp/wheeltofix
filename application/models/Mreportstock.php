<?php

class Mreportstock extends JW_Model {

public $_intWarehouseID;

public function __construct() {
	parent::__construct();	
	$this->_intWarehouseID = 1;
}

public function getItemsStock($intStartNo = -1, $intPerPage = -1) {
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "proc_title, prob_title, prod_title ASC";
	else $strOrderBy = "proc_title, prob_title, prod_title ASC LIMIT $intStartNo, $intPerPage";
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(pen.qty, 0) pending
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history
		WHERE trhi_type in ('purchase','purchase_retur','invoice','invoice_retur','adjustment','opname','mutasi_in','mutasi_out','kanvas_muat','kanvas_turun')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history th
		LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
		INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)	
		WHERE trhi_type in ('invoice') AND i.id NOT IN (SELECT deit_invoice_id FROM delivery_item di)
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
        GROUP BY trhi_product_id
	) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY $strOrderBy");
    
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
    
}

public function getCountStock() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

	/*$this->setQuery(
"SELECT p.id
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id
		FROM transaction_history
		WHERE trhi_type in ('purchase','purchase_retur','invoice','invoice_retur','adjustment','opname','mutasi_in','mutasi_out','kanvas_muat','kanvas_turun')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN (
		SELECT trhi_product_id
		FROM transaction_history th
			LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
			INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)	
		WHERE trhi_type in ('invoice') AND i.id NOT IN (SELECT deit_invoice_id FROM delivery_item di)
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY prod_title");*/
	$this->setQuery("SELECT id FROM jw_product p");
    
	return $this->getNumRows();
    
}
    
/*
public function getStockPrintout() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(pen.qty, 0) pending
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history
		WHERE trhi_type in ('adjustment','purchase','purchase_retur','invoice','invoice_retur')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history th
		LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
		INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)	
		WHERE trhi_type in ('invoice') AND i.id NOT IN (SELECT deit_invoice_id FROM delivery_item di)
        	AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY proc_title, prob_title, prob_title asc");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
*/
    
public function searchStockBySupplier($strSupplierName) {
    $strSupplierName = urldecode($strSupplierName);
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;
    
    	$this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(pen.qty, 0) pending
FROM jw_product p
	LEFT JOIN (SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
			   FROM transaction_history
			   WHERE trhi_type in ('purchase','purchase_retur','invoice','invoice_retur','adjustment','opname','mutasi_in','mutasi_out','kanvas_muat','kanvas_turun')
					AND trhi_warehouse_id = $intWarehouseID
					AND trhi_status in (2,3)
               GROUP BY trhi_product_id
				) st ON st.trhi_product_id = p.id
	LEFT JOIN (SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
				FROM transaction_history th
					LEFT JOIN invoice_item ii ON (ii.id = th.trhi_transaction_id)
					INNER JOIN invoice i ON (i.id = ii.invi_invoice_id)	
				WHERE trhi_type in ('invoice') AND i.id NOT IN (SELECT deit_invoice_id FROM delivery_item di)
					AND trhi_warehouse_id = $intWarehouseID
					AND trhi_status in (2,3)
                GROUP BY trhi_product_id 
				) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
WHERE (supp_name LIKE '%$strSupplierName%')
ORDER BY proc_title, prob_title, prob_title asc");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCountStockKanvas() {

	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

	/*$this->setQuery(
"SELECT p.id
FROM jw_product p
	LEFT JOIN (SELECT trhi_product_id
		FROM transaction_history
		WHERE trhi_type in ('kanvas_muat','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN (SELECT trhi_product_id
		FROM transaction_history th
			LEFT JOIN kanvas_item ki ON (ki.id = th.trhi_transaction_id)
			INNER JOIN kanvas k ON (k.id = ki.kavi_kanvas_id)	
		WHERE trhi_type in ('kanvas_muat','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY prod_title");*/
	$this->setQuery("SELECT id FROM jw_product p");
    
	return $this->getNumRows();
    
}

public function getItemsStockKanvas($intStartNo = -1, $intPerPage = -1) {
    
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "proc_title, prob_title, prod_title ASC";
	else $strOrderBy = "proc_title, prob_title, prod_title ASC LIMIT $intStartNo, $intPerPage";
	
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(pen.qty, 0) pending
FROM jw_product p
	LEFT JOIN (SELECT trhi_product_id, SUM((IF(trhi_type='kanvas_muat',1,-1)) * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
			   FROM transaction_history
			   WHERE trhi_type in ('kanvas_muat','kanvas_jual')
			   		AND trhi_warehouse_id = $intWarehouseID
			   		AND trhi_status in (2,3)
               GROUP BY trhi_product_id
				) st ON st.trhi_product_id = p.id
	LEFT JOIN (SELECT trhi_product_id, SUM((IF(trhi_type='kanvas_muat',1,-1)) * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
				FROM transaction_history th
					LEFT JOIN kanvas_item ki ON (ki.id = th.trhi_transaction_id)
					INNER JOIN kanvas k ON (k.id = ki.kavi_kanvas_id)	
				WHERE trhi_type in ('kanvas_muat','kanvas_jual') /* AND k.id NOT IN (SELECT deit_invoice_id FROM delivery_item di) */
					AND trhi_warehouse_id = $intWarehouseID
					AND trhi_status in (2,3)
                GROUP BY trhi_product_id
				) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY $strOrderBy");
    
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
    
}

public function searchStockKanvasBySalesman($strSalesmanName) {
    $strSalesmanName = urldecode($strSalesmanName);
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    	$this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(pen.qty, 0) pending
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM((IF(trhi_type='kanvas_muat',1,-1)) * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history
		WHERE trhi_type in ('kanvas_muat','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3)
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN (
		SELECT trhi_product_id, SUM((IF(trhi_type='kanvas_muat',1,-1)) * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
		FROM transaction_history th
		LEFT JOIN kanvas_item ki ON (ki.id = th.trhi_transaction_id)
		INNER JOIN kanvas k ON (k.id = ki.kavi_kanvas_id)
		LEFT JOIN jw_salesman s ON (s.id = k.kanv_salesman_id)
		WHERE trhi_type in ('kanvas_muat','kanvas_jual') /* AND k.id NOT IN (SELECT deit_invoice_id FROM delivery_item di) */
			AND trhi_warehouse_id = $intWarehouseID
			AND trhi_status in (2,3) AND (sals_name LIKE '%$strSalesmanName%')
		GROUP BY trhi_product_id
	) pen ON pen.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
ORDER BY proc_title, prob_title, prob_title asc");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;

}

public function getCountSellOut() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    /*$this->setQuery(
"SELECT p.id
FROM jw_product p
	LEFT JOIN (SELECT trhi_product_id
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
    	GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
ORDER BY proc_title, prob_title, prob_title asc");*/

    $this->setQuery("SELECT id FROM jw_product p");
    
	return $this->getNumRows();
}
    
public function getItemsSellOut($intStartNo = -1,$intPerPage = -1) {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;
    
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "proc_title, prob_title, prod_title ASC";
	else $strOrderBy = "proc_title, prob_title, prod_title ASC LIMIT $intStartNo, $intPerPage";
	
    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(st.subtotal, 0) subtotal
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM((trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty,
					SUM((trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) * (trhi_price / trhi_conv1)) AS subtotal
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
ORDER BY $strOrderBy");
    
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;    
    
}
    
public function searchSellOutBySupplier($strSupplierName) {
    $strSupplierName = urldecode($strSupplierName);
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(st.subtotal, 0) subtotal
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) AS qty,
			SUM((trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) * (trhi_price / trhi_conv1)) AS subtotal
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
WHERE (supp_name LIKE '%$strSupplierName%')
ORDER BY proc_title, prob_title, prob_title asc");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;    
    
}
    
public function getCountStockInValue() {
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    /*$this->setQuery(
"SELECT p.id
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
ORDER BY proc_title, prob_title, prob_title asc");*/

    $this->setQuery("SELECT id FROM jw_product p");
    
	return $this->getNumRows();
}
    
public function getItemsStockInValue($intStartNo = -1,$intPerPage = -1) {
    
    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "proc_title, prob_title, prod_title ASC";
	else $strOrderBy = "proc_title, prob_title, prod_title ASC LIMIT $intStartNo, $intPerPage";
	if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(st.qty, 0) * (prod_hpp / prod_conv1) nilai, coalesce(st.qty, 0) * (prod_price / prod_conv1) nilai_jual
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) AS qty
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
ORDER BY $strOrderBy");
    
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;    
    
}
    
public function searchStockInValueBySupplier($strSupplierName) {
    $strSupplierName = urldecode($strSupplierName);
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    // LEFT JOIN (SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty
    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(st.qty, 0) * (prod_hpp / prod_conv1) nilai, coalesce(st.qty, 0) * (prod_price / prod_conv1) nilai_jual
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) AS qty
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
WHERE (supp_name LIKE '%$strSupplierName%')
ORDER BY proc_title, prob_title, prob_title asc");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;    
    
}

}