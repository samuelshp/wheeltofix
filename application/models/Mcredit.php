<?php
/**
 * 
 */
class Mcredit extends JW_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getUMCList($intCustID)
	{
		$this->setQuery("
			SELECT umc.id as id, umcu_code as code, umcu_date  as `date`, umcu_cust_id as cust_id, umcu_amount as amount FROM uang_masuk_customer as umc
			WHERE umcu_cust_id = '$intCustID' AND umcu_status = ".STATUS_OUTSTANDING."
		");

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getInvoiceList($intCustID)
	{
		$this->setQuery("
			SELECT inv.id, invo_code, inv.cdate, invo_grandtotal, invo_payment_value FROM invoice as inv
			LEFT JOIN subkontrak as sub on inv.invo_subkontrak_id = sub.id
			LEFT JOIN kontrak as kont on sub.kontrak_id = kont.id
			WHERE kont.owner_id = $intCustID AND invo_status < ".STATUS_FINISHED." 
		");

		if ($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function updateUMC($arrUMCDipakai, $arrDianggapLunas)
	{		
		$this->initialize("uang_masuk_customer");		

		foreach ($arrUMCDipakai as $key => $value) {
			if ($value != ''){				
				$items = explode(",", $value);		
				$i = 0;
				while ($i < sizeof($items)){					
					$strItem = explode("-", $items[$i]);
					$this->dbSelect("umcu_amount", "id = ".$strItem[0]);
					$amount = $this->getNextRecord('Array')['umcu_amount'];
					$this->setQuery("SELECT SUM(invp_umcu_amount_used) as `terpakai` FROM invoice_paid WHERE invp_umcu_id = ".$strItem[0]." AND invp_status > ".STATUS_DELETED);
					$terpakai = (Double) $this->getNextRecord('Array')['terpakai'];
					// $terpakai = $terpakai + (Double) $strItem[1];
					
					if ($terpakai >= $amount) $status = STATUS_FINISHED;
					else $status = STATUS_WAITING_FOR_FINISHING;

					$intUpdate = $this->dbUpdate(
						array(
							'umcu_status' => $status,
							'umcu_terpakai' => $terpakai 
						),
						"id = '$strItem[0]'"
					);

					if(!$intUpdate) return false;
					else $i++;
				}
				
			}
		}
		return $intUpdate;
	}

	public function insertInvoicePaid($intTransactionID, $arrUMCDipakai, $arrDianggapLunas)
	{
		$this->initialize("invoice_paid");

		foreach ($arrUMCDipakai as $key => $value) {
			if ($value != ''){
				$items = explode(",", $value);		
				$i = 0;
				while ($i < sizeof($items)){
					$strItem = explode("-", $items[$i]);
					$intInsert = $this->dbInsert(
						array(
							'invp_txin_id' => $intTransactionID,
							'invp_invo_id' => $strItem[2],
							'invp_umcu_id' => $strItem[0],
							'invp_lunas' => (!is_null($arrDianggapLunas[$key])) ? 1 : 0,
							'invp_umcu_amount_used' => (!is_null($arrDianggapLunas[$key])) ? floatval($strItem[1])+floatval($strItem[4]) : $strItem[1]
						)
					);
					if(!$intInsert) return false;
					else $i++;
				}
			}			
		}

		return $intInsert;		
	}

	public function getInvoicePaidItemsByID($id)
    {
        $this->setQuery("SELECT * FROM invoice_paid WHERE invp_txin_id = $id");        

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

	public function updateInvoicePaid($arrUMCDipakai, $invpID,$arrDianggapLunas)
	{
		$this->initialize("invoice_paid");
		foreach ($arrUMCDipakai as $key => $value) {
			if ($value != ''){
				$items = explode(",", $value);		
				$i = 0;
				while ($i < sizeof($items)){
					$strItem = explode("-", $items[$i]);
					$intUpdate = $this->dbUpdate(
						array(
							'invp_invo_id' => $strItem[2],
							'invp_umcu_id' => $strItem[0],
							'invp_lunas' => (!is_null($arrDianggapLunas[$key])) ? 1 : 0,
							'invp_umcu_amount_used' => (!is_null($arrDianggapLunas[$key])) ? floatval($strItem[1])+floatval($strItem[4]) : $strItem[1]
						), "id = $invpID"
					);
					if(!$intUpdate) return false;
					else $i++;
				}
			}			
		}

		return true;
		// foreach ($arrPostData['txid_invp_id'] as $key => $v) {
		// 	$this->dbUpdate(
		// 		array(
		// 			'invp_invo_id' => $arrPostData[$key]['invp_invo_id'],
		// 			'invp_umcu_id' => $arrPostData[$key]['invp_umcu_id'],
		// 			'invp_umcu_amount_used' => $arrPostData[$key]['txid_totalbersih']
		// 		),
		// 		"id = $v"
		// 	);
		// }		
	}

	public function updateInvoice($arrUMCDipakai)
	{
		foreach ($arrUMCDipakai as $key => $value) {

			if ($value!='') {
				$items = explode(",", $value);		
				$i = 0;
				while ($i < sizeof($items)){
					$strItem = explode("-", $items[$i]);
					// $this->setQuery("SELECT SUM(invp_umcu_amount_used) as umcu_terpakai, invp_txin_id FROM invoice_paid as invp JOIN uang_masuk_customer as umc ON invp.invp_umcu_id = umc.id WHERE invp_invo_id = $strItem[2]");
					$this->setQuery("SELECT txid.txid_txin_id, SUM(txid_amount) AS dibayar, txid.txid_ref_id
						FROM transaction_in AS txin
						LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
						WHERE txid.txid_type = 'PPI' AND txid.txid_ref_id = ".$strItem[2]." AND txin.txin_status_trans > ".STATUS_DELETED."
						GROUP BY txid_ref_id");
					$invo_dibayar = $this->getNextRecord('Array')['dibayar'];
					
					$this->initialize("invoice");
					$this->dbUpdate(
						array(				
							'invo_payment_value' => $invo_dibayar,				
						),
						"id = $strItem[2]"
					);

					$this->dbSelect('invo_payment_value, invo_grandtotal, invo_status', "id= $strItem[2]");
					$invo_data = $this->getNextRecord('Array');

					if ($invo_data['invo_grandtotal'] == $invo_data['invo_payment_value']) $status = STATUS_FINISHED;
					else $status = STATUS_WAITING_FOR_FINISHING;

					$intUpdate = $this->dbUpdate(
						array(
							'invo_status' => $status
						),
						"id = $strItem[2]"
					);

					$i++;
				}
			}

		}	

		return true;	
	}

	public function rollbackAfterDelete($intTransactionID)
	{
		$this->setQuery("SELECT txid_type, txid_ref_id FROM transaction_in_detail WHERE txid_txin_id = $intTransactionID");

		if ($this->getNumRows() > 0) $transDetail = $this->getQueryResult('Array');
		else return false;

		foreach ($transDetail as $detail) {
			$intInvoiceID = $detail['txid_ref_id'];
			$strType = $detail['txid_type'];

			if ($strType == 'PPI') {
					
				$this->initialize('invoice');
				$this->dbUpdate(array(
					'invo_status' => STATUS_WAITING_FOR_FINISHING
				),"id = $intInvoiceID");

				// $this->setQuery("SELECT COALESCE(SUM(invp_umcu_amount_used),0) AS totalDibayar FROM invoice_paid WHERE invp_invo_id = $intInvoiceID AND invp_txin_id != $intTransactionID");
				$this->setQuery("SELECT COALESCE(SUM(txid_amount),0) AS totalDibayar
					FROM transaction_in AS txin
					LEFT JOIN transaction_in_detail AS txid ON txin.id = txid.txid_txin_id
					WHERE txid.txid_type = 'PPI' AND txid.txid_ref_id = $intInvoiceID 
					AND txin.txin_status_trans <> ".STATUS_DELETED."
					");
				if($this->getNumRows() > 0 ) $intTotalDibayar = $this->getNextRecord('Array')['totalDibayar'];
				else $intTotalDibayar = 0;

				$this->dbUpdate(array('invo_payment_value' => $intTotalDibayar),"id = $intInvoiceID");

				$this->initialize('invoice_paid');
				$this->dbUpdate(array('invp_status' => STATUS_DELETED),"invp_txin_id = ".$intTransactionID);
				
				$this->setQuery("
					SELECT invp_invo_id, invp_umcu_id, COALESCE(SUM(invp_umcu_amount_used),0) AS umcu_used FROM invoice_paid 
					WHERE invp_status <> ".STATUS_DELETED." AND invp_txin_id != $intTransactionID AND invp_umcu_id IN 
					(
						SELECT invp_umcu_id FROM invoice_paid WHERE invp_txin_id = $intTransactionID GROUP BY invp_umcu_id
					)
					UNION ALL
					SELECT 0, invp_umcu_id, 0 FROM invoice_paid WHERE invp_txin_id = $intTransactionID GROUP BY invp_umcu_id
				");
				
				if($this->getNumRows() > 0 ) $umcu = $this->getQueryResult('Array');
				else return false;

				foreach ($umcu as $e) {
					$umc_id = $e['invp_umcu_id'];
					if($e['invp_umcu_id'] != 0){
						$amount_umc_used = $e['umcu_used'];						
						break;
					} else $amount_umc_used = 0;					 
				}

				$this->setQuery("UPDATE uang_masuk_customer SET umcu_terpakai = ".$amount_umc_used.", umcu_status = IF (`umcu_terpakai` >= `umcu_amount`, ".STATUS_APPROVED.", ".STATUS_OUTSTANDING.") WHERE id = ".$umc_id);

			}

		}

		return true;

	}

	public function historyUMC($intTransactionID)
	{
		$this->setQuery("SELECT invp_umcu_id, cdate FROM invoice_paid WHERE invp_txin_id = ".$intTransactionID);

		if($this->getNumRows() > 0) $invp = $this->getNextRecord('Array');		
		else return 0;

		if($invp){
			$this->setQuery("SELECT SUM(invp_umcu_amount_used) AS terpakai_sebelumnya
			FROM invoice_paid
			WHERE invp_umcu_id = ".$invp['invp_umcu_id']." AND cdate <= '".$invp['cdate']."' AND invp_status > -1;
			");

			if($this->getNumRows() > 0) return $this->getNextRecord('Array');
			else return 0;
		}
	}

}