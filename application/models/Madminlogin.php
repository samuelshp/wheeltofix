<?php
/*
PUBLIC FUNCTION:
- getUserWarehouse()
- getAllSalesman(strKeyword)
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Madminlogin extends JW_Model {
private $_userID = '';
private $_userName = '';
private $_userPrivilege = '';

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('adm_login');
	$this->_userID = $this->session->userdata('strAdminID');
	$this->_userName = $this->session->userdata('strAdminUserName');
	$this->_userPrivilege = $this->session->userdata('strAdminPriviledge');
}

public function getUserWarehouse() {
	$this->dbSelect('adlg_warehouse_id',"adlg_login = '".$this->_userName."'");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->adlg_warehouse_id;
	else return false;
}

// patricklipesik begin
public function getAllUser($strKeyword = '') {
	$strKeyword = urldecode($strKeyword);
	
	if(!empty($strKeyword))
		$strWhere = " AND (adlg_login LIKE '%$strKeyword%')";
	else
		$strWhere = '';
	
	$this->setQuery(
'	SELECT *
	FROM adm_login
	WHERE adlg_status > 1'.$strWhere.'
	ORDER BY adlg_login ASC');

	if($this->getNumRows() > 0)
		return $this->getQueryResult('Array');
	else
		return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,adlg_login',"id = $intID");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}
// patricklipesik end

}

/* End of File */