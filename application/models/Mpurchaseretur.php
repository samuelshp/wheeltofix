<?php
/*
PUBLIC FUNCTION:
- getAllPOID()
- getSupplierData(intID)
- getDataAutoComplete(txtData)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsBySupplierID(intID,strDate)
- getAccountPayableItems()
- getItemsByProductID(intProductID,strLastStockUpdate)
- getItemsForFinance()
- getUserHistory(intUserID,intPerPage)
- getItemByID(intID)
- getCount()
- getDateMark(strFrom,strTo)
- getSuppliers(strFrom,strTo)
- getStockPurchased(intProductID,strLastStockUpdate)
- searchBySupplier(strSupplierName)
- add(intSupplierID,intWarehouseID,strDescription,intTax,intStatus,strDate,strEndDate,strExpDate,intDisc)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intDiscount,intStatus,intTax,flagEditable)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Mpurchaseretur extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();
	$this->initialize('purchase_retur');
}

public function getAllPOID() {
	$this->setQuery("SELECT id,prcr_supplier_id,cdate FROM purchase_retur WHERE prcr_status = '2' Retur By cdate DESC ");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getSupplierData($intID = 0) {
	$this->setQuery(
"SELECT supp_name,supp_address,supp_city,supp_phone,prcr_tax,prcr_description
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE p.id = $intID");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getDataAutoComplete($strKeyword = '') {
	$strKeyword = urldecode($strKeyword);

	$this->setQuery(
"SELECT p.id,p.cdate,supp_name,supp_address,supp_city,supp_phone,prcr_tax,prcr_description,prcr_supplier_id,prcr_code,prcr_discount
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE (p.prcr_code LIKE '%$strKeyword%' OR supp_name LIKE '%$strKeyword%' OR p.prcr_description LIKE '%$strKeyword%')");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItems($intStartNo = -1,$intPerPage = -1) {
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC, p.id DESC";
	else $strOrderBy = "p.cdate DESC, p.id DESC LIMIT $intStartNo, $intPerPage";
	
$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date,prcr_status, supp_name, supp_address, supp_city, supp_phone,prcr_code,prcr_grandtotal,prcr_tax
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
ORDER BY $strOrderBy");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDate($strDate) {
		$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date, prcr_tax, prcr_status, supp_name, supp_address, supp_city, supp_phone
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate DESC, p.id DESC");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
}

public function getItemsBySupplierID($intID,$strDate) {
	$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date, prcr_tax, prcr_status, supp_name, supp_address, supp_city, supp_phone
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE prcr_supplier_id = $intID AND DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate DESC, p.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAccountPayableItems() {
	$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date, prcr_tax, supp_name, supp_address, supp_city, supp_phone
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE prcr_status IN (0,2)
ORDER BY p.cdate DESC, p.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');

	$this->setQuery(
"SELECT pi.id, supp_name, supp_address, supp_city, p.cdate AS prcr_date, prcr_status, proi_quantity
FROM purchase_retur AS p
INNER JOIN purchase_retur_item AS pi ON proi_purchase_id = p.id
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE prci_product_id = $intProductID  AND p.cdate > '$strLastStockUpdate'");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForFinance() {
	$this->setQuery(
"SELECT p.id,supp_name AS name,p.cdate AS date
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE prcr_status NOT IN (1,3) AND p.id NOT IN (
SELECT fina_transaction_id FROM finance WHERE fina_type = 2
)");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date, prcr_status, supp_name, supp_address, supp_city,prcr_code
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE (p.cby = $intUserID OR p.mby = $intUserID)
ORDER BY p.cdate DESC, p.id DESC LIMIT 0, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID = 0) {
	if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
	else $strWhere = "WHERE p.id = $intID";
$this->setQuery(
"SELECT p.id,prcr_supplier_id AS supp_id, p.cdate AS prcr_date, prcr_tax, prcr_description, prcr_progress, prcr_status, supp_name, supp_address, supp_city, supp_phone,prcr_code,prcr_discount,prcr_editable,ware_name,prcr_warehouse_id,supp_npwp,supp_pkp,supp_tax_address,supp_tax_city,supp_tax_region,supp_tax_zipcode,prcr_grandtotal,prcr_end_date,prcr_exp_date
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON w.id = prcr_warehouse_id
$strWhere");

	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id');
	return $this->getNumRows();
}

public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as prcr_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'prcr_date ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getSuppliers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT prcr_supplier_id,DATE(cdate) AS prcr_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'prcr_supplier_id ASC');

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockPurchased($intProductID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0;

	$this->setQuery(
"SELECT proi_quantity
FROM purchase_retur AS p
INNER JOIN purchase_retur_item AS pi ON proi_purchase_id = p.id
WHERE proi_product_id = $intProductID AND prcr_status IN (2,3) AND p.cdate >= '$strLastStockUpdate'");

	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['proi_quantity'];

	return $intTotalStock;
}

public function searchBySupplier($strSupplierName, $arrSearchDate, $intStatus) {
	$strWhere = '';
	if(!empty($strSupplierName)) {
		$strSupplierName = urldecode($strSupplierName);
		$strWhere .= " AND (supp_name LIKE '%{$strSupplierName}%')";
	}
	if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (p.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (p.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (prcr_status = {$intStatus})";

	$this->setQuery(
"SELECT p.id, p.cdate AS prcr_date,prcr_status, supp_name, supp_address, supp_city, supp_phone,prcr_code,prcr_grandtotal,prcr_tax
FROM purchase_retur AS p
LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
WHERE p.id > 0{$strWhere}
ORDER BY p.cdate DESC, p.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intSupplierID,$intWarehouseID,$strDescription,$intTax,$intStatus,$strDate,$strEndDate,$strExpDate,$intDisc) {
	return $this->dbInsert(array(
		'prcr_supplier_id' => $intSupplierID,
		'prcr_branch_id' => $this->session->userdata('intBranchID'),
		'prcr_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
		'prcr_description' => $strDescription,
		'prcr_discount' => $intDisc,
		'prcr_tax' => $intTax,
		'prcr_status' => $intStatus,
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i'),
		'prcr_end_date'=>formatDate2(str_replace('/','-',$strEndDate),'Y-m-d'),
		'prcr_exp_date' => formatDate2(str_replace('/','-',$strExpDate),'Y-m-d')
	));
}

public function editByID($intID,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
			'prcr_progress' => $strProgress,
			'prcr_status' => $intStatus),
		"id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intDiscount,$intStatus,$intTax,$flagEditable) {
	return $this->dbUpdate(array(
			'prcr_description' => $strDescription,
			'prcr_progress' => $strProgress,
			'prcr_discount' => $intDiscount,
			'prcr_tax' => $intTax,
			'prcr_editable' => $flagEditable,
			'prcr_status' => $intStatus),
		"id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'prcr_status' => $intStatus),"id = $intID");
}

public function deleteByID($intID) {
	$status = $this->dbSelect('prcr_status',"id = $intID");
	
	if($status['prcr_status']='0' || $status['prcr_status']='1'){
		$this->setQuery("DELETE FROM purchase_retur_item WHERE prri_purchase_retur_id = $intID");
		return $this->dbDelete("id = $intID");
		
	} else return false;
}

}

/* End of File */