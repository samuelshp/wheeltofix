<?php
/*
PUBLIC FUNCTION:
- getSJByPurchaseID(intID)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsBySupplierID(intID,strDate)
- getAccountPayableItems()
- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
- getItemsForFinance()
- getUserHistory(intUserID,intPerPage)
- getSJByPOID(intID)
- getItemByID(intID)
- getPrintDataByID(intID)
- getCount()
- getDateMark(strFrom,strTo)
- getSuppliers(strFrom,strTo)
- getStockPurchased(intProductID,intWarehouseID,strLastStockUpdate)
- searchBySupplier(strSupplierName)
- add(intPO,intAcceptance,intSuppID,intWarehouseID,strEkspedition,strDescription,intTax,intDiscount,strSuratJalan,intStatus,strDate)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intStatus,strSuratJalan,strEkspedition,intDiscount,intTax,flagEditable)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

PRIVATE FUNCTION:
- 	
*/

class Mpurchase extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('purchase');
}
//new
public function Purchase_Order_GetSJByPOID($intPOID = 0) {

    $this->setQuery(
"SELECT p.id, p.cdate,prch_code
FROM purchase AS p
WHERE prch_po_id = $intPOID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}


public function updateQtyPB($jumlah, $nomor_po){
    $_CI =& get_instance();
    $_CI->load->model('Mpurchaseitem');
    $_CI->Mpurchaseitem->dbUpdate(array(
            'prci_quantity1' => $jumlah),
            "id = $nomor_po");
    // $this->setQuery(
    //     "UPDATE purchase_item SET prci_quantity1 = $jumlah
    //     WHERE id = $nomor_po"
    // );
    // return $this->dbUpdate(array(
	// 	'prci_quantity1' => $jumlah),
	// 	"id = $nomor_po");
    return true;
}

public function getLastPurchaseCode($code) {
	$this->setQuery(
    "SELECT SUBSTRING(prch_code,10,4) as code
    FROM purchase
    WHERE prch_code like '$code%'
    ORDER BY prch_code DESC");

    if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
    else return false;
}

public function getAllPurchaseItemBySupplier($idSupp=''){
    // $idSupp2 = $idSupp.substring(0, $idSupp.indexOf(','));
    // $idSupp3 = $idSupp.substring($idSupp.indexOf(',')+1, strlen($idSupp));
    $this->setQuery(
        "SELECT p.id, pi.id as purchase_id, jp.id as product_id, p.prch_code, jc.cust_name, k.owner_id, k.kont_name, sk.job as kont_job, p.cdate, po.idKontrak, pi.prci_description, pi.prci_quantity1, jp.prod_title, pi.prci_arrived1,	pi.prci_unit1,	pi.prci_price,	pi.cby
        FROM purchase as p
        LEFT JOIN purchase_item as pi ON pi.prci_purchase_id = p.id
        LEFT JOIN jw_product as jp ON jp.id = pi.prci_product_id
        LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id AND sk.kont_code = k.kont_code
        LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
        WHERE p.prch_code IN ('$idSupp')"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchaseItemBySupplier2($nama_supp=''){
    $this->setQuery(
        "SELECT ai.id, ai.acit_description, p.prch_code, js.supp_name, sk.job as kont_job, p.cdate, pi.prci_quantity1, jp.prod_title, ai.acit_arrived1, po.idKontrak, jp.id as product_id_baru, p.id as purchase_id_baru, pi.id as purchase_item_id
        FROM acceptance_item as ai
        LEFT JOIN jw_product as jp ON jp.id = ai.acit_product_id
        LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
        LEFT JOIN purchase_item as pi ON pi.prci_product_id = ai.acit_product_id
        LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id AND sk.kont_code = k.kont_code
        LEFT JOIN jw_supplier as js ON js.id = p.prch_supplier_id
        WHERE js.supp_name LIKE '%$nama_supp%' "
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

//sampe sini
public function getDataAutoComplete($strKeyword = '') {
    $strKeyword = urldecode($strKeyword);
    $this->setQuery(
"SELECT p.id,p.cdate,supp_name,supp_address,supp_city,supp_phone,prch_tax,prch_description,prch_supplier_id,prch_code,prch_discount,prch_warehouse_id,ware_name
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
WHERE (p.id IN ($strKeyWord) p.prch_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.")");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getPurchaseBySupplier($idSupp = ''){
    $this->setQuery(
        "SELECT prch_code, id, prch_po_id 
        FROM purchase
        WHERE prch_supplier_id = $idSupp"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getSJByPOID($intPOID = 0) {
	
	$this->setQuery(
"SELECT p.id, p.cdate,prch_code
FROM purchase AS p
WHERE prch_po_id = $intPOID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getUnpaidPurchaseByText($strKeyword = '') {
    if(!empty($strKeyword)) {
        $strWhere1 = " AND (prch_code LIKE '%$strKeyword%' OR supp_name LIKE '%$strKeyword%')";
        $strWhere2 = " AND (prcr_code LIKE '%$strKeyword%' OR supp_name LIKE '%$strKeyword%')";
    } else{
        $strWhere1 = '';
        $strWhere2 = '';
    }

    $this->setQuery(
"    SELECT id,prch_date,prch_code,supp_name,supp_address,supp_city,supp_phone,prch_grandtotal,supp_id,tipe
      FROM
      (
          (
            SELECT p.id,prch_code, supp_name, supp_address, supp_city, supp_phone, prch_grandtotal,p.cdate AS prch_date,s.id AS supp_id,'1' AS tipe
            FROM purchase AS p
            LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
            WHERE prch_status IN (".STATUS_APPROVED.") $strWhere1
          )
          UNION
          (
            SELECT pr.id,prcr_code AS prch_code, supp_name, supp_address, supp_city, supp_phone, prcr_grandtotal AS prch_grandtotal,pr.cdate AS prch_date,s.id AS supp_id,'2' AS tipe
            FROM purchase_retur AS pr
            LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
            WHERE prcr_status IN (".STATUS_APPROVED.") $strWhere2
          )
      ) AS t
    ORDER BY prch_code DESC
");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function checkBPB($idPurchase){
    $this->setQuery(
        "SELECT COUNT(id) as jumlah_data from acceptance_item WHERE acit_purchaseid = $idPurchase"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItems($intStartNo = -1,$intPerPage = -1) {
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC, p.id DESC";
	else $strOrderBy = "p.cdate DESC, p.id DESC LIMIT $intStartNo, $intPerPage";

    $whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";
	
	$this->setQuery(
"SELECT p.id, p.cdate AS prch_date, prch_grandtotal, prch_status, supp_name, supp_address, supp_city, supp_phone, ware_name,prch_code,prch_tax,prch_description, p.approvalPM, po.pror_code, k.kont_name, sk.job
FROM purchase AS p
LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
LEFT JOIN kontrak as k ON k.id = po.idKontrak
LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
WHERE prch_status >= ".STATUS_APPROVED." $whereInProject
ORDER BY $strOrderBy");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDate($strDate) {
	$this->setQuery(
"SELECT p.id, p.cdate AS prch_date, prch_tax, prch_status, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
WHERE DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsBySupplierID($intID,$strDate) {
	$this->setQuery(
"SELECT p.id, p.cdate AS prch_date, prch_tax, prch_status, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
WHERE prch_supplier_id = $intID AND DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForRepayment($intStartNo,$intPerPage) {

    $this->setQuery(
'    SELECT id, prch_date,supp_id,prch_code, supp_name, supp_address, supp_city, supp_phone, prch_grandtotal,tipe
    FROM(
        (
            SELECT p.id, p.cdate AS prch_date,s.id AS supp_id,prch_code, supp_name, supp_address, supp_city, supp_phone, prch_grandtotal,"1" AS tipe
            FROM purchase AS p
            LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
            WHERE prch_status IN ('.STATUS_APPROVED.')
        )
        UNION
        (
            SELECT pr.id, pr.cdate AS prch_date,s.id AS supp_id,prcr_code AS prch_code, supp_name, supp_address, supp_city, supp_phone,prcr_grandtotal AS prch_grandtotal,"2" AS tipe
            FROM purchase_retur AS pr
            LEFT JOIN jw_supplier AS s ON prcr_supplier_id = s.id
            WHERE prcr_status IN ('.STATUS_APPROVED.')
        )
    ) AS t
    ORDER BY prch_date DESC, id DESC LIMIT '.$intStartNo.', '.$intPerPage.'');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAccountPayableItems() {
	$this->setQuery(
"SELECT p.id, p.cdate AS prch_date, prch_tax, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
WHERE prch_status IN (".STATUS_OUTSTANDING.",".STATUS_APPROVED.")
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
	$this->setQuery(
"SELECT pi.id, supp_name, supp_address, supp_city, p.cdate AS prch_date, prch_status, prci_quantity
FROM purchase AS p
INNER JOIN purchase_item AS pi ON prci_purchase_id = p.id
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
WHERE prci_product_id = $intProductID AND prch_warehouse_id = $intWarehouseID AND p.cdate > '$strLastStockUpdate'");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForFinance() {
	$this->setQuery(
"SELECT p.id,supp_name AS name,p.cdate AS date
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
WHERE prch_status NOT IN (".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.") AND p.id NOT IN (
	SELECT fina_transaction_id FROM finance WHERE fina_type = 2
)");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT p.id, p.cdate AS prch_date, prch_status, supp_name, supp_address, supp_city,prch_code
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
WHERE (p.cby = $intUserID OR p.mby = $intUserID)
ORDER BY p.cdate DESC, p.id DESC LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}




public function getItemByID($intID = 0) {
	if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
	else $strWhere = "WHERE p.id = $intID";
	
	$this->setQuery(
"SELECT p.id, po.id as purchase_order_id, p.cdate AS prch_date,prch_exp_date,prch_jatuhtempo, prch_tax, prch_description, prch_progress, prch_status,prch_po_id,prch_supplier_id,supp_name,supp_cp_name, supp_cp_phone, supp_phone,supp_address,prch_code,pror_code,prch_discount,pror_supplier_id,prch_kontrak_pembelian_id,kopb_code,prch_grandtotal,prch_subtotal,prch_pph, k.kont_name, p.approvalPM,p.aby_pm,p.adate_pm, p.cby, p.mby, p.cdate, p.mdate
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN purchase_order AS po ON prch_po_id = po.id
LEFT JOIN kontrak AS k ON po.idKontrak = k.id
LEFT JOIN kontrak_pembelian AS kp ON p.prch_kontrak_pembelian_id = kp.id
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getPrintDataByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
    else $strWhere = "WHERE p.id = $intID";
	
	$this->setQuery(
"SELECT pror_exp_date,ware_name,acce_ekspedition_number, a.cdate AS acce_date,supp_npwp,supp_pkp
FROM purchase AS p
LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
LEFT JOIN purchase_order AS po ON prch_po_id = po.id
LEFT JOIN acceptance AS a ON prch_acceptance_id = a.id
LEFT JOIN jw_warehouse AS w ON a.acce_warehouse = w.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getCount() {
    $whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";
    
    $this->setQuery(
        "SELECT p.id, p.cdate AS prch_date, prch_grandtotal, prch_status, supp_name, supp_address, supp_city, supp_phone, ware_name,prch_code,prch_tax,prch_description, p.approvalPM, po.pror_code, k.kont_name, sk.job
        FROM purchase AS p
        LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
        LEFT JOIN kontrak as k ON k.id = po.idKontrak
        LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
        LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
        LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
        WHERE prch_status >= ".STATUS_APPROVED." $whereInProject");
	return $this->getNumRows();
}

public function getCount2() {

    $this->setQuery(
"    SELECT id
    FROM(
        (
            SELECT p.id
            FROM purchase AS p
            WHERE prch_status IN (".STATUS_APPROVED.") AND p.id NOT IN
              (
                SELECT repa_purchase_id
                FROM repayment
              )
        )
        UNION
        (
            SELECT p.id
            FROM purchase_retur AS p
            WHERE prcr_status IN (".STATUS_APPROVED.")
        )
    )AS t
    ORDER BY id DESC");

    return $this->getNumRows();
}

public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as prch_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'prch_date ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getSuppliers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT prch_supplier_id,DATE(cdate) AS prch_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'prch_supplier_id ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockPurchased($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0; 
	
	$this->setQuery(
"SELECT prci_quantity
FROM purchase AS p
INNER JOIN purchase_item AS pi ON prci_purchase_id = p.id
WHERE prci_product_id = $intProductID AND prch_warehouse_id = $intWarehouseID AND prch_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.") AND p.cdate >= '$strLastStockUpdate'");
	
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['prci_quantity'];
	
	return $intTotalStock;
}

public function searchBySupplier($arrSearchProyek, $strNoOp, $strSupplierName) {

	$strWhere = '';
	if(!empty($strSupplierName)) {
		$strSupplierName = urldecode($strSupplierName);
		$strWhere .= " AND s.supp_name LIKE '%$strSupplierName%'";
	}
	if(!empty($arrSearchProyek)) $strWhere .= " AND k.kont_name LIKE '%$arrSearchProyek%'";
    if(!empty($strNoOp)) $strWhere .= " AND p.prch_code LIKE '%$strNoOp%'";

    $whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
    "SELECT p.id, p.cdate AS prch_date, prch_grandtotal, prch_status, supp_name, supp_address, supp_city, supp_phone, ware_name,prch_code,prch_tax,prch_description, p.approvalPM, po.pror_code, k.kont_name, sk.job
    FROM purchase AS p
    LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
    LEFT JOIN kontrak as k ON k.id = po.idKontrak
    LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
    LEFT JOIN jw_supplier AS s ON prch_supplier_id = s.id
    LEFT JOIN jw_warehouse AS w ON prch_warehouse_id = w.id
    WHERE p.id > 0 AND prch_status >= 2$strWhere $whereInProject
    ORDER BY p.cdate ASC, p.id ASC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($strPurchaseCode,$strDate,$strDescription,$intPBID,$intSupplierID,$intKPID,$intSubTotal,$intDiscount,$intTax,$intCostAdd,$intGrandTotal,$intPPH,$idsubkon) {
    return $this->dbInsert(array(
        'prch_po_id' => $intPBID,
        'prch_code' => $strPurchaseCode,
        'prch_supplier_id' => $intSupplierID,
        'prch_branch_id' => 0,
        'prch_warehouse_id' => 0,
        'prch_kontrak_pembelian_id' => $intKPID,
        'prch_tax' => $intTax,
        'prch_discount' => $intDiscount,
        'prch_subtotal' => $intSubTotal,
        'prch_costadd' => $intCostAdd,
        'prch_grandtotal' => $intGrandTotal,
        'prch_pph' => $intPPH,
        'prch_description' => $strDescription,
        'prch_status' => 3,
        'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i'),
        'idSubKontrak' => $idsubkon
    ));
}

public function update($intPurchaseID,$strDate,$strDescription,$intSubTotal,$intDiscount,$intTax,$intCostAdd,$intGrandTotal,$intPPH) {
	return $this->dbUpdate(array(
        'prch_tax' => $intTax,
        'prch_discount' => $intDiscount,
        'prch_subtotal' => $intSubTotal,
        'prch_costadd' => $intCostAdd,
        'prch_grandtotal' => $intGrandTotal,
        'prch_pph' => $intPPH,
        'prch_description' => $strDescription,
	),"id = $intPurchaseID");
}

public function updateApproval($intID,$approve,$by) {
    return $this->dbUpdate(array(
        'approvalPM' => $approve,
        'aby_pm' => $by,
        'adate_pm' => date('Y-m-d H:i:s')),
        "id = $intID");
}

public function editByID($intID,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
		'prch_progress' => $strProgress,
		'prch_status' => $intStatus),
		"id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intStatus,$intDiscount,$intTax) {
	return $this->dbUpdate(array(
		'prch_description' => $strDescription,
		'prch_progress' => $strProgress,
		'prch_status' => $intStatus,
        'prch_discount' => $intDiscount,
        'prch_tax' => $intTax),
		"id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'prch_status' => $intStatus),"id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

public function getUserByJabatan($intID,$jabatan){
    $this->setQuery(
        "SELECT adlg_name, l.id, k.kont_name, sk.job
        FROM purchase p
        INNER JOIN purchase_order po ON po.id = p.prch_po_id
        INNER JOIN subkontrak sk ON sk.id = po.idSubKontrak
        INNER JOIN kontrak k ON k.id = sk.kontrak_id
        INNER JOIN subkontrak_team skt ON sk.id = skt.subkontrak_id
        INNER JOIN adm_login l ON l.id = skt.name
        WHERE jabatan_id = $jabatan AND skt.status = 2 AND po.pror_status >= ".STATUS_APPROVED." AND p.id = $intID
        group by adlg_name");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function close($intPurchaseID, $strUsername)
{    
    return $this->dbUpdate(array(
        'prch_progress' => "Closed oleh $strUsername",
        'prch_status' => STATUS_FINISHED
    ),"id=$intPurchaseID");
}

}

/* End of File */