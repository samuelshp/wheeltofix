<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ematerialunit extends Eloquent 
{
    protected $table = 'jw_material_unit';
    public $timestamps = false;

    public function material_units()
    {
        return $this->belongsTo(Ebahan::class);
    }
}