<?php
class Mdaftarpembayaranhutang extends JW_Model {

	// Constructor
	public function __construct() {
		parent::__construct();
		$this->initialize('daftar_pembayaran_hutang');
	}

	public function getAllData($intStartNo = -1, $intPerPage = -1)
	{
		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "dphu_date DESC";
		else $strOrderBy = "dphu_date DESC, id DESC LIMIT $intStartNo, $intPerPage";

		$this->setQuery("
			SELECT tbUnion.id, dphu_code, dphu_date, dphu_amount, supp_name, dphu_status FROM
				(
				(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, pi.pinv_supplier_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN purchase_invoice AS pi ON pi.id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'PI')
			UNION ALL
			(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, dp.dpay_supplier_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN downpayment AS dp ON dp.id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'DP')
			UNION ALL
			(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, txod.txod_supp_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN transaction_out_detail AS txod ON txod.txod_txou_id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'NKS' OR  dph_item.dphi_reff_type = 'NDS')  
				) AS tbUnion	
			LEFT JOIN jw_supplier AS supp ON supp.id = tbUnion.supp_id
			WHERE dphu_status != ".STATUS_DELETED."
			GROUP BY dphu_code
			ORDER BY $strOrderBy
		");
				
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getCount()
	{
		$this->dbSelect();
		return $this->getNumRows();
	}

	public function getDataByID($id)
	{
		$this->setQuery("SELECT *
            FROM (
                (select dph.id as id, dphu_code , dphu_date, dphu_amount, supp_name, dphu_status, dph.cby from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join downpayment as dp on dph_item.dphi_reff_id = dp.id
                left join jw_supplier as supp on dp.dpay_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'DP')
                UNION ALL
                (select dph.id as id, dphu_code, dphu_date, dphu_amount, supp_name, dphu_status, dph.cby from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join purchase_invoice as pi on dph_item.dphi_reff_id = pi.id
                left join jw_supplier as supp on pi.pinv_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'PI')
                UNION ALL
			    (select dph.id as id, dphu_code, dphu_date, dphu_amount, supp_name, dphu_status, dph.cby from daftar_pembayaran_hutang as dph 
			    left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
			    left join transaction_out_detail as txod on txod.txod_txou_id = dph_item.dphi_reff_id
			    left join jw_supplier as supp on txod.txod_supp_id = supp.id
			    where dph_item.dphi_reff_type = 'NKS')
                ) tblUnion
            WHERE id = '$id'
            ");
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}

	public function getDataByCode($strCode)
	{
		$this->dbSelect('id',"dphu_code = '$strCode'");
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}

	public function getPurchaseInvoiceData($intSuppID)
	{		
		$this->setQuery("
			SELECT pinv.id, pinv_code, supp_name, pinv_subtotal, pinv_discount, pinv_costadd, pinv_finaltotal, pinv_grandtotal, pinv_tax, pinv_pph, pinv_pembulatan, pinv_terbayar, pinv_sisa_bayar, pinv_totalbeforepph, acc.cdate FROM purchase_invoice AS pinv
			LEFT JOIN jw_supplier AS sup ON pinv_supplier_id = sup.id
			LEFT JOIN acceptance AS acc ON pinv_acceptance_id = acc.id
			WHERE pinv.pinv_supplier_id = '$intSuppID' 
			AND ((pinv.pinv_finaltotal - pinv.pinv_terbayar) > 0 AND (pinv_status != ".STATUS_DELETED."))
		");
		//AND pinv_status IN (2,3)
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getDebtData($intSuppID)
	{
		$this->setQuery("
			SELECT txou.id, txou_code, supp_name, txod_amount, txod_pph, txod_ppn, txou_date, txod_terbayar, txod_sisa_bayar FROM transaction_out AS txou
			LEFT JOIN transaction_out_detail AS txod ON txou.id = txod.txod_txou_id
			LEFT JOIN jw_supplier AS sup ON txod.txod_supp_id = sup.id			
			WHERE txod_type = 'NKS' AND txod.txod_supp_id = '$intSuppID' AND ( txou_status_trans != ".STATUS_DELETED." AND txou_status_trans < ".STATUS_WAITING_FOR_FINISHING.")
		");
		//AND pinv_status IN (2,3)
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getDownpaymentData($intSuppID)
	{
		$this->setQuery("
			SELECT dp.id, dpay_code, supp_name, dpay_amount, dpay_terbayar, dpay_sisa_bayar, dp.dpay_date, dpay_amount_final  FROM downpayment AS dp
			LEFT JOIN jw_supplier AS sup ON dpay_supplier_id = sup.id			
			WHERE dp.dpay_supplier_id = '$intSuppID' AND (dp.dpay_terbayar < dp.dpay_amount_final)
		");
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function getProductList($pi_id)
	{
		$this->setQuery("
		SELECT prod_title FROM purchase_invoice_item AS pi_item
		LEFT JOIN jw_product as prod ON pi_item.pinvit_product_id = prod.id
		WHERE pi_item.pinvit_purchase_invoice_id = '$pi_id'
		");

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	public function addDPH($arrDPH)
	{
		$arrDPH['dphu_code'] = generateTransactionCode($arrDPH['dphu_date'],'','daftar_pembayaran_hutang',TRUE);				
		return $this->dbInsert(array(		
			'dphu_code' => $arrDPH['dphu_code'],
			'dphu_date' => str_replace("/", "-", $arrDPH['dphu_date']),
			'dphu_amount' => str_replace(",", ".", str_replace(".","",$arrDPH['dphu_amount']))
		));
	}


	public function updateKodeDPP($kode_dpp, $id_dpp)
	{
		return $this->dbUpdate(array(
			'kode' => $kode_pb),
		"id = $id_dpp");
	}

	public function updateTglBuat($strDate, $intID_DPH)
	{
		return $this->dbUpdate(array(
			'dphu_date' => $strDate),
		"id = '$intID_DPH'");
	}

	public function updateAmount($intAmount, $intID_DPH)
	{
		return $this->dbUpdate(array(
			'dphu_amount' => $intAmount),
		"id = '$intID_DPH'");
	}

	public function updateTrxOutID($txod_ref_id, $intStatus = 2)
	{				
		return $this->dbUpdate(array('dphu_status' => $intStatus), "id = '$txod_ref_id'");
	}

	public function delete($intID_DPH)
	{		
		$intDelete = $this->dbUpdate(
			array(
				'dphu_status' => STATUS_DELETED
			), "id='$intID_DPH'"
		);
		$this->rollbackAfterDelete($intID_DPH);
		return $intDelete;
	}

	public function searchBy($strDate = '', $strDate2 = '', $strCode = '', $strOther = '', $intStartNo = -1, $intPerPage = -1)
	{
		$strWhere = '';
		if ($strDate != '' && $strDate2 == '') $strWhere .= " AND dphu_date = '$strDate'";
		if ($strDate2 != '') $strWhere .= " AND (dphu_date BETWEEN '$strDate' AND '$strDate2')";
		if ($strCode != '') $strWhere .= " AND dphu_code LIKE '%$strCode%'";
		if ($strOther != '') $strWhere .= " AND supp_name LIKE '%$strOther%'";

		if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "dphu_date DESC";
		else $strOrderBy = "dphu_date DESC, id DESC LIMIT $intStartNo, $intPerPage";

		$this->setQuery("
			SELECT tbUnion.id, dphu_code, dphu_date, dphu_amount, supp_name, dphu_status FROM
				(
				(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, pi.pinv_supplier_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN purchase_invoice AS pi ON pi.id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'PI')
			UNION ALL
			(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, dp.dpay_supplier_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN downpayment AS dp ON dp.id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'DP')
			UNION ALL
			(SELECT dph.id as id, dphu_code, dphu_date, dphu_amount, txod.txod_supp_id as supp_id, dphu_status FROM daftar_pembayaran_hutang AS dph
			LEFT JOIN daftar_pembayaran_hutang_item AS dph_item ON dph.id = dph_item.dphi_dphu_id
			LEFT JOIN transaction_out_detail AS txod ON txod.txod_txou_id = dph_item.dphi_reff_id
			WHERE dph_item.dphi_reff_type = 'NKS' OR  dph_item.dphi_reff_type = 'NDS')  
				) AS tbUnion	
			LEFT JOIN jw_supplier AS supp ON supp.id = tbUnion.supp_id
			WHERE dphu_status != ".STATUS_DELETED." $strWhere
			GROUP BY dphu_code
			ORDER BY $strOrderBy
		");
				
		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}

	
	public function checkPiInDph($idPi){
		$this->dbSelect('id',"dphu_code = '$strCode' AND dphu_status > ".STATUS_DELETED);
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}

	public function rollbackAfterDelete($intDPHid)
	{
		$this->setQuery("SELECT dphi_reff_type, dphi_reff_id FROM daftar_pembayaran_hutang_item WHERE dphi_dphu_id = '$intDPHid'");

		if($this->getNumRows() > 0 ) $result = $this->getQueryResult('Array');
		else $result = false;		

		foreach ($result as $value){
		
			$type = $value['dphi_reff_type'];
			$id = $value['dphi_reff_id'];

			switch ($type){
				case 'DP':
					$strTableName = "downpayment";
					$strFieldName = "dpay_status";
					$intRollbackStatus = STATUS_APPROVED;
					break;			
				case 'PI':
					$strTableName = "purchase_invoice";
					$strFieldName = "pinv_status";
					$intRollbackStatus = STATUS_APPROVED;
					break;
				case 'NKS':
					$strTableName = "transaction_out";
					$strFieldName = "txou_status_trans";
					$intRollbackStatus = STATUS_APPROVED;
					break;
			}
			
			$result = $this->setQuery("UPDATE `$strTableName` SET `$strFieldName` = $intRollbackStatus WHERE `id` = '$id'");
		}

		return $result;
		
	}

}