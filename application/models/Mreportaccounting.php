<?php
 
class Mreportaccounting extends JW_Model {
 
public $_intWarehouseID;
 
public function __construct() {
    parent::__construct();  
    $this->_intWarehouseID = 1;
}
 
// patricklipesik begin
public function searchAkuntansiProfit($txtStart,$txtEnd) {
    $branchID = $this->session->userdata('intBranchID');
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d').' 00:00:00';
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d').' 23:59:59';
     
    $this->setQuery(
"   SELECT b.prod_title AS produk, SUM(
        (trhi_quantity1*trhi_price) +
        (trhi_quantity2*trhi_price*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_price*(trhi_conv3/trhi_conv1))
    ) AS subtotal, SUM(
        (trhi_quantity1*trhi_hpp) +
        (trhi_quantity2*trhi_hpp*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_hpp*(trhi_conv3/trhi_conv1))
    ) AS hpp
    FROM transaction_history a
    JOIN jw_product b ON a.trhi_product_id = b.id
    WHERE a.trhi_type = 'invoice'
    AND a.trhi_status IN (2,3)
    AND a.trhi_branch_id = '".$branchID."'
    AND a.trhi_date >= '".$txtStart."'
    AND a.trhi_date <= '".$txtEnd."'
    GROUP BY a.trhi_product_id
    ORDER BY b.prod_title ASC");
     
    if($this->getNumRows() > 0)
        return $this->getQueryResult('Array');
    else
        return false;
}
public function getCountAkuntansiProfit() {
    $branchID = $this->session->userdata('intBranchID');
    $txtStart = date('Y-m-d').' 00:00:00';
    $txtEnd = date('Y-m-d').' 23:59:59';
     
    $this->setQuery(
"   SELECT b.prod_title AS produk, SUM(
        (trhi_quantity1*trhi_price) +
        (trhi_quantity2*trhi_price*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_price*(trhi_conv3/trhi_conv1))
    ) AS subtotal, SUM(
        (trhi_quantity1*trhi_hpp) +
        (trhi_quantity2*trhi_hpp*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_hpp*(trhi_conv3/trhi_conv1))
    ) AS hpp
    FROM transaction_history a
    JOIN jw_product b ON a.trhi_product_id = b.id
    WHERE a.trhi_type = 'invoice'
    AND a.trhi_status IN (2,3)
    AND a.trhi_branch_id = '".$branchID."'
    AND a.trhi_date >= '".$txtStart."'
    AND a.trhi_date <= '".$txtEnd."'
    GROUP BY a.trhi_product_id");
     
    return $this->getNumRows();
}
public function getItemsAkuntansiProfit($intStartNo = -1,$intPerPage = -1) {
    $branchID = $this->session->userdata('intBranchID');
    $txtStart = date('Y-m-d').' 00:00:00';
    $txtEnd = date('Y-m-d').' 23:59:59';
     
    if($intStartNo == -1 || $intPerPage == -1)
        $strLimit = "";
    else
        $strLimit = " LIMIT ".$intStartNo.", ".$intPerPage;
     
    $this->setQuery(
"   SELECT b.prod_title AS produk, SUM(
        (trhi_quantity1*trhi_price) +
        (trhi_quantity2*trhi_price*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_price*(trhi_conv3/trhi_conv1))
    ) AS subtotal, SUM(
        (trhi_quantity1*trhi_hpp) +
        (trhi_quantity2*trhi_hpp*(trhi_conv2*trhi_conv3/trhi_conv1)) +
        (trhi_quantity3*trhi_hpp*(trhi_conv3/trhi_conv1))
    ) AS hpp
    FROM transaction_history a
    JOIN jw_product b ON a.trhi_product_id = b.id
    WHERE a.trhi_type = 'invoice'
    AND a.trhi_status IN (2,3)
    AND a.trhi_branch_id = '".$branchID."'
    AND a.trhi_date >= '".$txtStart."'
    AND a.trhi_date <= '".$txtEnd."'
    GROUP BY a.trhi_product_id
    ORDER BY b.prod_title ASC
    ".$strLimit);
     
    if($this->getNumRows() > 0)
        return $this->getQueryResult('Array');
    else
        return false;
}
 
public function searchAkuntansiGeneralLedger($accountCode,$txtStart,$txtEnd,$intTax) {
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
    $sql = 'CALL func_report_general_ledger ("'.$accountCode.'","'.$accountCode.'","'.$txtStart.'","'.$txtEnd.'",'.$intTax.');';
    $query = $this->db->query($sql);
    if($this->db->trans_status() === FALSE){
        // $query->next_result();
        $this->next_result($query);
        $query->free_result();
    } else {
        $result = $query->result_array();
        // $query->next_result();
        $this->next_result($query);
        return $result;
    }
    return false;
}
 
public function searchAkuntansiLabaRugi($txtPeriode,$intTax) {
    $branchID = $this->session->userdata('intBranchID');
    $sql = 'CALL func_report_laba_rugi ("'.$txtPeriode.'",'.$intTax.','.$branchID.');';
    $query = $this->db->query($sql);
    if($this->db->trans_status() === FALSE){
        // $query->next_result();
        $this->next_result($query);
        $query->free_result();
    } else {
        $result = $query->result_array();
        // $query->next_result();
        $this->next_result($query);
        return $result;
    }
    return false;
}
 
public function searchAkuntansiNeraca($txtPeriode,$intTax) {
    $branchID = $this->session->userdata('intBranchID');
    $sql = 'CALL func_report_neraca ("'.$txtPeriode.'",'.$intTax.','.$branchID.');';
    $query = $this->db->query($sql);
    if($this->db->trans_status() === FALSE){
        // $query->next_result();
        $this->next_result($query);
        $query->free_result();
    } else {
        $result = $query->result_array();
        // $query->next_result();
        $this->next_result($query);
        return $result;
    }
    return false;
}
// patricklipesik end
 
}