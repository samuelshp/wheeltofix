<?php

class Mtinyfiledbvo extends CI_Model {

private $_arrTinyDBData;
private $_strFileName;

# CONSTRUCTOR
public function __construct() { 
	$_arrTinyDBData = array();
	$_strFileName = '';
}

public function initialize($strFileName,$strPath = '') {
	$this->_arrTinyDBData = array();
	if($strPath == '') $strFileName = "tinydb/".$strFileName.".txt";
	else $strFileName = $strPath.'/'.$strFileName.".txt";
	
	$this->_strFileName = $strFileName;
	$resFileHandle = fopen($strFileName, "r");
	if($resFileHandle == TRUE) {
		# Retrieving the data in textfile one by one
		while(!feof($resFileHandle)) {
			$arrTinyDBDataDump = fscanf($resFileHandle,"%s %s\n");
			list($strKey,$strData) = $arrTinyDBDataDump;
			# Replace some signs (don't change the order!!!)
			$strData = str_replace("\_","*^*",$strData);
			$strData = str_replace("_"," ",$strData);
			//$strData = str_replace("*^*","\_",$strData);
			//$strData = str_replace("\_","_",$strData);
			$strData = str_replace("*^*","_",$strData);
			$this->_arrTinyDBData[] = array('strKey' => $strKey,'strData' => $strData);
		}
		$this->setData($this->_arrTinyDBData);
		fclose($resFileHandle);
		return TRUE;
	} else return FALSE;
}

# GETTER

# Get all data in the array data
public function getAllData() { return $this->_arrTinyDBData; }

# Get single data in the array data by key
public function getSingleData($strKey) {
	foreach($this->getAllData() as $arrTinyDBDataEach)
		if($arrTinyDBDataEach['strKey'] == $strKey)
			return $arrTinyDBDataEach['strData'];
	return FALSE;
}

# SETTER

# Set the data variable
public function setData($arrTinyDBData) { $this->_arrTinyDBData = $arrTinyDBData; }

# Save the data into the textfile database
public function saveData($strFileName = '') {
	if($strFileName == '')$strFileName = $this->_strFileName;
	$strFileName = str_replace(site_url(),'',$strFileName);
	$resFileHandle = fopen($strFileName, "w");
	if($resFileHandle == TRUE) {
		foreach($this->getAllData() as $dataEach) {
			# Replace some signs (don't change the order!!!)
			$dataEach['strData'] = str_replace("_","*^*",$dataEach['strData']);
			//$dataEach['strData'] = str_replace("_","\_",$dataEach['strData']);
			$dataEach['strData'] = str_replace("\_","*^*",$dataEach['strData']);
			$dataEach['strData'] = str_replace(" ","_",$dataEach['strData']);
			$dataEach['strData'] = str_replace("*^*","\_",$dataEach['strData']);
			//fwrite($resFileHandle,$dataEach['strKey'].' '.$dataEach['strData']);
			fprintf($resFileHandle,"%s %s\r\n",$dataEach['strKey'],$dataEach['strData']);
		}
		fclose($resFileHandle);
		return TRUE;
	} else return FALSE;
}

# Set single data in the array data by key
public function setSingleData($strKey,$strData) {
	$intDataCount = count($this->_arrTinyDBData);
	for($i = 0; $i < $intDataCount; $i++)
		if($this->_arrTinyDBData[$i]['strKey'] == $strKey) {
			$this->_arrTinyDBData[$i]['strData'] = $strData;
		}
	return FALSE;
}


}

/* End of File */