<?php

class Madmlinklist extends CI_Model {
# CONSTRUCTOR
public function __construct() { parent::__construct(); }

# LIST
public function get($intPriviledgeID = 0) { # Retrieve all data
	if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Madmlinklist_get_'.$intPriviledgeID)) return $mixResult;
	$arrLinkList = array();

	if($intPriviledgeID > 0)
		$mixQueryResult = $this->db->query(
"SELECT DISTINCT ll.id,link_parent,name,link_icon,link_type,link_description,link_place,instruction
FROM adm_link_list AS ll,adm_link_priviledge AS lp 
WHERE (
	(ll.link_parent = 0 AND (ll.link_type != 'link' OR ll.link_type IS NULL)) OR 
	(ll.id = lp.adlk_link_id AND lp.adlk_login_id = $intPriviledgeID AND lp.adlk_allow_view > 0 AND ll.link_type != 'link')
) AND ll.link_status > 0
ORDER BY link_parent ASC, link_position ASC");

	else $mixQueryResult = $this->db->query(
"SELECT id,link_parent,name,link_icon,link_type,link_description 
FROM adm_link_list 
WHERE link_status > 0 AND link_type != 'link' 
ORDER BY id");

	# Prevent some link from appearing
	$arrResult = $mixQueryResult->result_array();
	for($i = 0; $i < count($arrResult); $i++)
		if($arrResult[$i]['link_type'] == 'link' && $arrResult[$i]['link_description'] == '') {
			unset($arrResult[$i]);
		}

	$this->cache->{JW_CACHE_TYPE}->save('Madmlinklist_get_'.$intPriviledgeID, $arrResult, JW_CACHE_EXP_1_DAY);
	return $arrResult;
}

public function getPriviledgeLinkList() { # Retrieve some data for adding priviledge purpose
	if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Madmlinklist_getPriviledgeLinkList')) return $mixResult;

	$mixQueryResult = $this->db->query(
"SELECT ll.id,ll.name,pll.name AS parent_name,ll.link_type
FROM adm_link_list AS ll, adm_link_list AS pll 
WHERE ll.link_status > 0 AND ll.link_parent != 0 AND ll.link_type != 'link' AND ll.link_parent = pll.id
ORDER BY ll.link_parent ASC, ll.id ASC");
	$arrResult = $mixQueryResult->result_array();

	$this->cache->{JW_CACHE_TYPE}->save('Madmlinklist_getPriviledgeLinkList', $arrResult, JW_CACHE_EXP_1_DAY);
	return $arrResult;
}

public function getAlert() {
	$arrLinkList = array();
	
	$mixQueryResult = $this->db->query(
"SELECT name,link_type,link_description 
FROM adm_link_list 
WHERE link_alert = 1 AND link_status > 0 
ORDER BY id");
	
	return $mixQueryResult->result_array();
}

# DETAIL
public function getAvailability($intLinkID, $intPrivilegeID = 0, $intMode = 0) {
	if(empty($intPrivilegeID) && $this->session->userdata('strAdminPriviledge') != '')
		$intPrivilegeID = $this->session->userdata('strAdminPriviledge');
	if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Madmlinklist_getAvailability_'.$intLinkID.'_'.$intPrivilegeID.'_'.$intMode)) return $mixResult;

	$bolReturn = FALSE;
	switch($intMode) {
		case 1: $strMode = 'adlk_allow_insert'; break;
		case 2: $strMode = 'adlk_allow_update'; break;
		case 3: $strMode = 'adlk_allow_delete'; break;
		default: $strMode = 'adlk_allow_view'; break;
	}
	$mixQueryResult = $this->db->query(
"SELECT id,link_type,link_description 
FROM adm_link_list 
WHERE id = $intLinkID AND link_status > 0 
ORDER BY id");
	if($mixQueryResult->num_rows() > 0) {
		$mixLinkData = $mixQueryResult->row();
		if($mixLinkData->link_type == 'link' && $mixLinkData->link_description == '') $bolReturn = TRUE;
		else {
			$mixQueryResult = $this->db->query(
"SELECT id 
FROM adm_link_priviledge 
WHERE adlk_login_id = $intPrivilegeID AND adlk_link_id = $intLinkID AND $strMode = 1");
			if($mixQueryResult->num_rows() > 0) $bolReturn = TRUE;
		}
	}

	$this->cache->{JW_CACHE_TYPE}->save('Madmlinklist_getAvailability_'.$intLinkID.'_'.$intPrivilegeID.'_'.$intMode, $bolReturn, JW_CACHE_EXP_1_DAY);
	return $bolReturn;
}

function getMenuPermission($intLinkID,$intStatus) {
	if($this->session->userdata('strAdminPriviledge') != '') $intPrivilegeID = $this->session->userdata('strAdminPriviledge');
	else $intPrivilegeID = 0;
	if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Madmlinklist_getMenuPermission_'.$intLinkID.'_'.$intStatus.'_'.$intPrivilegeID)) return $mixResult;

	$arrResult = array(
		'bolAllowView' => FALSE,'bolAllowInsert' => FALSE,'bolAllowUpdate' => FALSE,'bolAllowDelete' => FALSE,'bolAllowApprove' => FALSE,'bolBtnAdd' => FALSE,'bolBtnEdit' => FALSE,'bolBtnDelete' => FALSE,'bolBtnPrint' => FALSE,'bolBtnApprove' => FALSE,'bolBtnDisapprove' => FALSE,'bolBtnRollback' => FALSE
	);
	$mixQueryResult = $this->db->query("SELECT * FROM adm_link_priviledge WHERE adlk_login_id = $intPrivilegeID AND adlk_link_id = $intLinkID");
	if($mixQueryResult->num_rows() > 0) {
		$r = $mixQueryResult->row();
		$bolAllowView = $r->adlk_allow_view; $bolAllowInsert = $r->adlk_allow_insert; $bolAllowUpdate = $r->adlk_allow_update; $bolAllowDelete = $r->adlk_allow_delete; $bolAllowApprove = $r->adlk_allow_approve;

		$bolBtnAdd = FALSE; $bolBtnEdit = FALSE; $bolBtnDelete = FALSE; $bolBtnPrint = FALSE;
		$bolBtnApprove = FALSE; $bolBtnDisapprove = FALSE; $bolBtnRollback = FALSE;

		if($intStatus == 0) {
			if($bolAllowInsert) $bolBtnAdd = TRUE;
			if($bolAllowUpdate) $bolBtnEdit = TRUE;
			if($bolAllowDelete) $bolBtnDelete = TRUE;
			if($bolAllowApprove) $bolBtnApprove = TRUE;
			if($bolAllowApprove) $bolBtnDisapprove = TRUE;
		} else if($intStatus == 1) {
			if($bolAllowDelete) $bolBtnDelete = TRUE;
			if($bolAllowApprove) $bolBtnRollback = TRUE;
		} else if($intStatus == 2) {
			if($bolAllowInsert || $bolAllowUpdate) $bolBtnPrint = TRUE;
			if($bolAllowDelete) $bolBtnDelete = TRUE;
			if($bolAllowApprove) $bolBtnRollback = TRUE;
		} else if($intStatus == 3) {
			if($bolAllowInsert || $bolAllowUpdate) $bolBtnPrint = TRUE;
			if($bolAllowDelete) $bolBtnDelete = TRUE;
			//if($bolAllowApprove) $bolBtnRollback = TRUE;
		} else if($intStatus == 4) {
			if($bolAllowInsert || $bolAllowUpdate) $bolBtnPrint = TRUE;
		}

		$arrResult = array(
			'bolAllowView' => $bolAllowView,
			'bolAllowInsert' => $bolAllowInsert,
			'bolAllowUpdate' => $bolAllowUpdate,
			'bolAllowDelete' => $bolAllowDelete,
			'bolAllowApprove' => $bolAllowApprove,
			'bolBtnAdd' => $bolBtnAdd,
			'bolBtnEdit' => $bolBtnEdit,
			'bolBtnDelete' => $bolBtnDelete,
			'bolBtnPrint' => $bolBtnPrint,
			'bolBtnApprove' => $bolBtnApprove,
			'bolBtnDisapprove' => $bolBtnDisapprove,
			'bolBtnRollback' => $bolBtnRollback
		);
	}

	$this->cache->{JW_CACHE_TYPE}->save('Madmlinklist_getMenuPermission_'.$intLinkID.'_'.$intStatus.'_'.$intPrivilegeID, $arrResult, JW_CACHE_EXP_1_DAY);
	return $arrResult;
}

public function getHelp($intID,$bolLinkChecked = FALSE) {
	if($mixResult = $this->cache->{JW_CACHE_TYPE}->get('Madmlinklist_getHelp_'.$intID.'_'.intval($bolLinkChecked))) return $mixResult;

	if($bolLinkChecked) # Check link
		$mixQueryResult = $this->db->query(
"SELECT *
FROM adm_link_list
WHERE id = $intID");
	else # Check table
		$mixQueryResult = $this->db->query(
"SELECT *
FROM adm_link_list
WHERE link_type = 'table' AND link_description = $intID");

	if($mixQueryResult->num_rows() > 0) $mixResult = $mixQueryResult->row();
	else $mixResult = FALSE;

	$this->cache->{JW_CACHE_TYPE}->save('Madmlinklist_getHelp_'.$intID.'_'.intval($bolLinkChecked), $mixResult, JW_CACHE_EXP_1_DAY);
	return $mixResult;
}

}

/* End of File */