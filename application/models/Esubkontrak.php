<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Esubkontrak extends Eloquent 
{
    protected $table = 'subkontrak';
    public $timestamps = false;

    public function kontrak()
    {
        return $this->belongsTo(Ekontrak::class, 'kontrak_id');
    }

    public function owner()
    {
        return $this->belongsTo(Estaff::class);
    }

    public function subkontrak_team()
    {
        return $this->hasMany(Esubkontrakteam::class, 'subkontrak_id');
    }

    public function subkontrak_termin()
    {
        return $this->hasMany(Esubkontraktermin::class, 'subkontrak_id');
    }

    public function subkontrak_material()
    {
        return $this->hasMany(Esubkontrakmaterial::class, 'subkontrak_id');
    }

    public function subkontrak_nonmaterial()
    {
        return $this->hasMany(Esubkontraknonmaterial::class, 'subkontrak_id');
    }

}