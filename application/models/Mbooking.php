<?php
/*
PUBLIC FUNCTION:
- getSJByacceptance2ID(intID)
- getItems(intStartNo,intPerPage)
- getUserHistory(intUserID,intPerPage)
- getItemByID(intID)
- getCount()
- searchBySupplier(strSupplierName)
- checkAvailable(intLapangan,dateFrom,dateUntil)
- add(intPO,intAcceptance,intSuppID,intWarehouseID,strEkspedition,strDescription,intTax,intDiscount,strSuratJalan,intStatus,strDate)
- editByID(intStatus)
- editByID2($intID,$strDescription,$intStatus,$intTotal,$strDateFrom,$strDateUntil)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

PRIVATE FUNCTION:
- 	
*/

class Mbooking extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('booking');
}

public function getItems($intStartDate ,$intEndDate) {
    $this->setQuery(
"SELECT b.id,book_customer_name,book_customer_phone,book_from,book_until,cust_name,cust_phone,book_lapangan_id,book_status,
    CONCAT(DATE(book_from),' ',SEC_TO_TIME((TIME_TO_SEC(book_from) DIV 900) * 900)) AS book_from_round,
    CONCAT(DATE(book_until),' ',SEC_TO_TIME((TIME_TO_SEC(book_until) DIV 900) * 900)) AS book_until_round
FROM booking AS b
LEFT JOIN jw_customer AS c ON book_customer_id = c.id
WHERE book_from < '$intEndDate' AND book_until > '$intStartDate'
ORDER BY b.cdate ASC, b.id ASC");
	
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getUserHistory($intUserID,$intPerPage,$dateNow='') {
    if($dateNow == '') $strWhere = "AND book_from > '$dateNow''";
    else $strWhere='';

	$this->setQuery(
"SELECT b.id, b.cdate, book_code, book_lapangan_id, book_customer_name,book_customer_phone,book_from,book_until,cust_name,cust_phone
FROM booking AS b
LEFT JOIN jw_customer AS c ON book_customer_id = c.id
WHERE (b.cby = $intUserID OR b.mby = $intUserID) AND book_from > '$dateNow'
ORDER BY b.cdate DESC, b.id DESC LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID = 0) {
	if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
	else $strWhere = "WHERE p.id = $intID";
	
	$this->setQuery(
"SELECT 
    p.id, book_code, book_lapangan_id, book_product_id, book_customer_id, book_customer_name, book_customer_phone,
    book_from, book_until, book_total, book_status, book_customer_id AS cust_id, book_description,
    cust_code, cust_name, cust_phone, cust_address, cust_city, cust_outlettype, cust_limitkredit, p.cdate AS book_date,
    prod_title, prod_price, prod_currency, prod_hpp, prod_type
FROM booking AS p
LEFT JOIN jw_customer AS c ON book_customer_id = c.id
LEFT JOIN jw_product AS prod ON book_product_id = prod.id
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}


public function getCount() {
	$this->dbSelect('id');
	return $this->getNumRows();
}

public function searchBySupplier($strSupplierName) {
	$strSupplierName = urldecode($strSupplierName);

	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_grandtotal, acce_status, supp_name, supp_address, supp_city, supp_phone, ware_name,acce_code,acce_tax
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
WHERE (supp_name LIKE '%$strSupplierName%')
ORDER BY p.cdate DESC, p.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function checkAvailable($intLapangan=0,$dateFrom = '',$dateUntil = '') {
    if($dateFrom == '' || $dateUntil=='') return 0;

    $this->setQuery(
"SELECT p.id,p.book_from,book_until
FROM booking AS p
WHERE

      ((book_lapangan_id = $intLapangan AND book_from <= '$dateFrom' AND book_until > '$dateFrom') AND
      (book_lapangan_id = $intLapangan AND book_from < '$dateUntil' AND book_until >= '$dateUntil')) OR

      (book_lapangan_id = $intLapangan AND '$dateFrom' <= book_from AND '$dateUntil' >= book_until) OR

      (book_lapangan_id = $intLapangan AND '$dateFrom' < book_from AND '$dateUntil' > book_from AND '$dateUntil' <= book_until) OR

      (book_lapangan_id = $intLapangan AND '$dateFrom' >= book_from AND '$dateFrom' < book_until AND '$dateUntil' > book_until)
      ");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return 1;
}

/*public function checkAvailableUntil($intLapangan=0,$date = '') {
    if($date == '') return 0;

    $this->setQuery(
"SELECT p.id
FROM booking AS p
WHERE (book_from < '$date' AND book_until >= '$date' AND book_lapangan_id = $intLapangan)");

    if($this->getNumRows() > 0) return 0;
    else return 1;
}*/

public function add($intLapangan,$intProduct,$intCustomer,$intPrice,$strCustName,$strCustPhone,$strDateFrom,$strDateUntil,$strDescription,$intStatus,$strDate) {
    return $this->dbInsert(array(
        'book_lapangan_id' => $intLapangan,
        'book_product_id' => $intProduct,
        'book_customer_id' => !empty($intCustomer) ? $intCustomer : '1',
        'book_total' => !empty($intPrice) ? $intPrice : 0,
        'book_customer_name' => !empty($strCustName) ? $strCustName : '',
        'book_customer_phone' => !empty($strCustPhone) ? $strCustPhone : '',
        'book_from' => $strDateFrom,
        'book_until' => $strDateUntil,
        'book_description' => $strDescription,
        'book_status' => $intStatus,
        'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
    ));
}

public function editByID($intID,$intStatus) {
    return $this->dbUpdate(array(
            'book_status' => $intStatus),
        "id = $intID");
}

public function editByID2($intID,$strDescription,$intStatus,$intTotal,$strDateFrom,$strDateUntil) {
    return $this->dbUpdate(array(
            'book_from' => $strDateFrom,
            'book_until' => $strDateUntil,
            'book_description' => $strDescription,
            'book_status' => $intStatus,
            'book_total' => $intTotal),
        "id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */