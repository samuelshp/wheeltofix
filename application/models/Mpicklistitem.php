<?php

class Mpicklistitem extends JW_Model {
    private $_CI;

    public function __construct() { 
        parent::__construct(); 
        $this->initialize('pick_list_item');

        $this->_CI=& get_instance();
    }

    public function add($intPickListID, $arrItem) {
        $this->_CI->load->model('Minvoiceorderitem');

        for($i=0; $i < count($arrItem['idProduct']); $i++) {
            if($arrItem['chbxPick'][$i]){
                $intInsertItem = $this->dbInsert(array(
                    'pcli_pick_list_id' => $intPickListID,
                    'pcli_product_id' => $arrItem['idProduct'][$i],
                    'pcli_qty' => $arrItem['quantity_processed'][$i],
                    'pcli_qty_display' => $arrItem['quantity_processed'][$i]
                ));

                $this->_CI->Minvoiceorderitem->updateProcessed($arrItem['intSOID'], $arrItem['idProduct'][$i], $arrItem['aQty'][$i]);
            }
        }
        return $intInsertItem;
    }

    public function update($intPickListID, $intProductID, $arrItem) {
        $this->_CI->load->model('Minvoiceorderitem');

        for($i=0; $i < count($arrItem['idProduct']); $i++) {
            if($arrItem['chbxPick'][$i]){
                $intInsertItem = $this->dbUpdate(array(
                    'pcli_qty' => $arrItem['quantity_processed'][$i],
                    'pcli_qty_display' => $arrItem['quantity_processed'][$i]
                ),
                    "pcli_pick_list_id = $intPickListID AND pcli_product_id = $intProductID[$i]");
                
                $count = $this->recalculate($arrItem['intSOID'], $arrItem['idProduct'][$i]);

                $this->_CI->Minvoiceorderitem->updateProcessed($arrItem['intSOID'], $arrItem['idProduct'][$i], $count['totalQty']);
            }
        }
        return $intInsertItem;
    }

    public function recalculate($intSOID, $intProductID) {
        $this->setQuery("SELECT SUM(pcli_qty) AS totalQty, SUM(pcli_qty_display) AS totalQtyDisplay
        FROM pick_list AS pl
        LEFT JOIN pick_list_item AS pli ON pl.id = pli.pcli_pick_list_id
        WHERE pl.pcls_status != -1 AND pli.pcli_product_id = $intProductID AND pl.pcls_so_id = $intSOID");

        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;
    }
}