<?php
/*
PUBLIC FUNCTION:
- getItems(intStartNo,intPerPage)
- getItemsByDateRange(strFrom,strTo)
- getARAPItems(strType)
- getLeftoverItems($strType)
- getItemByID(intID)
- getUserHistory(intUserID,intPerPage)
- getCount()
- add(intFinanceType,intTransactionID,strDescription,intPrice,intStatus,strDate)
- addAutomatic(intFinanceType,intTransactionID,strName,strDate,intPrice)
- editByID(intID,$intStatus)
- editAutomatic(intFinanceType,$intTransactionID,intPrice)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mfinance extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('finance');
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT id, fina_type, fina_transaction_id, fina_title, fina_price, fina_status, cdate AS fina_date
FROM finance
ORDER BY cdate DESC LIMIT $intStartNo, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDateRange($strFrom,$strTo) {
	$this->setQuery(
"SELECT f.id, fina_type, fina_transaction_id, fina_title, fini_to, fini_title, fini_price, f.cdate AS fina_date, fi.cdate AS fini_date, fini_duedate
FROM finance AS f
INNER JOIN finance_item AS fi ON fini_finance_id = f.id AND fini_status = '3'
WHERE DATE(fi.cdate) >= DATE('$strFrom') AND DATE(fi.cdate) <= DATE('$strTo')
ORDER BY fi.cdate ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getARAPItems($strType = 'ALL') {
	if($strType == 'AR') $strWhere = "WHERE fina_type IN (1,3)";
	else if($strType == 'AP') $strWhere = "WHERE fina_type IN (2,4)";
	else $strWhere = '';
	
	$this->setQuery(
"SELECT f.id, fina_type, fina_transaction_id, fina_title, fini_to, fini_title, fini_price, f.cdate AS fina_date, fi.cdate AS fini_date, fini_duedate
FROM finance AS f
INNER JOIN finance_item AS fi ON fini_finance_id = f.id AND fini_status NOT IN (1,3)
$strWhere
ORDER BY fi.cdate ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getLeftoverItems($strType) {
	if($strType == 'AR') $strWhere = " AND fina_type IN (1,3)";
	else if($strType == 'AP') $strWhere = " AND fina_type IN (2,4)";
	else $strWhere = '';

	$this->dbSelect('id,fina_type,fina_transaction_id,fina_title,fina_price,cdate AS fina_date',"fina_status NOT IN (1,3)$strWhere",'cdate ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id, fina_type, fina_transaction_id, fina_title, fina_price, fina_status, cdate AS fina_date',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT id, fina_title, fina_status, cdate AS fina_date
FROM finance AS i
WHERE (i.cby = $intUserID OR i.mby = $intUserID)
ORDER BY i.cdate DESC LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id');
	
	return $this->getNumRows();
}

public function search($strKeyword) {
	$strKeyword = urldecode($strKeyword);
	
	$this->setQuery(
"SELECT id, fina_type, fina_transaction_id, fina_title, fina_price, fina_status, cdate AS fina_date
FROM finance
WHERE (fina_title LIKE '%$strKeyword%')
ORDER BY cdate DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intFinanceType,$intTransactionID,$strDescription,$intPrice,$intStatus,$strDate) { 
	return $this->dbInsert(array(
		'fina_type' => $intFinanceType,
		'fina_transaction_id' => $intTransactionID,
		'fina_title' => $strDescription,
		'fina_price' => $intPrice,
		'fina_status' => $intStatus, // Status tidak bisa hilang karena ada transaksi cashier
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d')
	)); 
}

public function addAutomatic($intFinanceType,$intTransactionID,$strName,$strDate,$intPrice) {
	$strDate = formatDate2(str_replace('/','-',$strDate),'d F Y');
	
	if($intFinanceType == 1) $strDescription = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-invoicedescription',array($strName.' - '.$strDate)); 
	else if($intFinanceType == 2) $strDescription = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-purchasedescription',array($strName.' - '.$strDate)); 
	else $strDescription = '';

	return $this->dbInsert(array(
		'fina_type' => $intFinanceType,
		'fina_transaction_id' => $intTransactionID,
		'fina_title' => $strDescription,
		'fina_price' => $intPrice,
		'fina_status' => 0));
}

public function editByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'fina_status' => $intStatus),"id = $intID");
}

public function editAutomatic($intFinanceType,$intTransactionID,$intPrice) {
	return $this->dbUpdate(array(
		'fina_price' => $intPrice),"fina_type = $intFinanceType AND fina_transaction_id = $intTransactionID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */