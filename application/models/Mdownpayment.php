<?php
/*
PUBLIC FUNCTION:
- getAllCustomer(strKeyword)
- getAllCustomerWithSupplierBind(strKeyword,intSupplier)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdownpayment extends JW_Model {

    // Constructor
    public function __construct() { 
    	parent::__construct();
    	
        $this->initialize('downpayment');
    }

    public function getAllDownpayment($intStartNo = -1,$intPerPage = -1) {
        if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "dp.dpay_date DESC, dp.id DESC";
        else $strOrderBy = "dp.dpay_date DESC, dp.id DESC LIMIT $intStartNo, $intPerPage";

        $this->setQuery(
        "SELECT dp.*, kopb_code, supp_name, kont_name
        FROM downpayment AS dp
        LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
        LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
        LEFT JOIN kontrak AS k on k.id = dpay_kontrak_id
        WHERE dpay_status > ".STATUS_DELETED."
        ORDER BY $strOrderBy");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getCount() {
        $this->dbSelect('id');
        return $this->getNumRows();
    }

    public function getDPbySuppID($intSupplierID) {
        $this->setQuery(
        "SELECT dp.id, dpay_code, dpay_date, dpay_supplier_id, dpay_amount, dpay_description, supp_name
        FROM downpayment AS dp
        LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
        WHERE dpay_status > ".STATUS_DELETED." AND dpay_supplier_id = $intSupplierID
        ORDER BY dpay_date DESC");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getLastCode($code) {
    	$this->setQuery(
        "SELECT SUBSTRING(dpay_code,10,4) as code
        FROM downpayment
        WHERE dpay_code like '$code%'
        ORDER BY dpay_code DESC");

        if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
        else return false;
    }

    public function add($strDPCode, $strDate, $intKontrakID, $intSupplierID, $intKPID, $intAmount,$intPph,$intAmountPph,$intPpn,$intAmountPpn,$intAmountFinal,$strDescription) {
        return $this->dbInsert(array(
            'dpay_code' => $strDPCode,
            'dpay_date' => $strDate,
            'dpay_kontrak_id' => $intKontrakID,
            'dpay_supplier_id' => $intSupplierID,
            'dpay_kontrak_pembelian_id' => (!empty($intKPID)) ? $intKPID : 0,
            'dpay_amount' => str_replace(['.',','],['','.'],$intAmount),
            'dpay_pph' => str_replace(['.',','],['','.'],$intPph),
            'dpay_amount_pph' => str_replace(['.',','],['','.'],$intAmountPph),
            'dpay_ppn' => str_replace(['.',','],['','.'],$intPpn),
            'dpay_amount_ppn' => str_replace(['.',','],['','.'],$intAmountPpn),
            'dpay_amount_final' => str_replace(['.',','],['','.'],$intAmountFinal),
            'dpay_description' => $strDescription,
            'dpay_status' => STATUS_APPROVED,
            'dpay_sisa_bayar' => str_replace(['.',','],['','.'],$intAmountFinal),
            'cdate' => date('Y-m-d H:i')
        ));
    }

    public function getAllDataByID($id){
        $this->setQuery(
            "SELECT dp.*, dp.dpay_amount_final, kopb_code, supp_name, kont_name, jc.cust_name
            FROM downpayment AS dp
            LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
            LEFT JOIN kontrak AS k on k.id = dpay_kontrak_id
            LEFT JOIN jw_customer as jc ON jc.id = k.owner_id
            LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
            WHERE dp.id = $id"
        );

        if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
        else return false;
    }

    public function getAllDpDataByIdSupplier($id){
        $this->setQuery(
            "SELECT dp.id, dpay_code, dpay_date, dpay_supplier_id, dpay_amount, kopb_code, dpay_description, supp_name
            FROM downpayment AS dp
            LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
            LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
            WHERE dpay_status > ".STATUS_DELETED." AND supp.id = $id"
        );

        if($this->getNumRows() > 0) return $this->getSelectedRow('Array');
        else return false;
    }

    public function getAllDpDataByIdSupplierAndIdKontrak($idKontrak, $idSupplier){
        $this->setQuery(
            "SELECT dp.*, kopb_code, supp.supp_name, k.kont_name
            FROM downpayment AS dp
            LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
            LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
            LEFT JOIN kontrak as k on k.id = dp.dpay_kontrak_id
            WHERE dpay_status > ".STATUS_DELETED." AND dp.dpay_supplier_id = $idSupplier AND dp.dpay_kontrak_id = $idKontrak AND dpay_terbayar > 0"
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
    }

    public function getAllDpDataByIdDp($id){
        $this->setQuery(
            "SELECT dp.*, kopb_code, supp_name, kont_name
            FROM downpayment AS dp
            LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
            LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
            LEFT JOIN kontrak as k on k.id = dp.dpay_kontrak_id
            LEFT JOIN purchase_invoice_history as pih on pih.pinvhis_dp = dp.id
            WHERE dp.id = $id AND dpay_status in (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")
            GROUP BY dp.id"
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
    }

    public function countDp($idSupplier,$idDp){
        $this->setQuery(
            "SELECT SUM(pinvhis_dipakai) as total_terpakai
            from purchase_invoice_history
            WHERE pinvhis_supplier = $idSupplier AND pinvhis_dp = $idDp AND pinvhis_status >= ".STATUS_APPROVED
        );

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
    }

    public function updateStatusToFinish($idDp){
        return $this->dbUpdate(array(
			'dpay_status' => STATUS_FINISHED),
		"id = $idDp");
    }

    public function checkEditableDp($idDp){
        $this->setQuery(
            "SELECT id
            FROM purchase_invoice_history
            where pinvhis_dp = $idDp");
    
            if($this->getNumRows() > 0) return $this->getQueryResult('Array');
            else return false;
    }

    public function editDp($intID,$pph,$ppn,$amount,$amount_pph,$amount_ppn,$final_total){
        return $this->dbUpdate(array(
            'dpay_pph' => $pph,
            'dpay_amount' => $amount,
            'dpay_amount_pph' => $amount_pph,
            'dpay_amount_ppn' => $amount_ppn,
            'dpay_amount_final' => $final_total,
            'dpay_ppn' => $ppn,
            'dpay_sisa_bayar' => $final_total 
        ),
		"id = $intID");
    }

    public function deleteDp($intID){
        return $this->dbUpdate(array(
            'dpay_status' => STATUS_DELETED
        ),
		"id = $intID");
    }

    public function updatePerson($id,$orang){
        return $this->dbUpdate(array(
            'mby' => $orang,
            'mdate' => date('Y/m/d H:i:s')),
		"id = $id");
    }

    public function searchBy($arrSearchData,$intStartNo = -1,$intPerPage = -1)
    {
        $where = '';
        if (!empty($arrSearchData['dateStart']) && empty($arrSearchData['dateEnd'])) $where .= " AND dpay_date = '".$arrSearchData['dateStart']."'";
        if (!empty($arrSearchData['dateEnd']) && !empty($arrSearchData['dateStart'])) $where .= "AND dpay_date BETWEEN '".$arrSearchData['dateStart']."' AND '".$arrSearchData['dateEnd']."'";
        if (!empty($arrSearchData['strSearchSupplier'])) $where .= "AND supp_name LIKE '%".$arrSearchData['strSearchSupplier']."%'";

        if (!empty($arrSearchData['txtSearchNoDP'])) $where .= " AND dpay_code = '".$arrSearchData['txtSearchNoDP']."'";

        if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "dp.dpay_date DESC, dp.id DESC";
        else $strOrderBy = "dp.dpay_date DESC, dp.id DESC LIMIT $intStartNo, $intPerPage";

        $this->setQuery(
        "SELECT dp.*, kopb_code, supp_name, kont_name
        FROM downpayment AS dp
        LEFT JOIN jw_supplier AS supp on supp.id = dpay_supplier_id
        LEFT JOIN kontrak_pembelian AS kp on kp.id = dpay_kontrak_pembelian_id
        LEFT JOIN kontrak AS k on k.id = dpay_kontrak_id
        WHERE dpay_status > ".STATUS_DELETED." $where
        ORDER BY $strOrderBy");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

}

/* End of File */