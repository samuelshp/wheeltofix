<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Euangmasukcustomer extends Eloquent 
{
    protected $table = 'uang_masuk_customer';
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo(Ecustomer::class);
    }

    public function account()
    {
        return $this->belongsTo(Eaccount::class);
    }

}