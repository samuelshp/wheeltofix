<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ejabatan extends Eloquent 
{
    protected $table = 'jw_jabatan';
    public $timestamps = false;
}