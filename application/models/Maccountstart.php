<?php

class Maccountstart extends JW_Model
{
    
    public function __construct()
    {
        parent::__construct(); 
	    $this->initialize('jw_account_start');
    }

    public function getPerPeriod($strOrderBy = '', $intStartNo = -1,$intPerPage = -1)
    {        
        if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "acst_period ".$intStartNo;
        else $strOrderBy = "acst_period ".$strOrderBy." LIMIT ". $intStartNo .",".$intPerPage;
        
        $this->setQuery('SELECT * FROM jw_account_start GROUP BY acst_period ORDER BY '.$strOrderBy);        
        
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function getByPeriod($strPeriod)
    {
        $this->setQuery('SELECT
            a.id,
            a.acco_code, 
            a.acco_name,
            tbSaldoAwal.*
        FROM jw_account a
        LEFT JOIN 
        (
            SELECT s.acse_account_id,s.acst_period,s.acst_debet,s.acst_credit, adlg.adlg_name FROM jw_account_start s
            LEFT JOIN adm_login adlg ON s.cby = adlg.id
        ) tbSaldoAwal ON a.id = tbSaldoAwal.acse_account_id
        WHERE acst_period = "'.$strPeriod.'" AND a.acco_status = 2
        ORDER BY `acco_code` ASC;
            ');
        return $this->getQueryResult('Array');
    }

    public function getCount() {
        $this->setQuery('SELECT `id` FROM jw_account_start GROUP BY `acst_period`');
        return $this->getNumRows();
    }

    public function getItems() {
       
        $this->setQuery('SELECT 
        `jw_account_start`.`acse_account_id`,
        `jw_account`.`acco_name`,
        `acst_period`,
        `acst_debet`,
        `acst_credit`,
        `adm_login`.`adlg_name`
    FROM jw_account_start 
    JOIN jw_account ON `jw_account_start`.`acse_account_id` = `jw_account`.`acco_code` 
    JOIN adm_login ON `adm_login`.`id` = `jw_account_start`.`cby` 
    WHERE acst_period = "'.$strPeriod.'"
    ORDER BY '.$strOrderBy.'
        ');        
    }

    public function add($arrAccountToInsert)
    {
        $intInsertID = $this->dbInsert(array(
            'acse_account_id' => $arrAccountToInsert['acse_account_id'],
            'acst_period' => formatDate2(str_replace('/','-',$arrAccountToInsert['acst_period']),'Y-m-d'),
            'acst_debet' => $arrAccountToInsert['acst_debet'],
            'acst_credit' => $arrAccountToInsert['acst_credit'],
        ));

        return $intInsertID;
    }

    public function update($arrAccountToUpdate)
    {
        return $this->dbUpdate(array(            
            'acst_debet' => $arrAccountToUpdate['acst_debet'],
            'acst_credit' => $arrAccountToUpdate['acst_credit']),
            array(
                'acse_account_id' => $arrAccountToUpdate['acse_account_id'],
                'acst_period'   => "'".$arrAccountToUpdate['acst_period']."'"
            ));
    }

    public function delete($strPeriod)
    {
        return $this->dbDelete("acst_period = '$strPeriod'");
    }

    public function sappInsert($arrData)
    {
        $this->initialize('saldoawal_penerimaan_pengeluaran_proyek');

        return $this->dbInsert($arrData);
    }

    public function sappUpdate($arrData, $intPage)
    {
        $this->initialize('saldoawal_penerimaan_pengeluaran_proyek');
        return $this->dbUpdate($arrData, "id=".$intPage);        
    }

    public function sappDelete($intPage)
    {
        $this->initialize('saldoawal_penerimaan_pengeluaran_proyek');
        return $this->dbDelete("id=".$intPage);        
    }

    public function sappBrowse()
    {
        $this->setQuery("
            SELECT a.*, b.kont_name FROM saldoawal_penerimaan_pengeluaran_proyek a
            JOIN kontrak b ON a.kontrak_id = b.id
            ORDER BY tanggal DESC
        ");

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function sappView($intPage)
    {
        $this->initialize('saldoawal_penerimaan_pengeluaran_proyek');
        
        $this->dbSelect('*','id = '.$intPage);

        if($this->getNumRows() > 0) return $this->getNextRecord('Array');
        else return false;   
    }

}


?>