<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ebahan extends Eloquent 
{
    protected $table = 'jw_product';
    public $timestamps = false;

    public function material_units()
    {
        return $this->hasMany(Ematerialunit::class, 'munit_material_id');
    }

    public function satuan_bayar()
    {
        return $this->belongsTo(Eunit::class, 'satuan_bayar_id');
    }

}