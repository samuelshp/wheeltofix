<?php
/*
PUBLIC FUNCTION:
- getHeaderIDByID(intID)
- getItemsByAdjustmentID(intAdjustmentID,strMode)
- add(intAdjustmentID,intAdjustmentType,intProductID,strProductDescription,intQuantity1,intQuantity2,intQuantity3, intUnit1,intUnit2,intUnit3,intPrice)
- editByID(intID,intQuantity1,intQuantity2,intQuantity3,intPrice,selAdjust)
- deleteByID(intID)

PRIVATE FUNCTION:

*/

class Madjustmentitem extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('adjustment_item');
}

public function getHeaderIDByID($intID) {
	$this->dbSelect('adit_adjustment_id',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->adit_adjustment_id;
	else return false;
}

public function getItemsByAdjustmentID($intAdjustmentID,$strMode = 'ALL') {
	$this->setQuery(
"SELECT a.id,pro.id AS product_id, adit_adjustment_type, adit_product_id, adit_quantity1, adit_quantity2, adit_quantity3, adit_price, prob_title, proc_title,prod_code,prod_title,adit_description,adit_unit1,adit_unit2,adit_unit3,proc_title, prod_conv1, prod_conv2, prod_conv3
FROM adjustment_item AS a
LEFT JOIN jw_product AS pro ON pro.id = a.adit_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE adit_adjustment_id =  $intAdjustmentID");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intAdjustmentID,$intAdjustmentType,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM adjustment WHERE id = $intAdjustmentID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'adit_adjustment_id' => $intAdjustmentID,
		'adit_adjustment_type' => $intAdjustmentType,
		'adit_product_id' => $intProductID,
		'adit_description' => $strProductDescription,
		'adit_quantity1' => $intQuantity1,
		'adit_quantity2' => $intQuantity2,
		'adit_quantity3' => $intQuantity3,
		'adit_unit1' => $intUnit1,
		'adit_unit2' => $intUnit2,
		'adit_unit3' => $intUnit3,
		'adit_price' => $intPrice,
		'cdate' => $strCDate));
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$selAdjust) {
	return $this->dbUpdate(array(
			'adit_quantity1' => $intQuantity1,
			'adit_quantity2' => $intQuantity2,
			'adit_quantity3' => $intQuantity3,
			'adit_adjustment_type' => $selAdjust,
			'adit_price' => $intPrice),
		"id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */