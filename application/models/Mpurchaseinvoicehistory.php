<?php
/*
PUBLIC FUNCTION:
- getAllCustomer(strKeyword)
- getAllCustomerWithSupplierBind(strKeyword,intSupplier)
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mpurchaseinvoicehistory extends JW_Model {

    // Constructor
    public function __construct() { 
    	parent::__construct();
    	
        $this->initialize('purchase_invoice_history');
    }

    public function getPiHistory($idPi){
        $this->dbSelect('*',"pinvhis_pinv_id = $idPi");
	
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
    }

    public function editStatusPinvHis($idPi){
        return $this->dbUpdate(array(
            'pinvhis_status' => STATUS_DELETED),
		"pinvhis_pinv_id = $idPi");
    }

}

/* End of File */