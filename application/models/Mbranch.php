<?php
/*
PUBLIC FUNCTION:
- getAllData()
- getItemByID(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Mbranch extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct();
	
	$this->initialize('jw_branch');
}

public function getAllData($strKeyword = '') {
	$strKeyword = urldecode($strKeyword);

    if(!empty($strKeyword)) $strWhere = " AND (bran_name LIKE '%$strKeyword%')";
    else $strWhere = '';

    $this->setQuery(
"SELECT id,bran_name
FROM 
WHERE bran_status > 1$strWhere
ORDER BY bran_name ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemByID($intID) {
	$this->dbSelect('id,bran_name',"id = $intID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

}

/* End of File */