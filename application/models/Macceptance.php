<?php
/*
PUBLIC FUNCTION:
- getSJByacceptance2ID(intID)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsBySupplierID(intID,strDate)
- getAccountPayableItems()
- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
- getItemsForFinance()
- getUserHistory(intUserID,intPerPage)
- getSJByPOID(intID)
- getItemByID(intID)
- getPrintDataByID(intID)
- getCount()
- getDateMark(strFrom,strTo)
- getSuppliers(strFrom,strTo)
- getStockacceptance2d(intProductID,intWarehouseID,strLastStockUpdate)
- searchBySupplier(strSupplierName)
- add(intPO,intAcceptance,intSuppID,intWarehouseID,strEkspedition,strDescription,intTax,intDiscount,strSuratJalan,intStatus,strDate)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intStatus,strSuratJalan,strEkspedition,intDiscount,intTax,flagEditable)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

PRIVATE FUNCTION:
- 	
*/

class Macceptance extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('acceptance');
}

public function getAllPOData(){
	$this->setQuery(
		"SELECT p.prch_code, p.cdate, sk.job as kont_job, k.kont_name, jo.ownr_name, jp.prod_title, skm.keterangan, skm.jumlah, skm.satuan, skm.keterangan
		FROM purchase as p
		LEFT JOIN purchase_order as po ON p.prch_po_id = po.id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN jw_owner as jo ON jo.ownr_code = k.owner_id
		LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
		LEFT JOIN subkontrak_material as skm ON skm.subkontrak_id = sk.id
		LEFT JOIN jw_product as jp ON jp.id = skm.material
		ORDER BY p.prch_code ASC
		"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}
public function getAllBPBData($previledge=0){

	if($previledge == 0){
		$this->setQuery(
			"SELECT *
			FROM acceptance
			WHERE acce_status IN (".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")
			"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
	else{
		$this->setQuery(
			"SELECT *
			FROM acceptance as a
			LEFT JOIN purchase as p ON p.id = a.acce_purchase_id
			LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
			LEFT JOIN subkontrak_team as skt ON skt.subkontrak_id = po.idSubKontrak
			WHERE skt.name = $previledge AND acce_status IN (".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")
            GROUP BY a.id
			"
		);

		if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
	}
}

public function getPOData($noPO){
	$this->setQuery(
		"SELECT p.prch_code, p.cdate, sk.job as kont_job, k.kont_name, jo.ownr_name, jp.prod_title, skm.keterangan, skm.jumlah, skm.satuan, skm.keterangan
		FROM purchase as p
		LEFT JOIN purchase_order as po ON p.prch_po_id = po.id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN jw_owner as jo ON jo.ownr_code = k.owner_id
		LEFT JOIN subkontrak as sk ON sk.kontrak_id = k.id
		LEFT JOIN subkontrak_material as skm ON skm.subkontrak_id = sk.id
		LEFT JOIN jw_product as jp ON jp.id = skm.material
		ORDER BY p.prch_code ASC
		"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getArrivedByPurchaseID($intPOID = 0) {
	
	$this->setQuery(
"SELECT p.id, p.cdate,acce_code
FROM acceptance AS p
WHERE acce_purchase_id = $intPOID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItems($intStartNo = -1,$intPerPage = -1) {
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.cdate DESC";
	else $strOrderBy = "p.cdate DESC, p.id DESC LIMIT $intStartNo, $intPerPage";
	
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_status, supp_name, supp_address, supp_city, supp_phone, ware_name,acce_code,acce_grandtotal,acce_tax, acce_payment
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
ORDER BY $strOrderBy");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

//Baru
public function getItemsBarU($intStartNo = -1,$intPerPage = -1) {
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "p.acce_code DESC, p.cdate DESC";
	else $strOrderBy = "p.cdate DESC, p.id DESC LIMIT $intStartNo, $intPerPage";
	
	$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
"SELECT po.pror_code, pu.prch_code, p.id, p.acce_code, p.cdate, po.idKontrak, ai.acit_description, k.kont_name, sk.job as kont_job, pi.pinv_code, p.approvalSM, js.supp_name
FROM acceptance AS p
LEFT JOIN acceptance_item as ai ON ai.acit_acceptance_id = p.id
LEFT JOIN purchase AS pu ON pu.id = ai.acit_purchase_id
LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
LEFT JOIN kontrak as k ON k.id = po.idKontrak
LEFT JOIN purchase_invoice as pi ON pi.pinv_acceptance_id = p.id
LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
WHERE p.acce_status IN (".STATUS_APPROVED.",".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject
GROUP BY p.id
ORDER BY $strOrderBy");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function updateKodeAcceptance($kode_acceptance, $intAcceptanceID){
	return $this->dbUpdate(array(
		'acce_code' => $kode_acceptance),
	"id = $intAcceptanceID");
}

public function updateStatus($intAcceptanceID, $intStatus)
{
	return $this->dbUpdate(array(
		'acce_status' => $intStatus
	), "id = $intAcceptanceID");
}

public function editAccStatus($accept_id=''){
	return $this->dbUpdate(array(
		'acce_status' => STATUS_WAITING_FOR_FINISHING),
	"id = $accept_id");
	// return $this->setQuery(
	// 	"UPDATE acceptance SET acce_status=3 WHERE id = $accept_id "
	// );
}

public function updateJamApprovalPM($accept_id, $date){
	return $this->dbUpdate(array(
		'acdate_pm' => $date),
	"id = $accept_id");
}

public function updateOrangApprovalPM($accept_id, $orang){
	return $this->dbUpdate(array(
		'aby_pm' => $orang),
	"id = $accept_id");
}

public function updateJamSave($accept_id, $date){
	return $this->dbUpdate(array(
		'acdate' => $date),
	"id = $accept_id");
}

public function updateOrangSave($accept_id, $orang){
	return $this->dbUpdate(array(
		'aby' => $orang),
	"id = $accept_id");
}

public function editPMStatus($accept_id=''){
	return $this->dbUpdate(array(
		'approvalPM' => STATUS_WAITING_FOR_FINISHING),
	"id = $accept_id");
	// return $this->setQuery(
	// 	"UPDATE acceptance SET approvalPM=3 WHERE id = $accept_id "
	// );
}

public function uneditPMStatus($accept_id=''){
	return $this->dbUpdate(array(
		'approvalPM' => 1),
	"id = $accept_id");
	// return $this->setQuery(
	// 	"UPDATE acceptance SET approvalPM=1 WHERE id = '$accept_id' "
	// );
}

public function uneditSaveStatus($accept_id='', $tanggal, $orang){
	$this->dbUpdate(array(
		'acce_status' => STATUS_REJECTED,
		'acdate' => $tanggal,
		'aby' => $orang
	),
	"id = $accept_id");

	/*Recalculate*/
	$this->setQuery("
		SELECT acc.idSubKontrak, acit_purchase_id, acit_purchase_item_id, acit_product_id 
		FROM acceptance as acc LEFT JOIN acceptance_item acc_item ON acc.id = acc_item.acit_acceptance_id
		WHERE acit_acceptance_id = $accept_id");
	if ($this->getNumRows() > 0) {
		$acit = $this->getQueryResult('Array');
		foreach ($acit as $v) {
			$purchase_id_lol = $v['acit_purchase_id'];
			$prodId = $v['acit_product_id'];
			$purchase_item_id = $v['acit_purchase_item_id'];
			$id_subkon = $v['idSubKontrak'];
			$new_terterima = $this->Macceptanceitem->recalculateTerterima($purchase_id_lol, $prodId, $purchase_item_id);    
		    if ($new_terterima !== FALSE) {                       
		        $this->load->model('Macceptanceitem');
		        $this->Msubkontrakmaterial->updateTerBpb($id_subkon,$prodId,$new_terterima['jumlah2']);
		        $this->Mpurchaseitem->UpdateTerterima($purchase_item_id, $new_terterima['jumlah'], $new_terterima['jumlah2']);
		    } else return false;
		}
	} else return false;
	
	return true;	
}

public function searchByIdAcc($code){
	$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
		"SELECT po.pror_code, pu.prch_code, p.id, p.acce_code, p.cdate, po.idKontrak, ai.acit_description, k.kont_name, sk.job as kont_job, pi.pinv_code, p.approvalSM, js.supp_name
		FROM acceptance AS p
		LEFT JOIN acceptance_item as ai ON ai.acit_acceptance_id = p.id
		LEFT JOIN purchase AS pu ON pu.id = p.acce_purchase_id
		LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN purchase_invoice as pi ON pi.pinv_acceptance_id = p.id AND pi.pinv_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.")
		LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
		LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
		WHERE p.acce_code LIKE '%$code%' AND p.acce_status IN (".STATUS_APPROVED.",".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject
		GROUP BY p.id
		ORDER BY p.id ASC, p.cdate ASC"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

//

public function getItemsByDate($strDate) {
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_tax, acce_status, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
WHERE DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsBySupplierID($intID,$strDate) {
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_tax, acce_status, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
WHERE acce_supplier_id = $intID AND DATE(p.cdate) = DATE('$strDate')
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}


public function getAccountPayableItems() {
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_tax, supp_name, supp_address, supp_city, supp_phone, ware_name
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
WHERE acce_status IN (".STATUS_OUTSTANDING.",".STATUS_APPROVED.")
ORDER BY p.cdate ASC, p.id ASC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
	$this->setQuery(
"SELECT pi.id, supp_name, supp_address, supp_city, p.cdate AS acce_date, acce_status, acit_quantity
FROM acceptance AS p
INNER JOIN acceptance_item2 AS pi ON acit_acceptance_id = p.id
LEFT JOIN jw_supplier AS s ON acce_supplfier_id = s.id
WHERE acit_product_id = $intProductID AND acce_warehouse_id = $intWarehouseID AND p.cdate > '$strLastStockUpdate'");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForFinance() {
	$this->setQuery(
"SELECT p.id,supp_name AS name,p.cdate AS date
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
WHERE acce_status NOT IN (".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.") AND p.id NOT IN (
	SELECT fina_transaction_id FROM finance WHERE fina_type = 2
)");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_status, supp_name, supp_address, supp_city,acce_code
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
WHERE (p.cby = $intUserID OR p.mby = $intUserID)
ORDER BY p.cdate DESC, p.id DESC
LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID = 0) {
	if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
	else $strWhere = "WHERE p.id = $intID";
	
	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, p.approvalSM, por.idKontrak, por.idSubKontrak, por.pror_code,  k.kont_name, acce_code, p.cby as cby_bpb, acce_tax, acce_description, acce_progress, acce_status, acce_purchase_id,acce_supplier_id,supp_name, ware_name,acce_code,prch_code,acce_discount,prch_supplier_id, acce_warehouse_id,acce_grandtotal, acce_payment, supp_address, supp_phone, acce_jatuhtempo, al.adlg_name, al2.adlg_name as approval,
	(SELECT SUM(ai.acit_quantity1 * pi.prci_price) FROM acceptance_item as ai 
	LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
	LEFT JOIN purchase_item as pi ON pi.prci_product_id = ai.acit_product_id AND pi.prci_purchase_id = p.id
	WHERE acit_acceptance_id = $intID) as grandTotal_BPB
FROM acceptance AS p
LEFT JOIN acceptance_item AS ai ON ai.acit_acceptance_id = p.id
LEFT JOIN purchase AS po ON p.acce_purchase_id = po.id
LEFT JOIN jw_supplier AS s ON s.id = po.prch_supplier_id
LEFT JOIN purchase_order AS por ON por.id = po.prch_po_id
LEFT JOIN kontrak AS k ON k.id = por.idKontrak
LEFT JOIN jw_warehouse AS w ON p.acce_warehouse_id = w.id
LEFT JOIN adm_login as al ON al.id = p.cby
LEFT JOIN adm_login as al2 ON al2.id = p.aby
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getPrintDataByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY p.cdate DESC, p.id DESC LIMIT 0,1";
    else $strWhere = "WHERE p.id = $intID";
	
	$this->setQuery(
"SELECT pror_end_date,ware_name,acce_ekspedition_number, a.cdate AS acce_date,supp_npwp,supp_pkp
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN purchase AS po ON acce_purchase_id = po.id
LEFT JOIN acceptance AS a ON acce_acceptance_id = a.id
LEFT JOIN jw_warehouse AS w ON a.acce_warehouse = w.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getCount() {
	$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
		"SELECT po.pror_code, pu.prch_code, p.id, p.acce_code, p.cdate, po.idKontrak, ai.acit_description, k.kont_name, sk.job as kont_job, pi.pinv_code, p.approvalSM, js.supp_name
		FROM acceptance AS p
		LEFT JOIN acceptance_item as ai ON ai.acit_acceptance_id = p.id
		LEFT JOIN purchase AS pu ON pu.id = ai.acit_purchase_id
		LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN purchase_invoice as pi ON pi.pinv_acceptance_id = p.id
		LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
		LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
		WHERE p.acce_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") $whereInProject
		GROUP BY p.id");

	return $this->getNumRows();
}



public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as acce_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'acce_date ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getSuppliers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT acce_supplier_id,DATE(cdate) AS acce_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'acce_supplier_id ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockacceptance2d($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0; 
	
	$this->setQuery(
"SELECT acit_quantity
FROM acceptance AS p
INNER JOIN acceptance_item2 AS pi ON acit_acceptance_id = p.id
WHERE acit_product_id = $intProductID AND acce_warehouse_id = $intWarehouseID AND acce_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.") AND p.cdate >= '$strLastStockUpdate'");
	
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['acit_quantity'];
	
	return $intTotalStock;
}

public function searchBySupplier($strSupplierName, $arrSearchDate, $intStatus) {
	$strWhere = '';
	if(!empty($strSupplierName)) {
		$strSupplierName = urldecode($strSupplierName);
		$strWhere .= " AND (supp_name LIKE '%{$strSupplierName}%')";
	}
	if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (p.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (p.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= STATUS_OUTSTANDING) $strWhere .= " AND (acce_status = {$intStatus})";

    $whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
"SELECT p.id, p.cdate AS acce_date, acce_grandtotal, acce_status, supp_name, supp_address, supp_city, supp_phone, ware_name,acce_code,acce_tax, acce_payment
FROM acceptance AS p
LEFT JOIN jw_supplier AS s ON acce_supplier_id = s.id
LEFT JOIN jw_warehouse AS w ON acce_warehouse_id = w.id
WHERE p.id > 0{$strWhere} AND p.acce_status IN (".STATUS_APPROVED.",".STATUS_REJECTED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") %whereInProject
ORDER BY p.cdate DESC, p.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($intPO,$intSuppID,$intWarehouseID,$strDescription,$intTax,$intDiscount,$intStatus,$strDate,$intTipeBayar, $strJatuhTempo,$strCustomCode = '', $creator, $id_subkon) {
	if($intTipeBayar == 1) $intStatus = 3;
	return $this->dbInsert(array(
		'acce_purchase_id' => !empty($intPO) ? $intPO : 0,
		'acce_code' => !empty($strCustomCode) ? $strCustomCode : '',
        'acce_supplier_id' => $intSuppID,
		'acce_branch_id' => $this->session->userdata('intBranchID'),
		'acce_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
        'acce_tax' => $intTax,
        'acce_discount' => $intDiscount,
        'acce_description' => $strDescription,
        'acce_jatuhtempo' => $strJatuhTempo,
        'acce_payment' => $intTipeBayar,
		'acce_status' => STATUS_APPROVED,
		'cby' => $creator,
		'cdate' => formatDate2(str_replace('/','-',$strDate." ".date("H:i:s")),'Y-m-d H:i:s'),
		'idSubKontrak' => $id_subkon
	));
}

public function editByID($intID,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
		'acce_progress' => $strProgress,
		'acce_status' => $intStatus),
		"id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intStatus,$intDiscount,$intTax) {
	return $this->dbUpdate(array(
		'acce_description' => $strDescription,
		'acce_progress' => $strProgress,
		'acce_status' => $intStatus,
        'acce_discount' => $intDiscount,
        'acce_tax' => $intTax),
		"id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'acce_status' => $intStatus),"id = $intID");
}

public function deleteByID($intID) {
	return $this->dbUpdate(array(
		'acce_status' => STATUS_DELETED),"id = $intID");
}

public function checkStatusForBPB($idBpb){
	$this->setQuery(
		"SELECT acce_status
		FROM acceptance
		WHERE id = $idBpb"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getHeaderBPB($bpb){
	$this->setQuery(
		"SELECT a.acce_code, k.kont_name, p.prch_supplier_id, a.cdate, a.id, js.supp_name, p.id as idOrderPembelian, k.id as idKontrak
		FROM acceptance as a
        LEFT JOIN acceptance_item as ai On ai.acit_acceptance_id = a.id
		LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
        LEFT JOIN jw_supplier as js ON js.id = p.prch_supplier_id
		LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		WHERE a.id = $bpb"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getDetailBPBByID($id){
	$this->setQuery(
		"SELECT po.pror_code, p.prch_code, ai.id, jp.prod_title, po.idSubKontrak, poi.proi_quantity1, ju2.unit_title as title_pb, ai.acit_quantity1, ju3.unit_title as title_terima, ai.acit_description, ai.acit_terpi, ai.acit_location, ju.unit_title as title_bayar, ai.acit_product_id, p.prch_po_id, p.id as idOrderPembelian, jp.id as idProduct, poi.proi_description, pi.prci_price, pi.id, ai.acit_quantity_bayar
		FROM acceptance_item as ai
        LEFT JOIN purchase as p ON p.id = ai.acit_purchase_id
		LEFT JOIN purchase_item as pi ON pi.id = ai.acit_purchase_item_id AND prci_product_id = ai.acit_product_id
		LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
		LEFT JOIN purchase_order_item as poi ON poi.id = pi.prci_proi_id AND poi.proi_purchase_order_id = p.prch_po_id AND poi.proi_product_id = ai.acit_product_id
		LEFT JOIN jw_product as jp ON jp.id = ai.acit_product_id
		LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_bayar_id
		LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_pb_id
		LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id
		WHERE ai.acit_acceptance_id = $id
        group by ai.id"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getDetailBPB($bpb){
	$this->setQuery(
		"SELECT 
		FROM acceptance_item as ai ON ai.acit_acceptance_id = a.id
		LEFT JOIN jw_product as jp ON jp.id = ai.acit_product_id
		LEFT JOIN jw_unit as 
		LEFT JOIN purchase as p ON p.id = a.acce_purchase_id
		LEFT JOIN purchase_order as po ON po.id = p.prch_po_id
		WHERE ai.acit_acceptance_ LIKE '%$bpb%'"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function searchByNamaProyek($nama_proyek){
	$whereInProject = "AND (p.cby = " . $this->session->userdata('strAdminID');
    if(!empty($this->session->userdata('strProjectInTeam'))) $whereInProject .= " OR sk.id IN (".$this->session->userdata('strProjectInTeam').")";
    $whereInProject .= ")";

	$this->setQuery(
		"SELECT po.pror_code, pu.prch_code, p.id, p.acce_code, p.cdate, po.idKontrak, ai.acit_description, k.kont_name, sk.job as kont_job, pi.pinv_code, p.approvalSM, js.supp_name
		FROM acceptance AS p
		LEFT JOIN acceptance_item as ai ON ai.acit_acceptance_id = p.id
		LEFT JOIN purchase AS pu ON pu.id = ai.acit_purchase_id
		LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN purchase_invoice as pi ON pi.pinv_acceptance_id = p.id
		LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
		LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
		WHERE k.kont_name LIKE '%$nama_proyek%' AND p.acce_status IN (2,1,3,4) $whereInProject
		GROUP BY p.id
		ORDER BY p.id ASC, p.cdate ASC"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getReportAcceptance($intKontrakID,$intSupplierID,$strDateStart,$strDateEnd){
	$strWhere = "WHERE ";
	if(!empty($intKontrakID)) $strWhere .= "po.idKontrak = $intKontrakID AND ";
	if(!empty($intSupplierID)) $strWhere .= "p.prch_supplier_id = $intSupplierID AND ";
	$this->setQuery(
		"SELECT a.id, acce_code, a.cdate, supp_name, kont_name
		FROM acceptance a
		LEFT JOIN purchase p ON p.id = a.acce_purchase_id
		LEFT JOIN purchase_order po ON po.id = p.prch_po_id
		LEFT JOIN jw_supplier s ON s.id = prch_supplier_id
		LEFT JOIN kontrak k ON k.id = po.idKontrak
		$strWhere a.cdate BETWEEN '$strDateStart' AND '$strDateEnd' AND acce_status IN (3,4)
		ORDER BY p.cdate ASC"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function countAllBPBItemByItemAndOPId($productId,$purchaseId){
	$this->setQuery(
		"SELECT sum(acit_quantity1) as jumlah
		from acceptance_item as ai
		LEFT JOIN acceptance as a ON a.id = ai.acit_acceptance_id
		where a.acce_status >= ".STATUS_APPROVED." AND `acit_purchase_id` = $purchaseId AND `acit_product_id` = $productId"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function searchByTglProyek($date){
	$this->setQuery(
		"SELECT po.pror_code, pu.prch_code, p.id, p.acce_code, p.cdate, po.idKontrak, ai.acit_description, k.kont_name, sk.job as kont_job, pi.pinv_code, p.approvalSM, js.supp_name
		FROM acceptance AS p
		LEFT JOIN acceptance_item as ai ON ai.acit_acceptance_id = p.id
		LEFT JOIN purchase AS pu ON pu.id = ai.acit_purchase_id
		LEFT JOIN purchase_order AS po ON po.id = pu.prch_po_id
		LEFT JOIN kontrak as k ON k.id = po.idKontrak
		LEFT JOIN purchase_invoice as pi ON pi.pinv_acceptance_id = p.id
		LEFT JOIN subkontrak as sk ON sk.id = po.idSubKontrak
		LEFT JOIN jw_supplier as js ON js.id = pu.prch_supplier_id
		WHERE p.acce_status IN (".STATUS_APPROVED.",".STATUS_WAITING_FOR_FINISHING.",".STATUS_FINISHED.") AND p.cdate = '$date'
		GROUP BY p.id
		ORDER BY p.id ASC, p.cdate ASC"
	);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

}

/* End of File */