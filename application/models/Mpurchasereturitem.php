<?php
/*
PUBLIC FUNCTION:
- getItemsByPurchaseID(intPurchaseID,strMode)
- getTotalItemsByPurchaseID(intPurchaseID,strMode)
- getLatestPrice($intProductID)
- add(intPurchaseID,intProductID,strProductDescription,intQuantity,intPrice,intDiscount)
- editByID(intID,intQuantity,intPrice,intDiscount)
- deleteByPurchaseID(intID)

PRIVATE FUNCTION:
- __construct()

 *********************************************/

class Mpurchasereturitem extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();

	$this->initialize('purchase_retur_item');
}

public function getItemsByPurchaseID($intPurchaseID) {
   $this->setQuery(
"SELECT p.id,pro.id as product_id, prri_quantity1, prri_quantity2, prri_quantity3, prri_price, prri_discount1, prri_discount2, prri_discount3, prri_product_id, prob_title, proc_title, prod_title,prri_description,prri_unit1,prri_unit2,prri_unit3,prod_code,prri_subtotal, prod_conv1, prod_conv2, prod_conv3
FROM  purchase_retur_item AS p
LEFT JOIN jw_product AS pro ON pro.id = p.prri_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE prri_purchase_retur_id =  $intPurchaseID AND prri_bonus = '0'
ORDER BY p.id ASC"
);

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsBonusByPurchaseID($intPurchaseID) {
    $this->setQuery(
"SELECT p.id,pro.id as product_id, prri_quantity1, prri_quantity2, prri_quantity3, prri_product_id, prob_title, proc_title, prod_title,prri_description,prri_unit1,prri_unit2,prri_unit3,prod_code
FROM  purchase_retur_item AS p
LEFT JOIN jw_product AS pro ON pro.id = p.prri_product_id
LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
WHERE prri_purchase_retur_id =  $intPurchaseID AND prri_bonus = '1'
ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getTotalItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
	/*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,prri_quantity,prri_price,prri_discount,prri_product_id',"prri_purchase_retur_id = $intPurchaseID");
	else $this->dbSelect('',"prri_purchase_retur_id = $intPurchaseID");


	return $this->getNumRows();*/
}

public function getLatestPrice($intProductID) {
	/*$this->setQuery("SELECT prod_currency FROM jw_product WHERE id = $intProductID");

	if($this->getNumRows() > 0) $intCurrencyID = $this->getNextRecord('Object')->prod_currency;
	else return false;

	$this->dbSelect('prri_price',"prri_product_id = $intProductID",'cdate DESC',1);

	if($this->getNumRows() > 0) return formatPriceCurrency($this->getNextRecord('Object')->prri_price,'BASE',$intCurrencyID);
	else return false;*/
}

public function add($intPurchaseID,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice,$intDiscount1='0',$intDiscount2='0',$intDiscount3='0',$intBonus='0') {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM purchase_retur WHERE id = $intPurchaseID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';
    return $this->dbInsert(array(
        'prri_purchase_retur_id' => $intPurchaseID,
        'prri_product_id' => $intProductID,
        'prri_description' => $strProductDescription,
        'prri_quantity1' => $intQuantity1,
        'prri_quantity2' => $intQuantity2,
        'prri_quantity3' => $intQuantity3,
        'prri_unit1' => $intUnit1,
        'prri_unit2' => $intUnit2,
        'prri_unit3' => $intUnit3,
        'prri_price' => $intPrice,
        'prri_discount1' => $intDiscount1,
        'prri_discount2' => $intDiscount2,
        'prri_discount3' => $intDiscount3,
        'prri_bonus' => $intBonus,
        'cdate' => $strCDate));
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {
	return $this->dbUpdate(array(
			'prri_quantity1' => $intQuantity1,
			'prri_quantity2' => $intQuantity2,
			'prri_quantity3' => $intQuantity3,
			'prri_price' => $intPrice,
			'prri_discount1' => $intDiscount1,
			'prri_discount2' => $intDiscount2,
			'prri_discount3' => $intDiscount3),
		"id = $intID");
}

public function deleteByPurchaseID($intID) {
	return $this->dbDelete("prri_purchase_retur_id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

}

/* End of File */