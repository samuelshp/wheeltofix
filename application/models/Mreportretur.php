<?php

class Mreportretur extends JW_Model {

public $_intWarehouseID;

public function __construct() {
	parent::__construct();	
	$this->_intWarehouseID = 1;
}

public function getItemsDetailReturJualPerBarangByCustomer($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invr_code ASC";
    else $strOrderBy = "invr_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, c.cust_name,it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountDetailReturJualPerBarangByCustomer() {
    /*$this->setQuery(
"SELECT iv.invr_code
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)");*/
	$this->setQuery("SELECT iv.invr_code FROM invoice_retur iv");

    return $this->getNumRows();

}

public function searchDetailReturJualPerBarangByCustomer($productID,$txtStart,$txtEnd) {

    $productID = urldecode($productID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, c.cust_name,it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
WHERE p.id = $productID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invr_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function  getItemsDetailReturJualPerCustomerByBarang($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invr_code ASC";
    else $strOrderBy = "invr_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, p.prod_title, it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function  getCountDetailReturJualPerCustomerByBarang() {
    /*$this->setQuery(
"SELECT iv.invr_code
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)");*/
	$this->setQuery("SELECT iv.invr_code FROM invoice_retur iv");

    return $this->getNumRows();
}

public function  searchDetailReturJualPerCustomerByBarang($custID,$txtStart,$txtEnd) {

    $custID = urldecode($custID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, p.prod_title, it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
WHERE c.id = $custID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invr_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
    
public function getItemsDetailReturJualPerSalesmanByBarang($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invr_code ASC";
    else $strOrderBy = "invr_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, p.prod_title, it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;

}

public function getCountDetailReturJualPerSalesmanByBarang() {
    /*$this->setQuery(
"SELECT iv.invr_code
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)");*/
	$this->setQuery("SELECT iv.invr_code FROM invoice_retur iv");

    return $this->getNumRows();

}

public function searchDetailReturJualPerSalesmanByBarang($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invr_code, iv.cdate, p.prod_title, it.inri_quantity1,it.inri_quantity2,
	it.inri_quantity3,p.prod_unit1,p.prod_unit2,p.prod_unit3,
	p.prod_conv1,p.prod_conv2,p.prod_conv3,it.inri_price,
	it.inri_discount1,it.inri_discount2,it.inri_discount3
FROM invoice_retur iv
	INNER JOIN invoice_retur_item it ON (it.inri_invoice_retur_id = iv.id)
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_product p ON (p.id = it.inri_product_id)
	LEFT JOIN jw_salesman s ON (s.id = iv.invr_salesman_id)
WHERE s.id = $salsID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invr_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsDetailReturJualPerSalesmanByCustomer($intStartNo = -1, $intPerPage = -1) {

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "invr_code ASC";
    else $strOrderBy = "invr_code ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT iv.invr_code,iv.cdate,c.cust_code,c.cust_name,iv.invr_grandtotal,iv.invr_taxin
FROM invoice_retur iv
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
ORDER BY $strOrderBy");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getCountDetailReturJualPerSalesmanByCustomer() {
    /*$this->setQuery(
"SELECT iv.invr_code
FROM invoice_retur iv
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)");*/
	$this->setQuery("SELECT iv.invr_code FROM invoice_retur iv");

    return $this->getNumRows();
}

public function searchDetailReturJualPerSalesmanByCustomer($salsID,$txtStart,$txtEnd) {

    $salsID = urldecode($salsID);
    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');

    $this->setQuery(
"SELECT iv.invr_code,iv.cdate,c.cust_code,c.cust_name,iv.invr_grandtotal,iv.invr_taxin
FROM invoice_retur iv
	LEFT JOIN jw_customer c ON (c.id = iv.invr_customer_id)
	LEFT JOIN jw_salesman s ON (s.id = iv.invr_salesman_id)
WHERE s.id = $salsID AND iv.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY invr_code ASC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsPurchaseRetur($intStartNo = -1, $intPerPage = -1) {
    

    if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "proc_title, prob_title, prod_title ASC";
	else $strOrderBy = "proc_title, prob_title, prod_title ASC LIMIT $intStartNo, $intPerPage";

    $this->setQuery(
"SELECT re.cdate, re.prcr_code, proc_title, prob_title, prod_title, 
    ri.prri_unit1, ri.prri_unit2, ri.prri_unit3,
    ri.prri_quantity1, ri.prri_quantity2, ri.prri_quantity3, ri.prri_price, 
    ri.prri_discount1, ri.prri_discount2, ri.prri_discount3
FROM purchase_retur re
	INNER JOIN purchase_retur_item ri ON (ri.prri_purchase_retur_id = re.id)
	LEFT JOIN jw_product p ON (p.id = ri.prri_product_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
	LEFT JOIN jw_product_brand b ON (b.id = p.prod_brand_id)");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;        
}

public function getCountPurchaseRetur() {
    /*$this->setQuery(
"SELECT re.cdate
FROM purchase_retur re
	INNER JOIN purchase_retur_item ri ON (ri.prri_purchase_retur_id = re.id)
	LEFT JOIN jw_product p ON (p.id = ri.prri_product_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
	LEFT JOIN jw_product_brand b ON (b.id = p.prod_brand_id)");*/
	$this->setQuery("SELECT re.cdate FROM purchase_retur re");
    
    return $this->getNumRows();
}

public function searchPurchaseReturPerSupplier($strSupplierName) {
	return false;
    // LEFT JOIN (SELECT trhi_product_id, SUM(trhi_transtype * (trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3)) AS qty

    $strSupplierName = urldecode($strSupplierName);
    if(empty($intWarehouseID)) $intWarehouseID = $this->_intWarehouseID;

    $this->setQuery(
"SELECT p.id, prod_code, prod_barcode, prod_title, prob_title, proc_title, 
	p.prod_conv1, p.prod_conv2, p.prod_conv3,
	coalesce(st.qty, 0) qty, coalesce(st.qty, 0) * (prod_hpp / prod_conv1) nilai
FROM jw_product p
	LEFT JOIN (
		SELECT trhi_product_id, SUM(trhi_quantity1 * trhi_conv1 + trhi_quantity2 * trhi_conv2 + trhi_quantity3 * trhi_conv3) AS qty
		FROM transaction_history
		WHERE trhi_type in ('invoice','kanvas_jual')
			AND trhi_warehouse_id = $intWarehouseID
		GROUP BY trhi_product_id
	) st ON st.trhi_product_id = p.id
	LEFT JOIN jw_product_brand b ON ( b.id = p.prod_brand_id)
	LEFT JOIN jw_product_category c ON (c.id = p.prod_category_id)
    LEFT JOIN jw_supplier s ON (s.id = p.prod_supplier_id)
WHERE (supp_name LIKE '%$strSupplierName%')
ORDER BY proc_title, prob_title, prob_title asc");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

}