<?php
/**
 * Mpurchaseitem.php
 *
 * @version	: 1.5
 * @author	: Victor Julian Lipesik (vjlipesik@gmail.com)
 * @date	: October 10, 2008 (January 5, 2012)
 *
 * This class is model to retrieve data from table purchase item
 *
 */

/********************************************
PUBLIC FUNCTION:
- getItemsByPurchaseID(intPurchaseID,strMode)
- getLatestPrice($intProductID)
- add(intPurchaseID,intProductID,strProductDescription,intQuantity,intPrice,intDiscount)
- editByID(intID,intQuantity,intPrice,intDiscount)
- deleteByPurchaseID(intID)

PRIVATE FUNCTION:

 *********************************************/

class Mpurchaseorderitem extends JW_Model {

// Constructor
public function __construct() {
    parent::__construct();

    $this->initialize('purchase_order_item');
}

//new

public function Purchase_Order_GetItemsByID($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/

    $this->setQuery(
"        SELECT p.id,pro.id as product_id, proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1,proi_purchased2,proi_purchased3, proi_price, proi_discount1, proi_discount2, proi_discount3, proi_product_id, prob_title, proc_title, prod_title,proi_description,proi_unit1,proi_unit2,proi_unit3,proc_title,prod_code,proi_subtotal, prod_conv1, prod_conv2, prod_conv3
    FROM jw_product AS pro
    LEFT JOIN purchase_order_item AS p ON pro.id = p.proi_product_id
    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
    WHERE proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '0'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function Purchase_Order_GetItemsByIDBaru($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/
    #ini yang lama WHERE proi_quantity1 > proi_terpurchase AND poi.proi_purchase_order_id = $intPurchaseID
    $this->setQuery(
    "SELECT poi.id, poi.cdate, po.idSubKontrak, jp.prod_title, ju.unit_title as 'sat_pb', ju2.unit_title as 'sat_bayar', ju3.unit_title as 'sat_terima', poi.proi_description, poi.proi_quantity1, poi.proi_unit1, poi.proi_quantity_terima, po.idSubKontrak, poi.proi_product_id, poi.proi_terpurchase, ju3.unit_title as title_terima, poi.proi_terpurchase_bpb
    FROM purchase_order_item AS poi 
    LEFT JOIn purchase_order as po ON po.id = poi.proi_purchase_order_id
    LEFT JOIN jw_product as jp ON jp.id = poi.proi_product_id 
    LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
    LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_bayar_id 
    LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id 
    WHERE poi.proi_purchase_order_id = $intPurchaseID
    GROUP BY poi.id "
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function Purchase_Order_GetItemsByPoiIDBaru($intPurchaseOrderID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/
    #ini yang lama WHERE proi_quantity1 > proi_terpurchase AND poi.proi_purchase_order_id = $intPurchaseID
    $this->setQuery(
    "SELECT poi.id, poi.cdate, po.idSubKontrak, jp.prod_title, ju.unit_title as 'sat_pb', ju2.unit_title as 'sat_bayar', ju3.unit_title as 'sat_terima', poi.proi_description, poi.proi_quantity1, poi.proi_unit1, poi.proi_quantity_terima, po.idSubKontrak, poi.proi_product_id, poi.proi_terpurchase, ju3.unit_title as title_terima, poi.proi_terpurchase_bpb
    FROM purchase_order_item AS poi 
    LEFT JOIn purchase_order as po ON po.id = poi.proi_purchase_order_id
    LEFT JOIN jw_product as jp ON jp.id = poi.proi_product_id 
    LEFT JOIN jw_unit as ju ON ju.id = jp.satuan_pb_id 
    LEFT JOIN jw_unit as ju2 ON ju2.id = jp.satuan_bayar_id 
    LEFT JOIN jw_unit as ju3 ON ju3.id = jp.satuan_terima_id 
    WHERE poi.proi_purchase_order_id = $intPurchaseOrderID
    GROUP BY poi.id "
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function checkStatusPO($no_pb = 0){
    $this->setQuery(
        "SELECT p.id 
        from purchase as p
        WHERE p.prch_po_id = $no_pb"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getTerPB($isi_subkontrak, $product_id_delete){
    $this->setQuery(
        "SELECT subkontrak_terpb
        from subkontrak_material
        WHERE subkontrak_id = $isi_subkontrak AND material = $product_id_delete
        GROUP BY material"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function updateTerPBAfterDelete($new_terpb, $product_id_delete, $isi_subkontrak){
    $this->initialize('subkontrak_material');
    return $this->dbUpdate(array(
        'subkontrak_terpb' => $new_terpb),
    "material = $product_id_delete AND subkontrak_id = $isi_subkontrak");
    $this->initialize('purchase_order_item');
}

public function Purchase_Order_GetBonusItemsByID($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/

    $this->setQuery(
"        SELECT p.id,pro.id as product_id, proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1,proi_purchased2,proi_purchased3, proi_product_id, prob_title, proc_title, prod_title,proi_description,proi_unit1,proi_unit2,proi_unit3,proc_title,prod_code
    FROM jw_product AS pro
    LEFT JOIN purchase_order_item AS p ON pro.id = p.proi_product_id
    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
    WHERE proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '1'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
public function Purchase_Order_GetPurchasedItemsIDByID($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/
    $this->setQuery(
"        SELECT p.id,p.pro_product_id as product_id, p.idKontrak, p.idSubKontrak
    FROM purchase_order_item AS p
    WHERE p.proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '0'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
public function Purchase_Order_GetPurchasedBonusItemsIDByID($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/
    $this->setQuery(
"        SELECT p.id,pro.id as product_id
    FROM jw_product AS pro
    LEFT JOIN purchase_order_item AS p ON pro.id = p.proi_product_id
    WHERE proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '1'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
//sampe sini
public function UpdatePurchasedItem($intPOID,$intProductID,$intPurchased1,$intPurchased2,$intPurchased3,$intBonus) {

    $this->setQuery("UPDATE purchase_order_item
        SET proi_purchased1 = proi_purchased1 + $intPurchased1,
        proi_purchased2 = proi_purchased2 + $intPurchased2,
        proi_purchased3 = proi_purchased3 + $intPurchased3
        WHERE
        proi_purchase_order_id= $intPOID AND
        proi_product_id =$intProductID AND
        proi_bonus =$intBonus"
    );

    /*$this->setQuery(
    "SELECT proi_quantity1, proi_quantity2, proi_quantity3, proi_purchased1,proi_purchased2,proi_purchased3
    FROM purchase_order_item AS p
    WHERE p.proi_purchase_order_id =  $intPOID AND  p.proi_bonus ='0'"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;*/

    return true;

}

public function updateStatusClosed($po_item){
    $this->initialize('subkontrak_material');
    return $this->dbUpdate(array(
        'statusClosed' => 1),
    "id = $po_item");
    $this->initialize('purchase_order_item');
}

public function UpdatePurchasedItemVIew($ID,$idItem,$intDif1,$intDif2,$intDif3,$intBonus) {

    $this->setQuery(
"        UPDATE purchase_order_item
    SET proi_purchased1 = proi_purchased1 + $intDif1,
     proi_purchased2 = proi_purchased2 + $intDif2,
     proi_purchased3 = proi_purchased3 + $intDif3
     WHERE
     proi_purchase_order_id= '$ID' AND proi_product_id= '$idItem' AND proi_bonus =$intBonus"
    );
    return true;

}

public function getMaxItemsByPurchaseItemIDAndProdID($intPurchaseID,$intProductID) {

    $this->setQuery(
"        SELECT proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1, proi_purchased2, proi_purchased3, proi_unit1,proi_unit2,proi_unit3
    FROM purchase_order_item AS p
    WHERE p.proi_purchase_order_id =  $intPurchaseID AND p.proi_product_id = $intProductID AND  p.proi_bonus ='0'"
    );

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getMaxBonusItemsByPurchaseItemIDAndProdID($intPurchaseID,$intProductID) {

    $this->setQuery(
"        SELECT proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1, proi_purchased2, proi_purchased3, proi_unit1,proi_unit2,proi_unit3
    FROM purchase_order_item AS p
    WHERE p.proi_purchase_order_id =  $intPurchaseID AND p.proi_product_id = $intProductID AND  p.proi_bonus ='1'"
    );

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
   /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
    else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/



   $this->setQuery(
"        SELECT p.id,pro.id as product_id, proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1,proi_purchased2,proi_purchased3, proi_price, proi_discount1, proi_discount2, proi_discount3, proi_product_id, prob_title, proc_title, prod_title,proi_description,proi_unit1,proi_unit2,proi_unit3,proc_title,prod_code, prod_conv1, prod_conv2, prod_conv3
    FROM jw_product AS pro
    LEFT JOIN purchase_order_item AS p ON pro.id = p.proi_product_id
    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
    WHERE proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '0'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllPurchaseOrderItem(){
    $this->setQuery(
        "SELECT * from subkontrak_material"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function updatePurchaseOrderItemAfterDelPurch($prod_id, $qty, $po_id){
    $this->initialize('subkontrak_material');
    return $this->dbUpdate(array(
        'proi_quantity1' => $qty,
        'subkontrak_terpb' => 0),
    "no_pb = $po_id AND material = $prod_id");
    $this->initialize('purchase_order_item');
}

public function updatePurchaseOrderItem($po_id, $keterangan, $qty, $qtyTerima){
    return $this->dbUpdate(array(
        'proi_description' => $keterangan,
        'proi_quantity1' => $qty,
        'proi_quantity_terima' => $qtyTerima
    ),
    "id = $po_id"
    );
}

public function getAllPurchaseOrderItemById($idPurchase){
    $this->setQuery(
        "SELECT * , p.prch_po_id
        from purchase_item
        LEFT JOIN purchase as p ON p.id = $idPurchase
        WHERE prci_purchase_id = $idPurchase"
    );
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getBonusItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
    /* if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
     else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");*/



    $this->setQuery(
"        SELECT p.id,pro.id as product_id, proi_quantity1, proi_quantity2, proi_quantity3,proi_purchased1,proi_purchased2,proi_purchased3, proi_product_id, prob_title, proc_title, prod_title,proi_description,proi_unit1,proi_unit2,proi_unit3,proc_title,prod_code, prod_conv1, prod_conv2, prod_conv3
    FROM jw_product AS pro
    LEFT JOIN purchase_order_item AS p ON pro.id = p.proi_product_id
    LEFT JOIN jw_product_brand AS brand ON pro.prod_brand_id = brand.id
    LEFT JOIN jw_product_category AS cat ON pro.prod_category_id = cat.id
    WHERE proi_purchase_order_id =  $intPurchaseID AND proi_bonus = '1'
    ORDER BY p.id ASC"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getTotalItemsByPurchaseID($intPurchaseID,$strMode = 'ALL') {
    /*if($strMode == 'COUNT_PRICE') $this->dbSelect('id,proi_quantity,proi_price,proi_discount,proi_product_id',"proi_purchase_order_id = $intPurchaseID");
    else $this->dbSelect('',"proi_purchase_order_id = $intPurchaseID");


    return $this->getNumRows();*/
}

public function getLatestPrice($intProductID) {
    /*$this->setQuery("SELECT prod_currency FROM jw_product WHERE id = $intProductID");

    if($this->getNumRows() > 0) $intCurrencyID = $this->getNextRecord('Object')->prod_currency;
    else return false;

    $this->dbSelect('proi_price',"proi_product_id = $intProductID",'cdate DESC',1);

    if($this->getNumRows() > 0) return formatPriceCurrency($this->getNextRecord('Object')->proi_price,'BASE',$intCurrencyID);
    else return false;*/
}

public function add($intPurchaseID,$intProductID,$strProductDescription,$intQuantity1='0',$intQuantity2='0',$intQuantity3='0',$intUnit1='0',$intUnit2='0',$intUnit3='0',$intPrice,$intDiscount1='0',$intDiscount2='0',$intDiscount3='0',$flagBonus='0') {
    
    // Tambahan untuk membuat cdate item sama dengan cdate header
    // Tujuannya supaya di transaction history juga cdatenya sama
    $this->setQuery("SELECT cdate FROM purchase_order WHERE id = $intPurchaseID");
    if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
    else $strCDate = '';
    
    return $this->dbInsert(array(
        'proi_purchase_order_id' => $intPurchaseID,
        'proi_product_id' => $intProductID,
        'proi_description' => $strProductDescription,
        'proi_quantity1' => $intQuantity1,
        'proi_quantity2' => $intQuantity2,
        'proi_quantity3' => $intQuantity3,
        'proi_unit1' => $intUnit1,
        'proi_unit2' => $intUnit2,
        'proi_unit3' => $intUnit3,
        'proi_price' => $intPrice,
        'proi_discount1' => $intDiscount1,
        'proi_discount2' => $intDiscount2,
        'proi_discount3' => $intDiscount3,
        'proi_bonus' => $flagBonus,
        'cdate' => $strCDate));
}

public function addToSubkontrakMaterial($subkontrakID,$title,$qty,$keterangan,$cdate,$close,$satuan,$noPB){
    $this->initialize('subkontrak_material');
    return $this->dbInsert(array(
        'subkontrak_id' => $subkontrakID,
        'material' => $title,
        'jumlah' => $qty,
        'keterangan' => $keterangan,
        'cdate' => $cdate,
        'statusClosed' => $close,
        'satuan' => $satuan,
        'no_pb' => $noPB
    ));
}

public function addToPurchaseOrderItem($poID, $prodID, $keterangan, $qty, $cdate, $qtyTerima){
    
    $this->initialize('purchase_order_item');
    
    return $this->dbInsert(array(
        'proi_purchase_order_id' => $poID,
        'proi_product_id' => $prodID,
        'proi_description' => $keterangan,
        'cdate' => $cdate,
        'proi_quantity1' => $qty,
        'proi_quantity_terima' => $qtyTerima
    ));
}

public function updateTerPB($qty, $idsubkon, $idproduct){

    $this->initialize('subkontrak_material');

    return $this->dbUpdate(array(
        'subkontrak_terpb' => $qty),
    "subkontrak_id = $idsubkon AND material = $idproduct");
    $this->initialize('purchase_order_item');
}

public function editByID($intID,$intQuantity1,$intQuantity2,$intQuantity3,$intPrice,$intDiscount1,$intDiscount2,$intDiscount3) {

    return $this->dbUpdate(array(
            'proi_quantity1' => $intQuantity1,
            'proi_quantity2' => $intQuantity2,
            'proi_quantity3' => $intQuantity3,
            'proi_price' => $intPrice,
            'proi_discount1' => $intDiscount1,
            'proi_discount2' => $intDiscount2,
            'proi_discount3' => $intDiscount3),
        "id = $intID");
}

public function deleteByPurchaseID($intID) {
    return $this->dbDelete("proi_purchase_order_id = $intID");
}

public function deleteByID($intID) {
    return $this->dbUpdate(array(
        'proi_quantity1' => $intQuantity1,
        'proi_quantity2' => $intQuantity2,
        'proi_quantity3' => $intQuantity3,
        'proi_price' => $intPrice,
        'proi_discount1' => $intDiscount1,
        'proi_discount2' => $intDiscount2,
        'proi_discount3' => $intDiscount3),
    "id = $intID");
}

public function updateTerpurchase($intID,$qty,$qty2,$po_id) {
    $this->setQuery("UPDATE purchase_order_item SET
        proi_terpurchase = $qty, proi_terpurchase_bpb = $qty2
        WHERE id = $intID");
    $this->checkAutoClose($po_id,$intID);
}

public function checkAutoClose($intPurchaseOrderID,$intPurchaseOrderItemID) {
    $arrData = $this->Purchase_Order_GetItemsByPoiIDBaru($intPurchaseOrderID);
    // $bolClosed = FALSE; $bolSearched = FALSE;
    $bolSearched = FALSE;
    $arrBolClosed = [];
    foreach($arrData as $e) {
        $bolSearched = TRUE;
        // if(floatval($e['proi_quantity1']) > floatval($e['proi_terpurchase']) || floatval($e['proi_quantity_terima']) > floatval($e['proi_terpurchase_bpb'])) {
        //     $bolClosed = FALSE; break;
        // }
        if(floatval($e['proi_quantity1']) <= floatval($e['proi_terpurchase']) || floatval($e['proi_quantity_terima']) <= floatval($e['proi_terpurchase_bpb'])) {
            $bolClosed = TRUE; 
            array_push($arrBolClosed, TRUE);
            // break;
        } else {
            array_push($arrBolClosed, FALSE);
        }
    }
    
    $bolClosed = (compareData(FALSE,$arrBolClosed,'notin')) ? TRUE : FALSE ;
    if($bolClosed && $bolSearched) $intStatusClose = STATUS_FINISHED;        
    else $intStatusClose = STATUS_WAITING_FOR_FINISHING;
    $_CI =& get_instance();
    $_CI->load->model('Mpurchaseorder');
    // if($bolClosed && $bolSearched) {        
    $_CI->Mpurchaseorder->dbUpdate(array(
        'pror_status' => $intStatusClose),
        "id = $intPurchaseOrderID");
    // }else{
    //     $_CI->Mpurchaseorder->dbUpdate(array(
    //         'pror_status' => 3),
    //         "id = $intPurchaseOrderID");
    // }

    return ($bolClosed && $bolSearched);
    // return $bolClosed;
}

public function recalculateTerPb($subKonPB,$id){
    $this->setQuery(
        "SELECT SUM(poi.proi_quantity_terima) as terpb_now
        FROM purchase_order_item as poi
        LEFT JOIN purchase_order as po ON po.id = poi.proi_purchase_order_id
        WHERE poi.proi_product_id = $id AND po.idSubKontrak = $subKonPB AND pror_status >= 2"
        );
    
        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
        else return false;
}

}
/* End of File */