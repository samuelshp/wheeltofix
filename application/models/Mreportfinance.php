<?php

class Mreportfinance extends JW_Model {

public $_intWarehouseID;

public function __construct() {
	parent::__construct();	
	$this->_intWarehouseID = 1;
}

public function getInvoiceBelumLunasPrintout() {
    $this->setQuery(
"SELECT 'IVC' as trans, invo_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invo_grandtotal as grandtotal, i.invo_jatuhtempo as jatuhtempo, i.invo_status as status
FROM invoice i
	LEFT JOIN jw_salesman s ON (s.id = i.invo_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
UNION
SELECT 'IVR' as trans, invr_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invr_grandtotal as grandtotal, i.cdate as jatuhtempo, i.invr_status as status
FROM invoice_retur i
	LEFT JOIN jw_salesman s ON (s.id = i.invr_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invr_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
ORDER BY date DESC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
    
public function getCountInvoiceBelumLunas() {
	/*$this->setQuery(
"SELECT invo_code as code
FROM invoice i
	LEFT JOIN jw_salesman s ON (s.id = i.invo_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
UNION
SELECT invr_code as code
FROM invoice_retur i
	LEFT JOIN jw_salesman s ON (s.id = i.invr_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invr_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
");*/
	$this->setQuery(
"SELECT invo_code as code
FROM invoice i
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
UNION
SELECT invr_code as code
FROM invoice_retur i
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
");
    
	return $this->getNumRows();
    
}
    
public function getItemsInvoiceBelumLunas($intStartNo = -1,$intPerPage = -1) {
    
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "date DESC";
	else $strOrderBy = "date DESC LIMIT $intStartNo, $intPerPage";
	
	$this->setQuery(
"SELECT 'IVC' as trans, invo_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invo_grandtotal as grandtotal, i.invo_jatuhtempo as jatuhtempo, i.invo_status as status
FROM invoice i
	LEFT JOIN jw_salesman s ON (s.id = i.invo_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
UNION
SELECT 'IVR' as trans, invr_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invr_grandtotal as grandtotal, i.cdate as jatuhtempo, i.invr_status as status
FROM invoice_retur i
	LEFT JOIN jw_salesman s ON (s.id = i.invr_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invr_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
ORDER BY $strOrderBy");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}
    
public function searchInvoiceBelumLunasBySalesman($txtStart,$txtEnd) {

    $txtStart = formatDate2(str_replace('/','-',$txtStart),'Y-m-d');
    $txtEnd = formatDate2(str_replace('/','-',$txtEnd),'Y-m-d');
    
    $this->setQuery(
"SELECT 'IVC' as trans, invo_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invo_grandtotal as grandtotal, i.invo_jatuhtempo as jatuhtempo, i.invo_status as status
FROM invoice i
	LEFT JOIN jw_salesman s ON (s.id = i.invo_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invo_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
  AND i.cdate BETWEEN '$txtStart' AND '$txtEnd'
UNION
SELECT 'IVR' as trans, invr_code as code, i.cdate as date, d.deli_time as delivery_time,
	sals_code as salesman_code, sals_name as salesman_name, cust_name as customer_name,
	i.invr_grandtotal as grandtotal, i.cdate as jatuhtempo, i.invr_status as status
FROM invoice_retur i
	LEFT JOIN jw_salesman s ON (s.id = i.invr_salesman_id)
	LEFT JOIN jw_customer c ON (c.id = i.invr_customer_id)
	LEFT JOIN delivery_item di ON (di.deit_invoice_id = i.id)
		INNER JOIN delivery d ON (d.id = di.deit_delivery_id)
WHERE i.id not in (SELECT p.paym_invoice_id FROM payment p WHERE p.paym_type in (2,3))
  AND i.cdate BETWEEN '$txtStart' AND '$txtEnd'
ORDER BY date DESC");
    
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

}