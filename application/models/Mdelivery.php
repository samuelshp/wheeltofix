<?php
/*
PUBLIC FUNCTION:
- getItems(intStartNo,intPerPage)
- getItemByID(intID)
- getCount()
- add(intWarehouseID,intSalesmanID,strEkspedition,strDescription,strDate)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intStatus,selEditable,strEkspedition)
- editStatusByID(intID,intStatus)
- deleteByID(intID)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdelivery extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('delivery');
}

public function getItems($intStartNo = -1,$intPerPage = -1) {
	if($intStartNo == -1 || $intPerPage == -1) $strOrderBy = "d.cdate DESC, d.id DESC";
	else $strOrderBy = "d.cdate DESC, d.id DESC LIMIT $intStartNo, $intPerPage";
	
	$this->setQuery(
"SELECT d.id, deli_code, deli_description, ware_name, sals_name, sals_address, sals_phone,d.cdate, deli_status
FROM delivery AS d
LEFT JOIN jw_warehouse AS w ON deli_froMwarehouse = w.id
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
ORDER BY $strOrderBy");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID = 0) {
	if($intID == 0) $strWhere = "ORDER BY d.deli_code DESC, d.cdate DESC, d.id DESC LIMIT 0,1";
	else $strWhere = "WHERE d.id = $intID";
	
	$this->setQuery(
"SELECT d.id,deli_code, d.cdate AS deli_date, deli_description, deli_status,sals_code,sals_name,sals_address,sals_phone,ware_name,deli_progress,deli_editable,deli_ekspedition_number,deli_time
FROM delivery AS d
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
LEFT JOIN jw_warehouse AS w ON deli_froMwarehouse = w.id
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}

public function getCount() {
	$this->dbSelect('id');
	return $this->getNumRows();
}

public function add($intWarehouseID,$intSalesmanID,$strEkspedition,$strDescription,$strDateSent,$strDate) {
	return $this->dbInsert(array(
		'deli_branch_id' => $this->session->userdata('intBranchID'),
		'deli_froMwarehouse' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
        'deli_salesman' => $intSalesmanID,
        'deli_ekspedition_number' => $strEkspedition,
        'deli_description' => $strDescription,
        'deli_status' => '2',
        'deli_time' => formatDate2(str_replace('/','-',$strDateSent),'Y-m-d H:i'),
		'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d')
	));
}

public function editByID($intID,$strProgress,$intStatus) {
	return $this->dbUpdate(array(
		'deli_progress' => $strProgress,
		'deli_status' => $intStatus),
		"id = $intID");
}

public function editByID3($intID,$dateBack,$status) {
    return $this->dbUpdate(array(
            'deli_timereturn' => formatDate2(str_replace('/','-',$dateBack),'Y-m-d H:i'),
            'deli_status' => $status),
        "id = $intID");
}

public function editByID2($intID,$strDescription,$strProgress,$intStatus,$selEditable,$strEkspedition) {
	return $this->dbUpdate(array(
		'deli_description' => $strDescription,
		'deli_progress' => $strProgress,
        'deli_editable' => $selEditable,
        'deli_ekspedition_number' => $strEkspedition,
		'deli_status' => $intStatus),
		"id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'prch_status' => $intStatus),"id = $intID");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

public function getUserHistory($intUserID,$intPerPage) {
    $this->setQuery(
"SELECT a.id, a.cdate AS deli_date,deli_code, deli_ekspedition_number, deli_status, sals_name, ware_name
FROM delivery AS a
LEFT JOIN jw_warehouse AS w ON w.id = a.deli_froMwarehouse
LEFT JOIN jw_salesman AS s ON s.id = a.deli_salesman
WHERE (a.cby = $intUserID OR a.mby = $intUserID)
ORDER BY a.cdate DESC, a.id DESC LIMIT 0, $intPerPage");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllActiveDelivery($strKeyword = '') {
    if(!empty($strKeyword)) {
        $strKeyword = urldecode($strKeyword);
        $strWhere = " AND (deli_code LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%' OR sals_code LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT d.id AS deli_id, deli_code, deli_ekspedition_number,deli_description,sals_name
FROM delivery AS d
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
WHERE (deli_status IN (2)'.$strWhere.')
ORDER BY d.cdate DESC, d.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function searchByDeliverer($strDelivererName, $arrSearchDate, $intStatus) {
    $strWhere = '';
    if(!empty($strDelivererName)) {
    	$strDelivererName = urldecode($strDelivererName);
        $strWhere .= " AND (sals_name LIKE '%{$strDelivererName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (d.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (d.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (deli_status = {$intStatus})";

    $this->setQuery(
"SELECT d.id, deli_code, deli_description, ware_name, sals_name, sals_address, sals_phone, d.cdate, deli_status
FROM delivery AS d
LEFT JOIN jw_warehouse AS w ON deli_froMwarehouse = w.id
LEFT JOIN jw_salesman AS s ON deli_salesman = s.id
WHERE d.id > 0{$strWhere}
ORDER BY d.cdate DESC,d.id DESC ");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

}

/* End of File */