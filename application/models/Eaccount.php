<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Eaccount extends Eloquent 
{
    protected $table = 'jw_account';
    public $timestamps = false;
}