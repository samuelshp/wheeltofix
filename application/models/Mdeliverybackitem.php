<?php
/*
PUBLIC FUNCTION:
- add($intDeliveryBackID,$intInvoiceID,$intPayment)

PRIVATE FUNCTION:
- __construct()	
*/

class Mdeliverybackitem extends JW_Model {

// Constructor
public function __construct() {
	parent::__construct();
	$this->initialize('deliveryback_item');
}

public function add($intDeliveryBackID,$intInvoiceID,$intPayment) {
	// Tambahan untuk membuat cdate item sama dengan cdate header
	// Tujuannya supaya di transaction history juga cdatenya sama
	$this->setQuery("SELECT cdate FROM deliveryback WHERE id = $intDeliveryBackID");
	if($this->getNumRows() > 0) $strCDate = $this->getNextRecord('Object')->cdate;
	else $strCDate = '';

	return $this->dbInsert(array(
		'dbit_deba_id' => $intDeliveryBackID,
		'dbit_invoice_id' => $intInvoiceID,
		'dbit_invoice_payment' => $intPayment,
		'cdate' => $strCDate
	));
}

public function getItemsByDeliveryBackID($intDeliveryBackID,$strMode = 'ALL') {
    if($strMode == 'COUNT_PRICE')
        $this->dbSelect('dbit_invoice_id',"dbit_deba_id = $intDeliveryBackID");
    else
        $this->setQuery(
"SELECT di.id,invo_code, cust_name, cust_address, invo_arrive, dbit_invoice_payment,cust_city,i.id as invo_id
FROM deliveryback_item AS di
LEFT JOIN invoice AS i ON dbit_invoice_id = i.id
LEFT JOIN jw_customer AS c ON c.id = i.invo_customer_id
WHERE dbit_deba_id = $intDeliveryBackID
ORDER BY di.id ASC");
    //$this->dbSelect('',"deit_delivery_id = $intDeliveryID");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function editByID($intDeliveryBackID,$intID,$intPayment) {
    return $this->dbUpdate(array(
            'dbit_invoice_payment' => $intPayment),
        "dbit_deba_id = $intDeliveryBackID AND dbit_invoice_id = $intID");
}

public function getPaymentStatusByDeliveryBackAndItemID($intDeliveryBackID,$intID) {

    $this->setQuery(
"SELECT di.id,dbit_invoice_payment AS prev_pay
FROM deliveryback_item AS di
WHERE dbit_deba_id = $intDeliveryBackID AND dbit_invoice_id = $intID
ORDER BY di.id ASC");
    //$this->dbSelect('',"deit_delivery_id = $intDeliveryID");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function deleteByDebaID($intID) {
    return $this->dbDelete("dbit_deba_id = $intID");
}

}

/* End of File */