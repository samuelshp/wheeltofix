<?php
$_CI =& get_instance();
$strPageTitle = $arrTableListData['name'];
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

$intAllowAdd = $arrTableListData['allow_insert'];
$intAllowEdit = $arrTableListData['allow_update'];
$intAllowDelete = $arrTableListData['allow_delete'];

$arrHiddenFilterExclude = array();
include(APPPATH."/views/$strViewFolder/contentheader.php");
if(file_exists(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/browse.php'))
    require_once(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/browse.php'); ?>  
<div class="col-xs-12">
    <form method="post" action="<?=site_url('table/dataoperation/'.$arrTableListData['id'])?>" id="frmTable" target="_blank"><?php
include(APPPATH."/views/$strViewFolder/table/browsetoolbar.php"); ?>  
        <div class="clearfix"></div>
        <?=$strPage?>
        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
            <thead>
                <tr><?php
if($arrTableListData['type'] != 'report'): ?>  
                    <th class="cb no-excel no-print"><i class="fa fa-list"></i></th><?php
endif;
foreach($arrFieldList as $e):
    if($strOrderBy == $e['title']) {
        if($strOrderMode == 'DESC') { $strOrderPrint = ' <i class="fa fa-sort-asc pull-right"></i>'; $strOrderMode = 'ASC'; }
        else { $strOrderPrint = ' <i class="fa fa-sort-desc pull-right"></i>'; $strOrderMode = 'DESC'; }
        $strProlongedOrderMode = "&order_mode=$strOrderMode";
    } else {
        $strOrderPrint = ''; $strProlongedOrderMode = '';
    } ?>  
                    <th>
                        <span class="no-print">
                            <a href="<?=site_url('table/browse/'.$arrTableListData['id'].'?'.$strURLQuery."per_page=$intPerPage&order_by=".$e['title'].$strProlongedOrderMode)?>"><?=$e['name']?></a><?=$strOrderPrint?>
                        </span>
                        <span class="no-screen"><?=$e['name']?></span>
                    </th><?php
endforeach;
if($arrTableListData['type'] != 'report'): ?>  
                    <th class="action no-excel no-print"><?=$this->lang->jw('Action')?></th><?php
endif; ?>  
                </tr>
                <tr class="filterFields no-excel no-print"><?php
if($arrTableListData['type'] != 'report'): ?>  
                    <th>&nbsp;</th><?php
endif;
foreach($arrFieldList as $e):
    $arrClass = array(); $bolSecondField = FALSE;
    if($e['field_type'] == 'TEXT' && compareData($e['validation'], array('DATETIME', 'DATE', 'TIME'))) {
        $bolSecondField = $e['name'].' '.$this->lang->jw('End');
        switch ($e['validation']) {
            case 'DATE': $arrClass[] = 'jwDate'; break;
            case 'DATETIME': $arrClass[] = 'jwDateTime'; break;
            case 'TIME': $arrClass[] = 'jwTime'; break;
        }
    } ?>  
                    <th>
                        <input type="text" name="<?='search_'.$e['title']?>" class="form-control input-sm<?=!empty($arrClass) ? ' '.implode(' ', $arrClass) : ''?>" value="<?=$this->input->get('search_'.$e['title'])?>" placeholder="<?=$e['name']?>"><?php
    if(!empty($bolSecondField)): ?>  
                        <input type="text" name="<?='second_'.$e['title']?>" class="form-control input-sm<?=!empty($arrClass) ? ' '.implode(' ', $arrClass) : ''?>" value="<?=$this->input->get('second_'.$e['title'])?>" placeholder="<?=$bolSecondField?>"><?php
    endif; ?>  
                    </th><?php
endforeach;
    if($arrTableListData['type'] != 'report'): ?>  
                    <th class="no-excel">&nbsp;</th><?php
    endif; ?>  
                </tr>
            </thead>
            <tbody><?php
# Display data in the table
$i = 0;
if(!empty($arrTableData)):
    foreach($arrTableData as $e):
        if(strpos($arrTableListData['link'], 'adminpage') !== FALSE) $strViewLink = site_url(array(str_replace('adminpage/', '', $arrTableListData['link']), $e['id']));
        else $strViewLink = site_url(array('table/view',$arrTableListData['id'],$e['id'])); ?>  
                <tr data-url="<?=$strViewLink?>"><?php
        if($arrTableListData['type'] != 'report'): ?>  
                    <td class="cb no-excel no-print"><input type="checkbox" name="cbRecord<?=$i?>" id="cbRecord<?=$i?>" value="<?=$e['id']?>" /></td><?php
        endif;
        foreach($arrFieldList as $e2): ?>  
                    <td><?=$e[$e2['title']]?></td><?php
        endforeach;
        if($arrTableListData['type'] != 'report'): ?>  
                    <td class="action no-excel no-print"><?php
            if(!empty($intAllowEdit)): ?>  
                        <a href="<?=site_url(array('table/edit',$arrTableListData['id'],$e['id']))?>" target="_blank"><i class="fa fa-edit"></i></a><?php
            endif;
        endif; ?>  
                    </td>
                </tr><?php
        $i++;
    endforeach;
else: ?>  
                <tr class="info"><td class="noData" colspan="<?=count($arrFieldList) + 2?>"><?=$this->lang->jw('no_data')?></td></tr><?php
endif; ?>  
            </tbody>
        </table></div>
        <?=$strPage?>
    </form>
</div><?php
foreach($arrResultURLQuery as $i => $e) if(strpos($i, 'filter_') === 0 && compareData($i, $arrHiddenFilterExclude, 'notin') && $e != ''): ?>
        <input type="hidden" name="<?=$i?>" value="<?=$e?>"><?php
endif;