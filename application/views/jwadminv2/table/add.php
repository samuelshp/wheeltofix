<?php
$strPageTitle = $arrTableListData['name'];
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => site_url(array('table/browse',$intTableID))),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

# Privilege
$intAllowAdd = $arrTableListData['allow_insert'];
$intAllowEdit = $arrTableListData['allow_update'];
$intAllowDelete = $arrTableListData['allow_delete'];

# Change some variable between default table view and customized
if(empty($strProcessLink)) $strProcessLink = site_url('table/dataoperation/'.$arrTableListData['id']); 

include(APPPATH."/views/$strViewFolder/contentheader.php");
if(file_exists(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/add.php'))
    require_once(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/add.php'); ?>  

<form name="frmTable" id="frmTable" method="post" action="<?=$strProcessLink?>" class="col-xs-12" enctype="multipart/form-data">
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
    <div class="panel-body"><?php
// Display data in the table
foreach($arrFieldList as $e) if(!empty($arrTableData[$e['title']]) && $e['allow_null'] != 2) { // To hide unwanted field
    if(strpos($arrTableData[$e['title']],'InStRuCtIoN') !== FALSE)
        $strInstruction = extractInstruction($arrTableData[$e['title']]); 
    else $strInstruction = ''; 

    if($e['field_type'] == 'TEXTAREA' && compareData($e['validation'],array('MULTIVALUE', 'LIST'))): ?>  
        <div class="form-group">
            <label class="col-xs-12 control-label">
                <?=$e['name']?>&nbsp;
                <?=empty($e['allow_null']) ? '*' : ''?><?php 
        if(!empty($strInstruction)): ?>  
                <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
        endif; ?>  
            </label>
            <div class="clearfix"></div>
        </div>
        <?=$arrTableData[$e['title']]?><?php

    else: ?>  
        <div class="form-group<?=$e['allow_null'] == 3 ? ' hidden' : ''?>">
            <div class="col-sm-4 col-md-2 control-label">
                <?=$e['name']?>&nbsp;
                <?=empty($e['allow_null']) ? '*' : ''?><?php 
        if(!empty($strInstruction)): ?>  
                <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
        endif; ?>  
            </div>
            <div class="col-sm-8 col-md-10">
               <?=$arrTableData[$e['title']]?>  
            </div>
            <div class="clearfix"></div>
        </div><?php
    endif;
} ?>  
    </div>
</div>
<div class="form-group action">
    <button type="submit" name="smtProcessType" id="smtProcessType" value="Publish" class="btn btn-primary"><span class="fa fa-plus"></span></button>&nbsp;
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
    <a href="<?=site_url(array('table/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
</div><?php
$strLastPage = lastPageURL();
if(!empty($strLastPage)): ?>  
<input type="hidden" name="lastPage" value="<?=$strLastPage?>" /><?php
endif; ?>  
</form>