<?php
$strPageTitle = $arrTableListData['name'];
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => site_url(array('table/browse',$intTableID))),
    2 => array('title' => '<i class="fa fa-edit"></i> Edit', 'link' => '')
);

# Change some variable between default table view and customized
if(empty($strProcessLink)) {
    $strProcessLink = site_url('table/dataoperation/'.$arrTableListData['id']);
    $intAllowAdd = $arrTableListData['allow_insert'];
    $intAllowDelete = $arrTableListData['allow_delete'];
    $bolDefaultTableMode = true;
} else {
    $intAllowAdd = 0;
    $intAllowDelete = 0;
    $bolDefaultTableMode = false;
}

include(APPPATH."/views/$strViewFolder/contentheader.php");
if(file_exists(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/edit.php'))
    require_once(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/edit.php'); ?>  

<form name="frmTable" id="frmTable" method="post" action="<?=$strProcessLink?>" class="col-xs-12" enctype="multipart/form-data"><?php
$i = 0;
foreach($arrTableData as $e): ?>  
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><input type="checkbox" name="cbRecord<?=$i?>" id="cbRecord<?=$i?>" value="<?=$e['id']?>" checked="true" /> <i class="fa fa-edit"></i> Edit Item</h3></div>
    <div class="panel-body"><?php

if(!function_exists('___display')) {
    function ___display($arrTableDataElement,$arrFieldListElement,$strLanguage = '') { 
        if(strpos($arrTableDataElement[$arrFieldListElement['title']],'InStRuCtIoN') !== FALSE)
            $strInstruction = extractInstruction($arrTableDataElement[$arrFieldListElement['title']]); 
        else $strInstruction = ''; 

        if($arrFieldListElement['field_type'] == 'TEXTAREA' && compareData($arrFieldListElement['validation'],array('MULTIVALUE', 'LIST'))): ?>  
        <div class="form-group">
            <label class="col-xs-12 control-label">
                <?=$arrFieldListElement['name'].(!empty($strLanguage)? ' ('.$strLanguage.')' : '')?>&nbsp;
                <?=empty($arrFieldListElement['allow_null']) ? '*' : ''?><?php 
            if(!empty($strInstruction) && empty($strLanguage)): ?>  
                <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
            endif; ?>  
            </label>
            <div class="clearfix"></div>
        </div>
        <?=$arrTableDataElement[$arrFieldListElement['title']]?><?php

        else: ?>  
        <div class="form-group<?=$arrFieldListElement['allow_null'] == 3 ? ' hidden' : ''?>">
            <div class="col-sm-4 col-md-2 control-label">
                <?=$arrFieldListElement['name'].(!empty($strLanguage)? ' ('.$strLanguage.')' : '')?>&nbsp;
                <?=empty($arrFieldListElement['allow_null']) ? '*' : ''?><?php 
            if(!empty($strInstruction) && empty($strLanguage)): ?>  
                <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
            endif; ?>  
            </div>
            <div class="col-sm-8 col-md-10">
               <?=$arrTableDataElement[$arrFieldListElement['title']]?>  
            </div>
            <div class="clearfix"></div>
        </div><?php
        endif;
    }
} // End of ___display

    foreach($arrFieldList as $e2) if(!empty($e[$e2['title']]) && $e2['allow_null'] != 2): // To hide unwanted field
        ___display($e,$e2); 
        // Apply for other language
        foreach ($arrLangAvlb as $e3) if (!empty($e[$e2['title'].'_'.$e3['strData2']])) {
            $e2Temp = $e2;
            $e2Temp['title'] = $e2['title'].'_'.$e3['strData2'];
            ___display($e,$e2Temp,$e3['strData']);
        }
    endif; ?>  
    </div>
</div><?php
    $i++;
endforeach; ?>  
    
<div class="form-group action">
    <button type="submit" name="smtProcessType" id="smtProcessType" value="Save" class="btn btn-primary"><span class="fa fa-pencil-square-o"></span></button>&nbsp;
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
    <a href="<?=site_url(array('table/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
</div><?php
$strLastPage = lastPageURL();
if(!empty($strLastPage)): ?>  
<input type="hidden" name="lastPage" value="<?=$strLastPage?>" /><?php
endif; ?>  
</form>