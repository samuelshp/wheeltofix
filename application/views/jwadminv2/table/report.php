<?php
$strTemplate = $arrTableListData['report_template'];

# Replace Labels
if(preg_match_all('/\[.*\]/', $strTemplate, $arrLang) && count($arrLang[0]) > 0)
    foreach($arrLang[0] as $e)
        $strTemplate = str_replace($e, $this->lang->jw(str_replace(array('[', ']'), '', $e)), $strTemplate);

# Replace Session Variables
foreach($this->session->userdata as $i => $e) if(!is_array($i) && !is_array($e))
	$strTemplate = str_replace('{'.$i.'}', $e, $strTemplate);

# Replace Functions
$arrTemp = array();
if(preg_match_all('/\${\{.*\}\}/U', $strTemplate, $arrTemp) && count($arrTemp[0]) > 0)
    foreach($arrTemp[0] as $e)
        $strTemplate = str_replace($e, eval('return '.str_replace(array('${{', '}}'), '', $e).';'), $strTemplate);


# Replace Filters
foreach($arrFieldList as $e) {
    $arrClass = array(); $bolSecondField = FALSE;
    if($e['field_type'] == 'TEXT' && compareData($e['validation'], array('DATETIME', 'DATE', 'TIME'))) {
        $bolSecondField = $e['name'].' '.$this->lang->jw('End');
        switch ($e['validation']) {
            case 'DATE': $arrClass[] = 'jwDate'; break;
            case 'DATETIME': $arrClass[] = 'jwDateTime'; break;
            case 'TIME': $arrClass[] = 'jwTime'; break;
        }
    }
    $strFieldContent = '<input type="text" name="search_'.$e['title'].'" class="form-control input-sm'.(!empty($arrClass) ? ' '.implode(' ', $arrClass) : '').'" value="'.$this->input->get('search_'.$e['title']).'" placeholder="'.$e['name'].'">';
    if(!empty($bolSecondField))
    	$strFieldContent .= '<input type="text" name="second_'.$e['title'].'" class="form-control input-sm'.(!empty($arrClass) ? ' '.implode(' ', $arrClass) : '').'" value="'.$this->input->get('second_'.$e['title']).'" placeholder="'.$bolSecondField.'">';

    $strTemplate = str_replace('{filter_'.$e['title'].'}', $strFieldContent, $strTemplate);
}

# Display Looping
if(preg_match_all('/\{\{loop\}\}(.)*\{\{\/loop\}\}/Us', $strTemplate, $arrLang) && !empty($arrLang[0][0])) {
	$strLoopTemplate = str_replace(array('{{loop}}', '{{/loop}}'), '', $arrLang[0][0]);
	$strTemplate = str_replace(array('{{loop}}', '{{/loop}}'), '', $strTemplate);
	$strLoopDisplay = '';
	foreach($arrTableData as $i => $e) {
		if(preg_match_all('/\{.*\}/U', $strLoopTemplate, $arrLang) && count($arrLang[0]) > 0) {
			$strTempLoopTemplate = $strLoopTemplate;
		    foreach($arrLang[0] as $f) if(!in_array($f, array('{{loop}}', '{{/loop}}')))
		        $strTempLoopTemplate = str_replace($f, $e[str_replace(array('{', '}'), '', $f)], $strTempLoopTemplate);
		    $strLoopDisplay .= $strTempLoopTemplate;
		}
	}

	$strTemplate = str_replace($strLoopTemplate, $strLoopDisplay, $strTemplate);
}


?>  
<div class="col-xs-12">
	<form method="post" action="<?=site_url('table/dataoperation/'.$arrTableListData['id'])?>" id="frmTable">
		<div class="col pull-left no-print">
		    <div class="btn-toolbar">
		    	<div class="btn-group">
		    		<button type="submit" name="subSearch" value="Search" class="btn btn-default hidden" data-toggle="tooltip" title="<?=$this->lang->jw('Find')?>"><i class="fa fa-search"></i></button>
		    		<button type="button" name="subPrint" value="Print" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->jw('Print Document')?>"><i class="fa fa-print"></i></button>
		    		<button type="button" name="subPrint" value="Excel" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->jw('Print To Excel')?>"><i class="fa fa-file-excel-o"></i></button>
		    	</div>
		    </div>
		</div>
		<div class="clearfix"></div>
		<?=$strPage?>
		<div class="clearfix"></div>
 		<?=$strTemplate?>
	</form>
</div>