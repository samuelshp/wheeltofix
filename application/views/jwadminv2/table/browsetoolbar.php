<?php
if($arrTableListData['id'] == 53) {
    $_CI->load->helper('inflector');
    $_CI->load->model('Mdbvo');
    $_CI->Mdbvo->setQuery("SELECT DISTINCT tiny_category FROM tinydb ORDER BY tiny_category ASC");
    $arrTinyDBList = $_CI->Mdbvo->getQueryResult('Array');
    $arrHiddenFilterExclude[] = 'filter_tiny_category';
} ?>  
<div class="col pull-left no-print">
    <div class="btn-toolbar"><?php
if($arrTableListData['type'] != 'report'): ?>  
        <div class="btn-group">
            <button type="button" class="btn btn-default" id="btnCheckAll" data-toggle="tooltip" title="<?=$this->lang->jw('Check All')?>"><input type="checkbox" name="cbCheckAll" id="cbCheckAll" /> <?=$this->lang->jw('FORM-check_all')?></button>
            <button type="submit" name="smtProcessType" value="View" class="btn btn-success" data-toggle="tooltip" title="<?=$this->lang->jw('Read')?>"><i class="fa fa-eye"></i></button><?php 
    if(!empty($intAllowEdit)): ?>  
            <button type="submit" name="smtProcessType" value="Edit" class="btn btn-warning" data-toggle="tooltip" title="<?=$this->lang->jw('Edit')?>"><i class="fa fa-edit"></i></button><?php 
    endif; 
    if(!empty($intAllowDelete)): ?>  
            <button type="submit" name="smtProcessType" value="Delete" class="btn btn-danger" data-toggle="tooltip" title="<?=$this->lang->jw('Delete')?>"><i class="fa fa-trash-o"></i></button><?php 
    endif; ?>  
        </div><?php
endif; ?>  
        <div class="btn-group"><?php 
if(!empty($intAllowAdd)):
    if($this->input->get('dependant') != ''): ?>  
            <a href="<?=site_url(array('table/add',$arrTableListData['id'])).'?dependant='.$this->input->get('dependant').'&filter_'.$this->input->get('dependant').'='.$this->input->get('filter_'.$this->input->get('dependant'))?>" class="btn btn-primary"><i class="fa fa-plus"></i></a><?php 
    else: ?>  
            <a href="<?=site_url(array('table/add',$arrTableListData['id']))?>" class="btn btn-primary" data-toggle="tooltip" title="<?=$this->lang->jw('Create')?>"><i class="fa fa-plus"></i></a><?php 
    endif;
endif; ?>  
            <button type="submit" name="subSearch" value="Search" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->jw('Find')?>"><i class="fa fa-search"></i></button><?php
    if($this->input->get('subSearch') != ''): ?>  
            <button type="button" name="subPrint" value="Print" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->jw('Print Document')?>"><i class="fa fa-print"></i></button>
            <button type="button" name="subPrint" value="Excel" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->jw('Print To Excel')?>"><i class="fa fa-file-excel-o"></i></button><?php
    endif; ?>  
        </div>
        <div class="btn-group">
            <div class="form-group"><?php
# Tiny DB Dropdown Filter
if(!empty($arrTinyDBList)): ?>  
                <select name="filter_tiny_category" class="form-control dropdown-toolbar" placeholder="<?=$this->lang->jw('FORM-select')?>"><?php
    foreach($arrTinyDBList as $e): ?>  
                    <option value="<?=$e['tiny_category']?>"<?=$this->input->get('filter_tiny_category') == $e['tiny_category'] ? ' selected' : ''?>><?=humanize($e['tiny_category'])?></option><?php
    endforeach; ?>  
                </select><?php
endif; ?>  
            </div>
            
        </div>
    </div>
</div><?php 
if(!empty($arrPageParams)): ?>  
<div class="col pull-right text-right no-print">
    <div class="form-inline">
        <small class="label label-info"><?=$this->lang->jw('Data')?> <?=$arrPageParams['intStart'] + 1?> - <?=$arrPageParams['intStart'] + $arrPageParams['intCount']?> (<?=$this->lang->jw('Total')?> <?=$arrPageParams['intTotal']?>)</small>
        <small>Show: </small>
        <select name="per_page" class="form-control dropdown-toolbar" placeholder="<?=$this->lang->jw('Show')?>">
            <option value="10"<?=($intPerPage == 10) ? ' selected' : ''?>>10</option>
            <option value="25"<?=($intPerPage == 25) ? ' selected' : ''?>>25</option>
            <option value="50"<?=($intPerPage == 50) ? ' selected' : ''?>>50</option>
            <option value="100"<?=($intPerPage == 100) ? ' selected' : ''?>>100</option>
            <option value="0"<?=($intPerPage == 0) ? ' selected' : ''?>><?=$this->lang->jw('All Data')?></option>
        </select>
    </div>
</div><?php 
endif; ?>  