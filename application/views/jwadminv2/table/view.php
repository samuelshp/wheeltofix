<?php
$strPageTitle = $arrTableListData['name'];
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => site_url(array('table/browse',$intTableID))),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

# Privilege
$intAllowAdd = $arrTableListData['allow_insert'];
$intAllowEdit = $arrTableListData['allow_update'];
$intAllowDelete = $arrTableListData['allow_delete'];

include(APPPATH."/views/$strViewFolder/contentheader.php");
if(file_exists(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/view.php'))
    require_once(APPPATH.'views/'.$this->config->item('jw_style').'/table/custom/'.$arrTableListData['title'].'/view.php'); ?>  

<div class="col-xs-12"><?php
$i = 0;
foreach($arrTableData as $e): ?>  
    <div class="panel panel-primary tableView" data-id="<?=$e['id']?>">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-eye"></i> View Item</h3></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-md-6"><?php
if(!function_exists('___display')) {
    function ___display($arrTableDataElement,$arrFieldListElement,$strLanguage = '') { 
        if(is_string($arrTableDataElement[$arrFieldListElement['title']]) &&
            strpos($arrTableDataElement[$arrFieldListElement['title']],'InStRuCtIoN') !== FALSE)
            $strInstruction = extractInstruction($arrTableDataElement[$arrFieldListElement['title']]); 
        else $strInstruction = ''; 

        if($arrFieldListElement['field_type'] == 'TEXTAREA' && $arrFieldListElement['validation'] == 'MULTIVALUE'): ?>  
                    <div class="form-group">
                        <label>
                            <?=$arrFieldListElement['name'].(!empty($strLanguage)? ' ('.$strLanguage.')' : '')?><?php 
            if(!empty($strInstr1uction) && empty($strLanguage)): ?>  
                            <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
            endif; ?>  
                        </label>
                    </div><?php
            $arrTempFieldList = json_decode($arrFieldListElement['description'], true);
            foreach($arrTempFieldList as $i => $e): ?>  
                    <div class="form-group">
                        <label><?=$e['name']?></label>
                        <div class="wrapper">
                            <?=translateDataIntoReadableStatements($e, !empty($arrTableDataElement[$i]) ? $arrTableDataElement[$i] : '')?>
                            <div class="clearfix"></div>
                        </div>
                    </div><?php
            endforeach;
        else: ?>  
                    <div class="form-group">
                        <label>
                            <?=$arrFieldListElement['name'].(!empty($strLanguage)? ' ('.$strLanguage.')' : '')?><?php 
            if(!empty($strInstr1uction) && empty($strLanguage)): ?>  
                            <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strInstruction?>"></i><?php 
            endif; ?>  
                        </label>
                        <div class="wrapper"><?php
            if($arrFieldListElement['field_type'] == 'TEXTAREA' && $arrFieldListElement['validation'] == 'LIST'):
                $arrTempFieldList = json_decode($arrFieldListElement['description'], true); ?>  
                            <div class="table-responsive">
                                <table class="table jwTableDescriptionList">
                                    <thead>
                                        <tr>
                                            <th class="counter"></th><?php
                foreach($arrTempFieldList as $e): ?>  
                                            <th><?=$e['name']?></th><?php
                endforeach; ?>  
                                        </tr>
                                    </thead>
                                    <tbody><?php
                foreach($arrTableDataElement[$arrFieldListElement['title']] as $e): ?>  
                                        <tr>
                                            <td class="counter"></td><?php
                    foreach($arrTempFieldList as $j => $f): ?>  
                                            <td><?=translateDataIntoReadableStatements($f, $e[$j])?></td><?php
                    endforeach; ?>  
                                        </tr><?php
                endforeach; ?>  
                                    </tbody>
                                </table>
                            </div><?php
            else: ?>  
                            <?=$arrTableDataElement[$arrFieldListElement['title']]?><?php
            endif; ?>  
                            <div class="clearfix"></div>
                        </div>
                    </div><?php
        endif;
    }
} // End of ___display

foreach($arrFieldList as $e2) if(compareData($e2['allow_null'],array(2, 3),'notin')): // To hide unwanted field

    ___display($e,$e2); 
    // Apply for other language
    foreach ($arrLangAvlb as $e3) if (!empty($e[$e2['title'].'_'.$e3['strData2']])) {
        $e2['title'] = $e2['title'].'_'.$e3['strData2'];
        ___display($e,$e2,$e3['strData']);
    }
endif; ?>  
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>ID</label>
                        <div class="wrapper"><?=$e['id']?></div>
                    </div><?php
if(!empty($arrTableListData['link'])): ?>  
                    <div class="form-group">
                        <label>Link</label>
                        <div class="wrapper"><?=site_url($arrTableListData['link'].'/'.$e['id'])?></div>
                    </div><?php
endif;
if(!empty($arrDependantList)): ?>  
                    <div class="form-group">
                        <label>Dependant</label>
                        <div class="wrapper">
                            <div class="table-responsive">
                                <table class="table jwTableDescriptionList">
                                    <tbody><?php
    foreach($arrDependantList as $e2): ?>  
                                        <tr>
                                            <td class="counter"></td>
                                            <td><a href="<?=site_url(array('table/browse', $e2['table_id'])).'?dependant='.$e2['filter_name'].'&filter_'.$e2['filter_name'].'='.$e['id']?>"><?=$e2['table_name']?> (<?=$e2['field_name']?>)</a></td>
                                        </tr><?php
    endforeach; ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><?php
endif;
foreach($arrFieldList as $e2) if($e2['allow_null'] == 2): // To hide unwanted field
    ___display($e,$e2); 
    // Apply for other language
    foreach ($arrLangAvlb as $e3) if (!empty($e[$e2['title'].'_'.$e3['strData2']])) {
        $e2['title'] = $e2['title'].'_'.$e3['strData2'];
        ___display($e,$e2,$e3['strData']);
    }
endif;
if(!empty($e['cdate']) && strpos($e['cdate'], '0000') === false): ?>  
                    <div class="form-group">
                        <label>Created</label>
                        <div class="wrapper"><?=formatDate2($e['cdate'],'d F Y H:i:s')?></div>
                    </div><?php
endif; 
if(!empty($e['mdate']) && strpos($e['mdate'], '0000') === false): ?>  
                    <div class="form-group">
                        <label>Modified</label>
                        <div class="wrapper"><?=formatDate2($e['mdate'],'d F Y H:i:s')?></div>
                    </div><?php
endif; ?>  
                </div>
            </div>
        </div>
    </div><?php
    $i++;
endforeach; ?>  
</div>