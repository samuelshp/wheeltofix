<script>
$(function() {
	var $element = $("input[name=prod_title]");
	$element.autocomplete({
		minLength:3,
		delay:500,
		source: function(request,response) {
			$.ajax({
				url: "<?=site_url('purchase_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $element.val(),
				beforeSend: function() {
					$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
				},
				complete: function() {
					$("#loadItem").html('');
				},success: function(data){
					var xmlDoc = $.parseXML(data);
					var xml = $(xmlDoc);
					$arrSelectedItem = xml;
					var display=[];
					$.map(xml.find('Product').find('item'),function(val,i){
						var intID = $(val).find('id').text();
						var strName=$(val).find('strNameWithPrice').text();
						var strKode=$(val).find('prod_code').text();
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					});
					response(display);
				}
			});
		},
		select: function(event, ui) {
		}
	});
});
</script>