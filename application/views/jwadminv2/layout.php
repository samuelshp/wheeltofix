<html lang="en">
<head>
    <title><?=$this->template->title->default('Administrator - '.$this->config->item('jw_website_name'))?></title>
    <meta charset="utf-8" />
    <meta name="author" content="tobsite.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php echo $this->template->meta; ?>  
    <?php echo $this->template->stylesheet; ?>  
    <link rel="shortcut icon" href="<?=base_url('upload/icon-tobsite.png')?>" />
    <link rel="stylesheet" href="<?= base_url('asset/compiled/css/app.css') ?>" />
    <script src="<?=base_url('asset/jquery/jquery.min.js')?>" type="text/javascript"></script>
</head>

<style type="text/css"><?php 
    if(is_file("asset/$strViewFolder/$strViewFile.css")) include("asset/$strViewFolder/$strViewFile.css"); ?>  
</style>

<body data-page="<?=$strViewFile?>">
<div id="wrapper">
    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top no-print" role="navigation">
        <?=$this->template->header?>  
        <?=$this->template->navigation?>  
    </nav>

    <div id="page-wrapper">
        <!-- Content -->
        <article class="row card">
            <?=$this->template->content?>  
        </article>

        <footer class="no-print">
            <?=$this->template->footer?>  
        </footer>
    </div>

</div>

<?=$this->template->javascript?>  
<!--[if lt IE 9]>
<script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?= base_url('asset/compiled/js/app.js') ?>"></script>
<script type="text/javascript"><?php
include_once("asset/$strViewFolder/setting.js"); ?>  
$(document).ready(function() {<?php
    include_once("asset/$strViewFolder/layout.js");
    if(is_file("asset/$strViewFolder/$strViewFile.js")) include_once("asset/$strViewFolder/$strViewFile.js"); ?>  
});</script>

<?=$this->template->customScript?>  

</body>
</html>