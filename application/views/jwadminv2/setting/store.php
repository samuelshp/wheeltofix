            <h3>Website <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strStoreInstruction?>"></i></h3>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Website Name</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_WEBSITE_NAME" value="<?=$strWebsiteName?>" class="form-control required text" maxlength="32" placeholder="Website Name" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Website Motto</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_WEBSITE_MOTTO" value="<?=$strWebsiteMotto?>" class="form-control required text" maxlength="128" placeholder="Website Motto" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Website Logo</div>
                <div class="col-sm-8 col-md-10"><div class="input-group">
                    <input type="text" name="JW_WEBSITE_LOGO" value="<?=$strWebsiteLogo?>" class="form-control text" maxlength="255" placeholder="Website Logo" id="JW_WEBSITE_LOGO" />
                    <span class="input-group-btn"><a class="btn btn-info btnFileBrowse" href="<?=base_url('asset/responsivefilemanager/dialog.php').'?type=1&field_id=JW_WEBSITE_LOGO'?>" type="button">Browse</a></span>
                </div></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Website Icon</div>
                <div class="col-sm-8 col-md-10"><div class="input-group">
                    <input type="text" name="JW_WEBSITE_ICON" value="<?=$strWebsiteIcon?>" class="form-control text" maxlength="255" placeholder="Website Icon" id="JW_WEBSITE_ICON" />
                    <span class="input-group-btn"><a class="btn btn-info btnFileBrowse" href="<?=base_url('asset/responsivefilemanager/dialog.php').'?type=1&field_id=JW_WEBSITE_ICON'?>" type="button">Browse</a></span>
                </div></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group hidden"><div class="col-sm-4 col-md-2 control-label">Products Per Page</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_PRODUCT_COUNT" value="<?=$intProductCount?>" class="form-control required number text" maxlength="3" placeholder="Products Per Page" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Items Per Page</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_POST_COUNT" value="<?=$intPostCount?>" class="form-control required number text" maxlength="3" placeholder="Posts Per Page" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group hidden"><div class="col-sm-4 col-md-2 control-label">Homepage Item Count</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_HOME_COUNT" value="<?=$intHomeCount?>" class="form-control required number text" maxlength="3" placeholder="Items Displayed At Homepage" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group hidden"><div class="col-sm-4 col-md-2 control-label">Sidebar Item Count</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_SIDEBAR_COUNT" value="<?=$intSidebarCount?>" class="form-control required number text" maxlength="3" placeholder="Items Displayed At Sidebar" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group hidden"><div class="col-sm-4 col-md-2 control-label">UI Language</div>
                <div class="col-sm-8 col-md-10"><?=$strLanguage?></div>
            </div>
            <p class="spacer">&nbsp;</p>