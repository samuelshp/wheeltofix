        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">UI Theme <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strThemeInstruction?>"></i></h3></div>
            <div class="panel-body"><?php
// Display data in the table
$i = 0;
if(!empty($arrThemeProperties)) foreach($arrThemeProperties as $e) if($e['strType'] == 'UI'): ?>  
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 theme"><label>
                    <div class="i"><img src="<?=$e['strImage']?>" /></div>
                    <h4><input type="radio" name="radThemeUI" value="<?=$e['strTitle']?>"<?=($e['strTitle'] == $strUITheme)? ' checked="checked"' : '' ?> /> <?=$e['strTitle']?></h4>
                </label></div><?php
    $i++;
endif; ?>  
                
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Admin Theme <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strThemeInstruction?>"></i></h3></div>
            <div class="panel-body"><?php
// Display data in the table
$i = 0;
if(!empty($arrThemeProperties)) foreach($arrThemeProperties as $e) if($e['strType'] == 'ADM'): ?>  
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 theme"><label>
                    <div class="i"><img src="<?=$e['strImage']?>" /></div>
                    <h4><input type="radio" name="radThemeAdmin" value="<?=$e['strTitle']?>"<?=($e['strTitle'] == $strAdminTheme)? ' checked="checked"' : '' ?> /> <?=$e['strTitle']?></div>
                </label></h4><?php
    $i++;
endif; ?>  
            </div>
        </div>