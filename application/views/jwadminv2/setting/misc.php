            <h3>Miscellaneous <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strMiscInstruction?>"></i></h3>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Invoice Print Type</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_PRINT_INVOICE_TYPE" value="<?=$strPrintInvoiceType?>" class="form-control text" maxlength="64" placeholder="Invoice Print Type" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
			<div class="form-group"><div class="col-sm-4 col-md-2 control-label">Cashier Print Type</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_PRINT_CASHIER_TYPE" value="<?=$strPrintCashierType?>" class="form-control text" maxlength="64" placeholder="Cashier Print Type" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
			<div class="form-group"><div class="col-sm-4 col-md-2 control-label">Barcode Print Type</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_PRINT_BARCODE_TYPE" value="<?=$strPrintBarcodeType?>" class="form-control text" maxlength="64" placeholder="Cashier Print Type" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Price Formula</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_PRICE_FORMULA" value="<?=$strPriceFormula?>" class="form-control text" placeholder="Price Formula" /></div>
            </div>
            <p class="spacer">&nbsp;</p>