            <h3>Owner <i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="<?=$strOwnerInstruction?>"></i></h3>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Name</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_OWNER_NAME" value="<?=$strOwnerName?>" class="form-control required text" maxlength="64" placeholder="Owner Name" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Owner Email</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_OWNER_EMAIL" value="<?=$strOwnerEmail?>" class="form-control required email text" maxlength="128" placeholder="Owner Email" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Owner Phone</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_OWNER_PHONE" value="<?=$strOwnerPhone?>" class="form-control text" maxlength="24" placeholder="Owner Phone" /></div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label">Owner Address</div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_OWNER_ADDRESS" value="<?=$strOwnerAddress?>" class="form-control text" maxlength="256" placeholder="Owner Address" /></div>
            </div><?php
for ($i=1; $i <= $intDataCount; $i++):
    $strOwnerData = 'strOwnerData'.$i;
    $strOwnerDataLabel = 'strOwnerData'.$i.'Label';
    $strOwnerDataType = 'strOwnerData'.$i.'Type';
    if(!empty($$strOwnerDataLabel)):
        if(!empty($$strOwnerDataType) && $$strOwnerDataType == 'file'): ?>  
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label"><?=$$strOwnerDataLabel?></div>
                <div class="col-sm-8 col-md-10"><div class="input-group">
                    <input type="text" name="JW_OWNER_DATA<?=$i?>" value="<?=$$strOwnerData?>" class="form-control text" maxlength="255" placeholder="<?=$$strOwnerDataLabel?>" id="<?=$strOwnerData?>" />
                    <span class="input-group-btn"><a class="btn btn-info btnFileBrowse" href="<?=base_url('asset/responsivefilemanager/dialog.php').'?type=2&field_id='.$strOwnerData?>" type="button">Browse</a></span>
                </div></div>
            </div><?php
        else: ?>  
            <p class="spacer">&nbsp;</p>
            <div class="form-group"><div class="col-sm-4 col-md-2 control-label"><?=$$strOwnerDataLabel?></div>
                <div class="col-sm-8 col-md-10"><input type="text" name="JW_OWNER_DATA<?=$i?>" value="<?=$$strOwnerData?>" class="form-control text" maxlength="256" placeholder="<?=$$strOwnerDataLabel?>" /></div>
            </div><?php
        endif;
    endif;
endfor; ?>  
            <p class="spacer">&nbsp;</p>