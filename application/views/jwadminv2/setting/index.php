<?php
$strPageTitle = 'Settings';
$strPageDescription = '';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-gear"></i> '.$strPageTitle, 'link' => ''),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<div class="col-xs-12">
    <ul class="nav nav-tabs" style="margin-bottom: 15px;"><?php
$strClass = '';
foreach($arrAvailability as $k => $v) if($v): 
    switch($k) {
        case 'store': $strTitle = 'Perusahaan'; break;
        case 'owner': $strTitle = 'Lain-lain'; break;
        case 'meta': $strTitle = 'Meta Data'; break;
        case 'theme': $strTitle = 'Theme'; break;
        case 'file': $strTitle = 'File'; break;
    } ?>  
        <li<?=$strClass?>><a href="#<?=$k?>" data-toggle="tab"><?=$strTitle?></a></li><?php
    if(!empty($strClass)) $strClass = '';
endif; ?>    
    </ul><?php
# Don't give action on form tag below to preserve hash code in url ?>  
    <form name="frmTable" id="frmTable" method="post">
    <div id="myTabContent" class="tab-content"><?php
$strClass = ' in';
foreach($arrAvailability as $k => $v) if($v): ?>  
        <div class="tab-pane fade<?=$strClass?>" id="<?=$k?>"><?php
    switch($k) {
        case 'store': include_once('store.php'); break;
        case 'owner': include_once('owner.php'); break;
        case 'meta': include_once('meta.php'); break;
        case 'theme': include_once('theme.php'); break;
    } ?>  
        </div><?php
    $strClass = '';
endif; ?>  
        <p class="spacer">&nbsp;</p>
    </div>
    <div class="form-group action" style="margin-top:25px;">
        <button type="submit" name="smtProcessType" id="smtProcessType" value="Save" class="btn btn-primary">Save</button>&nbsp;
        <a href="<?=site_url()?>" class="btn btn-default">Back</a>
    </div>
    </form>
</div>