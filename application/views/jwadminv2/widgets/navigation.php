<div class="collapse navbar-collapse" id="mainMenu">
    <ul class="nav navbar-nav side-nav">
        <li class="active"><a href="<?=site_url('main')?>"><i class="fa fa-dashboard"></i> Dashboard</a></li><?php
foreach($arrLinkList as $e) if($e['link_parent'] == 0 && compareData($e['link_place'],array(0,2))): ?>  
        <li class="dropdown">
            <?=translateLink($e)?>
            <ul class="dropdown-menu"><?php
    foreach($arrLinkList as $e2) if($e2['link_parent'] == $e['id']): ?>  
                <li>
                    <?=translateLink($e2)?>
                    <ul class="dropdown-menu always-open"><?php
        foreach($arrLinkList as $e3) if($e3['link_parent'] == $e2['id']): ?>  
                        <li><?=translateLink($e3)?></li><?php
        endif; ?>  
                    </ul>
                </li><?php
    endif; ?>  
            </ul>
        </li><?php
endif; ?>  
    </ul>
    <ul class="nav navbar-nav navbar-right navbar-user"><?php
foreach($arrLinkList as $e) if($e['link_parent'] == 0 && compareData($e['link_place'],array(1,2))): ?>  
        <li class="dropdown">
            <?=translateLink($e)?>
            <ul class="dropdown-menu"><?php
    foreach($arrLinkList as $e2) if($e2['link_parent'] == $e['id']): ?>  
                <li><?=translateLink($e2)?></li><?php
    endif; ?>  
            </ul>
        </li><?php
endif;
if(!empty($arrAlert)): ?>  
        <li class="dropdown alerts-dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-inbox"></i> Alerts <span class="badge"><?=count($arrAlert)?></span> <b class="caret"></b></a>
            <ul class="dropdown-menu"><?php
    foreach($arrAlert as $e): ?>  
                <li><a href="<?=site_url($e['link'])?>">
                    New <span class="text text-info"><?=$e['name']?></span><?php 
        if($e['count'] > 0): ?>  
                    <span class="badge"><?=$e['count']?></span><?php 
        endif; ?>  
                </a></li><?php
    endforeach; ?>  
            </ul>
        </li><?php
endif; ?>  
        <li class="dropdown user-dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-location-arrow"></i> <?=$this->session->userdata('strAdminUserName')?> <b class="caret"></b></a>
            <ul class="dropdown-menu"><?php
// Language 
/*for($i = 0; $i < count($arrLangAvlb); $i++): 
  if($arrLangAvlb[$i]['strKey'] != $this->session->userdata('jw_admin_language')): ?>  
        <li><a href="<?=site_url('main/change_language/'.$arrLangAvlb[$i]['strKey'])?>"><i class="fa fa-language"></i> <span class="label label-default"><?=$arrLangAvlb[$i]['strData']?></span></a></li><?php
  else: ?>   
    <li><a href="#"><i class="fa fa-language"></i> <span class="label label-primary"><?=$arrLangAvlb[$i]['strData']?></span></a></li><?php
  endif;  
endfor;*/ ?>  
                <!-- <li class="divider"></li> -->
                <li><a href="<?=site_url('/adminpage/administrator/edit')?>"><i class="fa fa-key"></i> My Account</a></li>
                <li><a href="<?=site_url('/adminpage/administrator/getOut')?>"><i class="fa fa-power-off"></i> Log Out</a></li>
            </ul>
        </li>
    </ul>
</div><!-- /.navbar-collapse -->