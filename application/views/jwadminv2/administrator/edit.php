<?php
$strPageTitle = $this->lang->jw('my_account');
$strPageDescription = '';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-key"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
<form method="post" action="<?=site_url('administrator/edit')?>" id="frmTable">
    <h3><?=$this->lang->jw('change_password')?></h3>
    <div class="form-group">
        <label><?=$this->lang->jw('username')?></label>
        <input class="form-control input-sm" disabled="true" value="<?=$arrAdminLogin['adlg_login']?>" />
    </div>
    <div class="form-group">
        <label><?=$this->lang->jw('old_password')?></label>
        <input type="password" name="txtOldPassword" id="txtOldPassword" class="required form-control input-sm" placeholder="<?=$this->lang->jw('type_old_password')?>" />
    </div>
    <div class="form-group">
        <label><?=$this->lang->jw('password')?></label>
        <input type="password" name="txtPassword" id="txtPassword" class="required form-control input-sm" minlength="6" placeholder="<?=$this->lang->jw('type_password')?>" />
    </div>
    <div class="form-group">
        <label><?=$this->lang->jw('confirm_password')?></label>
        <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" class="required form-control input-sm" placeholder="<?=$this->lang->jw('type_confirm_password')?>" />
    </div>
    <button type="submit" name="btnSubmit" value="submit" class="btn btn-primary btn-sm"><?=$this->lang->jw('change')?></button>
</form>
</div>