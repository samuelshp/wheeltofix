<div class="col-xs-12 col-sm-6 col-lg-4" id="loginBox">
	<h1><?php
if($this->config->item('jw_website_logo') != ''): ?>  
		<img src="<?=$this->config->item('jw_website_logo')?>" title="<?=$this->config->item('jw_website_motto')?>" alt="<?=$this->config->item('jw_website_name')?>" /><?php
else: ?>  
		<span title="<?=$this->config->item('jw_website_motto')?>"><?=$this->config->item('jw_website_name')?></span><?php
endif; ?>
		<br>
	</h1><?php
if(!empty($strMessage)): ?>  
    <div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?=$strMessage?></div><?php
endif; ?>  
    <form role="form" method="post" class="box box-bordered" action="<?=site_url('administrator/getIn')?>">
		<p class="text-center"><?=$this->lang->jw('2-MSG-login_first')?></p>
		<div class="form-group"><input type="text" name="username" class="form-control input-sm" rel="popover" placeholder="<?=$this->lang->jw('username')?>" value="<?=repopulate('username')?>" id="username" autofocus /></div>
		<div class="form-group"><input type="password" name="password" class="form-control input-sm" placeholder="<?=$this->lang->jw('password')?>" value="" id="pwdPassword" /></div>
		<div class="form-group"><p><?php if($this->session->userdata('intLoginAttempt') > 2) { require_once('script/recaptchalib.php'); echo recaptcha_get_html($publickey); } ?></p></div>
		<div class="form-group" style="margin-top:15px;"><button type="submit" value="submit" class="btn btn-primary btn-sm"><?=$this->lang->jw('login')?></button></div>
	</form>
</div>