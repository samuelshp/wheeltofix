<?php
$strPageTitle = 'Administrator Privilege';
$strPageDescription = '';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-table"></i> Administrator Privilege', 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<form method="post" action="<?=site_url('administrator/priviledge')?>" class="col-xs-12" id="frmTable">
    <div class="table-responsive"><table class="table table-bordered table-hover" id="adminPrivilege">
    <thead>
    <tr>
        <th rowspan="2">Page / Menu</th><?php
foreach($arrAdminPriviledge as $e2): ?>  
        <th colspan="5"><?=$e2['strData']?></th><?php
endforeach; ?>  
    </tr>
    <tr><?php
for($i = 0; $i < count($arrAdminPriviledge); $i++): ?>  
        <th class="cb"><i class="fa fa-eye"></i></th>
        <th class="cb"><i class="fa fa-plus"></i></th>
        <th class="cb"><i class="fa fa-edit"></i></th>
        <th class="cb"><i class="fa fa-trash-o"></i></th>
        <th class="cb"><i class="fa fa-thumbs-o-up"></i></th><?php
endfor; ?>  
     </tr>
     </thead>
    <tbody><?php
// Display data in the table
$i = 0;
foreach($arrAdminLinkList as $e): ?>  
                <tr>
                    <td><?=$e['parent_name'].' > '.$e['name']?></td><?php
    foreach($arrAdminPriviledge as $e2): 
        $bolFound = false; $strNotFound = '';
        foreach($arrAdminLinkPriviledge as $e3) if($e3['adlk_login_id'] == $e2['strKey'] && $e3['adlk_link_id'] == $e['id']) {
            if($e3['adlk_allow_view'] == 1) $strAllowView = ' checked = "Checked"'; else $strAllowView = '';
            if($e3['adlk_allow_insert'] == 1) $strAllowInsert = ' checked = "Checked"'; else $strAllowInsert = '';
            if($e3['adlk_allow_update'] == 1) $strAllowUpdate = ' checked = "Checked"'; else $strAllowUpdate = '';
            if($e3['adlk_allow_delete'] == 1) $strAllowDelete = ' checked = "Checked"'; else $strAllowDelete = '';
            if($e3['adlk_allow_approve'] == 1) $strAllowApprove = ' checked = "Checked"'; else $strAllowApprove = '';
            $bolFound = true; break;
        } 
        if(!$bolFound) {
            $strNotFound = ' class="danger"';
            $strAllowView = ''; $strAllowInsert = ''; $strAllowUpdate = ''; $strAllowDelete = ''; $strAllowApprove = '';
        } ?>  
                    <td<?=$strNotFound?>><input type="checkbox" name="cbAllowView[<?=$e['id']?>][<?=$e2['strKey']?>]" value="1"<?=$strAllowView?> /></td><?php
        if($e['link_type'] == 'table' || 1 == 1): ?>  
                    <td<?=$strNotFound?>><input type="checkbox" name="cbAllowInsert[<?=$e['id']?>][<?=$e2['strKey']?>]" value="1"<?=$strAllowInsert?> /></td>
                    <td<?=$strNotFound?>><input type="checkbox" name="cbAllowUpdate[<?=$e['id']?>][<?=$e2['strKey']?>]" value="1"<?=$strAllowUpdate?> /></td>
                    <td<?=$strNotFound?>><input type="checkbox" name="cbAllowDelete[<?=$e['id']?>][<?=$e2['strKey']?>]" value="1"<?=$strAllowDelete?> /></td>
                    <td<?=$strNotFound?>><input type="checkbox" name="cbAllowApprove[<?=$e['id']?>][<?=$e2['strKey']?>]" value="1"<?=$strAllowApprove?> /></td><?php
        else: ?>  
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><?php
        endif;
    endforeach; ?>  
                </tr><?php
    $i++;
endforeach; ?>  
    </tbody>
    </table></div>
    <div class="form-group action">
        <button type="submit" name="smtProcessType" id="smtProcessType" value="Save" class="btn btn-primary">Save</button>&nbsp;
        <a href="<?=site_url()?>" class="btn btn-default">Back</a>
    </div>
</form>