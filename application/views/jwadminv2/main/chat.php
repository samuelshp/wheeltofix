<?php
$strPageTitle = 'Live Chat';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="You can chat with the visitor of your website here" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-mail-forward"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<div class="col-xs-12">
	<!-- PLEASE COPY - PASTE YOUR CHAT WIDGET HERE -->
</div>