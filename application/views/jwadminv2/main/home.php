<?php
$strPageTitle = $this->lang->jw('dashboard');
$strPageDescription = 'Welcome, '.$this->session->userdata('strAdminUserName');
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<div class="col-xs-12">
    <div class="panel">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Main Menu</h3></div>
        <div class="panel-body"><div class="row"><?php
foreach($arrLinkList as $e) if($e['link_parent'] > 0): ?>  
            <div class="col-xs-6 col-md-3">
                <div class="wrapper">
                    <h4><?=translateLink($e)?> <i class="fa fa-info-circle text-success"></i></h4>
                    <div class="instruction">
                        <div class="wp">
                            <div class="wc">
                                <?=$e['instruction']?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><?php
endif; ?>  
        </div></div>
    </div>
</div>