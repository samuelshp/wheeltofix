<?php
$strPageTitle = 'Mass Mail';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="You can send mail or some newsletter to all of your members here" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> '.$this->lang->jw('dashboard'), 'link' => site_url()),
    1 => array('title' => '<i class="fa fa-mail-forward"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH."/views/$strViewFolder/contentheader.php"); ?>  

<div class="col-xs-12 col-md-4 col-lg-3" id="leftPan">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-envelope"></i> <?=$this->lang->jw('choose_draft')?></h3></div>
        <div class="panel-body">
            <div class="list-group">
                <div class="list-group-item"><?php
if(!empty($arrMailList)) foreach($arrMailList as $e): ?>  
                <p><a href="<?=site_url('main/mass_mail/'.$e['id'])?>"><?=$e['mail_title']?></a></p><?php
endforeach; ?>  
                </div>
            </div>
            <h3 class="panel-title"><i class="fa fa-info-circle"></i> Info</h3>
            <ul>
                <li>{name} = Receiver name</li>
                <li>{email} = Receiver email</li>
            </ul>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-8 col-lg-9" id="contentPan">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-mail-forward"></i> <?=$this->lang->jw('send_mass')?></h3></div>
        <div class="panel-body"><?php
if(!empty($arrMailContent['mail_title'])) $strMailTitle = $arrMailContent['mail_title'];
else $strMailTitle = '';
if(!empty($arrMailContent['mail_content'])) $strMailContent = $arrMailContent['mail_content'];
else $strMailContent = ''; ?>  
        <form name="frmSendMail" id="frmSendMail" method="post" action="<?=site_url('main/mass_mail/'.$intMailID)?>">
            <input type="hidden" name="hidMode" value="<?=$strMode?>" />
            <textarea name="txaTo" id="txaTo" class="required form-control" style="display:none;"></textarea>
            <div class="form-group"><div class="input-group">
                <span class="input-group-addon"><?=$this->lang->jw('MAIL-to')?></span>
                <select name="selTo" id="selTo" multiple="multiple" class="form-control" style="border-radius:0;" data-placeholder="<?=$this->lang->jw('FORM-select')?>"><?php
if(!empty($arrMemberList)) foreach($arrMemberList as $e): ?>  
                    <option value="<?=$e['mnws_email']?>"><?=$e['mnws_name']?></option><?php
endforeach; ?>  
                </select>
                <span class="input-group-addon"><label style="margin:0; font-weight:normal;"><input type="checkbox" id="cbSendCheckAll"> <?=$this->lang->jw('FORM-check_all')?></label></span>
            </div></div>
            <div class="form-group"><div class="input-group">
                <span class="input-group-addon"><?=$this->lang->jw('MAIL-subject')?></span>
                <input type="text" name="txtSubject" value="<?=$strMailTitle?>" class="required text form-control" maxlength="256" />
            </div></div>
            <div class="form-group">
                <label>Message</label>
                <textarea name="txaContent" id="txaContent" class="form-control"><?=$strMailContent?></textarea>
            </div>
            <button type="submit" name="subSend" id="subSend" value="Send" class="btn btn-primary"><?=$this->lang->jw('FORM-send')?></button>
            <button type="submit" name="subSave" value="Save as Draft" class="btn btn-warning" title="Save as Draft"><i class="fa fa-hdd-o"></i></button>
            <button type="submit" name="subDelete" value="Delete"  class="btn btn-danger pull-right"><?=$this->lang->jw('FORM-delete')?></button>
        </form>
        </div>
    </div>
</div>
