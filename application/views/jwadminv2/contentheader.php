<div class="col-xs-12">
    <h1><?=$strPageTitle?>
        <small class="no-print"><?php
if($this->input->get('dependant') != ''):
    $arrDependantFieldList = searchInArray($arrFieldList, 'title', $this->input->get('dependant'));
    echo translateDataIntoReadableStatements($arrDependantFieldList, $this->input->get('filter_'.$this->input->get('dependant')));
endif; ?>  
            <?=$strPageDescription?>
        </small>
    </h1><?php

if(!empty($arrBreadcrumb)): ?>  
    <ol class="breadcrumb no-print"><?php
    for($i = 0; $i < count($arrBreadcrumb); $i++) if($i == count($arrBreadcrumb) - 1): ?>  
        <li class="active"><?=$arrBreadcrumb[$i]['title']?></li><?php
    else: ?>  
        <li><a href="<?=$arrBreadcrumb[$i]['link']?>"><?=$arrBreadcrumb[$i]['title']?></a></li><?php
    endif; ?>  
    </ol><?php
endif;

if(!empty($strMessage)): ?>  
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=$strMessage?>
    </div><?php
endif; ?>  
</div>