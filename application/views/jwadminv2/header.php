<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainMenu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <i class="fa fa-minus"></i>
    </button>
    <a class="navbar-brand" href="<?=site_url('main')?>"><?=$this->config->item('jw_website_name')?></a>
</div>