<?php
$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)
if(!isset($_CI)) $_CI = new JW_Controller();
?><html>
<head>
    <title><?php echo $heading; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=str_replace('<jw:site_url>',base_url(),$_CI->config->item('jw_website_icon'))?>" />
    <link href="<?=base_url('asset/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/bootstrap/bootstrap.min.js')?>"></script>
<style type="text/css">
body { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);}
header h1 img {max-height:90px; margin-top:40px;}
footer {border-top:#ccc 1px solid; font-size:12px; padding:15px; color:#888;}
h1 {font-size:48px;} h2 {font-size:32px;}
.error-template {padding: 40px 15px;text-align: center;}
.error-actions {margin-top:15px;margin-bottom:15px;}
.error-actions .btn {margin-right:10px;}
.error-icon {font-size:108px; margin:-20px 0px -30px;}
.link a:not(:last-of-type):after {content:"|"; margin:0 5px 0 10px;}
.jumbotron {border:#ccc 1px solid;}
.jumbotron * {font-size:12px !important;}
</style>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6" style="margin:0 auto; float:none;">
            <header class="text-center"><?php
if($_CI->config->item('jw_website_logo') != ''): ?>
                <h1><img src="<?=str_replace('<jw:site_url>',base_url(),$_CI->config->item('jw_website_logo'))?>" title="<?=$_CI->config->item('jw_website_motto')?>" alt="<?=$_CI->config->item('jw_website_name')?>" /></h1><?php
else: ?>  
                <h1><?=$_CI->config->item('jw_website_name')?></h1>
                <h2><?=$_CI->config->item('jw_website_motto')?></h2><?php
endif; ?>  
                
            </header>
            <main class="error-template text-center">
                <div class="error-icon text-danger" style="-webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); -ms-transform: rotate(90deg); -o-transform: rotate(90deg); filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);">:(</div>