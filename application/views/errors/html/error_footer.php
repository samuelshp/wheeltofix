<?php
$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)
if(!isset($_CI)) $_CI = new JW_Controller();
?>
				<p class="text-danger">If you are an administrator of this website, please <b>record error information above</b> by <abbr title="Block the text > Ctrl + C > Ctrl + V">copy-paste the text</abbr> or <abbr title="Press Print Screen Button and paste it on image editor">take a screenshot</abbr> then email it to your webmaster</p>
				<p class="text-muted" style="margin-top:45px;">We are very sorry for your inconvenience. Meanwhile, please do the following action</p>
				<div class="error-actions">
                	<a href="<?=site_url()?>" class="btn btn-success"><span class="glyphicon glyphicon-home"></span> Home</a>
                	<a href="#" onclick="javascript:history.back();" class="btn btn-primary"><span class="glyphicon glyphicon-repeat"></span> Back</a>
                	<a href="<?=site_url('contact')?>" class="btn btn-warning"><span class="glyphicon glyphicon-envelope"></span> Contact Us</a>
                </div>
            </main>
            <footer class="text-center">
                <p>Copyright &copy; <?=date('Y')?> by <a href="<?=site_url()?>"><?=$_CI->config->item('jw_website_name')?></a>. Powered by <a href="http://www.tobsite.com">Tobsite.com</a></p>
            </footer>
        </div>
    </div>
</div>
</body>
</html>