<?php include('error_header.php'); ?>  
                <h1>Oops!</h1>
                <h3 class="text-info"><?php echo $heading; ?></h3>
                <div class="error-details jumbotron">
                    <?php echo $message; ?>
                </div>
<?php include('error_footer.php'); ?>  