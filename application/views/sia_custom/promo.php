<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function() {

$('input[name*=bonus_qty]').prop('disabled',false);
$('input[name*=prom_discount]').prop('disabled',true);

$('input[type=radio][name^=prom_include_note]').each(function() {
    var name = $(this).attr('name');
    var index = name.replace('prom_include_note', '');
    $(this).change(function(){
        if(this.value == '1') {
            $('input[name*=bonus_qty' + index + ']').prop('disabled',false);
            $('input[name*=prom_discount' + index + ']').prop('disabled',true);
            $('input[name*=prom_discount' + index + ']').val('');
        } else if(this.value == '2') {
            $('input[name*=bonus_qty' + index + ']').prop('disabled',true);
            $('input[name*=prom_discount' + index + ']').prop('disabled',false);
            $('input[name*=bonus_qty' + index + ']').val('');
        } else {
            $('input[name*=bonus_qty]').prop('disabled',false);
            $('input[name*=prom_discount]').prop('disabled',false);
        }
    });
});

$("input[name^=prom_product_id], input[name^=prom_bonus_product_id], input[name^=prom_brand_id], input[name^=prom_bonus_brand_id]").each(function() {
    var $that = $(this);
    var name = $that.attr('name');
    var val = $that.val();
    if(val == '0' || val == '') {
        $(this).val('');
    } else {
        var get_url = '';
        var mode = '';
        if(name.indexOf('brand') !== -1) {
            mode = 'brand';
            get_url = "<?=site_url('invoice_ajax/getProductBrandByID', NULL, FALSE)?>/" + val;
        } else {
            mode = 'product';
            get_url = "<?=site_url('invoice_ajax/getProductByID', NULL, FALSE)?>/" + val;
        }
        $.ajax({
            url: get_url,
            beforeSend: function() {
                $('input[name=' + name + '_submit]').val('');
                // $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                // $("#loadItem").html('');
            },success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                if(mode == 'brand') {
                    $that.val(xml.find('prob_title').text());
                } else if(mode == 'product') {
                    var strName = xml.find('prod_title').text();
                    var strKode = xml.find('prod_code').text();
                    $that.val('(' + strKode + ') ' + strName);
                }
            }
        });
    }
    $that.after('<input type="hidden" name="' + name + '_submit" class="submit" value="' + val + '" />');
});

$("input[name^=prom_product_id]").each(function() {
    var name = $(this).attr('name');
    var index = name.replace('prom_product_id', '');
    $(this).keypress(function() {
        $("input[name=prom_brand_id" + index + "]").val('');
        $("input[name=prom_brand_id_submit" + index + "]").val('');
    });
});
$("input[name^=prom_brand_id]").each(function() {
    var name = $(this).attr('name');
    var index = name.replace('prom_brand_id', '');
    $(this).keypress(function() {
        $("input[name=prom_product_id" + index + "]").val('');
        $("input[name=prom_product_id_submit" + index + "]").val('');
    });
});
$("input[name^=prom_bonus_product_id]").each(function() {
    var name = $(this).attr('name');
    var index = name.replace('prom_bonus_product_id', '');
    $(this).keypress(function() {
        $("input[name=prom_bonus_brand_id" + index + "]").val('');
        $("input[name=prom_bonus_brand_id_submit" + index + "]").val('');
    });
});
$("input[name^=prom_bonus_brand_id]").each(function() {
    var name = $(this).attr('name');
    var index = name.replace('prom_bonus_brand_id', '');
    $(this).keypress(function() {
        $("input[name=prom_bonus_product_id" + index + "]").val('');
        $("input[name=prom_bonus_product_id_submit" + index + "]").val('');
    });
});

$("input[name^=prom_product_id], input[name^=prom_bonus_product_id], input[name^=prom_brand_id], input[name^=prom_bonus_brand_id]").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        var name = this.element.context.attributes.name.value;
        var $that = $('input[name=' + name + ']');
        var get_url = '';
        var mode = '';
        if(name.indexOf('brand') !== -1) {
            mode = 'brand';
            get_url = "<?=site_url('invoice_ajax/getProductBrand', NULL, FALSE)?>/" + request.term;
        } else {
            mode = 'product';
            get_url = "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/0/0/0/" + request.term;
        }
        $.ajax({
            url: get_url,
            beforeSend: function() {
                $('input[name=' + name + '_submit]').val('');
                // $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                // $("#loadItem").html('');
            },success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedItem = xml;
                var display=[];
                if(mode == 'product') {
                    $.map(xml.find('Product').find('item'),function(val,i){
                        var intID = $(val).find('id').text();
                        var strName=$(val).find('strNameWithPrice').text();
                        var strKode=$(val).find('prod_code').text();
                        display.push({label:"("+strKode+") "+ strName,id:intID});
                    });
                } else if(mode == 'brand') {
                    $.map(xml.find('ProductBrand').find('item'),function(val,i){
                        var intID = $(val).find('id').text();
                        var strName=$(val).find('prob_title').text();
                        display.push({label:strName,id:intID});
                    });
                }
                response(display);
                if(display.length == 1) {
                    $that.data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                    $that.val(display[0].label);
                    $that.autocomplete('close').val('');
                }
            }
        });
    },
    select: function(event, ui) {
        var name = event.target.attributes.name.nodeValue;
        var $that = $('input[name=' + name + '_submit]');
        var mode = '';
        if(name.indexOf('brand') !== -1) {
            mode = 'brand';
        } else {
            mode = 'product';
        }
        if(mode == 'product') {
            var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
                return $(el).find('id').text() == ui.item.id;
            });
        } else if(mode == 'brand') {
            var selectedItem = $.grep($arrSelectedItem.find('ProductBrand').find('item'), function(el) {
                return $(el).find('id').text() == ui.item.id;
            });
        }
        $that.val($(selectedItem).find('id').text());
    }
});

$("#frmTable").submit(function(e) {
    $("input[name^=prom_product_id], input[name^=prom_bonus_product_id], input[name^=prom_brand_id], input[name^=prom_bonus_brand_id]").each(function() {
        var name = $(this).attr('name');
        var val = $(this).val();
        if(val.length > 0) {
            $(this).val($('input[name=' + name + '_submit]').val());
        }
    });
});

});</script>