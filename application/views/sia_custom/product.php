<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
var isRealSubmit = false;
var page_mode = $('body').data('page');

$("#frmTable").submit(function(e) {
    if(!isRealSubmit && page_mode != 'table/browse' && $('input[name="prod_code"]').length > 0) {
        e.preventDefault();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getAvailableProductCode', NULL, FALSE)?>/" + $('input[name="prod_code"]').val(),
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                var same='';
                $.map(xml.find('Product').find('item'),function(val,j){
                    var strTitle =$(val).find('prod_title').text();
                    same=same+" "+strTitle+"; ";
                });
                if(same != '') {
                    alert("Kode sudah dipakai untuk :"+same);
                } else {
                    isRealSubmit = true;
                    $("#smtProcessType").click();
                }
            }
        });
    }
});
});</script>