<?php
# Please include_once in every page needed sidebar
# E.g.: include_once(APPPATH.'views/travelify/sidebar.php');
?>
<div id="secondary">
	<aside id="calendar-3" class="widget widget_calendar">
		<h2 class="widget-title">Calendar</h2>
		<div id="calendar"></div>
		<div class="entry-meta-bar" style="padding-right:0px;background:none;border:none;">
			<a href="<?=site_url('main/calendar')?>" class="readmore" style="text-transform:none;">See Full Calendar</a>
		</div>
		<div style="clear:both;"></div>
	</aside>
</div>
<!-- #secondary -->