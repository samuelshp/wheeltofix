<?php
$_CI =& get_instance();
$arrData = array_merge($_CI->getArrData(),$this->_ci_cached_vars);
extract($arrData);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=!empty($strPageTitle) ? $strPageTitle : ''?></title>
	<meta name="author" content="tobsite.com" />
</head>
<body<?=(in_array(ENVIRONMENT, array('production'))) ? ' onload="window.print()"' : ''?>>
<?php
	include_once('sia_print/'.$strPrintFile.'.php');
?>
</body>
</html>