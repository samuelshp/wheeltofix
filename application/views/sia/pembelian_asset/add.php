<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-cubes"></i> Pembelian Asset', 'link' => site_url('pembelian_asset/browse')),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
	<form method="POST" action="">			
		<div class="row">
			<div class="col-xs-6">
				<div class="panel panel-primary">
	           		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Asset</h3></div>
	                <div class="panel-body">
	                	<!-- <div class="form-group">
                            <div class="col-sm-4 control-label">
                                Kode
                            </div>
                            <div class="col-sm-8">
                                Auto Generate
                            </div>
                            <div class="clearfix"></div>
                        </div> -->
                        <div class="form-group">
                            <div class="col-sm-4 control-label">
                                Tanggal
                            </div>
                            <div class="col-sm-8">
                                <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>		
                        <div class="form-group">
                            <div class="col-sm-4 control-label">
                                Supplier
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 control-label">
                                No. COA Asset
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control chosen" name="intCOAasset" id="intCOAasset" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['acco_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>	
                        <div class="form-group">
                            <div class="col-sm-4 control-label">
                                No. COA Akumulasi
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control chosen" name="intCOAakumulasi" id="intCOAakumulasi" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['acco_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>	
                        <div class="form-group">
                            <div class="col-sm-4 control-label">
                                No. COA Beban
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control chosen" name="intCOAbeban" id="intCOAbeban" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['acco_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>			                    
	                </div>
	        	</div>	
			</div>				
		</div>	
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-primary">
	           		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cubes"></i> Items</h3></div>
	                <div class="panel-body">
	                	<div class="table-responsive"><table class="op_item table table-bordered table-condensed table-hover" id="opItems">
			                    <thead>
			                        <tr>
			                            <th rowspan="2">Nama Barang</th>
			                            <th colspan="2">Unit</th>			                            
			                            <th rowspan="2">Harga</th>                                                        
			                            <th colspan="2">Nilai Perolehan</th>                            
			                            <th colspan="3">Penyusutan</th>                            			                            
			                        </tr>
			                        <tr>			                            
			                            <th>Jumlah</th>
			                            <th>Satuan</th>			                            
			                            <th>Default</th>                            
			                            <th>Rp.</th>			                           
			                            <th>Sisa (Rp.)</th>                            
			                            <th colspan="2">Lama (Bulan)</th>
			                        </tr>
			                    </thead>
			                    <tbody id="tbodyItem">                    
			                    	<td>
			                    		<input class="form-control" name="strNamaBarang[]" type="text" required style="z-index: 0" />	
			                    	</td>
		                            <td>
		                            	<input class="form-control currency" name="intAmountQty[]" type="text" required style="z-index: 0" />	
		                            </td>
		                            <td>
		                            	<input class="form-control" name="strSatuan[]" type="text" required style="z-index: 0" />
		                            </td>
		                            <td>
		                            	<div class="input-group">
		                                   <div class="input-group-addon">Rp.</div>
		                                   <input class="form-control currency intAmount" name="intAmount[]" type="text" required style="z-index: 0" />
		                               </div>
		                            </td>			                            
		                            <td>
		                            	<div class="input-group">
		                            		<div class="input-group-addon">Rp.</div>
		                            		<input class="form-control currency" name="intAmountDefault[]" type="text" required style="z-index: 0" />
		                            	</div>
		                            </td>                            
		                            <td>
		                            	<div class="input-group">
		                                   <div class="input-group-addon">Rp.</div>
		                                   <input class="form-control currency" name="intAmountFinal[]" type="text" required style="z-index: 0" />
		                               </div>
		                            </td>			                           
		                            <td>
		                            	<div class="input-group">
		                                   <div class="input-group-addon">Rp.</div>
		                                   <input class="form-control currency" name="intAmountPenyusutanSisa[]" type="text" required style="z-index: 0" />
		                            	</div>
		                            </td>                            
		                            <td>		                            	
		                                <input class="form-control currency" name="intLama[]" type="text" required style="z-index: 0" />	
		                            </td>
		                            <!-- <td><button type="button" name="tambahItem" class="btn btn-sm btn-link"><i class="fa fa-plus"></i></button></td> -->
			                    </tbody>			                    
			                </table>
			            </div>		                    
	                </div>
	        	</div>	
			</div>				
		</div>
		<div class="row">
			<div class="col-xs-8">
				<div class="panel panel-primary">	 
					<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Keterangan</h3></div>          	
	                <div class="panel-body">
	                	<div class="form-group">                            
                            <div class="col-sm-12">
                                <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                            </div>                            
                        </div>	                	
	                </div>
	        	</div>
			</div>
			<div class="col-xs-4">
				<div class="panel panel-primary">	 
					<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Summary</h3></div>          	
	                <div class="panel-body">	                	
                		<div class="row">
	                        <div class="col-sm-4 tdTitle">Subtotal</div>
	                        <div class="col-sm-8 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotal" class="form-control required currency" id="subTotal" value="0" readonly /></div>
	                        </div>
	                    </div>
	                    <p class="spacer">&nbsp;</p>
	                    <div class="row">
	                        <div class="col-sm-4 tdTitle">PPN</div>
	                        <div class="col-sm-8 tdDesc"><div class="input-group"><input type="text" name="ppn" class="form-control required currency" id="ppn" value="0" /><label class="input-group-addon">%</label></div>
	                        </div>
	                    </div>
	                    <p class="spacer">&nbsp;</p>
	                    <div class="row">
	                        <div class="col-sm-4 tdTitle">Grand Total</div>
	                        <div class="col-sm-8 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="grandTotal" class="form-control required currency" id="grandTotal" value="0" readonly /></div>
	                        </div>
	                    </div>
	                </div>
	        	</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button type="submit" class="btn btn-primary" name="smtMakePembelianAsset" value="Pembelian Asset"><i class="fa fa-plus"></i></button>
			</div>			
		</div>	
	</form>
</div>