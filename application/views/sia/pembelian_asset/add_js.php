<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);

$("input[name^=intAmount]").keyup(calculateSubtotal);
$("input[name=ppn]").on('keyup change',calculateGrandtotal);
$("button[name=tambahItem]").click(addRowItem);

function addRowItem(e) {
	var element = '<tr><td><input class="form-control input-sm" name="strNamaBarang[]" type="text" required style="z-index: 0" /></td>'+
                '<td><input class="form-control input-sm currency" name="intAmountQty[]" type="text" required style="z-index: 0" /></td>'+
                '<td><input class="form-control input-sm" name="strSatuan" type="text" required style="z-index: 0" /></td>'+
                '<td><div class="input-group"><div class="input-group-addon ">Rp.</div><input class="form-control input-sm currency" name="intAmount[]" type="text" required style="z-index: 0" /></div></td>'+
                '<td><div class="input-group"><div class="input-group-addon ">Rp.</div><input class="form-control input-sm currency" name="intAmountDefault[]" type="text" required style="z-index: 0" /></div></td>'+
                '<td><div class="input-group"><div class="input-group-addon ">Rp.</div><input class="form-control input-sm currency" name="intAmountFinal[]" type="text" required style="z-index: 0" /></div></td>'+
                '<td><div class="input-group"><div class="input-group-addon ">Rp.</div><input class="form-control input-sm currency" name="intAmountPenyusutanSisa[]" type="text" required style="z-index: 0" /></div></td>'+
                '<td><input class="form-control input-sm currency" name="intLama" type="text" required style="z-index: 0" /></td>'+
                '<td><button type="button" name="hapusItem" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td></tr>';
    $("tbody#tbodyItem").append(element);
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);

    $("button[name=hapusItem]").click(deleteRowItem);	
}

function deleteRowItem(e) {
	$(e.target)
        .parents('tr')
        .hide(150, function(){            
            $(e.target).parents('tr').remove()
            // calculateSubtotal();
        });
}

function calculateSubtotal() {
	var subTotal = 0;	
    var qty = $("input[name^=intAmountQty]").autoNumeric('get');
	$("input.intAmount").each(function(){
		subTotal += $(this).autoNumeric('get');
	});

	$("input[name=subTotal]").autoNumeric('set',subTotal*qty);
	calculateGrandtotal();
}

function calculateGrandtotal() {
	var subtotal = parseFloat($("input[name=subTotal]").autoNumeric('get'));
	var ppn = parseFloat($("input[name=ppn]").autoNumeric('get'));
	
	var grandtotal = subtotal + (subtotal*ppn/100);
	$("input[name=grandTotal]").autoNumeric('set',grandtotal);
}

$('.chosen').chosen();
</script>