<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

$date = date('Y/m/d H:i:s');

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>


<div class="col-xs-12">
    <?php
    //$strSearchAction = site_url('daftar_pembayaran_hutang/browse', NULL, FALSE);
    //include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchdph.php'); ?>    
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;">
        <a href="<?=site_url('pembelian_asset', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover main-list-table">
            <thead>
                <tr>                	
                    <th>Tanggal Pembelian</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>                	
                    <th>Total</th>
                    <th class="action">Action</th>      
                </tr>
            </thead>
            <tbody>
                <?php
                // Display data in the table
                if(!empty($arrPembelianAsset)):                    
                    $i=0;
                    foreach($arrPembelianAsset as $e):$i++ ?>
                        <tr>                            
                            <td><?=formatDate2($e['aset_date'],'d F Y')?></td>  
                            <td><?=$e['aset_name']?></td>    
                            <td><?=$e['jumlah_aset']?></td>                      
                            <td><?=setPrice($e['aset_grandtotal'])?></td>
                            <td class="action">
                                <a href="<?=site_url('pembelian_asset/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>                                
                                <!-- <a href="<?=site_url('daftar_pembayaran_hutang/?print='.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a> -->
                            </a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    else: ?>
                        <tr class="info"><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
                    endif; ?>
                </tbody>
            </table>
        </div>        
    </div>
