<?php
$strPageTitle = 'Detail Retur Penjualan Per Customer By Barang';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    //TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
    1 => array('title' => '<i class="fa fa-list"></i> Detail Retur Penjualan Per Customer By Barang', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('report_retur/detailreturjualpercustomerbybarang', NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
        <label>Cari Data:</label>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Awal</label><input type="text" name="txtStart" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Akhir</label><input type="text" name="txtEnd" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><input type="text" id="txtCustomerName"  class="form-control" placeholder="Pilih Pelanggan"/><label class="input-group-addon" id="loadCustomer"></label></div></div>

        <div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button><?php
if(!empty($strCustomerID)):
    $strPrintURL = site_url('report_retur/detailreturjualpercustomerbybarang?print=true&custid='.$strCustomerID.'&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
    <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
    <a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
endif; ?>
		</div>

        <input type="hidden" id="customerID" name="customerID"/>
        <input type="hidden" id="customerCode" name="customerCode"/>
        <input type="hidden" id="customerName" name="customerName"/>

    </form>
    <div class="col-xs-12 col-md-6 pull-left"><div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">Periode</div>
                <div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
                <div class="col-md-3">Hari ini</div>
                <div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">Kode</div>
                <div class="col-md-9">: <?=$strCustomerCode?></div>
                <div class="col-md-3">Nama</div>
                <div class="col-md-9">: <?=$strCustomerName?></div>
            </div>
        </div>
    </div></div>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div class="col-xs-12"><?=$strPage?></div>

    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
            <thead><tr>
                <th>No</th>
                <th>No Trans</th>
                <th>Tanggal</th>
                <th>Nama Barang</th>
                <th>QtyB</th>
                <th>Sat</th>
                <th>QtyT</th>
                <th>Sat</th>
                <th>QtyK</th>
                <th>Sat</th>
                <th>@Harga</th>
                <th>Disc%</th>
                <th>@NetHarga</th>
            </tr></thead>
            <tbody><?php
            if(!empty($arrItems)):
                foreach($arrItems as $e): ?>
                    <tr>
                        <td><?=$e['nourut']?></td>
                        <td><?=$e['invr_code']?></td>
                        <td><?=formatDate2($e['cdate'],'d/m/Y')?></td>
                        <td><?=$e['prod_title']?></td>
                        <td><?=$e['inri_quantity1']?></td>
                        <td><?=$e['prod_unit1']?></td>
                        <td><?=$e['inri_quantity2']?></td>
                        <td><?=$e['prod_unit2']?></td>
                        <td><?=$e['inri_quantity3']?></td>
                        <td><?=$e['prod_unit3']?></td>
                        <td><?=setPrice($e['inri_price'])?></td>
                        <td><?=$e['inri_discount1']?>+<?=$e['inri_discount2']?>+<?=$e['inri_discount3']?></td>
                        <td><?=setPrice($e['netprice'])?></td>
                    </tr>
                <?php endforeach;
            else :?>
                <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
            endif;?>
            </tbody>
        </table></div>
    <?=$strPage?>
</div>
