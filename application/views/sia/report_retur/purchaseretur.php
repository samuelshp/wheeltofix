<?php
$strPageTitle = 'Laporan Purchase Retur';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_retur/purchaseretur', NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group"><input type="text" name="txtSearchValue" class="form-control" placeholder="Pilih Supplier"/></div>
		<div class="col pull-right" style="margin:10px 0px 0px 10px;">
            <button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button><?php
if(!empty($strKeyword)):
    $strPrintURL = site_url('report_retur/purchaseretur?print=true&supplier='.$strKeyword, NULL, FALSE);?>
    <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
    <a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
endif; ?>  
        </div>
    </form>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div class="col-xs-12"><?=$strPage?></div>
	<div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th>Tanggal</th>
        <th>Kode</th>
        <th>Kategori</th>
        <th>Merk</th>
        <th>Nama</th>
        <th>Qty 1</th>        
        <th>Qty 2</th>
        <th>Qty 3</th>
        <th>Harga</th>
        <th>Diskon 1</th>
        <th>Diskon 2</th>
        <th>Diskon 3</th>
        <th>Total</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrRetur)):
    foreach($arrRetur as $e): ?>
        <tr>
            <td><?=formatDate2($e['cdate'],'d/m/Y')?></td>
            <td><?=$e['prcr_code']?></td>
            <td><?=$e['proc_title']?></td>
            <td><?=$e['prob_title']?></td>
            <td><?=$e['prod_title']?></td>            
            <td><?=$e['prri_quantity1']?></td>
            <td><?=$e['prri_quantity2']?></td>
            <td><?=$e['prri_quantity3']?></td>
            <td><?=setPrice($e['prri_price'])?></td>
            <td><?=$e['prri_discount1']?></td>
            <td><?=$e['prri_discount2']?></td>
            <td><?=$e['prri_discount3']?></td>
            <td><?=setPrice($e['prri_subtotal'])?></td>
        </tr><?php
    endforeach;    
else: ?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>