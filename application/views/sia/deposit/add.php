<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bank"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'deposit-deposit'), 'link' => site_url('deposit/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
    <form name="frmAddDeposit" id="frmAddDeposit" class="frmShop" method="post" action="<?=site_url('deposit', NULL, FALSE)?>">
        <div class="row">
            <!--data customer-->
            <div class="col-md-4 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?> <a href="<?=site_url('adminpage/table/add/33', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="form-group"><div class="input-group"><input type="text" id="customerName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"/><label class="input-group-addon" id="loadCustomer"></label></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></label><input id="customerAddress" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-city')?></label><input id="customerCity" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></label><input id="customerPhone" class="form-control" disabled /></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></label><input id="customerOutletTypeStr" class="form-control" disabled /></div></div>
                    </div>
                </div>
            </div> <!--data customer-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'deposit-deposit')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></h4>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" id="txtPrice" name="txtPrice" class="form-control currency required" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!--class row-->

        <div class="form-group"><button type="submit" name="smtMakeDeposit" value="Make Deposit" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

<input type="hidden" id="customerID" name="customerID"/>
    </form>
</div>