<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {

$("#txtPrice").autoNumeric('init', autoNumericOptionsRupiah);

$("#frmAddDeposit").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddDeposit").submit(function() {
	if($("#customerName").val()=='') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"); return false;
	}

	$('.currency').each(function(i) {
		var self = $(this);
		try {
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err) {
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});

	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
	
	return true;
});

var internal=0;
$("#customerName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_retur_ajax/getSaleOrderCustomerAutoCompleteWithSupplierBindRetur', NULL, FALSE)?>/" + $("#customerName").val()+ "/" + $("#supplierID").val(),
			beforeSend: function() {
				$("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadCustomer").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedCust = xml;
				var display=[];
				$.map(xml.find('Customer').find('item'),function(val,i){
					var name = $(val).find('cust_name').text();
					var address = $(val).find('cust_address').text();
					var city = $(val).find('cust_city').text();
                    var code = $(val).find('cust_code').text();
					var outlettype = $(val).find('cust_outlettype').text();
					var outlettypeStr = '';
					switch (outlettype) {
						case '1': outlettypeStr = 'BARU'; break;
						case '2': outlettypeStr = 'BIASA'; break;
						case '3': outlettypeStr = 'MEMBER'; break;
						case '4': outlettypeStr = 'RESELLER'; break;
						case '5': outlettypeStr = 'KANTOR'; break;
						default : outlettypeStr = 'N/A';
					}

					var strName=name+", "+ address +", "+city+", "+outlettypeStr;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedCust = $.grep($arrSelectedCust.find('Customer').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#customerID").val($(selectedCust).find('id').text());
		$("#customerAddress").val($(selectedCust).find('cust_address').text());
		$("#customerCity").val($(selectedCust).find('cust_city').text());
		$("#customerPhone").val($(selectedCust).find('cust_phone').text());
		$("#customerOutletType").val($(selectedCust).find('cust_outlettype').text());
        $('input[name="outletMarketType"]').val($(selectedCust).find('cust_markettype').text());
        internal=parseInt($(selectedCust).find('cust_internal').text());
		var outlettypeStr = '';
		switch ($(selectedCust).find('cust_outlettype').text()) {
			case '1': outlettypeStr = 'BARU'; break;
			case '2': outlettypeStr = 'BIASA'; break;
			case '3': outlettypeStr = 'MEMBER'; break;
			case '4': outlettypeStr = 'RESELLER'; break;
			case '5': outlettypeStr = 'KANTOR'; break;
			default : outlettypeStr = 'N/A';
		}
		$("#customerOutletTypeStr").val(outlettypeStr);
		$('input[name="customerOutletType"]').val($(selectedCust).find('cust_outlettype').text());
		$('input[name="customerID"]').val($(selectedCust).find('id').text());
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
        $("#txtNewItem").prop('disabled', false);
        $("#txtNewItemBonus").prop('disabled', false);
	}
});

});
</script>