<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bank"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'deposit-deposit'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><?php
$strSearchAction = site_url('deposit/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchelement.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('deposit', NULL, FALSE)?>" target="_blank" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th>
			<th class="action">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrDeposit)):
			foreach($arrDeposit as $e): ?>
			<tr>
				<td><?=formatPersonName('ABBR',$e['cust_name'],$e['cust_address'],$e['cust_city'],$e['cust_phone'])?></td>
				<td><?=formatDate2($e['depo_date'],'d F Y')?></td>
				<td><?=setPrice($e['depo_price'])?></td>
				<td><?=$e['depo_status']?></td>
				<td class="action">
					<?php if($e['bolAllowDelete']): ?><a href="<?=site_url('deposit/delete/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></a><?php endif; ?>  
				</td>
			</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</div>
