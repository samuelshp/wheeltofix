<?php
// $strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">  
    <?php
    $strSearchAction = site_url('credit/'.$strLink.'/browse', NULL, FALSE);
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchumc.php'); 
    echo $strBrowseMode;?> 
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage;?></div>
    <div class="col pull-left" style="text-align:right;">
        <a href="<?=site_url('credit/'.$strLink.'/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
    <table class="table table-bordered table-condensed table-hover main-list-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Tanggal</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
    <?php
    // Display data in the table    
    if(!empty($arrUangMasuk)):        
        foreach($arrUangMasuk as $e):?>
            <tr>
                <td><?= $e['umcu_code'] ?></td>
                <td><?= formatDate2($e['umcu_date'], "d F Y") ?></td>
                <td><?= $e['cust_name'] ?></td>
                <td><?= setPrice($e['umcu_amount']) ?></td>
                <td class="action">
                    <a href="<?= site_url('credit/'.$strLink.'/view/'.$e['id'])?>">
                        <i class="fa fa-eye"></i>
                    </a>                    
                </td>

            </tr>
    <?php
        endforeach;
    else: ?>
            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
        </tbody>
    </table>
    </div>
    <?=$strPage?>
</div>
