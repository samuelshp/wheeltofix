<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> '.$strPageTitle, 'link' => site_url('credit/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmEditUangMasukCustomer" id="frmEditUangMasukCustomer" method="post" action="<?=site_url('credit/'.$strLink.'/index/'.$intID, NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">                   
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            No. Transaksi
                        </div>
                        <div class="col-sm-7 col-md-9">                            
                        <input class="form-control" name="txtCode" type="text" value="<?=$arrUangMasukDetail['umcu_code']?>" readonly />
                        <input type="hidden" name="intID" value="<?=$arrUangMasukDetail['id']?>"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                          Tgl *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=$arrUangMasukDetail['umcu_date']?>" disabled />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required disabled>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1" <?=($arrUangMasukDetail['umcu_type'] == 1) ? "selected" : null ?>>Cash</option>
                                <option value="2" <?=($arrUangMasukDetail['umcu_type'] == 2) ? "selected" : null ?>>Bank</option>
                                <option value="3" <?=($arrUangMasukDetail['umcu_type'] == 3) ? "selected" : null ?>>Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="refGiro" <?=($arrUangMasukDetail['umcu_type'] == 3) ? null : "class='hide'"?> >
                        <div class="form-group" >
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text" value="<?=$arrUangMasukDetail['umcu_ref_giro']?>" disabled/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text" value="<?=$arrUangMasukDetail['umcu_jatuhtempo']?>" disabled/>
                            </div>
                            <div class="clearfix"></div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intCOA" id="intCOA" required disabled>
                                <option value="0" disabled selected>Select your option</option>
                                <?php foreach($arrCOA as $e):?>
                                    <option value="<?=$e['id']?>" <?=($arrUangMasukDetail['umcu_acco_id'] == $e['id']) ? "selected" : null ?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                <?php endforeach;?>          
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               Customer *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCustomer" id="intSupplier" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCustomer as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrUangMasukDetail['umcu_cust_id'] == $e['id']) ? "selected" : null ?> ><?=$e['cust_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                    <!-- <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               COA *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCOA" id="intCOA" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrUangMasukDetail['txin_acco_id'] == $e['id']) ? "selected" : null ?>><?=$e['acco_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                    </div>  -->                  
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input class="form-control currency" name="txtAmount" type="text" value="<?=$arrUangMasukDetail['umcu_amount']?>" disabled/>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>                        
                </div>
            </div>                
        </div>
    
        <?php if($bolAllowUpdate):?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" id="smtEditUangMasukCustomer" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button>  
                    <button type="submit" name="smtUpdateUangMasukCustomer" id="smtUpdateUangMasukCustomer" value="Update UMC" class="btn btn-warning hide">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button>      
                    <?php if($bolBtnPrint):?>
                    <button type="submit" id="btnPrint" name="subPrint" value="Print" class="btn btn-success"><i class="fa fa-print"></i></button>                           
                    <?php endif; ?>                      
                    <?php if($bolAllowDelete):?>                
                    <button type="submit" name="smtDeleteUangMasukCustomer" id="smtDeleteUangMasukCustomer" value="Delete UMC" class="btn btn-danger hide pull-right">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?>
                    </button>
                    <?php endif; ?>
                </div>                           
            </div>
        </div>     
        <?php endif; ?>

</form></div>