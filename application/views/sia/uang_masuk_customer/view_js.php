<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>

<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".chosen").chosen({'width': '100%'});

$("#intMethod").change(function(){
    var methodType = $(this).val();    
    if (methodType == 3) { 
        $("#refGiro").removeClass('hide');
    }else{
        $("#refGiro").addClass('hide');
    }
});

$("#smtEditUangMasukCustomer").click(function(){
    $(this).hide();
    $("#smtUpdateUangMasukCustomer").removeClass('hide');
    $("#smtDeleteUangMasukCustomer").removeClass('hide');
    $("#frmEditUangMasukCustomer .form-control").removeAttr('disabled');
    $("#frmEditUangMasukCustomer .chosen").chosen('destroy');
    $("#frmEditUangMasukCustomer .chosen").chosen();
});
</script>