<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script>

giroState = (state = true) => {
    $('#no-giro-input').prop('disabled', state);
    $('#tanggal-jt-input').prop('disabled', state);
}

checkType = () => {
    type  = $('#type-input').val();
    state = type == 3? false : true;
    giroState(state);
}

render = (object) => {
    
}


$(function() {
    var object = <?= json_encode($object) ?>;
    console.log(object);
    if(object != null) 
        render(object);

    checkType();
    $('#type-input').change(checkType);
    
})
</script>