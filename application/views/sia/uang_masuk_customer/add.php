<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('credit/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmAddUangMasukCustomer" id="frmAddUangMasukCustomer" method="post" action="<?=site_url('credit/'.$strLink.'/index', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">                   
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            No. Transaksi
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control" name="txtCode" type="text" value="Auto Generate" disabled />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                          Tgl *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=date('Y-m-d')?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1">Cash</option>
                                <option value="2">Bank</option>
                                <option value="3">Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="refGiro" class="hide">
                        <div class="form-group" >
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intCOA" id="intCOA" required>
                                <option value="0" disabled selected>Select your option</option>
                                <?php foreach($arrCOA as $e):?>
                                    <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                <?php endforeach;?>          
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               Customer *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCustomer" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCustomer as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['cust_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                    <!-- <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               COA *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCOA" id="intCOA" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['acco_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                    </div>        -->            
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input class="form-control currency" name="txtAmount" type="text" style="z-index: 0" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>                        
                </div>
            </div>                
        </div>
    
        <?php if($bolAllowInsert):?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtMakeUangMasukCustomer" id="smtMakePembayaranHutang" value="Make Pembayaran Hutang" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                    </button>                    
                </div>
            </div>
        </div>     
        <?php endif; ?>

</form></div>