<?php
if(!empty($strSearchKey) || !empty($strSearchDate) || !empty($intSearchStatus)) {
    $strSearchFormClass = ' has-warning';
    $strSearchButtonClass = ' btn-warning';
} else {
    $strSearchKey = '';
    $strSearchFormClass = ''; 
    $strSearchButtonClass = ' btn-primary';
} ?>
	<form name="searchForm" method="post" action="<?=$strSearchAction?>" class="form-inline pull-right" style="">
        <div class="input-group<?=$strSearchFormClass?>">
			<span class="input-group-addon"><i class="fa fa-th"></i></span>
	        <input type="text" id="txtSearchNoOp" name="txtSearchNoInvoice" class="form-control input-sm" placeholder="Search No. Invoice" style="width: 150px;" />
	        <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" id="txtSearchNoSupplier" name="txtSearchProject" class="form-control" placeholder="Search Nama Proyek" style="width: 150px; min-width: auto;" />
            <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary<?=$strSearchButtonClass?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
        </div>
    </form>