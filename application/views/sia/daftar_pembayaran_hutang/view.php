<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> Daftar Pembayaran Hutang', 'link' => site_url('daftar_pembayaran_hutang/browse')),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <form name="frmChangeDPH" id="frmChangeDPH" method="post" action="<?=site_url('daftar_pembayaran_hutang/', NULL, FALSE)?>" class="frmShop"> 
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-money"></i> Data Pembayaran</h3></div>
                <div class="panel-body">
                    <div class="col-xs-4">No. DPH : </div>
                    <div class="col-xs-8"><?=$arrDaftarPembayaranHutang['dphu_code']?> <!--DPH ID--><input type="hidden" name="dph_id" value="<?=$arrDaftarPembayaranHutang['id']?>"/></div>
                    <p class="spacer">&nbsp;</p>
                    <div class="col-xs-4">Tanggal dibuat : </div>
                    <div class="col-xs-8">
                        <span id="cdate"><?=formatDate2($arrDaftarPembayaranHutang['dphu_date'], "d F Y")?></span>
                        <input type="text" class="jwDateTime" name="tglEdit" value="<?=$arrDaftarPembayaranHutang['dphu_date']?>" style="display:none"/>
                    </div>                    
                    <p class="spacer">&nbsp;</p>
                    <div class="col-xs-4">Amount Pembayaran : </div>
                    <div class="col-xs-8"><?=setPrice($arrDaftarPembayaranHutang['dphu_amount'])?><!-- textbox untuk notif --><input name="dphu_amount" type="hidden" value="<?=$arrDaftarPembayaranHutang['dphu_amount']?>"/></div>
                    <p class="spacer">&nbsp;</p>
                    <div class="col-xs-4">Supplier : </div>
                    <div class="col-xs-8"><?=$arrDaftarPembayaranHutang['supp_name']?></div>
                    <p class="spacer">&nbsp;</p>   
                    <p class="spacer">&nbsp;</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover" >
                            <thead>
                                <tr class="headerItem">
                                    <th>No.PI</th>
                                    <th>Tanggal</th>
                                    <th>DPP</th>
                                    <th>Ppn</th>
                                    <th>Amount</th>
                                    <th>Pph</th>                                    
                                    <th>Amount Total</th>
                                    <th>Rencana Bayar</th>
                                    <th>Dianggap Lunas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($arrDaftarPembayaranHutangItem)):                                                                          
                                    foreach($arrDaftarPembayaranHutangItem as $e):                                    
                                ?>
                                        <tr class="item">
                                            <td><input type="hidden" value='<?= $e['item_id'] ?>' class='item_id' name="dphi_id[]"/><?= $e['code']?></td>                                                              
                                            <td><?=formatDate2($e['cdate'], 'd F Y')?></td>
                                            <td><?=setPrice($e['pinv_subtotal'])?></td>
                                            <td><?=$e['pinv_tax']?> %</td>
                                            <td><?=setPrice($e['pinv_totalbeforepph'])?></td>
                                            <td><?=$e['pph']?> %</td>
                                            <td><?=setPrice($e['pinv_finaltotal'])?></td>
                                            <td><div class="input-group">
                                                    <div class="input-group-addon">Rp.</div>
                                                    <input type="text" class="form-control currency input-sm amountrencanabayar" name="amountRencanaBayar[]" value="<?=$e['dphi_amount_rencanabayar']?>" data-v-min="0.00" disabled/>
                                                </div>
                                                <input type="hidden" name="itemAmountTotal[]" value="<?=$e['pinv_finaltotal']?>"/>
                                                <input type="hidden" class="sisa_bayar" name="sisa_bayar[]" value="<?=$e['dphi_sisabayar']?>">                                                
                                                <input type="hidden" name="itemTerbayar" value="<?= $e['pinvit_purchase_invoice_id'] ?>-<?=$e['dphi_amount_rencanabayar']?>" ></td>
                                            <td><input type="checkbox" name="chbxLunas[]" <?=($e['dphi_lunas'] == 1)?"checked":null;?> disabled></td>
                                        </tr>
                                        <?php
                                endforeach;
                                else:?>
                                    <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                                    <?php
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>                   
                </div>
            </div>
        </div>
                    
    </div>  
    <?php//if($bolAllowUpdate):?>
        <div class="row">
            <div class="col-md-12 ">
                <div class="form-group">
                    <?php if($arrDaftarPembayaranHutang['dphu_status'] < 4 ):?>
                    <button type="button" id="btnEditDPH" class="btn btn-primary"><i class="fa fa-edit"></i></button>        
                    <?php endif; ?>
                    <button type="submit" id="btnUpdateDPH" name="smtUpdateDaftarPembayaranHutang" value="Update DPH" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>                    
                    <?php if($bolBtnPrint):?>
                    <button type="submit" id="subPrint" name="subPrint" class="btn btn-success" value="Print DPH"><i class="fa fa-print"></i></button>
                    <?php endif; ?>                      
                    <?php if($arrDaftarPembayaranHutang['dphu_status'] < 4):?>    
                    <button type="submit" id="btnDeleteDPH" name="smtDeleteDaftarPembayaranHutang" value="Delete DPH" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>  
                    <?php endif; ?>                    
                </div> 
            </div>    
        </div>
        <?php//endif; ?>         
    </form>
</div>