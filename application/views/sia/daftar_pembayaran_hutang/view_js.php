<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>

<script type="text/javascript">
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("input.amountrencanabayar").on('keyup', calculateSisaBayar);

$("#btnEditDPH").click(function(){
    $("tr.headerItem").append(`<th>Delete</th>`);
    $("tr.item").append(`<td><input type="hidden" name="idToDelete[]" class="idToDelete"><button type="submit" class="btn btn-danger btn-sm" name="btnDeleteItem" value="Delete Item"><i class="fa fa-trash"></i></button></td>`);
    $("span#cdate").hide();
    $("input[name=tglEdit]").show();
    $(this).hide();
    $("#btnUpdateDPH").show();
    $("#btnDeleteDPH").show();
    $("input.amountrencanabayar").removeAttr('disabled');
    $("input[name^=chbxLunas]").removeAttr('disabled');
    deleteHandler();
    deleteItemHandler();    
});  

function deleteHandler() {
    $("button#btnDeleteDPH").click(function(e){          
        var konfirmasi = confirm("Apakah yakin akan menghapus DPH ini?");        
        if (konfirmasi) {
            $("#frmChangeDPH").submit();
        }else{
            e.preventDefault();
        }
    });
}

function deleteItemHandler(){
   $("button[name=btnDeleteItem]").click(function(e){  
        var id = $(this).closest('tr').find("input.item_id").val();
        console.log(id);
        $(this).closest('tr').find("input.idToDelete").val(id); 
        var konfirmasi = confirm("Apakah yakin akan menghapus item?");        
        if (konfirmasi) {
            $("#frmChangeDPH").submit();
        }else{
            e.preventDefault();
        }
    });
}

function calculateSisaBayar(e) {    
    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 9) {
        var totalAmount = $(e.target).parents("tr").find("input[name^='itemAmountTotal']").val();
        var rencanaBayar = $(e.target).autoNumeric('get');
        var sisaBayar = $(e.target).parents("tr").find('.sisa_bayar').val(totalAmount - rencanaBayar);        
    }    
}

</script>