<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> Daftar Pembayaran Hutang', 'link' => site_url('daftar_pembayaran_hutang/browse')),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<?php
$jumlahDataTemp = 0;
?>

<form method="POST" action="" id="formDPH">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"> Data Pembayaran Hutang</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-nopembayaran')?>
                            </div>
                            <div class="col-sm-7 col-md-8">
                            <input class="form-control" name="txtCode" type="text" value="Auto Generate" disabled />
                            </div>
                            <div class="clearfix"></div>
                        </div>                        
                        <!-- <p class="spacer">&nbsp;</p> -->
                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                Tanggal
                            </div>
                            <div class="col-sm-7 col-md-8">
                                <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                Supplier
                            </div>
                            <div class="col-sm-7 col-md-8">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                Amount
                            </div>
                            <div class="col-sm-7 col-md-8">
                                <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                    <input id="amount" type="text" readonly class="form-control currency" name="amount" style="z-index:0"> 
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Purchase</h3></div>
            <div class="panel-body">                               
                <div class="table-responsive"><table class="op_item table table-bordered table-condensed table-hover" id="opItems">
                    <thead>
                        <tr>
                            <th>No.PI</th>
                            <th>Tanggal</th>
                            <th>DPP</th>
                            <th>Ppn</th>                                                        
                            <th>Amount</th>                            
                            <th>PPh</th>
                            <th>Total</th>                            
                            <th>Rencana Bayar</th>
                            <th>Sisa Bayar</th>
                            <th>Status 2 minggu</th>
                            <th>Dianggap Lunas</th>
                            <th>Dibayar</th>
                        </tr>
                    </thead>
                    <tbody id="tbDataPurchase">                    
                        <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" name="smtMakeDaftarPembayaranHutang" class="btn btn-success pull-right" value="save" style="margin-left:5px;"><i class="fa fa-floppy-o"></i> Save</button>
        <a href="<?= site_url('daftar_pembayaran_hutang/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
    </div>
</form>