<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);      

$("#formDPH").submit(function(){
	return confirm("Apakah yakin akan menyimpan data?");
});	

$("select[name=intSupplier]").change(function(){
	var amountTotal;
    $("tr.result").remove();
    var purchInvoice = loadPurchaseInvoice($(this).val());    
    purchInvoice.done(function(result, textStatus, xhr){    	
    	if (result.success === true) {    			
        		$(".info").hide();
	            $.each(result.data, function(i, arr){	
	                $("#tbDataPurchase").append(`                	
		        		<tr class="result">
	                        <td><input type="hidden" value='' id="purchase_code" name="purchase_code"/>`+arr['pinv_code']+`</td>
	                        <td>`+arr['createAt']+`</td>	  
	                        <td class='currency-text'>`+arr['pinv_grandtotal']+`</td>                     	                        
	                        <td>`+arr['pinv_tax']+`%</td>                     	                        
	                        <td class='currency-text'>`+arr['pinv_totalbeforepph']+`</td>                                    
	                        <td>`+arr['pinv_pph']+`%</td>
	                        <td class='currency-text'>`+arr['pinv_finaltotal']+`</td>	                        
	                        <td>
	                            <div class="input-group">
	                            <div class="input-group-addon">Rp.</div>
	                                <input type="text" name="rencanaBayar[]" class="form-control input-sm currency rencanabayar" value="`+arr['sisa_bayar']+`" data-raw="`+arr['sisa_bayar']+`"/>
	                        </div></td>
	                        <td>
	                            <div class="input-group">
	                            <div class="input-group-addon">Rp.</div>
	                                <input type="text" name="sisaBayar[]" class="form-control input-sm currency sisabayar" value="0" data-v-min="-99999999999.99" v-max="`+arr['sisa_bayar']+`" data-raw="`+(arr['pinv_sisa_bayar'])+`" readonly/>
	                        </div></td>
	                        <td>`+arr['status']+`</td>
	                        <td><input type="checkbox" name="chbxLunas[]" class="chbxLunas" data-id="`+arr['pi_id']+`" data-type="`+arr['type']+`" checked/>
	                        </td>
	                        <td><input type="checkbox" name="chbxDibayar[]" class="chbxDibayar" data-id="`+arr['pi_id']+`" data-type="`+arr['type']+`" value=""/>
	                        </td>
	                                                            	                      
	                    </tr>                	
	                `);
	            });
	            $(".rencanabayar").keyup(function(){
	            	var elSisa = $(this).closest('tr.result').find('.sisabayar');
	            	var elCheckbox = $(this).closest('tr.result').find("input.chbxDibayar");
	            	if (elCheckbox.is(":checked")) elCheckbox.prop('checked', false);
	            	var raw_total = parseFloat($(this).data('raw'));
	            	var sisa = parseFloat($(this).closest('tr.result').find('.sisabayar').data('raw'));	            		            	
	            	$(this).closest('tr.result').find('.sisabayar').autoNumeric('set', raw_total-$(this).autoNumeric('get'));

	            	amountTotal = calculateAmount();
					$("#amount").autoNumeric('set',amountTotal);
	            });
	            $("input.chbxLunas").click(function(e){
	            	$(this).closest('tr').find('.chbxDibayar').prop('checked', false);	            	
	            });
	            $("input.chbxDibayar").click(function(e){	            	
	            	var txtRencanaBayar = $(this).closest('tr').find('.rencanabayar').autoNumeric('get');	            	
	            	var txtSisaBayar = $(this).closest('tr').find('.sisabayar').autoNumeric('get');
	            	var txtLunas = ($(this).closest('tr').find('.chbxLunas').is(':checked')) ? 1 : 0;	            	
					if(txtRencanaBayar != 0){
						amountTotal = calculateAmount();
					    $("#amount").autoNumeric('set',amountTotal);
					    $(this).val($(this).data('id')+"-"+txtRencanaBayar+"-"+$(this).data('type')+"-"+txtSisaBayar+"-"+txtLunas);
					}else{
						e.preventDefault();
						alert("Rencana Bayar tidak boleh kosong atau 0"	);
						$(this).closest('tr').find('.rencanabayar').focus();
					}  
				});				
            $(".chosen").trigger("chosen:updated");
            $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
            $(".currency-text").autoNumeric('init', {autoNumericOptionsRupiah, aSign: "IDR "});
    	}else{
    		$(".info").show();
    	}
    }).error(function(err){
    	alert("Oops. Gagal menampilkan data untuk DPH.");
    	console.log(err.statusText);
    });
});

function calculateAmount(){
    var amountTotal = 0;
    $("input.chbxDibayar:checked").each(function(){
    	// if ($(this).data('type') !== 'DP' || amountTotal == 0) {
    	if($(this).closest('tr').find('.chbxLunas').is(":checked")){
    		amountTotal += parseFloat($(this).closest('tr').find('.rencanabayar').autoNumeric('get')) + parseFloat($(this).closest('tr').find('.sisabayar').autoNumeric('get'));
    	}else{
    		amountTotal += parseFloat($(this).closest('tr').find('.rencanabayar').autoNumeric('get'));
    	}
    	// }else{
    	// 	amountTotal -= parseFloat($(this).closest('tr').find('.rencanabayar').autoNumeric('get')); 
    	// }
        
    });
    return amountTotal;
}

function loadPurchaseInvoice(idSupplier) {
	return $.ajax({
		url: "<?=site_url('api/purchase_invoice_list/list?h="+userHash+"&filter_supp_id=')?>"+idSupplier,
		method: "GET",
		dataType: "JSON"		
	});
}


$('.chosen').chosen({search_contains: true});

</script>