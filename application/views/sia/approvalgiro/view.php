<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> Approval Giro', 'link' => site_url('approvalgiro/'.$giroType.'/browse', NULL, FALSE)),    
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<form name="frmApprovalGiro" id="frmApprovalGiro" method="post" action="<?=site_url('approvalgiro/'.$giroType.'/', NULL, FALSE)?>" class="frmShop">
<div class="col-xs-5">
         
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?> Giro</h3></div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                    Kode Nota
                </div>
                <div class="col-sm-7 col-md-8">
                    <input class="form-control" name="txtCode" type="text" value="<?=$arrDataDetail['code']?>" readonly />
                    <input type="hidden" name="intID" value="<?=$arrDataDetail['id']?>">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                   Ref. Giro
                </div>
                <div class="col-sm-7 col-md-8">
                <input class="form-control" name="txtRefGiro" type="text" value="<?=$arrDataDetail['ref_giro']?>" readonly />
                </div>
                <div class="clearfix"></div>
            </div>                        
            <!-- <p class="spacer">&nbsp;</p> -->
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                   Tgl. Jatuh Tempo
                </div>
                <div class="col-sm-7 col-md-8">
                    <input type="text" id="tanggalJatuhTempo" name="txtDueDate" value="<?=$arrDataDetail['jatuhtempo']?>" class="form-control required" readonly/>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                    Nominal
                </div>
                <div class="col-sm-7 col-md-8">
                     <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                        <input id="amount" type="text" readonly class="form-control currency" name="amount" style="z-index:0" value="<?=$arrDataDetail['total']?>" readonly> 
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                    Tgl. Approval
                </div>
                <div class="col-sm-7 col-md-8">                  
                    <input type="text" id="tanggalApproval" name="txtDateApproval" value="<?=date_format(date_create($arrDataDetail['appgirodate']), "Y-m-d")?>" class="form-control required jwDateTime" disabled/>               
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 control-label">
                    Status
                </div>
                <div class="col-sm-7 col-md-8">  
                    <label class="radio-inline">
                      <input type="radio" name="appGiro" id="inlineRadio1" value="<?=$arrDataDetail['code']?>-2" <?=($arrDataDetail['status_giro'] == 2) ? "checked" : null ?> disabled > Kliring
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="appGiro" id="inlineRadio2" value="<?=$arrDataDetail['code']?>-1" <?=($arrDataDetail['status_giro'] == 1) ? "checked" : null ?> disabled > Bounce
                    </label>                 
                </div>
                </div>
                <div class="clearfix"></div>
            </div>               
            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->    
    </div>

    <div class="col-xs-12">
         <?php if($bolAllowUpdate):?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" name="smtEditApprovalGiro" id="smtEditApprovalGiro" value="Approval Giro" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button> 
                    <button type="submit" id="btnUpdate" name="smtUpdateApproval" value="Update Approval" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                    <?php if($bolAllowDelete):?>
                    <button type="submit" id="btnDelete" name="smtDeleteApproval" value="Delete Approval" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                           
                    <?php endif; ?>
                </div>
            </div>
        </div>     
        <?php endif; ?>
    </div>

</form></div>