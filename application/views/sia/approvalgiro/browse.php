<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php
$strSearchAction = site_url('approvalgiro/'.$giroType.'/browse', NULL, FALSE);
//include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchbpb.php'); ?>

<div class="col-xs-12">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('approvalgiro/'.$giroType.'/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" >
                <thead>
                <tr>
                    <th>Kode Nota</th>
                    <th>Ref. Giro</th>
                    <th>Jatuh Tempo</th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                    <th>Tgl. Approval</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="tbDetailGiro">
                    <?php if(!empty($arrApproved)):foreach($arrApproved as $e):?>
                    <tr>
                        <td><?=$e['code']?></td>
                        <td><?=$e['ref_giro']?></td>
                        <td><?=formatDate2($e['date'], 'd F Y')?></td>
                        <td><?=setPrice($e['total'])?></td>
                        <td><?=formatDate2($e['appgirodate'], 'd F Y')?></td>
                        <td><?=statusGiro($e['status_giro'])?></td>
                        <td><a href="<?=site_url('approvalgiro/'.$giroType.'/view/'.$e['id'])?>"><i class="fa fa-eye"></i></a></td>
                    </tr>
                    <?php endforeach; else:?>
                    <tr class="info"><td class="noData" colspan="13"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                    <?php endif;?>
                </tbody>
            </table></div>
    <?=$strPage?>
</div>