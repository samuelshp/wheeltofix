<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> Approval Giro', 'link' => site_url('approvalgiro/'.$giroType.'/browse', NULL, FALSE)),    
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12">
    <form name="frmApprovalGiro" id="frmApprovalGiro" method="post" action="<?=site_url('approvalgiro/'.$giroType, NULL, FALSE)?>" class="frmShop">    

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?> Giro</h3></div>
        <div class="panel-body" id="detailDPH">
            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" >
                <thead>
                <tr>
                    <th>Kode Nota</th>
                    <th>Ref. Giro</th>
                    <th>Jatuh Tempo</th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                    <th>Tgl. Approval</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody id="tbDetailGiro">
                    <?php if(!empty($arrDataGiro)): foreach($arrDataGiro as $i => $e):?>
                    <tr>
                        <td><?=$e['code']?></td>
                        <td><?=$e['ref_giro']?></td>
                        <td><?=formatDate2($e['jatuhtempo'], 'd F Y')?></td>
                        <td><?=setPrice($e['total'])?></td>
                        <td><input type="text" name="txtApprovalDate[]" class="form-control input-sm jwDateTime"></td>
                        <td>
                            <label class="radio-inline">
                              <input type="radio" name="appGiro[<?=$i?>]" id="inlineRadio1" value="<?=$e['code']?>-2"> Kliring
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="appGiro[<?=$i?>]" id="inlineRadio2" value="<?=$e['code']?>-1"> Bounce
                            </label>                            
                        </td>
                        <!-- <td><button>Reset</button></td> -->
                    </tr>
                    <?php endforeach; ?>                    
                    <?php else:?>
                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                    <?php endif;?>
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>

    <?php if($bolAllowInsert):?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <button type="submit" name="smtSaveApprovalGiro" id="smtSaveApprovalGiro" value="Make Approval Giro" class="btn btn-primary">
                    <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                </button>                    
            </div>
        </div>
    </div>     
    <?php endif; ?>

</form></div>