<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> Posting Transaksi', 'link' => '')    
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<form method="POST" action="" class="form-horizontal">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"> Input Periode Posting</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputPeriode" class="col-sm-2 control-label">Periode*</label>
                            <div class="col-sm-10">
                              <input type="number" class="form-control" id="inputPeriode" name="periode" placeholder="Periode">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputTahun" class="col-sm-2 control-label">Tahun*</label>
                            <div class="col-sm-10">
                              <input type="number" class="form-control" id="inputTahun" name="tahun" placeholder="Tahun">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-primary" name="btnPostingTransaksi" value="Posting Transaksi">OK</button>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</form>