<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('report/penerimaanpengeluaranproyek/', NULL, FALSE))
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmGenerateHutang" id="frmGenerateHutang" method="post" action="<?=site_url('report/penerimaanpengeluaranproyek/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Filter</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-report')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">                       
                                <label class="radio-inline">
                                    <input type="radio" name="strType" value="rekap" <?=($strType == 'rekap') ? "checked" : null ?> > Rekap
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="strType" value="detail" <?=($strType == 'detail') ? "checked" : null ?>> Detail
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>                                                
                        <div class="form-group" id="dateend">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-dateend')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDateEnd" type="text" autocomplete="off" value="<?=(!empty($txtDateEnd)) ? $txtDateEnd : date('Y-m-d')?>" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="detail" style="display: <?=($strType == 'detail') ? 'block' : 'none'; ?>">
                            <div class="form-group">
                                <div class="col-sm-5 col-md-3 control-label">
                                    Kontrak
                                </div>
                                <div class="col-sm-7 col-md-9">
                                    <select class="form-control chosen" name="idKontrak" id="idKontrak">
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrProject as $e):?>
                                            <option value="<?=$e['id']?>" <?=($idKontrak == $e['id']) ? "selected" : null ?> ><?=$e['kont_name']?></option>
                                        <?php endforeach;?>
                                        <option value="%" <?=($idKontrak == "%") ? "selected" : null ?>>Semua</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5 col-md-3 control-label">
                                    Jenis
                                </div>
                                <div class="col-sm-7 col-md-9">
                                    <select class="form-control chosen" name="jenis" id="jenis">
                                        <option value="0" disabled selected>Select your option</option>
                                        <option value="prestasi_before_thn_ini" <?=($jenis == "prestasi_before_thn_ini") ? "selected" : null ?> >Prestasi Sebelum Tahun Ini</option>
                                        <option value="termin_ditagihkan_thn_ini" <?=($jenis == "termin_ditagihkan_thn_ini") ? "selected" : null ?>>Termin Ditagihkan Tahun Ini</option>
                                        <option value="penerimaan_before_thn_ini" <?=($jenis == "penerimaan_before_thn_ini") ? "selected" : null ?>>Penerimaan Sebelum Tahun Ini</option>
                                        <option value="penerimaan_thn_ini" <?=($jenis == "penerimaan_thn_ini") ? "selected" : null ?>>Penerimaan Tahun Ini</option>
                                        <option value="biaya_before_thn_ini" <?=($jenis == "biaya_before_thn_ini") ? "selected" : null ?>>Biaya Sebelum Tahun Ini</option>
                                        <option value="biaya_thn_ini" <?=($jenis == "biaya_thn_ini") ? "selected" : null ?>>Biaya Tahun Ini</option>
                                        <option value="%" <?=($jenis == "%") ? "selected" : null ?>>Semua</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>                        
                </div>
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtProcessType" id="smtGenerateReportMutasiBiaya" value="Generate" class="btn btn-primary">
                        Generate
                    </button>                    
                </div>
            </div>
        </div>    

</form>

<div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-th-list"></i> <?=$strPageMode?>
                <div class="btn-print-group pull-right">
                    <button type="button" name="subPrint" value="Print" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-print"></i> Print</button>
                    <button type="button" name="subPrint" value="Excel" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-file-excel-o"></i> Export To CSV</button>
                </div>
            </h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive" id="section-to-print">
                <div class="hidden">
                    <h3><b><?=$this->config->item('jw_website_name')?></b></h3>
                    <h3><?=$strPageMode?></h3>
                    <p><?=$dataPrint?></p>
                    <small>Created at : <?=date('Y-m-d H:i:s')?></small>
                </div>                
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <?php if(!empty($arrReport)) {
                            for ($i=0; $i < $th_span['tr_count']; $i++) {
                        ?>
                            <tr>
                                <?php foreach ($th_span['fields'][$i] as $key => $value): ?>
                                        <th style="text-align: center; vertical-align: middle;" <?=(!empty($value['type']) ? $value['type']."span='".$value['count']."'" : null )?> >
                                            <?=str_replace('_',' ',str_replace(array('kiri','kanan'),'',$value['field']))?>
                                        </th>
                                <?php endforeach; ?>
                            </tr>
                        <?php                             
                            }}else{ ?>
                            <tr><th>Data</th></tr>
                        <?php } ?>                        
                    </thead>                   
                    <tbody>
                        <?php if(!empty($arrReport)) {
                            $separator = '';
                            if(!empty($th_separator)) $separator = $arrReport[0][$th_separator];
                            $subtotal = 0;
                            $total = 0;
                            foreach ($arrReport as $e) {
                                $bolBold = false;
                                if(isset($bold)) {
                                    foreach($th as $v) { if(in_array($e[$v],$bold)) {
                                        $bolBold = true;
                                        break;
                                    }}
                                }
                                if(!empty($th_subtotal)) {
                                    if($e[$th_separator] != $separator) {
                                        echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>SUBTOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($subtotal,'BASE',false).'</b></td></tr>';
                                        $subtotal = $e[$th_subtotal];
                                        $total += $e[$th_subtotal];
                                        $separator = $e[$th_separator];
                                    } else {
                                        $subtotal += $e[$th_subtotal];
                                        $total += $e[$th_subtotal];
                                    }
                                }
                                echo '<tr>';
                                foreach($th as $v) { if(in_array($v,$setPrice)) {?>
                                    <td><?php if($bolBold) echo '<b>'.setPrice($e[$v],'',false).'</b>'; else echo setPrice($e[$v],'',false);?></td>
                                <?php } else { ?>
                                    <td><?php if($bolBold) echo '<b>'.str_replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;', $e[$v]).'</b>'; else echo str_replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;', str_replace('_',' ',str_replace(array('kiri','kanan'),'',$e[$v])));?></td>
                                <?php } }
                                echo '</tr>';
                            }
                            // if(!empty($th_subtotal)) {
                            //     echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>SUBTOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($subtotal,'BASE',false).'</b></td></tr>';
                            //     echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>GRAND TOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($total,'BASE',false).'</b></td></tr>';
                            // }
                        } else {?>
                        <th>No Data Available</th>
                        <?php } ?>
                    </tbody>
                </table>
                <?php if(isset($dataPrintFooter)) echo $dataPrintFooter;?>
            </div>
        </div>
    </div>
<script>
$("button[name='subPrint'][value='Excel']").click(function() {
    $("#section-to-print table").table2excel({
        exclude: ".no-excel",
        name: "<?=$strPageTitle?>",
        filename: "<?=$strPageMode?>-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
});
</script>

</div>
