<div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-th-list"></i> <?=$strPageMode?>
                <div class="btn-print-group pull-right">
                    <button type="button" name="subPrint" value="Print" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-print"></i> Print</button>
                    <button type="button" name="subPrint" value="Excel" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-file-excel-o"></i> Export To CSV</button>
                </div>
            </h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive" id="section-to-print">
                <div class="hidden">
                    <h3><b><?=$this->config->item('jw_website_name')?></b></h3>
                    <h3><?=$strPageMode?></h3>
                    <p><?=$dataPrint?></p>
                    <small>Created at : <?=date('Y-m-d H:i:s')?></small>
                </div>
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <?php if(!empty($arrReport)) {
                                foreach ($th as $e) { ?>
                                    <th><?=ucwords(strtolower(str_replace('_',' ',str_replace(array('kiri','kanan'),'',$e))))?></th>
                            <?php }} else {?>
                                <th>Data</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($arrReport)) {
                            $separator = '';
                            if(!empty($th_separator)) $separator = $arrReport[0][$th_separator];
                            $subtotal = 0;
                            $total = 0;
                            foreach ($arrReport as $e) {
                                $bolBold = false;
                                if(isset($bold)) {
                                    foreach($th as $v) { if(in_array($e[$v],$bold)) {
                                        $bolBold = true;
                                        break;
                                    }}
                                }
                                if(!empty($th_subtotal)) {
                                    if($e[$th_separator] != $separator) {
                                        echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>SUBTOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($subtotal,'BASE',false).'</b></td></tr>';
                                        $subtotal = $e[$th_subtotal];
                                        $total += $e[$th_subtotal];
                                        $separator = $e[$th_separator];
                                    } else {
                                        $subtotal += $e[$th_subtotal];
                                        $total += $e[$th_subtotal];
                                    }
                                }
                                echo '<tr>';
                                foreach($th as $v) { if(in_array($v,$setPrice)) {?>
                                    <td><?php if($bolBold) echo '<b>'.setPrice($e[$v],'',false).'</b>'; else echo setPrice($e[$v],'',false);?></td>
                                <?php } else { ?>
                                    <td><?php if($bolBold) echo '<b>'.str_replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;', $e[$v]).'</b>'; else echo str_replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;', $e[$v]);?></td>
                                <?php } }
                                echo '</tr>';
                            }
                            // if(!empty($th_subtotal)) {
                            //     echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>SUBTOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($subtotal,'BASE',false).'</b></td></tr>';
                            //     echo '<tr><td class="text-right" colspan="'.array_search($th_subtotal, $th).'"><b>GRAND TOTAL</b></td><td colspan="'.(count($th)-array_search($th_subtotal, $th)).'"><b>'.setPrice($total,'BASE',false).'</b></td></tr>';
                            // }
                        } else {?>
                        <th>No Data Available</th>
                        <?php } ?>
                    </tbody>
                </table>
                <?php if(isset($dataPrintFooter)) echo $dataPrintFooter;?>
            </div>
        </div>
    </div>
<script>
$("button[name='subPrint'][value='Excel']").click(function() {
    $("#section-to-print table").table2excel({
        exclude: ".no-excel",
        name: "<?=$strPageTitle?>",
        filename: "<?=$strPageMode?>-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
});
</script>