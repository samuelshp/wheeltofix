<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy-mm-dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);

$(document).ready(function(){
    
});

 $('input[name="intMode"]').change(function(){
    var intMode = $(this).val();
    if (intMode == 1) { 
        $("#supplier").removeClass('hidden');
        $("#dateStart").removeClass('hidden');
        $("#project").removeClass('hidden');
    } else if (intMode == 2){        
        $("#supplier").removeClass('hidden');
        $("#dateStart").addClass('hidden');
        $("#project").removeClass('hidden');
    } else {
        $("#supplier").removeClass('hidden');
        $("#dateStart").removeClass('hidden');
        // $("#project").addClass('hidden');
    }
});

$(".chosen").chosen({width: "100%",search_contains: true});
</script>