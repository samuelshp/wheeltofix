<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('report/hutangbelumdifakturkan/', NULL, FALSE))
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmGenerateKas" id="frmGenerateKas" method="post" action="<?=site_url('report/hutangbelumdifakturkan/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Filter</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-report')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">                       
                                <label class="radio-inline">
                                    <input type="radio" name="intMode" id="frmPembayaranPiutang" value="2" <?=($intMode == 2) ? "checked" : null ?>> Rekap Hutang
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="intMode" id="frmNota" value="1" <?=($intMode == 1) ? "checked" : null ?>> Detail Hutang
                                </label>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group" id="proyek">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-project')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">                                
                                <select type="text" id="selSupplier" name="intProjectID" class="form-control chosen">
                                    <option value="%" <?=($intProjectID == $e['id'])? "selected" : null ?>>All Project</option>
                                    <?php foreach($arrProject as $e) { ?>
                                        <option value="<?=$e['id']?>" <?=($intProjectID == $e['id'])? "selected" : null ?> ><?=$e['kont_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>                        
                        <div class="form-group" id="datestart">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-datestart')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDateStart" type="text" autocomplete="off" value="<?=$strStartDate?>" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group" id="dateend">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-dateend')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDateEnd" type="text" autocomplete="off" value="<?=$strDateEnd?>" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>                        
                </div>
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtProcessType" id="smtGenerateReportKas" value="Generate" class="btn btn-primary">
                        Generate
                    </button>                    
                </div>
            </div>
        </div>    

</form>
<?php include_once(APPPATH.'views/'.$this->config->item('jw_ui_style').'/report/resultreport.php'); ?>
</div>