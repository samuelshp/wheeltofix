<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy-mm-dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);

$(document).ready(function(){
    $("button[name='subPrint'][value='Print']").click(function() {
        window.print();
    }); 
    var intMode = $('input[name="intMode"]:checked').val();
    if (intMode == 1) { 
        $("#dateStart").removeClass('hidden');
        $("#customer").removeClass('hidden');
        $("#project").removeClass('hidden');
    } else if (intMode == 2){        
        $("#dateStart").addClass('hidden');
        $("#customer").removeClass('hidden');
        $("#project").removeClass('hidden');
    } else {
        $("#dateStart").removeClass('hidden');
        $("#customer").removeClass('hidden');
        // $("#project").addClass('hidden');
    }
});

 $('input[name="intMode"]').change(function(){
    var intMode = $(this).val();
    if (intMode == 1) { 
        $("#dateStart").removeClass('hidden');
        $("#customer").removeClass('hidden');
        $("#project").removeClass('hidden');
    } else if (intMode == 2){        
        $("#dateStart").addClass('hidden');
        $("#customer").removeClass('hidden');
        $("#project").removeClass('hidden');
    } else {
        $("#dateStart").removeClass('hidden');
        $("#customer").removeClass('hidden');
        // $("#project").addClass('hidden');
    }
});

$(".chosen").chosen({width: "100%",search_contains: true});
</script>