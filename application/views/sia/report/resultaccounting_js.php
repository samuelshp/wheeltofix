<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy-mm-dd", showTimepicker: false });
$(".jwDateTimePeriod").datetimepicker({ dateFormat: "yy-mm", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);

$(document).ready(function(){
	$("button[name='subPrint'][value='Print']").click(function() {
	    window.print();
	});  
	var intMode = $('input[name="intMode"]:checked').val();
    if (intMode == 1) {
        $("#datestart").removeClass('hidden');
        $("#dateend").removeClass('hidden');
        $("#accountstart").removeClass('hidden');
        $("#accountend").removeClass('hidden');
        $("#transactioncode").addClass('hidden');
        $("#periode").addClass('hidden');
    } else if (intMode == 2){
        $("#datestart").addClass('hidden');
        $("#dateend").addClass('hidden');
        $("#accountstart").addClass('hidden');
        $("#accountend").addClass('hidden');
        $("#transactioncode").addClass('hidden');
        $("#periode").removeClass('hidden');
    } else if (intMode == 3){
        $("#datestart").addClass('hidden');
        $("#dateend").addClass('hidden');
        $("#accountstart").addClass('hidden');
        $("#accountend").addClass('hidden');
        $("#transactioncode").addClass('hidden');
        $("#periode").removeClass('hidden');
    } else if (intMode == 4){
        $("#datestart").removeClass('hidden');
        $("#dateend").removeClass('hidden');
        $("#accountstart").addClass('hidden');
        $("#accountend").addClass('hidden');
        $("#transactioncode").removeClass('hidden');
        $("#periode").addClass('hidden');
    } else {
        $("#datestart").addClass('hidden');
        $("#dateend").addClass('hidden');
        $("#accountstart").addClass('hidden');
        $("#accountend").addClass('hidden');
        $("#transactioncode").addClass('hidden');
        $("#periode").removeClass('hidden');
    }
	$('input[name="intMode"]').change(function(){
		var intMode = $('input[name="intMode"]:checked').val();
	    if (intMode == 1) {
	        $("#datestart").removeClass('hidden');
	        $("#dateend").removeClass('hidden');
	        $("#accountstart").removeClass('hidden');
	        $("#accountend").removeClass('hidden');
	        $("#transactioncode").addClass('hidden');
	        $("#periode").addClass('hidden');
	    } else if (intMode == 2){
	        $("#datestart").addClass('hidden');
	        $("#dateend").addClass('hidden');
	        $("#accountstart").addClass('hidden');
	        $("#accountend").addClass('hidden');
	        $("#transactioncode").addClass('hidden');
	        $("#periode").removeClass('hidden');
	    } else if (intMode == 3){
	        $("#datestart").addClass('hidden');
	        $("#dateend").addClass('hidden');
	        $("#accountstart").addClass('hidden');
	        $("#accountend").addClass('hidden');
	        $("#transactioncode").addClass('hidden');
	        $("#periode").removeClass('hidden');
	    } else if (intMode == 4){
	        $("#datestart").removeClass('hidden');
	        $("#dateend").removeClass('hidden');
	        $("#accountstart").addClass('hidden');
	        $("#accountend").addClass('hidden');
	        $("#transactioncode").removeClass('hidden');
	        $("#periode").addClass('hidden');
	    } else {
	        $("#datestart").addClass('hidden');
	        $("#dateend").addClass('hidden');
	        $("#accountstart").addClass('hidden');
	        $("#accountend").addClass('hidden');
	        $("#transactioncode").addClass('hidden');
	        $("#periode").removeClass('hidden');
	    }
	});
});

$(".chosen").chosen({width: "100%",search_contains: true});
</script>