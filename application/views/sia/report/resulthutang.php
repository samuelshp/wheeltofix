<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('report/hutang/', NULL, FALSE))
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmGenerateHutang" id="frmGenerateHutang" method="post" action="<?=site_url('report/hutang/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Filter</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-report')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">                       
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmPembayaranHutang" value="1" <?php if($intMode == 1) echo 'checked';?> > Rekap Hutang
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmNota" value="2" <?php if($intMode == 2) echo 'checked';?> > Detail Hutang
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmPengeluaranLain" value="3" <?php if($intMode == 3) echo 'checked';?> > Kartu Hutang
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="dateStart">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-datestart')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDateStart" type="text" value="<?=date('Y-m-d',strtotime($txtDateStart))?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="dateEnd">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-dateend')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDateEnd" type="text" value="<?=date('Y-m-d',strtotime($txtDateEnd))?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group hidden" id="supplier">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-supplier')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select type="text" id="selSupplier" name="intSupplierID" class="form-control chosen">
                                <option value="0" selected>All Supplier</option>
                                <?php foreach($arrSupplier as $e) { ?>
                                    <option <?php if($e['id']==$intSupplierID) echo 'selected';?> value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="project">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-project')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select type="text" name="intProjectID" class="form-control chosen">
                                <option value="0" selected>All Project</option>
                                <?php foreach($arrProject as $e) { ?>
                                    <option <?php if($e['id']==$intProjectID) echo 'selected';?> value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>                        
                </div>
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtProcessType" id="smtGenerateReportHutang" value="Generate" class="btn btn-primary">
                        Generate
                    </button>                    
                </div>
            </div>
        </div>    

</form>
<?php include_once(APPPATH.'views/'.$this->config->item('jw_ui_style').'/report/resultreport.php'); ?>
</div>
