<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('report/piutang/', NULL, FALSE))
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmGeneratePiutang" id="frmGeneratePiutang" method="post" action="<?=site_url('report/piutang/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Filter</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-report')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">                       
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmPembayaranPiutang" value="1" checked > Rekap Piutang
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmNota" value="2" > Detail Piutang
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="intMode" id="frmPengeluaranLain" value="3" > Kartu Piutang
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="dateStart">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-datestart')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDateStart" type="text" value="<?=date('Y-m-d')?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="dateEnd">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-dateend')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDateEnd" type="text" value="<?=date('Y-m-d')?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="customer">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-customer')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select type="text" id="selCustomer" name="intCustomerID" class="form-control chosen">
                                <option value="0" selected>All Customer</option>
                                <?php foreach($arrCustomer as $e) { ?>
                                    <option value="<?=$e['id']?>"><?=$e['cust_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group" id="project">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-project')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select type="text" name="intProjectID" class="form-control chosen">
                                <option value="0" selected>All Project</option>
                                <?php foreach($arrProject as $e) { ?>
                                    <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>                        
                </div>
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtProcessType" id="smtGenerateReportPiutang" value="Generate" class="btn btn-primary">
                        Generate
                    </button>                    
                </div>
            </div>
        </div>    

</form></div>