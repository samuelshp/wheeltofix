<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ 
	dateFormat: "yy-mm-dd", 
	showTimepicker: false,
	beforeShow: function (input, inst) {
        var rect = input.getBoundingClientRect();
        setTimeout(function () {
	        inst.dpDiv.css({ top: rect.top + 30});
        }, 0);
    }
});
$(".jwDateTimePeriod").datetimepicker({ dateFormat: "yy-mm", showTimepicker: false });

$("input[name=strType][type=radio]").click(function() {
	if ($(this).val() == 'detail') {
		$("div#detail").show();
	}else{
		$("div#detail").hide();
	}
});

$(document).ready(function(){
	$("button[name='subPrint'][value='Print']").click(function(){
		$("#section-to-print").removeClass('table-responsive');	   
		window.print();
		$("#section-to-print").addClass('table-responsive');
	});
});

$(".chosen").chosen({width: "100%",search_contains: true});
</script>