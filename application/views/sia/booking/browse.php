<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-calendar"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<style>
    .table-condensed > tbody > tr > th, .table-condensed > tbody > tr > td {padding:2px;}
    .tbBooking {margin:0px; width:100%; height:100%;}
    .tbBooking th, .tbBooking td {margin:0px; padding:0px; width:<?=100 / $intField?>%;}
    .tbBooking td {height:12px; font-size:10px; text-align:center;}
    .tbBooking td:not(:last-of-type) {border-right:#fff 1px solid;}
    .tbBooking tr:not(:last-of-type) td {border-bottom:#fff 1px solid;}
    .tbBooking td.occupied:hover {cursor:pointer; background-color:#357EBD;}
</style>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-lastbookingorder')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- View -</option><?php
            if(!empty($arrBookingList)) foreach($arrBookingList as $e):
                if($e['cust_name']==''){
                    $strListTitle = $e['book_customer_name'].', '.$e['book_customer_phone'].', '.$e['book_from'].' - '.$e['book_until'].', lapangan:'.$e['book_lapangan_id'];
                }else{
                    $strListTitle = $e['cust_name'].', '.$e['cust_phone'].', '.$e['book_from'].' - '.$e['book_until'].', lapangan:'.$e['book_lapangan_id'];
                }
                 ?>
            <option value="<?=site_url('booking/view/'.$e['id'], NULL, FALSE)?>"><?=$strListTitle?></option><?php
            endforeach; ?>
        </select>
    </div></div>
<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('booking/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
            <input type="text" name="txtSearchValue" value="" placeholder="Go To Date" class="jwDateTime form-control"/>
            <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
        </div>
    </form>

    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><ul class="pagination">
        <li><a href="<?=site_url('booking/browse/?page='.($intPage-1), NULL, FALSE)?>">← Previous</a></li>
        <li class="active"><a href="<?=site_url('booking/browse/?page=0', NULL, FALSE)?>">This Week</a></li>
        <li><a href="<?=site_url('booking/browse/?page='.($intPage+1), NULL, FALSE)?>">Next →</a></li>
    </ul></div>
    
    <div class="col pull-left" style="text-align:right; margin-left:10px;"><a href="<?=site_url('booking', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    
    <p class="spacer">&nbsp;</p>

    <div class="table-responsive"><table class="table table-bordered table-condensed">
		<thead><tr>
            <th>Time</th>
			<th>Sunday,<br /><?=formatDate2($arrHari[0]['strDate'],'d F Y')?></th>
            <th>Monday,<br /><?=formatDate2($arrHari[1]['strDate'],'d F Y')?></th>
            <th>Tuesday,<br /><?=formatDate2($arrHari[2]['strDate'],'d F Y')?></th>
            <th>Wednesday,<br /><?=formatDate2($arrHari[3]['strDate'],'d F Y')?></th>
            <th>Thursday,<br /><?=formatDate2($arrHari[4]['strDate'],'d F Y')?></th>
            <th>Friday,<br /><?=formatDate2($arrHari[5]['strDate'],'d F Y')?></th>
            <th>Saturday,<br /><?=formatDate2($arrHari[6]['strDate'],'d F Y')?></th>
		</tr></thead>
		<tbody><?php
		for($i = 0; $i < 24; $i++) { ?>  
			<tr><?php
            for($j = 0; $j < 8; $j++) {
                if(!empty($arrHari[$j - 1]) && formatDate2($strSearchedDate,'Y-m-d') == formatDate2($arrHari[$j - 1]['strDate'],'Y-m-d')) $strBgColor = '#fed';
                else $strBgColor = '#fff'; ?>  
                <td height="50px" bgcolor="<?=$strBgColor?>"><?php
                if($j == 0) echo $i.":00";
                else { 
                    echo '<table class="tbBooking">'; 
                    for($k=0; $k < 60; $k += 15) { ?>
                        <tr><?php 
                        for($l = 1; $l <= $intField; $l++) { 
                            $intArrBookKey = -1; 
                            if(!empty($arrHari[$j - 1])) $intArrBookKey = $arrHari[$j - 1][$i][$k][$l];
                            if($intArrBookKey > -1) {
                                switch($l) {
                                    case 1: $strBgColor = '#333'; $strColor = '#fff'; break;
                                    case 2: $strBgColor = '#555'; $strColor = '#fff'; break;
                                    case 3: $strBgColor = '#777'; $strColor = '#fff'; break;
                                    case 4: $strBgColor = '#999'; $strColor = '#000'; break;
                                    case 5: $strBgColor = '#bbb'; $strColor = '#000'; break;
                                    case 6: $strBgColor = '#ddd'; $strColor = '#000'; break;
                                    default: $strBgColor = '#fff'; $strColor = '#000'; break;
                                }
                                $strClass = ' class="occupied"';
                                $strTitle = $arrBooking[$intArrBookKey]['keterangan'];
                                $strID = $arrBooking[$intArrBookKey]['id'];
                            } else {
                                $strBgColor = '#fff'; $strColor = '#000'; $strClass = ''; $strTitle = ''; $strID = '';
                            }
                            ?>  
                            <td data-toggle="tooltip"<?=$strClass?> title="<?=$strTitle?>" id="<?=$strID?>" style="background-color:<?=$strBgColor?>; color:<?=$strColor?>;">
                                <?=$l?>
                            </td><?php 
                        } ?>
                        </tr><?php 
                    }
                    echo"</table>";
                } ?>  
                </td><?php
            } ?>  
			</tr><?php
        } ?>  
		</tbody>
	</table></div>
    
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><ul class="pagination">
        <li><a href="<?=site_url('booking/browse/?page='.($intPage-1), NULL, FALSE)?>">← Previous</a></li>
        <li class="active"><a href="<?=site_url('booking/browse/?page=0', NULL, FALSE)?>">This Week</a></li>
        <li><a href="<?=site_url('booking/browse/?page='.($intPage+1), NULL, FALSE)?>">Next →</a></li>
    </ul></div>
</div><!--
--><?/*=site_url('purchase/view/'.$e['id'].'?print=true')*/?>