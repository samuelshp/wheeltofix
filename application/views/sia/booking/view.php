<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-calendar"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-booking'), 'link' => site_url('booking/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-lastbookingorder')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- View -</option><?php
            if(!empty($arrBookingList)) foreach($arrBookingList as $e):
                if($e['cust_name']==''){
                    $strListTitle = $e['book_customer_name'].', '.$e['book_customer_phone'].', '.$e['book_from'].' - '.$e['book_until'].', lapangan:'.$e['book_lapangan_id'];
                }else{
                    $strListTitle = $e['cust_name'].', '.$e['cust_phone'].', '.$e['book_from'].' - '.$e['book_until'].', lapangan:'.$e['book_lapangan_id'];
                }
                ?>
                <option value="<?=site_url('booking/view/'.$e['id'], NULL, FALSE)?>"><?=$strListTitle?></option><?php
            endforeach; ?>
        </select>
</div></div>

<div class="col-xs-12"><form name="frmChangeBooking" id="frmChangeBooking" method="post" action="<?=site_url('booking/view/'.$intBookingID, NULL, FALSE)?>" class="frmShop">
<!-- Header Faktur -->
        <div class="row">
            <!--data customer-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
                    <div class="panel-body">
                        <?php
                        if($arrBookingData['cust_id']=='0'): ?>
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                            <div class="col-xs-8"><?=$arrBookingData['book_customer_name']?></div>
                            <p class="spacer">&nbsp;</p><?php
                        endif; ?>
                        <?php if($arrBookingData['cust_id']!='0'): ?>

                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                            <div class="col-xs-8"><?=$arrBookingData['cust_name']?></div>
                            <p class="spacer">&nbsp;</p><?php
                        endif; ?>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></div>
                        <div class="col-xs-8"><?=$arrBookingData['cust_address']?>, <?=$arrBookingData['cust_city']?></div>
                        <p class="spacer">&nbsp;</p>
                        <?php
                        if($arrBookingData['cust_id']=='0'): ?>
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
                            <div class="col-xs-8"><?=$arrBookingData['book_customer_phone']?></div>
                            <p class="spacer">&nbsp;</p><?php
                        endif; ?>
                        <?php if($arrBookingData['cust_id']!='0'): ?>
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
                            <div class="col-xs-8"><?=$arrBookingData['cust_phone']?></div>
                            <p class="spacer">&nbsp;</p><?php
                        endif; ?>
                        <!-- <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></div>
                        <div class="col-xs-8"><?php
                            switch($arrBookingData['cust_outlettype']) {
                                case 1: echo "RETAIL"; break;
                                case 2: echo "SEMI GROSIR"; break;
                                case 3: echo "STAR OUTLET"; break;
                                case 4: echo "GROSIR"; break;
                                case 5: echo "MODERN"; break;
                            }
                            ?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-limit')?></div> -->
                        <?php
                        if($arrBookingData['cust_id'] == '0'): ?>
                            <!-- <div class="col-xs-8"><label id="limitCustomer">Non Member</label></div>
                            <p class="spacer">&nbsp;</p> --><?php
                        endif; ?>
                        <?php if($arrBookingData['cust_id'] != '0'): ?>
                            <!-- <div class="col-xs-8"><label id="limitCustomer"><?=$arrBookingData['cust_limitkredit']?></label></div>
                            <p class="spacer">&nbsp;</p> --><?php
                        endif; ?>
                    </div>
                </div>
            </div> <!--data customer-->

            <!--data lapangan-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingdata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-lapanganid')?></label></div>
                        <div class="col-xs-8"><label>Lapangan <?=$arrBookingData['book_lapangan_id']?></label></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></label></div>
                        <div class="col-xs-8"><label><?=$arrBookingData['book_product_id']?></label></div>
                        <p class="spacer">&nbsp;</p>
                    <?php if($bolBtnEdit): ?>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingfrom')?></label><input type="text" id="txtFrom" name="txtFrom" value="<?=formatDate2($arrBookingData['book_from'],'Y-m-d H:i')?>" class="form-control required jwDateTime" /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookinguntil')?></label><input type="text" name="txtUntil" id="txtUntil" value="<?=formatDate2($arrBookingData['book_until'],'Y-m-d H:i')?>" class="form-control required jwDateTime" /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon">DP</label><input id="hargaTotal" name="intTotal" class="currency required form-control" value="<?=$arrBookingData['book_total']?>"/></div></div>
                    <?php else: ?>  
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingfrom')?></div>
                        <div class="col-xs-8"><?=formatDate2($arrBookingData['book_from'],'d F Y H:i')?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookinguntil')?></div>
                        <div class="col-xs-8"><?=formatDate2($arrBookingData['book_until'],'d F Y H:i')?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-total')?></div>
                        <div class="col-xs-8"><?=setPrice($arrBookingData['book_total'])?></div>
                        <p class="spacer">&nbsp;</p>
                    <?php endif; ?>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-status')?></div>
                        <div class="col-xs-8"><?=$arrBookingData['book_strstatus']?></div>
                        <p class="spacer">&nbsp;</p>
                        <!-- Dibuat -->
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
                        <div class="col-xs-8"><?=formatDate2($arrBookingData['book_date'],'d F Y H:i')?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div>
            </div> <!--data lapangan-->

            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                    <div class="panel-body">
                        <?php if($bolBtnEdit): ?>
                            <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($arrBookingData['book_description'])) echo $arrBookingData['book_description']; ?></textarea></div>
                        <?php else: ?>
                            <div class="form-group"><?php echo $arrBookingData['book_description'];?></div>
                        <?php endif; ?>
                        </div>
                </div>

            </div>
        </div> <!--class row-->

        <?php
        $rawStatus = $arrBookingData['book_rawstatus'];
        include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php');
        ?>

        <input type="hidden" id="customerID" name="customerID"/>
        <input type="hidden" name="customerOutletType"/>
        <input type="hidden" id= "outletID" name="outletID"/>
        <input type="hidden" name="outletMarketType"/>

</form></div>