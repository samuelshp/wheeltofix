<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-calendar"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-booking'), 'link' => site_url('booking/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- BODY -->
<div class="col-xs-12">
    <form name="frmAddBooking" id="frmAddBooking" class="frmShop" method="post" action="<?=site_url('booking', NULL, FALSE)?>">
        <div class="row">
            <!--data customer-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
                    <div class="panel-body">
                        <input type="radio" name="radioCustomer" value="0"> Non-Member
                        <input type="radio" name="radioCustomer" value="1" checked> Member <br>
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?> <a href="<?=site_url('adminpage/table/add/33', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="form-group"><div class="input-group"><input type="text" id="customerName" name="customerName" class="form-control required" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"/><label class="input-group-addon" id="loadCustomer"></label></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></label><input id="customerAddress" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-city')?></label><input id="customerCity" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></label><input id="customerPhone" name="customerPhone" class="form-control" disabled /></div></div>

                        <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></label><input id="customerOutletTypeStr" class="form-control" disabled/></div></div> -->
                        <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-limit')?></label><input id="customerLimit" class="form-control" disabled/></div></div> -->
                    </div>
                </div>
            </div> <!--data customer-->

            <!--data lapangan-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingdata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-pleaseselectfield')?> </h4>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-lapanganid')?></label>
                            <select name="intLapanganID" id="intLapanganID" class="form-control"><?php
for ($i = 1; $i <= $intField; $i++): ?>
                                <option value="<?=$i?>">Lapangan <?=$i?></option><?php
endfor; ?>  
                            </select>
                        </div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></label>
                            <select name="intProductID" id="intProductID" class="form-control"><?php
foreach($arrProduct as $e): ?>  
                                <option value="<?=$e['id']?>"><?=$e['prod_title']?></option><?php
endforeach; ?>  
                            </select>
                        </div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingfrom')?></label><input type="text" id="txtFrom" name="txtFrom" value="<?=date('Y-m-d H:i'); ?>" class="form-control required jwDateTime" /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookinguntil')?></label><input type="text" name="txtUntil" id="txtUntil" value="<?=date('Y-m-d H:i'); ?>" class="form-control required jwDateTime" /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon">DP</label><input id="hargaTotal" name="txtTotal" class="currency required form-control" /></div></div>
                        <div class="form-group" style="display:none;"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-status')?></label>
                            <!--<input id="idLapangan" class="form-control" />-->
                            <select name="intStatus" class="form-control">
                                <?php
                                for($i=0;$i<count($arrBookingStatus);$i++){?>
                                    <option value="<?php echo $arrBookingStatus[$i]['strKey']; ?>"><?php echo $arrBookingStatus[$i]['strData']; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div></div>
                    </div>
                </div>
            </div> <!--data lapangan-->

            <!--data salesman-->
            <div class="col-md-4">

                <!--tanggal-->
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                    <div class="panel-body">
                        <!-- Dibuat -->
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                        <!--<div class="form-group"><div class="input-group"><label class="input-group-addon"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')*/?></label><input type="text" id="txtExpDate" name="txtExpDate" value="<?php /*if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); */?>" class="form-control required jwDateTime" /></div></div>
-->
                    </div>
                </div> <!--tanggal-->

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                    <div class="panel-body">
                        <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
                    </div>
                </div>

            </div> <!--data salesman-->
        </div> <!--class row-->


        <div class="form-group"><button type="submit" name="smtMakeBooking" value="Make Invoice" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

        <input type="hidden" id="customerID" name="customerID"/>
        <input type="hidden" name="customerOutletType"/>
        <input type="hidden" id= "outletID" name="outletID"/>
        <input type="hidden" name="outletMarketType"/>
    </form>
</div>