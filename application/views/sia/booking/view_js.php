<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<!--<script type="text/javascript" src="<?/*=base_url('asset/jquery/moment-2.4.0.js')*/?>"></script>-->
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$("select[name='selJumpTo']").change(function() {
    if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});
$('.currency').autoNumeric('init', autoNumericOptionsRupiah);
$("#customerLimit").autoNumeric('init', autoNumericOptionsRupiah);
$(".jwDateTime").datetimepicker({ 
    dateFormat: "yy-mm-dd", 
    showTimepicker: true
   /* onClose: function(dateText,inst) {
        if($(this).attr('id') == 'txtFrom') {
            alert(dateText);
        }
    } */
});

$("#frmChangeBooking").validate({
    rules:{},
    messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
    errorClass: "input-group-addon error",
    errorElement: "label",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});

$("#frmChangeBooking").submit(function() {
    $('.currency').each(function(i) {
        var self = $(this);
        try {
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err) {
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });

    return true;
});

});</script>