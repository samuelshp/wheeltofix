<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('[data-toggle="tooltip"]').tooltip({container: 'body'});
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("select[name='selJumpTo']").change(function() {
    if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});

$("td.occupied").click(function(){
    if($(this).attr('id') >= 0){
        window.open('<?=site_url('booking/view', NULL, FALSE)?>/'+$(this).attr('id'));
    }
    // Perform your action on click here, like redirecting to a new url
    //window.open('www.google.com');
});

});</script>