<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<!--<script type="text/javascript" src="<?/*=base_url('asset/jquery/moment-2.4.0.js')*/?>"></script>-->
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('.currency').autoNumeric('init')
$("#customerLimit").autoNumeric('init', autoNumericOptionsRupiah);
$(".jwDateTime").datetimepicker({ 
    dateFormat: "yy-mm-dd", 
    showTimepicker: true
   /* onClose: function(dateText,inst) {
        if($(this).attr('id') == 'txtFrom') {
            alert(dateText);
        }
    } */
});

$("#frmAddBooking").validate({
    rules:{},
    messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
    errorClass: "input-group-addon error",
    errorElement: "label",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});

$("#frmAddBooking").submit(function() {
    $('.currency').each(function(i) {
        var self = $(this);
        try {
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err) {
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });

    var send1=$("#txtFrom").val().replace(/ /g, '_')+":00";
    var send2=$("#txtUntil").val().replace(/ /g, '_')+":00";
    $.ajax({
        async:false,
        url: "<?=site_url('booking_ajax/checkAvailable', NULL, FALSE)?>/" + $("#intLapanganID").val()+"/"+send1+"/"+send2,
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var avail=xml.find('available').text();
            if(avail==1){
                if(parseFloat( $("#customerLimit").autoNumeric('get'))<parseFloat( $("#subTotalTax").autoNumeric('get'))){
                    alert("jumlah pembelian melebihi limit yang dimiliki oleh pelanggan");
                    return false;
                }
                if($("input[name*='radioCustomer']").val()==0){
                    if($("#customerName").val()=='') {
                        alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"); return false;
                    }
                }else{
                    if($("#customerID").val()==0) {
                        alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"); return false;
                    }
                }
                $('.currency').each(function(i) {
                    var self = $(this);
                    try {
                        var v = self.autoNumeric('get');
                        self.autoNumeric('destroy');
                        self.val(v);
                    }catch(err) {
                        console.log("Not an autonumeric field: " + self.attr("name"));
                    }
                });

                if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
                return true;
            }else{
                var disp="Jadwal: \n";
                $.map(xml.find('available').find('item'),function(val,i){
                    var from = $(val).find('book_from').text();
                    var until = $(val).find('book_until').text();
                    disp+="-"+from+" sampai "+until+" \n";
                });
                disp+="telah dibooking";
                alert(disp);
            }
        }
    });
    return false;
});

$("input[name*='radioCustomer']").change(function() {
    if($(this).val()=='0') {
        $("#customerID").prop('disabled',false);
        $("#customerAddress").prop('disabled',true);
        $("#customerCity").prop('disabled',true);
        $("#customerPhone").prop('disabled',false);
    } else {
        $("#customerID").val(0);
        $("#customerID").prop('disabled',true);
        $("#customerAddress").prop('disabled',true);
        $("#customerCity").prop('disabled',true);
        $("#customerPhone").prop('disabled',true);
    }
});
$("#customerName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"<?=site_url('invoice_ajax/NULL')?>":"<?=site_url('invoice_ajax/getSaleOrderCustomerAutoCompleteWithSupplierBind', NULL, FALSE)?>/" + $("#customerName").val()+ "/" + $("#supplierID").val(),
            beforeSend: function() {
                ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"":$("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"":$("#loadCustomer").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedCust = xml;
                var display=[];
                $.map(xml.find('Customer').find('item'),function(val,i){
                    var name = $(val).find('cust_name').text();
                    var address = $(val).find('cust_address').text();
                    var city = $(val).find('cust_city').text();
                    var code = $(val).find('cust_code_old').text();
                    var outlettype = $(val).find('cust_outlettype').text();
                    var outlettypeStr = '';
                    switch (outlettype) {
                        case '1': outlettypeStr = 'RETAIL'; break;
                        case '2': outlettypeStr = 'SEMI GROSIR'; break;
                        case '3': outlettypeStr = 'STAR OUTLET'; break;
                        case '4': outlettypeStr = 'GROSIR'; break;
                        case '5': outlettypeStr = 'MODERN'; break;
                        default : outlettypeStr = 'N/A';
                    }

                    var strName=name+", "+ address +", "+city+", "+outlettypeStr;
                    var intID = $(val).find('id').text();
                    display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedCust = $.grep($arrSelectedCust.find('Customer').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        /*var paid=$(selectedCust).find('paid').text();
        if(paid!='0'){
            alert("Customer ini memiliki hutang pada pembelian dengan Supplier yang telah dipilih");
        }*/
        $("#customerID").val($(selectedCust).find('id').text());
        $("#customerAddress").val($(selectedCust).find('cust_address').text());
        $("#customerCity").val($(selectedCust).find('cust_city').text());
        $("#customerPhone").val($(selectedCust).find('cust_phone').text());
        $("#customerOutletType").val($(selectedCust).find('cust_outlettype').text());
        $("#customerLimit").autoNumeric('set',$(selectedCust).find('cust_limitkredit').text());
        $('input[name="outletMarketType"]').val($(selectedCust).find('cust_markettype').text());
        /* disini ajax e */
        $.ajax({
            url: "<?=site_url('invoice_ajax/getCustomerCreditByCustomerID', NULL, FALSE)?>/" + $("#customerID").val(),
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $.map(xml.find('Kredit').find('item'),function(val,i){
                    var credit = $(val).find('invo_grandtotal').text();
                    $("#customerLimit").autoNumeric('set',parseFloat($("#customerLimit").autoNumeric('get'))-parseFloat(credit));
                });
            }
        });
        var outlettypeStr = '';
        switch ($(selectedCust).find('cust_outlettype').text()) {
            case '1': outlettypeStr = 'RETAIL'; break;
            case '2': outlettypeStr = 'SEMI GROSIR'; break;
            case '3': outlettypeStr = 'STAR OUTLET'; break;
            case '4': outlettypeStr = 'GROSIR'; break;
            case '5': outlettypeStr = 'MODERN'; break;
            default : outlettypeStr = 'N/A';
        }
        $("#customerOutletTypeStr").val(outlettypeStr);
        $('input[name="customerOutletType"]').val($(selectedCust).find('cust_outlettype').text());
        $('input[name="customerID"]').val($(selectedCust).find('id').text());
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
    }
});

/* var counter=0;
$("#txtFrom").change(function(){ //ini isa jalan tapi ajax e ga jalan
    var send1=$("#txtFrom").val().replace(/ /g, '_')+":00";
    var send2=$("#txtUntil").val().replace(/ /g, '_')+":00";
    if(counter==0){
        $.ajax({
            url: "<?=site_url('booking_ajax/checkAvailable', NULL, FALSE)?>/" + $("#intLapanganID").val()+"/"+send1+"/"+send2,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                var avail=xml.find('available').text();
                alert(avail);
                counter=0;
            }
        });
    }
    counter=1;
});*/

    /*$("#txtFrom").datepicker({
        *//*onSelect: function(d,i){
            if(d !== i.lastVal){
                $(this).change();
            }
        }*//*
        onSelect: function(date, instance) {
            var send=$("#txtFrom").val().replace(/ /g, '_');
            $.ajax({
                url: "<?=site_url('booking_ajax/checkAvailable', NULL, FALSE)?>/" + $("#intLapanganID").val()+"/"+send,
                success: function(data){
                    var xmlDoc = $.parseXML(data);
                    var xml = $(xmlDoc);
                    var avail=xml.find('available');
                    alert(avail);
                    counter=0;
                }
            });
        }
    });*/

});</script>