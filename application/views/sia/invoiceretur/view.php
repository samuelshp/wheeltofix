<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-repeat"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturn'), 'link' => site_url('invoice_retur/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-lastinvoicereturn')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- Jump To -</option><?php
            if(!empty($arrInvoiceReturList)) foreach($arrInvoiceReturList as $e):
                $strListTitle = ' title="'.$e['cust_address'].', '.$e['cust_city'].'"'; ?>
            <option value="<?=site_url('invoice_retur/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['invr_code']/*formatDate2($e['invr_date'],'d/m/Y')*/?>] <?=$e['cust_name']?></option><?php
            endforeach; ?>
        </select>
    </div></div>

<?php $custid = $arrInvoiceReturData['cust_id']; ?>

<div class="col-xs-12">
<form name="frmChangeInvoice" id="frmChangeInvoice" method="post" action="<?=site_url('invoice_retur/view/'.$intInvoiceReturID, NULL, FALSE)?>" class="frmShop">
<div class="row">

    <!--data customer-->
    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
			<div class="col-xs-8"><?=$arrInvoiceReturData['cust_name']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></div>
			<div class="col-xs-8"><?=$arrInvoiceReturData['cust_address']?>, <?=$arrInvoiceReturData['cust_city']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
			<div class="col-xs-8"><?=$arrInvoiceReturData['cust_phone']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></div>
			<div class="col-xs-8">
				<?php
				switch($arrInvoiceReturData['cust_outlettype']) {
					case 1: echo "RETAIL"; break;
					case 2: echo "SEMI GROSIR"; break;
					case 3: echo "STAR OUTLET"; break;
					case 4: echo "GROSIR"; break;
					case 5: echo "MODERN"; break;
				}
				?>
			</div>
			<p class="spacer">&nbsp;</p>
		</div>
	</div></div> <!--data customer-->

    <!--kode & tanggal buat-->
    <div class="col-md-6">
        <div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
			<div class="col-xs-8"><?=$arrInvoiceReturData['invr_code']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
			<div class="col-xs-8"><?=formatDate2($arrInvoiceReturData['invr_date'],'d F Y')?></div>                                     <p class="spacer">&nbsp;</p>
		</div>
    </div>

        <!--data salesman-->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?></h3></div>
                <div class="panel-body">
                    <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
                    <div class="col-xs-8"><?=$arrInvoiceReturData['sals_code']?></div>
                    <p class="spacer">&nbsp;</p>

                    <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                    <div class="col-xs-8"><?=$arrInvoiceReturData['sals_name']?></div>
                    <p class="spacer">&nbsp;</p>
                </div>
            </div>
        </div> <!--data salesman-->

        <!--data gudang-->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?></h3></div>
                <div class="panel-body">
                    <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                    <div class="col-xs-8"><?=$arrInvoiceReturData['ware_name']?></div>
                    <p class="spacer">&nbsp;</p>
                </div>
            </div>
        </div> <!--data gudang-->

    </div><!--kode & tanggal buat-->
</div>
<div class="row">



</div> <!--row-->

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceitems')?></h3></div>
    <div class="panel-body"><?php
        if($bolBtnEdit): ?>
            <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                </div></div><?php
        endif; ?>

        <div class="table-responsive">
			<table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                </tr>
                </thead>
                <tbody><?php
                $i = 0;
                $suppid=$arrInvoiceReturData['supp_id'];
                $outlet=$arrInvoiceReturData['cust_outlettype'];
                $market=$arrInvoiceReturData['cust_markettype'];
                if(empty($arrInvoiceReturData['cust_internal'])){
                    $internal=1;
                }else{
                    $internal=$arrInvoiceReturData['cust_internal'];
                }
                $intTotalItem = 0;
                $idpurchaseawal='';
                $iditemawal='';
                if(!empty($arrInvoiceReturItem)):
                    foreach($arrInvoiceReturItem as $e):
                        $iditemawal=$iditemawal.$e['inri_product_id'].'-';
                        $idpurchaseawal=$idpurchaseawal.$e['id'].'-';
                        $intTotalItem++; ?>
                        <tr><?php
                                                    if($bolBtnEdit): ?>
                        <td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]" /></td><?php
                                                    endif; ?>
                        <td class="qty"><?php
                            if($bolBtnEdit): //if yes ?>
                            <div class="form-group">
                                <div class="col-xs-4"><input type="text" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity1']?>" class="required number form-control input-sm" id="txtItem1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit1'])?>" title="<?=formatUnitName($e['inri_unit1'])?>" /></div>
                                <div class="col-xs-4"><input type="text" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity2']?>" class="required number form-control input-sm" id="txtItem2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit2'])?>" title="<?=formatUnitName($e['inri_unit2'])?>" /></div>
                                <div class="col-xs-4"><input type="text" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity3']?>" class="required number form-control input-sm" id="txtItem3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit3'])?>" title="<?=formatUnitName($e['inri_unit3'])?>" /></div>
                                <input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['inri_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
                                <input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['inri_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
                                <input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['inri_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
                                <input type="hidden" name="idItem<?=$e['id']?>" value="<?=$e['inri_product_id']?>" id="idItem<?=$e['id']?>" />
                                <input type="hidden" value="<?=$e['prod_hpp']?>" id="HPP<?=$e['id']?>" />
                            </div><?php

                            else: echo
                                $e['inri_quantity1'].' '.formatUnitName($e['inri_unit1']).' + '.
                                $e['inri_quantity2'].' '.formatUnitName($e['inri_unit2']).' + '.
                                $e['inri_quantity3'].' '.formatUnitName($e['inri_unit3']);
                            endif; ?>
                                <input type="hidden" name="idProiID[<?=$e['id']?>]" value="<?=$e['id']?>" />
                        </td>
                        <td><b>(<?=$e['prod_code']?>) <?=$e['inri_description']?></b></td>
                        <td class="pr"><?php
                            if($bolBtnEdit): ?>
                                <div class="input-group"><label class="input-group-addon"><?=str_replace(' 0','',setPrice('','USED'))?></label><input type="text" name="txtItemPrice[<?=$e['id']?>]" class="required currency form-control input-sm" id="txtItemPrice<?=$e['id']?>" value="<?=$e['inri_price']?>" /></div><?php

                            else:
                                echo setPrice($e['inri_price'],'USED');
                            endif; ?>
                        </td>
                        <td class="disc"><?php
                            if($bolBtnEdit): ?>
                                <div class="form-group">
                                <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem1Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem1Disc<?=$e['id']?>" value="<?=$e['inri_discount1']?>" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>
                                <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem2Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem2Disc<?=$e['id']?>" value="<?=$e['inri_discount2']?>" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>
                                <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem3Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem3Disc<?=$e['id']?>" value="<?=$e['inri_discount3']?>" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>
                                </div><?php

                            else:
                                echo $e['inri_discount1']." % + ".$e['inri_discount2']." % + ".$e['inri_discount3']." %";
                            endif; ?>
                        </td>
                        <td class="subTotal" id="subTotal<?=$e['id']?>"><?=$e['inri_subtotal']?></td>
                        </tr><?php

                        $i++;
                    endforeach;

                    $iditemawal = $iditemawal.'0';
                    $idpurchaseawal = $idpurchaseawal.'0';
                else: ?>
                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                endif; ?>

                </tbody>
            </table>
        </div> <!--table-responsive-->

        <div class="row">
            <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?=$intInvoiceReturTotal?></div>
        </div>

    </div> <!--panel-body-->
</div>

<div class="row">
    <!--keterangan-->
    <div class="col-md-4"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
		<div class="panel-body"><?php
			if($bolBtnEdit): ?>
				<div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrInvoiceReturData['invr_description']?></textarea></div><?php
			else: ?>
                <input type="hidden" name="txaDescription" value="<?=$arrInvoiceReturData['invr_description']?>" />
				<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrInvoiceReturData['invr_description']?></div><?php
			endif; ?>
			<div class="form-group">
				<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')?></label>
				<input type="text" name="txtProgress" value="<?=$arrInvoiceReturData['invr_progress']?>" maxlength="64" class="form-control" />
			</div>
		</div>
	</div></div> <!--keterangan-->

    <div class="col-md-8"><div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceitems')?></h3></div>
        <div class="panel-body"><?php
            if($bolBtnEdit): ?>
                <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                    </div></div><?php
            endif; ?>

            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                    <thead>
                    <tr>
                        <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                    </tr>
                    </thead>
                    <tbody><?php
                    $i = 0;
                    $intTotalItemBonus = 0;
                    $idpurchasebonusawal='';
                    $iditembonusawal='';
                    if(!empty($arrInvoiceReturItemBonus)):
                        foreach($arrInvoiceReturItemBonus as $e):
                            $iditembonusawal=$iditembonusawal.$e['inri_product_id'].'-';
                            $idpurchaseawal=$idpurchaseawal.$e['id'].'-';
                            $intTotalItemBonus++; ?>
                            <tr><?php
                            if($bolBtnEdit): ?>
                                <td class="cb"><input type="checkbox" name="cbDeleteBonusAwal[<?=$e['id']?>]" /></td><?php
                            endif; ?>
                            <td class="qty"><?php
                                if($bolBtnEdit): //if yes ?>
                                    <div class="form-group">
                                    <div class="col-xs-4"><input type="text" name="txtItemBonus1Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity1']?>" class="required number form-control input-sm" id="txtItemBonus1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit1'])?>" title="<?=formatUnitName($e['inri_unit1'])?>" /></div>
                                    <div class="col-xs-4"><input type="text" name="txtItemBonus2Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity2']?>" class="required number form-control input-sm" id="txtItemBonus2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit2'])?>" title="<?=formatUnitName($e['inri_unit2'])?>" /></div>
                                    <div class="col-xs-4"><input type="text" name="txtItemBonus3Qty[<?=$e['id']?>]" value="<?=$e['inri_quantity3']?>" class="required number form-control input-sm" id="txtItemBonus3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inri_unit3'])?>" title="<?=formatUnitName($e['inri_unit3'])?>" /></div>
                                    <input type="hidden" name="txtItemBonus1Conv[<?=$e['id']?>]" value="<?=$e['inri_conv1']?>" id="txtItemBonus1Conv<?=$e['id']?>" />
                                    <input type="hidden" name="txtItemBonus2Conv[<?=$e['id']?>]" value="<?=$e['inri_conv2']?>" id="txtItemBonus2Conv<?=$e['id']?>" />
                                    <input type="hidden" name="txtItemBonus3Conv[<?=$e['id']?>]" value="<?=$e['inri_conv3']?>" id="txtItemBonus3Conv<?=$e['id']?>" />
                                    <input type="hidden" name="idItemBonus<?=$e['id']?>" value="<?=$e['inri_product_id']?>" id="idItemBonus<?=$e['id']?>" />
                                    </div><?php

                                else: echo
                                    $e['inri_quantity1'].' '.formatUnitName($e['inri_unit1']).' + '.
                                    $e['inri_quantity2'].' '.formatUnitName($e['inri_unit2']).' + '.
                                    $e['inri_quantity3'].' '.formatUnitName($e['inri_unit3']);
                                endif; ?>
                                <input type="hidden" name="idProiIDBonus[<?=$e['id']?>]" value="<?=$e['id']?>" />
                            </td>
                            <td><b>(<?=$e['prod_code']?>) <?=$e['inri_description']?></b></td>
                            </tr><?php

                            $i++;
                        endforeach;

                        $iditembonusawal = $iditembonusawal.'0';
                        $idpurchasebonusawal = $idpurchasebonusawal.'0';
                    else: ?>
                        <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                    endif; ?>

                    </tbody>
                </table>
            </div> <!--table-responsive-->

        </div> <!--panel-body-->
    </div></div>
</div>

<input type="hidden" id="supplierID" name="supplierID" value="<?=$suppid?>"/>
<input type="hidden" name="customerOutletType" value="<?=$outlet?>"/>
<input type="hidden" name="outletMarketType" value="<?=$market?>"/>
<input type="hidden" id="isInternal" value="<?=$internal?>"/>
<input type="hidden" name="totalItem" id="totalItem"/>
<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="idPurchaseAwal" name="idPurchaseAwal" value="<?=$idpurchaseawal?>"/>
<input type="hidden" name="totalItemBonus" id="totalItemBonus"/>
<input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
<input type="hidden" id="idPurchaseBonusAwal" name="idPurchaseBonusAwal" value="<?=$idpurchasebonusawal?>"/>

<?php 
    $rawStatus = $arrInvoiceReturData['invr_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>  

</form> <!--form-->

</div>