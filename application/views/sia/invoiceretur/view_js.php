<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);

$("select[name='selJumpTo']").change(function() {
	if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});

$("#frmChangeInvoice").submit(function() {
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	$('.subTotal').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});

$( "input[name*='Item']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	for(var zz=1;zz<=3;zz++){
		if($("#txtItem"+zz+"Qty"+at).val()<0){
			$("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
		}
	}
	if($("#txtItemPrice"+at).autoNumeric('get')<0){
		$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotal"+at).autoNumeric('get');
	var price = $("#txtItemPrice"+at).autoNumeric('get');
    var hpp=$("#HPP"+at).val();
    var priceafterdsc=price;
    for(var zz=1;zz<=3;zz++){
        priceafterdsc=priceafterdsc-(priceafterdsc*$("#txtItem"+zz+"Disc"+at).val()/100);
    }
    if(parseFloat(priceafterdsc)<parseFloat(hpp)){
        alert("harga dari barang kurang dari hpp");
    }
	var subtotal=$("#txtItem1Qty"+at).val()*price+($("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val())+($("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
	}
	$("#subTotal"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
});
var totalItem=0;
var numberItem=0;
var selectedItemBefore=$('#idItemAwal').val().split("-");
var selectedPurchaseBefore=$('#idPurchaseAwal').val().split("-");
var numberitemawal=selectedItemBefore.length-1;

var totalItemBonus=0;
var numberItemBonus=0;
var selectedItemBonusBefore=$('#idItemBonusAwal').val().split("-");
var selectedPurchaseBonusBefore=$('#idPurchaseBonusAwal').val().split("-");
var numberitembonusawal=selectedItemBonusBefore.length-1;

var internal=$("#isInternal").val();
$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="customerOutletType"]').val() + "/" + $('input[name="outletMarketType"]').val() + "/" + $('input[name="txtNewItem"]').val(),
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedItem = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBefore) > -1) {

                    } else {
                        display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var discount1 = $(selectedItem).find('skds_discount1').text();
        var discount2 = $(selectedItem).find('skds_discount2').text();
        var discount3 = $(selectedItem).find('skds_discount3').text();

        if(discount1==''){
            discount1=0;
        }
        if(discount2==''){
            discount2=0;
        }
        if(discount3==''){
            discount3=0;
        }

        var i=totalItem;
        if(numberItem=='0' && numberitemawal=='0'){

            $("#selectedItems tbody").html('');
        }
        $("#selectedItems tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm" id="qty1ItemX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm" id="qty2ItemX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td id="nmeItemX'+i+'"></td>'+
                '<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
                '<td class="disc"><div class="form-group">'+
                '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="'+discount1+'" placeholder="Discount 1" title="" value="0" /><label class="input-group-addon">%</label></div></div>' +
                '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="'+discount1+'" placeholder="Discount 2" title="" value="0"/><label class="input-group-addon">%</label></div></div>' +
                '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="'+discount1+'" placeholder="Discount 3" title="" value="0"/><label class="input-group-addon">%</label></div></div>' +
                '</div></td>'+
                '<td class="subTotal" id="subTotalX'+i+'">0</td>'+
                '<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
                '<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
                '<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
                '<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
                '<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
                '<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
                '<input type="hidden" id="conv1UnitX'+i+'" >' +
                '<input type="hidden" id="conv2UnitX'+i+'" >' +
                '<input type="hidden" id="conv3UnitX'+i+'" >'+
                '<input type="hidden" id="HppX'+i+'" >'+
                '<input type="hidden" id="HargaX'+i+'" >'+
                '</tr>'
        );
        var prodcode = $(selectedItem).find('prod_code').text();
        var prodtitle = $(selectedItem).find('prod_title').text();
        var probtitle = $(selectedItem).find('prob_title').text();
        var proctitle = $(selectedItem).find('proc_title').text();
        var strName=$(selectedItem).find('strName').text();
        var id=$(selectedItem).find('id').text();
        selectedItemBefore.push(id);
        $("#nmeItemX"+i).text("("+prodcode+") "+ strName);
        var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        /*$("#prcItemX"+i).val(0);*/
        $("#idItemX"+i).val($(selectedItem).find('id').text());
        $("#prodItemX"+i).val(prodtitle);
        $("#probItemX"+i).val(probtitle);
        $("#qty1ItemX"+i).hide();
        $("#qty2ItemX"+i).hide();
        $("#qty3ItemX"+i).hide();
        var hpp=$(selectedItem).find('prod_hpp').text();
        if(isNaN(internal) || internal == 1) {
            if ($('input[name="outletMarketType"]').val() == '1') {
                var tempprice=$(selectedItem).find('prod_pricemodern').text();
                if(parseInt(tempprice) < parseInt(hpp)){
                    $("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
                }else{
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                }
            } else if ($('input[name="outletMarketType"]').val() == '2') {
                var tempprice=$(selectedItem).find('prod_price').text();
                if(parseInt(tempprice) < parseInt(hpp)){
                    $("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
                }else{
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                }
            };
        }else{
            $("#prcItemX"+i).val(hpp);
            $("#HargaX"+i).val(hpp);
        }
        $("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#HppX"+i).val(hpp);
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedSO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    /*var intID = $(val).find('id').text();
                     var strUnit=$(val).find('unit_title').text();
                     $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemX"+i).show();
                    $("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
                    $("#sel"+zz+"IteMunitX"+i).text(unitStr[zz-1]);
                }
                /*$.ajax({
                    url: "<?=site_url('invoice_retur/getLatestPrice', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/" + $("#customerID").val(),
                    success: function(data){
                        var xmlDoc = $.parseXML(data);
                        var xml = $(xmlDoc);
                        var intPrice=xml.find('Product').find('invi_price').text();
                        var intDiscount1=xml.find('Product').find('invi_discount1').text();
                        var intDiscount2=xml.find('Product').find('invi_discount2').text();
                        var intDiscount3=xml.find('Product').find('invi_discount3').text();
                        if(isEmpty(intPrice)){
                            intPrice=0;
                        }
                        if(isEmpty(intDiscount1)){
                            intDiscount1=0;
                        }
                        if(isEmpty(intDiscount2)){
                            intDiscount2=0;
                        }
                        if(isEmpty(intDiscount3)){
                            intDiscount3=0;
                        }
                        $("#prcItemX"+i).autoNumeric('set',intPrice);
                        $("#dsc1ItemX"+i).val(intDiscount1);
                        $("#dsc2ItemX"+i).val(intDiscount2);
                        $("#dsc3ItemX"+i).val(intDiscount3);
                    }
                });*/
            }
        });
        totalItem++;
        numberItem++;
        $("#totalItem").val(totalItem);

        $('#txtNewItem').val(''); return false; // Clear the textbox
    }
});

$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="customerOutletType"]').val() + "/" + $('input[name="outletMarketType"]').val() + "/" + $('input[name="txtNewItemBonus"]').val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedItem = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1) {

                    } else {
                        display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalItemBonus;
        if(numberItemBonus=='0' && numberitembonusawal=='0'){
            $("#selectedItemsBonus tbody").html('');
        }
        var id=$(selectedItem).find('id').text();
        $("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td id="nmeItemBonusX'+i+'"></td>'+
                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                '<input type="hidden" id="sel1BonusUnitIDX'+i+'" name="sel1BonusUnitID'+i+'">' +
                '<input type="hidden" id="sel2BonusUnitIDX'+i+'" name="sel2BonusUnitID'+i+'">' +
                '<input type="hidden" id="sel3BonusUnitIDX'+i+'" name="sel3BonusUnitID'+i+'">'+
                '<input type="hidden" id="conv1BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv2BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv3BonusUnitX'+i+'" >'+
                '</tr>'
        );
        var prodcode = $(selectedItem).find('prod_code').text();
        var prodtitle = $(selectedItem).find('prod_title').text();
        var probtitle = $(selectedItem).find('prob_title').text();
        var proctitle = $(selectedItem).find('proc_title').text();
        var strName=$(selectedItem).find('strName').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text("("+prodcode+") "+ strName);
        var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        /*$("#prcItemX"+i).val(0);*/
        $("#idItemBonusX"+i).val($(selectedItem).find('id').text());
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedSO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    /*var intID = $(val).find('id').text();
                     var strUnit=$(val).find('unit_title').text();
                     $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"BonusUnitIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"BonusUnitX"+i).val(unitConv[zz-1]);
                    $("#sel"+zz+"ItemBonusUnitX"+i).text(unitStr[zz-1]);
                }
            }
        });
        totalItemBonus++;
        numberItemBonus++;
        $("#totalItemBonus").val(totalItemBonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
    }
});

$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemX"+at).val()<0){
            $("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
        }
    }
    if($("#prcItemX"+at).autoNumeric('get')<0){
        $("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
    }
    var previous=$("#subTotalX"+at).autoNumeric('get');
    var price = $("#prcItemX"+at).autoNumeric('get');
    var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
    /* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

    for(var zz=1;zz<=3;zz++){
        subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
    }
    $("#subTotalX"+at).autoNumeric('set',subtotal);
    var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
    $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }
});

$( "input[name*='cbDeleteAwal']").change(function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);
    var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
    $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    /*var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }*/
    numberitemawal--;
    if((numberitemawal+numberItem)=='0'){
        $("#selectedItems tbody").append(
            '<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
        return value != $("#idItem"+at).val();
    });
    $(this).closest("tr").remove();
});

$( "input[name*='cbDeleteBonusAwal']").change(function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);
    numberitembonusawal--;
    if((numberitembonusawal+numberItemBonus)=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonus"+at).val();
    });
    $(this).closest("tr").remove();
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDeleteX']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
    $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    /*var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }*/
    numberItem--;
    if((numberItem+numberitemawal)=='0'){
        $("#selectedItems tbody").append(
            '<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
        return value != $("#idItemX"+at).val();
    });
    $(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDeleteBonusX']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberItemBonus--;
    if((numberItemBonus+numberitembonusawal)=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});
});</script>