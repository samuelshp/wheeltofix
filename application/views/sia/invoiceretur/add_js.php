<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$("#txtNewItem").prop('disabled', true);
$("#txtNewItemBonus").prop('disabled', true);
$("#customerName").prop('disabled', true);
var selectedItemBefore = [];
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var mode=0;
var totalItem=0;
var numberItem=0;
var totalItemBonus = 0;
var numberItemBonus = 0;
var selectedItemBonusBefore=[];

$("#salesmanName").prop('disabled', true);
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);
//("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddInvoice").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

function reset() {}
function setManual() {}
function setAuto($intID) {}

$("#frmAddInvoice").submit(function() {

	if($("#supplierName").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectsupplier')?>"); return false;
	}

	if($("#warehouseName option:selected").val() == 0) {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
	}

	if($("#customerName").val()=='') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"); return false;
	}

	if(numberItem<=0) {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
	}

	$('.currency').each(function(i) {
		var self = $(this);
		try {
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err) {
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
	
	return true;
});

/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});

$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});
/*
$("#outletName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceOutletAutoComplete', NULL, FALSE)?>/" + $("#outletID").val(),
			beforeSend: function() {
				$("#loadOutlet").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadOutlet").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				var display=[];
				$.map(xml.find('Outlet').find('item'),function(val,i){
					var name = $(val).find('outl_title').text();
					*//*var address = $(val).find('cust_address').text();
					 var city = $(val).find('cust_city').text();*//*
					var code = $(val).find('outl_code').text();
					var strName=code + ", " + name*//*+", "+ address +", "+city*//*;
					var intID = $(val).find('id').text();
					display.push({label: strName, value: name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSO = $.grep($arrSelectedSO.find('Outlet').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#outletID").text($(selectedSO).find('id').text());
		$("#outletCode").text($(selectedSO).find('outl_code').text());
		$("#outletMarketType").text($(selectedSO).find('outl_markettype').text());
		*//*$("#customerAddress").text($(selectedSO).find('cust_address').text());
		 $("#customerCity").text($(selectedSO).find('cust_city').text());
		 $("#customerPhone").text($(selectedSO).find('cust_phone').text());*//*
		$('input[name="outletID"]').val($(selectedSO).find('id').text());
		$('input[name="outletCode"]').val($(selectedSO).find('outl_code').text());
		$('input[name="outletMarketType"]').val($(selectedSO).find('outl_markettype').text());
        $("#loadOutlet").html('<i class="fa fa-check"></i>');
	}
});*/

$("#supplierName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#supplierName").val(),
			beforeSend: function() {
				$("#loadSupplier").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupplier").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSupplier = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
                    var code = $(val).find('supp_code').text();
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSupplier = $.grep($arrSelectedSupplier.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		$("#supplierID").val($(selectedSupplier).find('id').text());
		$("#supplierAddress").text($(selectedSupplier).find('supp_address').text());
		$("#supplierCity").text($(selectedSupplier).find('supp_city').text());
		$("#supplierPhone").text($(selectedSupplier).find('supp_phone').text());
		$('input[name="supplierID"]').val($(selectedSupplier).find('id').text());

		totalItemBonus=0;
		numberItemBonus=0;
		selectedItemBonusBefore=[];
		selectedItemBefore=[];
		totalItem=0;
		numberItem=0;
		$("#selectedItems tbody").html('');
		$("#selectedItemsBonus tbody").html('');
		$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadSupplier").html('<i class="fa fa-check"></i>');
        $("#customerName").prop('disabled', false);
        $("#salesmanName").prop('disabled', false);
    }
});

var internal=0;
$("#customerName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_retur_ajax/getSaleOrderCustomerAutoCompleteWithSupplierBindRetur', NULL, FALSE)?>/" + $("#customerName").val()+ "/" + $("#supplierID").val(),
			beforeSend: function() {
				$("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadCustomer").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedCust = xml;
				var display=[];
				$.map(xml.find('Customer').find('item'),function(val,i){
					var name = $(val).find('cust_name').text();
					var address = $(val).find('cust_address').text();
					var city = $(val).find('cust_city').text();
                    var code = $(val).find('cust_code').text();
					var outlettype = $(val).find('cust_outlettype').text();
					var outlettypeStr = '';
					switch (outlettype) {
						case '1': outlettypeStr = 'BARU'; break;
						case '2': outlettypeStr = 'BIASA'; break;
						case '3': outlettypeStr = 'MEMBER'; break;
						case '4': outlettypeStr = 'RESELLER'; break;
						case '5': outlettypeStr = 'KANTOR'; break;
						default : outlettypeStr = 'N/A';
					}

					var strName=name+", "+ address +", "+city+", "+outlettypeStr;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedCust = $.grep($arrSelectedCust.find('Customer').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#customerID").val($(selectedCust).find('id').text());
		$("#customerAddress").val($(selectedCust).find('cust_address').text());
		$("#customerCity").val($(selectedCust).find('cust_city').text());
		$("#customerPhone").val($(selectedCust).find('cust_phone').text());
		$("#customerOutletType").val($(selectedCust).find('cust_outlettype').text());
        $('input[name="outletMarketType"]').val($(selectedCust).find('cust_markettype').text());
        internal=parseInt($(selectedCust).find('cust_internal').text());
		var outlettypeStr = '';
		switch ($(selectedCust).find('cust_outlettype').text()) {
			case '1': outlettypeStr = 'BARU'; break;
			case '2': outlettypeStr = 'BIASA'; break;
			case '3': outlettypeStr = 'MEMBER'; break;
			case '4': outlettypeStr = 'RESELLER'; break;
			case '5': outlettypeStr = 'KANTOR'; break;
			default : outlettypeStr = 'N/A';
		}
		$("#customerOutletTypeStr").val(outlettypeStr);
		$('input[name="customerOutletType"]').val($(selectedCust).find('cust_outlettype').text());
		$('input[name="customerID"]').val($(selectedCust).find('id').text());
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
        $("#txtNewItem").prop('disabled', false);
        $("#txtNewItemBonus").prop('disabled', false);
	}
});

$("#salesmanName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceSalesmanAutoComplete', NULL, FALSE)?>/" + $("#salesmanName").val()+ "/" + $("#supplierID").val(),
			beforeSend: function() {
				$("#loadSalesman").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSalesman").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				var display=[];
				$.map(xml.find('Salesman').find('item'),function(val,i){
					var name = $(val).find('sals_name').text();
					/*var address = $(val).find('cust_address').text();
					 var city = $(val).find('cust_city').text();*/
					var code = $(val).find('sals_code').text();
					var strName= name/*+", "+ address +", "+city*/;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSO = $.grep($arrSelectedSO.find('Salesman').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#salesmanID").text($(selectedSO).find('id').text());
		$("#salesmanCode").text($(selectedSO).find('sals_code').text());
		/*$("#customerAddress").text($(selectedSO).find('cust_address').text());
		 $("#customerCity").text($(selectedSO).find('cust_city').text());
		 $("#customerPhone").text($(selectedSO).find('cust_phone').text());*/
		$('input[name="salesmanID"]').val($(selectedSO).find('id').text());
		$('input[name="salesmanCode"]').val($(selectedSO).find('sals_code').text());
        $("#loadSalesman").html('<i class="fa fa-check"></i>');
	}
});

/*$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
 var at=this.name.substring(this.name.length-1,this.name.length);
 var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
 $("#subTotalNoTax").autoNumeric('set',totalNoTax);
 var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
 if(totalwithdisc<=0){
 $("#subTotalWithDisc").autoNumeric('set','0');
 $("#subTotalTax").autoNumeric('set','0');
 }else{
 $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
 var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
 $("#subTotalTax").autoNumeric('set',totalTax);
 }
 numberItem--;
 if(numberItem=='0'){
 $("#selectedItems").append(
 '<tr><td colspan="6">'+'' +
 '<h4 style="text-align:center">'+
 '<?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?>'+
 '</h4>'+
 '</td></tr>'
 );
 }
 $(this).closest("tr").remove();
 });*/
/*$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
	$(this).autoNumeric('init', autoNumericOptionsRupiah);
});*/

$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
	}
	if($("#prcItemX"+at).autoNumeric('get')<0){
		$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotalX"+at).autoNumeric('get');
	var price = $("#prcItemX"+at).autoNumeric('get');
	var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */
    var priceafterdsc=price;
    for(var zz=1;zz<=3;zz++){
        priceafterdsc=priceafterdsc-(priceafterdsc*$("#dsc"+zz+"ItemX"+at).val()/100);
    }
    var satuan=parseFloat(priceafterdsc)/parseFloat($("#conv1UnitX"+at).val());
    var hpp=parseFloat($("#HppX"+at).val())/parseFloat($("#conv1UnitX"+at).val());
    if(satuan<hpp){
        alert("harga satuan dari barang setelah didiskon kurang dari harga HPP");
    }
	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
	}
	$("#subTotalX"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	/*var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		//$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		//$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}*/
});


$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="customerOutletType"]').val() + "/" + $('input[name="outletMarketType"]').val() + "/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedItem = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBefore) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var discount1 = $(selectedItem).find('skds_discount1').text();
		var discount2 = $(selectedItem).find('skds_discount2').text();
		var discount3 = $(selectedItem).find('skds_discount3').text();

        if(discount1=='') discount1=0;
        if(discount2=='') discount2=0;
        if(discount3=='') discount3=0;

		var i=totalItem;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedItem).find('prod_conv1').text();
        var prod_conv2 = $(selectedItem).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
				'<td class="disc"><div class="form-group">'+
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="'+discount1+'" placeholder="Discount 1" title="" value="0" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="'+discount1+'" placeholder="Discount 2" title="" value="0"/><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="'+discount1+'" placeholder="Discount 3" title="" value="0"/><label class="input-group-addon">%</label></div></div>' +
				'</div></td>'+
				'<td id="subTotalX'+i+'">0</td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
                '<input type="hidden" id="HppX'+i+'" >'+
                '<input type="hidden" id="HargaX'+i+'" >'+
				'</tr>'
		);
        var prodcode = $(selectedItem).find('prod_code').text();
		var prodtitle = $(selectedItem).find('prod_title').text();
		var probtitle = $(selectedItem).find('prob_title').text();
		var proctitle = $(selectedItem).find('proc_title').text();
		var strName=$(selectedItem).find('strName').text();
		var id=$(selectedItem).find('id').text();
		selectedItemBefore.push(id);
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
		var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        /*$("#prcItemX"+i).val(0);*/

		$("#idItemX"+i).val($(selectedItem).find('id').text());
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
        var hpp=$(selectedItem).find('prod_hpp').text();
        if(isNaN(internal) || internal == 1) {
            if ($('input[name="outletMarketType"]').val() == '1') {
                var tempprice=$(selectedItem).find('prod_pricemodern').text();
                if(parseInt(tempprice) < parseInt(hpp)){
                    $("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
                }else{
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                }
            } else if ($('input[name="outletMarketType"]').val() == '2') {
                var tempprice=$(selectedItem).find('prod_price').text();
                if(parseInt(tempprice) < parseInt(hpp)){
                    $("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
                }else{
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                }
            };
        }else{
            $("#prcItemX"+i).val(hpp);
            $("#HargaX"+i).val(hpp);
        }
        $("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#HppX"+i).val(hpp);
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					/*var intID = $(val).find('id').text();
					 var strUnit=$(val).find('unit_title').text();
					 $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#sel"+zz+"IteMunitX"+i).text(unitStr[zz-1]);
				}
               /* $.ajax({
                    url: "<?=site_url('invoice_retur/getLatestPrice', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/" + $("#customerID").val(),
                    success: function(data){
                        var xmlDoc = $.parseXML(data);
                        var xml = $(xmlDoc);
                        var intPrice=xml.find('Product').find('invi_price').text();
                        var intDiscount1=xml.find('Product').find('invi_discount1').text();
                        var intDiscount2=xml.find('Product').find('invi_discount2').text();
                        var intDiscount3=xml.find('Product').find('invi_discount3').text();
                        if(intPrice==''){
                            intPrice=0;
                        }
                        if(intDiscount1==''){
                            intDiscount1=0;
                        }
                        if(intDiscount2==''){
                            intDiscount2=0;
                        }
                        if(intDiscount3==''){
                            intDiscount3=0;
                        }
                        $("#prcItemX"+i).autoNumeric('set',intPrice);
                        $("#dsc1ItemX"+i).val(intDiscount1);
                        $("#dsc2ItemX"+i).val(intDiscount2);
                        $("#dsc3ItemX"+i).val(intDiscount3);
                    }
                });*/
			}
		});
		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);

		$('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="customerOutletType"]').val() + "/" + $('input[name="outletMarketType"]').val() + "/" + $('input[name="txtNewItemBonus"]').val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedItem = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1) {

                    } else {
                        display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        var i=totalItemBonus;
        if(numberItemBonus=='0'){

            $("#selectedItemsBonus tbody").html('');
        }
        $("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td id="nmeItemBonusX'+i+'"></td>'+
                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                '<input type="hidden" id="sel1BonusUnitIDX'+i+'" name="sel1BonusUnitID'+i+'">' +
                '<input type="hidden" id="sel2BonusUnitIDX'+i+'" name="sel2BonusUnitID'+i+'">' +
                '<input type="hidden" id="sel3BonusUnitIDX'+i+'" name="sel3BonusUnitID'+i+'">'+
                '<input type="hidden" id="conv1BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv2BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv3BonusUnitX'+i+'" >'+
                '</tr>'
        );
        var prodcode = $(selectedItem).find('prod_code').text();
        var prodtitle = $(selectedItem).find('prod_title').text();
        var probtitle = $(selectedItem).find('prob_title').text();
        var proctitle = $(selectedItem).find('proc_title').text();
        var strName=$(selectedItem).find('strName').text();
        var id=$(selectedItem).find('id').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text("("+prodcode+") "+ strName);
        var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        /*$("#prcItemX"+i).val(0);*/

        $("#idItemBonusX"+i).val($(selectedItem).find('id').text());
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedSO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    /*var intID = $(val).find('id').text();
                     var strUnit=$(val).find('unit_title').text();
                     $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"BonusUnitIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"BonusUnitX"+i).val(unitConv[zz-1]);
                    $("#sel"+zz+"ItemBonusUnitX"+i).text(unitStr[zz-1]);
                }
            }
        });
        totalItemBonus++;
        numberItemBonus++;
        $("#totalItemBonus").val(totalItem);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
    }
});

$("#txtTax").change(function(){
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get');
	var totalTax=parseInt(totalNoTax)+parseInt(totalNoTax*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	/*var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		//$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		//$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}*/
	numberItem--;
	if(numberItem=='0'){
        $("#selectedItems tbody").append(
            '<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});


$("#warehouseName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#warehouseName").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedWare = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				if(display.length == 1) {
					setTimeout(function() {
	                    $("#warehouseName").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
	                    $('#warehouseName').autocomplete('close');
                    }, 100);
                }
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedWare = $.grep($arrSelectedWare.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedWare).find('id').text());
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});
setTimeout(function() {
    $("#warehouseName").val(primaryWarehouseName);
    $("#warehouseName").autocomplete('search');
},500);

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberItemBonus--;
    if(numberItemBonus=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});


    /*$("#dsc4Item").change(function(){
        var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
        var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
        if(totalwithdisc<=0){
            //$("#subTotalWithDisc").autoNumeric('set','0');
            $("#subTotalTax").autoNumeric('set','0');
        }else{
            //$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
            var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
            $("#subTotalTax").autoNumeric('set',totalTax);
        }
    });*/

});</script>