<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse'), 'link' => site_url('acceptance/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

$priviledge = $_SESSION['strAdminPriviledge'];

$canEdit = false;
if($arrAcceptanceData['acce_rawstatus'] != 0 && $bolEditable != null && $inPi ==0 ){
    $canEdit = true;
}
else{}

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- <div class="col-xs-6 col-xs-offset-6"><div class="form-group">
	<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-lastpurchase')?></label>
	<select name="selJumpTo" class="form-control">
		<option value="">- Jump To -</option><?php
		if(!empty($arrAcceptanceList)) foreach($arrAcceptanceList as $e):
			$strListTitle = ' title="'.$e['supp_address'].', '.$e['supp_city'].'"'; ?>
		<option value="<?=site_url('acceptance/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['acce_code']/*formatDate2($e['acce_date'],'d/m/Y')*/?>] <?=$e['supp_name']?></option><?php
		endforeach; ?>
	</select>
</div></div> -->

<div class="col-xs-12"><form name="frmChangeAcceptance" id="frmChangeAcceptance" method="post" action="<?=site_url('acceptance/view/'.$intAcceptanceID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <!-- <div class="col-md-6">
    <div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reply"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchasedata')?></h3></div>
		<div class="panel-body">
            <?=(compareData($arrAcceptanceData['acce_purchase_id'],array(0)))?'<div class="col-xs-4">'.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code').'</div><div class="col-xs-8">-</div>':
                '<div class="col-xs-4">'.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code').'</div>
						<div class="col-xs-8"><label id="purchaseID"><a href='.site_url("purchase/view/".$arrAcceptanceData['acce_purchase_id'], NULL, FALSE).'>'.$arrAcceptanceData['acce_code'].'</a></label></div>'?>
            <p class="spacer">&nbsp;</p>
            <?=(compareData($arrAcceptanceData['supp_name'],array('')))?'<div class="col-xs-4">'.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername').'</div><div class="col-xs-8">-</div>':
                '<div class="col-xs-4">'.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code').'</div>
						<div class="col-xs-8"><label id="suppName">'.$arrAcceptanceData['supp_name'].'</label></div>'?>
            <p class="spacer">&nbsp;</p>
		</div>
    </div>
    pembayaran-->
        <!-- <div class="panel panel-primary" id="panelDueDate">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-thumb-tack"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></h3></div>
            <div class="panel-body">
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></div>
                <div class="col-xs-8"><?=$arrAcceptanceData['acce_payment'] == '1' ? 'Tunai' : 'Kredit'?></div>
                <p class="spacer">&nbsp;</p><?php
    if(strtotime($arrAcceptanceData['acce_jatuhtempo']) > strtotime($arrAcceptanceData['acce_date'])): ?>  
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?></div>
                <div class="col-xs-8"><?=formatDate2($arrAcceptanceData['acce_jatuhtempo'],'d F Y')?></div>
                <p class="spacer">&nbsp;</p><?php
    endif; ?>  
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-payment')?></div>
                <div class="col-xs-8"><?php 
                    switch($arrAcceptanceData['acce_payment']) {
                        case 1: echo 'Tunai'; break;
                        case 2: echo 'Kredit'; break;
                    } ?></div>
                <p class="spacer">&nbsp;</p>
            </div>
        </div>pembayaran
    </div> -->

    <div class="col-md-6"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedata')?></h3></div>
            <div class="panel-body">
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
                <div class="col-xs-8"><?=$arrAcceptanceData['acce_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
                <div class="col-xs-8"><?=formatDate2($arrAcceptanceData['acce_date'],'d F Y')?></div>
                <p class="spacer">&nbsp;</p>
                <input type="hidden" name="idAcceptance" id="idAcceptance" value="<?=$arrAcceptanceData['id']?>"/> 
                <input type="hidden" name="codeAcceptance" id="codeAcceptance" value="<?=$arrAcceptanceData['acce_code']?>"/> 
            </div>
        </div></div>

</div>

<!-- <div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'acceptance-reciveditem')?></h3></div>
    <div class="panel-body"><?php
        if($arrAcceptanceData['acce_purchase_id']==0 && $bolBtnEdit): ?>
            <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                </div></div><?php
        endif; ?>
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
                <thead>
                    <tr>
                        <?php if($bolBtnEdit): ?>
                        <?=(compareData($arrAcceptanceData['acce_purchase_id'],array(0)))?'<th class="cb">'.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete').'</th>':
                            '<th class="qty">Jml Di Pembelian (Sisa)</th>'?>
                            <?php elseif($arrAcceptanceData['acce_purchase_id']>0):
                            echo '<th class="qty">Jml Di Pembelian (Sisa)</th>';
                        endif; ?>
                        
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div> -->


<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'acceptance-reciveditem')?></h3></div>
    <div class="panel-body">
    <!-- <?php
        if($arrAcceptanceData['acce_purchase_id']==0 && $bolBtnEdit): ?>
            <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                </div></div><?php
        endif; ?> -->
        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
			<thead>
			<tr>
				<th>No. Purchase</th>
                <th>Tgl. Purchase</th>
                <th>Supplier</th>
                <th>Proyek</th>
				<th>Barang</th>
                <th>Ket. PB</th>
				<th>Jml Purchase</th>
                <th>Jml PB</th>
                <th>Satuan PB</th>
				<th>Jml Diterima</th>
                <th>Satuan Diterima</th>
                <?php if($arrAcceptanceData['acce_rawstatus'] != 0 && $priviledge == 10 || $priviledge == 2 || $priviledge == 6 || $priviledge == 1 && $bolEditable != null): ?>
                    <th>Jml Bayar</th>
                    <th>Satuan Bayar</th>
                <?php endif;?>
                <!-- <th class="delete">Delete</th> -->
			</tr>
			</thead>
			<tbody><?php
			$i = 0;
			$intTotalItem = 0;
			$iditemawal='';
            $idpurchaseawal='';
			if(!empty($arrAcceptanceItem)):
				foreach($arrAcceptanceItem as $e):
					$iditemawal=$iditemawal.$e['product_id'].',';
                    $idpurchaseawal=$idpurchaseawal.$e['id'].',';
					$intTotalItem++;
                    if($arrAcceptanceData['acce_purchase_id']>0){
                        $max=$e['maxData']['prci_quantity1']*$e['acit_conv1']+$e['maxData']['prci_quantity2']*$e['acit_conv2']+$e['maxData']['prci_quantity3']*$e['acit_conv3'];
                        $already=$e['maxData']['prci_arrived1']*$e['acit_conv1']+$e['maxData']['prci_arrived2']*$e['acit_conv2']+$e['maxData']['prci_arrived3']*$e['acit_conv3'];
                        $here=$e['acit_quantity1']*$e['acit_conv1']+$e['acit_quantity2']*$e['acit_conv2']+$e['acit_quantity3']*$e['acit_conv3'];
                        $diff=$max-$already+$here;

                        if($e['acit_conv1'] > 0) {
                            $tempmax1=floor($diff/$e['acit_conv1']);
                            $diff=$diff%$e['acit_conv1'];
                        } else $tempmax1 = 0;
                        if($e['acit_conv2'] > 0) {
                            $tempmax2=floor($diff/$e['acit_conv2']);
                            $diff=$diff%$e['acit_conv2'];
                        } else $tempmax2 = 0;
                        if($e['acit_conv3'] > 0) {
                            $tempmax3=floor($diff/$e['acit_conv3']);
                            $diff=$diff%$e['acit_conv3'];
                        } else $tempmax3 = 0;
                    } ?>
				<tr <?php if($arrAcceptanceData['acce_purchase_id']>0 && $e['exist']=='0') :?> class="danger" <?php endif;?> >
                    <td> <?=$e['prch_code']?></td>
                    <?php 
                        //endif;
                    ?>
                    <td><?= $e['cdate']?></td>
                    <td><?= $e['supp_name']?></td>
                    <td><?= $e['kont_name']?></td>
                    <td><?= $e['prod_title'] ?></td>
                    <td><?= $e['proi_description'] ?></td>
                    <td class="jml"><?php
						//if($bolBtnEdit): ?>
							<input type="hidden" name="jumlah_purchase[<?=$i?>]" class="required currency form-control input-sm" id="jumlah_purchase<?=$i?>" value="<?=$arrTampilan[$i]['jml_purch_now']?>" /></div>
                            <?php

						//else:?>
                        <div class="form-group" align="center">
                            <?=$arrTampilan[$i]?> <?= $e['satuan_bayar'] ?>
                            <input type="hidden" name="jumlah_purchase_view[<?=$i?>]" id="jumlah_purchase_view<?=$i?>" value="<?=$arrTampilan[$i]?>" />
                        </div>
						<?php //endif; ?>
					</td>
                    <input type="hidden" name="jumlah_terterima[<?=$i?>]" id="jumlah_terterima[<?=$i?>]" value="<?=$e['purchi_terterima']?>"/>
                    <input type="hidden" name="ai_id[<?=$i?>]"  id="ai_id[<?=$i?>]" value="<?=$e['id']?>"/>
                    <input type="hidden" name="jumlah_item" id="jumlah_item" value="<?=$intTotalItem?>"/>
                    <input type="hidden" name="awal1Item[<?=$i?>]" value="<?=$e['acit_quantity1']?>" />
                    <input type="hidden" name="awal2Item[<?=$i?>]" value="<?=$e['acit_quantity2']?>" />
                    <input type="hidden" name="awal3Item[<?=$i?>]" value="<?=$e['acit_quantity3']?>" />
                    <input type="hidden" name="acceptance_id" id="acceptance_id" value="<?=$e['acc_id']?>" />
                    <td class="qty_pb">
                        <?php
                            if($canEdit): 
                        ?>
                                <div class="form-group">
                                    <input type="text" name="jumlah_pb[<?=$i?>]" class="required currency form-control input-sm" id="jumlah_pb<?=$i?>" value="<?=$e['acit_quantity_pb']?>" /></div><input type="hidden" name="qty_inputan_pb[<?=$i?>]" id="qty_inputan_pb" value="<?= $e[''] ?>" readonly >
                                </div>
                        <?php
                            else:
                        ?>
                        <div class="form-group">
                            <?= $e['acit_quantity_pb'] ?>
                            <input type="hidden" name="jumlah_pb_view[<?=$i?>]" id="jumlah_pb_view<?=$i?>" value="<?=$e['acit_quantity_pb']?>" />
                        </div>
                        <?php endif; ?>
                    </td>
                    <td><?= $e['satuan_pb'] ?></td>


                    <td class="qty_terima">
                        
                    <?php
                        if($canEdit): 
                    ?>
                            <div class="form-group">
                                <input type="text" name="jumlah_diterima[<?=$i?>]" class="required currency form-control input-sm" id="jumlah_diterima<?=$i?>" value="<?=$e['jumlah_diterima']?>" /></div><input type="hidden" name="qty_inputan[<?=$i?>]" id="qty_inputan" value="<?= $e['jumlah_diterima'] ?>" >
                            </div>
					<?php
                        else:
                    ?>
                        <div class="form-group">
                            <?= $e['jumlah_diterima'] ?>
                            <input type="hidden" name="jumlah_diterima_view[<?=$i?>]" id="jumlah_diterima_view<?=$i?>" value="<?=$e['jumlah_diterima']?>" />
                        </div>
						<?php endif; ?>
                    </td>
                    <td class="satuan"><?php
                        echo $e['satuan_terima']
                    ?>
					</td>

                    <?php if($priviledge == 10 || $priviledge == 2 || $priviledge == 6 || $priviledge == 1){ ?>
                    <td class="qty_bayar">
                    <?php
                        
                        if($arrAcceptanceData['acce_rawstatus'] != 0 && $priviledge == 10 || $priviledge == 2 || $priviledge == 6 || $priviledge == 1 && $bolEditable != null && $inPi == 0): 
                    ?>
                            <div class="form-group">
                                <input type="text" name="jumlah_bayar[<?=$i?>]" class="required currency form-control input-sm" id="jumlah_bayar<?=$i?>" value="<?=$e['acit_quantity_bayar']?>" /></div><input type="hidden" name="qty_inputan_bayar[<?=$i?>]" id="qty_inputan_bayar" value="<?= $e['acit_quantity_bayar'] ?>" >
                            </div>
					<?php
                        else:
                    ?>
                            <div class="form-group">
                                <?= $e['acit_quantity_bayar'] ?>
                                <input type="hidden" name="jumlah_bayar_view[<?=$i?>]" id="jumlah_bayar_view<?=$i?>" value="<?=$e['acit_quantity_bayar']?>" />
                            </div>
						<?php endif; ?>
                    </td>
                    <td class="satuan">
                        <?php echo $e['satuan_bayar'];?>
					</td>
                    <?php } ?>
                    <!-- <td><input type="checkbox" name="tanda_edit[<?=$i?>]" value="<?=$i?>" id="tanda_edit[<?=$i?>]"></td> -->
                    <input type="hidden" name="id_acit[<?=$i?>]" id="id_acit[<?=$i?>]" value="<?=$e['id']?>"/>
                    <input type="hidden" name="purchase_id_edit[<?=$i?>]" id="purchase_id_edit[<?=$i?>]" value="<?=$e['purchase_id_baru']?>"/>
                    <input type="hidden" name="product_id_edit[<?=$i?>]" id="product_id_edit[<?=$i?>]" value="<?=$e['product_id_baru']?>"/>
                    <input type="hidden" name="idsubkon[<?=$i?>]" id="idsubkon[<?=$i?>]" value="<?=$e['idSubKontrak']?>">
                    <input type="hidden" name="purchase_item_id_edit[<?=$i?>]" id="purchase_item_id_edit[<?=$i?>]" value="<?=$e['purchase_item_id']?>"/>
                    <!-- <td class="delete" style="width:45px;">
                        <button type="button" style="width:40px;" class="btn btn-danger" value="<?= $i ?>" name="delete_from_pb" id="delete_from_pb"><i class="fa fa-trash">
                        </i></button>
                    </td> -->
                </tr><?php

					$i++;
				endforeach;

                $iditemawal = $iditemawal.'0';
                $idpurchaseawal = $idpurchaseawal.'0';
			else: ?>
				<tr class="info"><td class="noData" colspan="13"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
			endif; ?>
			</tbody>
		</table></div>
    </div><!--/ Table Selected Items -->
</div>

    <div class="col-xs-12">
    <div class="form-group">
        <button type="submit" name="editAcceptanceItem" id="editAcceptanceItem" value="Edit Acceptance Item" class="btn btn-warning">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </button>
        <!-- <button type="submit" name="backPurchase" id="backPurchase" value="Cancel Edit Purchase" class="btn btn-danger">
            <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
        </button> -->
        <?php  
        $priviledge = $_SESSION['strAdminPriviledge'];
        $status = $arrAcceptanceData['acce_rawstatus'];
        // if($arrAcceptanceData['acce_rawstatus'] != 3){
        //     echo "
        //         <button type='submit' name='saveEdit' id='saveEdit' value='Edit PM Purchase' class='btn btn-primary'>
        //             SAVE</span>
        //         </button>";
        // }
        // else{
        //     echo "
        //     <button type='submit' name='unsaveEdit' id='saveEdit' value='Edit PM Purchase' class='btn btn-primary'>
        //         UNSAVE</span>
        //     </button>";
        // }
        if(($priviledge == 5 || $priviledge == 1 || $priviledge == 6 || $priviledge == 2) && $status==2 && $inPi == 0){
            echo "
            <button type='submit' name='approveSM' id='approveSM' value='Approve BPB' class='btn btn-primary'>
                APPROVE PELAKSANA</span>
            </button>";
        }
        if($status==3 && ($priviledge == 5 || $priviledge == 1 || $priviledge == 6 || $priviledge == 2 && $inPi == 0) ){
            echo "
            <button type='submit' name='unApproveSM' id='unApproveSM' value='Approve BPB' class='btn btn-danger'>
                DECLINE PELAKSANA</span>
            </button>";
        }
        ?>
    </div>
    <?php
        $rawStatus = $arrAcceptanceData['acce_rawstatus'];
        if($canEdit){
            include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
        }
    ?>    
</div>

</div>
	
</div> 

<input type="hidden" name="raw_status" id="raw_status" value="<?= $raw_status ?>"/>
<input type="hidden" name="jumlah_delete" id="jumlah_delete"/>
<input type="hidden" name="kumpulan_delete_id" id="kumpulan_delete_id"/>
<input type="hidden" name="IDSupp" id="IDSupp" value="<?=$arrAcceptanceData['acce_supplier_id']?>"/>
<input type="hidden" id="PurchaseID" name="PurchaseID" value="<?=$arrAcceptanceData['acce_purchase_id']?>"/>
<input type="hidden" id="WarehouseID" name="WarehouseID" value="<?=$arrAcceptanceData['acce_warehouse_id']?>"/>
<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
<input type="hidden" id="idAcceptanceAwal"  value="<?=$idpurchaseawal?>"/>
<input type="hidden" id="idAcceptanceBonusAwal"  value="<?=$idpurchasebonusawal?>"/>
<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItemBonusAwal" name="totalItemBonusAwal" value="<?=$intTotalBonusItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus" value="0"/>
<input type="hidden" name="total_edit_item" id="total_edit_item" value="<?= count($arrAcceptanceItem); ?>" />
<input type="hidden" id="kumpulan_id" name="kumpulan_id" value="0" />

</form></div>