<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-truck"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php
$strSearchAction = site_url('acceptance/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchbpb.php'); ?>

<div class="col-xs-12"><form name="frmAddAcceptance" id="frmAddAcceptance" method="post" action="<?=site_url('acceptance/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('acceptance', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<!-- Lama punya -->
				<!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name').' / '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehouse')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th> -->
			
			<th>No. GR</th>
			<th>Tanggal GR</th>
			<th>Nama Proyek</th>
			<th>Supplier</th>
			<th>Ket. BPB</th>
			<th>No. Purchase Invoice</th>
			<th class="action">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrAcceptance)):
			foreach($arrAcceptance as $e): ?>
			<tr>
				<td><?=$e['acce_code']?></td>
				<!-- <td><?=formatPersonName('ABBR',$e['supp_name'],$e['supp_address'],$e['supp_city'],$e['supp_phone'])?></td> -->
				<td><?=formatDate2($e['cdate'],'d F Y H:i')?></td>
				<!-- <td><?=$e['ware_name']?></td> -->
				<!-- <td><?=setPrice($e['acce_total'])?></td>
				<td><?php switch($e['acce_payment']) {
						case 1: echo 'Tunai'; break;
						case 2: echo 'Kredit'; break;
					} ?> | <?=$e['acce_status']?></td> -->
				<td><?=$e['kont_name']?></td>
				<td><?=$e['supp_name']?></td>
				<td><?php if($e['acit_description']==''){echo"Tidak ada";} else {echo $e['acit_description'];}?></td>
				<?php
					// if($e['approvalPM'] == 2){
					// 	echo "<td>Belum disetujui</td>";
					// }
					// else if($e['approvalPM'] == 3){
					// 	echo "<td><span class='glyphicon glyphicon-ok'></span></td>";
					// }
					// else{
					// 	echo "<td><span class='glyphicon glyphicon-remove'></span></td>";
					// }
					if(!empty($e['pinv_code'])){
						echo "<td class='pi'>".$e['pinv_code']."</td>";
					}
					else{
						echo "<td class='none'>Belum ada</td>";
					}

				?>

				<td class="action">
					<?php if($e['bolAllowView']): ?><a href="<?=site_url('acceptance/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
					<?php if($e['bolBtnPrint']): ?><a  id="print<?=$e['id']?>" href="<?=site_url('acceptance/view/'.$e['id'].'?print=true', NULL, FALSE)?>" id="Print-Link"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
				</td>
				<!-- <td class="delete" style="width:45px;">
					<div class="form-group">
						<button type="submit" style="width:40px;" class="btn btn-danger" value="<?= $e['id'] ?>" name="delAcceptance" id="delAcceptance"><i class="fa fa-trash">
						</i></button>
					</div>
				</td> -->
				<input type="hidden" name="acceptanceCode" id="acceptanceCode" value="<?= $e['acce_code'] ?>"/>
			</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="8"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</form>
</div><!--
--><?/*=site_url('purchase/view/'.$e['id'].'?print=true', NULL, FALSE)*/?>