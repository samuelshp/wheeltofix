<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var mode=0;
var totalItem=0;
var numberItem=0;
var totalItemBonus=0;
$("#data_semua").hide();
var numberItemBonus=0;
var selectedItemBefore=[];
var selectedItemBonusBefore=[];
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
function reset(){
    totalItem=0;
    numberItem=0;
    totalItemBonus=0;
    numberItemBonus=0;
    selectedItemBefore=[];
    selectedItemBonusBefore=[];
    $("#txtNewItem").val('');
    $("#txtNewItemBonus").val('');
    $("#subTotalNoTax").autoNumeric("set",0);
    $("#subTotalTax").autoNumeric("set",0);
    $("#subTotalWithDisc").autoNumeric("set",0);
};

//Baru


$(".PO").hide();
$("#selSupp").val("All");
$("#selSupp").prop('disabled',true);
$("#txtNewItem").prop('disabled',false);
$("#txtNewItemBonus").prop('disabled',false);
$("#supplierID").val(0);
$("#InternalSupp").val(0);
$("input[name*='radioPO']").change(function() {
    if($(this).val()=='0'){
        $(".Supplier").show();
        $(".PO").hide();
        $("#radioSupplier1").prop('checked',true);
        $("#selSupp").val("All");
        $("#loadSupp").html('');
        $("#supplierID").val(0);
        $("#InternalSupp").val(0);
        $("#selSupp").prop('disabled',true);
        $("#txtNewItem").prop('disabled',false);
        $("#txtNewItemBonus").prop('disabled',false);
        $("#warehouseName").prop('disabled', false);
        $("input[name=cbCopyCode]").prop('checked', false);
        $("input[name=cbCopyCode]").prop('disabled', true);
        $('input[name=txtCustomCode]').val('');
    }else{
        $(".Supplier").hide();
        $(".PO").show();
        //$("#selPO").val("");
        $("#loadPO").html('');
        //$("#txtNewItem").prop('disabled',true);
        $("#txtNewItemBonus").prop('disabled',true);
        $("#warehouseName").prop('disabled', true);
        $("input[name=cbCopyCode]").prop('disabled', false);
    }
    //reset();
    // $("#selectedItems tbody").html('');
    // $("#selectedItemsBonus tbody").html('');
    // $("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
    // $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');

});
$("input[name*='radioSupplier']").change(function(){
    if($(this).val()=='0'){
        $("#selSupp").val("All");
        $("#selSupp").prop('disabled',true);
        $("#txtNewItem").prop('disabled',false);
        $("#txtNewItemBonus").prop('disabled',false);
        $("#supplierID").val(0);
        $("#InternalSupp").val(0);
    }else{
        $("#selSupp").val("");
        $("#loadSupp").html('');
        $("#selSupp").prop('disabled',false);
        $("#txtNewItem").prop('disabled',true);
        $("#txtNewItemBonus").prop('disabled',true);
    }
    //reset();
    // $("#selectedItems tbody").html('');
    // $("#selectedItemsBonus tbody").html('');
    // $("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="10"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
    // $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');

});
$("#frmAddAcceptance").validate({
    rules:{},    
    messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
    errorClass: "input-group-addon error",
    errorElement: "label",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});

$("#frmAddAcceptance").submit(function() {
    if(numberItem<=0){
        alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
    }
    var form = $(this);
    $("#dsc4Item").autoNumeric('destroy');
    $('.submited').prop('disabled', false);
    $("#txtSJCode").prop('disabled', false);
    $("#txtEkspedition").prop('disabled', false);
    $('.currency').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
    
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
    
    return true;
});
/*  Batch process */
$("abbr.list").click(function() {
    if($("#selNewItem").children("option:selected").val() > 0) {
        if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
        else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
        $("#selNewItem").children("option:selected").remove();
    }
});
$("abbr.list").hover(function() {
    if($("#hidNewItem").val() != '') {
        strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
        $(this).attr("title",arrNewItem.length + ' Item(s)');
    }
});

var $arrSelectedPO;
var selectedPO;
$("#selPO").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getPurchaseAutoComplete', NULL, FALSE)?>/" + $('input[name="selPO"]').val(),
            beforeSend: function() {
                $("#loadPO").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadPO").html('');
            },
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Supplier').find('item'),function(val,i){
                    var cdate = $(val).find('cdate').text();
                    var intID = $(val).find('id').text();
                    var strPO = $(val).find('prch_code').text();
                    var strName = $(val).find('supp_name').text() + ", " + strPO;
                    display.push({label: strName, value: strPO,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        if($('input[name=cbCopyCode]').is(':checked')) {
            $('input[name=txtCustomCode]').val($(selectedPO).find('prch_code').text());
        }
        $("#supplierID").val($(selectedPO).find('prch_supplier_id').text());
        $("#supplierName").val($(selectedPO).find('supp_name').text());
        $("#WarehouseID").val($(selectedPO).find('prch_warehouse_id').text());
        $("#warehouseName").val($(selectedPO).find('ware_name').text());
        $("#txtTax").val($(selectedPO).find('prch_tax').text());
        $("#dsc4Item").autoNumeric('set',$(selectedPO).find('prch_discount').text());
        $("#txaDescription").val($(selectedPO).find('prch_description').text());
        $("#PurchaseID").val($(selectedPO).find('id').text());
        $("#PurchaseCode").val($(selectedPO).find('prch_code').text());
        var grandtotal=0;
        /*var idIteMpurchased=[];
        var qty1IteMpurchased=[];
        var qty2IteMpurchased=[];
        var qty3IteMpurchased=[];
        var idBonusIteMpurchased=[];
        var qty1BonusIteMpurchased=[];
        var qty2BonusIteMpurchased=[];
        var qty3BonusIteMpurchased=[];*/

        $.ajax({
            url: "<?=site_url('purchase_ajax/getDataForAcceptance', NULL, FALSE)?>/" + $("#PurchaseID").val(),
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                var i=0;
                var z=0;
                $("#selectedItems tbody").html('');
                $("#selectedItemsBonus tbody").html('');
                $("#selectedItemsBonus tbody").append(
                    '<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
                );
                selectedItemBonusBefore=[];
                $.map(xml.find('orderItem').find('item'),function(val,k){
                    var id = $(val).find('prci_product_id').text();
                    var max1 = parseInt($(val).find('prci_quantity1').text());
                    var max2 = parseInt($(val).find('prci_quantity2').text());
                    var max3 = parseInt($(val).find('prci_quantity3').text());
                    var purchased1 = parseInt($(val).find('prci_arrived1').text());
                    var purchased2 = parseInt($(val).find('prci_arrived2').text());
                    var purchased3 = parseInt($(val).find('prci_arrived3').text());
                    var price = $(val).find('prci_price').text();
                    var discount1 = $(val).find('prci_discount1').text();
                    var discount2 = $(val).find('prci_discount2').text();
                    var discount3 = $(val).find('prci_discount3').text();
                    var idProduct = $(val).find('prci_product_id').text();
                    var probTitle = $(val).find('prob_title').text();
                    var procTitle = $(val).find('proc_title').text();
                    var prodTitle = $(val).find('prod_title').text();
                    var prodCode = $(val).find('prod_code').text();
                    /*var j;
                    for(j=0;j<idIteMpurchased.length;j++){
                        if(idIteMpurchased[j]==idProduct){
                            max1 = parseInt(max1)-parseInt(qty1IteMpurchased[j]);
                            max2 = parseInt(max2)-parseInt(qty2IteMpurchased[j]);
                            max3 = parseInt(max3)-parseInt(qty3IteMpurchased[j]);
                        }
                    }*/
                    var tempUnitTitle=[];
                    var tempUnitConv=[];
                    var tempUnitID=[];
                    $.map(xml.find('Unit').find('Unit'+i).find('item'),function(val1,k1){
                        tempUnitTitle.push($(val1).find('unit_title').text());
                        tempUnitConv.push($(val1).find('unit_conversion').text());
                        tempUnitID.push($(val1).find('id').text());
                    });
                    var max=parseInt(max1)*parseInt(tempUnitConv[0])+parseInt(max2)*parseInt(tempUnitConv[1])+parseInt(max3)*parseInt(tempUnitConv[2]);
                    var already=parseInt(purchased1)*parseInt(tempUnitConv[0])+parseInt(purchased2)*parseInt(tempUnitConv[1])+parseInt(purchased3)*parseInt(tempUnitConv[2]);
                    var diff=parseInt(max)-parseInt(already);
                    if(tempUnitConv[0] > 1) {
                        max1=Math.floor(parseInt(diff)/parseInt(tempUnitConv[0]));
                        diff=diff%parseInt(tempUnitConv[0]);
                    }
                    if(tempUnitConv[1] > 1) {
                        max2=Math.floor(parseInt(diff)/parseInt(tempUnitConv[1]));
                        diff=diff%parseInt(tempUnitConv[1]);
                    }
                    max3=Math.floor(parseInt(diff)/parseInt(tempUnitConv[2]));
                    numberItem++;
                    var qty1HiddenClass = '';
                    var qty2HiddenClass = '';
                    var prod_conv1 = $(val).find('prod_conv1').text();
                    var prod_conv2 = $(val).find('prod_conv2').text();
                    if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
                    if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
                    $("#selectedItems tbody").append(
                        '<tr>'+
                            '<td  id="maxItemStrX'+i+'"></td>'+
                            '<td class="qty"><div class="form-group">'+
                            '<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm submited' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="'+max1+'"/></div>' +
                            '<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm submited' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="'+max2+'"/></div>' +
                            '<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm submited" id="qty3ItemX'+i+'" value="'+max3+'"/></div>'+
                            '</div></td>'+
                            '<td>('+prodCode+') <?=formatProductName("","'+prodTitle+'","'+probTitle+'","'+procTitle+'")?></td>'+
                            '<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="'+price+'"/></div></td>'+
                            '<td class="disc"><div class="form-group">'+
                            '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="'+discount1+'" /><label class="input-group-addon">%</label></div></div>' +
                            '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="'+discount2+'" /><label class="input-group-addon">%</label></div></div>' +
                            '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="'+discount3+'" /><label class="input-group-addon">%</label></div></div>'+
                            '</div></td>'+
                            '<td id="subTotalX'+i+'">0</td>'+
                            '<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
                            '<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
                            '<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
                            '<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
                            '<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
                            '<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
                            '<input type="hidden" id="conv1UnitX'+i+'" >' +
                            '<input type="hidden" id="conv2UnitX'+i+'" >' +
                            '<input type="hidden" id="conv3UnitX'+i+'" >'+
                            '<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
                            '<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
                            '<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
                            '<input type="hidden" name="existItem'+i+'" value="1">'+
                            '</tr>');

                    var maxStr='';
                    /*if(tempUnitTitle.length>0)
                        maxStr+=max1+' '+tempUnitTitle[0]+' ';
                    if(tempUnitTitle.length>1)
                        maxStr+=max2+' '+tempUnitTitle[1]+' ';
                    if(tempUnitTitle.length>2)
                        maxStr+=max3+' '+tempUnitTitle[2]+' ';*/
                    if(tempUnitTitle.length>0)
                        maxStr+=max1+' | ';
                    if(tempUnitTitle.length>1)
                        maxStr+=max2;
                    if(tempUnitTitle.length>2)
                        maxStr+=' | '+max3;
                    $("#maxItemStrX"+i).text(maxStr);
                    $("#max1ItemX"+i).val(max1);
                    $("#max2ItemX"+i).val(max2);
                    $("#max3ItemX"+i).val(max3);
                    $("#prodItemX"+i).val(prodTitle);
                    $("#probItemX"+i).val(probTitle);
                    $("#idItemX"+i).val(id);
                    for(var zz=1;zz<=3;zz++){
                        $("#qty"+zz+"ItemX"+i).hide();
                    }
                    var total=0;
                    for(var zz=1;zz<=tempUnitTitle.length;zz++){
                        $("#qty"+zz+"ItemX"+i).show();
                        $("#sel"+zz+"UnitIDX"+i).val(tempUnitID[zz-1]);
                        $("#conv"+zz+"UnitX"+i).val(tempUnitConv[zz-1]);
                        total+=$("#qty"+zz+"ItemX"+i).val()* $("#conv"+zz+"UnitX"+i).val() * (price / $("#conv1UnitX"+i).val());
                        $("#qty"+zz+"ItemX"+i).attr("placeholder",tempUnitTitle[zz-1]);
                    }
                    for(var zz=1;zz<=3;zz++){
                        total=total-(total*$("#dsc"+zz+"ItemX"+i).val()/100);
                    }
                    grandtotal+=total;
                    $("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
                    $("#subTotalX"+i).autoNumeric('set',total);
                    $("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
                    i++;
                    totalItem=i;
                    $("#totalItem").val(totalItem);
                });
                $("#subTotalNoTax").autoNumeric('set',grandtotal);
                var totalwithdisc=grandtotal-$("#dsc4Item").autoNumeric('get');
                if(totalwithdisc<=0){
                    $("#subTotalWithDisc").autoNumeric('set','0');
                    $("#subTotalTax").autoNumeric('set','0');
                }else{
                    $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
                    var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
                    $("#subTotalTax").autoNumeric('set',totalTax);
                }
            }
        });
        $.ajax({
            url: "<?=site_url('purchase_ajax/getBonusDataForAcceptance', NULL, FALSE)?>/" + $("#PurchaseID").val(),
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                var i=0;
                var z=0;
                $("#selectedItemsBonus tbody").html('');

                $.map(xml.find('orderItem').find('item'),function(val,k){
                    var id = $(val).find('prci_product_id').text();
                    var max1 = parseInt($(val).find('prci_quantity1').text());
                    var max2 = parseInt($(val).find('prci_quantity2').text());
                    var max3 = parseInt($(val).find('prci_quantity3').text());
                    var purchased1 = parseInt($(val).find('prci_arrived1').text());
                    var purchased2 = parseInt($(val).find('prci_arrived2').text());
                    var purchased3 = parseInt($(val).find('prci_arrived3').text());
                    var idProduct = $(val).find('prci_product_id').text();
                    var probTitle = $(val).find('prob_title').text();
                    var procTitle = $(val).find('proc_title').text();
                    var prodTitle = $(val).find('prod_title').text();
                    var prodCode = $(val).find('prod_code').text();
                    /*var j;
                    for(j=0;j<idBonusIteMpurchased.length;j++){
                        if(idBonusIteMpurchased[j]==idProduct){
                            max1 = parseInt(max1)-parseInt(qty1BonusIteMpurchased[j]);
                            max2 = parseInt(max2)-parseInt(qty2BonusIteMpurchased[j]);
                            max3 = parseInt(max3)-parseInt(qty3BonusIteMpurchased[j]);
                        }
                    }*/
                    var tempUnitTitle=[];
                    var tempUnitConv=[];
                    var tempUnitID=[];
                    $.map(xml.find('Unit').find('Unit'+i).find('item'),function(val1,k1){
                        tempUnitTitle.push($(val1).find('unit_title').text());
                        tempUnitConv.push($(val1).find('unit_conversion').text());
                        tempUnitID.push($(val1).find('id').text());
                    });
                    var max=parseInt(max1)*parseInt(tempUnitConv[0])+parseInt(max2)*parseInt(tempUnitConv[1])+parseInt(max3)*parseInt(tempUnitConv[2]);
                    var already=parseInt(purchased1)*parseInt(tempUnitConv[0])+parseInt(purchased2)*parseInt(tempUnitConv[1])+parseInt(purchased3)*parseInt(tempUnitConv[2]);
                    var diff=parseInt(max)-parseInt(already);
                    max1=Math.floor(parseInt(diff)/parseInt(tempUnitConv[0]));
                    diff=diff%parseInt(tempUnitConv[0]);
                    max2=Math.floor(parseInt(diff)/parseInt(tempUnitConv[1]));
                    diff=diff%parseInt(tempUnitConv[1]);
                    max3=Math.floor(parseInt(diff)/parseInt(tempUnitConv[2]));
                    var qty1HiddenClass = '';
                    var qty2HiddenClass = '';
                    var prod_conv1 = $(val).find('prod_conv1').text();
                    var prod_conv2 = $(val).find('prod_conv2').text();
                    if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
                    if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
                    $("#selectedItemsBonus tbody").append(
                        '<tr>'+
                            /*'<td></td>'+*/
                            '<td id="maxItemBonusStrX'+i+'"></td>'+
                            '<td class="qty"><div class="form-group">'+
                            '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm submited' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="'+max1+'"/></div>' +
                            '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm submited' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="'+max2+'"/></div>' +
                            '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm submited" id="qty3ItemBonusX'+i+'" value="'+max3+'"/></div>'+
                            '</div></td>'+
                            // '<td>('+prodCode+') <?=formatProductName("","'+prodTitle+'","'+probTitle+'","'+procTitle+'")?></td>'+
                            '<td>('+prodCode+') '+prodTitle+'</td>'+
                            '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                            '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                            '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                            '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                            '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                            '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                            '<input type="hidden" id="conv1UnitBonusX'+i+'" >' +
                            '<input type="hidden" id="conv2UnitBonusX'+i+'" >' +
                            '<input type="hidden" id="conv3UnitBonusX'+i+'" >'+
                            '<input type="hidden" id="max1ItemBonusX'+i+'" name="max1ItemBonus'+i+'">' +
                            '<input type="hidden" id="max2ItemBonusX'+i+'" name="max2ItemBonus'+i+'">' +
                            '<input type="hidden" id="max3ItemBonusX'+i+'" name="max3ItemBonus'+i+'">'+
                            '<input type="hidden" name="existBonusItem'+i+'" value="1">'+
                            '</tr>');
                    var maxStr='';
                    if(tempUnitTitle.length>0)
                        maxStr+=max1;
                    if(tempUnitTitle.length>1)
                        maxStr+=' | '+max2+' | ';
                    if(tempUnitTitle.length>2)
                        maxStr+=max3;
                    $("#maxItemBonusStrX"+i).text(maxStr);
                    $("#max1ItemBonusX"+i).val(max1);
                    $("#max2ItemBonusX"+i).val(max2);
                    $("#max3ItemBonusX"+i).val(max3);
                    $("#prodItemBonusX"+i).val(prodTitle);
                    $("#probItemBonusX"+i).val(probTitle);
                    $("#idItemBonusX"+i).val(id);
                    selectedItemBonusBefore.push(id);
                    for(var zz=1;zz<=3;zz++){
                        $("#qty"+zz+"ItemBonusX"+i).hide();
                    }
                    for(var zz=1;zz<=tempUnitTitle.length;zz++){
                        $("#qty"+zz+"ItemBonusX"+i).show();
                        $("#sel"+zz+"UnitBonusIDX"+i).val(tempUnitID[zz-1]);
                        $("#conv"+zz+"UnitBonusX"+i).val(tempUnitConv[zz-1]);
                        $("#qty"+zz+"ItemBonusX"+i).attr("placeholder",tempUnitTitle[zz-1]);
                    }
                    i++;
                    totalItemBonus=i;
                    numberItemBonus++;
                    $("#totalItemBonus").val(totalItemBonus);
                });
            }
        });
        /*$("#txtAcceptance").prop('disabled', false);*/
        $("#loadPO").html('<i class="fa fa-check"></i>');
    }
});
$("#warehouseName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#warehouseName").val(),
            beforeSend: function() {
                $("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadWarehouse").html('');
            },
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                $arrSelectedWare = xml;
                var display=[];
                $.map(xml.find('Warehouse').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var name = $(val).find('ware_name').text();
                    display.push({label: name, value: name, id:intID});
                });
                if(display.length == 1) {
                    setTimeout(function() {
                        $("#warehouseName").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                        $('#warehouseName').autocomplete('close');
                    }, 100);
                }
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedWare = $.grep($arrSelectedWare.find('Warehouse').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        $("#WarehouseID").val($(selectedWare).find('id').text());
        if($("#WarehouseID").val()!='' && $("#IDSupp").val()!='' ){
            $("#txtNewItem").prop('disabled',false);
            $("#txtNewItemBonus").prop('disabled',false);
        }else{
            $("#txtNewItem").prop('disabled',true);
            $("#txtNewItemBonus").prop('disabled',true);
        }
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
    }
});

setTimeout(function() {
    $("#warehouseName").val(primaryWarehouseName);
    $("#warehouseName").autocomplete('search');
},500);

$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
    $(this).autoNumeric('init', autoNumericOptionsRupiah);
});
$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemX"+at).val()<0){
            $("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
        }
    }
    if($("#prcItemX"+at).autoNumeric('get')<0){
        $("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
    }
    var previous=parseInt($("#subTotalX"+at).autoNumeric('get'));
    /*if(mode==1){*/
        if($("input[type='radio'][name='radioPO']:checked").val()=='1'){
            var max1=$("#max1ItemX"+at).val();
            var max2=$("#max2ItemX"+at).val();
            var max3=$("#max3ItemX"+at).val();
            var qty1=$("#qty1ItemX"+at).val();
            var qty2=$("#qty2ItemX"+at).val();
            var qty3=$("#qty3ItemX"+at).val();
            var conv1=$("#conv1UnitX"+at).val();
            var conv2=$("#conv2UnitX"+at).val();
            var conv3=$("#conv3UnitX"+at).val();
            var max=parseInt(max1)*parseInt(conv1)+parseInt(max2)*parseInt(conv2)+parseInt(max3)*parseInt(conv3);
            var qty=parseInt(qty1)*parseInt(conv1)+parseInt(qty2)*parseInt(conv2)+parseInt(qty3)*parseInt(conv3);
            if(qty>max ){
                $("#qty1ItemX"+at).val(max1);
                $("#qty2ItemX"+at).val(max2);
                $("#qty3ItemX"+at).val(max3);
            }
        }
        /*if(qty1>max1 ){
            $("#qty1ItemX"+at).val(max1);
        }
        if(qty2>max2 ){
            $("#qty2ItemX"+at).val(max2);
        }
        if(qty3>max3 ){
            $("#qty3ItemX"+at).val(max3);
        }*/
        var previous=$("#subTotalX"+at).autoNumeric('get');
        var price = $("#prcItemX"+at).autoNumeric('get');
        var subtotal=
            $("#qty1ItemX"+at).val() * price + 
            ($("#qty2ItemX"+at).val() * price / $("#conv1UnitX"+at).val() * $("#conv2UnitX"+at).val()) + 
            ($("#qty3ItemX"+at).val() * price / $("#conv1UnitX"+at).val() * $("#conv3UnitX"+at).val());
        /* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */
        for(var zz=1;zz<=3;zz++){
            subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
        }
        $("#subTotalX"+at).autoNumeric('set',subtotal);
        var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
        $("#subTotalNoTax").autoNumeric('set',totalNoTax);
        var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
        if(totalwithdisc<=0){
            $("#subTotalWithDisc").autoNumeric('set','0');
            $("#subTotalTax").autoNumeric('set','0');
        }else{
            $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
            var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
            $("#subTotalTax").autoNumeric('set',totalTax);
        }
    /*}*/
    /*else{
        var previous=$("#subTotalX"+at).autoNumeric('get');
        var price = $("#prcItemX"+at).autoNumeric('get');
        var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
        *//* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); *//*

        for(var zz=1;zz<=3;zz++){
            subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
        }
        $("#subTotalX"+at).autoNumeric('set',subtotal);
        var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
        $("#subTotalNoTax").autoNumeric('set',totalNoTax);
        var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
        if(totalwithdisc<=0){
            $("#subTotalWithDisc").autoNumeric('set','0');
            $("#subTotalTax").autoNumeric('set','0');
        }else{
            $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
            var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
            $("#subTotalTax").autoNumeric('set',totalTax);
        }
    }*/
});

var nilai_purchase='';
var nilai_purchase_temp = nilai_purchase;

//Baru

$("#opItems").on("click", "button[name*='delete_from_pb']", function(){
    console.log("isi dari delete");
    $(this).parent().parent().hide();
});

$("#opItems").on("click", "button[name*='add_to_diterima']", function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);    
    i = totalItem;
    $(this).parent().parent().toggle();
    $.ajax({
        url: "<?=site_url('purchase_ajax/getPurchaseItemById', NULL, FALSE)?>/" + at,
        beforeSend: function() {
            //$("#loadPO").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
        },
        complete: function() {
            //$("#loadPO").html('');
        },
        success: function(data){
            if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
            var xml = $(xmlDoc);
            console.log(xmlDoc);
            $arrSelectedPO = xml;
            var display=[];
            $.map(xml.find('PurchaseItem').find('item'),function(val,i){
                i = totalItem;
                var cdate = $(val).find('cdate').text();
                var purchase_code = $(val).find('prch_code').text();
                var nama_owner = $(val).find('cust_name').text();
                var keterangan = $(val).find('prci_description').text();
                var idKontrak = $(val).find('idKontrak').text();
                var kont_name = $(val).find('kont_name').text();
                var ket_pb = $(val).find('proi_description').text();
                var kont_job = $(val).find('kont_job').text();
                var supplier_name = $(val).find('supp_name').text();
                var prci_quantity = $(val).find('qty_tampil').text();
                console.log(prci_quantity);
                var nama_bahan = $(val).find('prod_title').text();
                var id_purchase_item = $(val).find('id').text();
                var purchase_id = $(val).find('purchase_id').text();
                var product_id = $(val).find('prci_product_id').text();
                var satuan_pb = $(val).find('title_pb').text();
                var satuan_terima = $(val).find('title_terima').text();
                var satuan_bayar = $(val).find('title_bayar').text();
                var terterima = $(val).find('purchi_terterima').text();
                var idsubkontrak = $(val).find('idSubKontrak').text();
                var price = $(val).find('prci_price').text();
                nilai_purchase = nilai_purchase_temp.concat(purchase_id);
                nilai_purchase_temp = nilai_purchase;
                if(ket_pb == ''){
                    ket_pb='Tidak ada';
                }
                $("input#subkontrakID").val(idsubkontrak);                
                $("tr.info").hide();
                $("#data_kosong").hide();
                $("#selectedItems tbody").append(
                '<tr style="text-align:center;">'+
                    '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'" id="cbDeleteX'+i+'" value="'+at+'"/></td>'+
                    '<td id="nomor_po'+i+'">'+purchase_code+'</td>'+
                    '<td id="cdate'+i+'">'+cdate+'</td>'+
                    '<td id="supp">'+supplier_name+'</td>'+
                    '<td id="proyek">'+kont_name+'</td>'+
                    // '<td id="id_kontrak'+i+'">'+kont_job+'</td>'+
                    '<td id="nmeItemX'+i+'">'+nama_bahan+'</td>'+
                    '<td><h6 align="left" style="width: 200px;">'+ket_pb+'</h6></td>'+
                    //'<td id="jumlah_lol'+i+'><input type="hidden" name="jumlah_po_fill'+i+'" id="jumlah_po_fill'+i+'" value="'+prci_quantity+'"/>'+prci_quantity+'</td>'+
                    // '<td id="satuan_po'+i+'">'+satuan_bayar+'</td>'+
                    // '<td id="keterangan_po'+i+'">'+keterangan+'</td>'+
                    '<td class="qtyTerima">'+
                    '<input type="text" class="currency" name="qty1ItemTerimaX'+i+'" style="max-width: 100px;" id="qty1ItemTerimaX'+i+'" placeholder="0" value=""/>'+
                    '</td>'+
                    '<td>'+satuan_pb+'</td>'+
                    '<td class="qty">'+
                    '<input type="text" class="currency" style="max-width: 100px;" name="qty1ItemX'+i+'" id="qty1ItemX'+i+'" placeholder="0" value=""/>'+
                    '</td>'+
                    '<td id="satuan_penerimaan"><label id="inputan_satuan'+i+'" name="inputan_satuan'+i+'">'+satuan_terima+'</label></td>'+
                    '<td class="qtyBayar" style="display:none;">'+
                    '<input type="text" class="currency" style="max-width: 100px;" name="qty1ItemBayarX'+i+'" id="qty1ItemBayarX'+i+'" placeholder="0" value="0"/>'+
                    '</td>'+
                    '<td class="satBayar" style="display:none;">'+satuan_bayar+'</td>'+
                    '<input type="hidden" id="jumlah_terterima'+i+'" name="jumlah_terterima'+i+'" value="'+terterima+'"/>'+
                    '<input type="hidden" id="purchase_item_id'+i+'" name="purchase_item_id'+i+'" value="'+at+'"/>'+
                    '<input type="hidden" name="jumlah_awal'+i+'" id="jumlah_awal'+i+'" value="'+prci_quantity+'"/>'+
                    '<input type="hidden" name="existItem'+i+'" value="1">'+
                    '<input type="hidden" name="purchase_item_id'+i+'" id="purchase_item_id'+i+'" value="'+id_purchase_item+'">'+
                    '<input type="hidden" name="product_id'+i+'" id="product_id'+i+'" value="'+product_id+'">'+
                    '<input type="hidden" name="purchase_id_input'+i+'" id="purchase_id_input'+i+'" value="'+purchase_id+'">'+
                    '<input type="hidden" name="purchase_id'+i+'" id="purchase_id'+i+'" value="'+purchase_id+'">'+
                    '<input type="hidden" name="idsubkon'+i+'" id="idsubkon'+i+'" value="'+idsubkontrak+'">'+
                    '<input type="hidden" name="price'+i+'" id="price'+i+'" value="'+price+'">'+
                    '<td id="lokasi_penerimaan'+i+'"><input type="text" id="inputan_keterangan'+i+'" name="inputan_keterangan'+i+'"></td>'+
                    '</tr>');
                    totalItem++;
                    numberItem++;
                    $("#totalItem").val(totalItem);
                    console.log(nilai_purchase);
                    if(priv == 2 || priv == 10 || priv == 6){
                        console.log('Masuk sini');
                        $(".qtyBayar").removeAttr("style");
                        $(".satBayar").removeAttr("style");
                        $(".headerQtyBayar").removeAttr("style");
                        $(".headerSatBayar").removeAttr("style");

                    }                
            });
            
        }
    });
});

var priv = $("#priv").val();


console.log(priv);

//Baru

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemBonusX"+at).val()<0){
            $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
        }
    }
    if($("input[type='radio'][name='radioPO']:checked").val()=='1'){
        var max1=$("#max1ItemBonusX"+at).val();
        var max2=$("#max2ItemBonusX"+at).val();
        var max3=$("#max3ItemBonusX"+at).val();
        var qty1=$("#qty1ItemBonusX"+at).val();
        var qty2=$("#qty2ItemBonusX"+at).val();
        var qty3=$("#qty3ItemBonusX"+at).val();
        var conv1=$("#conv1UnitBonusX"+at).val();
        var conv2=$("#conv2UnitBonusX"+at).val();
        var conv3=$("#conv3UnitBonusX"+at).val();
        var max=parseInt(max1)*parseInt(conv1)+parseInt(max2)*parseInt(conv2)+parseInt(max3)*parseInt(conv3);
        var qty=parseInt(qty1)*parseInt(conv1)+parseInt(qty2)*parseInt(conv2)+parseInt(qty3)*parseInt(conv3);
        if(qty>max ){
            $("#qty1ItemBonusX"+at).val(max1);
            $("#qty2ItemBonusX"+at).val(max2);
            $("#qty3ItemBonusX"+at).val(max3);
        }
    }
});
$("#dsc4Item").change(function(){
    var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
    var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }
})
$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(e){    
    // $("#table_row[1]").css("display", "block");
    // var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    // var row = $('table.op_item tr');
    //var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
    //$("#subTotalNoTax").autoNumeric('set',totalNoTax);
    // var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
    // if(totalwithdisc<=0){
    //  $("#subTotalWithDisc").autoNumeric('set','0');
    //  $("#subTotalTax").autoNumeric('set','0');
    // }else{
    //  $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
    //  var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
    //  $("#subTotalTax").autoNumeric('set',totalTax);
    // }
    // console.log($(this).val()+"HA"+"#table_row["+$(this).val()+"]");
    $(e.target).parents('tr').hide(0, function(){            
        $(e.target).parents('tr').remove()
    });
    // $("input[type='checkbox'][name='cbDeleteX0']").parents('tr').find('tr').remove();
    numberItem--;
    if(numberItem=='0'){
        $("input#subkontrakID").val() = '';
        $("tr.info").show();       
    }
    // selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
    //  return value != $("#idItemX"+at).val();
    // });    
});

var purc_id=[];
var count_keluar = 0;
var jumlahData = 0;

$("#selSupp").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/"  + $("#selSupp").val(),
            beforeSend: function() {
                $("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadSupp").html('');
            },
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                console.log(xmlDoc+" supplier auto complete");
                count_keluar_diajax=0;
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Supplier').find('item'),function(val,i){
                    if(count_keluar_diajax>=1){
                    }
                    else{
                        var name = $(val).find('supp_name').text();
                        var address = $(val).find('supp_address').text();
                        var city = $(val).find('supp_city').text();
                        var code = $(val).find('supp_code').text();
                        var strName=name+", "+ address +", "+city;
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ strName, value:"("+code+") "+ strName,id:intID});
                    }
                    var prch = $(val).find('prch_code').text();
                    purc_id.push(prch);
                    count_keluar++;
                    count_keluar_diajax++;
                    totalItem++;
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        $("#supplierID").val($(selectedPO).find('id').text());
        console.log(totalItem);
        $("#totalItem").val(totalItem);
        console.log($("#jumlahData").val()+"sebelum");
        jumlahData = $("#jumlahData").val();
            // $("#InternalSupp").val($(selectedPO).find('supp_internal').text());
            // $this->
            // reset();
            // $("#selectedItems tbody").html('');
            // $("#selectedItemsBonus tbody").html('');
            // $("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
            // $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
            // $("#loadSupp").html('<i class="fa fa-check"></i>');
            // $("#txtNewItem").prop('disabled',false);
            // $("#txtNewItemBonus").prop('disabled',false);
        
        //getPurchId();
        //console.log($hasilsemua);
        $idSupp = purc_id.toString();
        posisi_index_sementara = 0;
        count_keluar_temp = count_keluar;
        index_coma = 0;
        index = $idSupp.indexOf(',');
        for(var i=0; i<count_keluar; i++){
            if(index > 0){
                $idSupp2 = $idSupp.substring(index_coma, index);
                index_coma = index+1;
                index = $idSupp.indexOf(',', index+1);
            }
            else{
                $idSupp2 = $idSupp.substring(index_coma, $idSupp.length);
            }
            console.log($idSupp2);
            //Tidak dipake
                // while(count_keluar_temp > 0){
                //     if(count_keluar_temp == count_keluar){
                //         $idSupp2 = $idSupp.substring(0, $idSupp.indexOf(','));
                //         posisi_index_sementara = $idSupp.indexOf(',');
                //     }
                //     else if(count_keluar_temp > 2){
                //         index = $idSupp.indexOf(',');
                //         while(true){
                            
                //             if(index < 0){
                //                 break;
                //             }
                //             console.log(index);
                //         }
                //     }
                //     else{
                //         //$idSupp2 = $idSupp.substring($idSupp.indexOf(',')+1, $idSupp.indexOf(','));
                //         $idSupp2 = $idSupp.substring(0, $idSupp.indexOf(','));
                //     }
                //      Sesuatu disini
                //     count_keluar_temp--;
                // }

            $.ajax({
                url: "<?=site_url('purchase_ajax/getPurchaseItemBySupplier', NULL, FALSE)?>/"  + $idSupp2,
                beforeSend: function() {
                    //$("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                },
                complete: function() {
                    //$("#loadSupp").html('');
                },
                success: function(data){
                    if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                    var xml = $(xmlDoc);
                    console.log(xmlDoc);
                    $arrSelectedPO = xml;
                    var display=[];
                    $.map(xml.find('Purchase').find('item'),function(val,i){
                        $("#data_semua").show();
                        $("#no_data").hide();
                        $("#info").hide();
                        $(".noData").hide();
                        var product_id = $(val).find('product_id').text();
                        var purchase_id = $(val).find('id').text();
                        var code = $(val).find('prch_code').text();
                        var cdate = $(val).find('cdate').text();
                        var idKontrak = $(val).find('idKontrak').text();
                        var jumlah = $(val).find('jumlah').text();
                        var satuan = $(val).find('satuan').text();
                        var keterangan=$(val).find('keterangan').text();
                        var bahan = $(val).find('prod_title').text();
                        console.log(code+""+cdate+""+idKontrak+""+jumlah+""+satuan+""+keterangan+""+bahan);
                        $("#opItems tbody").append(
                        '<tr>'+
                            //'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'" id="cbDeleteX'+i+'/></td><td></td>'+
                            //'<td><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
                            '<td id="nomor_po'+i+'">'+code+'</td>'+
                            '<td id="cdate'+i+'">'+cdate+'</td>'+
                            '<td id="id_kontrak'+i+'">'+$(val).find('cust_name').text()+'</td>'+
                            //'<td id="nmeItemX'+i+'">'+nama_bahan+'</td>'+
                            //'<td id="nomor_po'+i+'" value='+code+'> '+code+' - '+cdate+' - '+idKontrak+' </td>'+
                            '<td id="nama_kontrak">'+$(val).find('kont_name').text()+'</td>'+
                            '<td id="nama_pekerjaan">'+$(val).find('kont_job').text()+'</td>'+
                            '<td id="nameItemX'+i+'">'+bahan+'</td>'+
                            //'<td id="satuan_po'+i+'">'+satuan+'</td>'+
                            '<td id="jumlah_po'+i+'">'+$(val).find('prci_quantity1').text()+'</td>'+
                                // '<td class="qty"><div class="form-group">'+
                                // //'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass +'" id="qty1ItemX'+i+'" value="0"/></div>' +
                                // //'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass +'" id="qty2ItemX'+i+'" value="0"/></div>' +
                                // //'<div class="col-xs-8"><input type="text" width="10" name="qty1ItemX'+i+'" class="required number form-control input-sm" id="qty1ItemX'+i+'" value="0"/></div>'+
                                // '</div></td>'+
                                // //'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
                                // '<td class="disc"><div class="form-group">'+
                                // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
                                // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
                                // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
                                // '</div></td>'+
                            //'<td id="subTotalX'+i+'">0</td>'+
                            '<td id="satuan_penerimaan"><div class="col-xs-8"><label id="inputan_satuan'+i+'" name="inputan_satuan'+i+'" value="Rit">Rit</label></div></td>'+
                            '<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
                            '<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
                            '<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
                            '<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
                            '<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
                            '<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
                            '<input type="hidden" id="conv1UnitX'+i+'" >' +
                            '<input type="hidden" id="conv2UnitX'+i+'" >' +
                            '<input type="hidden" id="conv3UnitX'+i+'" >'+
                            '<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
                            '<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
                            '<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
                            '<input type="hidden" name="existItem'+i+'" value="1">'+
                            '<input type="hidden" name="product_id'+i+'" id="product_id'+i+'" value="'+product_id+'">'+
                            '<input type="hidden" name="purchase_code'+i+'" id="purchase_code'+i+'" value="'+code+'">'+
                            '<input type="hidden" name="purchase_id'+i+'" id="purchase_id'+i+'" value="'+purchase_id+'">'+
                            '<input type="hidden" name="supplier_item'+i+'" id="supplier_item'+i+'" value="'+$(selectedPO).find('id').text()+'">'+
                            '<td id="keterangan">'+$(val).find('prci_description').text()+'</td>'+
                            '<td><button type="button" class="btn btn-primary" value="'+jumlahData+'" name="add_to_diterima['+$(val).find('purchase_id').text()+']" id="add_to_diterima['+$(val).find('purchase_id').text()+']"><i class="fa fa-plus"></i></button></td>'+
                            //'<td id="lokasi_penerimaan'+i+'"><div class="col-xs-8"><input type="text" id="inputan_keterangan'+i+'" name="inputan_keterangan'+i+'"></div></td>'+
                        '</tr>');   
                        jumlahData++; 
                    });
                    $("#jumlahData").val(jumlahData);
                    console.log($("#jumlahData").val());
                    //response(display);
                }
            });
        }
        count_keluar=0;
        numberItem++;
        $('#selSupp').val(''); return false;
    }
});

$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/"  + $("#supplierID").val() + "/" + $('input[name="txtNewItem"]').val() + "/" + $("#InternalSupp").val(),
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                console.log(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var type=$(val).find('prod_type').text();
                    if($.inArray(intID, selectedItemBefore) > -1 || type == '2'){

                    }else{
                        display.push({label: strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        console.log(totalItem+"sebelum");
        var i=totalItem;
        console.log(i+"a");
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
        if(numberItem=='0'){
            $("#selectedItems tbody").html('');
        }
        $("#selectedItems tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'" id="cbDeleteX'+i+'"/></td>'+
                '<td id="nomor_po'+i+'">'+$(selectedPO).find('prch_code').text()+' - '+$(selectedPO).find('cdate').text()+' - '+$(selectedPO).find('idKontrak').text()+'</td>'+
                '<td id="nmeItemX'+i+'"></td>'+
                '<td id="satuan_po'+i+'">'+$(selectedPO).find('prci_unit1').text()+'</td>'+
                '<td id="jumlah_po'+i+'">'+$(selectedPO).find('prci_quantity1').text()+'</td>'+
                '<td class="qty"><div class="form-group">'+
                //'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass +'" id="qty1ItemX'+i+'" value="0"/></div>' +
                //'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass +'" id="qty2ItemX'+i+'" value="0"/></div>' +
                '<div class="col-xs-8"><input type="text" width="10" name="qty1ItemX'+i+'" class="required number form-control input-sm" id="qty1ItemX'+i+'" value="0"/></div>'+
                '</div></td>'+
                //'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
                    // '<td class="disc"><div class="form-group">'+
                    // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
                    // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
                    // '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
                    // '</div></td>'+
                //'<td id="subTotalX'+i+'">0</td>'+
                '<td id="satuan_penerimaan"><div class="col-xs-8"><label id="inputan_satuan'+i+'" name="inputan_satuan'+i+'" value="Rit">Rit</label></div></td>'+
                '<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
                '<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
                '<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
                '<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
                '<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
                '<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
                '<input type="hidden" id="conv1UnitX'+i+'" >' +
                '<input type="hidden" id="conv2UnitX'+i+'" >' +
                '<input type="hidden" id="conv3UnitX'+i+'" >'+
                '<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
                '<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
                '<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
                '<input type="hidden" name="existItem'+i+'" value="1">'+
                '<input type="hidden" name="product_id'+i+'" id="product_id'+i+'" value="'+$(selectedPO).find('id').text()+'">'+
                '<td id="lokasi_penerimaan'+i+'"><div class="col-xs-8"><input type="text" id="inputan_keterangan'+i+'" name="inputan_keterangan'+i+'"></div></td>'+
                '</tr>');
        totalItem++;
        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        var id=$(selectedPO).find('id').text();
        selectedItemBefore.push(id);
        $("#nmeItemX"+i).text(strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
        $("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
        $("#idItemX"+i).val(id);
        $("#prodItemX"+i).val(prodtitle);
        $("#probItemX"+i).val(probtitle);
        $("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#qty1ItemX"+i).hide();
        $("#qty2ItemX"+i).hide();
        $("#qty3ItemX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemX"+i).show();
                    $("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
                    $("#sel"+zz+"IteMunitX"+i).text(unitStr[zz-1]);
                }
            }
        });
        numberItem++;
        $("#totalItem").val(totalItem);

        $('#txtNewItem').val(''); return false; // Clear the textbox
    }
});
$("#txtTax").change(function(){
    var totalwithdisc=$("#subTotalWithDisc").autoNumeric('get');
    var totalTax=parseFloat(totalwithdisc)+parseFloat(totalwithdisc*$("#txtTax").val()/100);
    $("#subTotalTax").autoNumeric('set',totalTax);
});
$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="txtNewItemBonus"]').val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strName').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1){

                    }else{
                        display.push({label: strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalItemBonus;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
        if(numberItemBonus=='0'){
            $("#selectedItemsBonus tbody").html('');
        }
        $("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td><label  id="nmeItemBonusX'+i+'"></label></td>'+
                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                '<input type="hidden" name="existBonusItem'+i+'" value="0">'+
                '</tr>');

        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        var id=$(selectedPO).find('id').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text(strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemBonusX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
        $("#idItemBonusX"+i).val(id);
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
                    $("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
                }
            }
        });
        totalItemBonus++;
        numberItemBonus++;
        $("#totalItemBonus").val(totalItemBonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
    }
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberItemBonus--;
    if(numberItemBonus=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});

$(document).on('click', '[id^=nmeItem] .fa-question-circle', function() {
    var $this = $(this);
    $.ajax({
        url: "<?=site_url('purchase_ajax/getLastProductPrice')?>",
        data: {
            product_id: $this.closest('tr').find('[id^=idItem]').val(),
        },
        dataType: 'json',
        success: function(data) {
            var msg = "Harga Pembelian Terakhir: \n\n";
            $.each(data.Items, function() {
                msg = msg + this.strDate + ' - ' + this.supp_name + ' - ' + this.strPrice + "\n";
            });
            alert(msg);
        }
    });
});

$('input[name=cbCopyCode]').change(function() {
    if($(this).is(':checked')) {
        $('input[name=txtCustomCode]').val($(selectedPO).find('prch_code').text());
    } else {
        $('input[name=txtCustomCode]').val('');
    }
});

});</script>