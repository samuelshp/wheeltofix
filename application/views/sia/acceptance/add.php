<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse'), 'link' => site_url('acceptance/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<?php
//$jumlahData = 0;
$jumlahDataTemp = 0;
?>


<div class="col-xs-12">
    <?php
    $strSearchAction = site_url('acceptance', NULL, FALSE);
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchbpbadd.php'); ?>
    <form name="frmAddAcceptance" id="frmAddAcceptance" method="post" action="<?=site_url('acceptance', NULL, FALSE)?>" class="frmShop">
    <div class="col-xs-12"><form name="frmAddAcceptance" id="frmAddAcceptance" method="post" action="<?=site_url('acceptance', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <!-- <div class="col-md-4"> -->
            <!-- <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-reply"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase')?></h3></div>
                <div class="panel-body">
                    <h4>loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchasestatus')</h4>
                    <div class="form-group"><div class="radio-group">
                        <label><input type="radio" name="radioPO" value="0" checked> Tanpa Pembelian</label> 
                        <label><input type="radio" name="radioPO" value="1"> Dengan Pembelian</label>
                        <p class="spacer">&nbsp;</p>
                        <label><input type="checkbox" name="cbCopyCode" value="0" disabled> Salin Kode Pembelian</label>
                    </div></div>
                    <p class="spacer">&nbsp;</p>
                </div>
            </div> -->
            <!-- pembayaran
            <div class="panel panel-primary" id="panelDueDate">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-thumb-tack"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></h3></div>
                <div class="panel-body">
                    <div class="form-group"><div class="input-group">
                            <label class="input-group-addon">loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-duedate')?></label>
                            <input type="text" id="txtJatuhTempo" name="txtJatuhTempo" value="0" class="form-control required" placeholder="loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?>" />
                            <label class="input-group-addon">hari</label>
                        </div></div>
                    <div class="form-group">
                        <label>loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></label>
                        <div class="radio-group">
                            <label><input type="radio" name="intTipeBayar" value="1">Tunai</label>
                            <label><input type="radio" name="intTipeBayar" value="2" checked>Kredit</label>
                        </div>
                    </div>
                </div>
            </div>pembayaran -->
        <!-- </div> -->

        <div class="col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> Data PB</h3></div>
                <div class="panel-body">
                    <div class="col-xs-8">
                        <h6>No BPB : Auto generate</h6>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="col-xs-8">
                        <h6>Tanggal : 
                            <!-- <label><?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?></label> -->
                        </h6>
                        <input type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                        <input type="hidden" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" name="tanggal_hari_ini" id="tanggal_hari_ini"/>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Tambah berdasarkan supplier -->
            <!-- <div class="col-md-7">
                <div class="panel panel-primary">

                    <div class="panel-heading">
                        <h3 class="panel-title PO"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedata')?></h3>
                        <h3 class="panel-title Supplier"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?></h3>
                    </div>
                    <div class="panel-body">
                        <h4 class="PO"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectpurchase')?> <a href="<?=site_url('purchase', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="form-group PO"><div class="input-group"><input type="text" name="selPO" id="selPO" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectpurchase')?>" /><label class="input-group-addon" id="loadPO"></label></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-id')*/?></label><input id="supplierID" class="form-control required"/></div></div>
                        <div class="form-group PO"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername')?> </label><input class="form-control" id="supplierName" disabled/></div></div>
                        <div class="form-group PO"><div class="input-group"><label class="input-group-addon">Dibuat oleh</label><input id="creatorName" class="form-control" disabled/></div></div>
                        <div class="form-group PO"><div class="input-group"><label class="input-group-addon">Priviledge</label><input id="creatorPriviledge" class="form-control" disabled/></div></div>
                        <h4 class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="radio-group">
                            <label class="Supplier"><input type="radio" name="radioSupplier" id="radioSupplier1" value="0" checked> All</label> 
                            <label class="Supplier"><input type="radio" name="radioSupplier" id="radioSupplier2" value="1"> Selected</label>
                        </div>
                        <p class="spacer"></p>
                        <div class="input-group Supplier"><input type="text" id="selSupp" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>"/><label class="input-group-addon" id="loadSupp"></label></div>
                    </div> 

                </div> -->
                <!-- <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'acceptance-acceptance')?></h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <input type="text" name="txtCustomCode" placeholder="Masukkan Kode Manual" class="form-control" />
                        </div>
                    </div>
                </div> -->
            <!-- </div> -->

            <!-- <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?></h3></div>
                    <div class="panel-body">
                        <h4>loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?> <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="form-group"><div class="input-group"><input type="text" id="warehouseName" name="warehouseName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>" /><label class="input-group-addon" id="loadWarehouse"></label></div></div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                    <div class="panel-body">
                        Dibuat
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                    </div>
                </div>
            </div> -->

    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Purchase</h3></div>
        <div class="panel-body">
            <div class="col-xs-12">
                <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive"><table class="op_item table table-bordered table-condensed table-hover" id="opItems">
                <thead>
                <tr>
                    <th>No. Pembelian</th>
                    <!-- <th>Tebal</th>
                    <th>Lebar</th>
                    <th>Panjang</th> -->
                    <th>Tanggal Pembelian</th>
                    <th>Supplier</th>
                    <th>Nama Proyek</th>
                    <!-- <th>Nama Pekerjaan</th> -->
                    <th>Bahan</th>
                    <th>Keterangan PB</th>
                    <th>Qty Pembelian</th>
                    <th>Satuan Pembelian</th>
                    <th>Keterangan</th>
                    <th>Tambah</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        //echo "<script>alert($arrDataPO);</script>";
                        if(!empty($arrDataPO)):
                            foreach($arrDataPO as $e):
                                if($jumlahDataTemp < $jumlahData){
                                    $jumlahDataTemp++;
                                }
                            if($e['prci_description'] == ''){
                                $e['prci_description'] = 'Tidak ada';
                            }
                            if($e['qty_tampil'] > 0){
                    ?>
                        <tr id="table_row[<?= $jumlahDataTemp ?>]">
                            <td><input type="hidden" value='<?= $e['prch_code'] ?>' id="purchase_code_op" name="purchase_code_op"><?= $e['prch_code'] ?></td>
                            <!-- <td>Tebal</td>
                            <td>Lebar</td>
                            <td>Panjang</td> -->
                            <td><?= $e['cdate'] ?></td>
                            <td><?= $e['supp_name'] ?></td>
                            <td><?= $e['kont_name'] ?></td>
                            <!-- <td><input type="hidden" value='<?= $e['kont_name'] ?>' id="kont_name_op" name="kont_name_op"><?= $e['kont_name'] ?></td> -->
                            <!-- <td><?= $e['kont_job'] ?></td> -->
                            <td><?= $e['prod_title'] ?></td>
                            <td><?php if($e['proi_description']=='')echo "Tidak ada"; else echo $e['proi_description'];?></td>
                            <td><?= $e['qty_tampil'] ?></td>
                            <td><?= $e['title_pb'] ?></td>
                            <td><?= $e['prci_description'] ?></td>
                            <td><button type="button" class="btn btn-primary" value="<?= $jumlahData ?>" name="add_to_diterima[<?= $e['id'] ?>]" id="add_to_diterima[<?= $e['id'] ?>]"><i class="fa fa-plus"></i></button></td>
                        </tr>
                    <?php
                            }
                            endforeach;
                            //$jumlahData = $jumlahDataTemp;
                            //$jumlahDataTemp = $jumlahData;
                        else:?>
                            <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    <?php
                        endif;
                    ?>
                </tbody>
            </table></div>
            <?=$strPage?>

            <!-- <div class="row">
                <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalNoTax">0</div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="row">
                <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                <div class="col-sm-2"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
                <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="row">
                <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                <div class="col-sm-2 tdDesc form-group"><div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalTax">0</div>
            </div> -->
            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'acceptance-reciveditem')?></h3></div>
        <div class="panel-body">
            <!-- <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItem" name="txtNewItem" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
            </div></div> -->

            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                    <!-- <th class="PO">Sisa Pembelian</th> -->
                    <th>No. OP</th>
                    <th>Tgl. OP</th>
                    <th>Supplier</th>
                    <th>Nama Proyek</th>
                    <!-- <th>Nama Pekerjaan</th> -->
                    <!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th> -->
                    <!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> Purchase</th>
                    <th>Satuan Purchase</th> -->
                    <th>Bahan</th>
                    <th style="max-width:550px;">Ket. PB</th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> PB</th>
                    <th>Satuan PB</th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> Diterima</th>
                    <th>Satuan diterima</th>
                    <th class="headerQtyBayar" style="display:none;"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> Bayar</th>
                    <th class="headerSatBayar" style="display:none;">Satuan Bayar</th>
                    <th>Ket. BPB</th>
                        <!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
                        <th></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th> -->
                </tr>
                </thead>
                <tbody>
                    <tr class="info"><td class="noData" colspan="14"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                </tbody>
            </table></div>

            <!-- <div class="row">
                <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalNoTax">0</div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="row">
                <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                <div class="col-sm-2"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
                <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div>
            </div>
            <p class="spacer">&nbsp;</p>
            <div class="row">
                <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                <div class="col-sm-2 tdDesc form-group"><div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                <div class="col-sm-2 tdDesc" id="subTotalTax">0</div>
            </div> -->
            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>
    
    <div class="row">

        <!-- <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
            <div class="panel-body">
                <div class="form-group"><textarea name="txaDescription" id="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
            </div>
        </div></div> -->

        <!-- Bonus
        <div class="col-md-8"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
            <div class="panel-body">
                <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                </div></div>

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                        <thead>
                        <tr>
                            //<th class="cb"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')*/?></th>
                            <th class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                            <th class="PO">Jml Di Pembelian (Sisa)</th>
                            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        </tbody>
                    </table></div>
            </div> Table Bonus 
        </div>
        </div>-->

    <!-- <div class="col-md-12"><div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
        
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover" id="selectedItems2">
                <thead>
                <tr>
                    <th>No. PO</th>
                    <th>Tanggal PO</th>
                    <th>No. Kontrak</th>
                    <th>Bahan</th>
                    <th>Qty PB</th>
                    <th>Satuan PB</th>
                    <th>Keterangan</th>
                    <th>Qty Terima</th>
                    <th>Satuan Terima</th>
                    <th>Lokasi</th>
                </tr>
                </thead>
                <tbody id="no_data">
                    <tr id="info">
                        <td class="noData" colspan="10"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                </tbody>
                <tbody id="data_semua">
                </tbody>
            </table>
        </div>
    </div> -->

    <div class="col-xs-12">
        <div class="form-group"><button type="submit" name="smtMakeAcceptance" value="Make Acceptance" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
    </div>
    </div>
        <input type="hidden" id="WarehouseID" name="WarehouseID"/>
        <input type="hidden" id="PurchaseID" name="PurchaseID"/>
        <input type="hidden" id="PurchaseCode" />
        <input type="hidden" id="totalItem" name="totalItem"/>
        <input type="hidden" id="id_purch" name="id_purch"/>
        <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
        <input type="hidden" name="supplierID" id="supplierID"/>
        <input type="hidden" name="subkontrakID" id="subkontrakID"/>
        <input type="hidden" name="InternalSupp" id="InternalSupp"/>
        <input type="hidden" name="priv" id="priv" value="<?=$_SESSION['strAdminPriviledge']?>"/>

    </form>
    </div>
</div>




