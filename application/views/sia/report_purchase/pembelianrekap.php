<?php
// created by patricklipesik
$strPageURL = 'pembelianrekap';
$strPageTitle = 'Rekap Pembelian';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
	0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
//TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
	1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_purchase/'.$strPageURL, NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Awal</label>
				<input type="text" name="txtStart" value="<?php if(!empty($strStart)) echo $strStart; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Akhir</label>
				<input type="text" name="txtEnd" value="<?php if(!empty($strEnd)) echo $strEnd; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<input type="text" id="txtSupplierName"  class="form-control" placeholder="Pilih Supplier"/>
				<label class="input-group-addon" id="loadSupplier"></label>
			</div>
		</div>
		
		<div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary">
				<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?>
			</button><?php
$strPrintURL = site_url('report_purchase/'.$strPageURL.'?print=true&supplierid='.$strSupplierID.'&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
			<a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
			<a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a>
		</div>
		
		<input type="hidden" id="supplierID" name="supplierID"/>
		<input type="hidden" id="supplierCode" name="supplierCode"/>
		<input type="hidden" id="supplierName" name="supplierName"/>
	</form>
	<div class="col-xs-12 col-md-6 pull-left">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3">Periode</div>
					<div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
					<div class="col-md-3">Hari ini</div>
					<div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3">Supplier</div>
					<div class="col-md-9">: <?=$strSupplierName?></div>
				</div>
			</div>
		</div>
	</div>
	<p class="spacer">&nbsp;</p>
	<hr />
	<div class="col-xs-12"><?=$strPage?></div>
	
	<p class="spacer">&nbsp;</p>
	<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Transaksi</th>
					<th>Supplier</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody><?php
			$sumGrandTotal = 0;
			if(!empty($arrItems)):
				foreach($arrItems as $e): ?>
					<tr>
						<td><?=$e['nourut']?></td>
						<td><?=formatDate2($e['tanggal'],'d/m/Y h:i:s')?></td>
						<td><a href="<?=site_url('acceptance/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=$e['faktur']?></a></td>
						<td><?=$e['supplier']?></td>
						<td><?=setPrice($e['total'])?></td>
					</tr>
				<?php
				$sumGrandTotal += $e['total'];
				endforeach;
			else :?>
				<tr><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
			endif;?>
			</tbody><?php
			if(!empty($sumGrandTotal)): ?>
			<thead>
				<tr>
					<th colspan="4" style="text-align:right;">Grand Total</th>
					<th><?=setPrice($sumGrandTotal)?></th>
				</tr>
			</thead><?php
			endif;?>
		</table>
	</div>
	<?=$strPage?>
</div>