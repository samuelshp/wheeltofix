<!-- created by patricklipesik -->
<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#searchForm").submit(function() {
	if($("#supplierName").val()=='') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectsupplier')?>"); return false;
	}
	return true;
});

$("#txtSupplierName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#txtSupplierName").val(),
			beforeSend: function() {
				$("#loadSupplier").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupplier").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSupplier = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
					
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label: strName, value: name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSupplier = $.grep($arrSelectedSupplier.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$('input[name="supplierID"]').val($(selectedSupplier).find('id').text());
		$("#supplierCode").val($(selectedSupplier).find('supp_code').text());
		$("#supplierName").val($(selectedSupplier).find('supp_name').text());
		$("#loadSupplier").html('<i class="fa fa-check"></i>');
	}
});
});</script>