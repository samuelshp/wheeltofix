<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-file-text-o"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php
$strSearchAction = site_url('purchase/browse', NULL, FALSE);
// include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchquotation.php'); ?>

<div class="col-xs-12"><form name="frmAddPicklist" id="frmAddPicklist" method="post" action="<?=site_url('pick_list/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('pick_list/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-so_id')?></th>			
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-wh_id')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-qty_display')?></th>
			<th class="action" style="width:5%;">Details</th>
		</tr></thead>
		<tbody>
		<?php
		
		// Display data in the table
		echo var_dump($arrPicklist);
		if(!empty($arrPicklist)):			
			foreach($arrPicklist as $p): ?>
			<tr>				
				<td><?=$p['pcli_kode']?></td>
				<td><?=$p['inor_code']?></td>		
				<td><?=$p['ware_name']?></td>		
				<td><?=$p['pcli_qty_display']?></td>		
				<td class="action">
					<?php if($bolAllowView): ?><a style="margin:0" href="<?=site_url('Pick_list/view/'.$p['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
				</td>
			</tr><?php endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
	</form>
</div>