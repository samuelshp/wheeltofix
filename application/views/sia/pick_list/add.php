<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-file-text-o"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title'), 'link' => site_url('pick_list/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);


include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<form name="" id="" method="post" action="" class="frmShop">

    <div class="col-md-12">
        <div class="row-md-12">

            <!-- nomor SO -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-sodata')?></h3>
                </div>
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selectSO')?> <a href="<?=site_url('purchase_order', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <div class="form-group"><div class="input-group"><input type="text" name="selSO" id="selSO" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selectSO')?>" /><label class="input-group-addon" id="loadSO"></label></div></div>
                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Username Pembuat</label><input id="creatorName" class="form-control" disabled/></div></div>
                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Priviledge</label><input id="creatorPriviledge" class="form-control" disabled/></div></div>                            
                </div>
                </div>
            </div>

            <!-- gudang -->
            <div class='col-md-4'>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-building-o"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-warehousedata')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <h4>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selectwarehouse')?>
                            <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>"
                                target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
                        </h4>
                        <select type="text" name="selWarehouse" id="selWarehouse" class="form-control chosen" required
                            placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php foreach($arrWarehouse as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['ware_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <!-- tanggal -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-date')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Dibuat</div>
                                <input type="text" id="txtDateBuat" name="txtDateBuat"
                                    value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>"
                                    class="form-control input-sm required jwDateTime" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- BARANG -->
        <div class="col-md-12">
            <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-invoiceitems')?></h3></div>
            <div class="panel-body">
                <!--input barang-->
                <!-- <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang [F6]" title="Tambah Barang [F6]" data-toggle="tooltip" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                </div></div> -->

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-item')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-qtySO')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-qtyPick')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-opsi')?></th>
                    </tr>
                    </thead>
                    <tbody><?php
                    if(!empty($b['book_lapangan_id'])): 
                    else: ?>  
                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                    endif; ?>  
                    </tbody>
                </table></div> <!--table-responsive-->

                <!-- <div class="row">
                    <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                    <div class="col-sm-2 tdDesc" id="subTotalNoTax">0</div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                    <div class="col-sm-2"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
                    <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                    <div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div>
                </div>   -->
            </div>
            </div>
            <div class="form-group">
                <button type="submit" name="addpicklist"  class="btn btn-primary">
                    <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                </button>
            </div>
        </div>
</form>