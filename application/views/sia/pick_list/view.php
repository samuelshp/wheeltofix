<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-sticky-note"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title'), 'link' => site_url('pick_list/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail Picklist', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-info-circle"></i> Detail</h3>
            </div>
            <div class="panel-body">
                <!-- kodepicklist -->
                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-code')?>
                    </div>
                    <div class="col-sm-8 col-md-10">
                        <?php foreach($arrPicklist as $p): ?>
                        <?=$p['pcli_kode']?>
                        <?php endforeach;?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- kodeinvoice -->
                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-so_id')?>
                    </div>
                    <div class="col-sm-8 col-md-10">
                        <?php foreach($arrPicklist as $p): ?>
                        <?=$p['inor_code']?>
                        <?php endforeach;?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- Gudang -->
                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-warehouse')?>
                    </div>
                    <div class="col-sm-8 col-md-10">
                        <?php foreach($arrPicklist as $p): ?>
                        <?=$p['ware_name']?>
                        <?php endforeach;?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-dropbox"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-invoiceitems')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <!-- <div class="form-group"><div class="input-group addNew">
                            <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang [F6]" title="Tambah Barang [F6]" data-toggle="tooltip" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                    </div></div> -->
    
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover" id="selectedItems">
                                <thead>
                                    <tr>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-item')?></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-qtySO')?></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-qtyPick')?></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-status')?></th>
                                    </tr>
                                </thead>
                                <tbody><?php
                        if(!empty($b['book_lapangan_id'])): 
                        else: ?>
                                    <tr class="info">
                                        <td class="noData" colspan="7">
                                            <?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?>
                                        </td>
                                    </tr><?php
                        endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        <!-- <div class="form-group action">    
    <?php if($bolBtnEdit): ?>                        
    <button type="submit" name="smtUpdateKontrakPembelian" id="smtUpdateKontrakPembelian" value="Update Kontbeli" class="btn btn-primary">
        <span class="fa fa-edit" aria-hidden="true"> </span> 
    </button>
    <button type="submit" name="subDelete" id="subDeleteInvoice" value="Delete Invoice" class="btn btn-danger pull-right" style="display: none">
        <span class="fa fa-trash" aria-hidden="true"> </span> 
    </button>                        
    <?php endif; ?>
    <a href="<?=site_url(array('kontrak_pembelian/browse',$intInvoiceID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
</div> -->
</div>