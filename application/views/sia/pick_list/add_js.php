<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
var totalitem=0;
var numberItem=0;
var subtotal=0;
var $arrSelectedPO;
var selectedPO;
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency, .qty").autoNumeric('init',autoNumericOptionsRupiah);
$("#frmAddQuotation").submit(function(){
    var form = $(this);
    $('.currency, .qty').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
    
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
    
    return true;
});
$("#customerName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('api/customer/list?h="+userHash+"&filter_cust_status=2&filter_cust_name=*')?>"+$("#customerName").val()+"*",
            beforeSend: function() {                
                $("#loadItem").html('Loading data');
            },
            complete: function() {                
                $("#loadItem").html('');
            },
            success: function(result){
                if(!result.success) $("#loadItem").html('No data');
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['id'];
                    var name = arr['cust_name'];
                    display.push({label: name, value: name, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO, function(el) {            
            return el.id == ui.item.id
        });                                
        var itemSelected = $(selectedPO)[0];        
        $("#intCustomerID").val(itemSelected.id);
        $("#customerAddress").val(itemSelected.cust_address);
        $("#customerCity").val(itemSelected.cust_city);
        $("#customerPhone").val(itemSelected.cust_phone);
        $("#customerOutletTypeStr").val(itemSelected.cust_limitkredit);
        $("#customerLimit").val(itemSelected.cust_limitkredit);
        $("#customerName").val(itemSelected.cust_name); 
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
        loadProject(itemSelected.id).done(function() {
            loadWarehouse('kontrak', itemSelected);
            loadSubkontrak(itemSelected);
        });
        return false;
        
    }
});

$("#selProyek").change(function(){
    var kontrakID = $(this).val();
    loadWarehouse('kontrak', kontrakID);
    loadSubkontrak(kontrakID);
});

var i=0;
$("#txtNewItem").keyup(function(e){
        if(e.keyCode == 8 || e.keyCode == 46) $("#loadItem").html('');
});
$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {        
        $.ajax({
            url: "<?=site_url('api/product_unit/list?h="+userHash+"&filter_prod_status=2&filter_prod_title=*')?>"+$("#txtNewItem").val()+"*",
            beforeSend: function() {                
                $("#loadItem").html('Loading data');
            },
            complete: function() {                
                $("#loadItem").html('');
            },
            success: function(result){
                if(!result.success) $("#loadItem").html('No data');
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['prod_id'];
                    var name = arr['prod_title'];
                    display.push({label: name, value: name, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO, function(el) {            
            return el.prod_id == ui.item.id
        });                                
        var i=totalitem;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        $("#totalItem").val(totalitem);
        var bahan = $(selectedPO)[0].prod_title;
        var harga = $(selectedPO)[0].prod_price;
        var satuan = $(selectedPO)[0].title_pb;
        var terima = $(selectedPO)[0].title_bpb;
        var intID = $(selectedPO)[0].prod_id;
        $("#selectedBahans tbody tr.info").hide();
        $("#selectedBahans tbody").append(
        '<tr name=itemrow'+totalitem+'>'+
            '<td class="cb"><input type="checkbox" name="cbDeleteX'+totalitem+'"/></td>'+
            '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct[]" id="idProduct'+totalitem+'" value="'+intID+'"><input type="hidden" name="isiBahanText[]" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+    
            '<td id="isiHarga'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control input-sm currency" type="text" id="inputHarga'+totalitem+'" value="'+harga+'" placeholder="0" name="inputHarga[]"><input type="hidden" name="inputAwal[]" id="inputAwal'+totalitem+'" value="0"></div></td>'+
            '<td id="isiQty'+totalitem+'"><div class="input-group"><input class="form-control input-sm qty" type="text" id="inputQty'+totalitem+'" value="" placeholder="0" name="inputQty[]"><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="0"><div class="input-group-addon">'+satuan+'</div></div></td>'+
            '<td id="isiDsc'+totalitem+'"><div class="input-group"><input class="form-control input-sm qty" type="text" id="inputDsc'+totalitem+'" value="0" name="inputDsc[]"><div class="input-group-addon">%</div></div></td>'+
            '<td id="isiSubtotal'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control input-sm qty" type="text" id="inputSubtotal'+totalitem+'" value="0" placeholder="0" name="inputSubtotal[]" readonly></div></td>'+
            '<td class="ket"><input class="form-control input-sm" type="text" id="isiKeterangan'+totalitem+'" name="isiKeterangan[]"/></td>'+
            '<input type="hidden" name="isiTerPB[]" id="isiTerPB'+totalitem+'" value="0">'+
        '</tr>');

        totalitem++;
        numberItem++;
        $(".currency, .qty").autoNumeric('init',autoNumericOptionsRupiah);
        $("#totalItem").val(totalitem);
        $("#txtNewItem").val(''); return false;
    }
});

$("#selectedBahans tbody").on('keyup', '.qty, .currency', function(e) {        
    calculateSubtotal(e);
});

$("#txtTax").keyup(function() {
    calculateFinaltotal();
});

$("#selectedBahans tbody").on('click', "input[type='checkbox'][name^='cbDeleteX']", function(e){    
    totalitem--;
    numberItem--;
    $(e.target)
    .parents('tr')
    .hide(1, function(){            
        $(e.target).parents('tr').remove()
        calculateGrandtotal();
    });        
    if(numberItem == 0){
        $("#selectedBahans tbody tr.info").show();
    }    
    $("#totalItem").val(totalitem);

});

function loadProject(customerID = 0){
	fitlerCustomerID = (customerID != 0) ? '&filter_owner_id='+customerID : '';
    return $.ajax({
        url: "<?=site_url('api/kontrak/list?h="+userHash+"&filter_status=2"+fitlerCustomerID+"&sort_kont_name=asc')?>",
        dataType: "json",
        method: "GET",
        success: function(result) {
        	if (result.success) {
	        	$('#selProyek').html('<option disabled value="0" selected>Select your option</option>');            
	                $.each(result.data, function(i,arr) {
	                    $("#selProyek").append('<option value="' + arr['id'] + '">' + arr['kont_name'] + '</option>');        
	                });
	            $("#selProyek").trigger("chosen:updated");
            }
        }        
    });  
}

function loadSubkontrak(kontrak) {
    return $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + kontrak,
        cache : false,
        success : function(result){
            $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
            if (result.count > 0) {
                $.each (result.data, function (index,arr) {                    
                    $('#selPekerjaan').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
                });
            }
            $("#selPekerjaan").trigger("chosen:updated");
        }
    });
}


function loadWarehouse(by,id) {
    if (by == 'kontrak') {
        var filter = "filter_kont-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $('#selWarehouse').append('<option value="' + arr['ware_id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }

    if (by == 'warehouse') {
        var filter = "filter_t-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $('#selWarehouse').append('<option value="' + arr['id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }
}

function calculateSubtotal(e) {
    var currentRow = $(e.currentTarget).closest('tr');
    var harga = currentRow.find("input[name^='inputHarga']").autoNumeric('get');
    var qty = currentRow.find("input[name^='inputQty']").autoNumeric('get');
    var disc = currentRow.find("input[name^='inputDsc']").autoNumeric('get');

    var amountDisc = (harga*qty)*(disc/100)
    
    subtotal = (harga*qty)-amountDisc;

    currentRow.find("input[name^='inputSubtotal']").autoNumeric('set',subtotal);

    calculateGrandtotal();
}

function calculateGrandtotal() {
    var grandTotal = 0;
    $("input[name^='inputSubtotal']").each(function(i,el) {
        grandTotal += parseFloat($(el).autoNumeric('get'));
    });    
    $("tfoot #grandTotal, #subTotalNoTax").autoNumeric('set',grandTotal);
    calculateFinaltotal();
}

function calculateFinaltotal() {
    var dpp = parseFloat($("#subTotalNoTax").autoNumeric('get'));
    var tax = parseFloat($("#txtTax").autoNumeric('get'));
    finalTotal = dpp + (dpp*tax/100);
    $("#subTotalTax").autoNumeric('set',finalTotal);
}


$('.chosen').chosen({search_contains: true});
</script>