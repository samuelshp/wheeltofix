<?php
if(!empty($strSearchKey) || !empty($strSearchDate) || !empty($strSearchDate2) || !empty($intSearchStatus)) {
    $strSearchFormClass = ' has-warning';
    $strSearchButtonClass = ' btn-warning';
} else {
    $strSearchDate = '';
    $strSearchKey = '';
    $strSearchFormClass = ''; 
    $strSearchButtonClass = ' btn-primary';
} ?>
	<form name="searchForm" method="post" action="<?=$strSearchAction?>" class="form-inline pull-right" style="">
        <div class="input-group<?=$strSearchFormClass?>">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	        <input type="text" name="txtSearchDate" value="<?=$strSearchDate?>" class="form-control jwDate input-sm" placeholder="Search By Date Start" style="width: 110px;" />
            <span class="input-group-addon">-</span>
            <input type="text" name="txtSearchDate2" value="<?=$strSearchDate2?>" class="form-control jwDate input-sm" placeholder="Search By Date End" style="width: 110px;" />
			<span class="input-group-addon"><i class="fa fa-user"></i></span>
	        <input type="text" id="txtSearchNo" name="txtSearchNo" class="form-control input-sm" placeholder="Search No." style="width: 110px;" />
	        <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" id="txtSearch" name="txtSearch" class="form-control" placeholder="Search <?=($intType == 3) ? "Keterangan": "Supplier"; ?>" style="width: 120px; min-width: auto;" />
            <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary<?=$strSearchButtonClass?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
        </div>
    </form>