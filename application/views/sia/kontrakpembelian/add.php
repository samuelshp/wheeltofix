<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-browse'), 'link' => site_url('kontrak_pembelian/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmAddKontrakPembelian" id="frmAddKontrakPembelian" method="post" action="<?=site_url('kontrak_pembelian', NULL, FALSE)?>" class="frmShop">
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-date')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtDate" type="date" value="<?=date('Y-m-d')?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-proyek')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intProyekID" required>
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrProyek as $e) {?>
                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-supplier')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intSupplierID" required>
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrSupplier as $e) {?>
                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-itemname')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intProductID" required id="product">
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrProduct as $e) {?>
                        <option data-satuan="<?=$e['unit_title']?>" value="<?=$e['id']?>"><?=$e['prod_title']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-qty')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtQty" type="number" placeholder="0" required id="qty" step="0.001" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-satuan')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <input class="form-control" name="txtSatuan" type="text" readonly id="satuan" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-price')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtPrice" type="number" placeholder="0" required id="price" step="0.01" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amount')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtAmount" type="number" placeholder="0" readonly required id="amount" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-po')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtPO" type="number" placeholder="0" required step="0.001" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-description')?>
            </div>
            <div class="col-sm-8 col-md-10">
               <textarea class="form-control" name="txtDescription"></textarea>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="form-group action">
    <button type="submit" name="smtSaveKontrakPembelian" id="smtProcessType" value="Add" class="btn btn-primary"><span class="fa fa-plus"></span></button>&nbsp;
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
    <a href="<?=site_url(array('table/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
</div>
</form></div>