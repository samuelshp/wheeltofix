<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-browse'), 'link' => site_url('kontrak_pembelian/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmEditKontrakPembelian" id="frmEditKontrakPembelian" method="post" action="<?=site_url('kontrak_pembelian', NULL, FALSE)?>" class="frmShop">
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Summary</h3></div>
    <div class="panel-body">        
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-date')?> *
                <input class="form-control " name="intID" type="hidden" value="<?=$arrBookingData['id']?>" />
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control " name="txtDate" type="date" value="<?=$arrBookingData['kopb_date']?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-proyek')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
                <select class="form-control chosen" name="intProyekID" required>
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrProyek as $e) {?>
                        <option value="<?=$e['id']?>" <?php echo ($arrBookingData['kopb_proyek_id'] == $e['id']) ? "selected":null ?> ><?=$e['kont_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-supplier')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
                <select class="form-control chosen" name="intSupplierID" required>
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrSupplier as $e) {?>
                        <option value="<?=$e['id']?>" <?php echo ($arrBookingData['kopb_supplier_id'] == $e['id']) ? "selected":null ?>><?=$e['supp_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-itemname')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
                <select class="form-control chosen" name="intProductID" required id="product">
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrProduct as $e) {?>
                        <option data-satuan="<?=$e['unit_title']?>" value="<?=$e['id']?>" <?php echo ($arrBookingData['kopb_product_id'] == $e['id']) ? "selected":null ?>><?=$e['prod_title']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-qty')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
               <input class="form-control" name="txtQty" type="number" placeholder="0" required id="qty" step="0.001" value="<?=$arrBookingData['kopb_qty']?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-satuan')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
                <input class="form-control" name="txtSatuan" type="text" readonly id="satuan" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-price')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
               <input class="form-control" name="txtPrice" type="number" placeholder="0" required id="price" step="0.01" value="<?=$arrBookingData['kopb_price']?>"/>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amount')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
               <input class="form-control" name="txtAmount" type="text" class="currency" placeholder="0" readonly required id="amount" value="<?=$arrBookingData['kopb_amount']?>"/>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-po')?> *
            </div>
            <div class="col-sm-8 col-md-10">            
               <input class="form-control" name="txtPO" type="number" placeholder="0" required step="0.001" value="<?=$arrBookingData['kopb_po']?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-description')?>
            </div>
            <div class="col-sm-8 col-md-10">            
               <textarea class="form-control" name="txtDescription"><?=$arrBookingData['kopb_description']?></textarea>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="form-group action">    
    <?php if($bolBtnEdit): ?>                        
    <button type="submit" name="smtUpdateKontrakPembelian" id="smtUpdateKontrakPembelian" value="Update Kontbeli" class="btn btn-primary">
        <span class="fa fa-edit" aria-hidden="true"> </span> 
    </button>                        
    <?php endif; ?>
    <!-- <button type="submit" name="smtSaveKontrakPembelian" id="smtProcessType" value="Add" class="btn btn-primary"><span class="fa fa-plus"></span></button>&nbsp; -->    
    <a href="<?=site_url(array('kontrak_pembelian/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
</div>
</form></div>