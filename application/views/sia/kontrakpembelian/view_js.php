<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<!-- <script type="text/javascript" src="http://localhost/sia/asset/chosen/chosen.jquery.min.js"></script> -->
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">    
$(".currency").autoNumeric('init', autoNumericOptionsRupiah); 
    $('.chosen').chosen();    
    $(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
    $("#satuan").val($("#product").find(':selected').data('satuan'));
    $("#qty").keyup(function(){
        $("#amount").val($("#qty").val()*$("#price").val());
    });
    $("#price").keyup(function(){
        $("#amount").val($("#qty").val()*$("#price").val());
    });
    $("#product").change(function(){
	    $("#satuan").val($(this).find(':selected').data('satuan'));
    })
    $("#frmEditKontrakPembelian").submit(function(){
        return confirm("Apakah anda yakin akan mengupdate kontrak pembelian? ");        
    });
</script>