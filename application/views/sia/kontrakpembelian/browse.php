<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12"><?php
$strSearchAction = site_url('purchase_order/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchelement.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('kontrak_pembelian', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-date')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-project')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-supplier')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-itemname')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amount')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amountpo')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amountpi')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-amountremain')?></th>
            <th class="action">Action</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrKontrakPembelian)):    
    foreach($arrKontrakPembelian as $e): ?>
        <tr>
            <td><?=$e['kopb_code']?></td>
            <td><?=formatDate2($e['kopb_date'],'d F Y')?></td>
            <td><?=$e['kont_name']?></td>
            <td><?=$e['supp_name']?></td>
            <td><?=$e['prod_title']?></td>
            <td><?=$e['kopb_qty']?></td>
            <td><?=$e['kopb_po']?></td>
            <td>0</td>
            <td><?=$e['kopb_qty']-$e['kopb_po']?></td>
            <td class="action">
                <?php if($bolButton['bolAllowView']): ?><a href="<?=site_url('kontrak_pembelian/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
                <?php if($bolButton['bolBtnPrint']): ?><a href="<?=site_url('kontrak_pembelian/view/'.$e['id'].'?print=true', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>  
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>