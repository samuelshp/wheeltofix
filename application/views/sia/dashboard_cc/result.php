<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> Dashboard Cost Control', 'link' => site_url('dashboard_cc/browse', NULL, FALSE)),
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">

<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />
<form name="frmGenerateDashboardCC" id="frmGenerateDashboardCC" method="post" action="<?=site_url('dashboard_cc/', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Nama Proyek
                    </h3>
                </div>
                <div class="panel-body">
                    <select type="text" id="selProyek" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>" name="intKontrakID">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrKontrak as $e) { ?>
                            <option value="<?=$e['id']?>" <?php if($e['id'] == $intKontrakID) echo 'selected';?>><?=$e['kont_name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                    <!-- #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> -->
                        Nama Pekerjaan
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control chosen" id="selPekerjaan" name="intSubkontrakID">
                            <option value="0" disabled>Select your option</option>
                        <?php foreach($arrSubkontrak as $e) { ?>
                            <option value="<?=$e['subkont_id']?>" <?php if($e['id'] == $intSubkontrakID) echo 'selected';?>><?=$e['job']?></option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                    <!-- #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> -->
                        Nama Bahan
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control chosen" id="selBahan" name="intProductID">
                            <option value="0" <?php if($intProductID == 0) echo 'selected';?>>Semua Bahan</option>
                        <?php foreach($arrBahan as $e) { ?>
                            <option value="<?=$e['product_id']?>" <?php if($e['product_id'] == $intProductID) echo 'selected';?>><?=$e['prod_title']?></option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <button type="submit" name="makeDashboardCC" id="makeDashboardCC" value="Generate" class="btn btn-primary">
                    Generate
                </button>                    
            </div>
        </div>
    </div>
</form>

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tabMaterial" aria-controls="tabMaterial" role="tab" data-toggle="tab">Material</a></li>
        <li role="presentation"><a href="#tabNonMaterial" aria-controls="tabNonMaterial" role="tab" data-toggle="tab">Non Material</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabMaterial">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-dropbox"></i> Material
                            <div class="btn-print-group pull-right">
                                <button type="button" name="subPrint" value="Print" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-print"></i> Print</button>
                                <button type="button" name="subPrint" value="Excel" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-file-excel-o"></i> Export To CSV</button>
                            </div>
                            </h3>
                        </div>
                        <div class="panel-body">

                        <div class="table-responsive" id="section-to-print"><table class="table table-bordered table-condensed table-hover" id="selectedBahans">
                        <thead>
                            <tr>
                                <th colspan="4" class="text-center">RAB</th>
                                <th colspan="3" class="text-center">PB</th>
                                <th colspan="5" class="text-center">OP</th>
                                <th colspan="5" class="text-center">PI</th>
                            </tr>
                            <tr>
                                <th style="width:100px;">Bahan</th>
                                <th>Qty</th>
                                <th>HPP</th>
                                <th>Total</th>
                                <th>Qty</th>                        
                                <th>PB</th>
                                <th>Sisa Qty</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th>Sisa (Rp.)</th>
                                <th>Sisa Qty</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th>Sisa (Rp.)</th>
                                <th>Sisa Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($arrData)) { foreach($arrData as $index => $e) {                                
                                $arrPB = array();
                                $arrP = array();
                                $arrPI = array();
                                if(!empty($e['strPB'])) $arrPB = explode('|', $e['strPB']);
                                if(!empty($e['strP'])) $arrP = explode('|', $e['strP']);                        
                                if(!empty($e['strPI'])) $arrPI = explode('|', $e['strPI']);                        
                                $maxPB = count($arrPB);
                                $maxP = count($arrP);
                                $maxPI = count($arrPI);
                                $max = max($maxPB,$maxP,$maxPI);                        
                            ?>
                            <tr style="border-top:2px solid black;">
                                <td rowspan="<?=($max+1)?>" style="vertical-align: middle;"><b><?=$e['prod_title']?> 
                                <?php if($e['substitusi'] != 0): ?>
                                <span data-toggle="tooltip" title="Substitusi dari : <?=$e['prod_title_sub']?>"><i class="fa fa-info-circle"></i></span>
                                <?php endif; ?>
                                </b></td>
                                <td><b><?=setPrice($e['qty'],'',FALSE)?> <?=$e['satuan_pb']?></b></td>
                                <td><b><?=setPrice($e['hpp'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['jumlah'],'',FALSE)?></b></td>
                                <td style="border-left:2px solid black;"><b><?=setPrice($e['qty_pb'],'',FALSE)?> <?=$e['satuan_pb']?></b></td>
                                <td><b><?=setPrice($e['sisa_pb'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sisa_qty_pb'],'',FALSE)?></b></td>
                                <td style="border-left:2px solid black;"><b><?=setPrice($e['qty_p'],'',FALSE)?> <?=$e['satuan_bayar']?></b></td>
                                <td><b><?=setPrice($e['price_p'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sum_p'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sisa_p'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sisa_qty_p'],'',FALSE)?></b></td>
                                <td style="border-left:2px solid black;"><b><?=setPrice($e['qty_pi'],'',FALSE)?> <?=$e['satuan_bayar']?></b></td>
                                <td><b><?=setPrice($e['price_pi'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sum_pi'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sisa_pi'],'',FALSE)?></b></td>
                                <td><b><?=setPrice($e['sisa_qty_pi'],'',FALSE)?></b></td>
                            </tr>
                            <?php for($i=0; $i<$max; $i++) { ?>
                            <tr >
                                <td colspan="3"></td>
                                <?php if ($i < $maxPB) { $temp = explode(',', $arrPB[$i]); ?>
                                    <td style="border-left:2px solid black;"><?=setPrice($temp[3],'',false).' '.$e['satuan_bayar']?></td>                            
                                    <td><a href="<?=site_url('purchase_order/view/'.$temp[0])?>" target="_blank"><?=$temp[1]?></a></td>
                                    <td>&nbsp;</td>
                                <?php } else { ?>
                                    <td colspan="3" style="border-left:2px solid black;">-</td>
                                <?php }?>
                                <?php if ($i < $maxP) { $temp = explode(',', $arrP[$i]); ?>
                                    <td style="border-left:2px solid black;"><?=setPrice($temp[3],'',false).' '.$e['satuan_bayar']?></td>
                                    <td><?=setPrice($temp[2]/$temp[3],'',false)?></td>
                                    <td><?=setPrice($temp[2],'',false)?></td>
                                    <td><a href="<?=site_url('purchase/view/'.$temp[0])?>" target="_blank"><?=$temp[1]?></a></td>
                                    <td>&nbsp;</td>
                                <?php } else { ?>
                                    <td colspan="5" style="border-left:2px solid black;">-</td>
                                <?php }?>
                                <?php if ($i < $maxPI) { $temp = explode(',', $arrPI[$i]); ?>
                                    <td style="border-left:2px solid black;"><?=setPrice($temp[3],'',false).' '.$e['satuan_bayar']?></td>
                                    <td><?=setPrice($temp[2]/$temp[3],'',false)?></td>
                                    <td><?=setPrice($temp[2],'',false)?></td>
                                    <td><a href="<?=site_url('purchase_invoice/view/'.$temp[0])?>" target="_blank"><?=$temp[1]?></a></td>
                                    <td>&nbsp;</td>
                                <?php } else { ?>
                                    <td colspan="5" style="border-left:2px solid black;">-</td>
                                <?php }?>
                            </tr>
                            <?php }}} else {?>
                            <tr class="info"><td class="noData" colspan="17"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                            <?php } ?>
                        </tbody>
                        </table></div>
                        
                        </div><!--/ Table Selected Items -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabNonMaterial">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-dropbox"></i> Non Material
                            <div class="btn-print-group pull-right">
                                <button type="button" name="subPrint" value="Print" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-print"></i> Print</button>
                                <button type="button" name="subPrint" value="Excel" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-file-excel-o"></i> Export To CSV</button>
                            </div>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive" id="section-to-print">
                              <table class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="text-center">RAB</th>
                                        <th colspan="2" class="text-center">PB</th>
                                        <th colspan="4" class="text-center">OP</th>
                                        <th colspan="4" class="text-center">PI</th>
                                        <th colspan="2" class="text-center">Pengeluaran Lain</th>
                                    </tr>
                                    <tr>
                                        <th style="width:100px;">Kategori</th>
                                        <th>Total</th>
                                        <th>Qty</th>                        
                                        <th>PB</th>                        
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Sisa (Rp.)</th>                        
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Sisa (Rp.)</th>                        
                                        <th>Total (Rp.)</th>                        
                                        <th>PGL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(!empty($arrNonMaterial)) { 
                                        foreach($arrNonMaterial as $index => $e) {
                                        $arrPB = array();
                                        $arrP = array();
                                        $arrPI = array();
                                        $arrPGL = array();
                                        if(!empty($e['strPB'])) $arrPB = explode('|', $e['strPB']);
                                        if(!empty($e['strP'])) $arrP = explode('|', $e['strP']);                        
                                        if(!empty($e['strPI'])) $arrPI = explode('|', $e['strPI']);
                                        if(!empty($e['strPGL'])) $arrPGL = explode('|', $e['strPGL']);                        
                                        $maxPB = count($arrPB);
                                        $maxP = count($arrP);
                                        $maxPI = count($arrPI);
                                        $maxPGL = count($arrPGL);
                                        $max = max($maxPB,$maxP,$maxPI,$maxPGL); 
                                    ?>
                                    <tr style="border-top:2px solid black;">
                                        <td rowspan="<?=($max+1)?>" style="vertical-align: middle;">
                                            <b><?=$e['kategori']?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['amount'],'',FALSE)?></b>
                                        </td>
                                        <td style="border-left:2px solid black;">
                                            <b><?=setPrice($e['qty_pb'],'',FALSE)?> <?=$e['satuan_pb']?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['sisa_pb'],'',FALSE)?></b>
                                        </td>                        
                                        <td style="border-left:2px solid black;">
                                            <b><?=setPrice($e['qty_p'],'',FALSE)?> <?=$e['satuan_bayar']?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['price_p'],'',FALSE)?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['sum_p'],'',FALSE)?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['sisa_p'],'',FALSE)?></b>
                                        </td>
                                        <td style="border-left:2px solid black;">
                                            <b><?=setPrice($e['qty_pi'],'',FALSE)?> <?=$e['satuan_bayar']?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['price_pi'],'',FALSE)?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['sum_pi'],'',FALSE)?></b>
                                        </td>
                                        <td>
                                            <b><?=setPrice($e['sisa_pi'],'',FALSE)?></b>
                                        </td>
                                        <td style="border-left:2px solid black;">
                                            <b><?=setPrice($e['sum_pgl'],'',FALSE)?></b>
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <?php for($i=0; $i<$max; $i++) { ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <?php if ($i < $maxPB) { $tempPOID=0; $rowspan=1; $temp = explode(',', $arrPB[$i]); if($temp[0] == $tempPOID) $rowspan++; ?>
                                            <td style="border-left:2px solid black;" rowspan="<?=$rowspan?>"><?=$temp[2]." ".$temp[4]?></td>
                                            <td rowspan="<?=$rowspan?>"><a href="<?=site_url('purchase_order/view/'.$temp[0])?>" target="_blank" data-toggle="tooltip" title="<?=$temp[3]?>"><?=$temp[1]?></a></td>               
                                        <?php } else { ?>
                                            <td rowspan="<?=$rowspan?>" colspan="2" style="border-left:2px solid black;">-</td>
                                        <?php }?>
                                        <?php if ($i < $maxP) { $tempPrchID=0; $rowspan=1; $temp = explode(',', $arrP[$i]); if($temp[0] == $tempPrchID) $rowspan++;?>
                                            <td style="border-left:2px solid black;" rowspan="<?=$rowspan?>"><?=$temp[4]." ".$temp[6]?></td>
                                            <td rowspan="<?=$rowspan?>"><?=setPrice($temp[2],'',false)?></td>
                                            <td rowspan="<?=$rowspan?>"><?=setPrice($temp[3],'',false)?></td>
                                            <td rowspan="<?=$rowspan?>"><a href="<?=site_url('purchase/view/'.$temp[0])?>" target="_blank" data-toggle="tooltip" title="<?=$temp[5]?>"><?=$temp[1]?></a></td>                            
                                        <?php } else { ?>
                                            <td rowspan="<?=$rowspan?>" colspan="4" style="border-left:2px solid black;">-</td>
                                        <?php }?>
                                        <?php if ($i < $maxPI) { $tempPinvID=0; $rowspan=1; $temp = explode(',', $arrPI[$i]); if($temp[0] == $tempPinvID) $rowspan++;?>
                                            <td style="border-left:2px solid black;" rowspan="<?=$rowspan?>"><?=$temp[3]." ".$temp[5]?></td>
                                            <td rowspan="<?=$rowspan?>"><?=setPrice($temp[2]/$temp[3],'',false)?></td>
                                            <td rowspan="<?=$rowspan?>"><?=setPrice($temp[2],'',false)?></td>
                                            <td rowspan="<?=$rowspan?>"><a href="<?=site_url('purchase_invoice/view/'.$temp[0])?>" target="_blank" data-toggle="tooltip" title="<?=$temp[4]?>"><?=$temp[1]?></a></td>
                                        <?php } else { ?>
                                            <td rowspan="<?=$rowspan?>" colspan="4" style="border-left:2px solid black;">-</td>
                                        <?php }?>
                                        <?php if ($i < $maxPGL) { $tempPGLID=0; $rowspan=1; $temp = explode(',', $arrPGL[$i]); if($temp[0] == $tempPGLID) $rowspan++;?>
                                            <td style="border-left:2px solid black;" rowspan="<?=$rowspan?>"><?=setPrice($temp[2],'',false)?></td>
                                            <td rowspan="<?=$rowspan?>"><a href="<?=site_url('debt/pengeluaran_lain/view/'.$temp[0])?>" target="_blank"><?=$temp[1]?></a></td>                            
                                        <?php } else { ?>
                                            <td rowspan="<?=$rowspan?>" colspan="2" style="border-left:2px solid black;">-</td>
                                        <?php }?>
                                    </tr>
                                    <?php }}} else {?>
                                    <tr class="info"><td class="noData" colspan="17"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                                    <?php } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    

    
</div>