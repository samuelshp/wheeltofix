<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery-table2excel/jquery.table2excel.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('[data-toggle="tooltip"]').tooltip();
$('.currency').autoNumeric('init', autoNumericOptionsRupiah);

$('#selProyek').change(function(){
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + $('#selProyek').val(),
        cache : false,
        success : function(result){
            $("#makeDashboardCC").prop('disabled', true);
            $('#selBahan').html('<option disabled value="0" selected>Select your option</option>');
            $("#selBahan").prop('disabled', true);
            $("#selBahan").trigger("chosen:updated");
            $("#selPekerjaan").prop('disabled', false);
            $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result['data'], function (index,arr) {
                $('#selPekerjaan').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
            });
            $("#selPekerjaan").trigger("chosen:updated");
        }
    });
});

$('#selPekerjaan').change(function(){
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/bahan_subkontrak_material/list?h="+userHash+"&filter_subkontrak_id=')?>" + $('#selPekerjaan').val(),
        cache : false,
        success : function(result){
            console.log(result);
            if(result['success']) {
                $('#selBahan').html('<option value="0" selected>All Bahan</option>');
                $.each (result['data'], function (index,arr) {
                    $('#selBahan').append('<option value="' + arr['material'] + '">' + arr['prod_title'] + '</option>');
                });
                $("#selBahan").prop('disabled', false);
                $("#selBahan").trigger("chosen:updated");
                $("#makeDashboardCC").prop('disabled', false);
            } else {
                $('#selBahan').html('<option disabled value="0" selected>No Data</option>');
                $("#selBahan").prop('disabled', true);
                $("#selBahan").trigger("chosen:updated");
                $("#makeDashboardCC").prop('disabled', true);
            }
        }
    });
});

$('.chosen').chosen({search_contains: true});
});

$("button[name='subPrint'][value='Print']").click(function() {
    window.print();
});

$("button[name='subPrint'][value='Excel']").click(function() {
    $("#section-to-print table").table2excel({
        exclude: ".no-excel",
        name: "<?=$strPageTitle?>",
        filename: "<?=$strPageMode?>-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
});

</script>