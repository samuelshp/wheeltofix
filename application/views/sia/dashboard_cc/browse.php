<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> Dashboard Cost Control', 'link' => site_url('dashboard_cc/browse', NULL, FALSE)),
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">

<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />
<form name="frmGenerateDashboardCC" id="frmGenerateDashboardCC" method="post" action="<?=site_url('dashboard_cc/', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Nama Proyek
                    </h3>
                </div>
                <div class="panel-body">
                    <select type="text" id="selProyek" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>" name="intKontrakID">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrKontrak as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                    <!-- #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> -->
                        Nama Pekerjaan
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control chosen" id="selPekerjaan" name="intSubkontrakID">
                            <option value="pilih_pekerjaan">Select your option</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                    <!-- #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> -->
                        Nama Bahan
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control chosen" id="selBahan" name="intProductID">
                            <option value="pilih_bahan">Select your option</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <button type="submit" name="makeDashboardCC" id="makeDashboardCC" value="Generate" class="btn btn-primary">
                    Generate
                </button>                    
            </div>
        </div>
    </div>
</form>
</div>