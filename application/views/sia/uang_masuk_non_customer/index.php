<?php
// $strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>


<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('uang_masuk_non_customer', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
        <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search your keyword here.." />
        <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;">
        <a href="<?=site_url('uang_masuk_non_customer/action/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
    <table class="table table-bordered table-condensed table-hover main-list-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Tanggal</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
    <?php
    // Display data in the table
    if(!empty($umncList)):
        foreach($umncList as $e): ?>
            <tr>
                <td><?= $e->kode ?></td>
                <td><?= formatDate2($e->cdate) ?></td>
                <td><?= setPrice($e->amount) ?></td>
                <td class="action">
                    <a href="<?= site_url('uang_masuk_non_customer/action/edit/'.$e->id) ?>">
                        <button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></button>
                    </a>
                    <button type="button" data-id="<?= $e->id ?>" class="btn btn-sm btn-danger delete-object-button"><i class="fa fa-trash"></i></button>
                </td>

            </tr>
    <?php
        endforeach;
    else: ?>
            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
        </tbody>
    </table>
    </div>
    <?=$strPage?>
</div>
