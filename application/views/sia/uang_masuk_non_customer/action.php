<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> Kontrak', 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <a href="<?= site_url('uang_masuk_customer/action/add') ?>">
    </a>
    <p class="spacer">&nbsp;</p>

    <form method="POST" action="<?= $formAction  ?>">
        <div class="row">
            <div class="col-sm-12 col-md-6">

                <div class="form-group">
                    <label>Kode</label>
                    <input type="text" id="kode-input" name="kode" class="form-control input-sm" placeholder="Kode Uang Masuk Customer" value="<?= $object? $object->kode : '' ?>">
                </div>

                <div class="form-group">
                    <label>COA</label>
                    <select class="form-control input-sm chosen" name="coa_id" id="coa-input">
                    <?php foreach($availableAccount as $account): ?>
                        <option value="<?= $account->id ?>"><?= $account->acco_name ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Amount</label>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="amount-input" type="number" min="0" class="form-control" name="amount"  value="<?= $object? $object->amount : '' ?>">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Type</label>
                    <select name="type" id="type-input" class="form-control">
                        <option value="1">Kas</option>
                        <option value="2">Bank</option>
                        <option value="3">Giro</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>No Giro</label>
                    <input id="no-giro-input" type="text" class="form-control" name="no_giro" placeholder="Nomor Giro"  value="<?= $object && $object->no_giro? $object->no_giro : '' ?>">
                </div>

                <div class="form-group">
                    <label>Tanggal Jatuh Tempo</label>
                    <input id="tanggal-jt-input" type="date" class="form-control input-sm" name="tanggal_jt" placeholder="Nilai Kontrak"  value="<?= $object && $object->tanggal_jt? $object->tanggal_jt : '' ?>">
                </div>

                <div class="form-group">
                <?php if(isset($object)): ?>
                    <input type="hidden" name="id" value="<?= $object->id ?>">
                <?php endif; ?>
                </div>

            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success pull-right" style="margin-left:5px;"><i class="fa fa-floppy-o"></i> Save</button>
                    <a href="<?= site_url('uang_masuk_customer') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>