<div class="form-group"><?php 
$CI =& get_instance();

$bolLocked = FALSE;
if(strpos($strViewFile, 'invoice/view') !== FALSE) {
	$this->load->model('Mtinydbvo', 'configPrintSuratJalan');
	$CI->configPrintSuratJalan->initialize('jwdefaultconfig');
	$strPrintInvoiceWithSuratJalan = $CI->configPrintSuratJalan->getSingleData('JW_PRINT_INVOICE_WITH_DELIVERY');

    if(!empty($arrInvoiceData['invo_locked']) && $arrInvoiceData['invo_locked'] == 2) $bolLocked = TRUE;
}

if($bolBtnEdit && !$bolLocked): ?>  
	<button type="submit" name="subSave" value="Edit" title="Edit" data-toggle="tooltip" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?></button><?php 
endif; 
if($bolBtnDelete && !$bolLocked): ?>  
	<button type="submit" name="subDelete" value="Delete" title="Delete" data-toggle="tooltip" onClick="return confirm('<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-confirmdelete')?>');" class="btn btn-danger pull-right"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></button><?php 
endif; 
if($bolBtnPrint): ?>  
	<button type="submit" name="subPrint" value="Print" title="Print<?=strpos($strViewFile, 'invoice/view') !== FALSE ? ' [F9]' : ''?>" data-toggle="tooltip" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button><?php 
endif;
if(strpos($strViewFile, 'invoice/view') !== FALSE && $bolBtnPrint && $strPrintInvoiceWithSuratJalan == 'yes'): ?>  
    <button type="submit" name="subPrint" value="Surat Jalan" title="Print + Surat Jalan" data-toggle="tooltip" class="btn btn-success"><i class="fa fa-truck"></i></button><?php 
endif;
if(strpos($strViewFile, 'invoice/view') !== FALSE && $bolBtnPrint && !empty($arrInvoiceData['cust_nppkp'])): ?>
    <button id="printTaxY<?=$intInvoiceID?>" type="submit" name="subPrintTax" value="Print Tax" title="Print + Pajak" data-toggle="tooltip" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button><?php
endif;
if($bolBtnApprove && !$bolLocked): ?>  
	<button type="submit" name="subSave" value="Edit" title="Setuju" data-toggle="tooltip" onClick="$('#selStatus').val(2);" class="btn btn-info"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-approve')?></button><?php 
endif; 
if($bolBtnDisapprove && !$bolLocked): ?>  
	<button type="submit" name="subSave" value="Edit" title="Tidak Setuju" data-toggle="tooltip" onClick="$('#selStatus').val(1);" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-disapprove')?></button><?php 
endif; 
if($bolBtnRollback && !$bolLocked): ?>  
<button type="submit" name="subSave" value="Edit" title="Batalkan" data-toggle="tooltip" onClick="$('#selStatus').val(0);" class="btn btn-default"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-rollback')?></button><?php 
endif; ?>  
	<input type="hidden" name="selEditable" value="0" />
	<input type="hidden" name="selStatus" id="selStatus" value="<?=$rawStatus?>" />
</div>