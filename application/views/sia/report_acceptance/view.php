<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase'), 'link' => site_url('purchase/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<script>console.log('a');</script>

<?php $suppid = $arrPurchaseData['supp_id'];
    $suppinternal = $arrPurchaseData['supp_internal'];?>
<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('purchase/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data</h3></div>
            <div class="panel-body">                
                <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-ponumber')?> : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['prch_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Tanggal dibuat : </div>
                <div class="col-xs-8"><?=formatDate2($arrPurchaseData['prch_date'],'d F Y')?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Supplier : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['supp_name']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Contact Person : </div>
                <div class="col-xs-8"><?=(!empty($arrPurchaseData['supp_cp_name']))? $arrPurchaseData['supp_cp_name'] : "-"?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">No Kontrak Pembelian : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['kopb_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Keterangan : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['prch_description']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Dibuat oleh : </div>
                <div class="col-xs-8"><?=$arrLogin['adlg_name']?> (<?=formatDate2($arrPurchaseData['cdate'],'d F Y H:i:s')?>)</div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Diedit oleh : </div>
                <div class="col-xs-8"><?=$arrEditedBy['adlg_name']?> (<?=formatDate2($arrPurchaseData['mdate'],'d F Y H:i:s')?>)</div>
                <p class="spacer">&nbsp;</p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_subtotal'])?></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_discount'])?></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_subtotal']-$arrPurchaseData['prch_discount'])?></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_tax']*($arrPurchaseData['prch_subtotal']-$arrPurchaseData['prch_discount'])/100)?></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <p class="spacer">&nbsp;</p>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_grandtotal'])?></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <p class="spacer">&nbsp;</p>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountpph')?></div>
                        <div class="col-sm-6 tdDesc"><?=setPrice($arrPurchaseData['prch_pph'])?></div>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">
    
<?php if($bolBtnEdit): ?>  
    <div class="form-group"><div class="input-group addNew">
        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
    </div></div><?php
endif; ?>  

    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
    <thead>
        <tr>
            <!-- <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>   -->
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-priceunitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
        </tr>
    </thead>
    <tbody><?php
    
$i = 0;
$intTotalItem = 0;
$iditemawal='';
if(!empty($arrPurchaseItem)):
    foreach($arrPurchaseItem as $e):?>
    <tr style="text-align:center;">
        <td><?=$e['prod_title']?></td>
        <td><?=$e['prci_quantity_pb']?> <?=$e['sat_pb']?></td>
        <td><?=$e['prci_quantity1']?> <?=$e['sat_bayar']?></td>
        <td><?=setPrice($e['prci_price'])?></td>
        <td><?=setPrice($e['prci_quantity1']*$e['prci_price'])?></td>
    </tr><?php

        $i++;
    endforeach;
    echo "<input type='hidden' id='jumlah_row' name='jumlah_row' value='".$jumlah_item."'/> ";
    $iditemawal = $iditemawal.'0';
else: ?>  
        <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
endif; ?>  
    </tbody>
    </table></div>
    </div><!--/ Table Selected Items -->
</div>
<?php
    $rawStatus = $arrPurchaseData['pror_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>
<a href="<?= site_url('purchase/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a></div>
</div>
</form>