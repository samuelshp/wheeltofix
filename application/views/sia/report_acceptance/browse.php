<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-browse'), 'link' => site_url('report_acceptance/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmGenerateReportAcceptance" id="frmGenerateReportAcceptance" method="post" action="<?=site_url('report_acceptance', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-filter')?> </h3>
                </div>
                <div class="panel-body">
                    <h4 class="PB"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?> <a href="<?=site_url('kontrak', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" class="form-control chosen" required name="intKontrakID" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrKontrak as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="panel-body">
                    <h4 class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" name="intSupplierID" id="selSupp" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrSupplier as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                        <?php } ?>
                    </select>                    
                    <div id="contactPersonInfo" style="display:none"></div>                
                </div>       
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-datestart')?></h4>
                    <input type="text" id="txtDateStart" name="txtDateStart" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-dateend')?></h4>
                    <input type="text" id="txtDateEnd" name="txtDateEnd" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>

    </div>

    <div class="form-group"><button type="submit" name="smtGenerateReportAcceptance" value="Generate Report Acceptance" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-generate')?></button></div>

</form></div>




