<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-browse'), 'link' => site_url('report_acceptance/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> Result', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('purchase/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-th"></i> Filter</h3></div>
            <div class="panel-body">
                <div class="col-xs-4">Proyek : </div>
                <div class="col-xs-8"><?=$arrDataFilter['kontrak']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Supplier : </div>
                <div class="col-xs-8"><?=$arrDataFilter['supplier']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Date : </div>
                <div class="col-xs-8"><?=$arrDataFilter['dateStart']?> - <?=$arrDataFilter['dateEnd']?></div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-th"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-report')?>
    <div class="btn-print-group pull-right">
                    <button type="button" name="subPrint" value="Print" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-print"></i> Print</button>
                    <button type="button" name="subPrint" value="Excel" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print Document"><i class="fa fa-file-excel-o"></i> Export To CSV</button>
                </div>
        </h3></div>
    <div class="panel-body">
    
    <div class="table-responsive" id="section-to-print">
        <div class="hidden">
            <h3><b><?=$this->config->item('jw_website_name')?></b></h3>
            <h3><?=$strPageMode?></h3>
            <p><?=$dataPrint?></p>
            <small>Created at : <?=date('Y-m-d H:i:s')?></small>
        </div>
        <table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
    <thead>
        <tr>
            <!-- <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>   -->
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-accecode')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-supplier')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-qtypb')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-qtyterima')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-qtybayar')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-price')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-total')?></th>
        </tr>
    </thead>
    <tbody><?php
    
if(!empty($arrAcceptance)):
    $total = 0;
    foreach($arrAcceptance as $key => $e):
        foreach($arrAcceptanceItem[$key] as $key2 => $v):?>
    <tr style="text-align:center;">
        <?php if($key2 == 0) :?><td rowspan="<?=count($arrAcceptanceItem[$key])?>"><?=$e['acce_code']?></td>
<td rowspan="<?=count($arrAcceptanceItem[$key])?>"><?=$e['supp_name']?></td><?php endif;?>
        <td><?=$v['prod_title']?></td>
        <td><?=$v['acit_quantity_pb']?> <?=$v['sat_pb']?></td>
        <td><?=$v['acit_quantity1']?> <?=$v['sat_terima']?></td>
        <td><?=$v['acit_quantity_bayar']?> <?=$v['sat_bayar']?></td>
        <td><?=setPrice($v['prci_price'],'BASE',false)?></td>
        <td><?=setPrice($v['acit_quantity_bayar']*$v['prci_price'],'BASE',false)?></td>
    </tr><?php
    $total += $v['acit_quantity_bayar']*$v['prci_price'];
    endforeach;
    endforeach;
    echo '<tr><td colspan="7" style="text-align:right;">TOTAL</td><td>'.setPrice($total,'BASE',false).'</td></tr>';
else: ?>  
        <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
endif; ?>  
    </tbody>
    </table></div>
    </div><!--/ Table Selected Items -->
</div>
<a href="<?= site_url('report_acceptance/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a></div>
</div>
</form>
<script>
    $(document).ready(function(){
        $("button[name='subPrint'][value='Print']").click(function() {
            window.print();
        });
        $("button[name='subPrint'][value='Excel']").click(function() {
            $("#section-to-print table").table2excel({
                exclude: ".no-excel",
                name: "<?=$strPageTitle?>",
                filename: "<?=$strPageMode?>-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
            });
        });
    });
</script>