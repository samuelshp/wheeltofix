<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12"><?php
$strSearchAction = site_url('downpayment/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchdp.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('downpayment', NULL, FALSE)?>" class="btn btn-primary" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-date')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-kontrak')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-supplier')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-contractno')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-amount')?></th>
            <th class="action">Action</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrDownpayment)):
    foreach($arrDownpayment as $e): 
        ?>
        <tr>
            <td><?=$e['dpay_code']?></td>
            <td><?=formatDate2($e['dpay_date'],'d F Y')?></td>
            <td><?=$e['kont_name']?></td>
            <td><?=$e['supp_name']?></td>
            <td><?=$e['kopb_code']?></td>
            <td><?=setPrice($e['dpay_amount_final'])?></td>
            <td class="action">
                <?php if($e['bolAllowView']): ?><a href="<?=site_url('downpayment/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>