<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-browse'), 'link' => site_url('downpayment/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
$rawStatus = $arrDownpayment['dpay_status'];

$boolEdit = true;

if($editable>0 || $rawStatus == 4 || $availabilityDph >0){
    $boolEdit = false;
}

?>

<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('downpayment/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data Downpayment</h3></div>
            <div class="panel-body">
                <div class="col-xs-4">No. DP : </div>
                <div class="col-xs-8"><?=$arrDownpayment['dpay_code']?></div>
                <input type="hidden" name="codeDp" id="codeDp" value="<?=$arrDownpayment['dpay_code']?>">
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Tanggal dibuat : </div>
                <div class="col-xs-8"><?=formatDate2($arrDownpayment['dpay_date'],'d F Y')?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Owner : </div>
                <div class="col-xs-8"><?=$arrDownpayment['cust_name']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Nama proyek : </div>
                <div class="col-xs-8"><?=$arrDownpayment['kont_name']?><!-- textbox untuk notif --><input name="kontrak_name" type="hidden" value="<?=$arrPurchaseData['kont_name']?>"/></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
            <div class="panel-body">
                <div class="form-group">
                    <textarea name="txtProgress" maxlength="64" class="form-control" readonly><?=$arrDownpayment['dpay_description']?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-money"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-ums')?></h3></div>
    <div class="panel-body">

    <?php if(!$boolEdit){}else{?>

    <div class="row">
        <div class="col-sm-1"><h6><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-ppn')?></h6></div>
        <div class="col-sm-2 tdDesc"><div class="input-group"><input placeholder="0" type="text" name="txtPpn" id="txtPpn" class="form-control required currency qty" maxlength="3" value="<?=$arrDownpayment['dpay_ppn']?>" /><label class="input-group-addon">%</label></div></div>
    </div>
    <div class="row">
        <div class="col-sm-1"><h6><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-pph')?></h6></div>
        <div class="col-sm-2 tdDesc"><div class="input-group"><input placeholder="0" type="text" name="txtPph" id="txtPph" class="form-control required currency qty" maxlength="3" value="<?=$arrDownpayment['dpay_pph']?>" /><label class="input-group-addon">%</label></div></div>
    </div>
    
    <?php }if($bolBtnEdit): ?>  
    <div class="form-group">
        <div class="input-group addNew">
            <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
        </div>
    </div>
<?php endif; ?>  

    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover" id="invoiceItemList">

        <p class="spacer">&nbsp;</p>
        <p class="spacer">&nbsp;</p>
        <p class="spacer">&nbsp;</p>
        
            <thead>
                <tr>
                    <th>Supplier</th>
                    <th>Amount</th>
                    <th>PPn</th>
                    <th>PPh</th>
                    <th>Total Amount</th>
                    <th>Terbayar</th>
                    <th>Terpakai</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $i = 0;
                $intTotalItem = 0;
                $iditemawal='';
                if(!empty($arrDownpayment)):
            ?>
                <tr>
                    <td><h6><?=$arrDownpayment['supp_name']?></h6></td>
                    <?php if(!$boolEdit){
                        echo '<td><h6>'.setPrice($arrDownpayment['dpay_amount']).'</h6></td>';
                    } else{ ?>
                        <td><input type="text" class="currency" name="txtAmount" id="txtAmount" value="<?=$arrDownpayment['dpay_amount']?>" /></td>
                    <?php } ?>
                    <td><h6><?=setPrice($arrDownpayment['dpay_amount_ppn'])?></h6></td>
                    <td><h6><?=setPrice($arrDownpayment['dpay_amount_pph'])?></h6></td>
                    <td><h6><?=setPrice($arrDownpayment['dpay_amount_final'])?></h6></td>
                    <td><h6><?=setPrice($arrDownpayment['dpay_terbayar'])?></h6></td>
                    <!-- <td><h6><?=setPrice($arrDownpayment['dpay_amount'])?></h6></td> -->
                    <td><h6><?=setPrice($arrDownpayment2['total_terpakai'])?></h6></td>
                </tr>
            <?php
                else: 
                ?>  
                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
                endif; 
                ?>  
            </tbody>
        </table>
        </div>
    </div><!--/ Table Selected Items -->
</div>


    <div class="form-group action">
        <!-- <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp; -->
        <a href="<?=site_url(array('downpayment/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
        <?php if($boolEdit) {?>
        <button type="submit" name="subSave" value="Edit" title="Edit" data-toggle="tooltip" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?></button>
        <button type="submit" name="subDelete" value="Edit" title="Edit" data-toggle="tooltip" class="btn btn-danger"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></button>
        <?php }?>
    </div>
    <?php
        
        include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
    ?> 
</div>

<input type="hidden" name="idDp" id="idDp" value="<?=$arrDownpayment['id']?>"/>
<input type="hidden" name="pph_awal" id="pph_awal" value="<?=$arrDownpayment['dpay_pph']?>"/>
<input type="hidden" name="ppn_awal" id="ppn_awal" value="<?=$arrDownpayment['dpay_ppn']?>"/>
<input type="hidden" name="amount_awal" id="amount_awal" value="<?=$arrDownpayment['dpay_amount']?>"/>


</form></div>