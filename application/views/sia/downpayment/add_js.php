<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script>
$('.currency').autoNumeric('init', autoNumericOptionsRupiah);

    $('#selectSupp').change(function(){
        $.ajax({ 
            type : "GET",
            dataType:'json',
            url: "<?=site_url('api/kontrak_pembelian/list?h="+userHash+"&sort_kopb_code&filter_kopb_supplier_id=')?>" + $('#selectSupp').val(),
            cache : false,
            success : function(result, textStatus, xhr){
                $('#selectKP select').html('');
                $('#selectKP select').html('<option value="0">Tanpa Kontrak Pembelian</option>');
                if (result.success === true) {
                    $.each (result.data, function (index,arr) {
                        $('#selectKP select').append('<option value="' + arr['id'] + '">' + arr['kopb_code'] + '</option>');
                    });
                    $("#selectKP select").trigger("chosen:updated");
                }
            }
        });
    })

    $('#txtPph').keyup(function(){
        var nilai_pph = $('#txtPph').autoNumeric('get');
        var nilai_amount = $('#txtAmount').autoNumeric('get');
        var nilai_amount_pph = ((nilai_pph*nilai_amount)/100);
        $('#txtAmountPph').autoNumeric('set', nilai_amount_pph);
        console.log("a");
        total();
    });
    $('#txtPpn').keyup(function(){
        var nilai_ppn = $('#txtPpn').autoNumeric('get');
        var nilai_amount = $('#txtAmount').autoNumeric('get');
        var nilai_amount_ppn = ((nilai_ppn*nilai_amount)/100);
        $('#txtAmountPpn').autoNumeric('set', nilai_amount_ppn);
        console.log("a");
        total();
    });
    $('#txtAmount').keyup(function(){
        total();
    });

    function total(){
        var nilai_pph = $('#txtPph').autoNumeric('get');
        var nilai_ppn = $('#txtPpn').autoNumeric('get');
        if(nilai_pph<0||nilai_pph==''){
            nilai_pph=0;
        }
        if(nilai_ppn<0||nilai_ppn==''){
            nilai_ppn=0;
        }
        var nilai_amount = $('#txtAmount').autoNumeric('get');
        var total = (parseFloat(nilai_amount)+((nilai_ppn*nilai_amount)/100))-((nilai_pph*nilai_amount)/100);
        $('#txtAmountFinal').autoNumeric('set', total);
    }

    $('.chosen').chosen({search_contains: true});
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
</script>