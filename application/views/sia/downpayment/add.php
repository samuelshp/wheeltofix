<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-browse'), 'link' => site_url('downpayment/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmAddDownpayment" id="frmAddDownpayment" method="post" action="<?=site_url('downpayment', NULL, FALSE)?>" class="frmShop">
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-date')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=date('Y/m/d')?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-selectkontrak')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intKontrakID" id="selectKontrak" required>
                    <option value="0" selected>Select your option</option>
                    <?php foreach($arrKontrak as $e) {?>
                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-supplier')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intSupplierID" id="selectSupp" required>
                    <option value="0" selected>Select your option</option>
                    <?php foreach($arrSupplier as $e) {?>
                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-contractno')?> *
            </div>
            <div class="col-sm-8 col-md-10" id="selectKP">
                <select class="form-control chosen" name="intKPID" required>

                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-amount')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                    <label class="input-group-addon">Rp.</label>
                    <input class="form-control currency" name="txtAmount" id="txtAmount" type="text" step="0.01" scale placeholder="0" required />
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-ppn')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                        <input class="form-control currency" name="txtPph" id="txtPpn" type="text" step="0.01" placeholder="0" required />
                        <label class="input-group-addon">%</label>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-amountppn')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                    <label class="input-group-addon">Rp.</label>
                    <input class="form-control currency" name="txtAmountPpn" id="txtAmountPpn" type="text" step="0.01" placeholder="0" readonly />
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-pph')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                        <input class="form-control currency" name="txtPph" id="txtPph" type="text" step="0.01" placeholder="0" required />
                        <label class="input-group-addon">%</label>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-amountpph')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                    <label class="input-group-addon">Rp.</label>
                    <input class="form-control currency" name="txtAmountPph" id="txtAmountPph" type="text" step="0.01" placeholder="0" readonly />
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-amountfinal')?>
            </div>
            <div class="col-sm-8 col-md-10">
                <div class="input-group">
                    <label class="input-group-addon">Rp.</label>
                    <input class="form-control currency" name="txtAmountFinal" id="txtAmountFinal" type="text" step="0.01" placeholder="0" readonly />
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-description')?>
            </div>
            <div class="col-sm-8 col-md-10">
               <textarea class="form-control " name="txtDescription"></textarea>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="form-group action">
    <button type="submit" name="smtSaveDownpayment" id="smtProcessType" value="Add" class="btn btn-primary"><span class="fa fa-plus"></span></button>&nbsp;
    <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp;
    <a href="<?=site_url(array('downpayment/browse',$intTableID))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
</div>
</form></div>