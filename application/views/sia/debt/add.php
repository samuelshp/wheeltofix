<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('debt/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmAddDebt" id="frmAddDebt" method="post" action="<?=site_url('debt/'.$strLink.'/', NULL, FALSE)?>" class="frmShop" data-type="<?=$intType?>">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-<?=($intType == 3) ? "danger":  "primary"?>">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">
                    <div class="form-group hide">
                        <div class="col-sm-8">                            
                            <label class="radio-inline">
                                <input type="radio" name="jenisForm" id="frmPembayaranHutang" value="1" <?=($intType == 1) ? 'checked':null?> >
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="jenisForm" id="frmNota" value="2" <?=($intType == 2) ? 'checked':null?> >
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="jenisForm" id="frmPengeluaranLain" value="3" <?=($intType == 3) ? 'checked':null?> />
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-nopembayaran')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control" name="txtCode" type="text" value="Auto Generate" disabled />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-tgl')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=date('Y-m-d')?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php if($intType != 2): ?>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1">Cash</option>
                                <option value="2">Bank</option>
                                <option value="3">Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php endif; ?>
                    <div id="refGiro" class="hide">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                        </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>        
                    <?php if($intType != 2):?>            
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intCOA" id="intCOA" required>                                
                                <option value="0" disabled selected>Select your option</option>                                
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php endif;?>

                    <!-- Pembayaran Hutang (PI) -->
                    <?php if($intType == 1):?>
                    <div id="fieldFormPembayaranHutang">                       
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Pembayaran Hutang (PI) -->                         

                    <!-- Nota Debet -->
                    <?php if($intType == 2):?>
                    <div id="fieldFormNota">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-project')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="listProyek[]" id="listProyek" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                    <?php endforeach;?>                                       
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCOA" id="intCOA" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name']?></option>
                                    <?php endforeach;?>                                       
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?> (DPP)*
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="intAmount[]" type="text" required style="z-index: 0" />
                               </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Ppn *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <div class="input-group">
                                   <input class="form-control currency" name="intPPN[]" type="text" required style="z-index: 0" />
                                   <div class="input-group-addon">%</div>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Total
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="intTotal[]" type="text" readonly style="z-index: 0" />
                               </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Pph *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <div class="input-group">
                                   <input class="form-control currency" name="intPPH[]" type="text" required style="z-index: 0" />
                                   <div class="input-group-addon">%</div>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Keterangan
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <textarea class="form-control" name="txtDescription"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>                   
                    </div>
                    <?php endif; ?>
                    <!-- End of Nota Debet --> 

                    <!-- Pengeluaran Lain-Lain -->
                    <?php if($intType == 3):?>
                    
                    <?php endif; ?>
                    <!-- End of Pengeluaran Lain-Lain --> 
                </div>                        
                </div>
            </div>                
        </div>

        <!-- Detail Items -->
        <?php if($intType == 1):?>
        <div class="panel panel-primary" id="detailPembayaranHutang">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailPembayaranHutang">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-nodph')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?> Rencana Bayar</th>
                        <th>Bayar</th>
                    </tr>
                    </thead>
                    <tbody id="tbDetailDPH">
                        <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 2):?>
        <div class="panel panel-primary hide" id="detailNota">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" >
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailNotaDebet">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nopurchase')?></th>
                        <th>Product</th>
                        <th>Proyek</th>                        
                    </tr>
                    </thead>
                    <tbody id="tbDetailNota" >
                        <tr class="info"><td class="noData" colspan="13"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 3):?>
        <div class="panel panel-danger" id="detailPengeluaranLain">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div><table class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?></th>                        
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th colspan="2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                    </tr>
                    </thead>
                    <tbody id="lainlain">                        
                            <tr>
                                <td>
                                    <select class="form-control chosen" name="intCOAdetail[]" required>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrCOA as $e):?>
                                            <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                        <?php endforeach;?>          
                                    </select>
                                </td>                                
                                <td>
                                    <select class="form-control chosen" name="listProyek[]" required>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrProject as $e):?>
                                            <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                        <?php endforeach;?>                                       
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmount[]" type="text" />
                                   </div>
                                </td>
                                <td><input type="text" name="keterangan[]" class="form-control"></td>
                                <td><button type="button" name="btnAddLainLain" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button></td>
                            </tr>                       
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtMakeDebt" id="smtMakePembayaranHutang" value="Make Pembayaran Hutang" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                    </button>                    
                </div>
            </div>
        </div>     

</form></div>