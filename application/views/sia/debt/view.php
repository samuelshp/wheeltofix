<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('debt/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmEditDebt" id="frmEditDebt" method="post" action="<?=site_url('debt/'.$strLink.'/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-<?=($intType == 3) ? "danger":  "primary"?>">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">                                        
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-nopembayaran')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control" name="txtCode" type="text" value="<?=$arrDebt['code']?>" readonly /><input name="intID" type="hidden" value="<?=$arrDebt['id']?>" readonly />                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-tgl')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=$arrDebt['date']?>" disabled/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php if($intType != 2): ?>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required disabled>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1" <?=($arrDebt['metode'] == 1 ? "selected": null )?> >Cash</option>
                                <option value="2" <?=($arrDebt['metode'] == 2 ? "selected": null )?> >Bank</option>
                                <option value="3" <?=($arrDebt['metode'] == 3 ? "selected": null )?> >Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php endif; ?>
                    <div <?=($arrDebt['metode'] == 3 ? null : 'class="hide"' )?>  id="refGiro">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text" value="<?=$arrDebt['txou_ref_giro']?>" disabled/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <input class="form-control jwDateTime" name="txtDueDate" value="<?=$arrDebt['txou_jatuhtempo']?>" type="text" disabled/>
                            </div>
                                <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php if($intType != 2):?>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">                            
                            <select class="form-control chosen" name="intCOA" id="intCOA" required disabled>
                                <option value="0" disabled selected>Select your option</option>
                                <?php foreach($arrCOA as $e):?>
                                    <option value="<?=$e['id']?>" <?=($arrDebt['acco_id'] == $e['id'] ? "selected": null )?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                <?php endforeach;?>          
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>

                    <!-- Pembayaran Hutang (PI) -->
                    <?php if($intType == 1):?>
                    <div id="fieldFormPembayaranHutang">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen disable" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrDebt['supp_id'] == $e['id'] ? "selected": null )?> ><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Pembayaran Hutang (PI) -->                         

                    <!-- Nota Debet -->
                    <?php if($intType == 2):?>
                    <div id="fieldFormNota">                        
                        <input type="hidden" name="intIDdetail[]" value="<?=$arrDetail[0]['id']?>">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intSupplier[]" id="intSupplier" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrDebt['supp_id'] == $e['id'] ? "selected": null )?>><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-project')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">                               
                                <select class="form-control chosen" name="listProyek[]" id="listProyek" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrDebt['refID'] == $e['id'] ? "selected": null )?> ><?=$e['kont_name']?></option>
                                    <?php endforeach;?>                                       
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCOA" id="intCOA" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCOA as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrDebt['acco_id'] == $e['id'] ? "selected": null )?> ><?=$e['acco_code']." - ".$e['acco_name']?></option>
                                    <?php endforeach;?>                                       
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?> (DPP) * 
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="intAmount[]" type="text" value="<?=$arrDetail[0]['txod_amount']?>" required disabled style="z-index: 0"/>
                               </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Ppn *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <div class="input-group">
                                   <input class="form-control currency" name="intPPN[]" type="text" required disabled value="<?=$arrDebt['txod_ppn'];?>"  style="z-index: 0"/>
                                   <div class="input-group-addon">%</div>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Total
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="intTotal[]" value="<?=($arrDetail[0]['txod_amount']+($arrDetail[0]['txod_amount']*$arrDebt['txod_ppn']/100))?>" type="text" readonly  style="z-index: 0" />
                               </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Pph *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <div class="input-group">
                                   <input class="form-control currency" name="intPPH[]" type="text" required disabled value="<?=$arrDebt['txod_pph'];?>" />
                                   <div class="input-group-addon">%</div>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                Keterangan
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <textarea class="form-control" name="txtDescription" disabled><?=$arrDebt['txou_description']?></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>                           
                    </div>
                    <?php endif; ?>
                    <!-- End of Nota Debet --> 

                    <!-- Pengeluaran Lain-Lain -->
                    <?php if($intType == 3):?>                   
                    <?php endif; ?>
                    <!-- End of Pengeluaran Lain-Lain --> 
                </div>                        
                </div>
            </div>                
        </div>

        <!-- Detail Items -->
        <?php if($intType == 1):?>
        <div class="panel panel-primary" id="detailPembayaranHutang">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailPembayaranHutang">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-nodph')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                    </tr>
                    </thead>
                    <tbody id="tbDetailDPH">
                        <?php if(!empty($arrDetail)): foreach($arrDetail as $e): ?>
                            <tr>
                                <td><?=$e['dphu_code'];?></td>
                                <td><?=setPrice($e['dphu_amount']);?></td>
                            </tr>
                        <?php endforeach; else:?>
                        <tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                        <?php endif; ?>
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 5):?>
        <div class="panel panel-primary" id="detailNota">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" >
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailNotaDebet">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nopurchase')?></th>
                        <th>Product</th>
                        <th>Proyek</th>
                        <!-- <th><?//=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th> -->
                    </tr>
                    </thead>
                    <tbody id="tbDetailNota" >
                        <?php if(!empty($arrDetail)): foreach($arrDetail as $e): ?>
                            <tr>
                                <td><?=$e['pinv_code'];?></td>
                                <td><?=$e['prod_title'];?></td>
                                <td><?=$e['kont_name'];?></td>
                                <!-- <td><?//=setPrice($e['pinv_grandtotal']);?></td> -->
                            </tr>
                        <?php endforeach; else:?>
                        <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                        <?php endif; ?>                       
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 3):?>
        <div class="panel panel-danger" id="detailPengeluaranLain">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div><table class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                        <th style="width: 5%"><button type="button" name="btnAddLainLain" class="btn btn-secondary btn-sm" disabled data-toggle="tooltip" title="Klik untuk menambah item"><i class="fa fa-plus"></i></button></th>
                    </tr>
                    </thead>
                    <tbody id="lainlain">      
                            <?php if(!empty($arrDetail)): foreach($arrDetail as $detail): ?>                  
                            <tr>
                                <td>
                                    <input type="hidden" name="intIDdetail[]" value="<?=$detail['id']?>">
                                    <select class="form-control chosen" name="intCOAdetail[]" required disabled>
                                        <option value="0" disabled>Select your option</option>
                                        <?php foreach($arrCOAdetail as $e):?>
                                            <option value="<?=$e['id']?>" <?=($detail['txod_acco_id'] == $e['id']) ? "selected" : null ?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                        <?php endforeach;?>          
                                    </select>
                                </td>                                
                                <td>
                                    <select class="form-control chosen" name="listProyek[]" required disabled>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrProject as $e):?>
                                            <option value="<?=$e['id']?>" <?=($detail['txod_ref_id'] == $e['id']) ? "selected" : null ?> ><?=$e['kont_name']?></option>
                                        <?php endforeach;?>                                       
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmount[]" type="text" value="<?=$detail['txod_totalbersih']?>" disabled />
                                   </div>
                                </td>
                                <td><input type="text" name="keterangan[]" class="form-control" value="<?=$detail['txod_description']?>" disabled></td>
                                <td id="btnRemoveItem"></td>
                            </tr>     
                            <?php endforeach; endif; ?>                  
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- Muncul Kalau: Pembayaran Hutang -> txou_status_trans =  -->    
                    <?php if(($intType === 2 && $arrDebt['txou_status_trans'] < 3) || ($intType === 1) || ($intType === 3)):?>
                    <button type="button" name="smtEditDebt" id="smtUpdateDebt" value="Update Debt" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button> 
                    
                    <button type="submit" id="btnUpdate" name="smtUpdateDebt" value="Update Debt" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                    
                    <button type="submit" id="btnPrint" name="subPrint" value="Print" class="btn btn-success"><i class="fa fa-print"></i></button>                    
                    
                    <button type="submit" id="btnDelete" name="smtDeleteDebt" value="Delete Debt" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                           
                    <?php endif; ?>
                </div>
            </div>
        </div>             
        <input type="hidden" name="deletedItem">
</form></div>