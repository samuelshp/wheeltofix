<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">

var listItemDeleted = [];
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);



$(document).ready(function(){
   
});

$(".disable").attr("disabled",true);

 $("#intMethod").change(function(){
    var methodType = $(this).val();    
    if (methodType == 3) { 
        $("#refGiro").removeClass('hide');
    }else{
        $("#refGiro").addClass('hide');        
    }
});

$("#fieldFormPembayaranHutang select[name=intSupplier]").change(function(){
    $.ajax({
        url: "<?=site_url('debt_ajax/getDPH/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result){   
            $(".result").remove();         
            if(result.length > 0){
                $(".info").hide();                              
                var total = 0;
                $.each(result, function(i, arr) {                    
                    $("#tbDetailDPH").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`id`]+"' />"+arr['dphu_code']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' name='intAmount[]' class='form-control input-sm currency' value="+arr['dphu_amount']+" readonly></div></td></tr>");
                    total += parseFloat(arr['dphu_amount']);
                });
                $("#tDetailPembayaranHutang").append("<tfoot><tr class='result active'><td>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        }
    });
});

$("#fieldFormNota select[name=intSupplier]").change(function(){
    $("option.result").remove();
    $.ajax({
        url: "<?=site_url('debt_ajax/getPurchaseInvoiceBySupplier/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result){
            $.each(result, function(i, arr){
                $("#fieldFormNota select[name=listPI]").append(`
                    <option value="`+arr['id']+`" class="result">`+arr['pinv_code']+`</option>
                `);
            });
            $(".chosen").trigger("chosen:updated");
        },
        error: function(err){
            alert("Oops, error.");
        }
    });
});

$("#fieldFormNota select#listPI").change(function(){    
    $.ajax({
        url: "<?=site_url('debt_ajax/getPurchaseInvoiceDetail/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result) {
            console.log(result);
            $(".result").remove();         
            if(result.length > 0){
                $(".info").hide();                              
                var total = 0;
                $.each(result, function(i, arr) {                    
                    $("#tbDetailNota").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`id`]+"' />"+arr['pinv_code']+"</td><td>"+arr['prod_title']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' name='intAmount[]' class='form-control input-sm currency' value="+arr['pinvit_subtotal']+" readonly></div></td></tr>");  
                    total += parseFloat(arr['pinvit_subtotal']);
                });
                $("#tDetailNotaDebet").append("<tfoot><tr class='result active'><td colspan='2'>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }   
        },
        error: function(err) {
            alert("Oops");
            console.log(err);
        }
    });
});

$("#smtUpdateDebt").click(function(){    
    $(this).hide();
    $("#btnUpdate").show();
    $("#btnDelete").show();
    $("button[name='btnAddLainLain']").removeAttr('disabled');
    $(".form-control").removeAttr('disabled');
    $(".disable").attr("disabled",true);
    $(".chosen").chosen('destroy');    
    $(".chosen").chosen({width: "100%",search_contains: true});
    $("td#btnRemoveItem").append('<button type="button" name="btnRemoveLainLain" class="btn btn-danger btn-sm remove-row-button"><i class="fa fa-trash"></i></button>');
    // deleteHandler();    
    $('button[name=btnAddLainLain]').click(addrowLainLain);
    $('button[name=btnRemoveLainLain]').click(removeRow);   
});

$("#fieldFormNota input[name^=intAmount]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($(this).autoNumeric('get'));
    var pph = parseFloat($("#fieldFormNota input[name^=intPPN]").autoNumeric('get'));    
    $("input[name^='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
});

$("#fieldFormNota input[name^=intPPN]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($("#fieldFormNota input[name^=intAmount]").autoNumeric('get'));
    var pph = parseFloat($(this).autoNumeric('get'));     
    $("input[name^='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
}); 

function addrowLainLain() {
    $('#lainlain').append(`
        <tr>
        <td>
            <select class="form-control chosen" name="intCOAdetail[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrCOA as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                <?php endforeach;?>          
            </select>
        </td>                                
        <td>
            <select class="form-control chosen" name="listProyek[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrProject as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                <?php endforeach;?>                                       
            </select>
        </td>
        <td>
            <div class="input-group">
                <div class="input-group-addon">Rp.</div>
                <input class="form-control input-sm currency" name="intAmount[]" type="text" required />
            </div>
        </td>
        <td><input type="text" name="keterangan[]" class="form-control"></td>
        <td><button type="button" name="btnRemoveLainLain" class="btn btn-danger btn-sm remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('.currency').autoNumeric('init', autoNumericOptionsRupiah);
    $('.chosen').chosen({width:"100%",search_contains: true});
    $('.remove-row-button').click(removeRow);
}

function calculateTotalNota(intNominal, intPPH) {
    return intNominal+(intNominal * intPPH/100);
}

function removeRow(e){        
    var currentElement = $(e.target).parents('tr');
    var id = currentElement.find('input[name^=intIDdetail]').val();
    
    listItemDeleted.push(id);
    $("input[name=deletedItem]").val(listItemDeleted);
    currentElement
        .hide(200, function(){            
            $(e.target).parents('tr').remove()
        });

}


$(".chosen").chosen({width: "100%",search_contains: true});
</script>