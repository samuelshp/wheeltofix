<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);

$("form#frmAddDebt").submit(function() {   
        var confirmBox = confirm("Apakah data yang diinputkan sudah benar? Mohon dipastikan, karena halaman ini tidak bisa diedit.");
        return confirmBox;
});

$("#intMethod").change(function(){
    var methodType = $(this).val();    
    var field;
    if (methodType == 3) {
        $("#refGiro").removeClass('hide');        
        field = "acco_detail";
    }else{        
        if (methodType == 2) {
            field = "acco_bank";
        }else{
            field = "acco_kas";
        }
        $("#refGiro").addClass('hide');
    }

    $.ajax({        
        url: "<?=site_url('api/jw_account/list?h="+userHash+"&filter_"+field+"=2')?>",
        method: "GET",
        dataType: "json",
        success: function(result) {                   
            $("#intCOA .optionResult").empty();               
            $.each(result.data, function(i, arr){                
                $("#intCOA").append(`
                    <option class="optionResult" value="`+arr['id']+`">`+arr['acco_code']+` - `+arr['acco_name']+`</option>
                `);
            });
            $(".chosen").trigger("chosen:updated");
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        }
    });

});

$('button[name=btnAddLainLain]').click(addrowLainLain);

$("#fieldFormPembayaranHutang select[name=intSupplier]").change(function(){
    $.ajax({
        url: "<?=site_url('api/detail_dph/list?h="+userHash+"&filter_supp_id="+$(this).val()+"')?>",
        method: "GET",
        dataType: "json",
        success: function(result, textStatus, xhr){            
            $(".result").remove();         
            if(result.success === true){
                $(".info").hide();                              
                var total = 0;
                $.each(result.data, function(i, arr) {                    
                    $("#tbDetailDPH").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`dph_id`]+"' />"+arr['dphu_code']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' name='intAmount[]' class='form-control input-sm currency' value="+arr['dphu_amount']+" readonly></div></td><td><input type='checkbox' name='chbxDibayar[]' class='chbxDibayar' data-id='`+arr['dph_id']+`' value='"+i+"-"+arr[`dphu_amount`]+"-"+arr['dph_id']+"'/></td></tr>");
                    total += parseFloat(arr['dphu_amount']);
                });
                $("#tDetailPembayaranHutang").append("<tfoot><tr class='result active'><td>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td><td style='max-width:80px'></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        }
    });
});

$("#fieldFormNota select[name=intSupplier]").change(function(){
    $("option.result").empty();
    $.ajax({
        url: "<?=site_url('api/purchase_invoice/list?h="+userHash+"&filter_pinv_supplier_id="+$(this).val()+"')?>",
        method: "GET",
        dataType: "json",
        success: function(result){            
            $.each(result, function(i, arr){
                $("#fieldFormNota select[name=listPI]").append(`
                    <option value="`+arr['id']+`" class="result">`+arr['pinv_code']+`</option>
                `);
            });
            $(".chosen").trigger("chosen:updated");
        },
        error: function(err){
            alert("Oops, error.");
        }
    });
});

// To Review - Not Used Anymore
$("#fieldFormNota select#listPI").change(function(){    
    $.ajax({
        url: "<?=site_url('debt_ajax/getPurchaseInvoiceDetail/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result) {
            console.log(result);            
            $(".result").empty();
            if(result.length > 0){
                $(".info").hide();                              
                var total = 0;
                $.each(result, function(i, arr) {                    
                    $("#tbDetailNota").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`id`]+"' />"+arr['pinv_code']+"</td><td>"+arr['prod_title']+"</td><td>"+arr['kont_name']+"</td></tr>");  
                    total += parseFloat(arr['pinvit_subtotal']);
                });
                //  $("#tDetailNotaDebet").append("<tfoot><tr class='result active'><td colspan='3'>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }   
        },
        error: function(err) {
            alert("Oops");
            console.log(err);
        }
    });
});

$("#fieldFormNota input[name^=intAmount]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($(this).autoNumeric('get'));
    var pph = parseFloat($("#fieldFormNota input[name^=intPPN]").autoNumeric('get'));
    $("input[name^='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
});

$("#fieldFormNota input[name^=intPPN]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($("#fieldFormNota input[name^=intAmount]").autoNumeric('get'));
    var pph = parseFloat($(this).autoNumeric('get'));
    $("input[name^='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
});

function calculateTotalNota(intNominal, intPPH) {
    return intNominal+(intNominal * intPPH/100);
}

// To Review - Not Used Anymore
$("#intDPH").change(function(){
    $(".info").hide();
    $("#tbDetailDPH").empty();
    $.ajax({
        url: "<?=site_url('pembayaran_hutang_ajax/getDetailDPH/"+$(this).val()+"');?>",
        method: 'POST',
        dataType: "json",
        success: function(result) {
            var totalAmount = 0;
            $.each(result, function(i, arr){                
                $('tbody#tbDetailDPH').append(`<tr name="rowDetail">
                <td>`+arr['cdate']+`</td>
                <td>`+arr['pinv_code']+`</td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="intAmount[]" class="form-control input-sm currency amount" value="`+arr[`pinv_grandtotal`]+`" readonly />
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="intRencanaBayar[]" class="form-control input-sm currency rencanabayar" value="0" />
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="intSisa[]" class="form-control input-sm currency sisa" value="0" readonly />
                    </div>
                </td>
                <td><input type="text" name="strKeterangan[]" class="form-control input-sm" /></td>
                <td><button type="button" class="btn btn-info btn-sm" name="btnApproval"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-approve')?></button></td>
                <td><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg"><i class='fa fa-plus'></i></button></td>
                </tr>`);
                totalAmount += parseInt(arr[`pinv_grandtotal`]);
            });            
            $('tbody#tbDetailDPH').append(`
                <tr id="rowTotal">
                <td colspan="2"></td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="intAmount[]" class="form-control input-sm currency" value="`+totalAmount+`" readonly />
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="totalRencanaBayar" class="form-control input-sm currency" value="0"/>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input type="text" name="totalSisa" class="form-control input-sm currency" value="0" readonly />
                    </div>
                </td>
                <td colspan="3"></td>
                </tr>
            `);
            $(".currency").autoNumeric('init', autoNumericOptionsRupiah);

            $("input.rencanabayar").keyup(hitungsisa);
            $("input.rencanabayar").keyup(hitungTotalRencanaBayar);
        }
    });
});

function hitungsisa(e) {    
    var amount = $(this).closest('tr').find('input.amount').autoNumeric('get');
    var rencanabayar = $(this).closest('tr').find('input.rencanabayar').autoNumeric('get');    
    $(this).closest('tr').find('input.sisa').autoNumeric('set', amount-rencanabayar);

    hitungTotalSisa();
}

function hitungTotalRencanaBayar() {
    var totalRencanaBayar = 0;
    
    $.each($('#tbDetailDPH .rencanabayar'), function(i,element){
        totalRencanaBayar += parseFloat($(element).autoNumeric('get'));        
    });    
    $('input[name=totalRencanaBayar]').autoNumeric('set', totalRencanaBayar);
}

function hitungTotalSisa() {
    var totalSisa = 0;
    
    $.each($('#tbDetailDPH .sisa'), function(i,element){
        totalSisa += parseFloat($(element).autoNumeric('get'));        
    });    
    $('input[name=totalSisa]').autoNumeric('set', totalSisa);
}

function addrowLainLain() {
    $('#lainlain').append(`
        <tr>
        <td>
            <select class="form-control chosen" name="intCOAdetail[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrCOA as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                <?php endforeach;?>          
            </select>
        </td>                                
        <td>
            <select class="form-control chosen" name="listProyek[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrProject as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                <?php endforeach;?>                                       
            </select>
        </td>
        <td>
            <div class="input-group">
                <div class="input-group-addon">Rp.</div>
                <input class="form-control input-sm currency" name="intAmount[]" type="text" />
            </div>
        </td>
        <td><input type="text" name="keterangan[]" class="form-control"></td>
        <td><button type="button" name="btnRemoveLainLain" class="btn btn-danger btn-sm remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('.currency').autoNumeric('init', autoNumericOptionsRupiah);
    $('.chosen').chosen({width:"100%",search_contains: true});
    $('.remove-row-button').click(removeRow);
}

function removeRow(e){    
    $(e.target)
        .parents('tr')
        .hide(200, function(){            
            $(e.target).parents('tr').remove()
        });
}

$(".chosen").chosen({width: "100%",search_contains: true});
</script>