<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bullseye"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustment'), 'link' => site_url('adjusment/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><form name="frmAddAdjustment" id="frmAddAdjustment" method="post" action="<?=site_url('accounting/adjustment', NULL, FALSE)?>" class="frmShop">
    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <!-- Dibuat -->
                    <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                 </div>
            </div>
            <div class="panel panel-primary">
    			<div class="panel-heading"><h3 class="panel-title">Gunakan Template</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" name="smtDeleteTemplate" value="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                            </span>
                            <select name="selTemplate" class="form-control">
                                <option><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-select')?></option><?php
foreach($arrTemplate as $e): ?>
                                <option value="<?=$e['id']?>"><?=$e['acte_title']?></option><?php
endforeach; ?>
                            </select>
                            <span class="input-group-btn">
                                <button type="submit" name="smtUseTemplate" value="Use" class="btn btn-primary"><i class="fa fa-play"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"><div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
                </div>
            </div></div>
    </div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustmentitems')?></h3></div>
        <div class="panel-body">
            <div class="row">
                <div class="table-responsive col-md-6 col-xs-12"><table class="table table-bordered table-condensed table-hover" id="selectedDebit">
                    <thead>
                    <tr>
                        <th colspan="3">Debit</th>
                    </tr>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                        <th>Nama Akun</th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    </tbody>
                </table>
                <div class="form-group"><div class="input-group col-xs-12 addNew">
                        <select  class="form-control" id="NewDebit">
                            <option value="0"> Tambah Akun Debit </option>
                            <?php
                            foreach($arrAkun as $e){
                                echo "<option value='".$e["id"]."'>".$e["acco_code"].",".$e["acco_name"]."</option>";
                            }
                            ?>
                        </select>
                    </div></div>
                </div>

                <div class="table-responsive col-md-6 col-xs-12"><table class="table table-bordered table-condensed table-hover" id="selectedKredit">
                    <thead>
                    <tr>
                        <th colspan="3">Kredit</th>
                    </tr>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                        <th>Nama Akun</th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    </tbody>
                </table>
                <div class="form-group "><div class="input-group  col-xs-12 addNew">
                        <select  class="form-control" id="NewKredit">
                            <option value="0"> Tambah Akun Kredit </option>
                            <?php
                                foreach($arrAkun as $e){
                                    echo "<option value='".$e["id"]."'>".$e["acco_code"].",".$e["acco_name"]."</option>";
                                }
                            ?>
                        </select>
                    </div></div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-10 tdTitle">Total Debit</div>
                <div class="col-sm-2 tdDesc" id="subTotalDebit">0</div>
            </div>

            <div class="row">
                <div class="col-sm-10 tdTitle">Total Kredit</div>
                <div class="col-sm-2 tdDesc" id="subTotalKredit">0</div>
            </div>

            <div class="row">
                <div class="col-sm-10 tdTitle">Selisih</div>
                <div class="col-sm-2 tdDesc" id="subTotal">0</div>
            </div>
            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        <input type="checkbox" name="cbSaveTemplate" value="1" />
                    </span>
                    <input type="text" class="form-control" name="txtTemplateName" placeholder="Ketik nama template untuk menyimpan" disabled />
                    <span class="input-group-btn">
                        <button type="submit" name="smtMakeAdjustment" value="Make Adjusment" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button>
                    </span>
                </div>
            </div>
        </div>
    </div>

<input type="hidden" id="totalDebit" name="totalDebit"/>
<input type="hidden" id="totalKredit" name="totalKredit"/>

</form></div>
