<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-adjustment');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('accounting/adjustmentbrowse', NULL, FALSE)?>" class="form-inline pull-right" style="">
    <div class="input-group" style="max-width:250px;">
	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Code" />
	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('accounting/adjustment', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th rowspan="2">
                Kode
            </th>
            <th colspan="2">
                Debit
            </th>
            <th colspan="2">
                Kredit
            </th>
            <th rowspan="2">
                Description
            </th>
            <th rowspan="2">
                Action
            </th>
        </tr>
        <tr>
            <th>Nama Akun</th>
            <th>Nominal</th>
            <th>Nama Akun</th>
            <th>Nominal</th>
        </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrBilling)):
    foreach($arrBilling as $e): ?>
    <form name="frmChangePayment[<?=$e['id']?>]" id="frmChangePayment<?=$e['id']?>" method="post" action="<?=site_url('accounting/adjustmentbrowse/'.$e['id'], NULL, FALSE)?>" class="frmShop">
        <tr>
            <td><?=$e['acad_code']?></td>
            <td>

                <?php
                if(!empty($e['debit'])){
                    foreach($e['debit'] as $er){
                        echo $er['acco_code'].",".$er['acco_name']."<br>";
                    }
                }
                ?>
            </td>
            <td>
                <?php
                if(!empty($e['debit'])){
                    foreach($e['debit'] as $er){
                        echo $er['adem_total']."<br>";
                    }
                }
                ?>
            </td>
            <td>
                <?php
                if(!empty($e['kredit'])){
                    foreach($e['kredit'] as $er){
                        echo $er['acco_code'].",".$er['acco_name']."<br>";
                    }
                }
                ?>
            </td>
            <td>
                <?php
                if(!empty($e['kredit'])){
                    foreach($e['kredit'] as $er){
                        echo $er['adem_total']."<br>";
                    }
                }
                ?>
            </td>
            <td>
                <?=$e['acad_description']?>
            </td>
            <td>
                <input type="hidden" name="selStatus" id="selStatus<?=$e['id']?>" value="<?=$e['acad_status']?>"/>
                <?php
                if($e['bolBtnDelete']): ?>
                    <button type="submit" name="subDelete" value="Delete" onclick="return confirm('<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-confirmdelete')?>');" class="btn btn-danger pull-right"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></button><?php
                endif;
                ?>
            </td>

        </tr>
    </form><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table>
    </div>
    <?=$strPage?>
</div>