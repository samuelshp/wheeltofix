<style>
.postingDate .ui-datepicker-calendar {display: none;}
.postingDate .ui-datepicker-title {color:#000;}
</style>
<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".postingDate").datepicker({ dateFormat: 'yy-mm'});

$("#searchForm").submit(function() {


    if($("#accountName").val()=='') {
        alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectaccount')?>"); return false;
    }

    return true;
});

$("#txtAccountName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('accounting_ajax/getAutoComplete', NULL, FALSE)?>/" + $("#txtAccountName").val(),
            beforeSend: function() {
                $("#loadAccount").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadAccount").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedAcc = xml;
                var display=[];
                $.map(xml.find('Account').find('item'),function(val,i){
                    var code = $(val).find('acco_code').text();
                    var name = $(val).find('acco_name').text();
                    
                    var strName=code+", "+name;
                    var intID = $(val).find('id').text();
                    display.push({label: strName, value: name,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedAcc = $.grep($arrSelectedAcc.find('Account').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        $('input[name="accountID"]').val($(selectedAcc).find('id').text());
        $("#accountCode").val($(selectedAcc).find('acco_code').text());
        $("#accountName").val($(selectedAcc).find('acco_name').text());
        $("#loadAccount").html('<i class="fa fa-check"></i>');
    }
});

$("#txtCustomerName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('invoice_ajax/getSaleOrderCustomerAutoComplete', NULL, FALSE)?>/" + $("#txtCustomerName").val(),
            beforeSend: function() {
                $("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadCustomer").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedCust = xml;
                var display=[];
                $.map(xml.find('Customer').find('item'),function(val,i){
                    var name = $(val).find('cust_name').text();
                    var address = $(val).find('cust_address').text();
                    var city = $(val).find('cust_city').text();

                    var strName=name+", "+ address +", "+city;
                    var intID = $(val).find('id').text();
                    display.push({label: strName, value: name,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedCust = $.grep($arrSelectedCust.find('Customer').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        $('input[name="customerID"]').val($(selectedCust).find('id').text());
        $("#customerCode").val($(selectedCust).find('cust_code').text());
        $("#customerName").val($(selectedCust).find('cust_name').text());
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
    }
});

$("#txtProductName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $("#txtProductName").val(),
            beforeSend: function() {
                $("#loadProduct").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadProduct").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedProduct = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var strName = $(val).find('strName').text();
                    var intID = $(val).find('id').text();
                    display.push({label: strName, value: strName,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedProduct = $.grep($arrSelectedProduct.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        $('input[name="productID"]').val($(selectedProduct).find('id').text());
        $("#productCode").val($(selectedProduct).find('prod_code').text());
        $("#productName").val($(selectedProduct).find('strName').text());
        $("#loadProduct").html('<i class="fa fa-check"></i>');
    }
});


$("#txtSalesmanName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('invoice_ajax/getInvoiceSalesmanAutoComplete', NULL, FALSE)?>/" + $("#txtSalesmanName").val(),
            beforeSend: function() {
                $("#loadSalesman").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadSalesman").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedSales = xml;
                var display=[];
                $.map(xml.find('Salesman').find('item'),function(val,i){
                    var name = $(val).find('sals_name').text();
                    var address = $(val).find('sals_address').text();
                    var city = $(val).find('sals_city').text();

                    var strName=name+", "+ address +", "+city;
                    var intID = $(val).find('id').text();
                    display.push({label: strName, value: name,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedSales = $.grep($arrSelectedSales.find('Salesman').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        $('input[name="salesmanID"]').val($(selectedSales).find('id').text());
        $("#salesmanCode").val($(selectedSales).find('sals_code').text());
        $("#salesmanName").val($(selectedSales).find('sals_name').text());
        $("#loadSalesman").html('<i class="fa fa-check"></i>');
    }
});

$("#txtSupplierName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#txtSupplierName").val(),
            beforeSend: function() {
                $("#loadSupplier").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadSupplier").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedSupplier = xml;
                var display=[];
                $.map(xml.find('Supplier').find('item'),function(val,i){
                    var name = $(val).find('supp_name').text();
                    var address = $(val).find('supp_address').text();
                    var city = $(val).find('supp_city').text();

                    var strName=name+", "+ address +", "+city;
                    var intID = $(val).find('id').text();
                    display.push({label: strName, value: name,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedSupplier = $.grep($arrSelectedSupplier.find('Supplier').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });

        $('input[name="supplierID"]').val($(selectedSupplier).find('id').text());
        $("#supplierCode").val($(selectedSupplier).find('supp_code').text());
        $("#supplierName").val($(selectedSupplier).find('supp_name').text());
        $("#loadSupplier").html('<i class="fa fa-check"></i>');
    }
});

});</script>