<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$("#txtNewItem").prop("disabled",true);
$("#selToWarehouse").val("0");
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$("#subTotalDebit").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalKredit").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotal").autoNumeric('init', autoNumericOptionsRupiah);
var templateSetting = '';
<?php if(!empty($arrTemplateSetting)): ?>
templateSetting = <?=$arrTemplateSetting?>;
<?php endif; ?>

$("#frmAddAdjustment").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
      $element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
      $element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

var buttonName = false;
$("#frmAddAdjustment").submit(function(e) {
    if(buttonName && buttonName == 'smtMakeAdjustment') {
        if(numberDebit<=0 || numberKredit<=0) {
            alert("Setidaknya harus ada 1 akun Debit dan 1 akun Kredit"); return false;
        }
        if($("#subTotalKredit").autoNumeric("get")!=$("#subTotalDebit").autoNumeric("get")){
            alert("Total Kredit dan Debit harus sama"); return false;
        }
        var form = $(this);
        $('.currency').each(function(i){
            var self = $(this);
            try{
                var v = self.autoNumeric('get');
                self.autoNumeric('destroy');
                self.val(v);
            }catch(err){
                console.log("Not an autonumeric field: " + self.attr("name"));
            }
        });

        if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-areyousure')?>") == false) return false;
    }
    return true;
});
$('button[type=submit]').click(function() {
    buttonName = $(this).attr('name');
});
var numberDebit=0;
var numberKredit=0;
var totalDebit=0;
var totalKredit=0;
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
		}
});

$("#NewDebit").change(function() {
   if($(this).val()!='0'){
       var i=totalDebit;
       if(numberDebit==0){
           $("#selectedDebit tbody").html('');
       }
       $("#selectedDebit tbody").append(
           '<tr>'+
               '<td class="cb"><input type="checkbox" name="cbDeleteDebitX'+i+'"/></td>'+
               '<td><label id="strDebitX'+i+'" name="strDebitX'+i+'"></label></td>'+
               '<td><input type="text" class="form-control currency" name="totalDebitX'+i+'" id="totalDebitX'+i+'" value="0"/></td>'+
               '<input type="hidden" name="idDebitX'+i+'" id="idDebitX'+i+'"/>'+
               '<input type="hidden" name="isDebitX'+i+'" id="isDebitX'+i+'" value="1"/>'+
               '</tr>');
       $("#strDebitX"+i).text($("#NewDebit option:selected").text());
       $("#idDebitX"+i).val($(this).val());
       $("#totalDebitX"+i).autoNumeric('init', autoNumericOptionsRupiah);
       totalDebit++;
       numberDebit++;
       $("#totalDebit").val(totalDebit);
   }
});

$("#NewKredit").change(function() {
    if($(this).val()!='0'){
        var i=totalKredit;
        if(numberKredit==0){
            $("#selectedKredit tbody").html('');
        }
        $("#selectedKredit tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteKreditX'+i+'"/></td>'+
                '<td><label id="strKreditX'+i+'" name="strKreditX'+i+'"></label></td>'+
                '<td><input type="text" class="form-control currency" name="totalKreditX'+i+'" id="totalKreditX'+i+'" value="0"/></td>'+
                '<input type="hidden" name="idKreditX'+i+'" id="idKreditX'+i+'"/>'+
                '<input type="hidden" name="isKreditX'+i+'" id="isKreditX'+i+'" value="1"/>'+
                '</tr>');
        $("#strKreditX"+i).text($("#NewKredit option:selected").text());
        $("#idKreditX"+i).val($(this).val());
        $("#totalKreditX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        totalKredit++;
        numberKredit++;
        $("#totalKredit").val(totalKredit);
    }
});

$("#selectedDebit").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberDebit--;
    if(numberDebit=='0'){
        $("#selectedDebit tbody").append(
            '<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    $("#isDebitX"+at).val(0);
    $(this).closest("tr").remove();
});

$("#selectedKredit").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberKredit--;
    if(numberKredit=='0'){
        $("#selectedKredit tbody").append(
            '<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    $("#isKreditX"+at).val(0);
    $(this).closest("tr").remove();
});

var sum=0;
$("#selectedDebit").on("focus","input[name*='totalDebit']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    sum=$("#totalDebitX"+at).autoNumeric('get');
});
$("#selectedDebit").on("change","input[name*='totalDebit']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    var now=$("#totalDebitX"+at).autoNumeric('get');
    var total=parseFloat($("#subTotalDebit").autoNumeric("get"))-parseFloat(sum)+parseFloat(now);
    $("#subTotalDebit").autoNumeric("set",total);
});

$("#selectedKredit").on("focus","input[name*='totalKredit']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    sum=$("#totalKreditX"+at).autoNumeric('get');
});
$("#selectedKredit").on("change","input[name*='totalKredit']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    var now=$("#totalKreditX"+at).autoNumeric('get');
    var total=parseFloat($("#subTotalKredit").autoNumeric("get"))-parseFloat(sum)+parseFloat(now);
    $("#subTotalKredit").autoNumeric("set",total);
});

$("input[name=cbSaveTemplate]").change(function() {
    if($(this).is(':checked')) $('input[name=txtTemplateName]').attr('disabled', false);
    else $('input[name=txtTemplateName]').attr('disabled', true).val('');
});

if(templateSetting) {
    $.each(templateSetting.debit,function(i,v) {
        $("#NewDebit").val(v).trigger('change');
    });
    $.each(templateSetting.kredit,function(i,v) {
        $("#NewKredit").val(v).trigger('change');
    });
}

});</script>