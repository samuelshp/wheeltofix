<?php
$strPageTitle = 'Akuntansi General Ledger';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bar-chart-o"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
<div class="row">
    
    <div class="col-xs-12 col-md-6 pull-left">
        <form name="frMposting" id="frMposting" method="post" action="<?=site_url('accounting/posting', NULL, FALSE)?>">
            <h4 style="margin:0 0 7px 0;">Posting Transaksi</h4>
            <div class="input-group">
                <label class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Periode</label>
                <input type="text" name="period" value="<?=date('Y-m')?>" class="required form-control postingDate" maxlength="7" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary" name="smtProcessType" id="smtProcessType" value="Save"><i class="glyphicon glyphicon-pencil"></i></button>
                </div>
            </div>
            <p style="margin-top:13px;"><a href="<?=site_url('adminpage/table/browse/23', NULL, FALSE)?>" target="_blank" class="btn btn-danger pull-right"><i class="glyphicon glyphicon-ban-circle"></i> Tutup Periode</a></p>
        </form>
    </div>
    <div class="col-xs-12 col-md-6 pull-right">
        <form name="searchForm" method="post" action="<?=site_url('accounting/generalledger', NULL, FALSE)?>">
            <label>Cari Data:</label>
            <div class="form-group"><div class="input-group"><label class="input-group-addon">Awal</label><input type="text" name="txtStart" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
            <div class="form-group"><div class="input-group"><label class="input-group-addon">Akhir</label><input type="text" name="txtEnd" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
            <div class="form-group"><div class="input-group"><input type="text" id="txtAccountName"  class="form-control" placeholder="Pilih Akun"/><label class="input-group-addon" id="loadAccount"></label></div></div>

            <div class="col pull-right" style="margin:10px 0px 0px 10px;">
                <button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button><?php
    if(!empty($strAccountID)):
        $strPrintURL = site_url('accounting/generalledger?print=true&accountid='.$strAccountID.'&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
        <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
        <a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
    endif; ?>  
            </div>
            <input type="hidden" id="accountID" name="accountID"/>
            <input type="hidden" id="accountCode" name="accountCode"/>
            <input type="hidden" id="accountName" name="accountName"/>
        </form>
    </div>
</div>
    <div class="pull-left col-xs-12 col-md-6" style="margin:-25px 0px 0px -15px;"><?=$strPage?></div>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div><div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-3">Periode</div>
            <div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
            <div class="col-md-3">Hari ini</div>
            <div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-3">Kode</div>
            <div class="col-md-9">: <?=$strAccountCode?></div>
            <div class="col-md-3">Nama</div>
            <div class="col-md-9">: <?=$strAccountName?></div>
        </div>
    </div>
    </div></div>

    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th>Tanggal</th>
        <th>Kode Transaksi</th>
        <th>Relasi</th>
        <th>Keterangan</th>
        <th>Debet</th>
        <th>Kredit</th>
		<th>Saldo</th>
    </tr></thead>
    <tbody><?php
if(!empty($arrItems)):
    foreach($arrItems as $e): ?>
        <tr>
            <td><?=formatDate2($e['dtTanggal'],'d/m/Y')?></td>
            <td><?=$e['vcKodeTransaksi']?></td>
            <td><?=$e['vcNamaRelasi']?></td>
			<td><?=$e['vcKeterangan']?></td>
			<td><?=setPrice($e['decD'])?></td>
			<td><?=setPrice($e['decK'])?></td>
			<td><?=setPrice($e['decTotal'])?></td>
        </tr>
<?php endforeach;
else :?>
    <tr><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif;?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>