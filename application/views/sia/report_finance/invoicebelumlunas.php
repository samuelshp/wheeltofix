<?php
$strPageTitle = 'Laporan Faktur Belum Lunas';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('report_finance/invoicebelumlunas', NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
        <label>Cari Data:</label>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Awal</label><input type="text" name="txtStart" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Akhir</label><input type="text" name="txtEnd" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        
        <div class="col pull-right" style="margin:10px 0px 0px 10px;">
            <button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button><?php
if($this->input->post('subSearch') != ''):
    $strPrintURL = site_url('report_finance/invoicebelumlunas?print=true&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
    <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
    <a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
endif; ?>  
        </div>

    <!--<input type="hidden" id="productID" name="productID"/>
        <input type="hidden" id="productCode" name="productCode"/>
        <input type="hidden" id="productName" name="productName"/>-->

    </form>
    <div class="col-xs-12 col-md-6 pull-left"><div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-3">Periode</div>
            <div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
            <div class="col-md-3">Hari ini</div>
            <div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
        </div>
    </div>
<!--<div class="col-md-6">
        <div class="row">
            <div class="col-md-3">Kode</div>
            <div class="col-md-9">: <?=$strProductCode?></div>
            <div class="col-md-3">Nama</div>
            <div class="col-md-9">: <?=$strProductName?></div>
        </div>
    </div>-->
    </div></div>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div class="col-xs-12"><?=$strPage?></div>

    <p class="spacer">&nbsp;</p>
	<div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th>NO</th>
        <th>NO TRANS</th>
        <th>TRANS</th>
        <th>TGL TRANS</th>
        <th>TGL KIRIM</th>
        <th>TOP</th>
        <th>TGL JATUH TEMPO</th>
        <th>OD</th>
        <th>KODE SLS</th>
        <th>NAMA SALESMAN</th>
        <th>NAMA CUSTOMER</th>
        <th>SALDO</th>
        <th>NOMINAL</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrInvoice)):
    $i = 1;
    foreach($arrInvoice as $e):
		$nominal = $e['grandtotal'];
		if($e['trans'] == 'IVR')
			$nominal = $nominal * -1;
	?>
        <tr>
			<td><?=$i?></td>
			<td><?=$e['code']?></td>
			<td><?=$e['trans']?></td>
			<td><?=formatDate2($e['date'],'d/m/Y')?></td>
			<td><?php if($e['delivery_time'] == '1970-01-01 07:00:00') echo ''; else echo formatDate2($e['delivery_time'],'d/m/Y'); ?></td>
			<td><?php echo floor(abs(strtotime(formatDate2($e['jatuhtempo'],'Y-m-d')) - strtotime(formatDate2($e['date'],'Y-m-d')))/(60*60*24)) ?></td>
			<td><?=formatDate2($e['jatuhtempo'],'d/m/Y')?></td>
			<td><?php echo floor(abs(strtotime(formatDate2($strEnd,'Y-m-d')) - strtotime(formatDate2($e['jatuhtempo'],'Y-m-d')))/(60*60*24)) ?></td>
			<td><?=$e['salesman_code']?></td>
			<td><?=$e['salesman_name']?></td>
			<td><?=$e['customer_name']?></td>
			<td><?=setPrice($e['grandtotal'])?></td>
			<td><?=setPrice($nominal)?></td>
        </tr><?php
        $i++;
    endforeach;    
else: ?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
</table></div>
    <?=$strPage?>
</div>