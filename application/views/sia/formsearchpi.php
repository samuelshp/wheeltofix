<?php
if(!empty($strSearchKey) || !empty($strSearchDate) || !empty($intSearchStatus)) {
    $strSearchFormClass = ' has-warning';
    $strSearchButtonClass = ' btn-warning';
} else {
    $strSearchKey = '';
    $strSearchFormClass = ''; 
    $strSearchButtonClass = ' btn-primary';
} ?>
	<form name="searchForm" method="post" action="<?=$strSearchAction?>" class="form-inline pull-right" style="">
        <div class="input-group<?=$strSearchFormClass?>">
            <span class="input-group-addon"><i class="fa fa-filter"></i></span>
            <input type="text" id="txtSearchNoPI" name="txtSearchNoPI" value="<?=$strSearchKey?>" class="form-control input-sm" placeholder="Search No. PI" style="width: 110px;" />	        
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" name="txtSearchDate" value="<?=$strSearchDate?>" class="form-control jwDate input-sm" placeholder="Search By Date" style="width: 110px;" />
	        <span class="input-group-addon" style="padding: 6px 6px;">-</span>
	        <input type="text" name="txtSearchDateTo" value="<?=$strSearchDateTo?>" class="form-control jwDate input-sm" placeholder="Range To" style="width: 110px;" />
			<span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary<?=$strSearchButtonClass?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
        </div>
    </form>