<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> Kontrak', 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12"><form name="frmChangeSubkontrak" id="frmChangeSubkontrak" method="post" action="<?=site_url('subkontrak/view/'.$intSubkontrakID, NULL, FALSE)?>" class="frmShop">      
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group pull-right">            
                <?php if($priv['canEditHeader'] || $priv['canEditTermin'] || $priv['canEditTeamMaterial']): ?>
                <button type="button" id="btnEdit" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                <button type="submit" id="btnUpdate" name="btnUpdateSubkontrak" class="btn btn-primary" style="display:none" value="Update Subkontrak"><i class="fa fa-pencil-square-o"></i></button>
                <?php endif; ?>
            </div>
        </div>        
    </div>    
    <div class="row">
        <?php //print_r($arrDataSubkontrak);?>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Nama Proyek</label>
                <select name="proyek_id" class="form-control chosen" disabled>
                    <?php foreach($availableKontrak as $kontrak): ?>
                        <option value="<?= $kontrak['id'] ?>" <?=($kontrak['id'] == $arrDataSubkontrak['kontrak_id']) ? "selected" : null ?> ><?= $kontrak['kont_name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <!-- <input id="kont-code" type="text" class="form-control input-sm" value="" disabled> -->
            </div>

            <div class="form-group">
                    <label>Kode Kontrak</label>
                    <input name="subkont_kode" type="text" class="form-control kontrak" name="subkont_kode" placeholder="Kode Subkontrak" value="<?=$arrDataSubkontrak['subkont_kode'] ?>" disabled>
            </div>

            <div class="form-group">
                <label>Nama Pekerjaan</label>
                <input name="job" type="text" class="form-control kontrak" placeholder="Nama Pekerjaan" value="<?=$arrDataSubkontrak['job'] ?>" disabled>
            </div>

            <div class="form-group">
                <label>Owner</label>
                <input type="text" name="owner-id" class="form-control kontrak" placeholder="Owner Proyek" value="<?=$arrDataSubkontrak['cust_name'] ?>" readonly> 
            </div>

        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Tanggal Subkontrak</label>
                <input name="subkont-date" type="date" class="form-control kontrak" value="<?=$arrDataSubkontrak['subkont_date'] ?>" placeholder="Nama Proyek" disabled>
            </div>

            <div class="form-group">
                <label>Nilai Kontrak after Tax</label>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input name="job-value" type="text" class="form-control kontrak currency" placeholder="Nilai Kontrak" value="<?=$arrDataSubkontrak['job_value'] ?>" disabled>
                </div>
            </div>

            <div class="form-group">
                <label>Total HPP</label>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="hpp" name="hpp" type="text" class="form-control kontrak currency" placeholder="0" value="<?=$arrDataSubkontrak['hpp'] ?>" readonly>
                </div>
            </div>

        </div>
    </div>
    <br><br>
    <?php if($priv['canViewTermin']): ?>
    <div class="form-group">
        <label>Termin & Rencana Pembayaran</label>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Jenis</th>
                    <th>Amount</th>
                    <th>Tanggal Tagih</th>
                    <th>Tanggal Bayar</th>
                    <th>Terbayar</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody id="termin-list">
            <?php if(!empty($arrDataSubkontrakTermin)): foreach($arrDataSubkontrakTermin as $e):?>
                <tr class="termin-row">
                    <td class="handle"><i class="fa fa-bars"></i></td>
                    <td><input type="text" name="jenis[]" class="form-control" value="<?=$e['jenis']?>" disabled required></td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input type="text" name="termin_amount[]" class="form-control currency" value="<?=$e['amount']?>" required>
                        </div>
                    </td>                
                    <td><input type="date" name="due_date[]" class="form-control" value="<?=$e['due_date']?>"></td>
                    <td><input type="date" name="terbayar_date[]" class="form-control" value="<?=$e['terbayar_date']?>"></td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input type="text" name="terbayar[]" class="form-control currency" value="<?=$e['terbayar']?>" required>
                        </div>
                    </td>                    
                    <td><button type="button" class="btn btn-sm btn-danger remove-row-button" style="display:none" data-tipe="termin" data-rowID="<?=$e['id']?>"><i class="fa fa-trash"></i></button></td>
                </tr>
                <?php endforeach; endif;?>
                <!-- <tr class="termin-row">
                    <td class="handle"><i class="fa fa-bars"></i></td>
                    <td><input type="text" name="jenis[]" class="form-control" value="<?= repopulate('jenis[]') ?>" placeholder="Nama Termin" required></td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input type="number" name="termin_amount[]" class="form-control termin-input" value="<?= repopulate('amount[]', 0) ?>" required>
                        </div>
                    </td>
                    <td><input type="date" name="due_date[]" class="form-control" required></td>
                </tr> -->
            </tbody>
        </table>
        <?php if($priv['canEditTermin']): ?>
        <button type="button" id="add-termin-button" class="btn btn-primary" disabled><i class="fa fa-plus"></i> Add Termin</button>
        <?php endif; ?>
    </div>
    <br>
    <?php endif; ?>
    <?php if($priv['canViewTeamMaterial']): ?>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label>Team</label>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Jabatan</th>
                            <th>Nama</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody id="team-list">
                        <?php if(!empty($arrDataSubkontrakTeam)): foreach($arrDataSubkontrakTeam as $e):?>
                        <tr class="team-row">
                            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="team-rowID[]" value="<?=$e['rowID']?>"/></td>
                            <td>
                                <select id="jab-${uid}" name="jabatan_id[]" class="form-control input-sm pic-select chosen" style="width:320px" disabled>
                                    <?php foreach($availableJabatan as $jabatan): ?>
                                        <option value="<?= $jabatan['strKey'] ?>" <?=($jabatan['strKey'] == $e['jabatan_id']) ? "selected" : null ?> ><?= $jabatan['strData'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <select id="peg-${uid}" name="name[]" class="form-control input-sm chosen" style="width:250px" disabled>
                                    <?php foreach($availableTeamName as $team): ?>
                                        <option value="<?= $team['id'] ?>" <?=($team['id'] == $e['id']) ? "selected" : null ?>><?= $team['adlg_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" data-tipe="team" data-rowID="<?=$e['rowID']?>" style="display:none"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <?php endforeach; endif; ?>
                    </tbody>
                </table>
                <?php if($priv['canEditTeamMaterial']): ?>
                <button type="button" id="add-team-button" class="btn btn-primary" disabled><i class="fa fa-plus"></i> Add Team</button>
                <button type="submit" name="btnCopyTeam" value="copy" id="add-team-button" class="btn btn-warning" style="padding-top:5px;" disabled><i class="fa fa-copy"></i> Copy Team</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        <label>Daftar Kebutuhan Material</label>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Bahan</th>
                    <th>Qty</th>
                    <th>Terpb</th>
                    <th style="width:50px">Satuan Bayar</th>
                    <th>Keterangan</th>
                    <th>Substitusi</th>
                    <th>Unit Price (hpp)</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody id="material-list">            
            <?php if(!empty($arrDataMaterial)): foreach($arrDataMaterial as $e):?>
            <tr class="material-row">
                <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="material-rowID[]" value="<?=$e['id_sk']?>"/></td>
                <td>
                    <div id="tooltip-mat-${uid}" data-toggle="tooltip" title="">
                        <select id="mat-${uid}" data-id="${uid}" name="material[]" class="form-control material-select chosen" style="width:150px" disabled>
                            <option value="0"> Choose Material</option>
                        <?php foreach($availableBahan as $bahan): ?>
                            <option value="<?= $bahan['id'] ?>" <?=($bahan['id'] == $e['id']) ? "selected": null?> > <?= $bahan['prod_title'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </td>        
                <td><input id="qty-${uid}" type="text" name="qty[]" data-id="${uid}" min="1" value="<?=$e['qty']?>" class="form-control input-sm qty-input float" disabled required></td>
                <td><input id="subkontrak_terpb-${uid}" type="text" name="subkontrak_terpb[]" data-id="${uid}" min="1" value="<?=$e['subkontrak_terpb']?>" class="form-control input-sm terpb-input float" ${priv.canEditTeamMaterial? '' : 'readonly' } disabled required></td>
                <td><input id="unit-${uid}" type="text" name="satuan_bayar" class="form-control input-sm" value="<?=$e['unit_title']?>" readonly style="width:50px"></td>
                <td><input type="text" name="keterangan[]" class="form-control input-sm" value="<?=$e['keterangan']?>" disabled></td>
                <td>
                    <div id="tooltip-substitusi-${uid}" data-toggle="tooltip" title="">
                        <select id="substitusi-${uid}" data-id="${uid}" name="substitusi[]" class="form-control substitusi-select chosen" style="width:150px" disabled>
                            <option selected> Choose Substitusi</option>
                        <?php foreach($availableSubstitusi as $substitusi): ?>
                            <option <?php if($substitusi['prod_id'] == $e['substitusi']) echo 'selected';?> value="<?=$substitusi['prod_id']?>"> <?= $substitusi['prod_title'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="hpp-${uid}" type="text" data-id="${uid}" name="hpp[]" class="form-control input-sm hpp-input currency" value="<?=$e['hpp']?>" disabled required>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="subtotal-${uid}" type="text" name="jumlah[]" class="form-control input-sm material-amount currency" value="<?=$e['jumlah']?>" readonly>
                    </div>
                </td>
                <td><button type="button" class="btn btn-sm btn-danger remove-row-button" style="display:none" data-tipe="material" data-rowID="<?=$e['id_sk']?>"><i class="fa fa-trash"></i></button></td>
            </tr>
            <?php endforeach; endif;?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th colspan="7">Total</th>
                    <th>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input id="material-total" type="text" class="form-control input-sm currency" value="<?=$intHPPSubkontrakMaterial?>" readonly >
                        </div>
                    </th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
        <?php if($priv['canEditTeamMaterial']): ?>
        <button type="button" id="add-material-button" class="btn btn-primary" disabled><i class="fa fa-plus"></i> Add Material</button>
        <?php endif; ?>
    </div>
    <br>
    <div class="form-group">
        <label>Daftar Kebutuhan Non-Material</label>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kategori</th>
                    <th>Amount (hpp)</th>
                </tr>
            </thead>
            <tbody id="non-material-list">            
            <?php if(!empty($arrDataNonMaterial)): foreach($arrDataNonMaterial as $e):?>
                <tr class="non-material-row">
                    <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="nonmaterial-rowID[]" value="<?=$e['id']?>"/></td>
                    <td><input type="text" name="kategori[]" class="form-control input-sm" placeholder="Nama Kategori" value="<?=$e['kategori']?>" disabled required></td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input type="text" name="amount[]" class="form-control input-sm currency" value="<?=$e['amount']?>"  required>
                        </div>
                    </td>
                    <td><button type="button" class="btn btn-sm btn-danger remove-row-button" style="display:none" data-tipe="nonmaterial" data-rowID="<?=$e['id']?>"><i class="fa fa-trash"></i></button></td>
                </tr>
            <?php endforeach; endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th>Total</th>
                    <th>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input id="nonmaterial-total" type="text" class="form-control input-sm currency" value="<?=$intHPPSubkontrakNonMaterial?>" readonly>
                        </div>
                    </th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
        <?php if($priv['canEditTeamMaterial']): ?>
        <button type="button" id="add-non-material-button" class="btn btn-primary" disabled><i class="fa fa-plus"></i> Add Non-material</button>
        <?php endif; ?>
    </div>
    <br>
    <?php endif; ?>

    <div class="form-group">                    
        <a href="<?= site_url('subkontrak/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>                
    </div>
    <div class="form-group hide">
        <textarea name="teamToDelete"></textarea>
    </div>

    </div>
</form></div>