<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $(".chosen").chosen({search_contains: true});
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
});
$('#add-team-button').click(addTeamRow);
$('#add-termin-button').click(addTerminRow);
$('#add-material-button').click(addMaterialRow);
$('#add-non-material-button').click(addNonMaterialRow);
$('table').on('click', '.remove-row-button', removeRow);
$('select[name=proyek_id]').change(findOwnerProyek);
$('#material-list').on('change', '.material-select', materialSelectHandler);
$('#material-list').on('keyup change', '.qty-input', calculateMaterialItemSubtotal);
$('#material-list').on('keyup change', '.hpp-input', calculateMaterialItemSubtotal);
$('#non-material-list').on('keyup change', '.nonmaterial-amount', calculateNonMaterialSubtotal);

function addTerminRow(){    
    $('#termin-list').append(`
        <tr class="termin-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td><input type="text" name="jenis[]" class="form-control input-sm" value="" placeholder="Nama Termin" required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="termin_amount[]" class="form-control input-sm currency" value="" required>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="terbayar[]" class="form-control input-sm currency" value="">
                </div>
            </td>
            <td><input type="text" name="terbayar_date[]" class="form-control input-sm jwDateTime" value=""></td>
            <td><input type="text" name="due_date[]" class="form-control input-sm jwDateTime" value=""></td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#termin-list .hidden-el').show('fast');    
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
}

function addTeamRow(){    
    $('#team-list').append(`
        <tr class="team-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td>
                <select id="jab-$uid" name="jabatan_id[]" class="form-control input-sm pic-select chosen">
                    <?php foreach($availableJabatan as $jabatan): ?>
                        <option value="<?= $jabatan['strKey'] ?>"><?= $jabatan['strData'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>
                <select id="peg-$uid" name="name[]" class="form-control peg-select input-sm chosen">
                    <?php foreach($availableTeamName as $pegawai): ?>
                        <option value="<?= $pegawai['id'] ?>"><?= $pegawai['adlg_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td style="text-align:center"><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#team-list .hidden-el').show('fast');
    $('#team-list .pic-select.chosen').chosen({width: "320px"});
    $('#team-list .peg-select.chosen').chosen({width: "250px"});
}

function addMaterialRow() {    
    $('#material-list').append(`
        <tr class="material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="material-rowID[]" value=""/></td>
            <td>
                <div id="tooltip-mat" data-toggle="tooltip" title="">
                    <select id="mat-new" data-id="new" name="material[]" class="form-control material-select chosen">
                        <option value="0"> Choose Material</option>
                    <?php foreach($availableBahan as $bahan): ?>
                        <option value="<?= $bahan['id'] ?>"> <?= $bahan['prod_title'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            
            <td><input id="qty-$" type="text" name="qty[]" data-id="$" min="0" value="" class="form-control input-sm qty-input float" required></td>
            <td><input id="subkontrak_terpb-$" type="text" name="subkontrak_terpb[]" value="" class="form-control input-sm float" required></td>
            <td><input type="text" name="satuan_bayar" class="form-control input-sm unit-title" readonly></td>
            <td><input type="text" name="keterangan[]" class="form-control input-sm" value="" ></td>
            <td>
                <div id="tooltip-substitusi-$uid" data-toggle="tooltip" title="">
                    <select id="substitusi-$uid" data-id="$uid" name="substitusi[]" class="form-control substitusi-select chosen" disabled>
                        <option selected> Choose Substitusi</option>
                    <?php foreach($availableSubstitusi as $substitusi): ?>
                        <option value="<?= $substitusi['prod_id'] ?>"> <?= $substitusi['prod_title'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="hpp-$uid" type="text" data-id="$uid" name="hpp[]" class="form-control input-sm hpp-input currency" value="" required>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="subtotal-$uid" type="text" name="jumlah[]" class="form-control input-sm material-amount currency" value="" readonly>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('#material-list .hidden-el').show('fast');
    $(`.material-select`).chosen({width: "150px"});
    $(`.substitusi-select`).chosen({width: "150px"});  
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $(".float").autoNumeric('init',{'aSep': ".",'aDec': ",", 'mDec': 4});  
}

function addNonMaterialRow(){
    $('#non-material-list').append(`
        <tr class="non-material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="nonmaterial-rowID[]" value=""/></td>
            <td><input type="text" name="kategori[]" class="form-control input-sm" placeholder="Nama Kategori" value="" required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="amount[]" class="form-control input-sm currency nonmaterial-amount" value="" required>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" ><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('#non-material-list .hidden-el').show('fast');  
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
}

function removeRow(e){
    $(e.target)
        .parents('tr')
        .hide(200, function(){            
            $(e.target).parents('tr').remove()
            calculateHPP();
        });
}

function findOwnerProyek() {
    $.ajax({        
        url: "<?=site_url('api/kontrak_customer_subkontrak/"+$(this).val()+"/t.id?h="+userHash+"')?>",
        method: 'GET',       
        success: function(result){                    
            $("input[name=owner-id]").val(result.data.cust_name);
        },
        error: function(err){
            alert("Oops, error.");
        },        
    });
}

function materialSelectHandler(e){        
    $.ajax({
        url: "<?= site_url('subkontrak_ajax/getMaterialItems/')?>"+$(this).val(),
        method: "GET",
        success: function(result) {
            $(e.target).closest(".material-row").find(".unit-title").val(result.unit_title);
        },
        error: function(err){

        }
    });        
}

function calculateMaterialItemSubtotal(e) {
    var qty = parseFloat($(e.target).closest(".material-row").find(".qty-input").autoNumeric('get'));
    var hpp = parseFloat($(e.target).closest(".material-row").find(".hpp-input").autoNumeric('get'));
    $(e.target).closest(".material-row").find(".material-amount").autoNumeric('set',qty*hpp);
    calculateMaterialSubtotal();
}

function calculateMaterialSubtotal() {
    var material = 0
    $('.material-amount').each(function(){
        material += parseFloat($(this).autoNumeric('get'));
    })
    $("#material-total").autoNumeric('set',material);
    calculateHPP();
}

function calculateNonMaterialSubtotal() {
    var nonmaterial = 0;   
    $('.nonmaterial-amount').each(function(){
        nonmaterial += parseFloat($(this).autoNumeric('get'));        
    });
    $("#nonmaterial-total").autoNumeric('set',nonmaterial);
    calculateHPP();    
}

function calculateHPP(){
    var subtotal_material = parseFloat($("#material-total").autoNumeric('get'));
    var subtotal_nonmaterial = parseFloat($("#nonmaterial-total").autoNumeric('get'));
    var hpp = subtotal_material + subtotal_nonmaterial;
    $("#hpp").autoNumeric('set',hpp);
}

</script>

<!-- <script>

let totalTermin = 0;
var priv        = <?= json_encode($priv) ?>;
var materials   = <?= json_encode($subkontrakMaterial) ?>;
console.log('materials:');
console.log(materials);

getKontrak = (id) => {
    return $.ajax({
        url: `<?= site_url('kontrak/get')?>`,
        method: 'POST',
        data: {id: id}
    })
}

getMaterial = (id) => {
    return $.ajax({
        url: `<?= site_url('material/get/') ?>${id}`,
        method: 'GET'
    })
}

getSmallestUnit = (id) => {
    return $.ajax({
        url: `<?= site_url('unit/smallestUnit/') ?>${id}`,
        method: 'GET'
    })
}

kontCodeEventHandler = (e) => {

    id = e.target.value;
    if(id != 0){
        getKontrak(id).then(resp => {
            if(resp){
                $('#owner-id').val(resp.cust_name);
                $('#kontrak-id').val(resp.id);
                $('#kont-name').val(resp.kont_name);
                // $('#job').val(resp.kont_job);
                $('#subkont-date').val(resp.kont_date);
                $('#kont-value').val(resp.kont_value);
            } else {
                $('.kontrak').val(`Warning: Kontrak belum dibuat`);
            }
            
        })
    }
}

updateMaterialTooltip = function(e) {
    uid = $(this).data('id');
    $(`#tooltip-mat-${uid}`).attr('title', $(`#mat-${uid}`).find(':selected').text());
    $(`#tooltip-mat-${uid}`).tooltip('fixTitle');
}

updateSubstitusiTooltip = function(e) {
    uid = $(this).data('id');
    $(`#tooltip-substitusi-${uid}`).attr('title', $(`#substitusi-${uid}`).find(':selected').text());
    $(`#tooltip-substitusi-${uid}`).tooltip('fixTitle');
}

addTeamRow = (e, team = null) => {
    uid = new Date().valueOf();

    $('#team-list').append(`
        <tr class="team-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td>
                <select id="jab-${uid}" name="jabatan_id[]" class="form-control input-sm pic-select chosen">
                    <?php foreach($availableJabatan as $jabatan): ?>
                        <option value="<?= $jabatan['strKey'] ?>" ${ team != null? (<?= $jabatan['strKey'] ?> == team.jabatan_id? 'selected' : '') : '' }><?= $jabatan['strData'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>
                <select id="peg-${uid}" name="name[]" class="form-control peg-select input-sm">
                    <?php foreach($availablePegawai as $pegawai): ?>
                        <option value="<?= $pegawai->id ?>" ${ team != null? (<?= $pegawai->id ?> == team.name? 'selected' : '') : '' }><?= $pegawai->adlg_name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td style="text-align:center"><button type="button" class="btn btn-sm btn-danger remove-row-button" ${priv.canEditTeamMaterial? '' : 'disabled' }><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#team-list .hidden-el').show('fast');
    $(`#jab-${uid}`).chosen({width: "320px"});
    $(`#peg-${uid}`).chosen({width: "250px"});
}

addTerminRow = (e, termin = null) => {
    // console.log(termin == null? 'true' : 'false')
    $('#termin-list').append(`
        <tr class="termin-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td><input type="text" name="jenis[]" class="form-control input-sm" value="${termin != null? termin.jenis : '' }" placeholder="Nama Termin" ${priv.canEditTermin? '' : 'readonly' } required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="number" name="termin_amount[]" class="form-control input-sm termin-input" value="${termin != null? termin.amount : '0' }" ${priv.canEditTermin? '' : 'readonly' } required>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="number" name="terbayar[]" class="form-control input-sm termin-input" value="${termin != null? termin.terbayar : '0' }" ${priv.canEditTermin? '' : 'readonly' }>
                </div>
            </td>
            <td><input type="date" name="terbayar_date[]" class="form-control input-sm" value="${termin != null? termin.terbayar_date : '0' }" ${priv.canEditTermin? '' : 'readonly' }></td>
            <td><input type="date" name="due_date[]" class="form-control input-sm" value="${termin != null? termin.due_date : '0' }" ${priv.canEditTermin? '' : 'readonly' }></td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" ${priv.canEditTermin? '' : 'disabled' }><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#termin-list .hidden-el').show('fast');
}

addMaterialRow = (e, material = null) => {
    uid = new Date().valueOf();
    $('#material-list').append(`
        <tr class="material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td>
                <div id="tooltip-mat-${uid}" data-toggle="tooltip" title="">
                    <select id="mat-${uid}" data-id="${uid}" name="material[]" class="form-control material-select chosen" ${priv.canEditTeamMaterial? '' : 'disabled' }>
                        <option value="0"> Choose Material</option>
                    <?php foreach($availableBahan as $bahan): ?>
                        <option value="<?= $bahan->id ?>" ${ material != null? (<?= $bahan->id ?> == material['product_id']? 'selected' : '') : '' }> <?= $bahan->prod_title ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            
            <td><input id="qty-${uid}" type="number" step="0.0001" name="qty[]" data-id="${uid}" min="0" value="${ material? material['qty'] : 1 }" class="form-control input-sm qty-input" ${priv.canEditTeamMaterial? '' : 'readonly' } required></td>
            <td><input id="subkontrak_terpb-${uid}" type="number" step="0.0001" name="subkontrak_terpb[]" data-id="${uid}" min="0" value="${ material? material['subkontrak_terpb'] : 0 }" class="form-control input-sm terpb-input" ${priv.canEditTeamMaterial? '' : 'readonly' } required></td>
            <td><input id="unit-${uid}" type="text" class="form-control input-sm" value="${ material? material['satuan_bayar'] : '' }"  readonly></td>
            <td><input type="text" name="keterangan[]" class="form-control input-sm" value="${ material? material['keterangan'] : '' }" ${priv.canEditTeamMaterial? '' : 'readonly' }></td>
            <td>
                <div id="tooltip-substitusi-${uid}" data-toggle="tooltip" title="">
                    <select id="substitusi-${uid}" data-id="${uid}" name="substitusi[]" class="form-control substitusi-select chosen" ${priv.canEditTeamMaterial? '' : 'disabled' }>
                        <option selected> Choose Substitusi</option>
                    <?php foreach($availableSubstitusi as $substitusi): ?>
                        <option value="<?= $substitusi['id'] ?>" ${ material != null? (<?= $substitusi['id'] ?> == material.substitusi? 'selected' : '') : '' }> <?= $substitusi['prod_title'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="hpp-${uid}" type="number" data-id="${uid}" name="hpp[]" class="form-control input-sm hpp-input" value="${ material? material['hpp'] : 0 }" ${priv.canEditTeamMaterial? '' : 'readonly' } required>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="subtotal-${uid}" type="number" name="jumlah[]" class="form-control input-sm material-amount" value="${ material? material['qty']*material['hpp'] : '0' }" ${priv.canEditTeamMaterial? '' : 'readonly' } readonly>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" ${priv.canEditTeamMaterial? '' : 'disabled' }><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('#material-list .hidden-el').show('fast');
    //chosen
    $(`#mat-${uid}`).chosen({width: "150px"});
    $(`#substitusi-${uid}`).chosen({width: "150px"});
    //tooltip
    $(`#tooltip-mat-${uid}`).attr('title', $(`#mat-${uid}`).find(':selected').text());
    $(`#tooltip-mat-${uid}`).tooltip();
    $(`#tooltip-substitusi-${uid}`).attr('title', $(`#substitusi-${uid}`).find(':selected').text());
    $(`#tooltip-substitusi-${uid}`).tooltip();
}

addNonMaterialRow = (e, nonmaterial = null) => {
    $('#non-material-list').append(`
        <tr class="non-material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td><input type="text" name="kategori[]" class="form-control input-sm" placeholder="Nama Kategori" value="${ nonmaterial? nonmaterial.kategori : '' }" ${priv.canEditTeamMaterial? '' : 'disabled' } required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="number" name="amount[]" class="form-control input-sm nonmaterial-amount" value="${ nonmaterial? nonmaterial.amount : '0' }" ${priv.canEditTeamMaterial? '' : 'disabled' } required>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" ${priv.canEditTeamMaterial? '' : 'disabled' }><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#non-material-list .hidden-el').show('fast');
}

// fungsi removeRow bisa digunakan untuk remove dari table team, material, dan non-material
removeRow = (e) => {
    $(e.target)
        .parents('tr')
        .hide(200, () => {
            $(e.target).parents('tr').remove()
            calculateSubkontrakHpp();
        });
}

Number.prototype.format = function(n, x, s, c) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = this.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

isIntegerKey = (e) => {
    var charCode = (e.which) ? e.which : e.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

isFloatKey = (e) => {
    var charCode = (e.which) ? e.which : e.keyCode
    if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;
    return true;
}

convertIntoPlainNumber = (string) => {
    convertedString = string.replace('Rp.', '').replace(' ', '').replace(/\./g, '').replace(/\./g, '').replace(',00', '');
    return convertedString;
}

convertIntoCurrency = (string, currency = false) => {
    num = parseInt(string);
    convertedString = '';
    if(currency)
        convertedString += 'Rp. ';

    convertedString = num.format(2, 3, '.', ',')
    return convertedString;
}

onFocusCurrencyInput = function(e){
    val = $(this).val();
    cvt = convertIntoPlainNumber(val);
    $(this).val(cvt);
}

onBlurCurrencyInput = function(e){
    val = $(this).val();
    cvt = convertIntoCurrency(val);
    $(this).val(cvt);
}

calculateTermin = () => {
    totalTermin = 0;
    $('.termin-input').each(function(index){
        totalTermin += parseInt($(this).val());
    })
}

calculateSubtotal = (e) => {
    uid = e.target.attributes.getNamedItem('data-id').value;
    qty = parseInt($(`#qty-${uid}`).val());
    hpp = parseInt($(`#hpp-${uid}`).val());
    subtotal = qty * hpp;
    $(`#subtotal-${uid}`).val(subtotal);
    calculateSubkontrakHpp();
}

calculateSubkontrakHpp = () => {
    material = 0;
    nonmaterial = 0;
    subkontrakHpp = 0;
    $('.material-amount').each(function(){
        material += parseInt($(this).val());
    })
    $('#material-total').val(material.format(2, 3, '.', ','));
    $('#material-total').attr('title', $('#material-total').val());
    $('#material-total').tooltip('fixTitle');

    $('.nonmaterial-amount').each(function(){
        nonmaterial += parseInt($(this).val());
    })
    $('#nonmaterial-total').val(nonmaterial.format(2, 3, '.', ','));
    $('#nonmaterial-total').attr('title',  $('#nonmaterial-total').val());
    $('#nonmaterial-total').tooltip();

    subkontrakHpp = material + nonmaterial;
    $('#hpp').val(subkontrakHpp.format(2, 3, '.', ','));
}

validateForm = (e) => {
    error   = false;
    errMsg  = [];
    kontrak = parseInt($('#job-value').val());
    calculateTermin();

    $('#kont-code').prop('disabled',false);
    $('.currency-format').each(onFocusCurrencyInput);
    // if(kontrak != totalTermin)
    //     errMsg.push('Nilai Kontrak dan jumlah list termin tidak sesuai');
    
    materialErr = false;
    $('.material-select').each((index) => {
        if($('.material-select')[index].value == 0)
            materialErr = true;
    })
    if(materialErr)
        errMsg.push('Form material harus dilengkapi')


    if(errMsg.length > 0){
        swal({
            text: errMsg.join(', '),
            icon: 'error'
        })
        return false;
    } else {
        return true;
    }
}



render = (object, hpp) => {
    // console.log(object);
    $('#job').val(object.job);
    $('#kontrak-id').val(object.kontrak_id);
    $('#subkont_kode').val(object.subkont_kode);
    if(object.kontrak) {
        if(object.kontrak.owner.cust_name) {
            $('#owner-id').val(object.kontrak.owner.cust_name);
        }
        if(object.kontrak.kont_name) {
            $('#kont-name').val(object.kontrak.kont_name);
        }
    }
    $('#subkont-date').val(object.subkont_date);
    $('#job-value').val(object.job_value);
    $('#hpp').val(hpp);

    $('#termin-list').empty(); 
    object.subkontrak_team.map(team => addTeamRow(this, team));
    object.subkontrak_termin.map(termin => addTerminRow(this, termin));
    // object.subkontrak_material.map(material => addMaterialRow(this, material));
    materials.map(material => addMaterialRow(this, material));
    object.subkontrak_nonmaterial.map(nonmaterial => addNonMaterialRow(this, nonmaterial))

    calculateSubkontrakHpp();
}

$(function(){
    var object     = <?= json_encode($object) ?>;
    var hpp        = <?= json_encode($hpp) ?>;

    if(object != null) 
        render(object, hpp);
    else
        document.getElementById('subkont-date').valueAsDate = new Date();

    $('#kont-code').change(kontCodeEventHandler);
    $('#add-team-button').click(addTeamRow);
    $('#add-termin-button').click(addTerminRow);
    $('#add-material-button').click(addMaterialRow);
    $('#add-non-material-button').click(addNonMaterialRow);
    $('table').on('click', '.remove-row-button', removeRow);
    $('.chosen').chosen();
    $('#material-list').on('change', '.material-select', materialSelectHandler);
    $('#material-list').on('keyup change', '.qty-input', calculateSubtotal);
    $('#material-list').on('keyup change', '.hpp-input', calculateSubtotal);
    $('#non-material-list').on('keyup change', '.nonmaterial-amount', calculateSubkontrakHpp);
    $('form').on('submit', validateForm)

    $('form').on('keydown', '.currency-format', isIntegerKey);
    $('form').on('focus', '.currency-format', onFocusCurrencyInput);
    $('form').on('blur', '.currency-format', onBlurCurrencyInput);
    $('.currency-format').each(onBlurCurrencyInput);

    $('#material-list').on('change', '.material-select', updateMaterialTooltip)
    $('#material-list').on('change', '.substitusi-select', updateSubstitusiTooltip)
    
})

</script> -->