<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>

<script type="text/javascript">

var toDelete = [];

$(document).ready(function(){
    $('.chosen').chosen({search_contains: true});    
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
    $(".float").autoNumeric('init',{'aSep': ".",'aDec': ",", 'mDec': 4});
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
});

$("button#btnEdit").click(function(){      
    $('html, body').scrollTop(0);      
    $("#frmChangeSubkontrak .form-control").removeAttr('disabled');
    $("#frmChangeSubkontrak .btn").removeAttr('disabled');
    $("#frmChangeSubkontrak .chosen").chosen('destroy');    
    $("#frmChangeSubkontrak .chosen").chosen();
    $('.remove-row-button').show();
    $(this).hide();
    $("button#btnUpdate").show();
});

$("button#btnUpdate").click(function(){
    return confirm("Apakah yakin akan menyimpan data?");
});

$("#add-termin-button").click(addTerminRow);
$("#add-team-button").click(addTeamRow);
$("#add-material-button").click(addMaterialRow);
$("#add-non-material-button").click(addNonMaterialRow);
$('#material-list').on('change', '.material-select', materialSelectHandler);
$('#material-list').on('keyup change', '.qty-input', calculateMaterialItemSubtotal);
$('#material-list').on('keyup change', '.hpp-input', calculateMaterialItemSubtotal);
$('#non-material-list').on('keyup change', '.nonmaterial-amount', calculateNonMaterialSubtotal);

$('table').on('click', '.remove-row-button', removeRow);

$("select[name=proyek_id]").change(function(){
    var proyek_id = $(this).val();
    $.ajax({
        url: "<?=site_url('api/subkontrak/"+proyek_id+"?h="+userHash+"')?>",                
        success: function(result){
            if(result.success){
                $("input[name=owner-id]").val(result.data.cust_name);            
            }
        },
        error: function(err){
            alert("Oops, error.");
        }
    });
});

function materialSelectHandler(e){        
    $.ajax({
        url: "<?= site_url('subkontrak_ajax/getMaterialItems/')?>"+$(this).val(),
        method: "GET",
        success: function(result) {
            console.log(result);
            $(e.target).closest(".material-row").find(".unit-title").val(result.unit_title);
        },
        error: function(err){

        }
    });        
}

function addTerminRow(){    
    $('#termin-list').append(`
        <tr class="termin-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i></td>
            <td><input type="text" name="jenis[]" class="form-control input-sm" value="" placeholder="Nama Termin" required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="termin_amount[]" class="form-control input-sm termin-input currency" value="0,00" required>
                </div>
            </td>
            
            <td><input type="date" name="due_date[]" class="form-control input-sm" value=""></td>
            <td><input type="date" name="terbayar_date[]" class="form-control input-sm" value=""></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="terbayar[]" class="form-control input-sm termin-input currency" value="0,00" >
                </div>
            </td>
            
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#termin-list .hidden-el').show('fast');    
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
}

function addTeamRow(){    
    $('#team-list').append(`
        <tr class="team-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="team-rowID[]" value=""/></td>
            <td>
                <select id="jab-$" name="jabatan_id[]" class="form-control input-sm pic-select chosen">
                    <?php foreach($availableJabatan as $jabatan): ?>
                        <option value="<?= $jabatan['strKey'] ?>"><?= $jabatan['strData'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td>
                <select id="peg-$" name="name[]" class="form-control peg-select input-sm">
                    <?php foreach($availableTeamName as $pegawai): ?>
                        <option value="<?= $pegawai['id'] ?>"><?= $pegawai['adlg_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);

    $('#team-list .hidden-el').show('fast');
    $(`.pic-select`).chosen({width: "320px"});
    $(`.peg-select`).chosen({width: "250px"});
}

function addMaterialRow() {    
    $('#material-list').append(`
        <tr class="material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="material-rowID[]" value=""/></td>
            <td>
                <div id="tooltip-mat" data-toggle="tooltip" title="">
                    <select id="mat-new" data-id="new" name="material[]" class="form-control material-select chosen">
                        <option value="0"> Choose Material</option>
                    <?php foreach($availableBahan as $bahan): ?>
                        <option value="<?= $bahan['id'] ?>"> <?= $bahan['prod_title'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            
            <td><input id="qty-$" type="text" name="qty[]" data-id="$" min="0" value="" class="form-control input-sm qty-input float" required></td>
            <td><input id="subkontrak_terpb-$" type="text" name="subkontrak_terpb[]" value="" class="form-control input-sm float" required></td>
            <td><input type="text" name="satuan_bayar" class="form-control input-sm unit-title" readonly></td>
            <td><input type="text" name="keterangan[]" class="form-control input-sm" value="" ></td>
            <td>
                <div id="tooltip-substitusi-$uid" data-toggle="tooltip" title="">
                    <select id="substitusi-$uid" data-id="$uid" name="substitusi[]" class="form-control substitusi-select chosen">
                        <option selected> Choose Substitusi</option>
                    <?php foreach($availableSubstitusi as $substitusi): ?>
                        <option value="<?= $substitusi['prod_id'] ?>"> <?= $substitusi['prod_title'] ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="hpp-$uid" type="text" data-id="$uid" name="hpp[]" class="form-control input-sm hpp-input currency" value="" required>
                </div>
            </td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="subtotal-$uid" type="text" name="jumlah[]" class="form-control input-sm material-amount currency" value="" readonly>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('#material-list .hidden-el').show('fast');
    $(`.material-select`).chosen({width: "150px"});
    $(`.substitusi-select`).chosen({width: "150px"});  
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $(".float").autoNumeric('init',{'aSep': ".",'aDec': ",", 'mDec': 4});  
}

function addNonMaterialRow(){
    $('#non-material-list').append(`
        <tr class="non-material-row hidden-el">
            <td class="handle"><i class="fa fa-bars"></i><input type="hidden" name="nonmaterial-rowID[]" value=""/></td>
            <td><input type="text" name="kategori[]" class="form-control input-sm" placeholder="Nama Kategori" value="" required></td>
            <td>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input type="text" name="amount[]" class="form-control input-sm currency nonmaterial-amount" value="" required>
                </div>
            </td>
            <td><button type="button" class="btn btn-sm btn-danger remove-row-button" ><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('#non-material-list .hidden-el').show('fast');  
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
}

function removeRow(e){    
    var tipe =  $(this).data('tipe');
    var rowID = $(this).data('rowid');
    toDelete.push({'tipe': tipe, 'rowid': rowID});
    $("textarea[name=teamToDelete]").val(JSON.stringify(toDelete));
    $(e.target)
        .parents('tr')
        .hide(200, function(){
            $(e.target).parents('tr').remove()
            // calculateSubkontrakHpp();
        });
}

function recordChange(e){
    console.log($(e.target));
}

function calculateMaterialItemSubtotal(e) {        
    var qty = parseFloat($(e.target).closest(".material-row").find(".qty-input").autoNumeric('get'));
    var hpp = parseFloat($(e.target).closest(".material-row").find(".hpp-input").autoNumeric('get'));
    $(e.target).closest(".material-row").find(".material-amount").autoNumeric('set',qty*hpp);    
    calculateMaterialSubtotal();
}

function calculateMaterialSubtotal() {        
    var material = 0
    $('.material-amount').each(function(){
        material += parseFloat($(this).autoNumeric('get'));
    })
    $("#material-total").autoNumeric('set',material);
    calculateHPP();
}

function calculateNonMaterialSubtotal() {    
    var nonmaterial = 0;   
    $('.nonmaterial-amount').each(function(){
        nonmaterial += parseFloat($(this).autoNumeric('get'));        
    });
    $("#nonmaterial-total").autoNumeric('set',nonmaterial);
    calculateHPP();    
}

function calculateHPP(){    
    var subtotal_material = parseFloat($("#material-total").autoNumeric('get'));
    var subtotal_nonmaterial = parseFloat($("#nonmaterial-total").autoNumeric('get'));
    var hpp = subtotal_material + subtotal_nonmaterial;
    $("#hpp").autoNumeric('set',hpp);
}

</script>