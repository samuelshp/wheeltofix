
<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> Kontrak', 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12" ><form name="frmAddSubkontrak" id="frmAddSubkontrak" method="post" action="<?=site_url('subkontrak/', NULL, FALSE)?>" class="frmShop">  
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Nama Project</label>
                    <select id="kont-code" name="proyek_id" class="form-control chosen">
                        <option value="0" selected>Select your option</option>
                        <?php foreach($availableKontrak as $kontrak): ?>                        
                            <option value="<?= $kontrak['id'] ?>" ><?= $kontrak['kont_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <!-- <input type="hidden" id="kontrak-id" name="kontrak_id" value="<?= $object? $object->kontrak_id : '' ?>"> -->
                </div>
                <div class="form-group">
                    <label>Kode Kontrak</label>
                    <input id="subkont_kode" type="text" class="form-control kontrak" name="subkont_kode" placeholder="Kode Subkontrak" value="<?= isset($object)? $object->kont_code : '' ?>" <?= !$priv['canEditHeader']? 'readonly' : '' ?> required>
                </div>

                <div class="form-group">
                    <label>Nama Pekerjaan</label>
                    <input id="job" type="text" class="form-control kontrak" name="job" placeholder="Nama Pekerjaan">
                </div>

                <div class="form-group">
                    <label>Owner</label>
                    <input type="text" id="owner-id" name="owner-id" class="form-control kontrak" placeholder="Owner Proyek" disabled> 
                </div>

            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Tanggal Subkontrak</label>
                    <input id="subkont-date" type="text" class="form-control kontrak jwDateTime" name="subkont_date" >
                </div>

                <div class="form-group">
                    <label>Nilai Kontrak after Tax</label>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="job-value" name="job-value" type="text" class="form-control kontrak currency" name="job_value" placeholder="Nilai Kontrak" value="0" style="z-index:0">
                    </div>
                </div>

                <div class="form-group">
                    <label>Total HPP</label>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="hpp" type="text" name="hppTotal" class="form-control kontrak currency" placeholder="0" value="0" style="z-index:0" readonly>
                    </div>
                </div>

            </div>
        </div>
        <br><br>
        <?php if($priv['canViewTermin']): ?>
        <div class="form-group">
            <label>Termin & Rencana Pembayaran</label>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Jenis</th>
                        <th>Amount</th>
                        <th>Terbayar</th>
                        <th>Tanggal Bayar</th>
                        <th>Tanggal Kuitansi</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody id="termin-list">
                    <tr class="termin-row">
                        <td class="handle"><i class="fa fa-bars"></i></td>
                        <td><input type="text" name="jenis[]" class="form-control" value="DP" required></td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input type="text" name="termin_amount[]" class="form-control currency" value="0,00" required>
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input type="text" name="terbayar[]" class="form-control input-sm currency" value="0,00" >
                            </div>
                        </td>
                        <td><input type="text" name="terbayar_date[]" class="form-control input-sm jwDateTime" ></td>
                        <td><input type="text" name="due_date[]" class="form-control jwDateTime"></td>
                        <td><button type="button" class="btn btn-sm btn-danger remove-row-button" disabled><i class="fa fa-trash"></i></button></td>
                    </tr>
                    <tr class="termin-row">
                        <td class="handle"><i class="fa fa-bars"></i></td>
                        <td><input type="text" name="jenis[]" class="form-control" value="<?= repopulate('jenis[]') ?>" placeholder="Nama Termin" required></td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input type="text" name="termin_amount[]" class="form-control currency" value="0,00" required>
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input type="text" name="terbayar[]" class="form-control input-sm currency" value="0,00">
                            </div>
                        </td>
                        <td><input type="text" name="terbayar_date[]" class="form-control input-sm jwDateTime" ></td>
                        <td><input type="text" name="due_date[]" class="form-control jwDateTime"></td>
                        <td><button type="button" class="btn btn-sm btn-danger remove-row-button" <?= !$priv['canEditTermin']? 'disabled' : '' ?>><i class="fa fa-trash"></i></button></td>
                    </tr>
                </tbody>
            </table>
            <?php if($priv['canEditTermin']): ?>
            <button type="button" id="add-termin-button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Termin</button>
            <?php endif; ?>
        </div>
        <br>
        <?php endif; ?>
        <?php if($priv['canViewTeamMaterial']): ?>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Team</label>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Jabatan</th>
                                <th>Nama</th>
                                <th style="width:60px;text-align:center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="team-list">
                        </tbody>
                    </table>
                    <?php if($priv['canEditTeamMaterial']): ?>
                    <button type="button" id="add-team-button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Team</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label>Daftar Kebutuhan Material</label>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Bahan</th>
                        <th>Qty</th>
                        <th>Terpb</th>
                        <th style="width:50px">Satuan Bayar</th>
                        <th>Keterangan</th>
                        <th>Substitusi</th>
                        <th>Unit Price (hpp)</th>
                        <th>Jumlah</th>
                        <th style="width:40px;text-align:center">Action</th>
                    </tr>
                </thead>
                <tbody id="material-list">
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th colspan="7">Total</th>
                        <th>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input id="material-total" data-toggle="tooltip" title="" type="text" class="form-control input-sm currency" value="0" readonly>
                            </div>
                        </th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <?php if($priv['canEditTeamMaterial']): ?>
            <button type="button" id="add-material-button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Material</button>
            <?php endif; ?>
        </div>
        <br>
        <div class="form-group">
            <label>Daftar Kebutuhan Non-Material</label>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kategori</th>
                        <th>Amount (hpp)</th>
                        <th style="width:60px;text-align:center">Action</th>
                    </tr>
                </thead>
                <tbody id="non-material-list">
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Total</th>
                        <th>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input id="nonmaterial-total" data-toggle="tooltip" title="" type="text" class="form-control input-sm currency" value="0" readonly>
                            </div>
                        </th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <?php if($priv['canEditTeamMaterial']): ?>
            <button type="button" id="add-non-material-button" class="btn btn-primary"><i class="fa fa-plus"></i> Add Non-material</button>
            <?php endif; ?>
        </div>
        <br>
        <?php endif; ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <?php if(isset($object)): ?>
                    <input type="hidden" name="id" value="<?= $object->id ?>">
                    <?php endif; ?>
                    
                    <?php if($priv['canEditHeader'] || $priv['canEditTermin'] || $priv['canEditTeamMaterial']): ?>
                    <button type="submit" name="smtMakeKontrak" class="btn btn-success pull-right" style="margin-left:5px;" value="Save Kontrak"><i class="fa fa-floppy-o"></i> Save</button>
                    <?php endif; ?>
                    <a href="<?= site_url('subkontrak/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
                </div>
            </div>
        </div>
</form></div>