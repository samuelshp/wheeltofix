<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script>
deleteObject = (id) => {
    return $.ajax({
        url: "<?= site_url('subkontrak/delete') ?>",
        type: 'POST',
        data: {id: id}
    });
}
   
function deleteButtonHandler(e)
{
    swal({
        title: "Warning",
        text: 'Are you sure you want to delete this data? \nthis action will also delete any data related to this data.',
        buttons: {
            cancel: "Cancel",
            delete: {text: "Delete"},
        },
        icon: "warning"
    }).then(data => {
        if(data){
            deleteObject($(this).data('id')).then((resp) => {
                console.log(resp);
                if(resp.status == 'ok'){
                    swal("Success", "Data successfully deleted", "success").then(() => {
                        location.reload();
                    });
                }
            }).fail(function(){
                swal("Error", "fail to delete data", "error");
            });     
        }
    });
}

$(function() {
    $('.main-list-table tbody').on('click', '.delete-object-button', deleteButtonHandler);

});
</script>