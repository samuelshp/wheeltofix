<?php
// $strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>


<div class="col-xs-12">
    <form name="searchForm" method="get" action="<?=site_url('subkontrak/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
        <input type="text" name="txtSearchValue" value="<?=$this->input->get('txtSearchValue')?>" class="form-control" placeholder="Search your keyword here.." />
        <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <?php if($priv['canEditHeader'] || $priv['canEditTermin']): ?>
    <div class="col pull-left" style="text-align:right;">
        <a href="<?=site_url('subkontrak', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
    </div>
    <?php endif; ?>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
    <table class="table table-bordered table-condensed table-hover main-list-table">
        <thead>
            <tr>
                <th>Nama Proyek</th>
                <th>Kode Kontrak</th>
                <th>Pekerjaan</th>
                <th>Tanggal Kontrak</th>
                <th>Tanggal DP</th>
                <th>PM</th>
                <th>Nilai Kontrak</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
    <?php
    // Display data in the table
    if(!empty($subkontrakList)):
        foreach($subkontrakList as $e): ?>
            <tr>
                <td><?=$e['kont_name']?></td>
                <td><?=$e['subkont_kode']?></td>
                <td><?=$e['job'] ?></td>
                <td><?=formatDate2($e['subkont_date']) ?></td>
                <td><?=$e['due_date'] ? formatDate2($e['due_date'], 'd F Y') : '-' ?></td>
                <td><?=$e['adlg_name'] ? $e['adlg_name'] : '-' ?></td>
                <td><?=setPrice($e['job_value'])?></td>
                <td class="action">
                    <a href="<?= site_url('subkontrak/view/'.$e['id']) ?>">
                        <i class="fa fa-eye"></i>
                    </a>                    
                </td>

            </tr>
    <?php
        endforeach;
    else: ?>
            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
        </tbody>
    </table>
    </div>
    <?=$strPage?>
</div>
