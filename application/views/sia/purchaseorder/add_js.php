<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
        dd = '0' + dd;
        } 
        if (mm < 10) {
        mm = '0' + mm;
        } 
        var today = dd + '/' + mm + '/' + yyyy;
        var today
        console.log("PB/"+yyyy+mm+"/0001");

$date = $("#todayDate").val();

var date_baru = $date.split("/"); 
var tahun = date_baru[0].substring(2,4);
console.log(date_baru);

$strRawDate= tahun+"/"+date_baru[1];

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$("#data_lama").hide();
//$("#txtNoPB").prop('disabled',true);
$("#txtNamaProyek").prop('disabled',true);
$("#txtNewItem").prop('disabled',true);
$kontrak_id_now=0;
$("#txtNewItemBonus").prop('disabled',true);
$("#frmAddPurchase").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddPurchase").submit(function() {
    // if($("#selSupp").val() == '') {
    //     alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectsupplier')?>"); return false;
    // }
    if(numberItem<=0){
        alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
    }
    var form = $(this);
    $('.currency').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
	
    return true;
});
/*  Batch process */
$("abbr.list").click(function() {
    if($("#selNewItem").children("option:selected").val() > 0) {
        if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
        else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
        $("#selNewItem").children("option:selected").remove();
    }
});
$("abbr.list").hover(function() {
    if($("#hidNewItem").val() != '') {
        strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
        $(this).attr("title",arrNewItem.length + ' Item(s)');
    }
});
var totalitembonus=0;
var numberitembonus=0;
var selectedItemBonusBefore=[];
var selectedItemBefore=[];
var totalitem=0;
var numberItem=0;
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);
/*$("#IDSupp").val(0);
$("#selSupp").val("All");
$("#InternalSupp").val(0);*/
$("#selSupp").val("");
$("#selSupp").prop('disabled',false);
$("#txtNewItem").prop('disabled',true);
$("#txtNewItemBonus").prop('disabled',true);
$("input[name*='radioSupplier']").change(function(){
   if($(this).val()=='0'){
       $("#selSupp").val("All");
       $("#selSupp").prop('disabled',true);
       $("#txtNewItem").prop('disabled',false);
       $("#txtNewItemBonus").prop('disabled',false);
       $("#IDSupp").val(0);
   }else{
       $("#selSupp").val("");
       $("#selSupp").prop('disabled',false);
       $("#txtNewItem").prop('disabled',true);
       $("#txtNewItemBonus").prop('disabled',true);
   }
   totalitembonus=0;
   numberitembonus=0;
   selectedItemBonusBefore=[];
   selectedItemBefore=[];
   totalitem=0;
   numberItem=0;
});

//Untuk nampilin data bagian nama proyek

$('#selProyek').change(function(){
    console.log($('#selProyek').val());
    $("#kontrakID").val($('#selProyek').val());
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + $('#selProyek').val(),
        cache : false,
        success : function(result){
            $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result.data, function (index,arr) {
                console.log(JSON.stringify(arr['id']));
                $('#selPekerjaan').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
            });
            $("#selPekerjaan").trigger("chosen:updated");
        }
    });
});

$('#selPekerjaan').change(function(){
    $("#subKontrakID").val($('#selPekerjaan').val());
    console.log($("#subKontrakID").val());
});

$('#selProduct').change(function(){
    console.log($("#selProduct_chosen").children(".chosen-single").text());
    $("#selProduct_chosen").children(".chosen-single").val("Select your option");
});

$('#selProduct').change(function(){    
    var id_subkon = $('#subKontrakID').val();
    var id_product = $('#selProduct').val();
    // loadBarangDibeli(id_subkon, id_product);
    $.ajax({       
        url: "<?=site_url('purchase_order_ajax/checkBahanToSubkontrakMaterial', NULL, FALSE)?>/"+id_subkon+"/"+id_product,
        success: function(data){
            console.log(data);
            var xmlDoc = $.parseXML(data);
            console.log(xmlDoc);
            console.log('Diatas isi dari check bahan');
            var xml = $(xmlDoc);
            $arrSelectedBahan = xml;
            var display=[];
            $.map(xml.find('Bahan').find('item'),function(val,i){
                var idsubkontrak = $(val).find('subkontrak_id').text();
                //$("#subKontrakID").val(idsubkontrak);
                var bahan = $(val).find('prod_title').text();
                var satuan = $(val).find('unit_title').text();
                var idprod = $(val).find('material').text();
                if(idsubkontrak == ''){
                    console.log('Kosong');
                    $(".info").hide();
                    $.ajax({
                        url: "<?=site_url('purchase_order_ajax/getBahanByIdProduct', NULL, FALSE)?>/" + $('#selProduct').val(),
                        success: function(data){
                            var xmlDoc = $.parseXML(data);
                            console.log(xmlDoc);
                            var xml = $(xmlDoc);
                            $arrSelectedCust = xml;
                            var display=[];
                            $.map(xml.find('Bahan').find('item'),function(val,i){
                                var bahan = $(val).find('prod_title').text();
                                var satuan = $(val).find('title_pb').text();
                                var terima = $(val).find('title_terima').text();
                                var intID =  $('#selProduct').val();
                                $("#selectedBahans tbody").append(
                                '<tr>'+
                                    '<td class="cb"><input type="checkbox" name="cbDeleteX'+totalitem+'"/></td>'+
                                    '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+intID+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                                    // '<td><input type="number" step="0.0001" name="isiTebal'+totalitem+'" id="isiTebal'+totalitem+'"></td>'+
                                    // '<td><input type="number" step="0.0001" name="isiLebar'+totalitem+'" id="isiLebar'+totalitem+'"></td>'+
                                    // '<td><input type="number" step="0.0001" name="isiPanjang'+totalitem+'" id="isiPanjang'+totalitem+'"></td>'+
                                    '<td id="isiQty'+totalitem+'"><input type="number" step="0.0001" id="inputQty'+totalitem+'" value="" placeholder="0" name="inputQty'+totalitem+'"><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="0"></td>'+
                                    '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="0"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                                    '<td id="isiQtyTerima'+totalitem+'"><input type="number" step="0.0001" id="inputQtyTerima'+totalitem+'" value="" placeholder="0" name="inputQtyTerima'+totalitem+'"></td>'+
                                    '<td id="isiSatuanTerima'+totalitem+'"><input type="hidden" name="isiSatuanTextTerima'+totalitem+'" id="isiSatuanTextTerima'+totalitem+'" value="'+terima+'">'+terima+'</td>'+
                                    '<td class="ket"><input type="text" id="isiKeterangan'+totalitem+'" name="isiKeterangan'+totalitem+'"/></td>'+
                                    '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="0">'+
                                '</tr>');
                                $("#totalItem").val(totalitem);
                                console.log($("#totalItem").val()+"jumlah item di table");
                                $('#txtNewItem').val('');
                                totalitem++;
                                numberItem++;
                            });
                        }
                    });
                }
                else{
                    console.log(idsubkontrak);
                    $.ajax({
                        type:"POST",
                        data:{
                            bahanid : $('#selProduct').val(),
                            idsubkon : idsubkontrak
                        },
                        url: "<?=site_url('purchase_order_ajax/getAllBahanAutoCompleteById', NULL, FALSE)?>",
                        beforeSend: function() {
                            $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                        },
                        complete: function() {
                            $("#loadItem").html('');
                        },
                        success: function(data){
                            var xmlDoc = $.parseXML(data);
                            console.log(xmlDoc);
                            console.log('Diatas isi dari bahan yang ada subkontrak idnya');
                            var xml = $(xmlDoc);
                            $arrSelectedBahan = xml;
                            $.map(xml.find('Bahan').find('item'),function(val,i){
                                var idsubkontrak = $(val).find('subkontrak_id').text();
                                var terpb_now = $(val).find('subkontrak_terpb').text();
                                var terima = $(val).find('title_terima').text();
                                var qty = $(val).find('qty').text();
                                $(".info").hide();
                                $("#selectedBahans tbody").append(
                                '<tr>'+
                                    '<td class="cb"><input type="checkbox" name="cbDeleteX'+totalitem+'"/></td>'+
                                    '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+idprod+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                                    // '<td><input type="number" step="0.0001" name="isiTebal'+totalitem+'" id="isiTebal'+totalitem+'"></td>'+
                                    // '<td><input type="number" step="0.0001" name="isiLebar'+totalitem+'" id="isiLebar'+totalitem+'"></td>'+
                                    // '<td><input type="number" step="0.0001" name="isiPanjang'+totalitem+'" id="isiPanjang'+totalitem+'"></td>'+
                                    '<td id="isiQty'+totalitem+'"><input type="number" step="0.0001" id="inputQty'+totalitem+'" placeholder="'+qty+'"  value="" name="inputQty'+totalitem+'"><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="'+qty+'"></td>'+
                                    '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="'+idsubkontrak+'"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                                    '<td id="isiQtyTerima'+totalitem+'"><input type="number" step="0.0001" id="inputQtyTerima'+totalitem+'" value="" placeholder="0" name="inputQtyTerima'+totalitem+'"></td>'+
                                    '<td id="isiSatuanTerima'+totalitem+'"><input type="hidden" name="isiSatuanTextTerima'+totalitem+'" id="isiSatuanTextTerima'+totalitem+'" value="'+terima+'">'+terima+'</td>'+
                                    '<td class="ket"><input type="text" id="isiKeterangan'+totalitem+'" name="isiKeterangan'+totalitem+'"/></td>'+
                                    '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="'+terpb_now+'">'+
                                '</tr>');
                                $("#totalItem").val(totalitem);
                                console.log($("#totalItem").val()+"jumlah item di table");
                                totalitem++;
                                numberItem++;
                            })
                        }
                    });
                }
            });
        }
    });
});

if( <?php echo $_SESSION['strAdminID'] ?> == 5 || <?php echo $_SESSION['strAdminID'] ?> == 2 || <?php echo $_SESSION['strAdminID'] ?> == 6 || <?php echo $_SESSION['strAdminID'] ?> == 1){
    $("#proyekName").autocomplete({
        minLength:3,
        delay:500,
        source: function(request,response) {
            $.ajax({
                type : "POST",
                dataType:'json',
                url: "<?=site_url('purchase_order_ajax/getNamaProyek', NULL, FALSE)?>/" + $("#proyekName").val() +"/"+ <?php echo $_SESSION['strAdminID'] ?>,
                beforeSend: function() {
                    $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                },
                complete: function() {
                    $("#loadItem").html('');
                },
                success: function(data){
                    var xmlDoc = $.parseXML(data);
                    console.log(xmlDoc);
                    var xml = $(xmlDoc);
                    $arrSelectedCust = xml;
                    var display=[];
                    $.map(xml.find('NamaProyek').find('item'),function(val,i){
                        var name = $(val).find('kont_name').text();
                        var code = $(val).find('kont_code').text();
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ name, value:"("+code+") "+ name,id:intID});
                    });
                    response(display);
			    }
            });
        },
        select: function(event, ui) {
            var selectedCust = $.grep($arrSelectedCust.find('NamaProyek').find('item'), function(el) {
                return $(el).find('id').text() == ui.item.id;
            });
            console.log($(selectedCust).find('id').text()+" hhaahha");
            $kontrak_id_now = $(selectedCust).find('id').text();
            $kontrak_code = $(selectedCust).find('kont_code').text();
            $('#kontrakID').val($kontrak_id_now);
            $("#kontrakCode").val($kontrak_code);
            console.log($('#kontrakID').val());
            console.log($('#kontrakCode').val());
        }
    })

    $("#pekerjaanName").autocomplete({
        minLength:3,
        delay:500,
        source: function(request,response) {
            $.ajax({
                type:"POST",
                data:{
                    pekerjaanName : $("#pekerjaanName").val(),
                    adminID : <?php echo $_SESSION['strAdminID'] ?>,
                    idKontrak: $('#kontrakID').val()
                },
                url: "<?=site_url('purchase_order_ajax/getNamaPekerjaan', NULL, FALSE)?>",
                beforeSend: function() {
                    $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                },
                complete: function() {
                    $("#loadItem").html('');
                },
                success: function(data){
                    var xmlDoc = $.parseXML(data);
                    console.log(xmlDoc);
                    console.log("Atas pekerjaan");
                    var xml = $(xmlDoc);
                    $arrSelectedCust = xml;
                    var display=[];
                    $.map(xml.find('NamaPekerjaan').find('item'),function(val,i){
                        var name = $(val).find('job').text();
                        var code = $(val).find('kont_code').text();
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ name, value:"("+code+") "+ name,id:intID});
                    });
                    response(display);
			    }
            });
        },
        select: function(event, ui) {
            var selectedCust = $.grep($arrSelectedCust.find('NamaPekerjaan').find('item'), function(el) {
                return $(el).find('id').text() == ui.item.id;
            });
            console.log($(selectedCust).find('id').text()+" hhaahha");
            $subkontrak_id_now = $(selectedCust).find('id').text();
            $kontrak_code = $(selectedCust).find('kont_code').text();
            $('#subKontrakID').val($subkontrak_id_now);
            console.log($('#subKontrakID').val());
        }
    })

}
else{
    $("#proyekName").autocomplete({
        minLength:3,
        delay:500,
        source: function(request,response) {
            $.ajax({
                url: "<?=site_url('purchase_order_ajax/getNamaProyek', NULL, FALSE)?>/" + $("#proyekName").val() +"/"+ <?php echo $_SESSION['strAdminID'] ?>,
                beforeSend: function() {
                    $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                },
                complete: function() {
                    $("#loadItem").html('');
                },
                success: function(data){
                    var xmlDoc = $.parseXML(data);
                    console.log(xmlDoc);
                    var xml = $(xmlDoc);
                    $arrSelectedCust = xml;
                    var display=[];
                    $.map(xml.find('NamaProyek').find('item'),function(val,i){
                        var name = $(val).find('kont_name').text();
                        var code = $(val).find('kont_code').text();
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ name, value:"("+code+") "+ name,id:intID});
                    });
                    response(display);
			    },
                select: function(event, ui) {
                    var selectedCust = $.grep($arrSelectedCust.find('NamaProyek').find('item'), function(el) {
                        return $(el).find('id').text() == ui.item.id;
                    });
                    console.log($(selectedCust).find('id').text()+" hhaahha");
                    $kontrak_id_now = $(selectedCust).find('id').text();
                    $kontrak_code = $(selectedCust).find('kont_code').text();
                    $('#kontrakID').val($kontrak_id_now);
                    $("#kontrakCode").val($kontrak_code);
                    console.log($('#kontrakID').val());
                    console.log($('#kontrakCode').val());
                }
            });
        }
    })

    $("#pekerjaanName").autocomplete({
        minLength:3,
        delay:500,
        source: function(request,response) {
            $.ajax({
                type:"POST",
                data:{
                    pekerjaanName : $("#pekerjaanName").val(),
                    adminID : <?php echo $_SESSION['strAdminID'] ?>,
                    idKontrak: $('#kontrakID').val()
                },
                url: "<?=site_url('purchase_order_ajax/getNamaPekerjaan', NULL, FALSE)?>",
                beforeSend: function() {
                    $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                },
                complete: function() {
                    $("#loadItem").html('');
                },
                success: function(data){
                    var xmlDoc = $.parseXML(data);
                    console.log(xmlDoc);
                    console.log("Atas pekerjaan");
                    var xml = $(xmlDoc);
                    $arrSelectedCust = xml;
                    var display=[];
                    $.map(xml.find('NamaPekerjaan').find('item'),function(val,i){
                        var name = $(val).find('job').text();
                        var code = $(val).find('kont_code').text();
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ name, value:"("+code+") "+ name,id:intID});
                    });
                    response(display);
			    }
            });
        },
        select: function(event, ui) {
            var selectedCust = $.grep($arrSelectedCust.find('NamaPekerjaan').find('item'), function(el) {
                return $(el).find('id').text() == ui.item.id;
            });
            console.log($(selectedCust).find('id').text()+" hhaahha");
            $subkontrak_id_now = $(selectedCust).find('id').text();
            $kontrak_code = $(selectedCust).find('kont_code').text();
            $('#subKontrakID').val($subkontrak_id_now);
            console.log($('#subKontrakID').val());
        }
    })
}
//Nampilin pekerjaan apabila sudah pilih nama proyek
$('#namaProyek').on('change', function(){
    $kontrak_id_now = $(this).find('option:selected').attr('id');
    $('#kontrakID').val($kontrak_id_now);
    console.log($kontrak_id_now+"hahaha");
    $('#namaPekerjaan')
        .find('option')
        .remove()
        .end()
        .append('<option value="pilih_pekerjaan">Pilih pekerjaan</option>')
    ;
    $.ajax({
        url: "<?=site_url('purchase_order_ajax/getNamaPekerjaan', NULL, FALSE)?>/"+this.value+"/"+customerCode+"/"+$kontrak_id_now,//sementara pembangunan
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            $.map(xml.find('NamaPekerjaan').find('item'),function(val,i){
                if($lol = 'Tidak dipakai'){
                    // var credit = $(val).find('invo_grandtotal').text();
                    // var limit = parseFloat($("#customerLimit").autoNumeric('get'))-parseFloat(credit);
                    // if(limit > 0) {
                    //    $("#customerLimit").autoNumeric('set',limit);
                    // } else {
                    //    $("#customerLimit").autoNumeric('set',0);
                    // }
                }
                //var id_kontrak = $(val).find('id').text();
                console.log(xmlDoc);
                var nama_kontrak = $(val).find('kont_job').text();
                var subkontrakID = $(val).find('id').text();
                var code_kontrak = $(val).find('kont_code').text();
                $("#namaPekerjaan").append($('<option></option').val(nama_kontrak).html(nama_kontrak+" - "+code_kontrak));
                $("#kontrakID").val($kontrak_id_now);
                $("#subKontrakID").val(subkontrakID);
                console.log($("#subKontrakID").val());
            });
        }
    });
});

//Baru

var intID='';
var customerCode="";

console.log($("#ownerName").val());

$("#ownerName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
            url: "<?=site_url('purchase_order_ajax/getOwnerData', NULL, FALSE)?>/" + $("#ownerName").val(),
            beforeSend: function() {
                $("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadCustomer").html('');
            },
			success: function(data){
                var xmlDoc = $.parseXML(data);
                console.log(xmlDoc);
				var xml = $(xmlDoc);
				$arrSelectedCust = xml;
				var display=[];
				$.map(xml.find('Owner').find('item'),function(val,i){
					var name = $(val).find('cust_name').text();
                        // var address = $(val).find('cust_address').text();
                        // var city = $(val).find('cust_city').text();
                    var code = $(val).find('cust_code').text();
                        //var outlettype = $(val).find('cust_outlettype').text();
                        //var outlettypeStr = '';
                        // switch (outlettype) {
                            // 	case '1': outlettypeStr = 'BARU'; break;
                            // 	case '2': outlettypeStr = 'BIASA'; break;
                            // 	case '3': outlettypeStr = 'MEMBER'; break;
                            // 	case '4': outlettypeStr = 'RESELLER'; break;
                            // 	case '5': outlettypeStr = 'KANTOR'; break;
                            // 	default : outlettypeStr = 'N/A';
                        // }

                        //var strName=name+", "+ address +", "+city+", "+outlettypeStr;
                    intID = $(val).find('id').text();
                    console.log(xmlDoc);
					display.push({label:"("+code+") "+ name, value:"("+code+") "+ name,id:intID});
				});
                response(display);
			}
		});
	},
	select: function(event, ui) {
        var selectedCust = $.grep($arrSelectedCust.find('Owner').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        console.log($(selectedCust).find('cust_code').text());
        var isCustomerTunai = false;
        if($("input[type='radio'][name='radioCustomer']:checked").val() == '0') {
            isCustomerTunai = true;
        }
        
        if($lol = ''){
            // $("#customerID").val($(selectedCust).find('id').text());
            // $("#customerAddress").val($(selectedCust).find('cust_address').text());
            // $("#customerCity").val($(selectedCust).find('cust_city').text());
            // $("#customerPhone").val($(selectedCust).find('cust_phone').text());
            // $("#customerOutletType").val($(selectedCust).find('cust_outlettype').text());
            // if(isCustomerTunai) {
            //   $("#customerLimit").autoNumeric('set',$(selectedCust).find('cust_limitkredit').text());
            // } else {
            //   $("#customerLimit").autoNumeric('set',0);
            // }
            // var deposit = $(selectedCust).find('cust_deposit').text();
            // $("#customerDeposit").autoNumeric('set',deposit);
            // if(deposit > 0) {
            //     $('input[name=intTipeBayar][value=5]').attr('disabled', false);
            // }
            // $("#txtJatuhTempo").val($(selectedCust).find('cust_jatuhtempo').text());
            // $('input[name="outletMarketType"]').val($(selectedCust).find('cust_markettype').text());
            // internal=parseInt($(selectedCust).find('cust_internal').text());
            /* disini ajax e */
        }//gak dipakai

        //Bawah untuk manggil nama proyek
        //if(!isCustomerTunai) {//Ini untuk apa?
        var nama_proyek='';
        customerCode = $(selectedCust).find('id').text();
        $.ajax({
            url: "<?=site_url('purchase_order_ajax/getNamaProyek', NULL, FALSE)?>/" + $(selectedCust).find('id').text(),
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $.map(xml.find('NamaProyek').find('item'),function(val,i){
                    if($lol = 'Tidak dipakai'){
                        // var credit = $(val).find('invo_grandtotal').text();
                        // var limit = parseFloat($("#customerLimit").autoNumeric('get'))-parseFloat(credit);
                        // if(limit > 0) {
                        //    $("#customerLimit").autoNumeric('set',limit);
                        // } else {
                        //    $("#customerLimit").autoNumeric('set',0);
                        // }
                    }
                    var id_kontrak = $(val).find('id').text();
                    console.log(id_kontrak);
                    var nama_kontrak = $(val).find('kont_name').text();
                    $("#namaProyek").append($('<option id='+id_kontrak+'></option>').val(nama_kontrak).html(nama_kontrak));
                });
            },
            select: function(event, ui) {
                var selectedProyek = $('#namaProyek').val();
                //ajax untuk dapatkan nama pekerjaan
            }
        });

            // $('#namaProyek').on('change', function(){
            //     $kontrak_id_now = $(this).find('option:selected').attr('id');
            //     $('#kontrakID').val($kontrak_id_now);
            //     console.log($kontrak_id_now+"hahaha");
            //     $('#namaPekerjaan')
            //         .find('option')
            //         .remove()
            //         .end()
            //         .append('<option value="pilih_pekerjaan">Pilih pekerjaan</option>')
            //     ;
            //     $.ajax({
            //         url: "<?=site_url('purchase_order_ajax/getNamaPekerjaan', NULL, FALSE)?>/"+this.value+"/"+customerCode+"/"+$kontrak_id_now,//sementara pembangunan
            //         success: function(data){
            //             var xmlDoc = $.parseXML(data);
            //             var xml = $(xmlDoc);
            //             $.map(xml.find('NamaPekerjaan').find('item'),function(val,i){
            //                 if($lol = 'Tidak dipakai'){
            //                     // var credit = $(val).find('invo_grandtotal').text();
            //                     // var limit = parseFloat($("#customerLimit").autoNumeric('get'))-parseFloat(credit);
            //                     // if(limit > 0) {
            //                     //    $("#customerLimit").autoNumeric('set',limit);
            //                     // } else {
            //                     //    $("#customerLimit").autoNumeric('set',0);
            //                     // }
            //                 }
            //                 //var id_kontrak = $(val).find('id').text();
            //                 console.log(xmlDoc);
            //                 var nama_kontrak = $(val).find('kont_job').text();
            //                 var subkontrakID = $(val).find('id').text();
            //                 var code_kontrak = $(val).find('kont_code').text();
            //                 $("#namaPekerjaan").append($('<option></option').val(nama_kontrak).html(nama_kontrak+" - "+code_kontrak));
            //                 $("#kontrakID").val($kontrak_id_now);
            //                 $("#subKontrakID").val(subkontrakID);
            //                 console.log($("#subKontrakID").val());
            //             });
            //         }
            //     });
            // });
        
        //}

        if($lol = 'Tidak dipake'){
            var outlettypeStr = '';
            switch ($(selectedCust).find('cust_outlettype').text()) {
                case '1': outlettypeStr = 'BARU'; break;
                case '2': outlettypeStr = 'BIASA'; break;
                case '3': outlettypeStr = 'MEMBER'; break;
                case '4': outlettypeStr = 'RESELLER'; break;
                case '5': outlettypeStr = 'KANTOR'; break;
                default : outlettypeStr = 'N/A';
            }
            $("#customerOutletTypeStr").val(outlettypeStr);
            $('input[name="customerOutletType"]').val($(selectedCust).find('cust_outlettype').text());
            $('input[name="customerID"]').val($(selectedCust).find('id').text());
            //Tidak dipake
                // if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
                //     if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#ownerName").val()!=''){
                //         $("#txtNewItem").prop('disabled',false);
                //         $("#txtNewItemBonus").prop('disabled',false);
                //     }else{
                //         $("#txtNewItem").prop('disabled',true);
                //         $("#txtNewItemBonus").prop('disabled',true);
                //     }
                // }else{
                //     if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerID").val()!=''){
                //         $("#txtNewItem").prop('disabled',false);
                //         $("#txtNewItemBonus").prop('disabled',false);
                //     }else{
                //         $("#txtNewItem").prop('disabled',true);
                //         $("#txtNewItemBonus").prop('disabled',true);
                //     }
                // }
                //selectedItemBefore=[];
                //selectedItemBonusBefore=[];
                //numberItem=0;
                //numberItemBonus=0;
                //strAllowedBonusID='';
                //$("#strBonusItem").html('');
                //$("#strBonusNotInclude").html('');
                //$("#selectedItems tbody").html('');
                //$("#selectedItemsBonus tbody").html('');
                //$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
                //$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        }
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
    }
});



$("#txtNew").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_order_ajax/getAllBahanAC', NULL, FALSE)?>/"+ $("#txtNew").val(),
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                console.log(xmlDoc);
                var xml = $(xmlDoc);
                $arrSelectedBahan = xml;
                var display=[];
                $.map(xml.find('Bahan').find('item'),function(val,i){
                    //if($(val).find('id').text() == $('#kontrakID').val()){
                    // var bahan = $(val).find('prod_title').text();
                    // var idProduct = $(val).find('id').text();
                    // var unit = $(val).find('unit_title').text();
                    // var unit_material_id = $(val).find('munit_material_id').text();
                    // var unit_id = $(val).find('munit_unit_id').text();
                    // var unit_conversion = $(val).find('unit_conversion').text();
                    // var qty = $(val).find('prod_qty1').text();
                        var idProd = $(val).find('id').text();
                        var bahan = $(val).find('prod_title').text();
                        var satuan = $(val).find('unit_title').text();
                        display.push({label:"("+bahan+") - "+ satuan, id:idProd});
                        numberItem++;
                    //}
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedBahan = $.grep($arrSelectedBahan.find('Bahan').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var bahan = $(selectedBahan).find('prod_title').text();
        var satuan = $(selectedBahan).find('unit_title').text();
        var idprod = $(selectedBahan).find('id').text();
        //console.log(koncode2);
        //Ajax bawah untuk check apakah ada atau tidak di subkontrak material
        $.ajax({
            type:"POST",
            data:{
                idkon : $('#kontrakID').val(),
                idprod2 : idprod
            },
            url: "<?=site_url('purchase_order_ajax/checkBahanToSubkontrakMaterial', NULL, FALSE)?>",
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                console.log(xmlDoc);
                console.log('Diatas isi dari check bahan');
                var xml = $(xmlDoc);
                $arrSelectedBahan = xml;
                var display=[];
                $.map(xml.find('Bahan').find('item'),function(val,i){
                    var idsubkontrak = $(val).find('subkontrak_id').text();
                    if(idsubkontrak == ''){
                        console.log('Kosong');
                        $(".info").hide();
                        $("#selectedBahans tbody").append(
                        '<tr>'+
                            '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+idprod+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                            '<td id="isiQty'+totalitem+'"><input type="text" id="inputQty'+totalitem+'" value="0" name="inputQty'+totalitem+'"><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="0"></td>'+
                            '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="0"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                            '<td id="dimensi'+totalitem+'"><input type="text" name="isiDimensi'+totalitem+'" id="isiDimensi'+totalitem+'" value="1"></td>'+
                            '<td id="total'+totalitem+'"><input type="text" name="isiTotal'+totalitem+'" id="isiTotal'+totalitem+'" value="" readonly></td>'+
                            '<td class="ket"><input type="text" id="isiKeterangan'+totalitem+'" name="isiKeterangan'+totalitem+'"/></td>'+
                            '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="0">'+
                        '</tr>');
                        $("#totalItem").val(totalitem);
                        console.log($("#totalItem").val()+"jumlah item di table");
                        $('#txtNewItem').val('');
                        totalitem++;
                    }
                    else{
                        console.log(idsubkontrak);
                        $.ajax({
                            type:"POST",
							data:{
								bahan2 : bahan,
								idsubkon : idsubkontrak
							},
                            url: "<?=site_url('purchase_order_ajax/getAllBahanAutoComplete', NULL, FALSE)?>",
                            beforeSend: function() {
                                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                            },
                            complete: function() {
                                $("#loadItem").html('');
                            },
                            success: function(data){
                                var xmlDoc = $.parseXML(data);
                                console.log(xmlDoc);
                                console.log('Diatas isi dari bahan yang ada subkontrak idnya');
                                var xml = $(xmlDoc);
                                $arrSelectedBahan = xml;
                                $.map(xml.find('Bahan').find('item'),function(val,i){
                                    var idsubkontrak = $(val).find('subkontrak_id').text();
                                    var terpb_now = $(val).find('subkontrak_terpb').text();
                                    var qty = $(val).find('qty').text();
                                    $(".info").hide();
                                    $("#selectedBahans tbody").append(
                                    '<tr>'+
                                        '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+idprod+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                                        '<td id="isiQty'+totalitem+'"><input type="text" id="inputQty'+totalitem+'" value="'+qty+'" name="inputQty'+totalitem+'"><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="'+qty+'"></td>'+
                                        '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="'+idsubkontrak+'"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                                        '<td id="dimensi'+totalitem+'"><input type="text" name="isiDimensi'+totalitem+'" id="isiDimensi'+totalitem+'" value="1"><input type="hidden" name="isiLebar'+totalitem+'" id="isiLebar'+totalitem+'" value="sesuatu"></td>'+
                                        '<td id="total'+totalitem+'"><input type="text" name="isiTotal'+totalitem+'" id="isiTotal'+totalitem+'" value="" readonly></td>'+
                                        '<td class="ket"><input type="text" id="isiKeterangan'+totalitem+'" name="isiKeterangan'+totalitem+'"/></td>'+
                                        '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="'+terpb_now+'">'+
                                    '</tr>');
                                    $("#totalItem").val(totalitem);
                                    console.log($("#totalItem").val()+"jumlah item di table");
                                    totalitem++;
                                })
                            }
                        });
                    }
                });
            }
            
        });
        $('#txtNew').val(''); return false;
        // if(numberItem=='0'){
        //     $("#selectedBahans tbody").html('');   
        // }
        // $(".info").hide(); // Clear the textbox
    }
});

//Baru

$("#selSupp").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#selSupp").val(),
            beforeSend: function() {
                $("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadSupp").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Supplier').find('item'),function(val,i){
                    var name = $(val).find('supp_name').text();
                    var address = $(val).find('supp_address').text();
                    var city = $(val).find('supp_city').text();
                    var code = $(val).find('supp_code').text();
                    var strName=name+", "+ address +", "+city;
                    var intID = $(val).find('id').text();
                    display.push({label:"("+code+") "+ strName, value:"("+code+") "+ strName,id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        $("#IDSupp").val($(selectedPO).find('id').text());
        $("#InternalSupp").val($(selectedPO).find('supp_internal').text());
        totalitembonus=0;
        numberitembonus=0;
        selectedItemBonusBefore=[];
        selectedItemBefore=[];
        totalitem=0;
        numberItem=0;
        $("#selectedItems tbody").html('');
        $("#selectedItemsBonus tbody").html('');
        $("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadSupp").html('<i class="fa fa-check"></i>');
        if($("#WarehouseID").val()!='' && $("#IDSupp").val()!='' ){
            $("#txtNewItem").prop('disabled',false);
            $("#txtNewItemBonus").prop('disabled',false);
        }else{
            $("#txtNewItem").prop('disabled',true);
            $("#txtNewItemBonus").prop('disabled',true);
        }
    }
});
$("#warehouseName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#warehouseName").val(),
            beforeSend: function() {
                $("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadWarehouse").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedWare = xml;
                var display=[];
                $.map(xml.find('Warehouse').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var name = $(val).find('ware_name').text();
                    display.push({label: name, value: name, id:intID});
                });
                if(display.length == 1) {
                    setTimeout(function() {
                        $("#warehouseName").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                        $('#warehouseName').autocomplete('close');
                    }, 100);
                }
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedWare = $.grep($arrSelectedWare.find('Warehouse').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        $("#WarehouseID").val($(selectedWare).find('id').text());
        if($("#WarehouseID").val()!='' && $("#IDSupp").val()!='' ){
            $("#txtNewItem").prop('disabled',false);
            $("#txtNewItemBonus").prop('disabled',false);
        }else{
            $("#txtNewItem").prop('disabled',true);
            $("#txtNewItemBonus").prop('disabled',true);
        }
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
    }
});
setTimeout(function() {
    $("#warehouseName").val(primaryWarehouseName);
    $("#warehouseName").autocomplete('search');
},500);

$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItem"]').val() + "/" + $("#InternalSupp").val(),
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    var type=$(val).find('prod_type').text();
                    if($.inArray(intID, selectedItemBefore) > -1 || type == '2'){

                    }else{
                        display.push({label: "("+strKode+") "+strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalitem;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
        if(numberItem=='0'){
            $("#selectedItems tbody").html('');   
        }
        $("#selectedItems tbody").append(
'<tr>'+
    '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
    '<td class="qty"><div class="form-group">'+
        '<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
        '<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
        '<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
    '</div></td>'+
    '<td id="nmeItemX'+i+'"></td>'+
    '<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
    '<td class="disc"><div class="form-group">'+
        '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
        '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
        '<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
    '</div></td>'+
    '<td id="subTotalX'+i+'">0</td>'+
    '<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
    '<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
    '<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
    '<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
    '<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
    '<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
    '<input type="hidden" id="conv1UnitX'+i+'" >' +
    '<input type="hidden" id="conv2UnitX'+i+'" >' +
    '<input type="hidden" id="conv3UnitX'+i+'" >'+
'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        var id=$(selectedPO).find('id').text();
        selectedItemBefore.push(id);
        $("#nmeItemX"+i).text("("+prodcode+") "+strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
        $("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
        $("#idItemX"+i).val(id);
        $("#prodItemX"+i).val(prodtitle);
        $("#probItemX"+i).val(probtitle);
        $("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        $("#qty1ItemX"+i).hide();
        $("#qty2ItemX"+i).hide();
        $("#qty3ItemX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemX"+i).show();
                    $("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
                    $("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
                }
            }
        });
        totalitem++;
        numberItem++;
        $("#totalItem").val(totalitem);

        $('#txtNewItem').val(''); return false; // Clear the textbox
    }
});

$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItemBonus"]').val() + "/" + $("#InternalSupp").val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1){

                    }else{
                        display.push({label: "("+strKode+") "+strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalitembonus;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
        if(numberitembonus=='0'){
            $("#selectedItemsBonus tbody").html('');
        }
        $("#selectedItemsBonus tbody").append(
'<tr>'+
    '<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
    '<td class="qty"><div class="form-group">'+
        '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
    '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
    '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
    '</div></td>'+
    '<td><label id="nmeItemBonusX'+i+'"></label></td>'+
    '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
    '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
    '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
    '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
    '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
    '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        var id=$(selectedPO).find('id').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text("("+prodcode+") "+strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemBonusX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
        $("#idItemBonusX"+i).val(id);
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
                    $("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
                }
            }
        });
        totalitembonus++;
        numberitembonus++;
        $("#totalItemBonus").val(totalitembonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
    }
});



$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemX"+at).val()<0){
            $("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
        }
    }
    if($("#prcItemX"+at).autoNumeric('get')<0){
        $("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
    }
    var previous=$("#subTotalX"+at).autoNumeric('get');
    var price = $("#prcItemX"+at).autoNumeric('get');
    var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
    /* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

    for(var zz=1;zz<=3;zz++){
        subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
    }
    $("#subTotalX"+at).autoNumeric('set',subtotal);
    var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
    $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }
});
$("#dsc4Item").change(function(){
    var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
    var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
    if(totalwithdisc<=0) {
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    } else {
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }
});
$("#txtTax").change(function(){
    var totalwithdisc=$("#subTotalWithDisc").autoNumeric('get');
    var totalTax=parseFloat(totalwithdisc)+parseFloat(totalwithdisc*$("#txtTax").val()/100);
    $("#subTotalTax").autoNumeric('set',totalTax);
});
$("#selectedBahans").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    // var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    //var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
    //$("#subTotalNoTax").autoNumeric('set',totalNoTax);
    // var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
    // if(totalwithdisc<=0){
    //     $("#subTotalWithDisc").autoNumeric('set','0');
    //     $("#subTotalTax").autoNumeric('set','0');
    // }else{
    //     $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
    //     var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
    //     $("#subTotalTax").autoNumeric('set',totalTax);
    // }
    console.log(totalitem+"sebelum");
    console.log(numberItem+"sebelum");
    totalitem--;
    numberItem--;
    console.log(totalitem+"sesudah");
    console.log(numberItem+"sesudah");
    if(numberItem=='0'){
        $("#selectedBahans tbody").append(
            '<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    // selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
    //     return value != $("#idItemX"+at).val();
    // });
    $(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberitembonus--;
    if(numberitembonus=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});

$(document).on('click', '[id^=nmeItem] .fa-question-circle', function() {
    var $this = $(this);
    $.ajax({
        url: "<?=site_url('purchase_ajax/getLastProductPrice')?>",
        data: {
            product_id: $this.closest('tr').find('[id^=idItem]').val(),
        },
        dataType: 'json',
        success: function(data) {
            var msg = "Harga Pembelian Terakhir: \n\n";
            $.each(data.Items, function() {
                msg = msg + this.strDate + ' - ' + this.supp_name + ' - ' + this.strPrice + "\n";
            });
            alert(msg);
        }
    });
});

$('.chosen').chosen({search_contains: true});

function loadBarangDibeli(id_subkon, id_product) {
    $.ajax({
        url: "<?=site_url('api/bahan_subkontrak_material/list?h="+userHash+"&filter_material="+id_product+"&filter_subkontrak_id="+id_subkon+"', NULL, FALSE)?>",
        dataType: "json",
        method: "GET",
        success: function(result) {
            console.log(result.data);
        },
        async: false
    });
}
});</script>