<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.$strPageTitle, 'link' => '')
);

//echo $_SESSION['strAdminID'];

//echo $_SESSION['strAdminPriviledge'];

$date = date('Y/m/d H:i:s');

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<!-- Baru dibawah -->
<div class="col-xs-12"><?php
$strSearchAction = site_url('purchase_order/browse', NULL, FALSE);

include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchpb.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('purchase_order', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead>
    <tr>
        <th>No. PB</th>
        <th>Tanggal PB</th>
        <th>Nama Proyek</th>
        <th>Nama Pekerjaan</th>
        <th>Kode subkontrak</th>
        <th>Approval Cost Control</th>
        <th>Approval PM</th>
        <th>No. OP</th>
        <th class="action">View Detail</th>
    </tr></thead>
    <tbody><?php

// Display data in the table
if(!empty($arrPurchase)):
    $i=0;
    foreach($arrPurchase as $e): 
        $i++;?>
        <tr style="text-align:center;">
            <td><?=$e['pror_code'] //diisi field nomor pb ?></td> 
            <td><?=formatDate2($e['cdate'],'d F Y')?></td>
            <td><?=$e['kont_name']//diisi field nomor kontrak?></td>
            <td><?=$e['kont_job']?></td>
            <td><?=$e['subkont_kode']?></td>
            <td><?=$e['approvalCC']//status apakah sudah disetujui atau belum?></td>
            <td><?=$e['approvalPM']//diisi field approval pm  ?></td>
            <td>
                <?php
                    echo "<script>console.log('".$arrAccPO[$i]."');</script>";
                    if($arrAccPO[$i] == "ada"){
                        // echo $e['prch_code'] ;
                        echo "<span class='glyphicon glyphicon-ok'></span>";
                    }
                    else{
                        if($e['pror_status'] == 4) echo "Closed";                    
                        else echo "Belum ada";
                    }
                    //echo "";
                ?>
                <!--//$e['']//diisi field nomor PO --></td>
            <td class="action">
                <?php if($e['bolAllowView']): ?><a href="<?=site_url('purchase_order/view/'.$e['id'], NULL, FALSE)?>" ><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
                <?php if($e['bolBtnPrint']): ?><a href="<?=site_url('purchase_order/view/'.$e['id'].'?print=true', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>  
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>

<!-- Lama -->
    <!-- <?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-phone"></i> '.$strPageTitle, 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

    <div class="col-xs-12"><?php
    //$strSearchAction = site_url('purchase_order/browse', NULL, FALSE);
    //include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchelement.php'); ?>
        <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
        <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('purchase_order', NULL, FALSE)?>" class="btn btn-primary" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
        <p class="spacer">&nbsp;</p>
        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
        <thead><tr>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name').' / '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th>
                <th class="action">Action</th>
        </tr></thead>
        <tbody><?php
    // Display data in the table
    if(!empty($arrPurchase)):
        foreach($arrPurchase as $e): ?>
            <tr>
                <td><?=$e['pror_code']?></td>
                <td><?=formatPersonName('ABBR',$e['supp_name'],$e['supp_address'],$e['supp_city'],$e['supp_phone'])?></td>
                <td><?=formatDate2($e['pror_date'],'d F Y')?></td>
                <td><?=setPrice($e['pror_total'])?></td>
                <td><?=$e['pror_status']?></td>
                <td class="action">
                    <?php if($e['bolAllowView']): ?><a href="<?=site_url('purchase_order/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
                    <?php if($e['bolBtnPrint']): ?><a href="<?=site_url('purchase_order/view/'.$e['id'].'?print=true', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>  
                </td>
            </tr><?php
        endforeach;
    else: ?>
            <tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
        </tbody>
        </table></div>
        <?=$strPage?>
    </div> -->