<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder'), 'link' => site_url('purchase_order/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);


include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<!-- Lama dibawah -->

<!-- Baru dibawah -->
<div class="col-xs-12">
    <form name="frmAddPurchase" id="frmAddPurchase" method="post" action="<?=site_url('purchase_order', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <!-- Data no pb -->
        <!-- <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i> 
                    #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?>
                    Nomor PB
                </h3>
            </div>
            <div class="panel-body">
                <input type="text" id="txtNoPB" name="txtNoPB" class="form-control required jwDateTime" readonly />
            </div>
        </div> -->
        <!-- Generate manual pada saat add nanti -->
    <!-- </div> -->
    <!-- <input type="hidden" id="txtNoPB" name="txtNoPB" class="form-control required jwDateTime" value="123ABC" readonly /> -->
        
        <div class="col-md-6"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <!-- Dibuat -->
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <label class="input-group-addon">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?>
                            </label> -->
                            <input type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                        <!-- </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- data owner -->
        <!-- <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> Data owner</h3></div>
                    <div class="panel-body">
                     Dibuat 
                     <div class="form-group">
                        <div class="input-group">
                            <label class="input-group-addon">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?>
                            </label> 
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="ownerName" name="ownerName" class="form-control" value="" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"/>
                                    <label class="input-group-addon" id="loadCustomer"></label>
                                </div>
                            </div>
                             <input type="text" id="dataOwner" name="dataOwner" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" readonly /> 
                         </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div> -->
    <!--data owner
     <div class="col-md-4"><div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-building-o"></i> 
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?>
            </h3>
        </div>
        <div class="panel-body">
            <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?> <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" id="warehouseName" name="warehouseName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>" />
                    <label class="input-group-addon" id="loadWarehouse"></label>
                </div>
            </div>
        </div>
    </div></div> -->

    <!-- nama proyek -->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        <!-- #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> -->
                        Nama Proyek
                    </h3>
                </div>
                <!-- <div class="panel-body">
                    <div class="form-group">
                        <select class="form-control" id="namaProyek" name="namaProyek">
                            <option value="pilih_proyek">Pilih proyek</option>
                        </select>
                    </div>
                </div> -->
                    <!-- <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" id="proyekName" name="proyekName" class="form-control" value="" placeholder="Masukkan nama proyek"/>
                                <label class="input-group-addon" id="loadCustomer"></label>
                            </div>
                        </div>
                    </div> -->
                    <div class="panel-body">
                        <select style="max-width:550px;" type="text" id="selProyek" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php foreach($arrKontrak as $e) { ?>
                                <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                    <!-- //loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata') -->
                        Nama Pekerjaan
                    </h3>
                </div>
                <!-- <div class="panel-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="pekerjaanName" name="pekerjaanName" class="form-control" value="" placeholder="Masukkan nama pekerjaan"/>
                            <label class="input-group-addon" id="loadCustomer"></label>
                        </div>
                    </div>
                </div> -->
                <div class="panel-body">
                    <select style="max-width:550px;" type="text" id="selPekerjaan" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <div class="form-group"><textarea style="height:50px;" id="txtaDescription" name="txtaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
                </div>
            </div>
        </div>

    </div>

        <!-- singkatan nama proyek -->
            <!-- <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-user"></i> 
                            #loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?>
                            Singkatan Nama Proyek
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="text" id="txtNamaProyek" name="txtNamaProyek" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" readonly />
                    </div>
                </div>
            </div> -->

<!-- Selected Items -->

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>

                <div class="panel-body">
                    <select type="text" id="selProduct" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" selected>Select your option</option>
                        <?php foreach($arrProduct as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['prod_title']?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedBahans">
                <thead>
                    <tr>
                        <th><i class="fa fa-trash"></th>
                        <th>Bahan</th>
                        <!-- <th>Tebal</th>
                        <th>Lebar</th>
                        <th>Panjang</th> -->
                        <th>Qty PB</th>
                        <th>Satuan PB</th>
                        <th>Qty Terima</th>
                        <th>Satuan Terima</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="info"><td class="noData" colspan="9"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    <input type="hidden" name="generateCode" id="generateCode"/>
                </tbody>
                </table></div>
                
                </div><!--/ Table Selected Items -->
            </div>
        </div>
    </div>

<div class="form-group">
    <button type="submit" name="smtMakePurchase" value="Make Purchase" class="btn btn-primary">
        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
    </button>
    <button type="submit" name="cancelMakePurchase" value="Cancel Make Purchase" class="btn btn-danger">
        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?>
    </button>
</div>

    <input type="hidden" id="kontrakCode" name="kontrakCode" value=""/>
    <input type="hidden" id="kontrakID" name="kontrakID" value=""/>
    <input type="hidden" id="subKontrakID" name="subKontrakID"/>
    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
    <input type="hidden" id="totalItem" name="totalItem"/>
    <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
    <input type="hidden" id="idProduct" name="idProduct"/>
</form></div>
