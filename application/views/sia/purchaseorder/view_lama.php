<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder'), 'link' => site_url('purchase_order/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
    <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-lastpurchaseorder')?></label>
    <select name="selJumpTo" class="form-control">
        <option value="">- Jump To -</option><?php
        if(!empty($arrPurchaseList)) foreach($arrPurchaseList as $e):
            $strListTitle = ' title="'.$e['supp_address'].', '.$e['supp_city'].'"'; ?>
        <option value="<?=site_url('purchase_order/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['pror_code']/*=formatDate2($e['pror_date'],'d/m/Y')*/?>] <?=$e['supp_name']?></option><?php
        endforeach; ?>
    </select>
</div></div>

<?php $suppid = $arrPurchaseData['supp_id'];
    $suppinternal = $arrPurchaseData['supp_internal'];?>
<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('purchase_order/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6"><div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?></h3></div>
        <div class="panel-body">
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
            <div class="col-xs-8"><?=$arrPurchaseData['supp_name']?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></div>
            <div class="col-xs-8"><?=$arrPurchaseData['supp_address']?>, <?=$arrPurchaseData['supp_city']?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
            <div class="col-xs-8"><?=$arrPurchaseData['supp_phone']?></div>
            <p class="spacer">&nbsp;</p>
        </div>
    </div></div>
    
    <div class="col-md-6"><div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
        <div class="panel-body">
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
            <div class="col-xs-8"><?=$arrPurchaseData['pror_code']?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
            <div class="col-xs-8"><?=formatDate2($arrPurchaseData['pror_date'],'d F Y')?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')?></div>
            <div class="col-xs-8"><?=formatDate2($arrPurchaseData['pror_exp_date'],'d F Y')?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehouse')?></div>
            <div class="col-xs-8"><?=$arrPurchaseData['ware_name']?></div>
            <p class="spacer">&nbsp;</p>
        </div>
    </div></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">
    
<?php if($bolBtnEdit): ?>  
    <div class="form-group"><div class="input-group addNew">
        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
    </div></div><?php
endif; ?>  

    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
    <thead>
        <tr>
            <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>  
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
        </tr>
    </thead>
    <tbody><?php
$i = 0;
$intTotalItem = 0;
$iditemawal='';
if(!empty($arrPurchaseItem)): 
    foreach($arrPurchaseItem as $e):
        $iditemawal=$iditemawal.$e['product_id'].',';
        $intTotalItem++; ?>
    <tr><?php
        if($bolBtnEdit): ?>
        <td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]" /></td><?php
        endif; ?>
        <td class="qty"><?php
        if($bolBtnEdit): //if yes ?>
        <div class="form-group">
            <div class="col-xs-4"><input type="text" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity1']?>" class="required number form-control input-sm<?=$e['prod_conv1'] <= 1 ? ' hidden' : ''?>" id="txtItem1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit1'])?>" title="<?=formatUnitName($e['proi_unit1'])?>" /></div>
            <div class="col-xs-4"><input type="text" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity2']?>" class="required number form-control input-sm<?=$e['prod_conv2'] <= 1 ? ' hidden' : ''?>" id="txtItem2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit2'])?>" title="<?=formatUnitName($e['proi_unit2'])?>" /></div>
            <div class="col-xs-4"><input type="text" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity3']?>" class="required number form-control input-sm" id="txtItem3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit3'])?>" title="<?=formatUnitName($e['proi_unit3'])?>" /></div>
            <input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['proi_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
            <input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['proi_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
            <input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['proi_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
            <input type="hidden" name="idItem<?=$e['id']?>" value="<?=$e['product_id']?>" id="idItem<?=$e['id']?>" />
            <br>
        </div><?php
        else: echo
            $e['proi_quantity1'].' '.formatUnitName($e['proi_unit1']).' + '.
            $e['proi_quantity2'].' '.formatUnitName($e['proi_unit2']).' + '.
            $e['proi_quantity3'].' '.formatUnitName($e['proi_unit3'])."<br>";
            if($e['proi_purchased1']!='0' || $e['proi_purchased2']!='0' || $e['proi_purchased3']!='0'){
            echo "di pembelian : <br>".
                $e['proi_purchased1'].' + '.
                $e['proi_purchased2'].' + '.
                $e['proi_purchased3'];}
        endif; ?>
            <input type="hidden" name="idProiID[<?=$e['id']?>]" value="<?=$e['id']?>" />
        </td>
        <td><b><?="(".$e['prod_code'].") ".$e['strName']?></b></td>
        <td class="pr"><?php
        if($bolBtnEdit): ?>
            <div class="input-group"><label class="input-group-addon"><?=str_replace(' 0','',setPrice('','USED'))?></label><input type="text" name="txtItemPrice[<?=$e['id']?>]" class="required currency form-control input-sm" id="txtItemPrice<?=$e['id']?>" value="<?=$e['proi_price']?>" /></div><?php
        else:
            echo setPrice($e['proi_price'],'USED');
        endif; ?>
        </td>
        <td class="disc"><?php
        if($bolBtnEdit): ?>
        <div class="form-group">
            <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem1Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem1Disc<?=$e['id']?>" value="<?=$e['proi_discount1']?>" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>
            <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem2Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem2Disc<?=$e['id']?>" value="<?=$e['proi_discount2']?>" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>
            <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem3Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem3Disc<?=$e['id']?>" value="<?=$e['proi_discount3']?>" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>
        </div><?php

        else:
            echo $e['proi_discount1']." % + ".$e['proi_discount2']." % + ".$e['proi_discount3']." %";
        endif; ?>
    </td>
    <td class="subTotal" id="subTotal<?=$e['id']?>"><?=$e['proi_subtotal']?></td>
    </tr><?php

        $i++;
    endforeach;

    $iditemawal = $iditemawal.'0';
else: ?>  
        <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
endif; ?>  
    </tbody>
    </table></div>
        
    <div class="row">
        <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></div>
        <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?=$intPurchaseTotal?></div>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="row">
        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
        <div class="col-sm-2 form-group"><?php
if($bolBtnEdit): ?>  
            <div class="input-group"><input type="text" name="dsc4" value="<?=$arrPurchaseData['pror_discount']?>" id="dsc4" class="required currency form-control" /><!--<label class="input-group-addon">%</label>--></div><?php
    
else:
    echo $arrPurchaseData['pror_discount'];
endif; ?></div>
        <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
        <div class="col-sm-2 tdDesc currency" id="subTotalWithDisc"><?=$arrPurchaseData['subtotal_discounted']?></div>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="row">
        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
        <div class="col-sm-2 tdDesc form-group">
            <?php
            if($bolBtnEdit): ?>
                <div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="<?=$arrPurchaseData['pror_tax']?>" /><label class="input-group-addon">%</label></div><?php

            else:
                echo $arrPurchaseData['pror_tax'];
            endif; ?>
        </div>
        <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
        <div class="col-sm-2 tdDesc currency" id="subTotalTax"><?=$arrPurchaseData['subtotal_taxed']?></div>
    </div>
    <p class="spacer">&nbsp;</p>
    </div><!--/ Table Selected Items -->
</div>

<div class="row">

<div class="col-md-4"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
    <div class="panel-body"><?php
if($bolBtnEdit): ?>  
        <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrPurchaseData['pror_description']?></textarea></div><?php
else: ?>
        <input type="hidden" name="txaDescription" value="<?=$arrPurchaseData['pror_description']?>"/>
		<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrPurchaseData['pror_description']?></div><?php
endif; ?>
        <div class="form-group">
            <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')?></label>
            <input type="text" name="txtProgress" value="<?=$arrPurchaseData['pror_progress']?>" maxlength="64" class="form-control" />
        </div>
    </div>
</div>
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> Pembelian</h3></div>
        <div class="panel-body">

            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="listPembelian">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-purchase')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-date')?></th>
                    </tr>
                    </thead>
                    <tbody><?php
                    if(!empty($arrPurchased)):
                        foreach($arrPurchased as $e): ?>
                            <tr>
                            <td><a href="<?=site_url('purchase/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=$e['prch_code']?></a></td>
                            <td><?=$e['cdate']?></td>
                            </tr><?php
                        endforeach;
                    else: ?>
                        <tr class="info"><td class="noData" colspan="2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
                    endif; ?>
                    </tbody>
                </table></div>

        </div><!--/ Pembelian -->
    </div>
</div>

<!-- Bonus -->
<div class="col-md-8"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
    <div class="panel-body">

<?php if($bolBtnEdit): ?>
    <div class="form-group"><div class="input-group addNew">
        <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
    </div></div><?php
endif; ?>

    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
    <thead>
        <tr><?php
            if($bolBtnEdit): ?>
                        <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php
            endif; ?>
            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
        </tr>
    </thead>
    <tbody><?php
        $i = 0;
        $intTotalBonusItem = 0;
        $iditembonusawal='';
        if(!empty($arrPurchaseBonusItem)):
            foreach($arrPurchaseBonusItem as $e):
                $iditembonusawal=$iditembonusawal.$e['product_id'].',';
                $intTotalBonusItem++; ?>
                <tr><?php
                if($bolBtnEdit): ?>
                    <td class="cb"><input type="checkbox" name="cbBonusAwal[<?=$e['id']?>]"/></td><?php
                endif; ?>
                    <td class="qty"><?php
                if($bolBtnEdit): //if yes ?>
                    <div class="form-group">
                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus1Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity1']?>" class="required number form-control input-sm" id="txtItemBonus1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit1'])?>" title="<?=formatUnitName($e['proi_unit1'])?>" /></div></div>
                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus2Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity2']?>" class="required number form-control input-sm" id="txtItemBonus2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit2'])?>" title="<?=formatUnitName($e['proi_unit2'])?>" /></div></div>
                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus3Qty[<?=$e['id']?>]" value="<?=$e['proi_quantity3']?>" class="required number form-control input-sm" id="txtItemBonus3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['proi_unit3'])?>" title="<?=formatUnitName($e['proi_unit3'])?>" /></div></div>
                        <input type="hidden" value="<?=$e['product_id']?>" id="idItemBonus<?=$e['id']?>" />
                    </div><?php

                else:
                    echo $e['proi_quantity1'].' '.formatUnitName($e['proi_unit1']).' + '.$e['proi_quantity2'].' '.formatUnitName($e['proi_unit2']).' + '.$e['proi_quantity3'].' '.formatUnitName($e['proi_unit3'])."<br>";
                    if($e['proi_purchased1']!='0' || $e['proi_purchased2']!='0' || $e['proi_purchased3']!='0'){
                        echo "di pembelian : <br>".
                            $e['proi_purchased1'].' + '.
                            $e['proi_purchased2'].' + '.
                            $e['proi_purchased3'];}
                endif; ?>
                        <input type="hidden" value="<?=$e['id']?>" name="idProiIDBonus[<?=$e['id']?>]" />
                    </td>
                    <td><b><?="(".$e['prod_code'].") ".$e['strName']?></b></td>
                </tr><?php
                $i++;
            endforeach;
            $iditembonusawal=$iditembonusawal.'0';
        else: ?>
                <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
        endif; ?>
    </tbody>
    </table></div>

    </div><!--/ Table Bonus -->
</div></div>



</div>

<?php
    $rawStatus = $arrPurchaseData['pror_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>  

<input type="hidden" id="IDSupp" value="<?=$suppid?>"/>
<input type="hidden" id="InternalSupp" value="<?=$suppinternal?>"/>
<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItemBonusAwal" name="totalItemBonusAwal" value="<?=$intTotalBonusItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus" value="0"/>

</form></div>