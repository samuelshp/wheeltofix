<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".currency").autoNumeric('init', autoNumericOptionsRupiah,{'mDec': 4});
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);
$("select[name='selJumpTo']").change(function() {
	if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});
var arrClosed = [];
var selectedItemBonusBefore=$('#idItemBonusAwal').val().split(",");
var selectedItemBefore=$('#idItemAwal').val().split(",");
var numberitembonusawal=selectedItemBonusBefore.length-1;
var numberitemawal=selectedItemBefore.length-1;
var totalitembonus=0;
var jumlahDelete = 0;
var numberitembonus=0;
if($("#jumlah_row").val()){
	$("#jumlah_row").val(0);
	var totalitem=$("#jumlah_row").val();
}
else{
	var totalitem=0;
}
var numberitem=0;
if($("#jumlah_row").val()){
	$("#jumlah_row").val(0);
	var numberItem = $("#jumlah_row").val();
}
else{
	var numberItem = 0;
}
var idkontrak = $("#idkontrak").val();

console.log($("#status_now").val()+"HAHAA");

if($("#status_pb").val() == 2){
	$("#th_delete").hide();
	$(".delete").hide();
}
else{
	$("#th_delete").show();
	$("#delete").show();
}

$("#frmChangePurchase").submit(function() {
	var form = $(this);
	// if(totalitem=='0' && numberitemawal=='0'){
	// 	alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>");
	// 	return false;
	// }
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	$('.subTotal').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});

$("#txtTax").change(function(){
	var totalwithdisc=parseFloat( $("#subTotalWithDisc").autoNumeric('get'));
	var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});

$( "#dsc4").change(function(){
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("input[name*='gantiStatus']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	console.log(at);
	if(arrClosed.includes(at)){
		var index = arrClosed.indexOf(at);
		arrClosed.splice(index, 1);
	}
	else{
		arrClosed.push(at);
	}
	//console.log(arrClosed);
	if($(this).prop("checked") == true){
		$("#undoClose").val(arrClosed);
	}
	else{
		$("#dataClose").val(arrClosed);
	}
	//console.log($("#dataClose").val());
});

$( "input[name*='cbDeleteAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitemawal--;
	if((numberitemawal+numberitem)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItem"+at).val();
	});
	$(this).closest("tr").remove();
});


$( "input[name*='cbBonusAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	numberitembonusawal--;
	if((numberitembonus+numberitembonusawal)=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonus"+at).val();
	});
	$(this).closest("tr").remove();
});

$( "input[name*='Item']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	for(var zz=1;zz<=3;zz++){
		if($("#txtItem"+zz+"Qty"+at).val()<0){
			$("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
		}
	}
	if($("#txtItemPrice"+at).autoNumeric('get')<0){
		$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotal"+at).autoNumeric('get');
	var price = $("#txtItemPrice"+at).autoNumeric('get');
	var subtotal=$("#txtItem1Qty"+at).val()*price+($("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val())+($("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
	}
	$("#subTotal"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

//Add item lama
	// $("#txtNewItem").autocomplete({
	// 	minLength:3,
	// 	delay:500,
	// 	source: function(request,response) {
	// 		$.ajax({
	// 			url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItem"]').val() + "/" + $("#InternalSupp").val(),
	// 			beforeSend: function() {
	// 				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
	// 			},
	// 			complete: function() {
	// 				$("#loadItem").html('');
	// 			},
	// 			success: function(data){
	// 				var xmlDoc = $.parseXML(data);
	// 				var xml = $(xmlDoc);
	// 				$arrSelectedPO = xml;
	// 				var display=[];
	// 				$.map(xml.find('Product').find('item'),function(val,i){
	// 					var intID = $(val).find('id').text();
	// 					var strName=$(val).find('strNameWithPrice').text();
	//                     var strKode=$(val).find('prod_code').text();
	// 					if($.inArray(intID, selectedItemBefore) > -1){

	// 					}else{
	// 						display.push({label: "("+strKode+") "+strName, value: '',id:intID});
	// 					}
	// 				});
	// 				response(display);
	// 			}
	// 		});
	// 	},
	// 	select: function(event, ui) {
	// 		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
	// 			return $(el).find('id').text() == ui.item.id;
	// 		});
	// 		var i=totalitem;
	// 		var qty1HiddenClass = '';
	//         var qty2HiddenClass = '';
	//         var prod_conv1 = $(selectedPO).find('prod_conv1').text();
	//         var prod_conv2 = $(selectedPO).find('prod_conv2').text();
	//         if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
	//         if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
	// 		if((numberitemawal+numberitem)=='0') {$("#invoiceItemList tbody").html('');}

	// 		$("#invoiceItemList tbody").append(
	// 			'<tr>' +
	// 				'<td class="cb"><input type="checkbox" name="cbDeleteNewX'+i+'"/></td>' +
	// 				'<td class="qty"><div class="form-group">' +
	// 					'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0" /></div>' +
	// 					'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0" /></div>' +
	// 					'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0" /></div>' +
	// 				'</div></td>' +
	// 				'<td><label id="nmeItemX'+i+'"></label></td>'+
	// 				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
	// 				'<td class="disc"><div class="form-group">' +
	// 					'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
	// 					'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
	// 					'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
	// 				'</div></td>'+
	// 				'<td class="subTotal" id="subTotalX'+i+'">0</td>'+
	// 				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
	// 				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
	// 				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
	// 				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
	// 				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
	// 				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
	// 				'<input type="hidden" id="conv1UnitX'+i+'" >' +
	// 				'<input type="hidden" id="conv2UnitX'+i+'" >' +
	// 				'<input type="hidden" id="conv3UnitX'+i+'" >'+
	// 			'</tr>'
	// 		);
	//         var prodcode = $(selectedPO).find('prod_code').text();
	// 		var prodtitle = $(selectedPO).find('prod_title').text();
	// 		var probtitle = $(selectedPO).find('prob_title').text();
	// 		var proctitle = $(selectedPO).find('proc_title').text();
	// 		var strName=$(selectedPO).find('strName').text();
	// 		var id=$(selectedPO).find('id').text();
	// 		selectedItemBefore.push(id);
	// 		$("#nmeItemX"+i).text("("+prodcode+") "+strName);
	// 		var proddesc = $(selectedPO).find('prod_description').text();
	//         if(proddesc != '') {
	//             $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
	//             $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
	//         }
	//         $("#nmeItemX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
	// 		$("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
	// 		$("#idItemX"+i).val(id);
	// 		$("#prodItemX"+i).val(prodtitle);
	// 		$("#probItemX"+i).val(probtitle);
	// 		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
	// 		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
	// 		$("#qty1ItemX"+i).hide();
	// 		$("#qty2ItemX"+i).hide();
	// 		$("#qty3ItemX"+i).hide();
	// 		$.ajax({
	// 			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
	// 			success: function(data){
	// 				var xmlDoc = $.parseXML(data);
	// 				var xml = $(xmlDoc);
	// 				$arrSelectedPO = xml;
	// 				/* var x=totalItem-1; */
	// 				var unitID=[];
	// 				var unitStr=[];
	// 				var unitConv=[];
	// 				$.map(xml.find('Unit').find('item'),function(val,j){
	// 					var intID = $(val).find('id').text();
	// 					var strUnit=$(val).find('unit_title').text();
	// 					var intConv=$(val).find('unit_conversion').text();
	// 					unitID.push(intID);
	// 					unitStr.push(strUnit);
	// 					unitConv.push(intConv);
	// 					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
	// 				});
	// 				for(var zz=1;zz<=unitID.length;zz++){
	// 					$("#qty"+zz+"ItemX"+i).show();
	// 					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
	// 					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
	// 					$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
	// 				}
	// 			}
	// 		});
	// 		totalitem++;
	// 		numberitem++;
	// 		$("#totalItem").val(totalitem);

	// 		$('#txtNewItem').val(''); return false; // Clear the textbox
	// 	}
	// });

$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_order_ajax/getAllBahanAC', NULL, FALSE)?>/"+ $("#txtNewItem").val(),
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                console.log(xmlDoc);
                var xml = $(xmlDoc);
                $arrSelectedBahan = xml;
                var display=[];
                $.map(xml.find('Bahan').find('item'),function(val,i){
                    //if($(val).find('id').text() == $('#kontrakID').val()){
                    // var bahan = $(val).find('prod_title').text();
                    // var idProduct = $(val).find('id').text();
                    // var unit = $(val).find('unit_title').text();
                    // var unit_material_id = $(val).find('munit_material_id').text();
                    // var unit_id = $(val).find('munit_unit_id').text();
                    // var unit_conversion = $(val).find('unit_conversion').text();
                    // var qty = $(val).find('prod_qty1').text();
                        var idProd = $(val).find('id').text();
                        var bahan = $(val).find('prod_title').text();
                        var satuan = $(val).find('unit_title').text();
                        display.push({label:"("+bahan+") - "+ satuan, id:idProd});
                        numberItem++;
                    //}
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedBahan = $.grep($arrSelectedBahan.find('Bahan').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var bahan = $(selectedBahan).find('prod_title').text();
        var satuan = $(selectedBahan).find('unit_title').text();
        var idprod2 = $(selectedBahan).find('id').text();
		var kodekon = $('#kode_kontrak').val();
		var convert_kodekon = kodekon.replace(/\//g,"-");
		console.log(idprod2+"haha");
        //Ajax bawah untuk check apakah ada atau tidak di subkontrak material
        $.ajax({
			type:"POST",
            data:{
                idsubkon : $('#idsubkon').val(),
                idprod : idprod2
            },
            url: "<?=site_url('purchase_order_ajax/checkBahanToSubkontrakMaterial', NULL, FALSE)?>",
            beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItem").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                console.log(xmlDoc);
                console.log('Diatas isi dari check bahan');
                var xml = $(xmlDoc);
                $arrSelectedBahan = xml;
                var display=[];
                $.map(xml.find('Bahan').find('item'),function(val,i){
                    var idsubkontrak = $(val).find('subkontrak_id').text();
					console.log(idsubkontrak+"ini isi yang didapat");
                    if(idsubkontrak == ''){
                        console.log('Kosong');
                        $(".info").hide();
                        $("#invoiceItemList tbody").append(
                        '<tr style="text-align:center;">'+
                            '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+idprod2+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                            '<td id="isiQty'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="0"><input style="width:100px;" type="text" id="editQty'+totalitem+'" name="editQty'+totalitem+'" value="0"/><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="0"></td>'+
                            '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                            '<td class="ket"><input type="text" style="width:200px;" id="editKeterangan'+totalitem+'" name="editKeterangan'+totalitem+'" value=""/></td>'+
                            '<td class="delete" style="width:45px;"><button type="button" style="width:40px;" class="btn btn-danger" value="'+idprod2+'" name="delete_before_add" id="delete_before_add"><i class="fa fa-trash"></i></button></td>'+
							'<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="0">'+
							'<input type="hidden" name="status_tambah'+totalitem+'" id="status_tambah'+totalitem+'" value="tidak"/>'+
							'<input type="hidden" name="status_baru'+totalitem+'" id="status_baru'+totalitem+'" value="0"/>'+
							'<input type="hidden" name="posisi'+totalitem+'" id="status_baru'+totalitem+'" value="0"/>'+
                        '</tr>');
                        $("#totalItem").val(totalitem);
                        console.log($("#totalItem").val()+"jumlah item di table");
                        $('#txtNewItem').val('');
						$("#jumlah_baru").val(numberItem);
                        totalitem++;
						numberItem++;
                    }
                    else{
						console.log(bahan);
                        console.log(idsubkontrak);//kalau itemnya ada
                        $.ajax({
							type:"POST",
							data:{
								bahan2 : bahan,
								idsubkon : idsubkontrak
							},
                            url: "<?=site_url('purchase_order_ajax/getAllBahanAutoComplete', NULL, FALSE)?>",
                            beforeSend: function() {
                                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                            },
                            complete: function() {
                                $("#loadItem").html('');
                            },
                            success: function(data){
                                var xmlDoc = $.parseXML(data);
                                console.log(xmlDoc);
                                console.log('Diatas isi dari bahan yang ada subkontrak idnya');
                                var xml = $(xmlDoc);
                                $arrSelectedBahan = xml;
                                $.map(xml.find('Bahan').find('item'),function(val,i){
                                    var idsubkontrak = $(val).find('subkontrak_id').text();
                                    var terpb_now = $(val).find('subkontrak_terpb').text();
									var idprod_ = $(val).find('material').text();
                                    var qty = $(val).find('qty').text();
                                    $(".info").hide();
                                    $("#invoiceItemList tbody").append(
                                    '<tr style="text-align:center;">'+
										'<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct'+totalitem+'" id="idProduct'+totalitem+'" value="'+idprod2+'"><input type="hidden" name="isiBahanText'+totalitem+'" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+
                                        '<td id="isiQty'+totalitem+'"><input type="hidden" name="isiSubKon'+totalitem+'" id="isiSubKon'+totalitem+'" value="'+idsubkontrak+'"/><input style="width:100px;" type="text" id="editQty'+totalitem+'" name="editQty'+totalitem+'" value="'+qty+'"/><input type="hidden" name="inputAwal'+totalitem+'" id="inputAwal'+totalitem+'" value="'+qty+'"></td>'+
                                        '<td id="isiSatuan'+totalitem+'"><input type="hidden" name="isiSatuanText'+totalitem+'" id="isiSatuanText'+totalitem+'" value="'+satuan+'">'+satuan+'</td>'+
                                        '<td class="ket"><input type="text" style="width:200px;" id="editKeterangan'+totalitem+'" name="editKeterangan'+totalitem+'" value=""/></td>'+
										'<td class="delete" style="width:45px;"><button type="button" style="width:40px;" class="btn btn-danger" value="'+idprod2+'" name="delete_before_add" id="delete_before_add"><i class="fa fa-trash"></i></button></td>'+
										'<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="'+terpb_now+'">'+
										'<input type="hidden" name="status_tambah'+totalitem+'" id="status_tambah'+totalitem+'" value="ada"/>'+
										'<input type="hidden" name="status_baru'+totalitem+'" id="status_baru'+totalitem+'" value="0"/>'+
                                    '</tr>');
                                    $("#totalItem").val(totalitem);
                                    console.log($("#totalItem").val()+"jumlah item di table");
                                    $("#jumlah_baru").val(numberItem);
									totalitem++;
									numberItem++;
                                })
                            }
                        });
                    }
                });
            }
            
        });
        $('#txtNewItem').val(''); return false;
        // if(numberItem=='0'){
        //     $("#selectedBahans tbody").html('');   
        // }
        // $(".info").hide(); // Clear the textbox
    }
});


$("#txtNewItemBonus").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $("#txtNewItemBonus").val() + "/" +$("#InternalSupp").val(),
			beforeSend: function() {
				$("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemBonus").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBonusBefore) > -1){

					}else{
						display.push({label: "("+strKode+") "+strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalitembonus;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if((numberitembonusawal+numberitembonus)=='0') $("#selectedItemsBonus tbody").html('');

		$("#selectedItemsBonus tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteNewBonusX'+i+'"/></td>'+
				'<td class="qty"><div class="form-group">' +
				'<div class="col-xs-4"><div class="form-group"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="0" /></div></div>' +
				'<div class="col-xs-4"><div class="form-group"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="0" /></div></div>' +
				'<div class="col-xs-4"><div class="form-group"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0" /></div></div>'+
				'</div></td>'+
				'<td id="nmeItemBonusX'+i+'"></td>'+
				'<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
				'<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
				'<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
				'<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
				'</tr>'
		);
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		var id=$(selectedPO).find('id').text();
		selectedItemBonusBefore.push(id);
		$("#nmeItemBonusX"+i).text("("+prodcode+") "+strName);
		var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemBonusX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
		$("#idItemBonusX"+i).val(id);
		$("#prodItemBonusX"+i).val(prodtitle);
		$("#probItemBonusX"+i).val(probtitle);
		$("#qty1ItemBonusX"+i).hide();
		$("#qty2ItemBonusX"+i).hide();
		$("#qty3ItemBonusX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemBonusX"+i).show();
					$("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
					$("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
				}
			}
		});
		totalitembonus++;
		numberitembonus++;
		$("#totalItemBonus").val(totalitembonus);

		$('#txtNewItemBonus').val(''); return false; // Clear the textbox
	}
});

$("#invoiceItemList").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
	}
	if($("#prcItemX"+at).autoNumeric('get')<0){
		$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotalX"+at).autoNumeric('get');
	var price = $("#prcItemX"+at).autoNumeric('get');
	var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
	}
	$("#subTotalX"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});
$("#dsc4").change(function(){
	var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalwithouttax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("#invoiceItemList").on("click","input[type='checkbox'][name*='cbDeleteNew']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitem--;
	if((numberitem+numberitemawal)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#invoiceItemList").on("click","button[name*='delete_before_add']", function(){
	$(this).parent().parent().remove();
// 	totalitem--;
// 	numberItem--;
// 	$("#jumlah_baru").val(numberItem);
// 	$("#status_baru").val(1);
// 	$("#totalItem").val(totalitem);
});

console.log($("#status_pb").val());

$("#invoiceItemList").on("click","button[name*='delete_from_pb']",function(){
	$(this).val();
    console.log($(this).val());
	//console.log(arrClosed);
    $temp = $("#kumpulan_delete_id").val();
    if($temp == ''){
        $("#kumpulan_delete_id").val($(this).val());
    }
    else{
	    $("#kumpulan_delete_id").val($temp+","+$(this).val());
    }
    $(this).parent().parent().hide();
    console.log($("#kumpulan_delete_id").val());
    jumlahDelete++;
    $("#jumlah_delete").val(jumlahDelete);
    console.log($("#jumlah_row").val()+'HAHAHA');
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDeleteNew']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberitembonus--;
	if((numberitembonus+numberitembonusawal)=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonusX"+at).val();
	});
	$(this).closest("tr").remove();
});

$(document).on('click', '[id^=nmeItem] .fa-question-circle', function() {
    var $this = $(this);
    $.ajax({
        url: "<?=site_url('purchase_ajax/getLastProductPrice')?>",
        data: {
            product_id: $this.closest('tr').find('[id^=idItem]').val(),
        },
        dataType: 'json',
        success: function(data) {
            var msg = "Harga Pembelian Terakhir: \n\n";
            $.each(data.Items, function() {
                msg = msg + this.strDate + ' - ' + this.supp_name + ' - ' + this.strPrice + "\n";
            });
            alert(msg);
        }
    });
});


});</script>