<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder'), 'link' => site_url('purchase_order/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<script>console.log('a');</script>

<?php $suppid = $arrPurchaseData['supp_id'];
    $suppinternal = $arrPurchaseData['supp_internal'];?>
<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('purchase_order/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data Proyek</h3></div>
            <div class="panel-body">
                <div class="col-xs-4">No. PB : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['pror_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Tanggal dibuat : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['cdate']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Owner : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['cust_name']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Nama proyek : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['kont_name']?><!-- textbox untuk notif --><input name="kontrak_name" type="hidden" value="<?=$arrPurchaseData['kont_name']?>"/></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Nama pekerjaan : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['job']?></div>
                <p class="spacer">&nbsp;</p>
                <input type="hidden" value="<?= $arrPurchaseData['id'] ?>" name="po_id" id="po_id" />
                <input type="hidden" value="<?= $arrPurchaseData['kont_code'] ?>" name="kode_kontrak" id="kode_kontrak" />
                <input type="hidden" value="<?= $arrPurchaseData['approvalCC'] ?>" name="status_cc" id="status_cc" />
                <input type="hidden" value="<?= $arrPurchaseData['pror_rawstatus'] ?>" name="status_pb" id="status_pb" />
                <input type="hidden" value="<?= $arrPurchaseData['approvalPM'] ?>" name="status_pm" id="status_pm" />
                <input type="hidden" value="<?= $arrPurchaseData['kontrak_id'] ?>" name="kontrakid" id="kontrakid" />
                <input type="hidden" value="<?=$arrPurchaseData['idSubKontrak']?>" name="subkontrakid" id="subkontrakid" />
                <?php 
                    if($bolBtnEdit){
                        echo '<input type="hidden" value="2" name="status_now" id="status_now"/>';
                    }
                    else{
                        echo '<input type="hidden" value="1" name="status_now" id="status_now"/>';
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
            <div class="panel-body">
                <div class="form-group">
                    <textarea name="txtProgress" maxlength="64" class="form-control" readonly><?=$arrPurchaseData['pror_description']?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">
    
<?php if($bolBtnEdit): ?>  
    <div class="form-group"><div class="input-group addNew">
        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
    </div></div><?php
endif; ?>  

    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
    <thead>
        <tr>
            <!-- <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>   -->
            <th>Bahan</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Qty Terima</th>
            <th>Satuan Terima</th>
            <th>Keterangan</th>
            <th id="th_delete">Delete</th>
        </tr>
    </thead>
    <tbody><?php
$i = 0;
$intTotalItem = 0;
$iditemawal='';
if(!empty($arrPurchaseItem)):
    foreach($arrPurchaseItem as $e):
        echo "<script>console.log('".$e['unit_title']."');</script>";
        //$iditemawal=$iditemawal.$e['product_id'].',';
        $intTotalItem++; ?>
    <tr style="text-align:center;"><?php
        if($bolBtnEdit): ?>
        <!-- <td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]" /></td> -->
        <input type="hidden" id="idkontrak" name="idkontrak" value="<?=$e['id']?>"/>
        <?php
        endif; ?>
        <td class="qty">
            <?php 
                echo $e['prod_title'];
            ?>
            <input type="hidden" name="idProiID<?=$i?>" value="<?=$e['id']?>" />
            <input type="hidden" name="idProduct2<?=$e['id']?>" value="<?=$e['proi_product_id']?>" />
            <input type="hidden" name="idProduct<?=$i?>" id="idProduct<?=$i?>" value="<?=$e['proi_product_id']?>">
        </td>
        <td style="width:100px;">
            <?php
                if($bolBtnEdit){
                    echo '<input style="width:100px;" type="number" step="0.0001" id="editQty'.$i.'" name="editQty'.$i.'" value="'.$e['proi_quantity1'].'" />'; 
                    echo '<input type="hidden" name="jumlahInputan'.$e['id'].'" id="jumlahInputan'.$e['id'].'" value="'.$e['proi_quantity1'].'" />';
                }
                else{
                    echo $e['proi_quantity1'];
                    echo '<input type="hidden" name="jumlahInputan'.$e['id'].'" id="jumlahInputan'.$e['id'].'" value="'.$e['proi_quantity1'].'" />';
                    echo '<input type="hidden" name="idProiID<?=$i?>" value="'.$e["id"].'" />';
                }
                #$e['proi_quantity1']
            ?>
        </td>
        <td class="pr">
            <?php echo $e['sat_pb']; 
                echo "<input type='hidden' name='isiSubKon".$i."' id='isiSubKon".$i."' value='".$e['idSubKontrak']."'/>";
            ?>
        </td>
        <td>
                <?php
                    if($bolBtnEdit){
                        echo '<input style="width:100px;" type="number" step="0.0001" id="editQtyTerima'.$i.'" name="editQtyTerima'.$i.'" value="'.$e['proi_quantity_terima'].'" />'; 
                        echo '<input type="hidden" name="jumlahInputanTerima'.$e['id'].'" id="jumlahInputanTerima'.$e['id'].'" value="'.$e['proi_quantity_terima'].'" />';
                    }
                    else{
                        echo $e['proi_quantity_terima'];
                        echo '<input type="hidden" name="jumlahInputanTerima'.$e['id'].'" id="jumlahInputanTerima'.$e['id'].'" value="'.$e['proi_quantity1'].'" />';
                    }
                ?>
        </td>
        <td><?php
            echo $e['title_terima'];
        ?></td>
        <td class="disc" style="width:200px;">
            <?php 
                if($bolBtnEdit){
                    echo '<input type="text" style="width:200px;" id="editKeterangan'.$i.'" name="editKeterangan'.$i.'" value="'.$e['proi_description'].'" '; 
                }
                else{
                    echo $e['proi_description'];
                }
                #echo $e['proi_description'];
            ?>
        </td>
        <!-- <td class="delete" style="width:45px;">
            <button type="button" style="width:40px;" class="btn btn-danger" value="<?= $e['id'] ?>" name="delete_from_pb" id="delete_from_pb"><i class="fa fa-trash">
            </i></button>
        </td> -->
        <?php 
            echo "<input type='hidden' name='isiTerPB".$i."' id='isiTerPB".$i."' value='".$e['subkontrak_terpb']."'>";
            echo "<input type='hidden' name='status_tambah".$i."' id='status_tambah".$i."' value='tidak'/>";
        ?>
    </tr><?php

        $i++;
    endforeach;
    echo "<input type='hidden' id='jumlah_row' name='jumlah_row' value='".$jumlah_item."'/> ";
    echo "<input type='hidden' id='jumlah_row3' name='jumlah_row3' value='".$jumlah_item."'/> ";
    $iditemawal = $iditemawal.'0';
else: ?>  
        <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
endif; ?>  
    </tbody>
    </table></div>
    </div><!--/ Table Selected Items -->
</div>
</div>
<div class="col-xs-12">
    <div class="form-group">
        <?php 
        $priviledge =$_SESSION['strAdminPriviledge'];
        if($_SESSION['strAdminPriviledge'] != 11 && $_SESSION['strAdminPriviledge'] != 9 && $_SESSION['strAdminPriviledge'] != 6 && $_SESSION['strAdminPriviledge'] != 5 && $arrPurchaseData['approvalCC'] != 1){
            if($bolBtnEdit){
                echo "
                <button type='submit' name='editPurchase' id='editPurchase' value='Edit Purchase' class='btn btn-warning'>
                    <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
                </button>
                <button type='submit' name='backPurchase' id='backPurchase' value='Cancel Edit Purchase' class='btn btn-danger'>
                    <span class='glyphicon glyphicon-repeat' aria-hidden='true'></span>
                </button>
                <br>
                ";
            } 
            else{
                if($arrPurchaseData['approvalCC'] == -1){
                    echo "
                    <br>
                    <h5 style='color:blue;'>Data menunggu approval Project Manager</h5><p class='spacer'>&nbsp;</p>
                    <button type='submit' name='disapprovePB' id='disapprovePB' value='Disapprove PB' class='btn btn-primary'>
                        <span>DISAPPROVE PB</span>
                    </button>";
                }
                if($arrPurchaseData['approvalCC'] == 2){
                    echo "
                        <button type='submit' name='disapprovePB' id='disapprovePB' value='Disapprove PB' class='btn btn-primary'>
                            <span>DISAPPROVE PB</span>
                        </button>
                    ";
                }
                if($arrPurchaseData['approvalCC'] == 3){
                    echo "
                        <h5 style='color:blue;'>Data sudah di approve oleh Project Manager</h5><p class='spacer'>&nbsp;</p>
                    ";
                }
            }
        }
        if($_SESSION['strAdminPriviledge'] == 11 && $arrPurchaseData['approvalCC'] != 1){
            if($arrPurchaseData['approvalPM'] == 3){
                echo "<h5 style='color:blue;'>Data sudah di approve oleh Project Manager</h5><p class='spacer'>&nbsp;</p>";
            }
            else if($arrPurchaseData['approvalCC'] == -1){
                echo "<h5 style='color:blue;'>PB ini tidak membutuhkan persetujuan CC</h5><p class='spacer'>&nbsp;</p>";
            }
            else{
                if($arrPurchaseData['approvalCC'] == 2){
                    echo "<button type='submit' name='ccEdit' id='ccEdit' value='Edit CC Purchase' class='btn btn-primary'>
                        <span>APPROVE CC</span>
                    </button>";
                    echo "<button type='submit' name='disccEdit' id='disccEdit' value='Disedit CC Purchase' class='btn btn-primary'>
                        <span>DECLINE CC</span>
                    </button>";
                }
            }
        } 
        if($_SESSION['strAdminPriviledge'] == 9 && $arrPurchaseData['approvalCC'] != 1){
            if($arrPurchaseData['approvalPM'] == 2){
                echo "
                <button type='submit' name='dispmEdit' id='dispmEdit' value='Disedit PM Purchase' class='btn btn-primary'>
                    <span>DECLINE PM</span>
                </button>
                <button type='submit' name='pmEdit' id='pmEdit' value='Edit PM Purchase' class='btn btn-primary'>
                    <span>APPROVE PM</span>
                </button>
                ";
            }
            else{
                echo "
                <h5 style='color:blue;'>Data menunggu pembuatan Order Pembelian</h5><p class='spacer'>&nbsp;</p>
                ";
            }
        }

        if(($_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 5) && $arrPurchaseData['pror_rawstatus'] != 1 ){          

        if ($arrPurchaseData['pror_rawstatus'] < 4) {
              # code...
            if($bolBtnEdit){
                echo "<button type='submit' name='editPurchase' id='editPurchase' value='Edit Purchase' class='btn btn-warning'>
                <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
            </button>
            <button type='submit' name='backPurchase' id='backPurchase' value='Cancel Edit Purchase' class='btn btn-danger'>
                <span class='glyphicon glyphicon-repeat' aria-hidden='true'></span>
            </button>";
            }
            else{
                echo "<button type='submit' name='disapprovePB' id='disapprovePB' value='Approve PB' class='btn btn-primary'>
                    <span>DISAPPROVE PB</span>
                </button>";

            }
          }  

            if($arrPurchaseData['approvalCC'] == 2 && $arrPurchaseData['pror_rawstatus'] < 4){
                echo "<button type='submit' name='ccEdit' id='ccEdit' value='Edit CC Purchase' class='btn btn-primary'>
                    <span>APPROVE CC</span>
                </button>";
                echo "
                <button type='submit' name='disccEdit' id='disccEdit' value='Disedit CC Purchase' class='btn btn-primary'>
                    <span>DECLINE CC</span>
                </button>";
            }
            else if($arrPurchaseData['approvalCC'] == -1){
                echo "<h5 style='color:blue;'>PB ini tidak membutuhkan persetujuan CC</h5><p class='spacer'>&nbsp;</p>";
            }
            else if($arrPurchaseData['approvalCC'] == 3){
                echo "<h5 style='color:blue;'>Data telah di approve oleh PM</h5><p class='spacer'>&nbsp;</p>";
            }
            else{
                echo "<h5 style='color:blue;'>Data menunggu approval dari PM</h5><p class='spacer'>&nbsp;</p>";
            }

            if($arrPurchaseData['approvalPM'] == 2 && $arrPurchaseData['pror_rawstatus'] < 4){
            echo "<button type='submit' name='pmEdit' id='pmEdit' value='Edit PM Purchase' class='btn btn-primary'>
                    <span>APPROVE PM</span>
                </button>";
            echo "            
                <button type='submit' name='dispmEdit' id='dispmEdit' value='Disedit PM Purchase' class='btn btn-primary'>
                     <span>DECLINE PM</span>
                </button>";
            }
            else if($arrPurchaseData['pror_rawstatus'] == 4){
                echo "<h5 style='color:blue;'>Data sudah terdapat di Order Pembelian</h5><p class='spacer'>&nbsp;</p>";
            }
            else if($arrPurchaseData['approvalPM'] == 1){
                echo "<h5 style='color:blue;'>Data di decline oleh PM </h5><p class='spacer'>&nbsp;</p>";
            }
            else{
                echo "<h5 style='color:blue;'>Data menunggu pembuatan Order Pembelian</h5><p class='spacer'>&nbsp;</p>";
            }
        }


        if(($priviledge == 2 || $priviledge == 6 || $priviledge == 5) && $arrPurchaseData['pror_rawstatus'] < 4){
            echo "
            <br><br>
            <button type='submit' name='closePB' id='closePB' value='Close PB' class='btn btn-danger'>
                <span>CLOSE PB</span>
            </button>";
        }
        ?>
    </div>
    
    <?php
    $rawStatus = $arrPurchaseData['pror_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
    
    if($bolBtnEdit){
        echo '<input type="hidden" name="status_pb" id="status_pb" value="1"/>';
    }
    else{
        echo '<input type="hidden" name="status_pb" id="status_pb" value="2"/>';
    }
    ?>
    </div>
<textarea style="display: none"><?=var_dump($this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus']));?></textarea>
<input type="hidden" name="idsubkon" id="idsubkon" value="<?= $arrPurchaseData['idSubKontrak']  ?>"/>
<input type="hidden" name="jumlah_delete" id="jumlah_delete"/>
<input type="hidden" name="kumpulan_delete_id" id="kumpulan_delete_id"/>
<input type="hidden" name="kumpulan_prod_id" id="kumpulan_prod_id"/>
<input type="hidden" id="jumlah_baru" name="jumlah_baru" value=""/>
<input type="hidden" id="undoClose" name="undoClose" value=""/>
<input type="hidden" id="dataClose" name="dataClose" value=""/>
<input type="hidden" id="IDSupp" value="<?=$suppid?>"/>
<input type="hidden" name="prorCode" id="prorCode" value="<?=$arrPurchaseData['pror_code']?>"/>
<input type="hidden" name="prorID" id="prorID" value="<?=$arrPurchaseData['id']?>"/>
<input type="hidden" id="InternalSupp" value="<?=$suppinternal?>"/>
<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItemBonusAwal" name="totalItemBonusAwal" value="<?=$intTotalBonusItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus" value="0"/>

</form></div>