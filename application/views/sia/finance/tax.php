<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('finance/tax', NULL, FALSE)?>" class="form-inline pull-right" style="">
    <div class="input-group" style="max-width:250px;">
	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Name" />
	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <p class="spacer">&nbsp;</p>
    <form name="frmAddTax" id="frmAddTax" method="post" action="<?=site_url('finance/tax', NULL, FALSE)?>" class="frmShop">
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-name')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-total')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-date')?></th>
            <th>Include</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrTax)):
    foreach($arrTax as $e): ?>
        <tr>
            <td>
                <?php if($e['numbertipe']<5){?>
                <a href="<?=site_url($e['tipe']."/view/".$e['id'], NULL, FALSE)?>">
                    <?=$e['kode']?></a>
                <?php }else {?>
                    <?=$e['kode']?>
                <?php }?>
            </td>
            <td><?=$e['nama']?></td>
            <td><?=setPrice($e['total'])?></td>
            <td><?=formatDate2($e['tanggal'],'d F Y')?></td>
            <td>
                <input type="radio" name="taxed<?=$e['numbertipe']?>[<?=$e['id']?>]" value="0" <?php if($e['checked']=='0') echo "checked";?>> Tidak<br>
                <input type="radio" name="taxed<?=$e['numbertipe']?>[<?=$e['id']?>]" value="1" <?php if($e['checked']=='1') echo "checked";?>> Ya
                <input type="checkbox" name="change<?=$e['numbertipe']?>[<?=$e['id']?>]" id="change<?=$e['numbertipe']?>X<?=$e['id']?>" unchecked hidden />
            </td>

        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table>
    </div>
    <button type="submit" name="smtMakeTax" value="Make Tax" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button>
    <?=$strPage?>
</div>