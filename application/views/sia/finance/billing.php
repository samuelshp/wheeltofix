<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-billing');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> '.$strPageTitle, 'link' => site_url('finance/billing', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12 col-md-4 pull-right">
    <form name="searchForm" method="post" action="<?=site_url('finance/billing', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
    	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Code / Supplier / Costumer" />
    	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form>
</div>
<form name="frmAddBilling" id="frmAddBilling" method="post" action="<?=site_url('finance/billing', NULL, FALSE)?>" class="frmShop">
<div class="col-xs-12 col-md-8 pull-left"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?></h3></div>
    <div class="panel-body">
        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmanname')?></label><input type="text" name="txtSalesman"  class="form-control required" /></div></div>
    </div>
</div></div>
    <p class="spacer">&nbsp;</p>
    <div class="pull-left" style="margin-top:-25px;"><?=$strPage?></div>

<div class="col-xs-12"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-money"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-addbilling')?></h3></div>
    <div class="table-body">
    <div class="table-responsive" style="padding:15px;"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><i class="fa fa-list"></i></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmanname')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-customername')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-total')?></th>
    </tr></thead>
    <tbody><?php
    // Display data in the table
    if(!empty($arrBilling)):
        foreach($arrBilling as $e): ?>
        <tr>
            <td class="cb"><?php
            if($e['bolBtnPrint']): ?>
                <input type="checkbox" name="print[<?=$e['id']?>]" id="print<?=$e['id']?>" value="<?=$e['id']?>"/><?php
            endif;?>
            </td>
            <td class="code"><?=$e['invo_code']?></td>
            <td class="title2"><?=formatPersonName('ABBR',$e['sals_name'],$e['sals_address'],$e['sals_phone'])?>
                <input type="hidden" name="salsid[<?=$e['id']?>]" value="<?=$e['sals_id']?>"/>
            </td>
            <td class="title2"><?=formatPersonName('ABBR',$e['cust_name'],$e['cust_address'],$e['cust_city'],$e['cust_phone'])?></td>
            <td><?=formatDate2($e['invo_date'],'d F Y')?></td>
            <td class="subTotal"><?=setPrice($e['invo_grandtotal'])?></td>
        </tr>
        <?php
        endforeach;
    else: ?>
            <tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
    </tbody>
    </table></div>
    </div>
</div></div>

    <div class="col-xs-6">
        <div class="pull-left"><button type="submit" name="smtMakeBilling" value="Make Billing" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button></div>
        
        <p class="spacer">&nbsp;</p>
        <div class="pull-left" style="margin-top:-25px;"><?=$strPage?></div>
    </div>
</form>