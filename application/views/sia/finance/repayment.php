<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-repayment');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => site_url('finance/repaymentbrowse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<style type="text/css">
    .brHide {display:none;}
</style>

<div class="col-xs-12 col-md-4 pull-right">
<form name="searchForm" method="post" action="<?=site_url('finance/repayment', NULL, FALSE)?>" class="form-inline pull-right" style="">
    <div class="input-group" style="max-width:250px;">
	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Code / Supplier / Costumer" />
	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>
</div>
<form name="frmAddPayment" id="frmAddRepayment" method="post" action="<?=site_url('finance/repayment', NULL, FALSE)?>" class="frmShop"><div class="form-group">
<div class="col-xs-12 col-md-6 pull-left"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
    <div class="panel-body">
    <div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
    </div>
</div></div>
    <p class="spacer">&nbsp;</p>
    <div class="col-xs-12" style="margin-top:-25px;"><?=$strPage?></div>

<div class="col-xs-12"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-usd"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-addrepayment')?></h3></div>
    <div class="table-body">
    <div class="table-responsive" style="padding:15px;">
        <p class="text-info">*) Untuk memilih tagihan mana yang akan dilunaskan, silakan centang salah satu pilihan pembayaran. Jika tidak dicentang, tagihan tidak akan disertakan.</p>
    <table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-invoicedate')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-total')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-type')?></th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrBilling)):
    foreach($arrBilling as $e): ?>
        <tr>
            <td><?=$e['prch_code']?></td>
            <td><?=formatPersonName('ABBR',$e['supp_name'],$e['supp_address'],$e['supp_phone'])?></td>
            <td><?=formatDate2($e['prch_date'],'d F Y')?></td>
            <td><?=setPrice($e['prch_grandtotal'])?></td>
            <td>
                <?php if($e['tipe']=="1"){?>
                    <input type="hidden" name="suppid[<?=$e['id']?>]" value="<?=$e['supp_id']?>"/>
                    <label><input type="checkbox" name="paid[<?=$e['id']?>]" id="tun<?=$e['id']?>" value="4"/> Tunai</label>&nbsp;&nbsp;
                    <!--<label><input type="checkbox" name="paid[<?/*=$e['id']*/?>]" id="kreditn<?/*=$e['id']*/?>" value="5"/> Kredit</label>&nbsp;&nbsp;-->
                    <label><input type="checkbox" name="paid[<?=$e['id']?>]" id="bgn<?=$e['id']?>" value="6"/> BG</label>
                    <br class="brHide" id="1br<?=$e['id']?>" />
                    <input type="text" name="paidBank[<?=$e['id']?>]" id="paidBank<?=$e['id']?>" class="form-control" placeholder="Bank Name"/>
                    <br class="brHide" id="2br<?=$e['id']?>" />
                    <input type="text" name="paidDescription[<?=$e['id']?>]" id="paidDescription<?=$e['id']?>" class="form-control" placeholder="BG number"/>
                    <br class="brHide" id="3br<?=$e['id']?>" />
                    <input type="text" name="paidDate[<?=$e['id']?>]" id="paidDate<?=$e['id']?>" class="form-control" placeholder="Paid Date"/>
                    <br />
                    <input type="text" name="paidKeterangan[<?=$e['id']?>]" class="form-control" placeholder="Description"/>
                <?php }else if($e['tipe']=="2"){?>
                    <input type="hidden" name="suppidretur[<?=$e['id']?>]" value="<?=$e['supp_id']?>"/>
                    <label><input type="checkbox" name="retured[<?=$e['id']?>]" id="ret<?=$e['id']?>" value="3"/> Retur </label>
                    <br />
                    <input type="text" name="paidKeteranganRetur[<?=$e['id']?>]" class="form-control" placeholder="Description"/>
                <?php } ?>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    </div>
</div></div>
    <p class="spacer">&nbsp;</p>
    <div class="col-xs-12 form-group"><button type="submit" name="smtMakeRepayment" value="Make Payment" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
    <div class="col-xs-12"><?=$strPage?></div>
</form>