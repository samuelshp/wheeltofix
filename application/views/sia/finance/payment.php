<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-payment');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('finance/paymentbrowse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<style type="text/css">
    .brHide {display:none;}
</style>

<div class="col-xs-12 col-md-4 pull-right">
    <form name="searchForm" method="post" action="<?=site_url('finance/payment', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
    	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Code / Salesman / Costumer" />
    	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form>
</div>
<form name="frmAddPayment" id="frmAddPayment" method="post" action="<?=site_url('finance/payment', NULL, FALSE)?>" class="frmShop">
<div class="col-xs-12 col-md-6 pull-left"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
    <div class="panel-body">
        <div class="form-group">
        <div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div>
        </div>
    </div>
</div></div>
    <p class="spacer">&nbsp;</p>
    <div class="pull-left" style="margin-top:-25px;"><?=$strPage?></div>

<div class="col-xs-12"><div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-addpayment')?></h3></div>
    <div class="table-body">
    <div class="table-responsive" style="padding:15px;">
        <p class="text-info">*) Untuk memilih tagihan mana yang akan dilunaskan, silakan centang salah satu pilihan pembayaran. Jika tidak dicentang, tagihan tidak akan disertakan.</p>
    <table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmanname')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-customername')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-total')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-type')?></th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrBilling)):
    foreach($arrBilling as $e): ?>  
        <tr>
            <td><?=$e['invo_code']?></td>
            <td><?=formatPersonName('ABBR',$e['sals_name'],$e['sals_address'],$e['sals_phone'])?></td>
            <td><?=formatPersonName('ABBR',$e['cust_name'],$e['cust_address'],$e['cust_city'],$e['cust_phone'])?></td>
            <td><?if($e['tipe']=="1"){echo formatDate2($e['invo_date'],'d F Y');}else{echo"-";}?></td>
            <td><?=setPrice($e['invo_grandtotal'])?></td>
            <td>
                <?php if($e['tipe']=="1"){?>
                    <input type="hidden" name="salsid[<?=$e['id']?>]" value="<?=$e['sals_id']?>"/>
                    <label><input type="checkbox" name="paid[<?=$e['id']?>]" id="tun<?=$e['id']?>" value="4"/> Tunai</label>&nbsp;&nbsp;
                    <!--<label><input type="checkbox" name="paid[<?/*=$e['id']*/?>]" id="kreditn<?/*=$e['id']*/?>" value="5"/> Kredit</label>&nbsp;&nbsp;-->
                    <label><input type="checkbox" name="paid[<?=$e['id']?>]" id="bgn<?=$e['id']?>" value="6"/> BG</label>
                    <br class="brHide" id="1br<?=$e['id']?>" />
                    <input type="text" name="paidBank[<?=$e['id']?>]" id="paidBank<?=$e['id']?>" class="form-control" placeholder="Bank Name"/>
                    <br class="brHide" id="2br<?=$e['id']?>" />
                    <input type="text" name="paidDescription[<?=$e['id']?>]" id="paidDescription<?=$e['id']?>" class="form-control" placeholder="BG number"/>
                    <br class="brHide" id="3br<?=$e['id']?>" />
                    <input type="text" name="paidDate[<?=$e['id']?>]" id="paidDate<?=$e['id']?>" class="form-control" placeholder="Paid Date"/>
                    <br />
                    <input type="text" name="paidKeterangan[<?=$e['id']?>]" class="form-control" placeholder="Description"/>
                <?php }else if($e['tipe']=="2"){?>
                    <input type="hidden" name="salsidretur[<?=$e['id']?>]" value="<?=$e['sals_id']?>"/>
                    <label><input type="checkbox" name="retured[<?=$e['id']?>]" id="ret<?=$e['id']?>" value="3"/> Retur </label>
                    <br />
                    <input type="text" name="paidKeteranganRetur[<?=$e['id']?>]" class="form-control" placeholder="Description"/>
                <?php } ?>
            </td>

        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    </div>
</div></div>

    <div class="col-xs-6">
        <div class="pull-left"><button type="submit" name="subSave" value="Make Payment" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
        <p class="spacer">&nbsp;</p>
        <div class="pull-left" style="margin-top:-25px;"><?=$strPage?></div>
    </div>
</form>