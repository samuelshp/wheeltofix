<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('input[name="txtDate"]').datepicker({
    dateFormat: "yy/mm/dd"
});
$('input[name*="paidDate"]').datepicker({
    dateFormat: "yy/mm/dd"
});
$("#frmAddPayment").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddPayment").submit(function() {
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-areyousurepayment')?>") == false) return false;
	
	return true;
});

/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});

$("input:checkbox").click(function() {
    var at=this.id.substring(this.id.indexOf('n')+1,this.id.length);
    if ($(this).is(":checked")) {
        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
        $(group).prop("checked", false);
        $(this).prop("checked", true);
        $("#paidDescription"+at).val('');
        $("#paidDate"+at).val('');
        $("#paidBank"+at).val('');
        if($(this).val()=='4'){
            $("#paidDescription"+at).hide();
            $("#paidBank"+at).hide();
            $("#paidDate"+at).hide();
            $("#1br"+at).hide();
            $("#2br"+at).hide();
            $("#3br"+at).hide();
        }else if($(this).val()=='5'){
            $("#paidDescription"+at).hide();
            $("#paidBank"+at).hide();
            $("#paidDate"+at).hide();
            $("#1br"+at).hide();
            $("#2br"+at).hide();
            $("#3br"+at).hide();
        }else if($(this).val()=='6'){
            $("#paidDescription"+at).show();
            $("#paidBank"+at).show();
            $("#paidDate"+at).show();
            $("#1br"+at).show();
            $("#2br"+at).show();
            $("#3br"+at).show();
        }
    } else {
        $(this).prop("checked", false);
        $("#paidDescription"+at).hide();
        $("#paidDate"+at).hide();
        $("#paidDescription"+at).val('');
        $("#paidDate"+at).val('');
        $("#paidBank"+at).hide();
        $("#paidBank"+at).val('');
        $("#1br"+at).hide();
        $("#2br"+at).hide();
        $("#3br"+at).hide();
    }
});

$('input[name*="paidDescription"]').hide();
$('input[name*="paidDate"]').hide();
$('input[name*="paidBank"]').hide();

});</script>