<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-repayment');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('finance/repaymentbrowse', NULL, FALSE)?>" class="form-inline pull-right" style="">
    <div class="input-group" style="max-width:250px;">
	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Code / Salesman / Costumer" />
	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('finance/repayment', NULL, FALSE)?>" target="_blank" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></th>
            <th class="action">Action</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrBilling)):
    foreach($arrBilling as $e): ?>
    <form name="frmChangePayment[<?=$e['id']?>]" id="frmChangePayment<?=$e['id']?>" method="post" action="<?=site_url('finance/repaymentbrowse/'.$e['id'], NULL, FALSE)?>" class="frmShop">
        <tr>
            <td><?=$e['repa_code']?></td>
            <td><?=formatPersonName('ABBR',$e['supp_name'],$e['supp_address'],$e['supp_phone'])?></td>
            <td><?php
                if($e['repa_type'] == '4') echo "Tunai <br />";
                else if($e['repa_type'] == '3') echo "Retur <br />";
                else if($e['repa_type'] == '5') echo "Kredit <br />";
                else if($e['repa_type'] == '6') echo "BG <br />";

                echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-total').": ".setPrice($e['prch_grandtotal']);

                if($e['repa_type']!='4' && $e['repa_type']!='3')
                    echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-duedate').": ".formatDate2($e['repa_date'],'d F Y').'<br />';

                if($e['repa_type'] == '6') echo 
                    "Bank: ".$e['repa_bank_id']."<br />".
                    "BG Number: ".$e['repa_bg_number'].'<br />';

                if($e['bolBtnEdit']) {
                    ?><input type="text" class="form-control" name="txtDescription<?=$e['id']?>" value="<?=$e['repa_description']?>" placeholder="Description" /><?php
                } else if(!empty($e['repa_description'])) echo 
                    "Description: ".$e['repa_description'];
                ?>  
            </td>
            <td class="action">
                <input type="hidden" name="selStatus" id="selStatus<?=$e['id']?>" value="<?=$e['repa_status']?>"/>
                <?php
                if($e['bolBtnEdit']): ?>
                    <button type="submit" name="subSave" value="Edit" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?></button><?php
                endif;
                if($e['bolBtnDelete']): ?>
                    <button type="submit" name="subDelete" value="Delete" onclick="return confirm('<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-confirmdelete')?>');" class="btn btn-danger pull-right"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></button><?php
                endif;
                if($e['bolBtnPrint']): ?>
                    <button type="submit" name="subPrint" value="Print" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button><?php
                endif;
                if($e['bolBtnApprove']): ?>
                    <button type="submit" name="subSave" value="Edit" onClick="$('#selStatus<?=$e['id']?>').val(2);" class="btn btn-info"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-approve')?></button><?php
                endif;
                if($e['bolBtnDisapprove']): ?>
                    <button type="submit" name="subSave" value="Edit" onClick="$('#selStatus<?=$e['id']?>').val(1);" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-disapprove')?></button><?php
                endif;
                if($e['bolBtnRollback']): ?>
                    <button type="submit" name="subSave" value="Edit" onClick="$('#selStatus<?=$e['id']?>').val(0);" class="btn btn-default"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-rollback')?></button><?php
                endif; ?>
            </td>

        </tr>
    </form><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table>
    </div>
    <?=$strPage?>
</div>