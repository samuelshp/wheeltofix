<?php
// created by patricklipesik
$strPageURL = 'akuntansilabarugi';
$strPageTitle = 'Laba Rugi';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
	0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
//TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
	1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);
$print_mode = 1;

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_accounting/'.$strPageURL, NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Periode</label>
				<input type="text" name="txtPeriode" value="<?php if(!empty($strPeriode)) echo $strPeriode; else echo date('Y-m'); ?>" class="required form-control postingDate" maxlength="7" />
			</div>
		</div>
		
		<div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary">
				<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?>
			</button><?php
$strPrintURL = site_url('report_accounting/'.$strPageURL.'?print=true&periode='.$strPeriode, NULL, FALSE);?>
			<a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
			<a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a>
		</div>
	</form>
	<p class="spacer">&nbsp;</p>
	<hr />
	<div class="col-xs-12"><?=$strPage?></div>
	<?php
	$spCurr = 'BASE';
	$spSign = TRUE;
	$spPass = FALSE;
	$io = 2;
	include_once('../../sia_print/reportakuntansilabarugi_export.php');
	?>
	
	<?=$strPage?>
</div>