<?php
// created by patricklipesik
$strPageURL = 'akuntansiprofit';
$strPageTitle = 'Profit Per Barang';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
	0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
//TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
	1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_accounting/'.$strPageURL, NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Awal</label>
				<input type="text" name="txtStart" value="<?php if(!empty($strStart)) echo $strStart; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Akhir</label>
				<input type="text" name="txtEnd" value="<?php if(!empty($strEnd)) echo $strEnd; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		
		<div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary">
				<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?>
			</button><?php
$strPrintURL = site_url('report_accounting/'.$strPageURL.'?print=true&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
			<a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
			<a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a>
		</div>
	</form>
	<div class="pull-left col-xs-12 col-md-6">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3">Periode</div>
					<div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
					<div class="col-md-3">Hari ini</div>
					<div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
				</div>
			</div>
		</div>
	</div>
	<p class="spacer">&nbsp;</p>
	<hr />
	<div class="col-xs-12"><?=$strPage?></div>
	<p class="spacer">&nbsp;</p>
	<div class="table-responsive">
		<table class="table table-bordered table-condensed table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Produk</th>
					<th>Penjualan</th>
					<th>HPP</th>
					<th>Selisih</th>
				</tr>
			</thead>
			<tbody><?php
			$sumGrandTotal1 = 0;
			$sumGrandTotal2 = 0;
			$sumGrandTotal3 = 0;
			if(!empty($arrItems)):
				foreach($arrItems as $e):
					$selisih = $e['subtotal'] - $e['hpp'];
			?>
					<tr>
						<td><?=$e['nourut']?></td>
						<td><?=$e['produk']?></td>
						<td><?=setPrice($e['subtotal'])?></td>
						<td><?=setPrice($e['hpp'])?></td>
						<td><?=setPrice($selisih)?></td>
					</tr>
				<?php
				$sumGrandTotal1 += $e['subtotal'];
				$sumGrandTotal2 += $e['hpp'];
				$sumGrandTotal3 += $selisih;
				endforeach;
			else :?>
				<tr><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
			endif;?>
			</tbody>
			<thead>
				<tr>
					<th colspan="2" style="text-align:right;">Grand Total</th>
					<th><?=setPrice($sumGrandTotal1)?></th>
					<th><?=setPrice($sumGrandTotal2)?></th>
					<th><?=setPrice($sumGrandTotal3)?></th>
				</tr>
			</thead>
		</table>
	</div>
	<?=$strPage?>
</div>