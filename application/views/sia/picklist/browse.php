<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
1 => array('title' => '<i class="fa fa-folder"></i> Pick List')
);

include_once(APPPATH.'/views/'.$strViewFolder.'/contentheader.php'); ?>



<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('picklist/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group">
			<span class="input-group-addon">
				<i class="fa fa-file"></i>
			</span>
            <input type="text" name="txtPickListCode" value="<?=$this->input->get('txtPickListCode')?>" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-searchPickListCode')?>" />

			<span class="input-group-addon">
				<i class="fa fa-folder"></i>
			</span>
            <input type="text" name="txtSOID" value="<?=$this->input->get('txtSOID')?>" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-searchSOID')?>" />
            
			<span class="input-group-addon">
				<i class="fa fa-table"></i>
			</span>
            <input type="text" name="txtDate" value="<?=$this->input->get('txtDate')?>" class="form-control jwDateTime" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-searchDate')?>" />
			
            <span class="input-group-btn"><button type="submit" name="plSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
        </div>
    </form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('picklist/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
				<thead><tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-codepicklist')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-codeSO')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-warehousename')?></th>
					<th class="action">Action</th>
				</tr></thead>
				<tbody><?php
				// Display data in the table
				if(!empty($arrPickList)):
					// echo var_dump($arrPickList);
					foreach($arrPickList as $e): ?>
						<tr>
						<td><?=$e['pcls_code']?></td>
						<td><?=$e['inor_code']?></td>
						<td><?=$e['ware_name']?></td>
						<td>
						    <?php if($bolAllowView): ?><a href="<?=site_url('picklist/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
						</td>
						</tr><?php
					endforeach;
				else: ?>
					<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-nodata')?></td></tr><?php
				endif; ?>
				</tbody>
			</table></div>
    	</div>
    </div>
	<?=$strPage?>
</div>