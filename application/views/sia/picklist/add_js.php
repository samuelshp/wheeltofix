<script type="text/javascript">
var $arrSelectedSO;
var selectedSO;
$("#kodeSO").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('api/sopicklist/list?h="+userHash+"&filter_inor_status=2&filter_inor_code=*')?>"+$("#kodeSO").val()+"*",
            beforeSend: function() {                
                $("#loadSO").html('Loading data');
            },
            complete: function() {                
                $("#loadSO").html('');
            },
            success: function(result){
                if(!result.success) $("#loadSO").html('No data');
                $arrSelectedSO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['so_id'];
                    var codeSO = arr['inor_code'];
                    display.push({label: codeSO, value: codeSO, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedSO = $.grep($arrSelectedSO, function(el) {            
            return el.so_id == ui.item.id
        });                                
        var itemSelected = $(selectedSO)[0];        
        $("#intSOID").val(itemSelected.so_id);
        $("#intWarehouseID").val(itemSelected.inor_warehouse_id);
        $("#intProductID").val(itemSelected.inor_product_id);
        $("#nameWarehouse").val(itemSelected.ware_name);
        loadSOItem(itemSelected.so_id);
    }
});

function loadSOItem(id) {
    return $.ajax({
        type : "GET",
        dataType : "json",
        url : "<?=site_url('api/sopicklistitem/list?h="+userHash+"&filter_so-id=')?>" + id,
        cache : false,
        success : function(result) {
            if(result.count > 0) {
                $('tr.info .noData').hide();
                $.each(result.data, function(index, arr){
                    $('#dataItem').append(
                        '<tr name="itemRow">'+
                            '<td>' + arr['prod_title'] + '</td>'+
                            '<td><input readonly class="form-control input-sm qty" value="' + arr['inoi_qty'] + '" id="maxQty" name="maxQty[]" type="text" placeholder="' + arr['inoi_qty'] + '"></td>'+
                            '<td><div class="input-group"><input class="form-control input-sm qty" type="text" id="quantity_processed" name="quantity_processed[]" value="' + arr['selisih'] + '"><div class="input-group-addon">' + arr['unit_title'] + '</div></div></td>'+
                            '<td class="cb"><input type="checkbox" name="chbxPick[]"></td>'+
                            '<input type="hidden" name="idProduct[]" id="idProduct" value="' + arr['inoi_product_id'] + '">' +
                            '<input type="hidden" name="bQty[]" id="bQty" value="' + arr['inoi_qty_processed'] + '">' +
                            '<input type="hidden" name="aQty[]" id="aQty" value="0">' +
                        '</tr>'
                    );
                }); 
            }
            $('#dataItem').trigger("chosen:updated");
        }
    });
}

$("#frmAddPickList").submit(function() {
    if($("#frmAddPickList input:checkbox:checked").length) {
        return true;
    } else {
        alert("Pilih satu barang");
        return false;
    }
});


$("#dataItem tbody").on('keyup', '#quantity_processed', function(e) {
    if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
        calculateQuantity(e);
    }
});

function calculateQuantity(e) {
    var quantityTotal = 0;
    var currentRow = $(e.currentTarget).closest('tr');
    
    var bQty = parseFloat(currentRow.find("input[id^='bQty']").val());

    var mQty = parseFloat(currentRow.find("input[id^='maxQty']").val());
    
    var tQty = parseFloat(currentRow.find("input[id^='quantity_processed']").val());
    
    quantityTotal = bQty + tQty;

    if(quantityTotal > mQty) {
        alert("Quantity melebihi maksimal");
    } else {
        currentRow.find("input[id^='aQty']").val(quantityTotal);
    }
}
</script>