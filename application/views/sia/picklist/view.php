<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-folder"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title'), 'link' => site_url('picklist/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php if($arrPickList[0]['pcls_status'] == '3') {
    $bolBtnEdit = TRUE;
} ?>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-lastpicklist')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- Jump To -</option><?php
            if(!empty($arrPickList)) foreach($arrPickList as $e):?>
            <option value="<?=site_url('picklist/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['inor_code']?>] <?=$e['pcls_code']?></option><?php
            endforeach; ?>
        </select>
</div></div>

<?php if(!empty($arrPickList)) { ?>
    <?php foreach($arrPickList as $e) { ?>
        <div class="col-xs-12">
        <form name="frmAddInvoice" id="frmAddInvoice" class="frmShop" method="post" action="<?=site_url('picklist/view/'.$e['id'], NULL, FALSE)?>">
            <div class="row">
                <!-- Kode Pick List -->
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-id-card"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-datapicklist')?></h3></div>
                        <div class="panel-body">
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-code')?></div> <?= $e['pcls_code']; ?>
                        </div>
                    </div>
                </div>
                <!-- Kode Pick List -->

                <!-- Kode SO -->
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-id-card"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-dataSO')?></h3></div>
                        <div class="panel-body">
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-code')?></div> <?= $e['inor_code']; ?>
                        </div>
                    </div>
                </div>
                <!-- Kode SO -->

                <!-- Nama Gudang -->
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-datawarehouse')?></h3></div>
                        <div class="panel-body">
                            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-name')?></div>
                            <div class="col-xs-8"></div> <?= $e['ware_name']; ?>
                        </div>
                    </div>
                </div>
                <!-- Nama Gudang-->

                <div class="col-md-12">
                    <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-itempurchased')?></h3></div>
                    <div class="panel-body">
                        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="updateItem">
                            <thead>
                            <tr>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-item')?></th>
                                <th style="width : 15%"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-quantitytaken')?></th>
                                <th style="width:10%">Pick</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($arrPickList)) { ?>
                                <?php foreach($arrPickList as $e) { ?>
                                    <tr>
                                        <td><?= $e['prod_title'] ?></td>
                                        <td><div class="input-group"><input class="form-control input-sm qty editable" type="text" id="quantity_processed<?=$i?>" value="<?=$e['pcli_qty']?>" placeholder="0" name="quantity_processed[]">
                                            <div class="input-group-addon"><?=$e['unit_title']?></div>
                                            </div></td>
                                        <td class="cb"><input type="checkbox" name="chbxPick[]" checked ></td>
                                        <input readonly type="hidden" name="maxQty[]" id="maxQty" value="<?= $e['inoi_qty'];?>">
                                        <input readonly type="hidden" name="procQty[]" id="procQty" value="<?= $e['inoi_qty_processed'];?>">
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                            <?php } ?>
                            </tbody>
                        </table></div> <!--table-responsive-->
                        <p class="spacer">&nbsp;</p>
                        
                    </div> <!--panel-body-->
                </div>
                <div class="form-group">
                <?php foreach($arrPickList as $e) {
                    $rawStatus = $e['pcls_status'];
                    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php');
                } ?>
                </div>
            </div> <!--class row-->
            <input type="hidden" name="pcli_id" id="pcli_id" value="<?=$e['id']?>">
            <input type="hidden" name="intSOID" id="intSOID" value="<?=$e['inor_id']?>">
            <input type="hidden" name="idProduct[]" id="idProduct[]" value="<?=$e['pcli_product_id']?>">
        </form>
        </div>
    <?php } ?>
<?php } ?>