<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
1 => array('title' => '<i class="fa fa-folder"></i> Pick List')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <form name="frmAddPickList" id="frmAddPickList" class="frmShop" method="post" action="<?=site_url('picklist/add/'.$strMode, NULL, FALSE)?>">
        <div class="row">
            <!-- Kode SO -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-list-ul"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-dataSO')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-code')?></h4>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="kodeSO" id="kodeSO" class="form-control " placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selectSO')?>" autocomplete="off"/>
                                <label class="input-group-addon" id="loadSO"></label>
                                <input type="hidden" name="intSOID" id="intSOID">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Kode SO -->
            <!-- Data Gudang -->
            <div class="col-md-4">
                <div class="panel panel-primary" id="panelWarehouse">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-datawarehouse')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-name')?></h4>
                        <input readonly id="nameWarehouse" name="nameWarehouse" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-selecwarehouse')?>">
                        <input type="hidden" name="intWarehouseID" id="intWarehouseID">
                    </div>
                </div>
            </div>
            <!-- Data Gudang -->
            <!-- Tanggal -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-date')?></h3></div>
                    <div class="panel-body">
                        <!-- Dibuat -->
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                    </div>
                </div>
            </div>
            <!-- Produk -->
            <div class="col-md-12">
                <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-itempurchased')?></h3></div>
                <div class="panel-body">
                    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" nama="dataItem[]" id="dataItem">
                        <thead>
                        <tr>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-item')?></th>
                            <th style="width:20%;"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-quantity')?></th>
                            <th style="width:20%"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-quantitytaken')?></th>
                            <th style="width:20%">Pick</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tbody> 
                        <tr class="info">
                            <td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                        </tr>
                        </tbody>
                    </table></div> <!--table-responsive-->
                    <p class="spacer">&nbsp;</p>
                </div> <!--panel-body-->
            </div>
            <div class="form-group"><button type="submit" name="smtMakePickList" id="smtMakePickList" value="Make Pick List" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
        </div>        
    </form>
</div>