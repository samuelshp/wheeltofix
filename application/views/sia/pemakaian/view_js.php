<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {

var numberItem=0;
var selKontrak = $('#selKontrak').val();
loadChooseProduct(selKontrak);

$('.qty').autoNumeric('init', autoNumericOptionsRupiah);

$(document).on('keyup','input.qty[data-max]',function () {
    var max = parseFloat($(this).data('max'));
    var current = parseFloat($(this).autoNumeric('get'));       
    $(this).parent('.input-group').removeClass('has-error');
    $('input.qty[data-max][data-toggle="tooltip"]').tooltip('destroy');
    $(this).removeAttr("title");
    if (current > max) {        
        $(this).attr("title","Nilai yang diinputkan melebihi maximum.");
        $('input.qty[data-max][data-toggle="tooltip"]').tooltip();
        $(this).parent('.input-group').addClass('has-error');
        return false;
    }    
});

$("button#smtEditUsage").click(function() {
    $(this).hide();
    $("div#editMode").show();    
    $('.editable').each(function(i){
        var self = $(this);
        try{
            self.removeAttr('readonly');
            self.removeAttr('disabled');
        }catch(err){
            console.log("Error occured when trying to edit.");
        }
    });
});

$("#frmAddPemakaian").submit(function() {

    if($(".has-error").length){
        alert("Terdapat salah input data. Mohon dicek kembali."); 
        return false;
    }

    if($("#selProyek").val() == null || $("#selProyek").val() == 0){
        alert("Proyek tidak boleh kosong");
        return false;
    }

    if($("#selWarehouse").val() == null || $("#selWarehouse").val() == 0){
        alert("Gudang tidak boleh kosong");
        return false;
    }

    if($("#selKontrak").val() == null || $("#selKontrak").val() == 0 ){
        alert("Kontrak tidak boleh kosong");
        return false;
    }

    if($(".product").length == 0) {
        alert("Product tidak boleh kosong");
        return false;
    }
    
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-areyousure')?>") == false) return false;
    
    return true;
});

$('#selProyek').change(function(){
    var selProyek = $('#selProyek').val();   
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/kontrak/"+selProyek+"?h="+userHash+"&filter_status=2')?>",
        cache : false,
        success : function(result){
            loadWarehouse('kontrak',selProyek);
            loadSubkontrak(selProyek);
        }
    });
});

$('#selKontrak').change(function() {
    var selKontrak = $(this).val();   
    loadChooseProduct(selKontrak);
});

var i=0;
$('#selProduct').change(function(){
    var selKontrak = $('#selKontrak').val();   
    var selProduct = $('#selProduct').val();   
    var selWarehouse = $('#selWarehouse').val();
    loadProduct(selKontrak, selProduct, selWarehouse);
});

$('#selSubstitution').change(function(){
    var selKontrak = $('#selKontrak').val();   
    var selProduct = $('#selProduct').val();   
    var selWarehouse = $('#selWarehouse').val();
    var selSubstitution = $('#selSubstitution').val();
    var numberSubstitution = $('#numberSubstitution').val();
    $.ajax({
        url: "<?=site_url('api/material_pemakaian/list?h="+userHash+"&filter_subkontrak_id=')?>"+selKontrak+"&filter_material="+selSubstitution+"&filter_trst_warehouse_id="+selWarehouse, 
        cache: false,                       
        success: function(result){                            
            if (result.success){
                var subkontrak_material_id = result.data[0]['subkontrak_material_id'];
                var product = result.data[0]['prod_title'];
                var material = result.data[0]['material'];
                var qtyrap = result.data[0]['jumlah_awal'];
                var qty = result.data[0]['gudang_qty'];
                var satuan = result.data[0]['unit_title'];
                var max = 0;
                if(qtyrap>qty){
                    max = qty;
                }else{
                    max = qtyrap;
                }
                var readonly = "";
                if(qtyrap<=0 || qty<=0){
                    readonly = "readonly";
                }
                if(checkProduct(selSubstitution)){
                    $("#selectedItems tbody tr.info").hide();        
                    $("#selectedItems tbody").append(
                    '<tr>'+
                        '<td id="name'+i+'">'+product+'<input type="hidden" name="productID[]" class="required number form-control input-sm product" id="productID'+i+'" value="' + material + '"/><input type="hidden" name="subkontrakMaterialID[]" class="required number form-control input-sm product" id="subkontrakMaterialID'+i+'" value="' + subkontrak_material_id + '"/></td>'+
                        '<td><input type="text" readonly name="qtyRAP'+i+'" class="required number form-control input-sm qty" id="qtyRAP'+i+'" value="'+qtyrap+'" /></td>'+
                        '<td><input type="text" readonly name="qty'+i+'" class="required number form-control input-sm qty" id="qty'+i+'" value="'+qty+'" /></td>'+
                        '<td><div class="input-group"><input type="text" '+readonly+' name="qtyTotal[]" class="required form-control input-sm calculate qty" id="qtyTotal'+i+'" placeholder="0" data-max="'+max+'" /><div class="input-group-addon">'+satuan+'</div></div></td>'+
                        '<td style="width:45px;"><button type="button" style="width:40px;" class="btn btn-danger btn-sm delete" value="'+i+'" data-product-id="'+material+'" name="delete" id="delete" tabIndex="-1"><i class="fa fa-trash"></i></button></td>'+
                    '</tr>');
                    numberItem++;

                    if($('#name'+numberSubstitution).length > 0) {
                        $('#name'+numberSubstitution).closest("tr").remove();
                        numberItem--;
                    }

                    $("#numberSubstitution").val(i);
                    i++;
                }

                $('.qty').autoNumeric('init', autoNumericOptionsRupiah);
            }else{
                if(selSubstitution!=0){
                    alert("Subtitusi tidak tersedia.");
                }
                if($('#name'+numberSubstitution).length > 0) {
                    $('#name'+numberSubstitution).closest("tr").remove();
                    loadProduct(selKontrak, selProduct, selWarehouse);
                }
            }
        }
    });
});

$("#selectedItems").on('click', '.delete', function () {
    //if(numberItem>1){
        var at = $(this).val();
        $('#name'+at).closest("tr").remove();
        numberItem--;
        if(numberItem == 0) $("#selectedItems tbody tr.info").show();
    /*} else {
        alert('Item tidak boleh kosong');
    }*/
});

async function loadWarehouse(by,id) {
    if (by == 'kontrak') {
        var filter = "filter_kont-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $("#idwarehouse").val(arr['ware_id']);
                        $('#selWarehouse').append('<option value="' + arr['ware_id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }

    if (by == 'warehouse') {
        var filter = "filter_t-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $('#selWarehouse').append('<option value="' + arr['ware_id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }
}

function loadProject(){
    return $.ajax({
        url: "<?=site_url('api/kontrak/list?h="+userHash+"&filter_status=2&sort_kont_name=asc')?>",
        dataType: "json",
        method: "GET"        
    });  
}

function loadSubkontrak(kontrak, subkontselected = 0) {
    return $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + kontrak,
        cache : false,
        success : function(result){
            $('#selKontrak').html('<option disabled value="0" selected>Select your option</option>');
            if (result.count > 0) {
                $.each (result.data, function (index,arr) {  
                    var selected = (arr['id'] == subkontselected) ? 'selected' : '';                  
                    $('#selKontrak').append('<option value="' + arr['id'] + '" '+selected+'>' + arr['job'] + '</option>');
                });
            }
            $("#selKontrak").trigger("chosen:updated");
        }
    });
}

function checkProduct(selProduct) {
    var flag = true;
    $('.product').each(function(){
        if(selProduct==$(this).val()){
            flag = false;
        }
    });
    return flag;
}

function loadChooseProduct(selKontrak) {
    return $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/bahan_subkontrak_material/list?h="+userHash+"&filter_subkontrak_id=')?>" + selKontrak,
        cache : false,
        success : function(result){
            if(result['success']) {
                $('#selProduct').html('<option value="0" selected>Select your option</option>');
                $.each (result['data'], function (index,arr) {
                    $('#selProduct').append('<option value="' + arr['material'] + '">' + arr['prod_title'] + '</option>');
                });
                //$("#selProduct").prop('disabled', false);
            } else {
                $('#selProduct').html('<option disabled value="0" selected>No Data</option>');
                $("#selProduct").prop('disabled', true);
            }
        }
    });
}

function loadProduct(selKontrak, selProduct, selWarehouse) {
    return $.ajax({
        url: "<?=site_url('api/material_pemakaian/list?h="+userHash+"&filter_subkontrak_id=')?>"+selKontrak+"&filter_material="+selProduct+"&filter_trst_warehouse_id="+selWarehouse, 
        cache: false,                       
        success: function(result){                            
            loadSubstitution(selKontrak, selProduct, i);
            if (result.success){
                var subkontrak_material_id = result.data[0]['subkontrak_material_id'];
                var product = result.data[0]['prod_title'];
                var material = result.data[0]['material'];
                var qtyrap = parseFloat(result.data[0]['jumlah_awal']);
                var qty = parseFloat(result.data[0]['gudang_qty']);
                var satuan = result.data[0]['unit_title'];
                var readonly = "";
                var max = 0;
                if(qtyrap>qty){
                    max = qty;
                }else{
                    max = qtyrap;
                }
                if(qtyrap<=0 || qty<=0){
                    readonly = "readonly";
                }
                if(checkProduct(selProduct)){
                    $("#selectedItems tbody tr.info").hide();        
                    $("#selectedItems tbody").append(
                    '<tr>'+
                        '<td id="name'+i+'">'+product+'<input type="hidden" name="productID[]" class="required number form-control input-sm product" id="productID'+i+'" value="' + material + '"/><input type="hidden" name="subkontrakMaterialID[]" class="required number form-control input-sm product" id="subkontrakMaterialID'+i+'" value="' + subkontrak_material_id + '"/></td>'+
                        '<td><input type="text" readonly name="qtyRAP'+i+'" class="required number form-control input-sm qty" id="qtyRAP'+i+'" value="'+qtyrap+'" /></td>'+
                        '<td><input type="text" readonly name="qty'+i+'" class="required number form-control input-sm qty" id="qty'+i+'" value="'+qty+'" /></td>'+
                        '<td><div class="input-group"><input type="text" '+readonly+' name="qtyTotal[]" class="required form-control input-sm calculate qty" id="qtyTotal'+i+'" placeholder="0" data-max="'+max+'" /><div class="input-group-addon">'+satuan+'</div></div></td>'+
                        '<td style="width:45px;"><button type="button" style="width:40px;" class="btn btn-danger btn-sm delete" value="'+i+'" data-product-id="'+material+'" name="delete" id="delete" tabIndex="-1"><i class="fa fa-trash"></i></button></td>'+
                    '</tr>');
                    i++;
                    numberItem++;
                }

                $('.qty').autoNumeric('init', autoNumericOptionsRupiah);
            }else{
                if(selProduct!=0){
                    alert("Produk tidak tersedia.");
                }
            }
        }
    });
}

function loadSubstitution(selKontrak, product, number) {
    return $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/bahan_subtitusi_material/list?h="+userHash+"&filter_subkontrak_id=')?>" +selKontrak+'&filter_material='+product,
        cache : false,
        success : function(result){
            if(result['success']) {
                $('#selSubstitution').html('<option value="0" selected>Select your option</option>');
                $.each (result['data'], function (index,arr) {
                    if(arr['substitusi']!=0){
                        $('#selSubstitution').append('<option value="' + arr['substitusi'] + '">' + arr['prod_title'] + '</option>');
                        $("#selSubstitution").prop('disabled', false);
                    }else{
                        $('#selSubstitution').html('<option disabled value="0" selected>No Data</option>');
                        $("#selSubstitution").prop('disabled', true);
                    }
                });
                
                $("#numberSubstitution").val(number);
            } else {
                $('#selSubstitution').html('<option disabled value="0" selected>No Data</option>');
                $("#selSubstitution").prop('disabled', true);
                $("#numberSubstitution").val(0);
            }
        }
    });
}

$('.chosen').chosen({search_contains: true, width: '100%'});

});</script>