<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({dateFormat: "yy/mm/dd"});
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".jwTime").datetimepicker({ dateFormat: "yy/mm/dd", timeOnly: true });
});</script>