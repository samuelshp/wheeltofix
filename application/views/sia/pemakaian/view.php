<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-pie-chart"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaian'), 'link' => site_url('pemakaian/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<div class="col-xs-12">
    <form name="frmAddPemakaian" id="frmAddPemakaian" method="post" action="<?=site_url('pemakaian', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-projectdata')?> </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Nama Proyek</label>
                        <p><?=$arrParent['proyek']?></p>
                        <input type="hidden" id="selProyek" name="idkontrak" value="<?=$arrParent['kontrak_id']?>" />
                    </div>
                    <div class="form-group">
                        <label>Nama Gudang</label>
                        <p><?=$arrParent['gudang']?></p>
                        <input type="hidden" id="selWarehouse" name="selwarehouse" value="<?=$arrParent['warehouse_id']?>" />
                    </div>
                    <div class="form-group">
                        <label>Nama Kontrak</label>
                        <p><?=$arrParent['kontrak']?></p>
                        <input type="hidden" id="selKontrak" name="idsubkon" value="<?=$arrParent['subkontrak_id']?>" />
                    </div>
                </div>
            </div>
        </div>

        <!-- nama proyek -->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-productdata')?> </h3>
                </div>
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproduct')?></h4>
                    <div class="form-group">
                        <select type="text" id="selProduct" class="form-control chosen editable" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproject')?>">
                            <option value="0" selected>Select your option</option>
                        </select>
                    </div>
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectsubstitution')?></h4>
                    <div class="form-group">
                        <select type="text" id="selSubstitution" class="form-control chosen editable" disabled placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproject')?>">
                            <option value="0" selected>Select your option</option>
                        </select>
                        <input type="hidden" name="numberSubstitution" id="numberSubstitution" value="0" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaianitems')?></h3></div>
        <div class="panel-body">
            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-product')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-qtyrap')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-qty')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-total')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-jumlah')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-delete')?></th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        if(count($arrItem)>0){
                            foreach($arrItem as $i => $e){
                                $subkontrak_material_id = $e['subkontrak_material_id'];
                                $product = $e['prod_title'];
                                $material = $e['material'];
                                $qty_total = $e['qty_total'];//parseFloat(result.data[0]['jumlah_awal']);
                                $qtyrap = $e['jumlah_awal'];//parseFloat(result.data[0]['jumlah_awal']);
                                $qty = $e['gudang_qty'];//parseFloat(result.data[0]['gudang_qty']);
                                $satuan = $e['unit_title'];
                                $readonly = "";
                                $max = 0;
                                if($qtyrap>$qty){
                                    $max = $qty;
                                }else{
                                    $max = $qtyrap;
                                }
                                if($qtyrap<=0 || $qty<=0){
                                    $readonly = "readonly";
                                }
                    ?>
                    <tr>
                        <td id="name<?=$i?>"><?=$product?>
                            <input type="hidden" name="productID[]" class="required number form-control input-sm product" id="productID<?=$i?>" value="<?=$material?>"/>
                            <input type="hidden" name="subkontrakMaterialID[]" class="required number form-control input-sm product" id="subkontrakMaterialID<?=$i?>" value="<?=$subkontrak_material_id?>"/>
                        </td>
                        <td>
                            <input type="text" readonly name="qtyRAP<?=$i?>" class="required number form-control input-sm qty" id="qtyRAP<?=$i?>" value="<?=$qtyrap?>" />
                        </td>
                        <td>
                            <input type="text" readonly name="qty<?=$i?>" class="required number form-control input-sm qty" id="qty<?=$i?>" value="<?=$qty?>" />
                        </td>
                        <td class="qty">
                            0
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="text" <?=$readonly?> name="qtyTotal[]" class="required form-control input-sm qty editable" disabled id="qtyTotal<?=$i?>" value="<?=$qty_total?>" placeholder="0" data-max="<?=$max?>" />
                                <div class="input-group-addon"><?=$satuan?></div>
                            </div>
                        </td>
                        <td style="width:45px;">
                            <button type="button" style="width:40px;" class="btn btn-danger btn-sm delete editable" disabled value="<?=$i?>" data-product-id="<?=$material?>" name="delete" id="delete" tabIndex="-1"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    <?php $i++; }}else{ ?>
                    <tr class="info"><td class="noData" colspan="9"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    <?php } ?>
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>

    <div class="form-group">
        <?php if($bolAllowUpdate && $bolAllowUpdate): ?>
        <button type="button" id="smtEditUsage" name="smtEditUsage" value="Edit Usage" class="btn btn-primary">
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
        </button>
        <div id="editMode" style="display: none">
            <button type="submit" id="smtEditUsage2" name="smtEditUsage" value="Update Usage" class="btn btn-warning">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
            </button>
        <?php endif; if($bolAllowDelete && $bolBtnDelete): ?>         
            <button type="submit" id="smtDeleteUsage" name="smtDeleteUsage" value="Delete Usage" class="btn btn-danger pull-right">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?>
            </button>
        <?php endif; ?>
        </div>
    </div>       
</div>

    <input type="hidden" id="id_pemakaian" name="id_pemakaian" value="<?php echo $arrParent['id']; ?>"/>
    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
</form></div>