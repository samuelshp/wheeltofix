<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-pie-chart"></i> '.$strPageTitle, 'link' => '')
);

//echo $_SESSION['strAdminID'];

//echo $_SESSION['strAdminPriviledge'];

$date = date('Y/m/d H:i:s');

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<!-- Baru dibawah -->
<div class="col-xs-12"><?php
$strSearchAction = site_url('pemakaian/browse', NULL, FALSE);

include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchusage.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('pemakaian', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Proyek</th>
        <th>Kontrak</th>
        <th class="action">Action</th>
    </tr></thead>
    <tbody><?php

// Display data in the table
if(!empty($arrUsage)):
    $i=0;
    foreach($arrUsage as $e): 
        $i++;?>
        <tr>
            <td><?=formatDate2($e['cdate'],'d F Y')?></td>
            <td><?=$e['kont_name']//diisi field nomor kontrak?></td>
            <td><?=$e['kont_job']?></td>
            <td class="action">
                <a href="<?=site_url('pemakaian/view/'.$e['id'], NULL, FALSE)?>" ><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>
            </td>
        </tr><?php
    endforeach;
else: ?>
    <tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>