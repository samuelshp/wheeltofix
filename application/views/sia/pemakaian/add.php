<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-pie-chart"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaian'), 'link' => site_url('pemakaian/browse', NULL, FALSE))
);

$arrBreadcrumb[2] = array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '');

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<!-- Lama dibawah -->

<!-- Baru dibawah -->
<div class="col-xs-12">
    <form name="frmAddPemakaian" id="frmAddPemakaian" method="post" action="<?=site_url('pemakaian', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-projectdata')?> </h3>
                </div>
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?> <a href="<?=site_url('kontrak', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <div class="form-group">
                        <select type="text" id="selProyek" name="idkontrak" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php foreach($arrKontrak as $e) { ?>
                                <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <h4>Pilih Gudang <a href="<?=site_url('')?>" target="_blank"><i class="fa fa-plus" title="Tambah"></i></a></h4>
                    <div class="form-group">
                        <select id="selWarehouse" name="selwarehouse" class="form-control chosen" disabled>
                            <option value="0" selected>Select your option</option>        
                        </select>
                        <input type="hidden" id="idwarehouse" name="idwarehouse" value="0" />
                    </div>
                    <div id="subkontrak">
                        <h4>Pilih Kontrak <a href="#" target="_blank"><i class="fa fa-plus" title="Tambah"></i></a></h4>
                        <div class="form-group">
                            <select id="selKontrak" name="idsubkon" disabled class="form-control chosen">
                                <option value="0" disabled selected>Select your option</option>
                            </select>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- nama proyek -->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-productdata')?> </h3>
                </div>
                <div class="panel-body">
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproduct')?></h4>
                    <div class="form-group">
                        <select type="text" id="selProduct" disabled class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproject')?>">
                            <option value="0" selected>Select your option</option>
                        </select>
                    </div>
                    <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectsubstitution')?></h4>
                    <div class="form-group">
                        <select type="text" id="selSubstitution" disabled class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-selectproject')?>">
                            <option value="0" selected>Select your option</option>
                        </select>
                        <input type="hidden" name="numberSubstitution" id="numberSubstitution" value="0" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaianitems')?></h3></div>
        <div class="panel-body">
            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-product')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-qtyrap')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-qty')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-total')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-jumlah')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-delete')?></th>
                </tr>
                </thead>
                <tbody>
                    <tr class="info"><td class="noData" colspan="9"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>

    <div class="form-group">
        <button type="submit" name="smtMakeUsage" value="Make Usage" class="btn btn-primary">
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
        </button>
    </div>        
</div>

</form></div>