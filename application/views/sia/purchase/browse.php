<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php
$strSearchAction = site_url('purchase/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchop.php'); ?>

<div class="col-xs-12"><form name="frmAddAcceptance" id="frmAddAcceptance" method="post" action="<?=site_url('purchase/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('purchase', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder')?></th>
			<th>Nama Proyek</th>
			<th>Nama Pekerjaan</th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
			<!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th> -->
			<th class="action" style="min-width:120px;">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrPurchase)):
			foreach($arrPurchase as $e): ?>
			<tr>
				<td><?=formatDate2($e['prch_date'],'d F Y')?></td>
				<td><?=$e['prch_code']?></td>
				<td><?=$e['pror_code']?></td>
				<td><?=$e['kont_name']?></td>
				<td><?=$e['job']?></td>
				<td><?=$e['supp_name']?></td>
				<td><?=setPrice($e['prch_total'])?></td>
				<!-- <td><?=$e['prch_status']?></td> -->
				<td style="min-width:120px;" class="action">
					<?php if($e['bolAllowView']): ?><a style="margin:0" href="<?=site_url('purchase/view/'.$e['id'], NULL, FALSE)?>"><button type="button" class="btn btn-sm btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></button></a><?php endif; ?>
					<?php if($e['bolAllowUpdate']): ?><a style="margin:0" href="<?=site_url('purchase/edit/'.$e['id'], NULL, FALSE)?>"><button type="button" class="btn btn-sm btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?></button></a><?php endif; ?>
					<?php if($e['prch_status'] == 3) {?>
					<!-- <button onclick="return confirm('Are you sure to delete?');" type="submit" class="btn btn-danger" value="<?= $e['id'] ?>" name="delPurchase" id="delPurchase"><i class="fa fa-trash">
					</i></button>-->
					<?php } ?> 

				</td>
			</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
	</form>
</div><!--
--><?/*=site_url('purchase/view/'.$e['id'].'?print=true')*/?>