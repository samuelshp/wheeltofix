<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase'), 'link' => site_url('purchase/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Edit', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmEditPurchase" id="frmEditPurchase" method="post" action="<?=site_url('purchase', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbdata')?> </h3>
                </div>
                <div class="panel-body">
                <div class="col-xs-4">No. PO : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['prch_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">No. PB : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['pror_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Supplier : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['supp_name']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Contact Person : </div>
                <div class="col-xs-8"><?=(!empty($arrPurchaseData['supp_cp_name']))? $arrPurchaseData['supp_cp_name'] : "-"?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">No Kontrak Pembelian : </div>
                <div class="col-xs-8"><?=$arrPurchaseData['kopb_code']?></div>
                <p class="spacer">&nbsp;</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <!-- Dibuat -->
                    <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="date" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div> -->
                    <input type="text" id="txtDate" name="txtDate" value="<?=date('Y/m/d',strtotime($arrPurchaseData['prch_date']))?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-th"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <textarea id="txtDescription" name="txtDescription" class="form-control" rows="5"></textarea>
                </div>
            </div>
        </div>

    </div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
        <div class="panel-body">

            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-priceunitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($arrPurchaseItem as $key => $e) { ?>
                    <tr>
                        <td id="name<?=$key?>"><?=$e['prod_title']?></td>
                        <td class="qty"><input type="text" name="qtyItem<?=$key?>" class="required number form-control input-sm" id="qtyItem<?=$key?>" value="<?=$e['prci_quantity_pb']?>" /><input type="hidden" name="poiID<?=$key?>" value="<?=$e['prci_proi_id']?>"/><input type="hidden" name="piID<?=$key?>" value="<?=$e['id']?>"/><input type="hidden" name="productID<?=$key?>" class="required number form-control input-sm" id="productID<?=$key?>" value="<?=$e['prci_product_id']?>"/></td>
                        <td><input type="text" name="satuanItem<?=$key?>" class="form-control input-sm" id="satuanItem<?=$key?>" value="<?=$e['sat_pb']?>" readonly tabIndex="-1" /></td>
                        <td class="qty"><input type="number" name="qtyBayar<?=$key?>" class="required number form-control input-sm calculate" id="qtyBayar<?=$key?>" value="<?=$e['prci_quantity1']?>" step="0.0001"/></td>
                        <td><input type="text" name="satuanItem<?=$key?>" class="form-control input-sm" id="satuanItem<?=$key?>" value="<?=$e['sat_bayar']?>" readonly tabIndex="-1" /></td>
                        <td class="qty"><input type="text" name="hargaBayar<?=$key?>" class="required number form-control input-sm calculate currency" id="hargaBayar<?=$key?>" value="<?=$e['prci_price']?>" step="0.01" /></td>
                        <td><input type="text" readonly name="total<?=$key?>" class="required number form-control input-sm currency" id="total<?=$key?>" value="0" tabIndex="-1" /></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>
	
	<div class="row">

        <!-- Biaya -->
        <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-costadd')?></h3></div>
            <div class="panel-body">
                <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                </div></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                        <thead>
                        <tr>
                            <!--<th class="cb"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')*/?></th>-->
                            <th class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-cost')?></th>
                            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--/ Table Biaya -->
        </div></div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-downpayment')?></h3></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover" id="downpayment">
                            <thead>
                            <tr>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-date')?></th>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalNoTax" class="form-control required currency" id="subTotalNoTax" value="0" readonly /></div>
                        </div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                        <div class="col-sm-6"><div class="input-group"><label class="input-group-addon">Rp.</label><input onClick="this.select();" type="text" name="dsc4" class="form-control required currency calculate" id="dsc4Item" value="<?=$arrPurchaseData['prch_discount']?>" /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalWithDisc" tabindex="-1" class="form-control required currency" id="subTotalWithDisc" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtTax" id="txtTax" class="form-control required calculate" maxlength="3" value="<?=$arrPurchaseData['prch_tax']?>" /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalTax" tabindex="-1" class="form-control required currency" id="subTotalTax" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-costadd')?></div>
                        <div class="col-sm-6 tdDesc form-group"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalCost" tabindex="-1" class="form-control required currency" id="subTotalCost" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
                        <div class="col-sm-6 tdDesc form-group"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="grandTotal" tabindex="-1" class="form-control required currency" id="grandTotal" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pph')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtPPH" id="txtPPH" class="form-control required calculate" maxlength="3" value="<?=$arrPurchaseData['prch_pph']?>" /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountpph')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="amountPPH" tabindex="-1" class="form-control required currency" id="amountPPH" value="0" readonly /></div></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-group"><button type="submit" name="smtUpdatePurchase" value="Update Purchase" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?></button></div>

<input type="hidden" id="purchaseID" name="intPurchaseID" value="<?=$this->uri->segment(3)?>" />
<input type="hidden" id="totalItem" name="totalItem" value="<?=count($arrPurchaseItem)?>" />
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
<input type="hidden" name="po_id" id="po_id" value="<?=$arrPurchaseData['purchase_order_id']?>"/>

</form></div>




