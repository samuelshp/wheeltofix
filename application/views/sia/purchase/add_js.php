<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {

var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var mode=0;
var totalItem=0;
var numberItem=0;
var totalItemBonus=0;
var numberItemBonus=0;
var selectedItemBefore=[];
var selectedItemBonusBefore=[];
$('.currency').autoNumeric('init', autoNumericOptionsRupiah);
$("#selPB").prop('disabled', true);
$("#selKP").prop('disabled', true);
$("#selSupp").prop('disabled', true);

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

function calculate() {
    var x = 0;
    var y = 0;
    var subtotal = 0;
    var grandTotal = 0;
    while(x < numberItem) {
        if($("#poiID" + y).length > 0) {
            var qty = $("#qtyBayar"+y).val();
            var price = $("#hargaBayar"+y).autoNumeric('get');
            var total = qty*price;
            $("#total"+y).autoNumeric('set',total);
            subtotal += total;
            x++;
        }
        y++;
    }
    // alert(subtotal);
    $("#subTotalNoTax").autoNumeric('set',subtotal);

    var totalwithdisc=subtotal-$("#dsc4Item").autoNumeric('get');
    $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);

    var tax = $("#txtTax").val();
    var pph = $("#txtPPH").val();

    var totalTax=totalwithdisc+((totalwithdisc*tax)/100);
    $("#subTotalTax").autoNumeric('set',totalTax);

    var addCost = $("#subTotalCost").autoNumeric('get');
    grandTotal = (totalTax*1) + (addCost*1);

    $("#grandTotal").autoNumeric('set',grandTotal);

    var totalPPH=(totalwithdisc*pph)/100;
    
    $("#amountPPH").autoNumeric('set',totalPPH);
}
$(".calculate").on('keyup', function(){
    calculate();
});

// $("#frmAddPurchase").validate({
// 	rules:{},    
// 	messages:{},
//     showErrors: function(errorMap, errorList) {
//         $.each(this.validElements(), function (index, element) {
//             var $element = $(element);
//             $element.data("title", "").removeClass("error").tooltip("destroy");
//             $element.closest('.form-group').removeClass('has-error');
//         });
//         $.each(errorList, function (index, error) {
//             var $element = $(error.element);
//             $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
//             $element.closest('.form-group').addClass('has-error');
//         });
//     },
// 	errorClass: "input-group-addon error",
// 	errorElement: "label",
// 	highlight:function(element, errorClass, validClass) {
// 		$(element).parents('.control-group').addClass('error');
// 	},
// 	unhighlight: function(element, errorClass, validClass) {
// 		$(element).parents('.control-group').removeClass('error');
// 		$(element).parents('.control-group').addClass('success');
// 	}
// });

$("#frmAddPurchase").submit(function() {
    if($('#selPB').val() == null) {
        alert("PB tidak boleh kosong");
        return false;
    }
    if($('#selSupp').val() == null) {
        alert("Supplier tidak boleh kosong");
        return false;
    }
	var form = $(this);
    $('.submited').prop('disabled', false);
	$("#txtSJCode").prop('disabled', false);
	$("#txtEkspedition").prop('disabled', false);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
	
	return true;
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemBonusX"+at).val()<0){
            $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
        }
    }
    if($("input[type='radio'][name='radioPO']:checked").val()=='1'){
        var max1=$("#max1ItemBonusX"+at).val();
        var max2=$("#max2ItemBonusX"+at).val();
        var max3=$("#max3ItemBonusX"+at).val();
        var qty1=$("#qty1ItemBonusX"+at).val();
        var qty2=$("#qty2ItemBonusX"+at).val();
        var qty3=$("#qty3ItemBonusX"+at).val();
        var conv1=$("#conv1UnitBonusX"+at).val();
        var conv2=$("#conv2UnitBonusX"+at).val();
        var conv3=$("#conv3UnitBonusX"+at).val();
        var max=parseInt(max1)*parseInt(conv1)+parseInt(max2)*parseInt(conv2)+parseInt(max3)*parseInt(conv3);
        var qty=parseInt(qty1)*parseInt(conv1)+parseInt(qty2)*parseInt(conv2)+parseInt(qty3)*parseInt(conv3);
        if(qty>max ){
            $("#qty1ItemBonusX"+at).val(max1);
            $("#qty2ItemBonusX"+at).val(max2);
            $("#qty3ItemBonusX"+at).val(max3);
        }
    }
});

$('#selOwner').change(function(){	
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/purchase_order_product/list?h="+userHash+"&filter_idKontrak=')?>" + $('#selOwner').val(),
        cache : false,
        success : function(result){
        	$("#selSupp").prop('disabled', true);
        	$("#selSupp").trigger("chosen:updated");
        	$("#selPB").prop('disabled', false);
            $('#selPB').html('<option disabled value="0" selected>Select your option</option>');
            if(result.success){
                $.each (result.data, function (index,arr) {
                    $('#selPB').append('<option value="' + arr['po_id'] + '">'+ arr['createAt'] +' [' + arr['pror_code'] + '] '+ arr['prod_title'] +'</option>');
                });
                $("#selPB").trigger("chosen:updated");
            }
        }
    });
});

$('#selPB').change(function(){
    console.log($('#selPB').val() + "milik BPB");
    $("#contactPersonInfo").hide();
    $("#po_id").val($('#selPB').val());
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/purchase_order_item_purchase/list?h="+userHash+"&filter_proi_purchase_order_id=')?>" + $('#selPB').val(),
        cache : false,
        success : function(result){
            $("#selectedItems tbody").html('');
            $("#selSupp").prop('disabled', false);
            $("#selSupp").val(0);
            $("#selSupp").trigger("chosen:updated");
            $('#selKP').html('<option disabled value="0" selected>Select your option</option>');
        	$("#selKP").trigger("chosen:updated");
            $("#selKP").prop('disabled', true);
            var i=0;
            $.each (result.data, function (index,arr) {
                $("#selectedItems tbody").append(
                '<tr>'+
                    '<td id="name'+i+'">'+arr['prod_title']+'</td>'+
                    '<td class="qty"><input type="text" name="qtyItem'+i+'" class="required number form-control input-sm" id="qtyItem'+i+'" value="' + (arr['proi_quantity1'] - arr['proi_terpurchase']) + '" /><input type="hidden" name="productID'+i+'" class="required number form-control input-sm" id="productID'+i+'" value="' + arr['proi_product_id'] + '"/><input type="hidden" name="poiID'+i+'" class="required number form-control input-sm" id="poiID'+i+'" value="' + arr['poi_id'] + '"/></td>'+
                    '<td><input type="text" name="satuanItem'+i+'" class="form-control input-sm" id="satuanItem'+i+'" value="' + arr['sat_pb'] + '" readonly tabIndex="-1" /></td>'+
                    '<td><input type="text" name="description'+i+'" class="form-control input-sm" id="description'+i+'" value="'+arr['proi_description']+'" readonly title="'+arr['proi_description']+'" tabIndex="-1" /></td>'+
                    '<td class="qty"><input type="text" name="qtyItemTerima'+i+'" class="required number form-control input-sm" id="qtyItemTerima'+i+'" value="" placeholder="0" /></td>'+
                    '<td><input type="text" name="satuanItem'+i+'" class="form-control input-sm" id="satuanItem'+i+'" value="' + arr['sat_terima'] + '" readonly tabIndex="-1" /></td>'+
                    '<td class="qty"><input type="number" name="qtyBayar'+i+'" class="required number form-control input-sm calculate" id="qtyBayar'+i+'" placeholder="0" step="0.0001"/></td>'+
                    '<td><input type="text" name="satuanItem'+i+'" class="form-control input-sm" id="satuanItem'+i+'" value="' + arr['sat_bayar'] + '" readonly tabIndex="-1" /></td>'+
                    '<td class="qty"><input type="text" name="hargaBayar'+i+'" class="required number form-control input-sm calculate currency" id="hargaBayar'+i+'" placeholder="0" step="0.01" /></td>'+
                    '<td><input type="text" readonly name="total'+i+'" class="required number form-control input-sm currency" id="total'+i+'" value="0" tabIndex="-1" /></td>'+
                    '<td class="delete" style="width:45px;"><button type="button" style="width:40px;" class="btn btn-danger btn-sm delete_from_po" value="'+i+'" data-product-id="'+arr['proi_product_id']+'" name="delete_from_po" id="delete_from_po" tabIndex="-1"><i class="fa fa-trash"></i></button></td>'+
                    '<td><input type="hidden" name="idsubkon" id="idsubkon" value="'+arr['idSubKontrak']+'"></td>'+
                '</tr>');
                i++;
            });
            $('.currency').autoNumeric('init', autoNumericOptionsRupiah);
            $(".calculate").on('keyup', function() {
                calculate();
            });
	        numberItem = i;
            $("#totalItem").val(i);
            calculate();
        }
    });
});

$('#selKP').change(function(){
    $("#kpId").val($('#selKP').val());
});

$('#selSupp').change(function(){
    var intSuppID = $(this).val();
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "<?=site_url('api/jw_supplier/"+intSuppID+"?h="+userHash+"')?>",
        // cache: false,
        success: function(result){
            if(result.success){
            var result = result.data;            
                $("#contactPersonInfo").html('<br/><i class="fa fa-phone"></i> Contact Person : <span id="cpdetail">'+result.supp_cp_name+'</span>');
                $("#contactPersonInfo").show();
            }
            
        }
    });

    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/kontrak_pembelian_supplier/list?h="+userHash+"&filter_kopb_supplier_id=')?>" + $('#selSupp').val(),
        cache : false,
        success : function(result){
        	$("#selKP").prop('disabled', false);
            $('#selKP').html('<option disabled value="0" selected>Select your option</option>');
            if (result.success) {
                $.each (result.data, function (index,arr) {                    
                    $('#selKP').append('<option data-product-id="'+arr['kopb_product_id']+'" value="' + arr['kp_id'] + '">[' + arr['kopb_code'] + '] ' + arr['prod_title'] + '</option>');
                });
                $("#selKP").trigger("chosen:updated");                
            }
        }
    });

    loadDownpayment($('#selSupp').val());
});

$("#selectedItems").on('click', '.delete_from_po', function () {
    if(numberItem>1){
        var at = $(this).val();
        $('#name'+at).closest("tr").remove();
        numberItem--;
        $("#totalItem").val(numberItem);
        calculate();
	} else {
		alert('Item tidak boleh kosong, silahkan pilih PB kembali');
	}
})

var $arrSelectedCost;
var selectedCost;
$("#txtNewItemBonus").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getAddCostAutoComplete', NULL, FALSE)?>/"+$("#txtNewItemBonus").val(),
			beforeSend: function() {
				$("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemBonus").html('');
			},
			success: function(data){
				if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                console.log($("#txtNewItemBonus").val());
                console.log(JSON.stringify(xmlDoc));
				var xml = $(xmlDoc);
				$arrSelectedCost = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('prod_title').text();
					display.push({label:strName, value:strName,id:intID});
                });
                response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedCost = $.grep($arrSelectedCost.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItemBonus;
		if(numberItemBonus=='0'){
			$("#selectedItemsBonus tbody").html('');
		}
        var name = 'asd';
		$("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
                '<td id="name'+i+'">'+name+'</td>'+
                '<td class="qty"><input type="hidden" name="accoID" value="'+name+'" /><input type="number" name="amountX'+i+'" class="required number form-control input-sm" id="amountX'+i+'" placeholder="0"/></td>'+
            '</tr>');

		totalItemBonus++;
		numberItemBonus++;
		$("#totalItemBonus").val(totalItemBonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
	}
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItemBonus--;
	if(numberItemBonus=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonusX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("change","input[type='number']",function(){
    //change additional cost here
});

function loadDownpayment(intSupplierID) {    
    return $.ajax({
        url: "<?=site_url('api/downpayment/list?h="+userHash+"&filter_dpay_supplier_id=')?>"+intSupplierID,
        dataType: "json",
        method: "GET",
        success: function(result) {
            if (result.success) {
                $("#downpayment tbody").html('');
                $.each(result.data, function (index,arr) {
                    $('#downpayment tbody').append('<tr><td>' + arr['dpay_date'] + '</td><td class="currency-text">' + arr['dpay_amount'] + '</td><td>'+arr['dpay_description']+'</td></tr>');
                });
            $(".currency-text").autoNumeric('init',{autoNumericOptionsRupiah, aSign: 'IDR '});
            }
        }        
    });    
}

$('.chosen').chosen({search_contains: true});

});</script>