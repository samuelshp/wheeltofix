<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase'), 'link' => site_url('purchase/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmAddPurchase" id="frmAddPurchase" method="post" action="<?=site_url('purchase', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbdata')?> </h3>
                </div>
                <div class="panel-body">
                    <h4 class="PB"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?> <a href="<?=site_url('kontrak', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" id="selOwner" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrKontrak as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="panel-body">
                    <h4 class="PB"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectpb')?> <a href="<?=site_url('purchase_order', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" name="selPB" id="selPB" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectpb')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
                <div class="panel-body">
                    <h4 class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" name="supplierID" id="selSupp" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrSupplier as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                        <?php } ?>
                    </select>                    
                    <div id="contactPersonInfo" style="display:none"></div>                
                </div>                
                <div class="panel-body">
                    <h4 class="KP"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectkp')?> <a href="<?=site_url('kontrak_pembelian', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                    <select type="text" name="kpID" id="selKP" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectkp')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <!-- Dibuat -->
                    <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="date" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div> -->
                    <input type="text" id="txtDate" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-th"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <textarea id="txtDescription" name="txtDescription" class="form-control" rows="5"></textarea>
                </div>
            </div>
        </div>

    </div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
        <div class="panel-body">

            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtybpb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitbpb')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-priceunitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-delete')?></th>
                </tr>
                </thead>
                <tbody>
                    <tr class="info"><td class="noData" colspan="11"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>
	
	<div class="row">

        <!-- Biaya -->
        <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-costadd')?></h3></div>
            <div class="panel-body">
                <div class="form-group"><div class="input-group addNew">
                    <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                </div></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                        <thead>
                        <tr>
                            <!--<th class="cb"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')*/?></th>-->
                            <th class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-cost')?></th>
                            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--/ Table Biaya -->
        </div></div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-downpayment')?></h3></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover" id="downpayment">
                            <thead>
                            <tr>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-date')?></th>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalNoTax" class="form-control required currency" id="subTotalNoTax" value="0" readonly /></div>
                        </div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                        <div class="col-sm-6"><div class="input-group"><label class="input-group-addon">Rp.</label><input onClick="this.select();" type="text" name="dsc4" class="form-control required currency calculate" id="dsc4Item" value="0" /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalWithDisc" tabindex="-1" class="form-control required currency" id="subTotalWithDisc" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtTax" id="txtTax" class="form-control required calculate" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalTax" tabindex="-1" class="form-control required currency" id="subTotalTax" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-costadd')?></div>
                        <div class="col-sm-6 tdDesc form-group"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalCost" tabindex="-1" class="form-control required currency" id="subTotalCost" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
                        <div class="col-sm-6 tdDesc form-group"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="grandTotal" tabindex="-1" class="form-control required currency" id="grandTotal" value="0" readonly /></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pph')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtPPH" id="txtPPH" class="form-control required calculate" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountpph')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="amountPPH" tabindex="-1" class="form-control required currency" id="amountPPH" value="0" readonly /></div></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-group"><button type="submit" name="smtMakePurchase" value="Make Purchase" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

<input type="hidden" id="totalItem" name="totalItem"/>
<input type="hidden" id="kpId" name="kpId"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
<input type="hidden" name="po_id" id="po_id" value="<?=$arrPurchaseData['purchase_order_id']?>"/>

</form></div>




