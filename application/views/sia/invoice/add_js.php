<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />

<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-sliderAccess.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
});</script>
<script type="text/javascript">$(document).ready(function() {

$('.currency').autoNumeric('init', autoNumericOptionsRupiah);
$('#price').autoNumeric('set',0);

$("#frmAddInvoice").submit(function() {
    if($('#selTermin').val() == null) {
        alert("Pilih pekerjaan yang belum lunas dibayar");
        return false;
    }
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
    $('.currency').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
    
    
    return true;
});


$("#selSubkontrak").prop('disabled', true);
$("#selTermin").prop('disabled', true);

$('#selKontrak').change(function(){
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('/api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + $('#selKontrak').val(),
        cache : false,
        success : function(result, textStatus, xhr){                        
            $('#selTermin').val('');
            $('#subkontrakTerminID').val(0);
            $('#price').autoNumeric('set',0);
            $("#selTermin").prop('disabled', true);
            $('#selTermin').html('<option disabled value="0" selected>Select your option</option>');            
            if (result.success === true) {                
                $("#selSubkontrak").prop('disabled', false);
                $('#selSubkontrak').html('<option disabled value="0" selected>Select your option</option>');
                $.each (result.data, function (index,arr) {                    
                    $('#selSubkontrak').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
                });
            }
            $("#selSubkontrak").trigger("chosen:updated");
            $("#selTermin").trigger("chosen:updated");
        }
    });

});
$('#selSubkontrak').change(function(){
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/last_termin/list?h="+userHash+"&filter_subkontrak_id=')?>" + $('#selSubkontrak').val(),
        cache : false,
        success : function(result){
            $("#selTermin").prop('disabled', false);
            $('#selTermin').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result.data, function (index,arr) {                
                $('#selTermin').append('<option value="' + arr['subkontrak_termin_id'] + '" data-price="'+ arr['amount'] +'">' + arr['jenis'] + ' ['+arr['amount']+']</option>');                
            });
            $("#selTermin").trigger("chosen:updated");
            $('#price').autoNumeric('set',0);
            
        },
        error: function(xhr, error){
            $("#selTermin").prop('disabled', true);
            $('#selTermin').html('<option disabled value="0" selected>Tidak ada invoice yang dapat dibuat / sudah lunas</option>');
        },
    });
});

$('#selTermin').change(function(){
    var price = $('#selTermin').find(':selected').data('price');
    $('#price').autoNumeric('set',price);
});

$('.chosen').chosen({search_contains: true});

});</script>