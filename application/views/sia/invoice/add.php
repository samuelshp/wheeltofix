<?php
$strMenuInstruction .= '<h4>Hotkeys</h4>
<ol>
    <li>[F6] Tambah Barang</li>
    <li>[F7] Tambah Barang Bonus</li>
    <li>[F8] Masukkan Pembayaran</li>
    <li>[F9] Buat Penjualan</li>
</ol>';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-share"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice'), 'link' => site_url('invoice/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <form name="frmAddInvoice" id="frmAddInvoice" class="frmShop" method="post" action="<?=site_url('invoice/index/', NULL, FALSE)?>">
                <div class="panel panel-primary" id="panelSO">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control" name="txtDate" type="date" value="<?=date('Y-m-d')?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intKontrakID" required id="selKontrak">
                    <option value="0" disabled selected>Select your option</option>
                    <?php foreach($arrKontrak as $e) {?>
                        <option value="<?=$e['id']?>"><?=$e['kont_name']?> [<?=$e['cust_name']?>]</option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-job')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intSubkontrakID" required id="selSubkontrak">
                    <option value="0" disabled selected>Select your option</option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-termin')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <select class="form-control chosen" name="intSubkontrakTerminID" required id="selTermin">
                    <option value="0" disabled selected>Select your option</option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <input class="form-control currency" name="txtPrice" type="text" readonly id="price" placeholder="0" />
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

        <div class="form-group">
            <button type="submit" name="smtMakeInvoice" value="Make Invoice" class="btn btn-primary" title="Buat Invoice" data-toggle="tooltip"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button>
        </div>
    </form>
</div>