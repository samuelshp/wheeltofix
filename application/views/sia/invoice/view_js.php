<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {

$("#selKontrak").prop('disabled', true);
$("#selSubkontrak").prop('disabled', true);
$("#selTermin").prop('disabled', true);
$('.chosen').chosen({search_contains: true});

$("#smtEditInvoice").click(function(){
	$("#smtUpdateInvoice").show();
	$("#subDeleteInvoice").show();
	$(this).hide();
	$("input[type='date']").removeAttr('readonly');
});

});</script>