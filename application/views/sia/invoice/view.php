<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice'), 'link' => site_url('invoice/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmAddInvoice" id="frmAddInvoice" method="post" action="<?=site_url('invoice/view/'.$arrInvoiceData['id'], NULL, FALSE)?>" class="frmShop">
<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Summary</h3></div>
    <div class="panel-body">
        <div class="form-group">            
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?>
            </div>
            <div class="col-sm-8 col-md-10">
               <?=$arrInvoiceData['invo_code'];?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">            
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?> *
            </div>
            <div class="col-sm-8 col-md-10">
               <input class="form-control " name="txtDate" type="date" readonly value="<?=formatDate2($arrInvoiceData['invo_jatuhtempo'],'Y-m-d')?>" />
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intKontrakID" required id="selKontrak">
                    <option value="0" disabled selected><?=$arrInvoiceData['kont_name']?></option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-job')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intSubkontrakID" required id="selSubkontrak">
                    <option value="0" disabled selected><?=$arrInvoiceData['job']?></option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-termin')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <select class="form-control chosen" name="intSubkontrakTerminID" required id="selTermin">
                    <option value="0" disabled selected><?=$arrInvoiceData['jenis']?></option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?> *
            </div>
            <div class="col-sm-8 col-md-10">
                <input class="form-control currency" name="txtPrice" type="text" readonly id="price" value="<?=setPrice($arrInvoiceData['amount'],'BASE',FALSE)?>" />
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="form-group action">    
    <?php if($arrInvoiceData['invo_status'] < STATUS_FINISHED): ?>
    <button type="button" name="smtEditInvoice" id="smtEditInvoice" class="btn btn-primary"><span class="fa fa-edit" aria-hidden="true"> </span> </button>                        
    <button type="submit" name="smtUpdateInvoice" id="smtUpdateInvoice" value="Update Invoice" class="btn btn-warning" style="display: none">
        <span class="fa fa-edit" aria-hidden="true"> </span> 
    </button>
    <button type="submit" name="subDelete" id="subDeleteInvoice" value="Delete Invoice" class="btn btn-danger pull-right" style="display: none">
        <span class="fa fa-trash" aria-hidden="true"> </span> 
    </button>                        
    <?php endif; ?>
    <a href="<?=site_url(array('invoice/browse'))?>" class="btn btn-default"><span class="fa fa-reply"></span></a>
    <!-- <button type="reset" name="resReset" value="Reset" class="btn btn-warning"><span class="fa fa-undo"></span></button>&nbsp; -->
</div>
</form></div>