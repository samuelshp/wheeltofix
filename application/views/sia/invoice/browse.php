<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-share"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice'), 'link' => '')
);

include_once(APPPATH.'/views/'.$strViewFolder.'/contentheader.php'); ?>

<div class="col-xs-12"><?php
$strSearchAction = site_url('invoice/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchinvoice.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('invoice', NULL, FALSE)?>" target="_blank" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-job')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-termin')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
			<th class="action">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrInvoice)):
			foreach($arrInvoice as $e): ?>
				<tr>
				<td><?=$e['invo_code']?></td>
				<td>[<?=$e['kont_name']?>] <?=$e['job']?></td>
				<td><?=$e['jenis']?></td>
				<td><?=setPrice($e['invo_grandtotal'])?></td>
				<td><?=formatDate2($e['invo_date'],'d F Y')?></td>
				<td class="action">
					<?php if(!empty($e['strDescription'])): ?><a href="javascript:void(0)" role="button" data-html="true" data-toggle="tooltip" data-placement="left" title="<?=$e['strDescription']?>"><i class="fa fa-comment"></i></a><?php endif; ?>  
					<?php if($e['bolAllowView']): ?><a href="<?=site_url('invoice/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
					<?php if($e['bolBtnPrint']): ?><a href="<?=site_url('invoice/view/'.$e['id'].'?print=true', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
                    <?php if($e['bolBtnPrint'] && !empty($e['cust_nppkp'])): ?><a id="printtaxY<?=$e['id']?>" href="<?=site_url('invoice/view/'.$e['id'].'?print2=true')?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
                </td>
				</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</div>