<?php
$strMenuInstruction .= '<h4>Hotkeys</h4>
<ol>
    <li>[F6] Tambah Barang</li>
    <li>[F7] Tambah Barang Bonus</li>
    <li>[F8] Masukkan Pembayaran</li>
    <li>[F9] Buat Penjualan</li>
</ol>';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
// $arrBreadcrumb = array(
//     0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
//     1 => array('title' => '<i class="fa fa-sticky-note"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice'), 'link' => site_url('invoice/browse', NULL, FALSE)),
//     2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
// );

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <div class="form-group">
        <div class="input-group addNew">
            <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang [F6]" title="Tambah Barang [F6]" data-toggle="tooltip" class="form-control input-lg" /><label class="input-group-addon" id="loadItem"><i class="fa fa-search" aria-hidden="true"></i>
</label>
        </div>
    </div>
</div>
<div class="col-xs-12">
    <form name="frmAddInvoice" id="frmAddInvoice" class="frmShop" method="post" action="<?=site_url('invoice/', NULL, FALSE)?>">
        <div class="panel panel-primary" id="panelItem">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceitems')?></h3></div>
            <div class="panel-body">
                <!--input barang-->                
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                    <thead>
                    <tr>
                        <th class="cb notSO"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    </tbody>
                </table></div> <!--table-responsive-->

               <div class="row">
                    <div class="col-sm-2 col-sm-offset-7 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                    <div class="col-sm-2 tdDesc">
                        <div class="form-group" id="subTotalNoTax" >0</div>
                    </div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-2 col-sm-offset-7 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                    <div class="col-sm-3"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
                   <!--  <div class="col-sm-2 tdTitle"><h3><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></h3></div>
                    <div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div> -->
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-2 col-sm-offset-7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                    <div class="col-sm-3"><div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0"<?=$strMode == 'cashier' || $bolCashierFormatInNormalInvoice == 'yes' ? ' readonly' : ''?> /><label class="input-group-addon">%</label></div></div>
                    <!-- <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                    <div class="col-sm-2 tdDesc"><label id="subTotalTax">0</label></div> -->
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">                    
                    <div class="col-sm-2 col-sm-offset-7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                    <div class="col-sm-3"><label id="subTotalTax">0</label></div>
                </div>
                <p class="spacer">&nbsp;</p>  
            </div> <!--panel-body-->
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-block" type="button" id="btnPembayaran" data-toggle="modal" data-target="#modalPembayaran">Pembayaran</button>
                    </div>
                </div>
            </div>               
        </div>       

<!--         <div class="form-group">
            <button type="submit" name="smtMakeInvoice" value="Make Invoice" class="btn btn-primary" title="Buat Penjualan [F9]" data-toggle="tooltip"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button><?php
if($strMode != 'cashier' && $bolCashierFormatInNormalInvoice == 'yes'): ?>  
            <button type="submit" name="smtMakeInvoice" value="Make Cashier Invoice" class="btn btn-success" title="Buat Penjualan Kasir" data-toggle="tooltip"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button><?php
endif; ?>  
        </div> -->

    <div class="modal fade" id="modalPembayaran" tabindex="-1" role="dialog" aria-labelledby="modalPembayaranLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="modalPembayaranLabel"><i class="fa fa-money"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></h2>
          </div>
          <div class="modal-body">
                <div class="form-group col-sm-12">
                    <label>Jumlah harus dibayar</label>
                    <div class="input-group">
                        <label class="input-group-addon">Rp.</label>
                        <input type="text" name="txtAmountTotalPayment" id="txtAmountTotalPayment" class="form-control currency input-lg required" value="0" readonly />
                    </div>    
                </div>
                
                <div class="form-group col-sm-12">
                    <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-paymentmethod')?></label>
                    <div class="radio-group">
                        <label><input type="radio" name="intTipeBayar" value="1"<?=$intDefaultPaymentType == 1 ? ' checked' : ''?>>Tunai</label>
                        <label><input type="radio" name="intTipeBayar" value="2"<?=$intDefaultPaymentType == 2 ? ' checked' : ''?>>Kredit</label>
                        <label><input type="radio" name="intTipeBayar" value="3"<?=$intDefaultPaymentType == 3 ? ' checked' : ''?>>Debit</label>
                        <label><input type="radio" name="intTipeBayar" value="4"<?=$intDefaultPaymentType == 4 ? ' checked' : ''?>>Transfer</label>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="input-group" id="inputNoCard" style="display: none">
                        <label class="input-group-addon"><i class="fa fa-credit-card-alt"></i></label>
                        <input type="text" name="cardNo" class="form-control" placeholder="No. Kartu" />
                    </div>
                </div>
                <?php
if($strMode == 'cashier' || $bolCashierFormatInNormalInvoice == 'yes'): ?>  
                <div class="form-group col-sm-6">
                    <label>Nominal</label>
                        <div class="input-group">
                            <label class="input-group-addon">Rp.</label>
                            <input type="text" name="txtPayment" id="txtPayment" class="form-control currency input-lg required" value="" placeholder="Jumlah Pembayaran" title="Input Pembayaran [F8]" data-toggle="tooltip" />
                        </div>
                </div>
                <div class="form-group col-sm-6">
                    <label>Kembalian</label>
                    <div class="input-group">
                        <label class="input-group-addon">Rp.</label>
                        <input type="text" name="txtPaymentChange" id="txtPaymentChange" class="form-control input-lg currency" value="0" readonly />
                    </div>                    
                </div>
                <p class="spacer">&nbsp;</p><?php
endif; ?>
          </div>
          <div class="modal-footer">            
            <button type="submit" class="btn btn-primary btn-block btn-lg"  name="smtMakeInvoice" value="Make Bill">Proses</button>
          </div>
        </div>
      </div>
    </div>

        <input type="hidden" id="intWarehouseID" name="intWarehouseID"/>
        <input type="hidden" id="intSOID" name="intSOID"/>
        <input type="hidden" id="totalItem" name="totalItem"/>        
        <input type="hidden" name="supplierID" id="supplierID"/>
        <input type="hidden" id="intCustomerID" name="intCustomerID"/>
        <input type="hidden" name="customerOutletType"/>
        <input type="hidden" id="salesmanID" name="salesmanID"/>
        <input type="hidden" id= "outletID" name="outletID"/>
        <input type="hidden" name="outletMarketType"/>
        <input type="hidden" name="subTotalNoTax">
        <input type="hidden" name="subTotalTax">
    </form>
</div>

<!-- Modal -->
