<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<style type="text/css">
<?php if($strMode == 'cashier' || $bolCashierFormatInNormalInvoice == 'yes'): ?>
    #subTotalTax{font-size: 24px;}
<?php endif; ?>
</style>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-sliderAccess.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {

var supplierID=0;
var customerID=0;
var totalitem=0;
var numberItem=0;

function reset() {
    $("#customerName").val('');
    $('#selProyek').html('<option disabled value="0" selected>Select your option</option>');
    $("#selProyek").trigger("chosen:updated");
    $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
    $("#selPekerjaan").trigger("chosen:updated");
    $('#selWarehouse').html('<option disabled value="0" selected>Select your option</option>');
    $("#selWarehouse").trigger("chosen:updated"); 
    $("input[type='text']:not(.jwDateTime)").val('');
    $("#selectedItems tbody tr.info").show();
    $("#selectedItems tbody tr[name^='itemrow']").remove();
    $('.currency,.qty').autoNumeric('set', 0);
    $("#subTotalNoTax, #subTotalTax, #subTotalWithDisc").autoNumeric('set', 0);
}

$("input[type='radio'][name='radioSO']").click(function(){    
    if ($(this).val() == 0) {
        $("#Supplier, #panelSalesman, #panelItem").show();
        $("#SO,#panelTermin").hide();
        $("#customerName").prop('disabled',false);        
        reset();
    }else if($(this).val() == 1){
        $("#Supplier,#panelTermin").hide();
        $("#SO, #panelSalesman, #panelItem").show();
        $("#customerName, #txtNewItem").prop('disabled',true);        
        reset();
    }else{
        $("#Supplier,#SO, #panelSalesman, #panelItem").hide(); 
        $("#panelTermin").show();       
        $("#customerName").prop('disabled',false);
        loadProject('project',0);
        reset();
    }
});

$("input[type='radio'][name='radioSupplier']").click(function() {
    if($(this).val() == 0) {
        $("#supplierName").prop('readonly',true);
    }else{
        $("#supplierName").prop('readonly',false);
    }
});

$('.currency,.qty').autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalNoTax, #subTotalTax, #subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$('#price').autoNumeric('set',0);
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddInvoice").submit(function() {
    if($("input[type='radio'][name='radioSO']").val() == 2 && $('#selTermin').val() == null) {
        alert("Pilih pekerjaan yang belum lunas dibayar");
        return false;
    }
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
    $('.currency,.number,.qty').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
    
    
    return true;
});

$("#supplierName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('api/supplier/list?h="+userHash+"&filter_supp_status=2&filter_supp_name=*')?>"+$("#supplierName").val()+"*",
            beforeSend: function() {                
                $("#loadSupplier").html('Loading data');
            },
            complete: function() {                
                $("#loadSupplier").html('');
            },
            success: function(result){
                if(!result.success) $("#loadSupplier").html('No data');
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['id'];
                    var name = arr['supp_name'];
                    display.push({label: name, value: name, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO, function(el) {            
            return el.id == ui.item.id
        });                                
        var itemSelected = $(selectedPO)[0];
        supplierID = itemSelected.id;
        $("#supplierID").val(itemSelected.id);        
        $("#supplierName").val(itemSelected.supp_name); 
        $("#loadSupplier").html('<i class="fa fa-check"></i>');        
        return false;   
    }
});

$("#customerName").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('api/customer/list?h="+userHash+"&filter_cust_status=2&filter_cust_name=*')?>"+$("#customerName").val()+"*",
            beforeSend: function() {                
                $("#loadItem").html('Loading data');
            },
            complete: function() {                
                $("#loadItem").html('');
            },
            success: function(result){
                if(!result.success) $("#loadItem").html('No data');
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['id'];
                    var name = arr['cust_name'];
                    display.push({label: name, value: name, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO, function(el) {            
            return el.id == ui.item.id
        });                                
        var itemSelected = $(selectedPO)[0];        
        $("#intCustomerID").val(itemSelected.id);
        $("#customerAddress").val(itemSelected.cust_address);
        $("#customerCity").val(itemSelected.cust_city);
        $("#customerPhone").val(itemSelected.cust_phone);
        $("#customerOutletTypeStr").val(itemSelected.cust_limitkredit);
        $("#customerLimit").val(itemSelected.cust_limitkredit);
        $("#customerName").val(itemSelected.cust_name); 
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
        loadProject('customer',itemSelected.id).done(function() {
            loadWarehouse('kontrak', itemSelected);
            loadSubkontrak('kontrak',itemSelected);
        });
        return false;   
    }
});

$("#selSO").autocomplete({
    minLength: 3,
    delay: 500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('api/solist/list?h="+userHash+"&filter_inor_status=2&filter_inor_code=*')?>"+$("#selSO").val()+"*",
            beforeSend: function(){
                $("#loadSO").html('Loading data');
            },
            complete: function() {                
                $("#loadSO").html('');
            },
            success: function(result){
                if(!result.success) $("#loadSO").html('No data');                
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['so_id'];
                    var name = arr['inor_code'];
                    display.push({label: name, value: name, id:intID});
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {                
        var selectedPO = $.grep($arrSelectedPO, function(el) {                        
            return el.so_id == ui.item.id
        });        
        var itemSelected = $(selectedPO)[0];        
        $("#intSOID").val(itemSelected.so_id);
        $("#creatorName").val(itemSelected.adlg_name);
        $("#creatorPriviledge").val(itemSelected.priviledge);        
        $("#loadSO").html('<i class="fa fa-check"></i>');
        loadCustomer(itemSelected.inor_customer_id).done(function(customer){
            $("#intCustomerID").val(customer.data.id);
            $("#customerAddress").val(customer.data.cust_address);
            $("#customerCity").val(customer.data.cust_city);
            $("#customerPhone").val(customer.data.cust_phone);
            $("#customerOutletTypeStr").val(customer.data.cust_limitkredit);
            $("#customerLimit").val(customer.data.cust_limitkredit);
            $("#customerName").val(customer.data.cust_name);
        });
        loadProject('project',itemSelected.inor_kont_id);
        loadSubkontrak('subkontrak',itemSelected.inor_subkont_id);
        loadWarehouse('kontrak', itemSelected.inor_warehouse_id);
        loadItemBySO(itemSelected.so_id);
        // loadProject(itemSelected.id).done(function() {
        //     loadWarehouse('kontrak', itemSelected);
        //     loadSubkontrak(itemSelected);
        // });
    }
});

$("#selProyek").change(function(){
    var kontrakID = $(this).val();
    customerID = $(this).find(':selected').data('customer');    
    $("#intCustomerID").val(customerID);
    loadWarehouse('kontrak', kontrakID);
    loadSubkontrak('kontrak',kontrakID);
    loadCustomer(customerID).done(function(customer){
        $("#intCustomerID").val(customer.data.id);
        $("#customerAddress").val(customer.data.cust_address);
        $("#customerCity").val(customer.data.cust_city);
        $("#customerPhone").val(customer.data.cust_phone);
        $("#customerOutletTypeStr").val(customer.data.cust_limitkredit);
        $("#customerLimit").val(customer.data.cust_limitkredit);
        $("#customerName").val(customer.data.cust_name);
    });
});

$("#selPekerjaan").change(function(){
    var subkontrakID = $(this).val();
    loadTermin(subkontrakID);
});

$("#selTermin").change(function(){
    var termin = $(this).val();
    $.ajax({
        type : "GET",
        dataType: 'json',
        url: "<?=site_url('api/subkontrak_termin/"+termin+"?h="+userHash+"')?>",
        cache : false,
        success : function(result){                          
            if (result.success) {
                $("#selectedTermin tbody tr.info").hide();
                var item = result.data;                
                $("#selectedTermin tbody").html('');
                $("#selectedTermin tbody").append(
                '<tr name=itemrow>'+            
                    '<td id="isiJenis">'+item.jenis+'</td>'+    
                    '<td id="isiTglTagih"><input type="text" class="form-control input-sm jwDateTime" value="'+item.due_date+'" /></td>'+
                    '<td id="isiQty"><input type="text" class="form-control input-sm jwDateTime" value="'+item.terbayar_date+'" /></td>'+
                    '<td id="isiDsc">-</td>'+
                    '<td id="isiSubtotal" class="currency">'+item.amount+'</td>'+                                
                '</tr>');
                $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
                $('.currency').autoNumeric('init', autoNumericOptionsRupiah);
                $("input[name='subTotalTax'], input[name='subTotalNoTax']").val(item.amount);
            }
        }
    });
});

var i=0;
$("#txtNewItem").keyup(function(e){
        if(e.keyCode == 8 || e.keyCode == 46) $("#loadItem").html('');
});
$("#txtNewItem").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        var filter_by_supplier = "";
        if (supplierID != 0) {
            filter_by_supplier = "&filter_prod_supplier_id="+supplierID;
        }
        $.ajax({
            url: "<?=site_url('api/product_unit/list?h="+userHash+"&filter_prod_status=2&"+filter_by_supplier+"&filter_prod_title=*')?>"+$("#txtNewItem").val()+"*",
            beforeSend: function() {                
                $("#loadItem").html('Loading data');
            },
            complete: function() {                
                $("#loadItem").html('');
            },
            success: function(result){
                if(!result.success){ $("#loadItem").html('No data'); return false; }
                $arrSelectedPO = result.data;
                var display=[];                
                $.each(result.data, function(i, arr){
                    var intID = arr['prod_id'];
                    var name = arr['prod_title'];
                    display.push({label: name, value: name, id:intID});
                });                
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO, function(el) {            
            return el.prod_id == ui.item.id
        });                                
        var i=totalitem;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        $("#totalItem").val(totalitem);
        var bahan = $(selectedPO)[0].prod_title;
        var harga = $(selectedPO)[0].prod_price;
        var satuan = $(selectedPO)[0].title_utama;
        var terima = $(selectedPO)[0].title_bpb;
        var intID = $(selectedPO)[0].prod_id;
        $("#selectedItems tbody tr.info").hide();
        $("#selectedItems tbody").append(
        '<tr name=itemrow>'+
            '<td class="cb"><input type="checkbox" name="cbDeleteX[]"/></td>'+
            '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct[]" id="idProduct'+totalitem+'" value="'+intID+'"><input type="hidden" name="isiBahanText[]" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+    
            '<td id="isiHarga'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control currency" type="text" id="inputHarga'+totalitem+'" value="'+harga+'" placeholder="0" name="inputHarga[]"><input type="hidden" name="inputAwal[]" id="inputAwal'+totalitem+'" value="0"></div></td>'+
            '<td id="isiQty'+totalitem+'"><div class="input-group"><input class="form-control qty" type="text" id="inputQty'+totalitem+'" value="" placeholder="0" name="inputQty[]"><input type="hidden" name="inputAwal[]" id="inputAwal'+totalitem+'" value="0"><div class="input-group-addon">'+satuan+'</div></div></td>'+
            '<td id="isiDsc'+totalitem+'"><div class="input-group"><input class="form-control qty" type="text" id="inputDsc'+totalitem+'" value="0" name="inputDsc[]"><div class="input-group-addon">%</div></div></td>'+
            '<td id="isiSubtotal'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control qty" type="text" id="inputSubtotal'+totalitem+'" value="0" placeholder="0" name="inputSubtotal[]" readonly></div></td>'+            
            '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="0">'+
        '</tr>');

        totalitem++;
        numberItem++;
        $(".currency, .qty").autoNumeric('init',autoNumericOptionsRupiah);
        $("#totalItem").val(totalitem);
        $("#txtNewItem").val(''); return false;
    }
});

$("#selectedItems tbody").on('click', "input[type='checkbox'][name^='cbDeleteX']", function(e){    
    totalitem--;
    numberItem--;
    $(e.target)
    .parents('tr')
    .hide(1, function(){            
        $(e.target).parents('tr').remove()
        calculateGrandtotal();
    });        
    if(numberItem == 0){
        $("#selectedItems tbody tr.info").show();
    }    
    $("#totalItem").val(totalitem);
});

$("#selectedItems tbody").on('keyup', '.qty, .currency', function(e) {        
    calculateSubtotal(e);
});

$("#txtTax, #dsc4Item").keyup(function() {
    calculateFinaltotal();
});

function highlightTextInField($element) {
    // var e = jQuery.Event("keydown");
    // var element_id = $element.attr('id');
    // e.ctrlKey = true;
    // e.which = 65;
    // $element.trigger(e);
    $element.trigger('select');
}

$(document).keydown(function (e) {
    var code = e.charCode || e.keyCode || e.which;
    var submit = false;
    var $element = null;
    switch(code) {
        case 13:
            if (e.ctrlKey) {
                $element = $('#btnPembayaran');
                submit = true;
            }else{
                e.preventDefault();    
            }
        break;
        case 117: // F6: Barang
            if(e.ctrlKey) {
                $element = $('#selectedItems tbody tr:last-child input[name^=qty]');
            } else {
                $element = $('#txtNewItem');
            }
        break;
        case 118: // F7: Barang Bonus
            if(e.ctrlKey) {
                $element = $('#selectedItemsBonus tbody tr:last-child input[name^=qty]');
            } else {
                $element = $('#txtNewItemBonus');
            }
        break;
        case 119: // F8: Pembayaran
            $element = $('#txtPayment');
        break;
        case 120: // F9: Submit
            $element = $('button[name=smtMakeInvoice]');
            submit = true;
        break;        
    }    
    if($element && $element.length) {        
        e.preventDefault();
        $element.focus();
        highlightTextInField($element);

        if(submit) {
            setTimeout(function() {
                // alert("Submitted");
                $element.click();
            }, 100);
        }
    }
});

$("#txtPayment").change(function() {
    var totalWithTax=$("#subTotalTax").autoNumeric('get');
    if(parseInt(totalWithTax) == 0) {
        return false;
    }
    var payment = $(this).autoNumeric('get');
    var paymentChange =  parseInt(payment) - parseInt(totalWithTax);
    if(paymentChange < 0) {
        alert('Maaf, pembayaran kurang');
        $(this).val('');
        $('#txtPaymentChange').val(0);
    } else {
        $("#txtPaymentChange").autoNumeric('set', paymentChange);
    }
});

$("input[name='intTipeBayar']").click(function () {
    if ($(this).val() == '2' || $(this).val() == '3') {
        $("#inputNoCard").show();
    }else{
        $("#inputNoCard").hide();
    }
});

$("#btnPembayaran").click(function(){
    var totalPembayaran = $('#subTotalTax').autoNumeric('get');
    $("#modalPembayaran").find("#txtAmountTotalPayment").autoNumeric('set', totalPembayaran);
});

// $("#selSubkontrak").prop('disabled', true);
// $("#selTermin").prop('disabled', true);

// $('#selKontrak').change(function(){
//     $.ajax({ 
//         type : "GET",
//         dataType:'json',
//         url: "<?=site_url('/api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + $('#selKontrak').val(),
//         cache : false,
//         success : function(result, textStatus, xhr){                        
//             $('#selTermin').val('');
//             $('#subkontrakTerminID').val(0);
//             $('#price').autoNumeric('set',0);
//             $("#selTermin").prop('disabled', true);
//             $('#selTermin').html('<option disabled value="0" selected>Select your option</option>');            
//             if (result.success === true) {                
//                 $("#selSubkontrak").prop('disabled', false);
//                 $('#selSubkontrak').html('<option disabled value="0" selected>Select your option</option>');
//                 $.each (result.data, function (index,arr) {                    
//                     $('#selSubkontrak').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
//                 });
//             }
//             $("#selSubkontrak").trigger("chosen:updated");
//             $("#selTermin").trigger("chosen:updated");
//         }
//     });

// });
// $('#selSubkontrak').change(function(){
//     $.ajax({ 
//         type : "GET",
//         dataType:'json',
//         url: "<?=site_url('api/last_termin/list?h="+userHash+"&filter_subkontrak_id=')?>" + $('#selSubkontrak').val(),
//         cache : false,
//         success : function(result){
//             $("#selTermin").prop('disabled', false);
//             $('#selTermin').html('<option disabled value="0" selected>Select your option</option>');
//             $.each (result.data, function (index,arr) {                
//                 $('#selTermin').append('<option value="' + arr['subkontrak_termin_id'] + '" data-price="'+ arr['amount'] +'">' + arr['jenis'] + ' ['+arr['amount']+']</option>');                
//             });
//             $("#selTermin").trigger("chosen:updated");
//             $('#price').autoNumeric('set',0);
            
//         },
//         error: function(xhr, error){
//             $("#selTermin").prop('disabled', true);
//             $('#selTermin').html('<option disabled value="0" selected>Tidak ada invoice yang dapat dibuat / sudah lunas</option>');
//         },
//     });
// });

// $('#selTermin').change(function(){
//     var price = $('#selTermin').find(':selected').data('price');
//     $('#price').autoNumeric('set',price);
// });

function loadCustomer(id = '') {    
    var param;
    if (id != '') {
        param = id+"?h="+userHash+"&filter_cust_status=2";        
    }else{
        param = "list?h="+userHash+"&filter_cust_status=2";
    }
    
    return $.ajax({
        url: "<?=site_url('api/customer/"+param+"')?>",
    });
}

function loadProject(by = 'customer',id = 0){
    if (by == 'customer') {
        fitlerCustomerID = (id != 0) ? '&filter_owner_id='+id : '';
        return $.ajax({
            url: "<?=site_url('api/kontrak/list?h="+userHash+"&filter_status=2"+fitlerCustomerID+"&sort_kont_name=asc')?>",
            dataType: "json",
            method: "GET",
            success: function(result) {
                if (result.success) {
                    $('#selProyek').html('<option disabled value="0" selected>Select your option</option>');            
                        $.each(result.data, function(i,arr) {
                            $("#selProyek").append('<option value="' + arr['id'] + '">' + arr['kont_name'] + '</option>');        
                        });
                    $("#selProyek").trigger("chosen:updated");
                }
            }        
        });  
    }

    if (by == 'project') {
        fitlerProjectID = (id != 0) ? '&filter_id='+id : '';
        return $.ajax({
            url: "<?=site_url('api/kontrak/list?h="+userHash+"&filter_status=2"+fitlerProjectID+"&sort_kont_name=asc')?>",
            dataType: "json",
            method: "GET",
            success: function(result) {
                if (result.success) {
                    $('#selProyek').html('<option disabled value="0" selected>Select your option</option>');            
                        $.each(result.data, function(i,arr) {
                            var selected = (arr['id'] == id) ? "selected": null;
                            $("#selProyek").append('<option data-customer="'+arr['owner_id']+'" value="' +arr['id']+ '"'+selected+'>' + arr['kont_name'] + '</option>');        
                        });
                    $("#selProyek").trigger("chosen:updated");
                }
            }        
        });  
    }
}

function loadSubkontrak(by,id) {
    if (by == 'kontrak') {
        return $.ajax({ 
            type : "GET",
            dataType:'json',
            url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>" + id,
            cache : false,
            success : function(result){
                $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
                if (result.count > 0) {
                    $.each (result.data, function (index,arr) {                    
                        $('#selPekerjaan').append('<option value="' + arr['id'] + '">' + arr['job'] + '</option>');
                    });
                }
                $("#selPekerjaan").trigger("chosen:updated");
            }
        });
    }

    if (by == 'subkontrak') {
        return $.ajax({ 
            type : "GET",
            dataType:'json',
            url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_id=')?>" + id,
            cache : false,
            success : function(result){                
                $('#selPekerjaan').html('<option disabled value="0">Select your option</option>');
                if (result.count > 0) {
                    $.each (result.data, function (index,arr) {                    
                        var selected = (arr['id'] == id) ? "selected":null;
                        $('#selPekerjaan').append('<option value="' + arr['id'] + '"'+selected+'>' + arr['job'] + '</option>');
                    });
                }
                $("#selPekerjaan").trigger("chosen:updated");
            }
        });
    }
}


function loadWarehouse(by,id) {
    if (by == 'kontrak') {
        var filter = "filter_kont-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $('#selWarehouse').append('<option value="' + arr['ware_id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }

    if (by == 'warehouse') {
        var filter = "filter_t-id="+id;
        return $.ajax({
            type : "GET",
            dataType: 'json',
            url: "<?=site_url('api/warehouse_kontrak/list?h="+userHash+"&"+filter+"')?>",
            cache : false,
            success : function(result){                          
                if (result.success) {
                    $('#selWarehouse').html('<option disabled value="0">Select your option</option>');
                    $.each (result.data, function (index,arr) {
                        var selected = arr['ware_id'] == arr['kont_warehouse_id'] ? "selected" : null;
                        $('#selWarehouse').append('<option value="' + arr['id'] + '" selected>' + arr['ware_name'] + '</option>');
                    });
                    $("#selWarehouse").trigger("chosen:updated");                
                }
            } 
        });
    }
}

function loadTermin(subkont) {
    return $.ajax({
        type : "GET",
        dataType: 'json',
        url: "<?=site_url('api/subkontrak_termin/list?h="+userHash+"&filter_subkontrak_id="+subkont+"&filter_terbayar=0&filter_terbayar=lt-amount&filter_status=2')?>",
        cache : false,
        success : function(result){                          
            if (result.success) {                
                $('#selTermin').html('<option disabled value="0" selected>Select your option</option>');
                $.each (result.data, function (index,arr) {                    
                    $('#selTermin').append('<option value="' + arr['id'] + '">' + arr['jenis'] + '</option>');
                });
                // $("#selTermin").trigger("chosen:updated");                
            }
        } 
    });
}

function loadItemBySO(SOID){
    return $.ajax({
        url: "<?=site_url('api/soitem/list?h="+userHash+"&filter_inoi_status=2&filter_inoi_invoice_order_id=')?>"+SOID,
        beforeSend: function() {                
            $("#loadItem").html('Loading data');
        },
        complete: function() {                
            $("#loadItem").html('');
        },
        success: function(result){            
            if (result.success){
                selectedPO = result.data;
                var i=totalitem;
                var qty1HiddenClass = '';
                var qty2HiddenClass = '';
                $("#totalItem").val(totalitem);
                var bahan = $(selectedPO)[0].prod_title;
                var harga = $(selectedPO)[0].inoi_price;
                var qty = $(selectedPO)[0].inoi_qty_display;
                var satuan = $(selectedPO)[0].unit_title; 
                var dsc = $(selectedPO)[0].inoi_discount;
                var subtotalItem = $(selectedPO)[0].inoi_subtotal;
                var subtotal = parseFloat($(selectedPO)[0].inoi_subtotal);
                var tax = parseFloat($(selectedPO)[0].inor_tax);
                var dsc4 = parseFloat($(selectedPO)[0].inor_discount);
                var grandTotal = parseFloat($(selectedPO)[0].inor_grandtotal);
                var intID = $(selectedPO)[0].inoi_product_id;
                $("#selectedItems tbody tr.info").hide();
                $("#selectedItems tbody").append(
                '<tr name=itemrow>'+
                    '<td class="cb"><input type="checkbox" name="cbDeleteX[]"/></td>'+
                    '<td id="isiBahan'+totalitem+'" value="'+bahan+'"><input type="hidden" name="idProduct[]" id="idProduct'+totalitem+'" value="'+intID+'"><input type="hidden" name="isiBahanText[]" id="isiBahanText'+totalitem+'" value="'+bahan+'">'+bahan+'</td>'+    
                    '<td id="isiHarga'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control input-sm currency" type="text" id="inputHarga'+totalitem+'" value="'+harga+'" placeholder="0" name="inputHarga[]"><input type="hidden" name="inputAwal[]" id="inputAwal'+totalitem+'" value="0"></div></td>'+
                    '<td id="isiQty'+totalitem+'"><div class="input-group"><input class="form-control input-sm qty" type="text" id="inputQty'+totalitem+'" value="'+qty+'" placeholder="0" name="inputQty[]"><input type="hidden" name="inputAwal[]" id="inputAwal'+totalitem+'" value="0"><div class="input-group-addon">'+satuan+'</div></div></td>'+
                    '<td id="isiDsc'+totalitem+'"><div class="input-group"><input class="form-control input-sm qty" type="text" id="inputDsc'+totalitem+'" value="'+dsc+'" name="inputDsc[]"><div class="input-group-addon">%</div></div></td>'+
                    '<td id="isiSubtotal'+totalitem+'"><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control input-sm qty" type="text" id="inputSubtotal'+totalitem+'" value="'+subtotalItem+'" placeholder="0" name="inputSubtotal[]" readonly></div></td>'+            
                    '<input type="hidden" name="isiTerPB'+totalitem+'" id="isiTerPB'+totalitem+'" value="0">'+
                '</tr>');
                totalitem++;
                numberItem++;
                $(".currency, .qty").autoNumeric('init',autoNumericOptionsRupiah);
                $("#subTotalNoTax").autoNumeric('set',subtotal);
                $("#dsc4Item").autoNumeric('set',dsc4);
                $("#subTotalWithDisc").autoNumeric('set',(subtotal-(subtotal*dsc4/100)));
                $("#txtTax").autoNumeric('set',tax);
                $("#subTotalTax").autoNumeric('set',grandTotal);                
                $("#totalItem").val(totalitem);

                $("input[name='subTotalNoTax']").val(subtotal);    
                $("input[name='subTotalTax']").val(grandTotal);
            }else{                
                $("#selectedItems tbody tr.info").show();               
            }
        }
    });
}

function calculateSubtotal(e) {
    var currentRow = $(e.currentTarget).closest('tr');
    var harga = currentRow.find("input[name^='inputHarga']").autoNumeric('get');
    var qty = currentRow.find("input[name^='inputQty']").autoNumeric('get');
    var disc = currentRow.find("input[name^='inputDsc']").autoNumeric('get');

    var amountDisc = (harga*qty)*(disc/100)
    
    subtotal = (harga*qty)-amountDisc;

    currentRow.find("input[name^='inputSubtotal']").autoNumeric('set',subtotal);

    calculateGrandtotal();
}

function calculateGrandtotal() {
    var grandTotal = 0;
    $("input[name^='inputSubtotal']").each(function(i,el) {
        grandTotal += parseFloat($(el).autoNumeric('get'));
    });
    $("input[name='subTotalNoTax']").val(grandTotal);    
    $("#subTotalNoTax").autoNumeric('set',grandTotal);
    calculateFinaltotal();
}

function calculateFinaltotal() {
    var dpp = parseFloat($("#subTotalNoTax").autoNumeric('get'));
    var dsc = parseFloat($("#dsc4Item").autoNumeric('get'));
    var tax = parseFloat($("#txtTax").autoNumeric('get'));
    var finalTotal = dpp - dsc;
    $("#subTotalWithDisc").autoNumeric('set',finalTotal);
    finalTotal = finalTotal + (finalTotal*tax/100);
    $("input[name='subTotalTax']").val(finalTotal);
    $("#subTotalTax").autoNumeric('set',finalTotal);
}

$('.chosen').chosen({search_contains: true});

});</script>