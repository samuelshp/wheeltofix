<?php
$strPageTitle = 'Stok In Value';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_stock/stockinvalue', NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group"><div class="input-group">
            <input type="text" name="txtSearchValue" class="form-control" placeholder="Pilih Supplier"/>
            <span class="input-group-btn">
                <button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button>
            </span>
        </div>
        </div>

		<div class="col pull-right" style="margin:0px 0px 0px 10px;"><?php
if(!empty($strKeyword)):
    $strPrintURL = site_url('report_stock/stockinvalue?print=true&supplier='.$strKeyword, NULL, FALSE);?>
            <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
            <a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
endif; ?>  
        </div>

    </form>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div class="col-xs-12"><?=$strPage?></div>
	<div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th>KODE</th>
        <th>BARCODE</th>
        <th>KATEGORI</th>
        <th>MERK</th>
        <th>NAMA</th>
        <th colspan="3">QTY TERJUAL</th>
        <th>TOTAL HPP</th>
        <th>TOTAL JUAL</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrStock)):
    foreach($arrStock as $e): ?>
        <tr>
            <td><?=$e['prod_code']?></td>
            <td><?=$e['prod_barcode']?></td>
            <td><?=$e['proc_title']?></td>
            <td><?=$e['prob_title']?></td>
            <td><?=$e['prod_title']?></td>
            <td><?=$e['qty1']?></td>
            <td><?=$e['qty2']?></td>
            <td><?=$e['qty3']?></td>
            <td><?=setPrice($e['nilai'])?></td>
            <td><?=setPrice($e['nilai_jual'])?></td>
        </tr><?php
    endforeach;    
else: ?>
        <tr><td class="noData" colspan="10"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>