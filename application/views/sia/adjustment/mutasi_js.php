<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$("#txtNewItem").prop("disabled",true);
$("#selToWarehouse").val("0");
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddAdjustment").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddAdjustment").submit(function() {
	if($("#txtWarehouse1").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
	}
	if($("#txtWarehouse2").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
	}
	if(numberItem<=0){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
	}
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-areyousure')?>") == false) return false;
	
	return true;
});
var totalItem=0;
var numberItem=0;
var selectedItemBefore=[];
$("#selAdjustmentType").change(function() {
	if($(this).children("option:selected").val() > 2) {
		$("#txtWarehouse2").prop("disabled",false);
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>");
	} else {
		$("#txtWarehouse2").prop("disabled",true);
		$("#txtWarehouse2").val("");
		$("#selToWarehouse").val("0");
	}
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});

$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    var type=$(val).find('prod_type').text();
					if($.inArray(intID, selectedItemBefore) > -1 || type == '2'){

					}else{
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItem;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td><label id="maxStrX'+i+'"></label></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
				'<td id="subTotalX'+i+'">0</td>'+
				/*'<td><div class="form-group"><select class="required input-sm" name="selAdjust'+i+'" id="selAdjustX'+i+'">' +
				'<option value="1"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-replenishment")?></option>' +
				'<option value="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-reduction")?></option>' +
				'</select></div></td>'+*/
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
				'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		var id=$(selectedPO).find('id').text();
		selectedItemBefore.push(id);
		var max=0;
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
		var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        var hpp=$(selectedPO).find('prod_hpp').text();
        if(hpp==''){
            $("#prcItemX"+i).val(0);
        }else{
            $("#prcItemX"+i).val($(selectedPO).find('prod_hpp').text());
        }
		$("#idItemX"+i).val(id);
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
				}
				$.ajax({
					url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
					success: function(data){
						var xmlDoc = $.parseXML(data);
						var xml = $(xmlDoc);
						$arrSelectedPO = xml;
						max =xml.find('Stock').text();
						for(var rr = 1; rr <= 3; rr++) {
							if($("#conv"+rr+"UnitX"+i).val() != '') {
								var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
						        if(rr == 1) {
						            var conv = xml.find('prod_conv1').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
						                $("#maxStrX"+i).text(hasil);
						                $("#max1ItemX"+i).val(hasil);
						            } else {
						                $("#maxStrX"+i).text('0');
						                $("#max1ItemX"+i).val('0');
						            }
						        } else if(rr == 2) {
						            conv = xml.find('prod_conv2').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
						                $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
						                $("#max2ItemX"+i).val(hasil);
						            } else {
						                $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | 0");
						                $("#max2ItemX"+i).val('0');
						            }
						        } else if(rr == 3) {
									$("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
									$("#max3ItemX"+i).val(hasil);
								}
							}
						}
					}
				});
			}
		});

		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);

		$('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

var valuebefore=0;
$("#selectedItems").on("focus","input[id*='qty']",function(){
	valuebefore=$(this).val();
});
$("#selectedItems").on("change","input[id*='qty']",function(){
	if($(this).val()<0)
		$(this).val(-1*$(this).val());
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var max=0;
	var now=0;
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
		if($("#max"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			max+=$("#conv"+zz+"UnitX"+at).val()*$("#max"+zz+"ItemX"+at).val();
		}

		if($("#qty"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			now+=$("#qty"+zz+"ItemX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
		}
	}
	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		$(this).val(valuebefore);
	}else{
		var previous=$("#subTotalX"+at).autoNumeric('get');
		var price = $("#prcItemX"+at).autoNumeric('get');
		var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
		/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */
		/*if($("#selAdjustX"+at).val()=='2'){
		 subtotal=-subtotal;
		 }*/
		$("#subTotalX"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		/*var conversion=1;
		 $.ajax({
		 url: "<?site_url('inventory_ajax/getConversionUnit', NULL, FALSE)?>/" + $("#selIteMunit"+at).val(),
		 success: function(data){
		 var xmlDoc = $.parseXML(data);
		 var xml = $(xmlDoc);
		 $arrSelectedPO = xml;
		 conversion = xml.find('Unit').find('item').find('unit_conversion').text();
		 if(max < $("#qtyItem"+at).val()*conversion){
		 var newQty=max/conversion;
		 $("#qtyItem"+at).val(Math.floor(newQty));
		 }
		 }
		 });*/
	}
});

$("#selectedItems").on("change","select[name*='selAdjust']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	if($('#selAdjustX'+at).val()==2){
		var max1=$("#max1ItemX"+at).val();
		var max2=$("#max2ItemX"+at).val();
		var max3=$("#max3ItemX"+at).val();
		var qty1=$("#qty1ItemX"+at).val();
		var qty2=$("#qty2ItemX"+at).val();
		var qty3=$("#qty3ItemX"+at).val();
		if(qty1>max1 ){
			$("#qty1ItemX"+at).val(max1);
		}
		if(qty2>max2 ){
			$("#qty2ItemX"+at).val(max2);
		}
		if(qty3>max3 ){
			$("#qty3ItemX"+at).val(max3);
		}
		var previous=$("#subTotalX"+at).autoNumeric('get');
		var price = $("#prcItemX"+at).autoNumeric('get');
		var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
		/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */
		$("#subTotalX"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	}

});

/*$("#selectedItems").on("change","select[name*='selAdjust']",function(){
 if($(this).val()=='2'){
 var at=this.name.substring(this.name.length-1,this.name.length);
 var value = $(this).val();
 var convert1=1;
 var convert2=1;
 if (value !== selectedBefore) {
 $.ajax({
 url: "<?=site_url('inventory_ajax/getComparisonUnits', NULL, FALSE)?>/" + selectedBefore+"/"+value,
 success: function(data){
 var xmlDoc = $.parseXML(data);
 var xml = $(xmlDoc);
 $arrSelectedPO = xml;
 var display=[];
 $.map(xml.find('Unit').find('item'),function(val,i){
 var intID = $(val).find('id').text();
 var compValue = $(val).find('unit_conversion').text();
 if(intID==selectedBefore){
 convert1=compValue;
 }
 else{
 convert2=compValue;
 }
 });
 if(convert1*$('#qtyItem'+at).val()>$('#maxItem'+at).val()){
 var temp=0;
 $("#qtyItem"+at).val(temp);
 }
 selectedBefore=value;
 }
 });
 }
 }
 });*/

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems").append(
			'<tr><td colspan="7">'+'' +
				'<h4 style="text-align:center">'+
				'<?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?>'+
				'</h4>'+
				'</td></tr>'
		);
	}
	$(this).closest("tr").remove();
});

$("#txtWarehouse").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedPO).find('id').text());
		if($("#WarehouseID").val()!='' /*&& $("#IDSupp").val()!=''*/){
			$("#txtNewItem").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}else{
			$("#txtNewItem").prop('disabled',true);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}
		$("#selectedItems tbody").html('');
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		)
		selectedItemBefore=[];
		totalItem=0;
		numberItem=0;
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});

$("#txtWarehouse2").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse2").val(),
			beforeSend: function() {
				$("#loadWarehouse2").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse2").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID2").val($(selectedPO).find('id').text());
        $("#loadWarehouse2").html('<i class="fa fa-check"></i>');
	}
});

$("#selSupp").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#selSupp").val(),
			beforeSend: function() {
				$("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupp").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label: strName, value: strName,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#IDSupp").val($(selectedPO).find('id').text());
		if($("#WarehouseID").val()!='' /*&& $("#IDSupp").val()!=''*/){
			$("#txtNewItem").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}else{
			$("#txtNewItem").prop('disabled',true);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}
		selectedItemBefore=[];
		totalItem=0;
		numberItem=0;
		$("#selectedItems tbody").html('');
		$("#selectedItemsBonus tbody").html('');
		$("#selectedItems tbody").append('<tr class="info"><td colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadSupp").html('<i class="fa fa-check"></i>');
    }
});
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);

});</script>