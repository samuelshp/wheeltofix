<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);
$("select[name='selJumpTo']").change(function() {
	if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});
var selectedItemBefore=$('#idItemAwal').val().split("-");
var selectedAdjustmentdBefore=$('#idAdjustmentAwal').val().split("-");
var numberitemawal=selectedItemBefore.length-1;
var totalitem=0;
var numberitem=0;
numberitemawal=$("#totalItemAwal").val();
$("#txtWarehouse").prop('disabled',true);
$("#txtWarehouse2").prop('disabled',true);

$("#frmChangeAdjustment").submit(function() {
	var form = $(this);
	if(totalitem=='0' && numberitemawal=='0'){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>");
		return false;
	}

    $("#txtWarehouse1").prop('disabled',false);
    $("#txtWarehouse2").prop('disabled',false);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	$('.subTotal').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});

$.ajax({
	url: "<?=site_url('inventory_ajax/getProductStockNew', NULL, FALSE)?>/" + $('#idItemAwal').val()+"/" + $('#idAdjustmentAwal').val()+"/" + $("#WarehouseID").val(),
	success: function(data){
		var xmlDoc = $.parseXML(data);
		var xml = $(xmlDoc);
		$arrSelectedPO = xml;
		$.map(xml.find('Stock').find('item'),function(val,i){
			var max=$(val).find('stock').text();
			var id=$(val).find('id').text();
            for(var zz=1;zz<=3;zz++){
                max=Math.floor(parseInt(max)-(parseInt($("#txtItem"+zz+"Conv"+id).val())*parseInt($("#txtItem"+zz+"Qty"+id).val())))
            }
			for(var zz=1;zz<=3;zz++){
				if($("#txtItem"+zz+"Conv"+id).val()!=''){
					var hasil=Math.floor(parseInt(max)/parseInt($("#txtItem"+zz+"Conv"+id).val()));
					if(zz==1){
						var conv = xml.find('prod_conv1').text();
						if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
							$("#maxStrAwal"+id).text(hasil);
							$("#max1Item"+id).val(hasil);
			            } else {
			                $("#maxStrAwal"+id).text('0');
							$("#max1Item"+id).val('0');
			            }
					}
					if(zz==2){
						conv = xml.find('prod_conv2').text();
			            if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
							$("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
							$("#max2Item"+id).val(hasil);
			            } else {
			                $("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | 0");
							$("#max2Item"+id).val('0');
			            }
					}
					if(zz==3){
						$("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
						$("#max3Item"+id).val(hasil);
					}
				}
			}
		});
	}
});



$( "input[name*='cbDeleteAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	numberitemawal--;
	if((numberitemawal+numberitem)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItem"+at).val();
	});
	$(this).closest("tr").remove();
});




$( "input[name*='Item']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	for(var zz=1;zz<=3;zz++){
		if($("#txtItem"+zz+"Qty"+at).val()<0){
			$("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
		}
		if($("#txtItem"+zz+"Qty"+at).val()>$("#max"+zz+"Item"+at).val()){
            if($("#sel1Adjust"+at).is(':checked')){

            }else{
			    $("#txtItem"+zz+"Qty"+at).val($("#max"+zz+"Item"+at).val());
            }
		}
	}
	if($("#txtItemPrice"+at).autoNumeric('get')<0){
		$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotal"+at).autoNumeric('get');
	var price = $("#txtItemPrice"+at).autoNumeric('get');
	var subtotal=$("#txtItem1Qty"+at).val()*price+($("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val())+($("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

	$("#subTotal"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
});

$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBefore) > -1){

					}else{
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalitem;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if((numberitemawal+numberitem)=='0') {$("#invoiceItemList tbody").html('');}

		$("#invoiceItemList tbody").append(
			'<tr>' +
				'<td class="cb"><input type="checkbox" name="cbDeleteNewX'+i+'"/></td>' +
				'<td><label id="maxStrX'+i+'"></label></td>'+
				'<td class="qty"><div class="form-group">' +
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0" /></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0" /></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0" /></div>' +
				'</div></td>' +
				'<td><label id="nmeItemX'+i+'"></label></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
				'<td class="subTotal" id="subTotalX'+i+'">0</td>'+
				'<td><div class="form-group">' +
                '<input type="radio" name="selAdjust'+i+'" id="selAdjust1X'+i+'" value="1" checked><label id="selText1X'+i+'"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-replenishment")?><br></label>' +
                '<input type="radio" name="selAdjust'+i+'" id="selAdjust2X'+i+'" value="2"><label id="selText2X'+i+'"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-reduction")?></label>' +
                '</div></td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
				'</tr>'
		);
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		var id=$(selectedPO).find('id').text();
		selectedItemBefore.push(id);
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
		var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
		$("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
		$("#idItemX"+i).val(id);
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
        if($("#isMutasi").val()!=0){
            $("#selAdjust2X"+i).hide();
            $("#selAdjust1X"+i).val(3);
            $("#selText1X"+i).text("Mutasi");
            $("#selText2X"+i).text("");
        }
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
				}
				$.ajax({
					url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
					success: function(data){
						var xmlDoc = $.parseXML(data);
						var xml = $(xmlDoc);
						$arrSelectedPO = xml;
						max =xml.find('Stock').text();
						for(var rr = 1; rr <= 3; rr++) {
							if($("#conv"+rr+"UnitX"+i).val() != '') {
								var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
                                if(rr == 1) {
                                    var conv = xml.find('prod_conv1').text();
                                    if(parseInt(conv) > 1) {
                                        max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                        $("#maxStrX"+i).text(hasil);
                                        $("#max1ItemX"+i).val(hasil);
                                    } else {
                                        $("#maxStrX"+i).text('0');
                                        $("#max1ItemX"+i).val('0');
                                    }
                                } else if(rr == 2) {
                                    conv = xml.find('prod_conv2').text();
                                    if(parseInt(conv) > 1) {
                                        max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                        $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
                                        $("#max2ItemX"+i).val(hasil);
                                    } else {
                                        $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | 0");
                                        $("#max2ItemX"+i).val('0');
                                    }
                                } else if(rr == 3) {
									$("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
									$("#max3ItemX"+i).val(hasil);
								}
							}
						}
					}
				});
			}
		});
		totalitem++;
		numberitem++;
		$("#totalItem").val(totalitem);

		$('#txtNewItem').val(''); return false; // Clear the textbox
	}
});



$("#invoiceItemList").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
	}
	if($("#prcItemX"+at).autoNumeric('get')<0){
		$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotalX"+at).autoNumeric('get');
	var price = $("#prcItemX"+at).autoNumeric('get');
	var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */
	$("#subTotalX"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);

});

$("#invoiceItemList").on("click","input[type='checkbox'][name*='cbDeleteNew']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);

	numberitem--;
	if((numberitem+numberitemawal)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#invoiceItemList").on("click","input[name*='selAdjust']:radio",function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);
    if($('#sel2Adjust'+at).is(':checked')){
        for(var zz=1;zz<=3;zz++){
            if($("#txtItem"+zz+"Qty"+at).val()<0){
                $("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
            }
            if($("#txtItem"+zz+"Qty"+at).val()>$("#max"+zz+"Item"+at).val()){
                if($("#sel1Adjust"+at).is(':checked')){

                }else{
                    $("#txtItem"+zz+"Qty"+at).val($("#max"+zz+"Item"+at).val());
                }
            }
        }
        if($("#txtItemPrice"+at).autoNumeric('get')<0){
            $("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
        }
        var previous=$("#subTotal"+at).autoNumeric('get');
        var price = $("#txtItemPrice"+at).autoNumeric('get');
        var subtotal=$("#txtItem1Qty"+at).val()*price+($("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val())+($("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val());
        /* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

        $("#subTotal"+at).autoNumeric('set',subtotal);
        var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
        $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    }
});

$("#invoiceItemList").on("click","input[id*='selAdjust']:radio",function(){
    var at=this.id.substring(this.id.indexOf("X")+1,this.id.length);
    if($('#selAdjust2X'+at).is(':checked')){
        for(var zz=1;zz<=3;zz++){
            if($("#qty"+zz+"ItemX"+at).val()<0){
                $("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
            }
            if($("#qty"+zz+"ItemX"+at).val()>$("#max"+zz+"ItemX"+at).val()){
                if($("#selAdjust1X"+at).is(':checked')){

                }else{
                    $("#qty"+zz+"ItemX"+at).val($("#max"+zz+"ItemX"+at).val());
                }
            }
        }
        if($("#txtItemPrice"+at).autoNumeric('get')<0){
            $("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
        }
        var previous=$("#subTotalX"+at).autoNumeric('get');
        var price = $("#prcItemX"+at).autoNumeric('get');
        var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
        /* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

        $("#subTotalX"+at).autoNumeric('set',subtotal);
        var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
        $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    }
});

$("#txtWarehouse").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete')?>/" + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedPO).find('id').text());
		if($("#WarehouseID").val()!='' /*&& $("#IDSupp").val()!=''*/){
			$("#txtNewItem").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}else{
			$("#txtNewItem").prop('disabled',true);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}
		$("#selectedItems tbody").html('');
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		)
		selectedItemBefore=[];
		totalitem=0;
		numberitem=0;
	}
});

$("#txtWarehouse2").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse2").val(),
			beforeSend: function() {
				$("#loadWarehouse2").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse2").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID2").val($(selectedPO).find('id').text());
	}
});


});</script>