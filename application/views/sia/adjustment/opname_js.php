<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
var calculated=0;
$("#txtNewItem").prop('disabled',true);
$(".hideme").hide();
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddPurchase").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddPurchase").submit(function(e) {
	if(calculated==0) {
		e.preventDefault();

		if($("#WarehouseID").val() == '') {
			alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
		}
		if(numberItem<=0){
			alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
		}

		$("#btnSumItem").click(); return false;
		// alert("Tekan tombol Hitung Barang"); return false;
	} else {
		if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-areyousure')?>") == false) return false;
	}

	return true;
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});

$("#btnSumItem").click(function(){
	if(numberItem <= 0) return false;

	var purchasedIDItem=[];
	var purchasedQty1Item=[];
	var purchasedQty2Item=[];
	var purchasedQty3Item=[];
	var purchasedConv1Item=[];
	var purchasedConv2Item=[];
	var purchasedConv3Item=[];
	var purchasedNameItem=[];
	var purchasedProdItem=[];
	var purchasedProbItem=[];
	var purchasedUnit1Item=[];
	var purchasedUnit2Item=[];
	var purchasedUnit3Item=[];
	var purchasedHolder1Item=[];
	var purchasedHolder2Item=[];
	var purchasedHolder3Item=[];
    var purchasedPriceItem=[];
	for(var i=0;i<totalitem;i++){
		if(isNaN(parseInt($("#qty1ItemX"+i).val()))){
			$("#qty1ItemX"+i).val(0);
		}
		if(isNaN(parseInt($("#qty2ItemX"+i).val()))){
			$("#qty2ItemX"+i).val(0);
		}
		if(isNaN(parseInt($("#qty3ItemX"+i).val()))){
			$("#qty3ItemX"+i).val(0);
		}
		var flag=0;
		for(var j=0;j<purchasedIDItem.length;j++){
			if(purchasedIDItem[j]==$("#idItemX"+i).val()){
				purchasedQty1Item[j]=parseInt(purchasedQty1Item[j])+(parseInt($("#qty1ItemX"+i).val()));
				purchasedQty2Item[j]=parseInt(purchasedQty2Item[j])+(parseInt($("#qty2ItemX"+i).val()));
				purchasedQty3Item[j]=parseInt(purchasedQty3Item[j])+(parseInt($("#qty3ItemX"+i).val()));
				flag=1;
			}
		}
		if(flag==0){
			purchasedIDItem.push($("#idItemX"+i).val());
			purchasedQty1Item.push(parseInt($("#qty1ItemX"+i).val()));
			purchasedQty2Item.push(parseInt($("#qty2ItemX"+i).val()));
			purchasedQty3Item.push(parseInt($("#qty3ItemX"+i).val()));
			purchasedNameItem.push($("#nmeItemX"+i).text());
			purchasedProbItem.push($("#probItemX"+i).val());
			purchasedProdItem.push($("#prodItemX"+i).val());
			purchasedUnit1Item.push($("#sel1UnitIDX"+i).val());
			purchasedUnit2Item.push($("#sel2UnitIDX"+i).val());
			purchasedUnit3Item.push($("#sel3UnitIDX"+i).val());
			purchasedHolder1Item.push($("#qty1ItemX"+i).attr("placeholder"));
			purchasedHolder2Item.push($("#qty2ItemX"+i).attr("placeholder"));
			purchasedHolder3Item.push($("#qty3ItemX"+i).attr("placeholder"));
			purchasedConv1Item.push(parseInt($("#conv1UnitX"+i).val()));
			purchasedConv2Item.push(parseInt($("#conv2UnitX"+i).val()));
			purchasedConv3Item.push(parseInt($("#conv3UnitX"+i).val()));
            purchasedPriceItem.push(parseFloat($("#PriceX"+i).val()));
		}
	}
	for(var i=0;i<purchasedIDItem.length;i++){
		if(purchasedNameItem[i]==''){
			purchasedIDItem.splice(i,1);
			purchasedQty1Item.splice(i,1);
			purchasedQty2Item.splice(i,1);
			purchasedQty3Item.splice(i,1);
			purchasedNameItem.splice(i,1);
			purchasedProbItem.splice(i,1);
			purchasedProdItem.splice(i,1);
			purchasedUnit1Item.splice(i,1);
			purchasedUnit2Item.splice(i,1);
			purchasedUnit3Item.splice(i,1);
			purchasedHolder1Item.splice(i,1);
			purchasedHolder2Item.splice(i,1);
			purchasedHolder3Item.splice(i,1);
			purchasedConv1Item.splice(i,1);
			purchasedConv2Item.splice(i,1);
			purchasedConv3Item.splice(i,1);
            purchasedPriceItem.splice(i,1);
		}
	}
	$("#selectedItems tbody").html('');
	totalitem=0;
	numberItem=0;
	var id='';
	var tag='';
	for(var i=0;i<purchasedIDItem.length;i++){
		var qty1HiddenClass = '';
	    var qty2HiddenClass = '';
	    var prod_conv1 = purchasedUnit1Item[i];
	    var prod_conv2 = purchasedUnit2Item[i];
	    if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
	    if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';

		$("#selectedItems tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" /></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" /></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" /></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="hideme" id="stockX'+i+'"></td>'+
				'<td class="hideme" id="differentX'+i+'"></td>'+
				'<input type="hidden" id="diffY1X'+i+'" name="1different'+i+'" >'+
                '<input type="hidden" id="diffY2X'+i+'" name="2different'+i+'" >'+
                '<input type="hidden" id="diffY3X'+i+'" name="3different'+i+'" >'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="priceItemX'+i+'" name="priceItem'+i+'" value="0">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'" value="0">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'" value="0">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'" value="0">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
				'</tr>');
		$("#nmeItemX"+i).text(purchasedNameItem[i]);
        $("#priceItemX"+i).val(purchasedPriceItem[i]);
		$("#idItemX"+i).val(purchasedIDItem[i]);
		id+=purchasedIDItem[i]+"-";
		tag+=i+"-";
		$("#prodItemX"+i).val(purchasedProdItem[i]);
		$("#probItemX"+i).val(purchasedProbItem[i]);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
		if(purchasedUnit1Item[i]!=0){
			$("#qty1ItemX"+i).show();
			$("#qty1ItemX"+i).val(purchasedQty1Item[i]);
			$("#sel1UnitIDX"+i).val(purchasedUnit1Item[i]);
			$("#conv1UnitX"+i).val(purchasedConv1Item[i]);
			$("#qty1ItemX"+i).attr("placeholder",purchasedHolder1Item[i]);
		}
		if(purchasedUnit2Item[i]!=0){
			$("#qty2ItemX"+i).show();
			$("#qty2ItemX"+i).val(purchasedQty2Item[i]);
			$("#sel2UnitIDX"+i).val(purchasedUnit2Item[i]);
			$("#conv2UnitX"+i).val(purchasedConv2Item[i]);
			$("#qty2ItemX"+i).attr("placeholder",purchasedHolder2Item[i]);
		}
		if(purchasedUnit3Item[i]!=0){
			$("#qty3ItemX"+i).show();
			$("#qty3ItemX"+i).val(purchasedQty3Item[i]);
			$("#sel3UnitIDX"+i).val(purchasedUnit3Item[i]);
			$("#conv3UnitX"+i).val(purchasedConv3Item[i]);
			$("#qty3ItemX"+i).attr("placeholder",purchasedHolder3Item[i]);
		}
		totalitem++;
		numberItem++;

	}
	$(".hideme").show();
	id+="0";
	tag+="0"
	$("#totalItem").val(totalitem);
	$.ajax({
		url: "<?=site_url('inventory_ajax/getProductStockNew', NULL, FALSE)?>/" + id + "/"+tag+"/" + $("#WarehouseID").val(),
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$.map(xml.find('Stock').find('item'),function(val,i){
				var stock = $(val).find('stock').text();
				var counter=$(val).find('id').text();
				var string='';
				if(stock==''){
					stock=0;
				}
				var sisa=stock;
				var now=0;
				for(var zz=1;zz<=3;zz++){
					if($("#conv"+zz+"UnitX"+counter).val()>0){
						var temp=0;
						temp=parseInt(sisa/$("#conv"+zz+"UnitX"+counter).val());
						sisa=sisa-(temp*$("#conv"+zz+"UnitX"+counter).val());
						$("#stockX"+counter).text($("#stockX"+counter).text()+temp);
						if(zz<3){
							$("#stockX"+counter).text($("#stockX"+counter).text()+" | ");
						}
						if(isNaN($("#qty"+zz+"ItemX"+counter).val()) || $("#qty"+zz+"ItemX"+counter).val()<0){
							$("#qty"+zz+"ItemX"+counter).val(0);
						}
						now+=parseInt($("#qty"+zz+"ItemX"+counter).val()*$("#conv"+zz+"UnitX"+counter).val());
					}
				}
				var selisih=parseInt(now-stock);
                var tanda='';
                if(selisih<0){
                    tanda="-";
                }
                var stringdiff = [];
                for(var zz=1;zz<=3;zz++) {
                    var temp=0;
                    if($("#conv"+zz+"UnitX"+counter).val() > 1 || zz >= 3) {
                    	temp=parseInt(selisih/$("#conv"+zz+"UnitX"+counter).val());
                    }
                    $("#diffY"+zz+"X"+counter).val(temp);
					stringdiff.push(temp);
                    selisih=selisih-temp*$("#conv"+zz+"UnitX"+counter).val();
                }
                var stringdiff = stringdiff.join(' + ');
                stringdiff = stringdiff.replace(/\-/g, '');
                if(tanda!=""){
                    $("#differentX"+counter).text(tanda+"("+stringdiff+")");
                }else{
                    $("#differentX"+counter).text(stringdiff);
                }
			});

			calculated=1;
			$("#frmAddPurchase").submit();
		}
	});
});

var totalitembonus=0;
var numberitembonus=0;
var totalitem=0;
var numberItem=0;


$("#txtNewItem").autocomplete({
	minLength:1,
	delay:500,
	source: function(request,response) {
		var filterType = $('#selProductType option:selected').val();
		var unselect = ['0'];
		$('[id^=idItemX]').each(function(index) {
			unselect.push($(this).val());
		});
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoComplete', NULL, FALSE)?>/"  + $('input[name="txtNewItem"]').val() + '/' + unselect.join('-') + '/' + filterType,
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				if(filterType == 'Product') {
					$.map(xml.find('Product').find('item'),function(val,i) {
						var intID = $(val).find('id').text();
						var strName=$(val).find('strName').text();
	                    var strKode=$(val).find('prod_code').text();
	                    var type=$(val).find('prod_type').text();
						if(type == '1') display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					});
				} else if(filterType == 'Category') {
					$.map(xml.find('Category').find('item'),function(val,i) {
						var intID = $(val).find('id').text();
						var strName=$(val).find('proc_title').text();
						display.push({label: strName, value: strName, id:intID});
					});
				} else if(filterType == 'Brand') {
					$.map(xml.find('Brand').find('item'),function(val,i) {
						var intID = $(val).find('id').text();
						var strName=$(val).find('prob_title').text();
						display.push({label: strName, value: strName, id:intID});
					});
				}
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var filterType = $('#selProductType option:selected').val();
		var selectedPO = $.grep($arrSelectedPO.find(filterType).find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		if(filterType == 'Category') var fieldSelector = 'proc_title';
		else if(filterType == 'Category') var fieldSelector = 'prob_title';
		var selectedValue = $(selectedPO).find(fieldSelector).text();

		if(filterType == 'Product') {
			addSelectedItem(selectedPO);
		} else {
			$.grep($arrSelectedPO.find('Product').find('item'), function(el) {
				if($(el).find(fieldSelector).text() == selectedValue) addSelectedItem(el);
			});
		}

		$('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

function addSelectedItem(selectedPO) {
	var i=totalitem;
	calculated=0;
	if(numberItem=='0'){
		$("#selectedItems tbody").html('');
	}
	var qty1HiddenClass = '';
    var qty2HiddenClass = '';
    var prod_conv1 = $(selectedPO).find('prod_conv1').text();
    var prod_conv2 = $(selectedPO).find('prod_conv2').text();
    if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
    if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
	$("#selectedItems tbody").append(
		'<tr>'+
			'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
			'<td class="qty"><div class="form-group">'+
			'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
			'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
			'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
			'</div></td>'+
			'<td id="nmeItemX'+i+'"></td>'+
			'<td class="hideme" id="stockX'+i+'"></td>'+
			'<td class="hideme" id="differentX'+i+'"></td>'+
			'<td class="hideme" ><input type="checkbox" id="includeX'+i+'" name="include'+i+'" value="1"></td>'+
			'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
			'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
			'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
            '<input type="hidden" id="PriceX'+i+'">' +
			'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'" value="0">' +
			'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'" value="0">' +
			'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'" value="0">'+
			'<input type="hidden" id="conv1UnitX'+i+'" >' +
			'<input type="hidden" id="conv2UnitX'+i+'" >' +
			'<input type="hidden" id="conv3UnitX'+i+'" >'+
			'</tr>');
    var prodcode = $(selectedPO).find('prod_code').text();
	var prodtitle = $(selectedPO).find('prod_title').text();
	var probtitle = $(selectedPO).find('prob_title').text();
	var proctitle = $(selectedPO).find('proc_title').text();
	var strName=$(selectedPO).find('strName').text();
    var price=$(selectedPO).find('prod_hpp').text();
	var id=$(selectedPO).find('id').text();
	$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
	var proddesc = $(selectedPO).find('prod_description').text();
    if(proddesc != '') {
        $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
        $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
    }
	$("#idItemX"+i).val(id);
	$("#prodItemX"+i).val(prodtitle);
	$("#probItemX"+i).val(probtitle);
    $("#PriceX"+i).val(price);
	$("#qty1ItemX"+i).hide();
	$("#qty2ItemX"+i).hide();
	$("#qty3ItemX"+i).hide();
	$.ajax({
		url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$arrSelectedPO = xml;
			/* var x=totalItem-1; */
			var unitID=[];
			var unitStr=[];
			var unitConv=[];
			$.map(xml.find('Unit').find('item'),function(val,j){
				var intID = $(val).find('id').text();
				var strUnit=$(val).find('unit_title').text();
				var intConv=$(val).find('unit_conversion').text();
				unitID.push(intID);
				unitStr.push(strUnit);
				unitConv.push(intConv);
				/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
			});
			for(var zz=1;zz<=unitID.length;zz++){
				$("#qty"+zz+"ItemX"+i).show();
				$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
				$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
				$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
			}
		}
	});
	$(".hideme").hide();
	totalitem++;
	numberItem++;
	$("#totalItem").val(totalitem);
}

$('#btnPrintList').click(function() {
	var unselect = [];
	$('[id^=idItemX]').each(function(index) {
		unselect.push($(this).val());
	});
	if($('#WarehouseID').val())
		window.open('<?=site_url('inventory/print_opname', NULL, FALSE)?>/' + $('#WarehouseID').val() + '/' + unselect.join('-'), '_blank');
	else alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>");
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItem--;
	if(numberItem=='0'){
        $('.hideme').hide();
		$("#selectedItems tbody").html('');
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>');
	}
	$(this).closest("tr").remove();
});


$("#txtWarehouse").autocomplete({
	minLength:1,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		calculated=0;
		$("#WarehouseID").val($(selectedPO).find('id').text());
		$("#txtNewItem").prop('disabled',false);
		$("#selectedItems tbody").html('');
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>');
		$(".hideme").hide();
		numberItem=0;
		totalitem=0;
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});

});</script>