<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bullseye"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustment'), 'link' => site_url('adjustment/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
	<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-lastpurchaseorder')?></label>
	<select name="selJumpTo" class="form-control">
		<option value="">- Jump To -</option><?php
		if(!empty($arrAdjustmentList)) foreach($arrAdjustmentList as $e):
			$strListTitle = ' title="'.$e['from_ware'].' -> '.$e['to_ware'].'"'; ?>
		<option value="<?=site_url('adjustment/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['ajst_code']/*formatDate2($e['ajst_date'],'d/m/Y')*/?>] <?=$e['from_ware']?> -> <?=$e['to_ware']?></option><?php
		endforeach; ?>
	</select>
</div></div>

<?php $fromid = $arrAdjustmentData['from_id']; ?>
<?php $toid = $arrAdjustmentData['to_id']; ?>
<div class="col-xs-12"><form name="frmChangeAdjustment" id="frmChangeAdjustment" method="post" action="<?=site_url('adjustment/view/'.$intAdjustmentID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehouse')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-atwarehouse')?></div>
			<?php if($bolBtnEdit): ?>
				<div class="col-xs-8"><div class="input-group"><input type="text" id="txtWarehouse" name="txtWarehouse" value="<?=$arrAdjustmentData['from_ware']?>" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>"/><label class="input-group-addon" id="loadWarehouse"></label></div></div>
			<?php else: ?>
				<div class="col-xs-8"><?=$arrAdjustmentData['from_ware']?></div>
			<?php endif; ?>
			<p class="spacer">&nbsp;</p>
			<?php if($arrAdjustmentData['to_ware']!=''){ ?>
				<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-towarehouse')?></div>
				<?php if($bolBtnEdit) { ?>
					<div class="col-xs-8"><div class="input-group"><input type="text" id="txtWarehouse2" name="txtWarehouse2" value="<?=$arrAdjustmentData['to_ware']?>" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>"/><label class="input-group-addon" id="loadWarehouse2"></label></div></div>
				<?php }else{ ?>
					<div class="col-xs-8"><?=$arrAdjustmentData['to_ware']?></div>
				<?php } ?>
				<p class="spacer">&nbsp;</p>
			<?php }?>
		</div>
	</div></div>

    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
			<div class="col-xs-8"><?=$arrAdjustmentData['ajst_code']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
			<div class="col-xs-8"><?=formatDate2($arrAdjustmentData['ajst_date'],'d F Y')?></div>
			<p class="spacer">&nbsp;</p>
		</div>
	</div></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">
    	<?php if($bolBtnEdit): ?>
        <div class="form-group"><div class="input-group addNew">
			<input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
		</div></div>
        <?php endif; ?>

        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
			<thead>
			<tr>
				<?php if($bolBtnEdit) { ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php } ?>
				<?php if($bolBtnEdit) { ?><th>Stock</th> <?php }?>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustmenttype')?></th>
			</tr>
			</thead>
			<tbody><?php
            $mutasi=0;
			$i = 0;
			$intTotalItem = 0;
			$iditemawal='';
			$idpurchaseawal = '';
			if(!empty($arrAdjustmentItem)):
				foreach($arrAdjustmentItem as $e):
					$iditemawal=$iditemawal.$e['product_id'].'-';
					$idpurchaseawal=$idpurchaseawal.$e['id'].'-';
					$intTotalItem++; ?>
					<tr><?php
					if($bolBtnEdit) { ?>
						<td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]" /></td><?php
					} ?>
					<?php if($bolBtnEdit) { ?>
						<td><label id="maxStrAwal<?=$e['id']?>"></label></td>
					<?php } ?>
					<td class="qty"><?php
						if($bolBtnEdit): //if yes ?>
						<div class="form-group">
							<div class="col-xs-4"><input type="text" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['adit_quantity1']?>" class="required number form-control input-sm<?=$e['prod_conv1'] <= 1 ? ' hidden' : ''?>" id="txtItem1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['adit_unit1'])?>" title="<?=formatUnitName($e['adit_unit1'])?>" /></div>
							<div class="col-xs-4"><input type="text" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['adit_quantity2']?>" class="required number form-control input-sm<?=$e['prod_conv2'] <= 1 ? ' hidden' : ''?>" id="txtItem2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['adit_unit2'])?>" title="<?=formatUnitName($e['adit_unit2'])?>" /></div>
							<div class="col-xs-4"><input type="text" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['adit_quantity3']?>" class="required number form-control input-sm" id="txtItem3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['adit_unit3'])?>" title="<?=formatUnitName($e['adit_unit3'])?>" /></div>
							<input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['adit_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
							<input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['adit_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
							<input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['adit_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
							<input type="hidden" name="idItem<?=$e['id']?>" value="<?=$e['product_id']?>" id="idItem<?=$e['id']?>" />
							<input type="hidden" id="max1Item<?=$e['id']?>"  />
							<input type="hidden" id="max2Item<?=$e['id']?>"  />
							<input type="hidden" id="max3Item<?=$e['id']?>"  />
						</div><?php

						else: echo
							$e['adit_quantity1'].' '.formatUnitName($e['adit_unit1']).' + '.
							$e['adit_quantity2'].' '.formatUnitName($e['adit_unit2']).' + '.
							$e['adit_quantity3'].' '.formatUnitName($e['adit_unit3']);
						endif; ?>
							<input type="hidden" name="idAditID[<?=$e['id']?>]" value="<?=$e['id']?>" />
					</td>
					<td><b><?="(".$e['prod_code'].") ".$e['strName']?></b></td>
					<td class="pr"><?php
						if($bolBtnEdit): ?>
							<div class="input-group"><label class="input-group-addon"><?=str_replace(' 0','',setPrice('','USED'))?></label><input type="text" name="txtItemPrice[<?=$e['id']?>]" class="required currency form-control input-sm" id="txtItemPrice<?=$e['id']?>" value="<?=$e['adit_price']?>" /></div>
						<?php
						else:
							echo setPrice($e['adit_price'],'USED');
						endif; ?>
					</td>
					<td class="subTotal" id="subTotal<?=$e['id']?>"><?=$e['adit_subtotal']?></td>
					<td><?php
						if($bolBtnEdit) { ?>
							<?php if($arrAdjustmentData['to_ware']==''){ ?>
								<div class="form-group">
                                    <input type="radio" name="selAdjust[<?=$e['id']?>]" id="sel1Adjust<?=$e['id']?>" value="1" <?php if($e['adit_adjustment_type']=='1'){ ?> checked <?php } ?>><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-replenishment")?><br>
                                    <input type="radio" name="selAdjust[<?=$e['id']?>]" id="sel2Adjust<?=$e['id']?>" value="2" <?php if($e['adit_adjustment_type']=='2'){ ?> checked <?php } ?>><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-reduction")?>
							    </div>
							<?php } else{ $mutasi++;?>
								<div class="form-group">
								    <?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"adjustment-mutation")?>
                                    <input type="hidden" name="selAdjust[<?=$e['id']?>]" id="selAdjust<?=$e['id']?>" value="3">
                                </div>
							<?php } ?>
						<?php }
						else {
							if($e['adit_adjustment_type']=='1'){
								echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-replenishment');
							}elseif($e['adit_adjustment_type']=='2'){
								echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-reduction');
							}elseif($e['adit_adjustment_type']=='3'){
								echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-mutation');
                                $mutasi++;
							}
						} ?>
					</td>
					</tr><?php

					$i++;
				endforeach;

				$iditemawal = $iditemawal.'0';
				$idpurchaseawal = $idpurchaseawal.'0';
			else: ?>
				<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
			endif; ?>
			</tbody>
		</table></div>

        <div class="row">
            <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?=$intAdjustmentTotal?></div>
        </div>
        <p class="spacer">&nbsp;</p>

    </div><!--/ Table Selected Items -->
</div>

<div class="row">

    <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
            <div class="panel-body"><?php
                if($bolBtnEdit): ?>
                    <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrAdjustmentData['ajst_description']?></textarea></div><?php
                else: ?>
                    <input type="hidden" name="txaDescription" value="<?=$arrAdjustmentData['ajst_description']?>" />
					<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrAdjustmentData['ajst_description']?></div><?php
                endif; ?>
                <div class="form-group">
                    <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')?></label>
                    <input type="text" name="txtProgress" value="<?=$arrAdjustmentData['ajst_progress']?>" maxlength="64" class="form-control" />
                </div>
            </div>
        </div></div>
		
</div>

<?php 
    $rawStatus = $arrAdjustmentData['ajst_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>  

<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>
<input type="hidden" id="idAdjustmentAwal" name="idAdjustmentAwal" value="<?=$idpurchaseawal?>"/>
<input type="hidden" id="WarehouseID" name="WarehouseID" value="<?=$fromid?>"/>
<input type="hidden" id="WarehouseID2" name="WarehouseID2" value="<?=$toid?>"/>
<input type="hidden" id="isMutasi"  value="<?=$mutasi?>"/>
</form></div>