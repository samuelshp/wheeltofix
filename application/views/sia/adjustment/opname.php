<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-bullseye"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustment'), 'link' => site_url('adjustment/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-crosshairs"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-opname'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><form name="frmAddPurchase" id="frmAddPurchase" method="post" action="<?=site_url('adjustment/opname', NULL, FALSE)?>" class="frmShop">

	<div class="row">
		<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-bookmark"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedata')?></h3></div>
			<div class="panel-body">
				<h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?> <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
				<div class="form-group"><div class="input-group"><input type="text" id="txtWarehouse" name="txtWarehouse" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>"/><label class="input-group-addon" id="loadWarehouse"></label></div></div>
				<!-- Dibuat -->
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
			 </div>
		</div></div>
		<div class="col-md-6"><div class="panel panel-primary">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
				<div class="panel-body">
					<div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
				</div>
			</div></div>

	</div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-opnameitem')?></h3></div>
        <div class="panel-body">
        	<div class="form-group"><div class="input-group addNew">
        		<span class="input-group-btn">
        			<select id="selProductType" class="form-control" style="width: 100px;">
        				<option value="Product">Produk</option>
        				<option value="Category">Kategori</option>
        				<option value="Brand">Merk</option>
        			</select>
        		</span>
        		<span class="input-group-addon" style="padding: 6px 6px;">
        			<i class="fa fa-filter"></i>
        		</span>
				<input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
			</div></div>

            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
				<thead>
                *) quantity yang dimasukkan berupa quantity real
				<tr>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
					<th class="hideme">Stock</th>
					<th class="hideme">Difference</th>
				</tr>
				</thead>
				<tbody>
				<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
				</tbody>
			</table></div>
			<p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>


    <div class="form-group">
    	<button type="button" id="btnSumItem" class="btn btn-primary hidden"><i class="fa fa-calculator"></i></button>
    	<button type="submit" name="smtMakePurchase" value="Make Purchase" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button>
    	<a class="btn btn-success btn-sm pull-right" href="javascript: void(0)" id="btnPrintList"><i class="fa fa-list-alt"></i></a>
    </div>

<input type="hidden" id="IDSupp" name="IDSupp"/>
<input type="hidden" id="totalItem" name="totalItem"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
<input type="hidden" id="WarehouseID" name="WarehouseID"/>

</form></div>