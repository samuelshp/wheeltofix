<?php
if(!empty($strSearchKey) || !empty($strSearchDate) || !empty($intSearchStatus)) {
    $strSearchFormClass = ' has-warning';
    $strSearchButtonClass = ' btn-warning';
} else {
    $strSearchKey = '';
    $strSearchFormClass = ''; 
    $strSearchButtonClass = ' btn-primary';
} ?>
    <form name="searchForm" method="post" action="<?=$strSearchAction?>" class="form-inline pull-right" style="">
        <div class="input-group<?=$strSearchFormClass?>">
            <span class="input-group-addon"><i class="fa fa-file"></i></span>
            <input type="text" id="txtSearchNo" name="txtSearchNo" class="form-control input-sm" placeholder="Search No. Pembayaran" style="width: 110px;" />
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" id="txtSearchTgl" name="txtSearchTgl" class="form-control jwDate" placeholder="Search Tanggal Pembayaran" style="width: 120px; min-width: auto;" value=""/>
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" id="txtSearchCustomer" name="txtSearchCustomer" class="form-control input-sm" placeholder="Search Customer" style="width: 110px;" />
            <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary<?=$strSearchButtonClass?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
        </div>
    </form>