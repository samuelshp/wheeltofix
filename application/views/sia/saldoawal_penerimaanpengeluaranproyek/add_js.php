<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);   
$("input.currency").autoNumeric('set',0);

$("form#frm").submit(function(){	
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);			
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));			
		}
	});	
});

$('.chosen').chosen({width: "100%",search_contains: true});   
</script>