<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> Saldo Awal Penerimaan-Pengeluran Proyek', 'link' => site_url('account_start/sappproyek/browse')),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
    <form method="POST" action="<?=site_url('account_start/sappproyek/', NULL, FALSE)?>" id="frm" >          
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New</h3></div>
                    <div class="panel-body" >                        
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Tanggal
                            </div>
                            <div class="col-sm-10">
                                <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDate"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Proyek
                            </div>
                            <div class="col-sm-10">
                                <select type="text" name="strProyek" class="form-control chosen">
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e) { ?>
                                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>                       
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Invoice <small>(Prestasi Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="prestasibefore" type="text" required style="z-index: 0" />                                   
                                </div>                                
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                PPI/Kas Bank Masuk <small>(Penerimaan Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="penerimaanbefore" type="text" required style="z-index: 0" />
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Purchase Invoice <small>(Biaya Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="biayabefore" type="text" required style="z-index: 0" />
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>  
            </div>              
        </div>                 
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary" name="btnSave" value="save"><i class="fa fa-plus"></i></button>
            </div>          
        </div>  
    </form>
</div>