<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-money"></i> Saldo Awal Penerimaan-Pengeluran Proyek', 'link' => site_url('account_start/sappproyek/browse')),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
    <form method="POST" action="<?=site_url('account_start/sappproyek/', NULL, FALSE)?>" id="frm" >          
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New</h3></div>
                    <div class="panel-body" >                        
                        <div class="form-group">
                            <input type="hidden" name="intPage" value="<?=$arrDetail['id']?>">
                            <div class="col-sm-2 control-label">
                                Tanggal
                            </div>
                            <div class="col-sm-10">                                
                                <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($arrDetail['tanggal'])) echo $arrDetail['tanggal']; else echo date('Y/m/d'); ?>" class="form-control required jwDate" disabled/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Proyek
                            </div>
                            <div class="col-sm-10">
                                <select type="text" name="strProyek" class="form-control chosen" disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e) { ?>
                                        <option value="<?=$e['id']?>" <?=($arrDetail['kontrak_id'] == $e['id']) ? "selected": null ?>><?=$e['kont_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Invoice <small>(Prestasi Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="prestasibefore" type="text" required style="z-index: 0" value="<?=$arrDetail['prestasi_before_nominal']?>" disabled/>                                   
                                </div>                                
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                PPI/Kas Bank Masuk <small>(Penerimaan Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="penerimaanbefore" type="text" required style="z-index: 0" value="<?=$arrDetail['penerimaan_before_nominal']?>" disabled/>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">
                                Purchase Invoice <small>(Biaya Sebelum)</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-group">
                                   <div class="input-group-addon">Rp.</div>
                                   <input class="form-control currency" name="biayabefore" type="text" required style="z-index: 0" value="<?=$arrDetail['biaya_before_nominal']?>" disabled/>
                                </div>                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>  
            </div>              
        </div>                 
        <div class="row">
            <div class="col-xs-12">            
                <button type="button" id="btnEdit" class="btn btn-primary"><i class="fa fa-edit"></i></button>
                <button type="submit" id="btnUpdate" name="smtUpdate" value="Update DPH" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                <button type="submit" id="btnDelete" name="smtDelete" value="Delete DPH" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                  
            </div>          
        </div>  
    </form>
</div>