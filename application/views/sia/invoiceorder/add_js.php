<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
var internal=0;
$("#supplierName").val("All");
$("#supplierName").prop('disabled',true);
$("#txtNewItem").prop('disabled',true);
$("#txtNewItemBonus").prop('disabled',true);
$("#supplierID").val(0);
$("#customerID").val('');
$('input[name="customerOutletType"]').val(2);
$('input[name="outletMarketType"]').val(1);
var selectedItemBefore = [];
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';<?php
if(!empty($arrOwnerInfo['strOwnerData4'])) {
    $arrPriceFormula = explode(' | ', $arrOwnerInfo['strOwnerData4']);
    foreach($arrPriceFormula as $i => $e) {
        $arrPriceFormula[$i] = explode(':', $e);
    }
    echo PHP_EOL."var arrPriceFormula = ".json_encode($arrPriceFormula).";".PHP_EOL;
} ?>  
var mode=0;
var totalItem=0;
var numberItem=0;
var totalItemBonus = 0;
var numberItemBonus = 0;
var selectedItemBonusBefore=[];
var allowedBonusID=[];
var allowedBonusQty=[];
var strAllowedBonusID='';
var notIncludedBonusBrand = [];
var notIncludedBonusName = [];
var notIncludedBonusDisc = [];
var notIncludedBonusQty = [];

$("#customerLimit").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddInvoice").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddInvoice").submit(function() {
    if($("input[type='radio'][name='radioCustomer']:checked").val()!=0){
        if(parseFloat( $("#customerLimit").autoNumeric('get'))<parseFloat( $("#subTotalTax").autoNumeric('get'))){
            alert("jumlah pembelian melebihi limit yang dimiliki oleh pelanggan member");
            // return false;
        }
    }
	$("#dsc4ItemX").autoNumeric('destroy');

	if($("#supplierName").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectsupplier')?>"); return false;
	}

	if($("#warehouseName option:selected").val() == 0) {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
	}

	if($("#customerName").val()=='') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"); return false;
	}

	if(numberItem<=0) {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
	}
    //tambah sini
    for(var i=0;i<notIncludedBonusBrand.length;i++){
        $("#strBonusNotInclude").append('' +
            '<input type="hidden" name="potonganBrand['+i+']" value="'+notIncludedBonusBrand[i]+'"/>' +
            '<input type="hidden" name="potonganName['+i+']" value="'+notIncludedBonusName[i]+'"/>' +
            '<input type="hidden" name="potonganDisc['+i+']" value="'+notIncludedBonusDisc[i]+'"/>' +
            '<input type="hidden" name="potonganQty['+i+']" value="'+notIncludedBonusQty[i]+'"/>');
    }
	$('.currency').each(function(i) {
		var self = $(this);
		try {
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err) {
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});

	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;

	return true;
});

$("input[name*='radioSupplier']").change(function(){
    if($(this).val()=='0'){
        $("#supplierName").val("All");
        $("#supplierName").prop('disabled',true);
        $("#supplierID").val(0);
    }else{
        $("#supplierName").val("");
        $("#supplierName").prop('disabled',false);
    }
    $("#txtNewItem").prop('disabled',true);
    $("#txtNewItemBonus").prop('disabled',true);
    totalItemBonus=0;
    numberItemBonus=0;
    selectedItemBonusBefore=[];
    selectedItemBefore=[];
    totalItem=0;
    numberItem=0;
});
$("input[name*='radioCustomer']").change(function(){
    if($(this).val()==0){
        $("#customerID").val(0);
        $('input[name="customerOutletType"]').val(2);
        $('input[name="outletMarketType"]').val(1);
    }else{
        $("#customerID").val('');
    }
    $("#customerAddress").val("-");
    $("#customerCity").val("-");
    $("#customerPhone").val("-");
    $("#customerOutletType").val("-");
    $("#customerLimit").autoNumeric('set',0);
    $("#txtJatuhTempo").val(0);
    $("#txtNewItem").prop('disabled',true);
    $("#txtNewItemBonus").prop('disabled',true);
    totalItemBonus=0;
    numberItemBonus=0;
    selectedItemBonusBefore=[];
    selectedItemBefore=[];
    totalItem=0;
    numberItem=0;
});
$("#customerName").change(function(){
    if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
       if($(this).val()!='' && $("#WarehouseID").val()!='' && $("#supplierID").val()!=''){
           $("#txtNewItem").prop('disabled',false);
           $("#txtNewItemBonus").prop('disabled',false);
       }else{
           $("#txtNewItem").prop('disabled',true);
           $("#txtNewItemBonus").prop('disabled',true);
       }
    }
});
$("#supplierName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getSupplierAutoComplete', NULL, FALSE)?>/" + $("#supplierName").val(),
			beforeSend: function() {
				$("#loadSupplier").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupplier").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSupplier = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
                    var code = $(val).find('supp_code').text();
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSupplier = $.grep($arrSelectedSupplier.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		$("#supplierID").val($(selectedSupplier).find('id').text());
		$("#supplierAddress").text($(selectedSupplier).find('supp_address').text());
		$("#supplierCity").text($(selectedSupplier).find('supp_city').text());
		$("#supplierPhone").text($(selectedSupplier).find('supp_phone').text());
		$('input[name="supplierID"]').val($(selectedSupplier).find('id').text());

		totalItemBonus=0;
		numberItemBonus=0;
		selectedItemBonusBefore=[];
		selectedItemBefore=[];
		strAllowedBonusID='';
		totalItem=0;
		numberItem=0;
        if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerName").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }else{

            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerID").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }
		$("#customerName").prop('disabled',false);
        $("#salesmanName").prop('disabled',false);
		$("#strBonusItem").html('');
        $("#strBonusNotInclude").html('');
		$("#selectedItems tbody").html('');
		$("#selectedItemsBonus tbody").html('');
		$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadSupplier").html('<i class="fa fa-check"></i>');
    }
});

$("#customerName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"<?=site_url('invoice_ajax/NULL', NULL, FALSE)?>":"<?=site_url('invoice_ajax/getSaleOrderCustomerAutoCompleteWithSupplierBind', NULL, FALSE)?>/" + $("#customerName").val()+ "/" + $("#supplierID").val(),
			beforeSend: function() {
                ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"":$("#loadCustomer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
                ($("input[type='radio'][name='radioCustomer']:checked").val()==0)?"":$("#loadCustomer").html('');
			},
			success: function(data){
                if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
                    $("#customerID").val(0);
                    $("#customerAddress").val("-");
                    $("#customerCity").val("-");
                    $("#customerPhone").val("-");
                    $("#customerOutletType").val(2);
                    $("#customerLimit").autoNumeric('set',0);
                }
                else{
                    var xmlDoc = $.parseXML(data);
                    var xml = $(xmlDoc);
                    $arrSelectedCust = xml;
                    var display=[];
                    $.map(xml.find('Customer').find('item'),function(val,i){
                        var name = $(val).find('cust_name').text();
                        var address = $(val).find('cust_address').text();
                        var city = $(val).find('cust_city').text();
                        var code = $(val).find('cust_code').text();
                        var outlettype = $(val).find('cust_outlettype').text();
                        var outlettypeStr = '';
                        switch (outlettype) {
                            case '1': outlettypeStr = 'BARU'; break;
                            case '2': outlettypeStr = 'BIASA'; break;
                            case '3': outlettypeStr = 'MEMBER'; break;
                            case '4': outlettypeStr = 'RESELLER'; break;
                            case '5': outlettypeStr = 'KANTOR'; break;
                            default : outlettypeStr = 'N/A';
                        }

                        var strName=name+", "+ address +", "+city+", "+outlettypeStr;
                        var intID = $(val).find('id').text();
                        display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
                    });
                    response(display);
                }
			}
		});
	},
	select: function(event, ui) {
		var selectedCust = $.grep($arrSelectedCust.find('Customer').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
        $.ajax({
            url: "<?=site_url('invoice_ajax/checkCustomerBill', NULL, FALSE)?>/" + $(selectedCust).find('id').text()+ "/" + $("#supplierID").val(),
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                var notif='Customer ini memiliki invoice yang belum dibayar dengan id : ';
                var counter=0;
                $.map(xml.find('Customer').find('item'),function(val,i){
                    var invoiceid = $(val).find('id').text();
                    notif+='/n -'+invoiceid;
                    counter=1;
                });
                if(counter!='0'){
                    alert(notif);
                }
            }
        });
		$("#customerID").val($(selectedCust).find('id').text());
		$("#customerAddress").val($(selectedCust).find('cust_address').text());
		$("#customerCity").val($(selectedCust).find('cust_city').text());
		$("#customerPhone").val($(selectedCust).find('cust_phone').text());
		$("#customerOutletType").val($(selectedCust).find('cust_outlettype').text());
		$("#customerLimit").autoNumeric('set',$(selectedCust).find('cust_limitkredit').text());
        $("#txtJatuhTempo").val($(selectedCust).find('cust_jatuhtempo').text());
        $('input[name="outletMarketType"]').val($(selectedCust).find('cust_markettype').text());
		internal=parseInt($(selectedCust).find('cust_internal').text());
		/* disini ajax e */
		$.ajax({
			url: "<?=site_url('invoice_ajax/getCustomerCreditByCustomerID', NULL, FALSE)?>/" + $("#customerID").val(),
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$.map(xml.find('Kredit').find('item'),function(val,i){
					var credit = $(val).find('invo_grandtotal').text();
                    var limit = parseFloat($("#customerLimit").autoNumeric('get'))-parseFloat(credit);
                    if(limit > 0) {
                       $("#customerLimit").autoNumeric('set',limit);
                    } else {
					   $("#customerLimit").autoNumeric('set',0);
                    }
				});
			}
		});
		var outlettypeStr = '';
		switch ($(selectedCust).find('cust_outlettype').text()) {
			case '1': outlettypeStr = 'BARU'; break;
            case '2': outlettypeStr = 'BIASA'; break;
            case '3': outlettypeStr = 'MEMBER'; break;
            case '4': outlettypeStr = 'RESELLER'; break;
            case '5': outlettypeStr = 'KANTOR'; break;
			default : outlettypeStr = 'N/A';
		}
		$("#customerOutletTypeStr").val(outlettypeStr);
		$('input[name="customerOutletType"]').val($(selectedCust).find('cust_outlettype').text());
		$('input[name="customerID"]').val($(selectedCust).find('id').text());
        if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerName").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }else{
            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerID").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }
		selectedItemBefore=[];
		selectedItemBonusBefore=[];
		numberItem=0;
		numberItemBonus=0;
		strAllowedBonusID='';
		$("#strBonusItem").html('');
        $("#strBonusNotInclude").html('');
		$("#selectedItems tbody").html('');
		$("#selectedItemsBonus tbody").html('');
		$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadCustomer").html('<i class="fa fa-check"></i>');
    }
});

$("#salesmanName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url:($("input[type='radio'][name='radioSalesman']:checked").val()==0)?"<?=site_url('invoice_ajax/getInvoiceSalesmanAutoComplete', NULL, FALSE)?>/" + $("#salesmanName").val() + "/0":"<?=site_url('invoice_ajax/getInvoiceSalesmanAutoComplete', NULL, FALSE)?>/" + $("#salesmanName").val() + "/" + $("#supplierID").val(),
			beforeSend: function() {
				$("#loadSalesman").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSalesman").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				var display=[];
				$.map(xml.find('Salesman').find('item'),function(val,i){
					var name = $(val).find('sals_name').text();
					/*var address = $(val).find('cust_address').text();
					var city = $(val).find('cust_city').text();*/
					var code = $(val).find('sals_code').text();
					var strName= name/*+", "+ address +", "+city*/;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedSO = $.grep($arrSelectedSO.find('Salesman').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#salesmanID").text($(selectedSO).find('id').text());
		$("#salesmanCode").text($(selectedSO).find('sals_code').text());
		/*$("#customerAddress").text($(selectedSO).find('cust_address').text());
		$("#customerCity").text($(selectedSO).find('cust_city').text());
		$("#customerPhone").text($(selectedSO).find('cust_phone').text());*/
		$('input[name="salesmanID"]').val($(selectedSO).find('id').text());
		$('input[name="salesmanCode"]').val($(selectedSO).find('sals_code').text());
        $("#loadSalesman").html('<i class="fa fa-check"></i>');
    }
});
$("input[name='intJualKanvas']:radio").change(function () {
    totalItemBonus=0;
    numberItemBonus=0;
    selectedItemBonusBefore=[];
    selectedItemBefore=[];
    strAllowedBonusID='';
    totalItem=0;
    numberItem=0;
    $("#strBonusItem").html('');
    $("#strBonusNotInclude").html('');
    $("#selectedItems tbody").html('');
    $("#selectedItemsBonus tbody").html('');
    $("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
    $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');

});
$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
	$(this).autoNumeric('init', autoNumericOptionsRupiah);
});

var valuebefore=0;
var oldprice=0;
var olddsc1=0;
var olddsc2=0;
var olddsc3=0;
var oldqty1=0;
var oldqty2=0;
var oldqty3=0;

$("#selectedItems").on("focus","input[type='text'][name*='PriceEffect']",function(){
	valuebefore=$(this).val();
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	oldprice=$("#prcItemX"+at).autoNumeric('get');
	olddsc1=$("#dsc1ItemX"+at).val();
	olddsc2=$("#dsc2ItemX"+at).val();
	olddsc3=$("#dsc3ItemX"+at).val();
    oldqty1=$("#qty1PriceEffectX"+at).val();
    oldqty2=$("#qty2PriceEffectX"+at).val();
    oldqty3=$("#qty3PriceEffectX"+at).val();
});
$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var max=0;
	var now=0;
	var temp='';
    if($('input[name="intJualKanvas"]:radio:checked').val()=="1"){
        max=1;
        now=0;
    }else{
        for(var zz=1;zz<=3;zz++){
            if($("#qty"+zz+"PriceEffectX"+at).val()<0){
                $("#qty"+zz+"PriceEffectX"+at).val($("#qty"+zz+"PriceEffectX"+at).val()*-1);
            }
            if($("#max"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
                max+=$("#conv"+zz+"UnitX"+at).val()*$("#max"+zz+"ItemX"+at).val();
            }

            if($("#qty"+zz+"PriceEffectX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
                now+=$("#qty"+zz+"PriceEffectX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
            }
            if($("#SameX"+at).val()!=-1 && $("#qty"+zz+"ItemBonusX"+$("#SameX"+at).val()).val()>0 && $("#conv"+zz+"UnitBonusX"+$("#SameX"+at).val()).val()>0){
                now+=$("#qty"+zz+"ItemBonusX"+$("#SameX"+at).val()).val()*$("#conv"+zz+"UnitBonusX"+$("#SameX"+at).val()).val();
            }
        }
    }

	if(max < now  && $("#typeItemX"+at).val() == '1') {
        alert("jumlah barang yang dimasukkan melebihi stock yang ada");
        // $(this).val(valuebefore);
        getBonus(at, true);
    } else {
		getBonus(at, true);
	}
});

function getBonus(at, doCountSubtotal) {
    var product='';
    var brand='';
    var qty='';
    for(var i=0;i<=totalItem;i++)
    {
        if($("#nmeItemX"+i).text()!='')
        {
            var temp=0;
            product=product+$("#idItemX"+i).val()+"-";
            brand=brand+$("#idBrandX"+i).val()+"-";
            if($("#conv1UnitX"+i).val()>0 && $("#qty1PriceEffectX"+i).val()>0){
                temp=parseInt(temp)+parseInt($("#conv1UnitX"+i).val())*parseInt($("#qty1PriceEffectX"+i).val());
            }
            if($("#conv2UnitX"+i).val()>0 && $("#qty2PriceEffectX"+i).val()){
                temp=parseInt(temp)+parseInt($("#conv2UnitX"+i).val())*parseInt($("#qty2PriceEffectX"+i).val());
            }
            if($("#conv3UnitX"+i).val()>0 && $("#qty3PriceEffectX"+i).val()){
                temp=parseInt(temp)+parseInt($("#conv3UnitX"+i).val())*parseInt($("#qty3PriceEffectX"+i).val());
            }
            qty=qty+temp.toString()+"-";
        }
    }
    product=product+"0";
    brand=brand+"0";
    qty=qty+"0";
    var date=$('input[name="txtDate"]').val();
    date=date.replace(/\//g, '-');
    $("#selectedItemsBonus tbody").html('');
    $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
    numberItemBonus=0;
    totalItemBonus=0;
    strAllowedBonusID='';
    selectedItemBonusBefore=[];
    $("#strBonusItem").html('');
    $("#strBonusNotInclude").html('');
    $.ajax({
        url: "<?=site_url('invoice_ajax/getBonusByPromo', NULL, FALSE)?>/" + product + '/' + brand + "/" + qty + "/" + date,
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var counterbonus1 =0;
            var counterbonus2 =0;
            allowedBonusID=[];
            allowedBonusQty=[];
            notIncludedBonusBrand=[];
            notIncludedBonusName=[];
            notIncludedBonusDisc=[];
            notIncludedBonusQty=[];
            $.map(xml.find('Bonus').find('item'),function(val,i){
                var intID = $(val).find('id').text();
                var intQty=$(val).find('qty').text();
                var intDsc=$(val).find('discount').text();
                var intInclude=$(val).find('include').text();
                var strName=$(val).find('name').text();
                var type=$(val).find('type').text();
                if(type == 'merk') {
                    if((parseInt(intQty)>0) && parseInt(intInclude)==1){
                        if(counterbonus1==0){
                            $("#strBonusItem").append(
                                'Bonus yang dapat diambil : <br>'
                            );
                            counterbonus1++;
                        }
                        $("#strBonusItem").append(
                            '> '+strName
                        );
                        if(parseInt(intQty)>0){
                            $("#strBonusItem").append(
                                ' (Bonus: ' + intQty + ')'
                            );
                        }
                        $("#strBonusItem").append(
                            '<br>'
                        );
                        allowedBonusID.push(intID);
                        strAllowedBonusID=strAllowedBonusID+intID+'-';
                        allowedBonusQty.push(intQty);
                    } else if((parseFloat(intDsc)>0) && parseInt(intInclude)>1){
                        notIncludedBonusBrand.push(intID);
                        notIncludedBonusDisc.push(intDsc);
                        notIncludedBonusName.push(strName);
                        var temp=0
                        for(var iii=0;iii<=totalItem;iii++)
                        {
                            if($("#idBrandX"+iii).val()==intID)
                            {
                                if($("#conv1UnitX"+iii).val()>0 && $("#qty1PriceEffectX"+iii).val()>0){
                                    temp=parseInt(temp)+parseInt($("#conv1UnitX"+iii).val())*parseInt($("#qty1PriceEffectX"+iii).val());
                                }
                                if($("#conv2UnitX"+iii).val()>0 && $("#qty2PriceEffectX"+iii).val()){
                                    temp=parseInt(temp)+parseInt($("#conv2UnitX"+iii).val())*parseInt($("#qty2PriceEffectX"+iii).val());
                                }
                                if($("#conv3UnitX"+iii).val()>0 && $("#qty3PriceEffectX"+iii).val()){
                                    temp=parseInt(temp)+parseInt($("#conv3UnitX"+iii).val())*parseInt($("#qty3PriceEffectX"+iii).val());
                                }
                            }
                        }
                        notIncludedBonusQty.push(temp);
                        if(counterbonus2==0){
                            $("#strBonusNotInclude").append(
                                'Bonus yang dipisahkan dari nota : <br>'
                            );
                            counterbonus2++;
                        }
                        $("#strBonusNotInclude").append(
                            '> '+temp+" "+strName
                        );
                        if(parseInt(intDsc)>0){
                            $("#strBonusNotInclude").append(
                                ' (Potongan / Item: ' + intDsc + ')'
                            );
                        }
                        $("#strBonusNotInclude").append(
                            '<br>'
                        );
                    }
                } else if(type == 'diskon') {
                    console.log('diskon');
                    if($("#idItemX"+at).val() == intID) {
                        $("#dsc1ItemX"+at).val(intDsc);
                    }
                } else if(type == 'produk') {
                    console.log('produk');
                    var bonus_code = $(val).find('bonus').text();
                    $('#txtNewItemBonus').val(bonus_code);
                    setTimeout(function() {
                        $('#txtNewItemBonus').autocomplete('search', bonus_code);
                        // Set quantity
                        setTimeout(function() {
                            var product_id = $(val).find('id').text();
                            var $product_element = $("#selectedItems").find("input[type=hidden][id^=idItemX][value="+product_id+"]");
                            var product_at = $product_element.attr('id');
                            product_at = product_at.substring(product_at.indexOf('X')+1,product_at.length);
                            var qty_type = $(val).find('qty_type').text();
                            var qty = $(val).find('qty').text();
                            var syarat_qty = $(val).find('syarat').text();
                            var product_qty = $("#qty"+qty_type+"PriceEffectX"+product_at).val();
                            var result_qty = Math.floor(parseInt(product_qty) / parseInt(syarat_qty)) * qty;
                            $("#selectedItemsBonus tbody tr:last-child").find("input[id^=qty"+qty_type+"ItemBonus]").val(result_qty);
                        }, 1000);
                    }, 500);
                }
            });
            strAllowedBonusID=strAllowedBonusID+'0';
        },
        complete: function() {
            if(doCountSubtotal) countSubtotal(at);
        }
    });
}

function countSubtotal(at) {
    if($("#prcItemX"+at).autoNumeric('get')<0){
        $("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
    }
    var previous=$("#subTotalX"+at).autoNumeric('get');
    var price = $("#prcItemX"+at).autoNumeric('get');
    var priceafterdsc=price;
    var subtotal=0;
    if($("#conv1UnitX"+at).val()>0 && $("#qty1PriceEffectX"+at).val()>0){
        subtotal+=$("#qty1PriceEffectX"+at).val()*price;
    }
    if($("#conv2UnitX"+at).val()>0 && $("#qty2PriceEffectX"+at).val()>0){
        subtotal+=$("#qty2PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val();
    }
    if($("#conv3UnitX"+at).val()>0 && $("#qty3PriceEffectX"+at).val()>0){
        subtotal+=$("#qty3PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val();
    }
    for(var zz=1;zz<=3;zz++){
        subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
        priceafterdsc=priceafterdsc-(priceafterdsc*$("#dsc"+zz+"ItemX"+at).val()/100);
    }
    var satuan=parseFloat(priceafterdsc)/parseFloat($("#conv1UnitX"+at).val());
    var hpp=parseFloat($("#HppX"+at).val())/parseFloat($("#conv1UnitX"+at).val());
    if(satuan<hpp){
        if($("#typeItemX"+at).val() == '1') {
            // var confirmHPP = "Harga satuan dari barang setelah didiskon kurang dari harga hpp, HPP: " + $("#HppX"+at).val() + ", apakah ingin lanjut?";
            var confirmHPP = "Harga satuan dari barang setelah didiskon kurang dari harga HPP";
            alert(confirmHPP);
            // if(!confirm(confirmHPP)) {
            //     // $("#prcItemX"+at).autoNumeric('set',$("#HargaX"+at).val());
            //     $("#prcItemX"+at).autoNumeric('set',oldprice);
            //     $("#dsc1ItemX"+at).val(olddsc1);
            //     $("#dsc2ItemX"+at).val(olddsc2);
            //     $("#dsc3ItemX"+at).val(olddsc3);
            //     $("#qty1PriceEffectX"+at).val(oldqty1);
            //     $("#qty2PriceEffectX"+at).val(oldqty2);
            //     $("#qty3PriceEffectX"+at).val(oldqty3);
            //     return false;
            // }
        }
    }
    $("#subTotalX"+at).autoNumeric('set',subtotal);
    var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
    $("#subTotalNoTax").autoNumeric('set',totalNoTax);
    var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
    if(totalwithdisc<=0){
        $("#subTotalWithDisc").autoNumeric('set','0');
        $("#subTotalTax").autoNumeric('set','0');
    }else{
        $("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
        var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
        $("#subTotalTax").autoNumeric('set',totalTax);
    }
}


$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({

			url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $('input[name="supplierID"]').val() + "/" + $('input[name="customerOutletType"]').val() + "/" + $('input[name="outletMarketType"]').val() + "/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
                $("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedItem = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    var type=$(val).find('prod_type').text();
					if($.inArray(intID, selectedItemBefore) > -1 || type == '2') {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
                if(display.length == 1) {
                    setTimeout(function() {
                        $("#txtNewItem").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                        $('#txtNewItem').autocomplete('close').val('');
                    }, 100);
                }
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var discount1 = $(selectedItem).find('skds_discount1').text();
		var discount2 = $(selectedItem).find('skds_discount2').text();
		var discount3 = $(selectedItem).find('skds_discount3').text();

        if(discount1=='') discount1=0;
        if(discount2=='') discount2=0;
        if(discount3=='') discount3=0;

		var i=totalItem;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedItem).find('prod_conv1').text();
        var prod_conv2 = $(selectedItem).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr id="trItem'+i+'">'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>' +
				'<td  id="maxStrX'+i+'" class="canvas"></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" id="qty1PriceEffectX'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" id="qty2PriceEffectX'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" id="qty3PriceEffectX'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0" list="dlPriceList'+i+'"/></div></td>'+
				'<td class="disc"><div class="form-group">'+
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="'+discount1+'" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="'+discount2+'" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="'+discount3+'" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
				'</div></td>'+
				'<td id="subTotalX'+i+'">0</td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
                '<input type="hidden" id="typeItemX'+i+'" name="typeItemX'+i+'">'+
				'<input type="hidden" id="idBrandX'+i+'" name="idBrand'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" value="0">' +
				'<input type="hidden" id="conv2UnitX'+i+'" value="0">' +
				'<input type="hidden" id="conv3UnitX'+i+'" value="0">'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
				'<input type="hidden" id="SameX'+i+'" value="-1">'+
				'<input type="hidden" id="HppX'+i+'" >'+
                '<input type="hidden" id="HargaX'+i+'" >'+
				'</tr>'
		);
        var prodcode = $(selectedItem).find('prod_code').text();
		var prodtitle = $(selectedItem).find('prod_title').text();
		var probtitle = $(selectedItem).find('prob_title').text();
		var proctitle = $(selectedItem).find('proc_title').text();
		var strName=$(selectedItem).find('strName').text();
		var id=$(selectedItem).find('id').text();
        var type = $(selectedItem).find('prod_type').text();
		selectedItemBefore.push(id);
		var max=0;
		if(prodcode == '') {
          $("#nmeItemX"+i).text(strName);
        } else {
          $("#nmeItemX"+i).text("("+prodcode+") "+ strName);
        }
        var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
		var hpp=$(selectedItem).find('prod_hpp').text();
		if(isNaN(internal) || internal == 1) {
			if ($('input[name="outletMarketType"]').val() == '1') {
				var tempprice=$(selectedItem).find('prod_pricemodern').text();
				/*if(parseInt(tempprice) < parseInt(hpp)){
					$("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
				}else{*/
					$("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
				/*}*/
			} else if ($('input[name="outletMarketType"]').val() == '2') {
				var tempprice=$(selectedItem).find('prod_price').text();
				/*if(parseInt(tempprice) < parseInt(hpp)){
					$("#prcItemX"+i).val(hpp);
                    $("#HargaX"+i).val(hpp);
				}else{*/
					$("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
				/*}*/
			};
		}else{
			var tempprice=$(selectedItem).find('prod_price').text();
            /*$("#prcItemX"+i).val(hpp);
            $("#HargaX"+i).val(hpp);*/
            $("#prcItemX"+i).val(tempprice);
            $("#HargaX"+i).val(tempprice);
		}
		$("#HppX"+i).val(hpp);
		$("#idItemX"+i).val($(selectedItem).find('id').text());
		$("#idBrandX"+i).val($(selectedItem).find('id_brand').text());
        $("#typeItemX"+i).val($(selectedItem).find('prod_type').text());
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        if(typeof arrPriceFormula != 'undefined') {
            $('#trItem'+i).append('<datalist id="dlPriceList'+i+'"></datalist>');
            $.each(arrPriceFormula, function(index, v) {
                var formula = v[1].replace(/\{\{p\}\}/g, 'parseFloat(' + tempprice + ')');
                $('#dlPriceList'+i).append('<option value="' + eval(formula) + '">'+ v[0] +'</option>');
            });
        }
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					/*var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					$('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#sel"+zz+"IteMunitX"+i).text(unitStr[zz-1]);
				}
				var indexof=-1;
				for(var zz=0;zz<totalItemBonus;zz++){
					if($("#idItemBonusX"+zz).val()==id){
						indexof=zz;
					}
				}
				if(indexof==-1){
					$.ajax({
						url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
						success: function(data){
							var xmlDoc = $.parseXML(data);
							var xml = $(xmlDoc);
							$arrSelectedPO = xml;
                            max =xml.find('Stock').text();
							for(var rr = 1; rr <= 3; rr++) {
                                if($("#conv"+rr+"UnitX"+i).val() != '') {
                                    var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
                                    if(rr == 1) {
                                        var conv = xml.find('prod_conv1').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                            $("#maxStrX"+i).text(hasil);
                                            $("#max1ItemX"+i).val(hasil);
                                        } else {
                                            $("#maxStrX"+i).text('0');
                                            $("#max1ItemX"+i).val('0');
                                        }
                                    } else if(rr == 2) {
                                        conv = xml.find('prod_conv2').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                            $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
                                            $("#max2ItemX"+i).val(hasil);
                                        } else {
                                            $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | 0");
                                            $("#max2ItemX"+i).val('0');
                                        }
                                    } else if(rr == 3) {
                                        $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
                                        $("#max3ItemX"+i).val(hasil);
                                    }
                                }
                            }
						}
					});
				}else
				{
					$("#SameX"+i).text(indexof);
					$("#maxStrX"+i).text($("#maxStrBonusX"+indexof).text());
					$("#max1ItemX"+i).val($("#max1ItemBonusX"+indexof).val());
					$("#max2ItemX"+i).val($("#max2ItemBonusX"+indexof).val());
					$("#max3ItemX"+i).val($("#max3ItemBonusX"+indexof).val());
				}
                if($('input[name="intJualKanvas"]:radio:checked').val()=="1"){
                    $(".canvas").hide();
                }
                else{
                    $(".canvas").show();
                }
			}
		});
		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);

        $('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

$("#txtTax").change(function(){
	var totalNoTax=$("#subTotalWithDisc").autoNumeric('get');
	var totalTax=parseInt(totalNoTax)+parseInt(totalNoTax*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    // $(this).closest("tr").find('input, select, textarea').rules('remove');
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
	$("#selectedItemsBonus tbody").html('');
	$("#selectedItemsBonus tbody").append(
		'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
	);

	getBonus(at);
});


$("#warehouseName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#warehouseName").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedWare = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
                if(display.length == 1) {
                    setTimeout(function() {
                        $("#warehouseName").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                        $('#warehouseName').autocomplete('close');
                    }, 100);
                }
				response(display);
            }
		});
	},
	select: function(event, ui) {
		var selectedWare = $.grep($arrSelectedWare.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedWare).find('id').text());
        if($("input[type='radio'][name='radioCustomer']:checked").val()==0){
            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerName").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }else{
            if($("#WarehouseID").val()!='' && $("#supplierID").val()!=''  &&  $("#customerID").val()!=''){
                $("#txtNewItem").prop('disabled',false);
                $("#txtNewItemBonus").prop('disabled',false);
            }else{
                $("#txtNewItem").prop('disabled',true);
                $("#txtNewItemBonus").prop('disabled',true);
            }
        }
		selectedItemBefore=[];
		selectedItemBonusBefore=[];
		strAllowedBonusID='';
		$("#strBonusItem").html('');
        $("#strBonusNotInclude").html('');
		totalItem=0;
		totalItemBonus=0;
		numberItem=0;
		numberItemBonus=0;
		$("#selectedItems tbody").html('');
		$("#selectedItemsBonus tbody").html('');
		$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
    }
});
setTimeout(function() {
    $("#warehouseName").val(primaryWarehouseName);
    $("#warehouseName").autocomplete('search');
},500);

$("#dsc4Item").change(function(){
	var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
})

$("#txtNewItemBonus").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		if(strAllowedBonusID!='') {
			$.ajax({
				url: "<?=site_url('invoice_ajax/getBonusAutoCompleteByPromo', NULL, FALSE)?>/" + strAllowedBonusID + "/" + $('input[name="txtNewItemBonus"]').val(),
				beforeSend: function() {
					$("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
				},
				complete: function() {
					$("#loadItemBonus").html('');
				},
				success: function(data){
					var xmlDoc = $.parseXML(data);
					var xml = $(xmlDoc);
					$arrSelectedPO = xml;
					var display=[];
					$.map(xml.find('Product').find('item'),function(val,i){
						var intID = $(val).find('id').text();
						var strName=$(val).find('strName').text();
						if($.inArray(intID, selectedItemBonusBefore) > -1){

						}else{
							display.push({label: strName, value: '',id:intID});
						}
					});
                    if(display.length == 1) {
                        setTimeout(function() {
                            $("#txtNewItemBonus").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
                            $('#txtNewItemBonus').autocomplete('close').val('');
                        }, 100);
                    }
					response(display);
                }
			});
		}
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItemBonus;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberItemBonus=='0'){
			$("#selectedItemsBonus tbody").html('');
		}
		$("#selectedItemsBonus tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
				'<td  id="maxStrBonusX'+i+'" class="canvas"></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td><label id="nmeItemBonusX'+i+'"></label></td>'+
				'<input type="hidden" id="idBonusBrandX'+i+'" name="idBonusBrand'+i+'">'+
				'<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
				'<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
				'<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
				'<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
				'<input type="hidden" id="conv1UnitBonusX'+i+'" >' +
				'<input type="hidden" id="conv2UnitBonusX'+i+'" >' +
				'<input type="hidden" id="conv3UnitBonusX'+i+'" >'+
				'<input type="hidden" id="max1ItemBonusX'+i+'" name="max1ItemBonus'+i+'">' +
				'<input type="hidden" id="max2ItemBonusX'+i+'" name="max2ItemBonus'+i+'">' +
				'<input type="hidden" id="max3ItemBonusX'+i+'" name="max3ItemBonus'+i+'">'+
				'<input type="hidden" id="SameBonusX'+i+'" value="-1">'+
				'</tr>');
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		var id=$(selectedPO).find('id').text();
		selectedItemBonusBefore.push(id);
		$("#idBonusBrandX"+i).val($(selectedPO).find('brand_id').text());
		$("#nmeItemBonusX"+i).text(strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemBonusX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
		$("#idItemBonusX"+i).val(id);
		$("#prodItemBonusX"+i).val(prodtitle);
		$("#probItemBonusX"+i).val(probtitle);
		$("#qty1ItemBonusX"+i).hide();
		$("#qty2ItemBonusX"+i).hide();
		$("#qty3ItemBonusX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemBonusX"+i).show();
					$("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitBonusX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
				}
				var indexof=-1;
				for(var zz=0;zz<totalItem;zz++){
					if($("#idItemX"+zz).val()==id){
						indexof=zz;
					}
				}
				if(indexof==-1){
					$.ajax({
						url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
						success: function(data){
							var xmlDoc = $.parseXML(data);
							var xml = $(xmlDoc);
							$arrSelectedPO = xml;
							max =xml.find('Stock').text();
							for(var rr = 1; rr <= 3; rr++) {
                                if($("#conv"+rr+"UnitBonusX"+i).val() != '') {
                                    var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                    if(rr == 1) {
                                        var conv = xml.find('prod_conv1').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                            $("#maxStrBonusX"+i).text(hasil);
                                            $("#max1ItemBonusX"+i).val(hasil);
                                        } else {
                                            $("#maxStrBonusX"+i).text('0');
                                            $("#max1ItemBonusX"+i).val('0');
                                        }
                                    } else if(rr == 2) {
                                        conv = xml.find('prod_conv2').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                            $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                            $("#max2ItemBonusX"+i).val(hasil);
                                        } else {
                                            $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | 0");
                                            $("#max2ItemBonusX"+i).val('0');
                                        }
                                    } else if(rr == 3) {
                                        $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                        $("#max3ItemBonusX"+i).val(hasil);
                                    }
                                }
                            }
						}
					});
				}else
				{
					$("#SameBonusX"+i).val(indexof);
					$("#maxStrBonusX"+i).text($("#maxStrX"+indexof).text());
					$("#max1ItemBonusX"+i).val($("#max1ItemX"+indexof).val());
					$("#max2ItemBonusX"+i).val($("#max2ItemX"+indexof).val());
					$("#max3ItemBonusX"+i).val($("#max3ItemX"+indexof).val());
				}
                if($('input[name="intJualKanvas"]:radio:checked').val()=="1"){
                    $(".canvas").hide();
                }
                else{
                    $(".canvas").show();
                }
			}
		});
		totalItemBonus++;
		numberItemBonus++;
		$("#totalItemBonus").val(totalItemBonus);

        $('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItemBonus--;
	if(numberItemBonus=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonusX"+at).val();
	});

	$(this).closest("tr").remove();

});
var oldquantity;
$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffectBonus']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var at2=this.id.substring(this.id.indexOf('y')+1,this.id.indexOf('I'));
	var indexof=0;
	var max=0;
	var now=0;
	var temp='';

    if($('input[name="intJualKanvas"]:radio:checked').val()=="1"){
        max=1;
        now=0;
    }else{
        for(var zz=1;zz<=3;zz++){
            if($("#qty"+zz+"ItemBonusX"+at).val()<0){
                $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
            }
            if($("#max"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"UnitBonusX"+at).val()>0){
                max+=$("#conv"+zz+"UnitBonusX"+at).val()*$("#max"+zz+"ItemBonusX"+at).val();
            }

            if($("#qty"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"UnitBonusX"+at).val()>0){
                now+=$("#qty"+zz+"ItemBonusX"+at).val()*$("#conv"+zz+"UnitBonusX"+at).val();
            }
            if($("#SameBonusX"+at).val()!=-1 && $("#qty"+zz+"PriceEffectX"+$("#SameBonusX"+at).val()).val()>0 && $("#conv"+zz+"UnitX"+$("#SameBonusX"+at).val()).val()>0){
                now+=$("#qty"+zz+"PriceEffectX"+$("#SameBonusX"+at).val()).val()*$("#conv"+zz+"UnitX"+$("#SameBonusX"+at).val()).val();
            }
        }
    }
	if(max<now ){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		// $(this).val(oldquantity);
	}else{
		for(var i=0;i<allowedBonusID.length;i++){
			if(allowedBonusID[i]==$("#idBonusBrandX"+at).val()){
				indexof=i;
				break;
			}
		}
		if($(this).val()<0){
			$(this).val($(this).val()*-1);
		}
		var konversi=$("#conv"+at2+"UnitBonusX"+at).val();
		var temp;
		if(oldquantity==''){
			temp=0;
		}else{
			temp=oldquantity;
		}
		if(parseInt($(this).val())*parseInt(konversi)>parseInt(temp)*parseInt(konversi)+parseInt(allowedBonusQty[indexof])){
			alert("jumlah barang yang dimasukkan melebihi bonus yang dapat diberikan");
			// $(this).val(oldquantity);
		}
		if(oldquantity!=$(this).val()){
			if(oldquantity!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])+(parseInt(oldquantity)*parseInt(konversi));
                alert(allowedBonusQty);
			}
			if($(this).val()!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])-(parseInt($(this).val())*parseInt(konversi));
                alert(allowedBonusQty);
			}
		}
	}

});

$("#selectedItemsBonus").on("focus","input[type='text'][name*='PriceEffectBonus']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	oldquantity=$(this).val();
});

$(document).on('click', '[id^=nmeItem] .fa-question-circle', function() {
    var $this = $(this);
    $.ajax({
        url: "<?=site_url('invoice_ajax/getLastProductPrice')?>",
        data: {
            product_id: $this.closest('tr').find('[id^=idItem]').val(),
        },
        dataType: 'json',
        success: function(data) {
            var msg = "Harga Penjualan Terakhir: \n\n";
            $.each(data.Items, function() {
                msg = msg + this.strDate + ' - ' + this.invo_customer_name + ' - ' + this.strPrice + "\n";
            });
            alert(msg);
        }
    });
});

$("input[name*='radioCustomer']").prop('disabled',true);
$("input[name*='intJualKanvas']").prop('disabled',true);

});</script>