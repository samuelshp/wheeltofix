<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-share"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceorder'), 'link' => site_url('invoice_order/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- BODY -->
<div class="col-xs-12">
    <form name="frmAddInvoice" id="frmAddInvoice" class="frmShop" method="post" action="<?=site_url('invoice_order', NULL, FALSE)?>">
        <div class="row">

            <!--data supplier-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="radio-group">
                            <label><input type="radio" name="radioSupplier" value="0" checked> All</label>
                            <label><input type="radio" name="radioSupplier" value="1"> Selected</label>
                        </div>
                        <div class="input-group"><input type="text" id="supplierName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>" /><label class="input-group-addon" id="loadSupplier"></label></div>
                    </div>
                </div>

                <!--data gudang-->
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?> <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="form-group"><div class="input-group"><input type="text" id="warehouseName" name="warehouseName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>" /><label class="input-group-addon" id="loadWarehouse"></label></div></div>
                    </div>
                </div> <!--data gudang-->

                <!--tanggal-->
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                    <div class="panel-body">
                        <!-- Dibuat -->
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')?></label><input type="text" id="txtExpDate" name="txtExpDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>

                    </div>
                </div> <!--tanggal-->
            </div> <!--data supplier-->

            <!--data outlet-->
            <!--<div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletdata')*/?></h3></div>
                    <div class="panel-body">
                        <h4><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectoutlet')*/?></h4>
                        <div class="form-group"><div class="input-group"><input type="text" id="outletName" class="form-control" placeholder="<?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectoutlet')*/?>" /><label class="input-group-addon" id="loadOutlet"></label></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')*/?></label><input type="text" name="outletCode" class="form-control required" disabled /></div></div>
                    </div>
                </div>
            </div>--> <!--data outlet-->

            <!--data salesman-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectsalesman')?> <a href="<?=site_url('adminpage/table/add/42', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="radio-group">
                            <label><input type="radio" name="radioSalesman" value="0" checked> Freelance</label>
                            <label><input type="radio" name="radioSalesman" value="1"> By Supplier</label>
                        </div>
                        <div class="form-group"><div class="input-group"><input type="text" id="salesmanName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectsalesman')?>" /><label class="input-group-addon" id="loadSalesman"></label></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></label><input type="text" name="salesmanCode" class="form-control required" disabled /></div></div>
                    </div>
                </div>

                <!--penjualan kanvas-->
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-share"></i> Penjualan</h3></div>
                    <div class="panel-body">
                        <!-- Dibuat -->
                        <div style="display:none;">
                            <input type="radio" name="intJualKanvas" value="0" checked> Umum<br>
                            <input type="radio" name="intJualKanvas" value="1"> Kanvas
                        </div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></label>
                                <select id="intTipeBayar" name="intTipeBayar" class="form-control">
                                    <option value="1">Tunai</option>
                                    <option value="2">Kredit</option>
                                </select>
                            </div></div>
                    </div>
                </div><!--penjualan kanvas-->

            </div> <!--data salesman-->

            <!--data customer-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?> <a href="<?=site_url('adminpage/table/add/33', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <div class="radio-group" style="display:none;">
                            <label><input type="radio" name="radioCustomer" value="0" > Non-Member</label>
                            <label><input type="radio" name="radioCustomer" value="1" checked> Member</label>
                        </div>
                        <div class="form-group"><div class="input-group"><input type="text" id="customerName" name="customerName" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectcustomer')?>"/><label class="input-group-addon" id="loadCustomer"></label></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></label><input id="customerAddress" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-city')?></label><input id="customerCity" class="form-control" disabled /></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></label><input id="customerPhone" class="form-control" disabled /></div></div>

                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></label><input id="customerOutletTypeStr" class="form-control" disabled/></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-limit')?></label><input id="customerLimit" class="form-control" disabled/></div></div>
                    </div>
                </div>
            </div> <!--data customer-->
        </div> <!--class row-->

        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceitems')?></h3></div>
            <div class="panel-body">
                <!--input barang-->
                <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                </div></div>

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                    <thead>
                    <tr>
                        <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                        <th class="canvas">Max</th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    </tbody>
                </table></div> <!--table-responsive-->

                <div class="row">

                    <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                    <div class="col-sm-2 tdDesc" id="subTotalNoTax">0</div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                    <div class="col-sm-2"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
                    <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                    <div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                    <div class="col-sm-2 tdDesc form-group"><div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                    <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                    <div class="col-sm-2 tdDesc" id="subTotalTax">0</div>
                </div>
                <p class="spacer">&nbsp;</p>
            </div> <!--panel-body-->
        </div>

        <div class="row">

            <div class="col-md-4"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                    <div class="panel-body">
                        <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
                    </div>
                </div>
            </div>

            <!-- Bonus -->
            <div class="col-md-8"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
                    <div class="panel-body">
                        <div class="form-group"><div class="input-group addNew">
                            <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                        </div></div>
                        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                                <thead>
                                <tr>
                                    <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                                    <th class="canvas"  >Max</th>
                                    <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                                </tbody>
                        </table></div>
                        <div class="form-group">
                            <label id="strBonusItem"></label>
                        </div>
                        <div class="form-group">
                            <label id="strBonusNotInclude"></label>
                        </div>
                    </div>
                </div>
            </div><!--/ Table Bonus -->

        </div>

        <div class="form-group"><button type="submit" name="smtMakeInvoice" value="Make Invoice" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

        <input type="hidden" id="WarehouseID" name="WarehouseID"/>
        <input type="hidden" id="SOID" name="SOID"/>
        <input type="hidden" id="totalItem" name="totalItem"/>
        <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
        <input type="hidden" name="supplierID" id="supplierID"/>
        <input type="hidden" id="customerID" name="customerID"/>
        <input type="hidden" name="customerOutletType"/>
        <input type="hidden" id="salesmanID" name="salesmanID"/>
        <input type="hidden" id= "outletID" name="outletID"/>
        <input type="hidden" name="outletMarketType"/>
    </form>
</div>