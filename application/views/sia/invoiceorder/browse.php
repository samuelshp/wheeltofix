<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-share"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceorder'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><?php
$strSearchAction = site_url('invoice_order/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchelement.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('invoice_order', NULL, FALSE)?>" target="_blank" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name').' / '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?> Delivery / Back</th>
			<th class="action">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrInvoice)):
			foreach($arrInvoice as $e): ?>
				<tr>
				<td><?=$e['inor_code']?></td>
				<td><?=formatPersonName('ABBR',$e['cust_name'],$e['cust_address'],$e['cust_city'],$e['cust_phone'])?></td>
				<td><?=formatDate2($e['inor_date'],'d F Y')?></td>
				<td><?=setPrice($e['inor_total'])?></td>
				<td><?=$e['inor_status']?></td>
                <td>
                    <?php
                    if(!empty($e['deli_date'])){
                        echo formatDate2($e['deli_date'],'d F Y')." / ".formatDate2($e['deba_date'],'d F Y');
                    }else{
                        echo "-";
                    }
                    ?>
                </td>
				<td>
					<?php if($e['bolAllowView']): ?><a href="<?=site_url('invoice_order/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
					<?php if($e['bolBtnPrint']): ?><a href="<?=site_url('invoice_order/view/'.$e['id'].'?print=true', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
                    <?php if($e['bolBtnPrint'] && !empty($e['cust_nppkp'])): ?><a id="printtaxY<?=$e['id']?>" href="<?=site_url('invoice_order/view/'.$e['id'].'?print2=true', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
                </td>
				</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</div>