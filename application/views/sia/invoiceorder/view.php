<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-share"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoiceorder-invoiceorder'), 'link' => site_url('invoice_order/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

if(!empty($arrOwnerInfo['strOwnerData4'])) {
    $arrPriceFormula = explode(' | ', $arrOwnerInfo['strOwnerData4']);
    foreach($arrPriceFormula as $i => $e) {
        $arrPriceFormula[$i] = explode(':', $e);
    }
}

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoiceorder-lastinvoiceorder')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- Jump To -</option><?php
            if(!empty($arrInvoiceList)) foreach($arrInvoiceList as $e):
                $strListTitle = ' title="'.$e['cust_address'].', '.$e['cust_city'].'"'; ?>
            <option value="<?=site_url('invoice_order/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['inor_code']/*formatDate2($e['inor_date'],'d/m/Y')*/?>] <?=$e['cust_name']?></option><?php
            endforeach; ?>
        </select>
</div></div>

<?php $custid = $arrInvoiceData['cust_id']; ?>

<div class="col-xs-12">
    <form name="frmChangeInvoice" id="frmChangeInvoice" method="post" class="frmShop" action="<?=site_url('invoice_order/view/'.$intInvoiceID, NULL, FALSE)?>" >
        <div class="row">

            <!--data customer-->
            <div class="col-md-6"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-customerdata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['cust_name']?><?php if($arrInvoiceData['cust_address']==''){echo " (Non Member)";}?></div>
                        <p class="spacer">&nbsp;</p>
                        <?php if($arrInvoiceData['cust_address']!=''){?>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['cust_address']?>, <?=$arrInvoiceData['cust_city']?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['cust_phone']?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-outletmarkettype')?></div>
                        <div class="col-xs-8"><?php
                                switch($arrInvoiceData['cust_outlettype']) {
                                    case 1: echo "RETAIL"; break;
                                    case 2: echo "SEMI GROSIR"; break;
                                    case 3: echo "STAR OUTLET"; break;
                                    case 4: echo "GROSIR"; break;
                                    case 5: echo "MODERN"; break;
                                }
                        ?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-limit')?> (setelah dikurangi SO)</div>
                        <div class="col-xs-8"><label id="limitCustomer"><?=$arrInvoiceData['cust_limitkredit']?></label></div>
                        <p class="spacer">&nbsp;</p>
                        <?php } ?>
                    </div>
            </div></div> <!--data customer-->

            <!--kode & tanggal buat-->
            <div class="col-md-6"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['inor_code']?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
                        <div class="col-xs-8"><?=formatDate2($arrInvoiceData['inor_date'],'d F Y')?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')?></div>
                        <div class="col-xs-8"><?=formatDate2($arrInvoiceData['inor_exp_date'],'d F Y')?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
            </div></div><!--kode & tanggal buat-->
        </div>
        <div class="row">

            <!--data salesman-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['sals_code']?></div>
                        <p class="spacer">&nbsp;</p>

                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['sals_name']?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div>
            </div> <!--data salesman-->

            <!--data gudang-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-building-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                        <div class="col-xs-8"><?=$arrInvoiceData['ware_name']?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div>
            </div> <!--data gudang-->

            <!--data pembayaran -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></h3></div>
                    <div class="panel-body">
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-payment')?></div>
                        <div class="col-xs-8"><?php if($arrInvoiceData['inor_payment']=='1'){
                                echo "Tunai";
                            }else {
                                echo "Kredit";
                            }?></div>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-xs-4">Penjualan</div>
                        <div class="col-xs-8"><?php if($arrInvoiceData['inor_jualkanvas']=='1'){
                                echo "Kanvas";
                            }else {
                                echo "Biasa";
                            }?></div>
                        <p class="spacer">&nbsp;</p>

                    </div>
                </div>
            </div> <!--data pembayaran-->


        </div> <!--row-->

        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoiceitems')?></h3></div>
            <div class="panel-body">
                <?php if($bolBtnEdit): ?>
                <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
                    </div></div>
                <?php endif; ?>

                <div class="table-responsive">
					<table class="table table-bordered table-condensed table-hover" id="selectedItems">
                        <thead>
                        <tr>
                            <?php
                            if($bolBtnEdit): ?>
                                <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php
                            endif; ?>
                            <?php if($bolBtnEdit): ?>
                            <th class="canvas">Stock</th><?php
                            endif; ?>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $i = 0;
                        $intTotalItem = 0;
                        $iditemawal='';
                        $idpurchaseawal = '';
                        if(!empty($arrInvoiceItem)):
                            foreach($arrInvoiceItem as $e):
                                $iditemawal=$iditemawal.$e['product_id'].'-';
                                $idpurchaseawal=$idpurchaseawal.$e['id'].'-';
                                $intTotalItem++; ?>
                                <tr><?php
                                if($bolBtnEdit): ?>
                                    <td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]"/></td><?php
                                endif; ?>
                                <?php if($bolBtnEdit): ?>
                                <td class="canvas"><label id="maxStrAwal<?=$e['id']?>"><?=$e['inoi_max1']." | ".$e['inoi_max2']." | ".$e['inoi_max3']?></label></td><?php
                                endif; ?>
                                <td class="qty"><?php
                                    if($bolBtnEdit): //if yes ?>
                                    <div class="form-group">
                                        <div class="col-xs-4"><input type="text" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity1']?>" class="required number form-control input-sm<?=$e['prod_conv1'] <= 1 ? ' hidden' : ''?>" id="txtItem1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit1'])?>" title="<?=formatUnitName($e['inoi_unit1'])?>" /></div>
                                        <div class="col-xs-4"><input type="text" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity2']?>" class="required number form-control input-sm<?=$e['prod_conv2'] <= 1 ? ' hidden' : ''?>" id="txtItem2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit2'])?>" title="<?=formatUnitName($e['inoi_unit2'])?>" /></div>
                                        <div class="col-xs-4"><input type="text" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity3']?>" class="required number form-control input-sm" id="txtItem3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit3'])?>" title="<?=formatUnitName($e['inoi_unit3'])?>" /></div>
                                        <input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
                                        <input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
                                        <input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
                                        <input type="hidden" name="idItem[<?=$e['id']?>]" value="<?=$e['inoi_product_id']?>" id="idItem<?=$e['id']?>" />
                                        <input type="hidden" name="idBrand[<?=$e['id']?>]" value="<?=$e['prod_brand_id']?>" id="idBrand<?=$e['id']?>"/>
                                        <input type="hidden" id="max1Item<?=$e['id']?>"  value="<?=$e['inoi_max1']?>" />
                                        <input type="hidden" id="max2Item<?=$e['id']?>"  value="<?=$e['inoi_max2']?>" />
                                        <input type="hidden" id="max3Item<?=$e['id']?>"  value="<?=$e['inoi_max3']?>" />
                                        <input type="hidden" id="Hpp<?=$e['id']?>"  value="<?=$e['prod_hpp']?>" />
                                        <input type="hidden" id="Harga<?=$e['id']?>"  value="<?=$e['inoi_price']?>" />
                                    </div><?php

                                    else: echo
                                        $e['inoi_quantity1'].' '.formatUnitName($e['inoi_unit1']).' + '.
                                        $e['inoi_quantity2'].' '.formatUnitName($e['inoi_unit2']).' + '.
                                        $e['inoi_quantity3'].' '.formatUnitName($e['inoi_unit3']);?>
                                        <input type="hidden" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity1']?>" class="required number form-control input-sm" id="txtItem1Qty<?=$e['id']?>"/>
                                        <input type="hidden" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity2']?>" class="required number form-control input-sm" id="txtItem2Qty<?=$e['id']?>"/>
                                        <input type="hidden" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity3']?>" class="required number form-control input-sm" id="txtItem3Qty<?=$e['id']?>"/>
                                        <input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
                                        <input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
                                        <input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['inoi_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
                                        <input type="hidden" name="idItem[<?=$e['id']?>]" value="<?=$e['inoi_product_id']?>" id="idItem<?=$e['id']?>" />
                                        <input type="hidden" name="idBrand[<?=$e['id']?>]" value="<?=$e['prod_brand_id']?>" id="idBrand<?=$e['id']?>"/>
                                        <input type="hidden" id="max1Item<?=$e['id']?>"  value="<?=$e['inoi_max1']?>" />
                                        <input type="hidden" id="max2Item<?=$e['id']?>"  value="<?=$e['inoi_max2']?>" />
                                        <input type="hidden" id="max3Item<?=$e['id']?>"  value="<?=$e['inoi_max3']?>" />
                                        <input type="hidden" id="Hpp<?=$e['id']?>"  value="<?=$e['prod_hpp']?>" />
                                        <input type="hidden" id="Harga<?=$e['id']?>"  value="<?=$e['inoi_price']?>" />
                                    <?php
                                    endif; ?>
                                        <input type="hidden" name="idProiID[<?=$e['id']?>]" value="<?=$e['id']?>" />
                                </td>
                                <td><b><label id="nmeItem<?=$e['id']?>">(<?=$e['prod_code']?>) <?=$e['inoi_description']?></label></b></td>
                                <td class="pr"><?php
                                    if($bolBtnEdit): ?>
                                        <div class="input-group"><label class="input-group-addon"><?=str_replace(' 0','',setPrice('','USED'))?></label><input type="text" name="txtItemPrice[<?=$e['id']?>]" class="required currency form-control input-sm" id="txtItemPrice<?=$e['id']?>" value="<?=$e['inoi_price']?>" list="dlPriceList<?=$e['id']?>" /></div><?php

                                    else:
                                        echo setPrice($e['inoi_price'],'USED');
                                    endif;
                                    if(!empty($arrPriceFormula)): ?>
                                    <datalist id="dlPriceList<?=$e['id']?>"><?php
                                        foreach($arrPriceFormula as $e2): 
                                            $strFormula = str_replace('{{p}}', 'floatval('.$e['inoi_price'].')', $e2[1]);
                                            eval('$strValue = '.$strFormula.';'); ?>
                                        <option value="<?=$strValue?>"><?=$e2[0]?></option><?php
                                        endforeach; ?>
                                    </datalist><?php
                                    endif; ?>
                                </td>
                                <td class="disc"><?php
                                    if($bolBtnEdit): ?>
                                        <div class="form-group">
                                        <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem1Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem1Disc<?=$e['id']?>" value="<?=$e['inoi_discount1']?>" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>
                                        <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem2Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem2Disc<?=$e['id']?>" value="<?=$e['inoi_discount2']?>" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>
                                        <div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem3Disc[<?=$e['id']?>]" class="required number form-control input-sm" id="txtItem3Disc<?=$e['id']?>" value="<?=$e['inoi_discount3']?>" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>
                                        </div><?php

                                    else:
                                        echo $e['inoi_discount1']." % + ".$e['inoi_discount2']." % + ".$e['inoi_discount3']." %";
                                    endif; ?>
                                </td>
                                <td class="subTotal" id="subTotal<?=$e['id']?>"><?=$e['inoi_subtotal']?></td>
                                </tr><?php

                                $i++;
                            endforeach;

                            $iditemawal = $iditemawal.'0';
                            $idpurchaseawal = $idpurchaseawal.'0';
                        else: ?>
                            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                        endif; ?>

                        </tbody>
                    </table>
                </div> <!--table-responsive-->

                <div class="row">

                    <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                    <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?=$intInvoiceTotal?></div>
                </div>

                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                    <div class="col-sm-2 form-group"><?php
                        if($bolBtnEdit): ?>
                            <div class="input-group"><input type="text" name="dsc4" value="<?=$arrInvoiceData['inor_discount']?>" id="dsc4" class="required currency form-control" /><!--<label class="input-group-addon">%</label>--></div><?php

                        else:
                            echo $arrInvoiceData['inor_discount'];
                        endif; ?>
                    </div>
                    <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                    <div class="col-sm-2 tdDesc currency" id="subTotalWithDisc"><?=$arrInvoiceData['subtotal_discounted']?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                    <div class="col-sm-2 tdDesc form-group">
                        <?php
                        if($bolBtnEdit): ?>
                            <div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="<?=$arrInvoiceData['inor_tax']?>" /><label class="input-group-addon">%</label></div><?php

                        else:
                            echo $arrInvoiceData['inor_tax'].' %';
                        endif; ?>
                    </div>
                    <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                    <div class="col-sm-2 tdDesc currency" id="subTotalTax"><?=$intInvoiceGrandTotal?></div>
                </div>
                <p class="spacer">&nbsp;</p>
            </div> <!--panel-body-->
        </div>

        <div class="row">
            <!--keterangan-->
            <div class="col-md-4"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                    <div class="panel-body"><?php
                        if($bolBtnEdit): ?>
                            <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrInvoiceData['inor_description']?></textarea></div><?php
                        else: ?>
                            <input type="hidden" name="txaDescription" value="<?=$arrInvoiceData['inor_description']?>" />
							<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrInvoiceData['inor_description']?></div><?php
                        endif; ?>
                        <div class="form-group">
                            <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')?></label>
                            <input type="text" name="txtProgress" value="<?=$arrInvoiceData['inor_progress']?>" maxlength="64" class="form-control" />
                        </div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div></div> <!--keterangan-->

            <div class="col-md-8"><div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
                    <div class="panel-body"><?php
                        if($bolBtnEdit): ?>
                            <div class="form-group"><div class="input-group addNew">
                                    <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                                </div></div><?php
                        endif; ?>

                        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                                <thead>
                                <div class="form-group">
                                    <label id="strBonusItem"></label>
                                </div>
                                <tr><?php
                                    if($bolBtnEdit): ?>
                                        <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                                        <th class="cb canvas">Stock</th><?php
                                    endif; ?>
                                    <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
                                </tr>
                                </thead>
                                <tbody><?php
                                    $i = 0;
                                    $intTotalBonusItem = 0;
                                    $iditembonusawal='';
                                    $idpurchasebonusawal = '';
                                    if(!empty($arrInvoiceBonusItem)):
                                        foreach($arrInvoiceBonusItem as $e):
                                            $iditembonusawal=$iditembonusawal.$e['product_id'].'-';
                                            $idpurchasebonusawal=$idpurchasebonusawal.$e['id'].'-';
                                            $intTotalBonusItem++; ?>
                                            <tr><?php
                                            if($bolBtnEdit): ?>
                                                <td class="cb"><input type="checkbox" name="cbBonusAwal[<?=$e['id']?>]"/></td>
                                                <td class="cb canvas"><label id="maxItemBonusAwal<?=$e['id']?>"><?=$e['inoi_max1']." | ".$e['inoi_max2']." | ".$e['inoi_max3']?></label></td><?php
                                            endif; ?>
                                            <td class="qty"><?php
                                                if($bolBtnEdit): //if yes ?>
                                                <div class="form-group">
                                                    <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus1Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity1']?>" class="required number form-control input-sm<?=$e['prod_conv1'] <= 1 ? ' hidden' : ''?>" id="txtItemBonus1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit1'])?>" title="<?=formatUnitName($e['inoi_unit1'])?>" /></div></div>
                                                    <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus2Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity2']?>" class="required number form-control input-sm<?=$e['prod_conv2'] <= 1 ? ' hidden' : ''?>" id="txtItemBonus2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit2'])?>" title="<?=formatUnitName($e['inoi_unit2'])?>" /></div></div>
                                                    <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus3Qty[<?=$e['id']?>]" value="<?=$e['inoi_quantity3']?>" class="required number form-control input-sm" id="txtItemBonus3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['inoi_unit3'])?>" title="<?=formatUnitName($e['inoi_unit3'])?>" /></div></div>
                                                    <input type="hidden" name="idItemBonus[<?=$e['id']?>]" value="<?=$e['product_id']?>" id="idItemBonus<?=$e['id']?>" />
                                                    <input type="hidden" name="idBonusBrand[<?=$e['id']?>]" value="<?=$e['prod_brand_id']?>" id="idBonusBrand<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_conv1']?>" id="conv1UnitBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_conv2']?>" id="conv2UnitBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_conv3']?>" id="conv3UnitBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_max1']?>" id="max1ItemBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_max2']?>" id="max2ItemBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="<?=$e['inoi_max3']?>" id="max3ItemBonus<?=$e['id']?>"/>
                                                    <input type="hidden" value="-1" id="SameBonusAwal<?=$e['id']?>"/>
                                                    <input type="hidden" value="-1" id="IsAwalSameBonusAwal<?=$e['id']?>"/>
                                                </div><?php

                                                else:
                                                    echo $e['inoi_quantity1'].' '.formatUnitName($e['inoi_unit1']).' + '.$e['inoi_quantity2'].' '.formatUnitName($e['inoi_unit2']).' + '.$e['inoi_quantity3'].' '.formatUnitName($e['inoi_unit3']);
                                                endif; ?>
                                                    <input type="hidden" value="<?=$e['id']?>" name="idProiIDBonus[<?=$e['id']?>]" />
                                            </td>
                                            <td><b>(<?=$e['prod_code']?>) <?=$e['strName']?></b></td>
                                            </tr><?php
                                            $i++;
                                        endforeach;
                                        $iditembonusawal=$iditembonusawal.'0';
                                        $idpurchasebonusawal = $idpurchasebonusawal.'0';
                                    else: ?>
                                        <?php
                                        if($bolBtnEdit): ?>
                                            <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                                        else: ?>
                                            <tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                                            <?php
                                        endif; ?>
                                    <?php
                                    endif; ?>
                                </tbody>
                            </table></div>

                        <div class="form-group">
                            <label id="strBonusNotInclude"></label>
                            <br><br>
                            <label id="strBonusNotIncludeTaken">
                                Bonus yang dipisahkan dari nota (yang telah diaplikasikan) : <br>
                                <?php
                                if(!empty($arrPotongan)):
                                    foreach($arrPotongan as $e):
                                        echo "> ".$e['poto_item_qty']." ".$e['poto_name']." ,Potongan / Item => ".$e['poto_nominal']."<br>";
                                    endforeach;
                                endif;
                                ?>
                            </label>
                        </div>

                    </div><!--/ Table Bonus -->
                </div></div>
        </div>

<?php 
    $rawStatus = $arrInvoiceData['inor_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>
    <?php if($bolBtnPrint && !empty($arrInvoiceData['cust_nppkp'])): ?><button id="printTaxY<?=$intInvoiceID?>" type="submit" name="subPrintTax" value="Print Tax" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button><?php endif; ?>


    <input type="hidden" id="idPurchaseAwal" name="idPurchaseAwal" value="<?=$idpurchaseawal?>"/>
    <input type="hidden" id="idPurchaseBonusAwal" name="idPurchaseBonusAwal" value="<?=$idpurchasebonusawal?>"/>
    <input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
    <input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
    <input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
    <input type="hidden" id="totalItemBonusAwal" name="totalItemBonusAwal" value="<?=$intTotalBonusItem?>"/>
    <input type="hidden" id="txtDate" name="txtDate" value="<?=$arrInvoiceData['date']?>"/>
    <input type="hidden" id="totalItem" name="totalItem" value="0"/>
    <input type="hidden" id="totalItemBonus" name="totalItemBonus" value="0"/>
    <input type="hidden" id="CustomerID" name="CustomerID" value="<?=$arrInvoiceData['inor_customer_id']?>"/>
    <input type="hidden" id="SuppID" name="SuppID" value="<?=$arrInvoiceData['inor_supplier_id']?>"/>
    <input type="hidden" id="CustOutletType" name="CustOutletType" value="<?if($arrInvoiceData['cust_outlettype']!=''){echo $arrInvoiceData['cust_outlettype'];}else{echo'5';}?>"/>
    <input type="hidden" id="MarketOutletType" name="MarketOutletType" value="<?if($arrInvoiceData['cust_markettype']!=''){echo $arrInvoiceData['cust_markettype'];}else{echo'1';}?>"/>
    <input type="hidden" id="Internal" name="Internal" value="<?=$arrInvoiceData['cust_internal']?>"/>
    <input type="hidden" id="WarehouseID" name="WarehouseID" value="<?=$arrInvoiceData['ware_id']?>"/>
    <input type="hidden" id="TambahanID"  value="<?=$strTambahanId?>"/>
    <input type="hidden" id="TambahanQty"  value="<?=$strTambahanQty?>"/>
    <input type="hidden" id="change" name="change" value="0"/>
    <input type="hidden" id="canvas" value="<?=$arrInvoiceData['inor_jualkanvas']?>"/>

    </form> <!--form-->
</div>