<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
<?php
if(!empty($arrOwnerInfo['strOwnerData4'])) {
    $arrPriceFormula = explode(' | ', $arrOwnerInfo['strOwnerData4']);
    foreach($arrPriceFormula as $i => $e) {
        $arrPriceFormula[$i] = explode(':', $e);
    }
    echo PHP_EOL."var arrPriceFormula = ".json_encode($arrPriceFormula).";".PHP_EOL;
} ?>  
if(parseFloat($("#limitCustomer").text()) < 0) {
    $("#limitCustomer").text(0);
}
$("#limitCustomer").autoNumeric('init', autoNumericOptionsRupiah);

function getbonus(){
    if($("#canvas").val()=="1"){
        $(".canvas").hide();
    }
	selectedItemBonusBefore=[];
	numberitembonus=0;
	if(first!=1){
		numberitembonusawal=0;}
	var product='';
	var qty='';
	for(var i=0;i<=totalitemawal;i++)
	{
		if($("#nmeItem"+selectedIDPurchase[i]).text()!='')
		{
			var temp=0;
			product=product+$("#idBrand"+selectedIDPurchase[i]).val()+"-";
			if($("#txtItem1Conv"+selectedIDPurchase[i]).val()>0 && $("#txtItem1Qty"+selectedIDPurchase[i]).val()>0){
				temp=parseInt(temp)+parseInt($("#txtItem1Conv"+selectedIDPurchase[i]).val())*parseInt($("#txtItem1Qty"+selectedIDPurchase[i]).val());
			}
			if($("#txtItem2Conv"+selectedIDPurchase[i]).val()>0 && $("#txtItem2Qty"+selectedIDPurchase[i]).val()){
				temp=parseInt(temp)+parseInt($("#txtItem2Conv"+selectedIDPurchase[i]).val())*parseInt($("#txtItem2Qty"+selectedIDPurchase[i]).val());
			}
			if($("#txtItem3Conv"+selectedIDPurchase[i]).val()>0 && $("#txtItem3Qty"+selectedIDPurchase[i]).val()){
				temp=parseInt(temp)+parseInt($("#txtItem3Conv"+selectedIDPurchase[i]).val())*parseInt($("#txtItem3Qty"+selectedIDPurchase[i]).val());
			}
			qty=qty+temp.toString()+"-";
		}
	}
	for(var i=0;i<=totalitem;i++)
	{
		if($("#nmeItemX"+i).text()!='')
		{
			var temp=0;
			product=product+$("#idBrandX"+i).val()+"-";
			if($("#conv1UnitX"+i).val()>0 && $("#qty1PriceEffectX"+i).val()>0){
				temp=parseInt(temp)+parseInt($("#conv1UnitX"+i).val())*parseInt($("#qty1PriceEffectX"+i).val());
			}
			if($("#conv2UnitX"+i).val()>0 && $("#qty2PriceEffectX"+i).val()){
				temp=parseInt(temp)+parseInt($("#conv2UnitX"+i).val())*parseInt($("#qty2PriceEffectX"+i).val());
			}
			if($("#conv3UnitX"+i).val()>0 && $("#qty3PriceEffectX"+i).val()){
				temp=parseInt(temp)+parseInt($("#conv3UnitX"+i).val())*parseInt($("#qty3PriceEffectX"+i).val());
			}
			qty=qty+temp.toString()+"-";
		}
	}

	product=product+"0";
	qty=qty+"0";
	var date=$('input[name="txtDate"]').val();
	date=date.replace(/\//g, '-');
	strAllowedBonusID='';
    $("#strBonusItem").html('');
    $("#strBonusItem").append(
        'Bonus yang dapat diambil (dari Database) : <br>' +
        '> Tidak Ada <br>'
    );
    $("#strBonusNotInclude").html('');
    if($("#change").val()=='1'){
        $("#strBonusNotInclude").append(
            'Bonus yang dipisahkan dari nota (dari Database & akan diambil) : <br>' +
            '> Tidak Ada <br>'
        );
    }else{
        $("#strBonusNotInclude").append(
            'Bonus yang dipisahkan dari nota (dari Database) : <br>' +
            '> Tidak Ada <br>'
        );
    }
	$.ajax({
		url: "<?=site_url('invoice_ajax/getBonusByPromo', NULL, FALSE)?>/" + product + "/0/" + qty + "/" + date,
		success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var counterbonus1 =0;
            var counterbonus2 =0;
            allowedBonusID=[];
            allowedBonusQty=[];
            notIncludedBonusBrand=[];
            notIncludedBonusName=[];
            notIncludedBonusDisc=[];
            notIncludedBonusQty=[];
            $.map(xml.find('Bonus').find('item'),function(val,i){
                var intID = $(val).find('id').text();
                var intQty=$(val).find('qty').text();
                var intDsc=$(val).find('discount').text();
                var intInclude=$(val).find('include').text();
                var strName=$(val).find('name').text();
                if((parseInt(intQty)>0) && parseInt(intInclude)==1){
                    if(counterbonus1==0){
                        $("#strBonusItem").html('');
                        $("#strBonusItem").append(
                            'Bonus yang dapat diambil (dari Database) : <br>' +
                                '> Tidak Ada <br>'
                        );
                        counterbonus1++;
                    }
                    $("#strBonusItem").append(
                        '> '+strName
                    );
                    if(parseInt(intQty)>0){
                        $("#strBonusItem").append(
                            ' (Bonus: ' + intQty + ')'
                        );
                    }
                    $("#strBonusItem").append(
                        '<br>'
                    );
                    allowedBonusID.push(intID);
                    strAllowedBonusID=strAllowedBonusID+intID+'-';
                    allowedBonusQty.push(intQty);
                } else if((parseFloat(intDsc)>0) && parseInt(intInclude)>1){
                    if(counterbonus2==0 ){
                        $("#strBonusNotInclude").html('');
                        if($("#change").val()=='1'){
                            $("#strBonusNotInclude").append(
                                'Bonus yang dipisahkan dari nota (dari Database & akan diambil) : <br>'
                            );
                        }else{
                            $("#strBonusNotInclude").append(
                                'Bonus yang dipisahkan dari nota (dari Database) : <br>'
                            );
                        }
                    }
                    counterbonus2++;
                    notIncludedBonusBrand.push(intID);
                    notIncludedBonusName.push(strName);
                    notIncludedBonusDisc.push(intDsc);
                    var temp=0
                    for(var iii=0;iii<=totalitem;iii++)//point1
                    {
                        if($("#idBrandX"+iii).val()==intID)
                        {
                            if($("#conv1UnitX"+iii).val()>0 && $("#qty1PriceEffectX"+iii).val()>0){
                                temp=parseInt(temp)+parseInt($("#conv1UnitX"+iii).val())*parseInt($("#qty1PriceEffectX"+iii).val());
                            }
                            if($("#conv2UnitX"+iii).val()>0 && $("#qty2PriceEffectX"+iii).val()){
                                temp=parseInt(temp)+parseInt($("#conv2UnitX"+iii).val())*parseInt($("#qty2PriceEffectX"+iii).val());
                            }
                            if($("#conv3UnitX"+iii).val()>0 && $("#qty3PriceEffectX"+iii).val()){
                                temp=parseInt(temp)+parseInt($("#conv3UnitX"+iii).val())*parseInt($("#qty3PriceEffectX"+iii).val());
                            }
                        }
                    }
                    for(var iii=0;iii<=totalitemawal;iii++)//point1
                    {
                        if($("#idBrand"+selectedIDPurchase[iii]).val()==intID)
                        {
                            if($("#txtItem1Conv"+selectedIDPurchase[iii]).val()>0 && $("#txtItem1Qty"+selectedIDPurchase[iii]).val()>0){
                                temp=parseInt(temp)+parseInt($("#txtItem1Conv"+selectedIDPurchase[iii]).val())*parseInt($("#txtItem1Qty"+selectedIDPurchase[iii]).val());
                            }
                            if($("#txtItem2Conv"+selectedIDPurchase[iii]).val()>0 && $("#txtItem2Qty"+selectedIDPurchase[iii]).val()){
                                temp=parseInt(temp)+parseInt($("#txtItem2Conv"+selectedIDPurchase[iii]).val())*parseInt($("#txtItem2Qty"+selectedIDPurchase[iii]).val());
                            }
                            if($("#txtItem3Conv"+selectedIDPurchase[iii]).val()>0 && $("#txtItem3Qty"+selectedIDPurchase[iii]).val()){
                                temp=parseInt(temp)+parseInt($("#txtItem3Conv"+selectedIDPurchase[iii]).val())*parseInt($("#txtItem3Qty"+selectedIDPurchase[iii]).val());
                            }
                        }
                    }
                    notIncludedBonusQty.push(temp);
                    $("#strBonusNotInclude").append(
                        '> '+temp+" "+strName
                    );
                    if(parseInt(intDsc)>0){
                        $("#strBonusNotInclude").append(
                            ' (Potongan / Item: ' + intDsc + ')'
                        );
                    }
                    $("#strBonusNotInclude").append(
                        '<br>'
                    );
                }
            });
            strAllowedBonusID=strAllowedBonusID+'0';
            if(first==1){
                for(var ii=0;ii<numberitembonusawal;ii++){
                    var indexof=0;
                    for(var k=0;k<allowedBonusID.length;k++){
                        if(allowedBonusID[k]==$("#idBonusBrand"+selectedIDPurchaseBonus[ii]).val()){
                            indexof=k;
                            break;
                        }
                    }
                    for(var k=1;k<=3;k++){
                        var konversi=$("#conv"+k+"UnitBonus"+selectedIDPurchaseBonus[ii]).val();
                        if($("#txtItemBonus"+k+"Qty"+selectedIDPurchaseBonus[ii]).val()!='' && $("#txtItemBonus"+k+"Qty"+selectedIDPurchaseBonus[ii]).val()>0){
                            allowedBonusQty[indexof]=allowedBonusQty[indexof]-($("#txtItemBonus"+k+"Qty"+selectedIDPurchaseBonus[ii]).val()*konversi);
                        }
                    }
                }

                for(var ii=0;ii<numberitemawal;ii++){
                    for(var jj=0;jj<numberitembonusawal;jj++){
                        if($("#idItem"+selectedIDPurchase[ii]).val()==$("#idItemBonus"+selectedIDPurchaseBonus[jj]).val()){
                            $("#SameBonusAwal"+selectedIDPurchaseBonus[jj]).val(selectedIDPurchase[ii]);
                            $("#IsAwalSameBonusAwal"+selectedIDPurchaseBonus[jj]).val(1);
                        }
                    }
                }
                first=0;
            }
        }
	});
}
var internal=$('#Internal').val();
var selectedItemBonusBefore=$('#idItemBonusAwal').val().split("-");
var selectedItemBefore=$('#idItemAwal').val().split("-");
var selectedIDPurchase=$("#idPurchaseAwal").val().split("-");
var selectedIDPurchaseBonus=$("#idPurchaseBonusAwal").val().split("-");
var arrTambahanID=$("#TambahanID").val().split("-");
var arrTambahanQty=$("#TambahanQty").val().split("-");
var numberitembonusawal=selectedItemBonusBefore.length-1;
var numberitemawal=selectedItemBefore.length-1;
var totalitembonus=0;
var totalitemawal=numberitemawal;
var numberitembonus=0;
var totalitem=0;
var numberitem=0;
var allowedBonusID=[];
var allowedBonusQty=[];
var strAllowedBonusID='';
var notIncludedBonusBrand=[];
var notIncludedBonusName=[];
var notIncludedBonusDisc=[];
var notIncludedBonusQty=[];
numberitemawal=$("#totalItemAwal").val();
numberitembonusawal=$("#totalItemBonusAwal").val();
var first=1;
getbonus();

$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);

$("select[name='selJumpTo']").change(function() {
	if($(this).find("option:selected").val() != '') window.location.href = $(this).find("option:selected").val();
});

$('button[id*="printTax"]').click(function(){
    var text = prompt("Tanggal Nota:", "");
    var at=this.id.substring(this.id.indexOf('Y')+1,this.id.length);
    var link=$("#frmChangeInvoice").attr("action");
    $.ajax({
        url: "<?=site_url('invoice_ajax/checkPrintLog', NULL, FALSE)?>/" + at,
        async:false,
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var fakturnumber=xml.find('Kode').find('prlo_invoice_number').text();
            if(fakturnumber!=''){
                link=link+"?date="+text+"&kode="+fakturnumber+"&first=0";
            }else{
                var kode = prompt("Kode Faktur Pajak:", "");
                link=link+"?date="+text+"&kode="+kode+"&first=1";
            }
        }
    });
    $("#frmChangeInvoice").attr("action",link);
});
$("#frmChangeInvoice").submit(function() {
    if($("CustomerID").val()>0){
        if(parseFloat( $("#limitCustomer").autoNumeric('get'))<parseFloat( $("#subTotalTax").autoNumeric('get'))){
            alert("jumlah pembelian melebihi limit yang dimiliki oleh pelanggan");
            // return false;
        }
    }
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	$('.subTotal').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
    //tambah sini
    for(var i=0;i<notIncludedBonusBrand.length;i++){
        $("#strBonusNotInclude").append('' +
            '<input type="hidden" name="potonganBrand['+i+']" value="'+notIncludedBonusBrand[i]+'"/>' +
            '<input type="hidden" name="potonganName['+i+']" value="'+notIncludedBonusName[i]+'"/>' +
            '<input type="hidden" name="potonganDisc['+i+']" value="'+notIncludedBonusDisc[i]+'"/>' +
            '<input type="hidden" name="potonganQty['+i+']" value="'+notIncludedBonusQty[i]+'"/>');
    }
	return true;
});

/*$.ajax({
	url: "<?=site_url('inventory_ajax/getProductStockNew', NULL, FALSE)?>/" + $('#idItemAwal').val()+"/" + $('#idPurchaseAwal').val()+"/" + $("#WarehouseID").val(),
	success: function(data){
		var xmlDoc = $.parseXML(data);
		var xml = $(xmlDoc);
		$arrSelectedPO = xml;
		$.map(xml.find('Stock').find('item'),function(val,i){
			var max=$(val).find('stock').text();
			var id=$(val).find('id').text();
            for(var zz=1;zz<=3;zz++){
                if($("#txtItem"+zz+"Conv"+id).val()!=''){
                    var hasil=Math.floor(parseInt(max)/parseInt($("#txtItem"+zz+"Conv"+id).val()));
                    if(zz==1){
                        var conv = xml.find('prod_conv1').text();
                        if(parseInt(conv) > 1) {
                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
                            $("#maxStrAwal"+id).text(hasil);
                            $("#max1Item"+id).val(hasil);
                        } else {
                            $("#maxStrAwal"+id).text('0');
                            $("#max1Item"+id).val('0');
                        }
                    }
                    if(zz==2){
                        conv = xml.find('prod_conv2').text();
                        if(parseInt(conv) > 1) {
                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
                            $("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
                            $("#max2Item"+id).val(hasil);
                        } else {
                            $("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | 0");
                            $("#max2Item"+id).val('0');
                        }
                    }
                    if(zz==3){
                        $("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
                        $("#max3Item"+id).val(hasil);
                    }
                }
            }
		});
	}
});*/

$("#dsc4").change(function(){
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("#txtTax").change(function(){
	var totalNoTax=$("#subTotalWithDisc").autoNumeric('get');
	var totalTax=parseInt(totalNoTax)+parseInt(totalNoTax*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});



$( "input[name*='cbDeleteAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitemawal--;
	if((numberitemawal+numberitem)=='0'){
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItem"+at).val();
	});
	numberitembonus=0;
	numberitembonusawal=0;
	$("#selectedItemsBonus tbody").html('');
	$("#selectedItemsBonus tbody").append(
		'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
	);
	$(this).closest("tr").remove();
    $("#change").val(1);

	getbonus();
});

$( "input[name*='cbBonusAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var indexof=0;
	for(var i=0;i<allowedBonusID.length;i++){
		if(allowedBonusID[i]==$("#idBonusBrand"+at).val()){
			indexof=i;
			break;
		}
	}
	for(var i=1;i<=3;i++){
		var konversi=$("#conv"+i+"UnitBonus"+at).val();
		if($("#txtItemBonus"+i+"Qty"+at).val()!='' && $("#txtItemBonus"+i+"Qty"+at).val()>0){
			allowedBonusQty[indexof]=allowedBonusQty[indexof]+($("#txtItemBonus"+i+"Qty"+at).val()*konversi);
		}
	}
	numberitembonusawal--;
	if((numberitembonusawal+numberitembonus)=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonus"+at).val();
	});
	$(this).closest("tr").remove();

});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDeleteX']",function(){
	var at=this.name.substring(this.name.indexOf("X")+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitem--;
	if((numberitemawal+numberitem)=='0'){
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	numberitembonus=0;
	numberitembonusawal=0;
	$("#selectedItemsBonus tbody").html('');
	$("#selectedItemsBonus tbody").append(
		'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
	);
	$(this).closest("tr").remove();

	getbonus();
});

$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoCompletePerSupplierID', NULL, FALSE)?>/" + $("#SuppID").val() + "/" + $('#CustOutletType').val() + "/" + $('#MarketOutletType').val() + "/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedItem = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBefore) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedItem = $.grep($arrSelectedItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var discount1 = $(selectedItem).find('skds_discount1').text();
		var discount2 = $(selectedItem).find('skds_discount2').text();
		var discount3 = $(selectedItem).find('skds_discount3').text();

        if(discount1=='') discount1=0;
        if(discount2=='') discount2=0;
        if(discount3=='') discount3=0;

		var i=totalitem;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedItem).find('prod_conv1').text();
        var prod_conv2 = $(selectedItem).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberitem+numberitemawal=='0'){

			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>' +
				'<td  id="maxStrX'+i+'" class="canvas"></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" id="qty1PriceEffectX'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" id="qty2PriceEffectX'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" id="qty3PriceEffectX'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0" list="dlPriceList'+i+'"/></div></td>'+
				'<td class="disc"><div class="form-group">'+
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="'+discount1+'" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="'+discount1+'" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="'+discount1+'" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
				'</div></td>'+
				'<td class="subTotal" id="subTotalX'+i+'">0</td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="idBrandX'+i+'" name="idBrand'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" value="0">' +
				'<input type="hidden" id="conv2UnitX'+i+'" value="0">' +
				'<input type="hidden" id="conv3UnitX'+i+'" value="0">'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
				'<input type="hidden" id="HppX'+i+'">'+
                '<input type="hidden" id="HargaX'+i+'" >'+
				'</tr>'
		);
        var prodcode = $(selectedItem).find('prod_code').text();
		var prodtitle = $(selectedItem).find('prod_title').text();
		var probtitle = $(selectedItem).find('prob_title').text();
		var proctitle = $(selectedItem).find('proc_title').text();
		var strName=$(selectedItem).find('strName').text();
		var id=$(selectedItem).find('id').text();
		selectedItemBefore.push(id);
		var max=0;
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
        var proddesc = $(selectedItem).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#nmeItemX"+i).append('<i class="fa fa-question-circle text-success pull-right"></i>');
		var hpp=$(selectedItem).find('prod_hpp').text();
		if(isNaN(internal) || internal==0){
			if ($('input[name="MarketOutletType"]').val() == '1') {
				var tempprice=$(selectedItem).find('prod_pricemodern').text();
                /*if(parseInt(tempprice) < parseInt(hpp)){
                     $("#prcItemX"+i).val(hpp);
                     $("#HargaX"+i).val(hpp);
                 }else{*/
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                /*}*/
			} else if ($('input[name="MarketOutletType"]').val() == '2') {
				var tempprice=$(selectedItem).find('prod_price').text();
                /*if(parseInt(tempprice) < parseInt(hpp)){
                     $("#prcItemX"+i).val(hpp);
                     $("#HargaX"+i).val(hpp);
                 }else{*/
                    $("#prcItemX"+i).val(tempprice);
                    $("#HargaX"+i).val(tempprice);
                /*}*/
			};
		}else{
			$("#prcItemX"+i).val(hpp);
            $("#HargaX"+i).val(hpp);
		}
		$("#HppX"+i).val(hpp);
		$("#idItemX"+i).val($(selectedItem).find('id').text());
		$("#idBrandX"+i).val($(selectedItem).find('id_brand').text());
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
        if(typeof arrPriceFormula != 'undefined') {
            $('#trItem'+i).append('<datalist id="dlPriceList'+i+'"></datalist>');
            $.each(arrPriceFormula, function(index, v) {
                var formula = v[1].replace(/\{\{p\}\}/g, 'parseFloat(' + tempprice + ')');
                $('#dlPriceList'+i).append('<option value="' + eval(formula) + '">'+ v[0] +'</option>');
            });
        }
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
        $("#change").val(1);
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedSO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					/*var intID = $(val).find('id').text();
					 var strUnit=$(val).find('unit_title').text();
					 $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>');*/
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#sel"+zz+"IteMunitX"+i).text(unitStr[zz-1]);
				}

				$.ajax({
					url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
					success: function(data){
						var xmlDoc = $.parseXML(data);
						var xml = $(xmlDoc);
						$arrSelectedPO = xml;
						max =xml.find('Stock').text();
						for(var rr=0;rr<arrTambahanID.length;rr++){
							if(arrTambahanID[rr]==id){
								max=parseInt(max)+parseInt( arrTambahanQty[rr]);
							}
						}
                        for(var rr = 1; rr <= 3; rr++) {
                            if($("#conv"+rr+"UnitX"+i).val() != '') {
                                var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
                                if(rr == 1) {
                                    var conv = xml.find('prod_conv1').text();
                                    if(parseInt(conv) > 1) {
                                        max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                        $("#maxStrX"+i).text(hasil);
                                        $("#max1ItemX"+i).val(hasil);
                                    } else {
                                        $("#maxStrX"+i).text('0');
                                        $("#max1ItemX"+i).val('0');
                                    }
                                } else if(rr == 2) {
                                    conv = xml.find('prod_conv2').text();
                                    if(parseInt(conv) > 1) {
                                        max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
                                        $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
                                        $("#max2ItemX"+i).val(hasil);
                                    } else {
                                        $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | 0");
                                        $("#max2ItemX"+i).val('0');
                                    }
                                } else if(rr == 3) {
                                    $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
                                    $("#max3ItemX"+i).val(hasil);
                                }
                            }
                        }
                        if($("#canvas").val()=="1"){
                            $(".canvas").hide();
                        }else{
                            $(".canvas").show();
                        }
					}
				});
			}
		});
		totalitem++;
		numberitem++;
		$("#totalItem").val(totalitem);

        $('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

var valuebefore=0;
var oldprice=0;
var olddsc1=0;
var olddsc2=0;
var olddsc3=0;
var oldqty1=0;
var oldqty2=0;
var oldqty3=0;
$("#selectedItems").on("focus","input[type='text'][name*='txtItem']",function(){
	valuebefore=$(this).val();
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    oldprice=$("#prcItemX"+at).autoNumeric('get');
    olddsc1=$("#dsc1ItemX"+at).val();
    olddsc2=$("#dsc2ItemX"+at).val();
    olddsc3=$("#dsc3ItemX"+at).val();
    oldqty1=$("#qty1PriceEffectX"+at).val();
    oldqty2=$("#qty2PriceEffectX"+at).val();
    oldqty3=$("#qty3PriceEffectX"+at).val();
});

$("#selectedItems").on("change","input[type='text'][name*='txtItem']",function(){
	var at=this.id.substring(this.id.indexOf('y')+1,this.id.length);
	var max=0;
	var now=0;
	var temp='';
    if($("#canvas").val()=="1"){
        max=1;
        now=0;
    }
    else{
        for(var zz=1;zz<=3;zz++){
            if($("#txtItem"+zz+"Qty"+at).val()<0){
                $("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
            }
            if($("#max"+zz+"Item"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
                max+=$("#txtItem"+zz+"Conv"+at).val()*$("#max"+zz+"Item"+at).val();
            }

            if($("#txtItem"+zz+"Qty"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
                now+=$("#txtItem"+zz+"Qty"+at).val()*$("#txtItem"+zz+"Conv"+at).val();
            }
        }
    }
	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		// $(this).val(valuebefore);
	}else{
		if($("#txtItemPrice"+at).autoNumeric('get')<0){
			$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
		}
		var previous=$("#subTotal"+at).autoNumeric('get');
		var price = $("#txtItemPrice"+at).autoNumeric('get');
		var subtotal=0;
		if($("#txtItem1Conv"+at).val()>0 && $("#txtItem1Qty"+at).val()>0){
			subtotal+=$("#txtItem1Qty"+at).val()*price;
		}
		if($("#txtItem2Conv"+at).val()>0 && $("#txtItem2Qty"+at).val()>0){
			subtotal+=$("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val();
		}
		if($("#txtItem3Conv"+at).val()>0 && $("#txtItem3Qty"+at).val()>0){
			subtotal+=$("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val();
		}
		for(var zz=1;zz<=3;zz++){
			subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
		}
		$("#subTotal"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
		if(totalwithdisc<=0){
			$("#subTotalWithDisc").autoNumeric('set','0');
			$("#subTotalTax").autoNumeric('set','0');
		}else{
			$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
			var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
			$("#subTotalTax").autoNumeric('set',totalTax);
		}

		$("#selectedItemsBonus tbody").html('');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		numberitembonus=0;
		totalitembonus=0;
        $("#change").val(1);
		getbonus();
	}

});

$("#selectedItems").on("focus","input[type='text'][name*='qty']",function(){
	valuebefore=$(this).val();
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	oldprice=$("#prcItemX"+at).autoNumeric('get');
	olddsc1=$("#dsc1ItemX"+at).val();
	olddsc2=$("#dsc2ItemX"+at).val();
	olddsc3=$("#dsc3ItemX"+at).val();
});
$("#selectedItems").on("change","input[type='text'][name*='qty']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var max=0;
	var now=0;
	var temp='';
    if($("#canvas").val()=="1"){
        max=1;
        now=0;
    }else{
        for(var zz=1;zz<=3;zz++){
            if($("#qty"+zz+"PriceEffectX"+at).val()<0){
                $("#qty"+zz+"PriceEffectX"+at).val($("#qty"+zz+"PriceEffectX"+at).val()*-1);
            }
            if($("#max"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
                max+=$("#conv"+zz+"UnitX"+at).val()*$("#max"+zz+"ItemX"+at).val();
            }

            if($("#qty"+zz+"PriceEffectX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
                now+=$("#qty"+zz+"PriceEffectX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
            }
        }
    }
	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		// $(this).val(valuebefore);
	}else{
		if($("#prcItemX"+at).autoNumeric('get')<0){
			$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
		}
		var previous=$("#subTotalX"+at).autoNumeric('get');
		var price = $("#prcItemX"+at).autoNumeric('get');
		var subtotal=0;
		if($("#conv1UnitX"+at).val()>0 && $("#qty1PriceEffectX"+at).val()>0){
			subtotal+=$("#qty1PriceEffectX"+at).val()*price;
		}
		if($("#conv2UnitX"+at).val()>0 && $("#qty2PriceEffectX"+at).val()>0){
			subtotal+=$("#qty2PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val();
		}
		if($("#conv3UnitX"+at).val()>0 && $("#qty3PriceEffectX"+at).val()>0){
			subtotal+=$("#qty3PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val();
		}
		for(var zz=1;zz<=3;zz++){
			subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
		}
		var satuan=parseFloat(subtotal)/parseFloat(now);
        if(now==0){
            satuan=price;
            for(var zzz=1;zzz<=3;zzz++){
                satuan=satuan-(satuan*$("#dsc"+zzz+"ItemX"+at).val()/100);
            }
            satuan=parseFloat(satuan)/parseFloat($("#conv1UnitX"+at).val());
        }
        var hpp=parseFloat($("#HppX"+at).val())/parseFloat($("#conv1UnitX"+at).val());
		if(satuan<hpp){
            if($("#typeItemX"+at).val() == '1') {
                var confirmHPP = "Harga satuan dari barang setelah didiskon kurang dari harga HPP";
                alert(confirmHPP);
                // if(!confirm(confirmHPP)) {
                //     $("#prcItemX"+at).autoNumeric('set',oldprice);
                //     $("#dsc1ItemX"+at).val(olddsc1);
                //     $("#dsc2ItemX"+at).val(olddsc2);
                //     $("#dsc3ItemX"+at).val(olddsc3);
                //     $("#qty1PriceEffectX"+at).val(oldqty1);
                //     $("#qty2PriceEffectX"+at).val(oldqty2);
                //     $("#qty3PriceEffectX"+at).val(oldqty3);
                //     return false;
                // }
            }
        }
		$("#subTotalX"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
		if(totalwithdisc<=0){
			$("#subTotalWithDisc").autoNumeric('set','0');
			$("#subTotalTax").autoNumeric('set','0');
		}else{
			$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
			var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
			$("#subTotalTax").autoNumeric('set',totalTax);
		}

		$("#selectedItemsBonus tbody").html('');
		$("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		numberitembonus=0;
		totalitembonus=0;
		getbonus();
	}

});
$( "input[name*='ItemPrice']").focus(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	oldprice=$("#txtItemPrice"+at).autoNumeric('get');
	olddsc1=$("#txtItem1Disc"+at).val();
	olddsc2=$("#txtItem2Disc"+at).val();
	olddsc3=$("#txtItem3Disc"+at).val();
});


$( "input[name*='ItemPrice']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var now=0;
	for(var zz=1;zz<=3;zz++){
		if($("#txtItem"+zz+"Qty"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
			now+=$("#txtItem"+zz+"Qty"+at).val()*$("#txtItem"+zz+"Conv"+at).val();
		}
	}
	if($("#txtItemPrice"+at).autoNumeric('get')<0){
		$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotal"+at).autoNumeric('get');
	var price = $("#txtItemPrice"+at).autoNumeric('get');
	var subtotal=0;
	if($("#txtItem1Conv"+at).val()>0 && $("#txtItem1Qty"+at).val()>0){
		subtotal+=$("#txtItem1Qty"+at).val()*price;
	}
	if($("#txtItem2Conv"+at).val()>0 && $("#txtItem2Qty"+at).val()>0){
		subtotal+=$("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val();
	}
	if($("#txtItem3Conv"+at).val()>0 && $("#txtItem3Qty"+at).val()>0){
		subtotal+=$("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val();
	}
	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
	}
    var satuan=parseFloat(subtotal)/parseFloat(now);
    if(now==0){
        satuan=price;
        for(var zzz=1;zzz<=3;zzz++){
            satuan=satuan-(satuan*$("#txtItem"+zzz+"Disc"+at).val()/100);
        }
        satuan=parseFloat(satuan)/parseFloat($("#txtItem1Conv"+at).val());
    }
    var hpp=parseFloat($("#Hpp"+at).val())/parseFloat($("#txtItem1Conv"+at).val());
	if(satuan<hpp){
		/*$("#txtItemPrice"+at).autoNumeric('set',$("#Harga"+at).val());
		$("#txtItem1Disc"+at).val(olddsc1);
		$("#txtItem2Disc"+at).val(olddsc2);
		$("#txtItem3Disc"+at).val(olddsc3);*/
        // var confirmHPP = "harga satuan dari barang setelah didiskon kurang dari harga HPP: "+$("#Hpp"+at).val();
        var confirmHPP = "harga satuan dari barang setelah didiskon kurang dari harga HPP";
		alert(confirmHPP);
		// return false;
	}
	$("#subTotal"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$( "input[name*='Disc']").focus(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	oldprice=$("#txtItemPrice"+at).autoNumeric('get');
	olddsc1=$("#txtItem1Disc"+at).val();
	olddsc2=$("#txtItem2Disc"+at).val();
	olddsc3=$("#txtItem3Disc"+at).val();
});

$( "input[name*='Disc']").change(function(){ /* cek lagi apa keluar hpp e nek rendah . . . */
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var now=0;
	for(var zz=1;zz<=3;zz++){

		if($("#txtItem"+zz+"Qty"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
			now+=$("#txtItem"+zz+"Qty"+at).val()*$("#txtItem"+zz+"Conv"+at).val();
		}
	}
	if($("#txtItemPrice"+at).autoNumeric('get')<0){
		$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotal"+at).autoNumeric('get');
	var price = $("#txtItemPrice"+at).autoNumeric('get');
	var subtotal=0;
	if($("#txtItem1Conv"+at).val()>0 && $("#txtItem1Qty"+at).val()>0){
		subtotal+=$("#txtItem1Qty"+at).val()*price;
	}
	if($("#txtItem2Conv"+at).val()>0 && $("#txtItem2Qty"+at).val()>0){
		subtotal+=$("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val();
	}
	if($("#txtItem3Conv"+at).val()>0 && $("#txtItem3Qty"+at).val()>0){
		subtotal+=$("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val();
	}
	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
	}
	var satuan=parseFloat(subtotal)/parseFloat(now);
    if(now==0){
        satuan=price;
        for(var zzz=1;zzz<=3;zzz++){
            satuan=satuan-(satuan*$("#txtItem"+zzz+"Disc"+at).val()/100);
        }
        satuan=parseFloat(satuan)/parseFloat($("#txtItem1Conv"+at).val());
    }
    var hpp=parseFloat($("#Hpp"+at).val())/parseFloat($("#txtItem1Conv"+at).val());
	if(satuan<hpp){
		/*$("#txtItemPrice"+at).autoNumeric('set',$("#Harga"+at).val());
		$("#txtItem1Disc"+at).val(olddsc1);
		$("#txtItem2Disc"+at).val(olddsc2);
		$("#txtItem3Disc"+at).val(olddsc3);*/
        var confirmHPP = "harga satuan dari barang setelah didiskon kurang dari harga HPP";
		alert(confirmHPP);
	}
	$("#subTotal"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("#selectedItems").on("focus","input[type='text'][id*='prcItemX']",function(){
	valuebefore=$(this).val();
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	oldprice=$("#prcItemX"+at).autoNumeric('get');
	olddsc1=$("#dsc1ItemX"+at).val();
	olddsc2=$("#dsc2ItemX"+at).val();
	olddsc3=$("#dsc3ItemX"+at).val();
});

$("#selectedItems").on("change","input[type='text'][id*='prcItemX']",function(){ /* ini harga */
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var now=0;
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"PriceEffectX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			now+=$("#qty"+zz+"PriceEffectX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
		}
	}
	if($("#prcItemX"+at).autoNumeric('get')<0){
		$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotalX"+at).autoNumeric('get');
	var price = $("#prcItemX"+at).autoNumeric('get');
	var subtotal=0;
	if($("#conv1UnitX"+at).val()>0 && $("#qty1PriceEffectX"+at).val()>0){
		subtotal+=$("#qty1PriceEffectX"+at).val()*price;
	}
	if($("#conv2UnitX"+at).val()>0 && $("#qty2PriceEffectX"+at).val()>0){
		subtotal+=$("#qty2PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val();
	}
	if($("#conv3UnitX"+at).val()>0 && $("#qty3PriceEffectX"+at).val()>0){
		subtotal+=$("#qty3PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val();
	}
	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
	}
	var satuan=parseFloat(subtotal)/parseFloat(now);
    if(now==0){
        satuan=price;
        for(var zzz=1;zzz<=3;zzz++){
            satuan=satuan-(satuan*$("#dsc"+zzz+"ItemX"+at).val()/100);
        }
        satuan=parseFloat(satuan)/parseFloat($("#conv1UnitX"+at).val());
    }
    var hpp=parseFloat($("#HppX"+at).val())/parseFloat($("#conv1UnitX"+at).val());
	if(satuan<hpp){
		/*$("#prcItemX"+at).autoNumeric('set',$("#HargaX"+at).val());
		$("#dsc1ItemX"+at).val(olddsc1);
		$("#dsc2ItemX"+at).val(olddsc2);
		$("#dsc3ItemX"+at).val(olddsc3);*/
        var confirmHPP
		alert("harga satuan dari barang setelah didiskon kurang dari harga hpp, HPP: "+$("#HppX"+at).val());
		return false;
	}
	$("#subTotalX"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("#selectedItems").on("focus","input[type='text'][id*='dsc'][name*=PriceEffect]",function(){
	valuebefore=$(this).val();
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	oldprice=$("#prcItemX"+at).autoNumeric('get');
	olddsc1=$("#dsc1ItemX"+at).val();
	olddsc2=$("#dsc2ItemX"+at).val();
	olddsc3=$("#dsc3ItemX"+at).val();
});

$("#selectedItems").on("change","input[type='text'][id*='dsc'][name*=PriceEffect]",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var now=0;
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"PriceEffectX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			now+=$("#qty"+zz+"PriceEffectX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
		}
	}
	if($("#prcItemX"+at).autoNumeric('get')<0){
		$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
	}
	var previous=$("#subTotalX"+at).autoNumeric('get');
	var price = $("#prcItemX"+at).autoNumeric('get');
	var subtotal=0;
	if($("#conv1UnitX"+at).val()>0 && $("#qty1PriceEffectX"+at).val()>0){
		subtotal+=$("#qty1PriceEffectX"+at).val()*price;
	}
	if($("#conv2UnitX"+at).val()>0 && $("#qty2PriceEffectX"+at).val()>0){
		subtotal+=$("#qty2PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val();
	}
	if($("#conv3UnitX"+at).val()>0 && $("#qty3PriceEffectX"+at).val()>0){
		subtotal+=$("#qty3PriceEffectX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val();
	}
	for(var zz=1;zz<=3;zz++){
		subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
	}
	var satuan=parseFloat(subtotal)/parseFloat(now);
    if(now==0){
        satuan=price;
        for(var zzz=1;zzz<=3;zzz++){
            satuan=satuan-(satuan*$("#dsc"+zzz+"ItemX"+at).val()/100);
        }
        satuan=parseFloat(satuan)/parseFloat($("#conv1UnitX"+at).val());
    }
    var hpp=parseFloat($("#HppX"+at).val())/parseFloat($("#conv1UnitX"+at).val());
	if(satuan<hpp){
		/*$("#prcItemX"+at).autoNumeric('set',$("#HargaX"+at).val());
		$("#dsc1ItemX"+at).val(olddsc1);
		$("#dsc2ItemX"+at).val(olddsc2);
		$("#dsc3ItemX"+at).val(olddsc3);*/
        var confirmHPP = "harga satuan dari barang setelah didiskon kurang dari harga HPP";
		alert(confirmHPP);
		// return false;
	}
	$("#subTotalX"+at).autoNumeric('set',subtotal);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});

$("#txtNewItemBonus").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		if(strAllowedBonusID!=''){
			$.ajax({
				url: "<?=site_url('invoice_ajax/getBonusAutoCompleteByPromo', NULL, FALSE)?>/" + strAllowedBonusID + "/" + $('input[name="txtNewItemBonus"]').val(),
				beforeSend: function() {
					$("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
				},
				complete: function() {
					$("#loadItemBonus").html('');
				},
				success: function(data){
					var xmlDoc = $.parseXML(data);
					var xml = $(xmlDoc);
					$arrSelectedPO = xml;
					var display=[];
					$.map(xml.find('Product').find('item'),function(val,i){
						var intID = $(val).find('id').text();
						var strName=$(val).find('strName').text();
                        var strKode=$(val).find('strKode').text();
						if($.inArray(intID, selectedItemBonusBefore) > -1){

						}else{
							display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
						}
					});
					response(display);
				}
			});
		}
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalitembonus;
        var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberitembonus+numberitembonusawal==0){
			$("#selectedItemsBonus tbody").html('');
		}
		$("#selectedItemsBonus tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
				'<td><label id="maxStrBonusX'+i+'" class="canvas"></label></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td><label id="nmeItemBonusX'+i+'"></label></td>'+
				'<input type="hidden" id="idBonusBrandX'+i+'" name="idBonusBrand'+i+'">'+
				'<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
				'<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
				'<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
				'<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
				'<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
				'<input type="hidden" id="conv1UnitBonusX'+i+'" >' +
				'<input type="hidden" id="conv2UnitBonusX'+i+'" >' +
				'<input type="hidden" id="conv3UnitBonusX'+i+'" >'+
				'<input type="hidden" id="max1ItemBonusX'+i+'" >' +
				'<input type="hidden" id="max2ItemBonusX'+i+'" >' +
				'<input type="hidden" id="max3ItemBonusX'+i+'" >'+
				'<input type="hidden" id="SameBonusX'+i+'" >' +
				'<input type="hidden" id="IsAwalSameBonusX'+i+'" >'+
				'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		var id=$(selectedPO).find('id').text();
		selectedItemBonusBefore.push(id);
		$("#idBonusBrandX"+i).val($(selectedPO).find('brand_id').text());
		$("#nmeItemBonusX"+i).text("("+prodcode+") "+ strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
		$("#idItemBonusX"+i).val(id);
		$("#prodItemBonusX"+i).val(prodtitle);
		$("#probItemBonusX"+i).val(probtitle);
		$("#qty1ItemBonusX"+i).hide();
		$("#qty2ItemBonusX"+i).hide();
		$("#qty3ItemBonusX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemBonusX"+i).show();
					$("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitBonusX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
				}
				var indexof=-1;
				for(var zz=0;zz<totalitemawal;zz++){
					if($("#idItem"+selectedIDPurchase[zz]).val()==id){
						indexof=selectedIDPurchase[zz];
						$("#IsAwalSameBonusX"+i).val(1);
					}
				}
				if(indexof==-1){
					for(var zz=0;zz<totalitem;zz++){
						if($("#idItemX"+zz).val()==id){
							indexof=zz;
							$("#IsAwalSameBonusX"+i).val(0);
						}
					}
				}
				if(indexof==-1){
					$.ajax({
						url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
						success: function(data){
							var xmlDoc = $.parseXML(data);
							var xml = $(xmlDoc);
							$arrSelectedPO = xml;
							var max =xml.find('Stock').text();
							for(var rr=0;rr<arrTambahanID.length;rr++){
								if(arrTambahanID[rr]==id){
									max=parseInt(max)+parseInt( arrTambahanQty[rr]);
								}
							}
							for(var rr = 1; rr <= 3; rr++) {
                                if($("#conv"+rr+"UnitBonusX"+i).val() != '') {
                                    var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                    if(rr == 1) {
                                        var conv = xml.find('prod_conv1').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                            $("#maxStrBonusX"+i).text(hasil);
                                            $("#max1ItemBonusX"+i).val(hasil);
                                        } else {
                                            $("#maxStrBonusX"+i).text('0');
                                            $("#max1ItemBonusX"+i).val('0');
                                        }
                                    } else if(rr == 2) {
                                        conv = xml.find('prod_conv2').text();
                                        if(parseInt(conv) > 1) {
                                            max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
                                            $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                            $("#max2ItemBonusX"+i).val(hasil);
                                        } else {
                                            $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | 0");
                                            $("#max2ItemBonusX"+i).val('0');
                                        }
                                    } else if(rr == 3) {
                                        $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                        $("#max3ItemBonusX"+i).val(hasil);
                                    }
                                }
                            }
						}
					});
				}else
				{
					if($("#IsAwalSameBonusX"+i).val()==1){
						$("#SameBonusX"+i).val(indexof);
						$("#maxStrBonusX"+i).text($("#maxStrAwal"+indexof).text());
						$("#max1ItemBonusX"+i).val($("#max1Item"+indexof).val());
						$("#max2ItemBonusX"+i).val($("#max2Item"+indexof).val());
						$("#max3ItemBonusX"+i).val($("#max3Item"+indexof).val());
					}else{
						$("#SameBonusX"+i).val(indexof);
						$("#maxStrBonusX"+i).text($("#maxStrX"+indexof).text());
						$("#max1ItemBonusX"+i).val($("#max1ItemX"+indexof).val());
						$("#max2ItemBonusX"+i).val($("#max2ItemX"+indexof).val());
						$("#max3ItemBonusX"+i).val($("#max3ItemX"+indexof).val());
					}

				}
                if($("#canvas").val()=="1"){
                    $(".canvas").hide();
                }else{
                    $(".canvas").show();
                }
			}
		});
		totalitembonus++;
		numberitembonus++;
		$("#totalItemBonus").val(totalitembonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
	}
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffectBonus']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var at2=this.id.substring(this.id.indexOf('y')+1,this.id.indexOf('I'));
	var indexof=0;
	var max=0;
	var now=0;
	var temp='';

	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemBonusX"+at).val()<0){
			$("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
		}
		if($("#max"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"UnitBonusX"+at).val()>0){
			max+=$("#conv"+zz+"UnitBonusX"+at).val()*$("#max"+zz+"ItemBonusX"+at).val();
		}

		if($("#qty"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"UnitBonusX"+at).val()>0){
			now+=$("#qty"+zz+"ItemBonusX"+at).val()*$("#conv"+zz+"UnitBonusX"+at).val();
		}
		if($("#SameBonusX"+at).val()!=-1){
			if($("#IsAwalSameBonusX"+at).val()==1){
				/* kalok awal */
				if($("#txtItem"+zz+"Qty"+$("#SameBonusX"+at).val()).val()>0 && $("#txtItem"+zz+"Conv"+$("#SameBonusX"+at).val()).val()>0){
					now+=$("#txtItem"+zz+"Qty"+$("#SameBonusX"+at).val()).val()*$("#txtItem"+zz+"Conv"+$("#SameBonusX"+at).val()).val();
				}
			}else{
				/* kalok ga */
				if($("#qty"+zz+"PriceEffectX"+$("#SameBonusX"+at).val()).val()>0 && $("#conv"+zz+"UnitX"+$("#SameBonusX"+at).val()).val()>0){
					now+=$("#qty"+zz+"PriceEffectX"+$("#SameBonusX"+at).val()).val()*$("#conv"+zz+"UnitX"+$("#SameBonusX"+at).val()).val();
				}
			}
		}
	}
	if(valuebefore=='')
	{
	 valuebefore=0;
	}
	if((parseInt(max)+parseInt(valuebefore))<now ){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		// $(this).val(valuebefore);
	}else{
		for(var i=0;i<allowedBonusID.length;i++){
			if(allowedBonusID[i]==$("#idBonusBrandX"+at).val()){
				indexof=i;
				break;
			}
		}
		if($(this).val()<0){
			$(this).val($(this).val()*-1);
		}
		var konversi=$("#conv"+at2+"UnitBonusX"+at).val();
		var temp;
		if(valuebefore==''){
			temp=0;
		}else{
			temp=valuebefore;
		}
        if(parseInt($(this).val())*parseInt(konversi)>parseInt(temp)*parseInt(konversi)+parseInt(allowedBonusQty[indexof])){
			alert("jumlah barang yang dimasukkan melebihi bonus yang dapat diberikan");
			// $(this).val(valuebefore);
		}
		if(valuebefore!=$(this).val()){
			if(valuebefore!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])+(parseInt(valuebefore)*parseInt(konversi));
			}
			if($(this).val()!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])-(parseInt($(this).val())*parseInt(konversi));
			}
		}
	}
});

$("#selectedItemsBonus").on("focus","input[type='text'][name*='Qty']",function(){
	var at=this.id.substring(this.id.indexOf('y')+1,this.id.length);
	valuebefore=$(this).val();
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='Qty']",function(){
	var at=this.id.substring(this.id.indexOf('y')+1,this.id.length);
	var at2=this.id.substring(this.id.indexOf('s')+1,this.id.indexOf('Q'));
	var indexof=0;
	var max=0;
	var now=0;
	var temp='';

	for(var zz=1;zz<=3;zz++){
		if($("#txtItemBonus"+zz+"Qty"+at).val()<0){
			$("#txtItemBonus"+zz+"Qty"+at).val($("#txtItemBonus"+zz+"Qty"+at).val()*-1);
		}
		if($("#max"+zz+"ItemBonus"+at).val()>0 && $("#conv"+zz+"UnitBonus"+at).val()>0){
			max+=$("#conv"+zz+"UnitBonus"+at).val()*$("#max"+zz+"ItemBonus"+at).val();
		}

		if($("#txtItemBonus"+zz+"Qty"+at).val()>0 && $("#conv"+zz+"UnitBonus"+at).val()>0){
			now+=$("#txtItemBonus"+zz+"Qty"+at).val()*$("#conv"+zz+"UnitBonus"+at).val();
		}
		if($("#SameBonusAwal"+at).val()!=-1){
			if($("#IsAwalSameBonusAwal"+at).val()==1){
				/* kalok awal */
				if($("#txtItem"+zz+"Qty"+$("#SameBonusAwal"+at).val()).val()>0 && $("#txtItem"+zz+"Conv"+$("#SameBonusAwal"+at).val()).val()>0){
					now+=$("#txtItem"+zz+"Qty"+$("#SameBonusAwal"+at).val()).val()*$("#txtItem"+zz+"Conv"+$("#SameBonusAwal"+at).val()).val();
				}
			}else{
				/* kalok ga */
				if($("#qty"+zz+"PriceEffectX"+$("#SameBonusAwal"+at).val()).val()>0 && $("#conv"+zz+"UnitX"+$("#SameBonusAwal"+at).val()).val()>0){
					now+=$("#qty"+zz+"PriceEffectX"+$("#SameBonusAwal"+at).val()).val()*$("#conv"+zz+"UnitX"+$("#SameBonusAwal"+at).val()).val();
				}
			}
		}
	}
	if((parseInt(max)+parseInt(valuebefore))<now ){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		// $(this).val(valuebefore);
	}else{
		for(var i=0;i<allowedBonusID.length;i++){
			if(allowedBonusID[i]==$("#idBonusBrand"+at).val()){
				indexof=i;
				break;
			}
		}
		if($(this).val()<0){
			$(this).val($(this).val()*-1);
		}
		var konversi=parseInt($("#conv"+at2+"UnitBonus"+at).val());
		var temp;
		if(valuebefore==''){
			temp=0;
		}else{
			temp=valuebefore;
		}
        if(parseInt($(this).val())*parseInt(konversi)>parseInt(temp)*parseInt(konversi)+parseInt(allowedBonusQty[indexof])){
            alert("jumlah barang yang dimasukkan melebihi bonus yang dapat diberikan");
            // $(this).val(valuebefore);
        }
		if(valuebefore!=$(this).val()){
			if(valuebefore!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])+(parseInt(valuebefore)*parseInt(konversi));
			}
			if($(this).val()!=''){
				allowedBonusQty[indexof]=parseInt(allowedBonusQty[indexof])-(parseInt($(this).val())*parseInt(konversi));
			}
		}
	}
});

$("#selectedItemsBonus").on("focus","input[type='text'][name*='PriceEffectBonus']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	valuebefore=$(this).val();
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDeleteBonusX']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var indexof=0;
	for(var i=0;i<allowedBonusID.length;i++){
		if(allowedBonusID[i]==$("#idBonusBrandX"+at).val()){
			indexof=i;
			break;
		}
	}
	for(var i=1;i<=3;i++){
		var konversi=$("#conv"+i+"UnitBonusX"+at).val();
		if($("#qty"+i+"ItemBonusX"+at).val()!='' && $("#qty"+i+"ItemBonusX"+at).val()>0){
			allowedBonusQty[indexof]=allowedBonusQty[indexof]+($("#qty"+i+"ItemBonusX"+at).val()*konversi);
		}
	}
	numberitembonus--;
	if(numberitembonus + numberitembonusawal==0){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonusX"+at).val();
	});
	$(this).closest("tr").remove();

});

$(document).on('click', '[id^=nmeItem] .fa-question-circle', function() {
    var $this = $(this);
    $.ajax({
        url: "<?=site_url('invoice_ajax/getLastProductPrice')?>",
        data: {
            product_id: $this.closest('tr').find('[id^=idItem]').val(),
        },
        dataType: 'json',
        success: function(data) {
            var msg = "Harga Penjualan Terakhir: \n\n";
            $.each(data.Items, function() {
                msg = msg + this.strDate + ' - ' + this.invo_customer_name + ' - ' + this.strPrice + "\n";
            });
            alert(msg);
        }
    });
});

});</script>