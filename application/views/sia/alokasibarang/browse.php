<?php
echo "<script>console.log('aaa');</script>";
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-cubes"></i> Alokasi Barang Pembelian', 'link' => site_url('alokasibarang/browse', NULL, FALSE)),
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
    <div class="row">
        <div class="col-md-12">
                <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
                    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('alokasibarang/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
        </div>
        <div class="col-md-12">                            
            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover" id="selectedBahans">
                    <thead>
                        <tr>
                            <th>Owner</th>
                            <th>Nama Proyek</th>                                    
                            <th>Pekerjaan</th>
                            <th class="action">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            //echo "<script>alert($arrDataPO);</script>";
                            if(!empty($dataAlokasi)):
                                foreach($dataAlokasi as $e):
                        ?>
                        <tr>
                            <td><?= $e['cust_name'] ?></td>
                            <td><?= $e['kont_name'] ?></td>
                            <td><?= $e['job'] ?></td>
                            <td class="action"><a href="<?=site_url('alokasibarang/view/'.$e['id'])?>"><i class="fa fa-eye"></i></a></td>                                   
                        </tr>
                        <?php
                                endforeach;
                            else:
                        ?>
                        <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        <?php
                            endif;
                        ?>
                    </tbody>
                </table>
            </div>                            
        </div>
    </div>
</div>