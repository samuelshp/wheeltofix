<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">

$("select[name^='item']").change(function(){
	var elValue = $(this).val();		
	$(this).find("option").val(elValue+"-"+$(this).data('item'));
	elValue = null;	
});

$("#smtUpdateAlokasi").click(function(){    
    $(this).hide();
    $("#btnUpdate").show();
//    $("#btnDelete").show();
	$("tr.item").append(`<td><input type="hidden" name="idToDelete[]" class="idToDelete"><button type="submit" class="btn btn-danger btn-sm" name="btnDeleteItem" value="Delete Item"><i class="fa fa-trash"></i></button></td>`);
    $(".form-control").removeAttr('disabled');
    $(".disable").attr("disabled",true);
    $(".chosen").chosen('destroy');    
    $(".chosen").chosen({width: "100%",search_contains: true});
    deleteItemHandler();    
});

function deleteItemHandler() {
	 $("button[name=btnDeleteItem]").click(function(e){
        var id = $(this).closest('tr').find("input.item_id").val();
        console.log(id);
        $(this).closest('tr').find("input.idToDelete").val(id); 
        var konfirmasi = confirm("Apakah yakin akan menghapus item?");        
        if (konfirmasi) {
            $(this).parents('tr').hide(200, function(){            
		            $(this).parents('tr').remove()
	        });		    
        }

        e.preventDefault();
    });
}

$(".chosen").chosen({width: "100%",search_contains: true});
</script>