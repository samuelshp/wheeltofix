<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery-dateformat.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">

let arrAlokasi = [];	

var $loading = $('.ajax-loader').hide();
var $table = $('table');

$("#intProyek").change(function(){
	$("#arrKontrak .optionResult").empty();
	loadKontrak($(this).val()).done(function(result, textStatus, xhr){
		if(xhr.status == 200){
			if (result.success) {
				$.each(result.data, function(i,arr){				
					$("select#arrKontrak").append(`
						<option class="optionResult" value="`+arr['id']+`">`+arr['job']+`</option>
					`);
				});
				$("select#arrKontrak").trigger("chosen:updated");
			}
		}
	});
});

$("#arrKontrak").change(function(){
	var intProyek = $("select#intProyek").val();
	var intKontrak = $(this).val();	

$(document)
	.ajaxStart(function () {
	  	$table.hide();
	    $loading.show();
	})
	.ajaxStop(function () {
		$loading.hide();
		$table.show()
	});

	$.ajax({		
		url: "<?=site_url('api/alokasi_barang_pembelian_purchase/list?h="+userHash+"&filter_pb-idKontrak="+intProyek+"&filter_pb-idSubKontrak=')?>"+intKontrak,
		method: "GET",
		dataType: "json",
		success: function(result, textStatus, xhr){
			if (xhr.status == 200) {
				if (result.success) {								
					$("#tbDataPurchase .info").hide();

					$.each(result.data, function(i,arr){
						var prch_date = $.format.date(new Date(Date.parse(arr['prch_date'].replace(/[-]/g,'/'))), "dd MMMM yyyy"); 					
						$("#tbDataPurchase").append(`
							<tr>								
								<td>`+arr['prod_title']+` <input type='hidden' name="purchaseAlocated[]"/></td>
								<td>`+arr['pror_code']+`</td>
								<td>`+arr['prch_code']+` <input type='hidden' class="detail" value='`+arr['op_item_id']+`' data-type="prci"/></td>
								<td>`+prch_date+`</td>
								<td>`+arr['prci_quantity1']+" "+arr['unit_title']+`</td>
								<td class='currency-text'>`+arr['prci_price']+`</td>
								<td class='currency-text'>`+arr['totalBelumPPn']+`</td>
								<td>
									<select class="form-control chosen alokasiPembelian">
										<option value="-1" disabled selected>Select your option</option>
										<option value="0">TIDAK DIALOKASIKAN</option>
									</select>
								</td>
							</tr>
						`);					
					});

					$.ajax({
						url: "<?=site_url('api/subkontrak_nonmaterial/list?h="+userHash+"&filter_subkontrak_id=')?>"+intKontrak,
						method: "GET",
						dataType: "json",
						success: function(result, textStatus, xhr){
							$.each(result.data, function(i,arr){						
								$("select.alokasiPembelian").append(`<option value="`+arr.id+`">`+arr.kategori+`</option>`);
							});	
							$("select.alokasiPembelian").trigger("chosen:updated");
						},
						error: function(err) {
							alert("Gagal memuat data subkontrak non-material");
						}
					});

					selectItemHandler();					
					$(".currency-text").autoNumeric('init', {autoNumericOptionsRupiah, aSign: "IDR "});
					$(".chosen").chosen({width: "100%",search_contains: true});
				}
			}else{
				alert("Gagal memuat data purchase ["+xhr.status+"]");
			}
		},
		error: function(err) {
			alert("Gagal memuat data purchase [connection]");
		}
	});

	$.ajax({
		url: "<?=site_url('api/alokasi_barang_pembelian_pengeluaran/list?h="+userHash+"&filter_txod_ref_id=')?>"+intProyek,
		method: "GET",
		dataType: "json",
		success: function (result, textStatus, xhr) {
			if (xhr.status = 200) {
				if (result.success) {
					$("#tbDataPengeluaranLain .info").hide();

					$.each(result.data, function(i,arr){
						var txou_date = $.format.date(new Date(Date.parse(arr['txou_date'].replace(/[-]/g,'/'))), "dd MMMM yyyy");					
						$("#tbDataPengeluaranLain").append(`
							<tr>
								<td>`+arr['txou_code']+`<input type='hidden' name="purchaseAlocated[]"/></td>
								<td>`+txou_date+`<input type='hidden' class="detail" value='`+arr['txod_id']+`' data-type='pgl'/></td>
								<td>`+arr['txod_description']+`</td>
								<td class='currency-text'>`+arr['txod_totalbersih']+`</td>								
								<td>
									<select class="form-control chosen alokasiPengeluaran">
										<option value="0" disabled selected>Select your option</option>
										<option value="1">TIDAK DIALOKASIKAN</option>
									</select>
								</td>
							</tr>
						`);					
					});

					$.ajax({
						url: "<?=site_url('api/subkontrak_nonmaterial/list?h="+userHash+"&filter_subkontrak_id=')?>"+intKontrak,
						method: "GET",
						dataType: "json",
						success: function(result, textStatus, xhr){
							$.each(result.data, function(i,arr){						
								$("select.alokasiPengeluaran").append(`<option value="`+arr.id+`">`+arr.kategori+`</option>`);
							});	
							$("select.alokasiPengeluaran").trigger("chosen:updated");
						},
						error: function(err) {
							alert("Gagal memuat data subkontrak non-material");
						}
					});

					selectItemHandler();
					$(".currency-text").autoNumeric('init', {autoNumericOptionsRupiah, aSign: "IDR "});
					$(".chosen").chosen({width: "100%",search_contains: true});					
				}
			}
		},
		error: function(err) {
			// body...
		}
	});

});



function loadKontrak(intProyek) {
	return $.ajax({		
		url: "<?=site_url('api/subkontrak/list?h="+userHash+"&filter_kontrak_id=')?>"+intProyek,
		method: "GET",
		dataType: "json"
	});
}

function loadPurchaseData(intProyek, intKontrak) {
	return true;
}

function loadPengeluaranLain(intProyek) {
	// body...
}

function loadAlokasiData(intKontrak) {	
	return true;
}

function selectItemHandler() {

	$("select.alokasiPembelian, select.alokasiPengeluaran").change(function() {
		var valSelect = $(this).val();		
		var item = $(this).parents("tr").find("input.detail");		
		if (valSelect >= 0) {
			$(this).parents("tr").find("input[name^='purchaseAlocated']").val(item.data('type')+"-"+item.val()+"-"+valSelect);
		}		
	});
	
}

$(".chosen").chosen({search_contains: true});
</script>