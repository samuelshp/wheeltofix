<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-cubes"></i> Alokasi Barang Pembelian', 'link' => site_url('alokasibarang/browse')),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<form method="POST" action="<?=site_url('alokasibarang')?>">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"> Data Alokasi Barang Pembelian</h3></div>
                    <div class="panel-body">    
                        <input type="hidden" name="intSubkontrakID" value="<?=$arrDataAlokasi['id']?>">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-12 control-label">
                                Nama Proyek : <?=$arrDataAlokasi['kont_name']?>
                            </div>                            
                            <div class="clearfix"></div>
                        </div>    

                        <div class="form-group">
                            <div class="col-sm-12 col-md-12 control-label">
                                Nama Kontrak : <?=$arrDataAlokasi['job']?>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Purchase</h3></div>
            <div class="panel-body">                               
                <div class="table-responsive" style="min-height: 10px; max-height: 500px; overflow: auto !important;"><table class="table table-bordered table-condensed table-hover" id="purchased">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>No. PB</th>
                                <th>No. OP</th>
                                <th>Tanggal OP</th>
                                <th>Qty</th>                            
                                <th>Amount</th>
                                <th>Total (belum PPn)</th>
                                <th colspan="2">Alokasi</th>
                            </tr>
                        </thead>
                        <tbody id="tbDataPurchase">
                            <?php if(!empty($arrPurchase)): foreach($arrPurchase as $e): ?>
                                <tr class="item">
                                    <td><?=$e['prod_title']?></td>
                                    <td><?=$e['pror_code']?></td>
                                    <td><?=$e['prch_code']?></td>
                                    <td><?=formatDate($e['prch_date'], "d F Y")?></td>
                                    <td><?=$e['prci_quantity1']." ".$e['unit_title']?></td>
                                    <td><?=setPrice($e['prci_price'])?></td>
                                    <td><?=setPrice($e['totalBelumPPn'])?></td>
                                    <td>
                                        <select class="form-control chosen" name="itemPembelianLain[]" disabled data-item="<?=$e['albp_id'];?>">
                                            <option value="-1" disabled>Select your option</option>
                                            <option value="0" <?=($e['albp_subkon_nonmaterial_id'] == 0 ) ? "selected" : null?> >TIDAK DIALOKASIKAN</option>
                                            <?php foreach($arrSubkontrakNonMaterial as $v): ?>
                                                <option value="<?=$v['id']?>" <?=($e['skm_id'] == $v['id']) ? "selected" : null?> ><?=$v['kategori']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?//=(!empty($e['kategori']) ? $e['kategori'] : 'TIDAK DIALOKASIKAN')?></td>
                                </tr>
                            <?php endforeach; else:?>
                            <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                      
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Pengeluaran Lain</h3></div>
            <div class="panel-body">                               
                <div class="table-responsive" style="min-height: 10px; max-height: 500px; overflow: auto !important;"><table class="table table-bordered table-condensed table-hover" id="pengeluaranlain">
                        <thead>
                            <tr>                                
                                <th>No. Transaksi</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Amount</th>                                
                                <th colspan="2">Alokasi</th>
                            </tr>
                        </thead>
                        <tbody id="tbDataPengeluaranLain">                    
                            <?php if(!empty($arrPengeluaran)): foreach($arrPengeluaran as $e): ?>
                                <tr class="item">
                                    <td><?=$e['txou_code']?></td>
                                    <td><?=formatDate($e['txou_date'], "d F Y")?></td>                                    
                                    <td><?=$e['txod_description']?></td>
                                    <td><?=setPrice($e['txod_totalbersih'])?></td>
                                    <td>
                                        <select class="form-control chosen" name="itemPengeluaranLain[]" disabled data-item="<?=$e['albp_id'];?>">
                                            <option value="-1" disabled>Select your option</option>
                                            <option value="0" <?=($e['albp_subkon_nonmaterial_id'] == 0 ) ? "selected" : null?> >TIDAK DIALOKASIKAN</option>
                                            <?php foreach($arrSubkontrakNonMaterial as $v): ?>
                                                <option value="<?=$v['id']?>" <?=($e['skm_id'] == $v['id']) ? "selected" : null?> ><?=$v['kategori']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endforeach; else:?>
                            <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                      
                            <?php endif; ?>                     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">                
                    <button type="button" name="smtEditAlokasi" id="smtUpdateAlokasi" value="Update Debt" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button> 
                    
                    <button type="submit" id="btnUpdate" name="smtUpdateAlokasi" value="Update" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                    
                    <button type="submit" id="btnDelete" name="smtDeleteAlokasi" value="Delete" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                           
                    
                </div>
            </div>
        </div>             
    </div>

    <input type="hidden" name="deleteItem"/>
</form>