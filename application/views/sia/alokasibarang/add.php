<?php
//daftar pembayaran hutang
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-cubes"></i> Alokasi Barang Pembelian', 'link' => site_url('alokasibarang/browse')),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<form method="POST" action="<?=site_url('alokasibarang')?>">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"> Data Alokasi Barang Pembelian</h3></div>
                    <div class="panel-body">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                Nama Proyek
                            </div>
                            <div class="col-sm-7 col-md-8">
                                <select class="form-control chosen" name="intProyek" id="intProyek" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrDataProyek as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>    

                        <div class="form-group">
                            <div class="col-sm-5 col-md-4 control-label">
                                Nama Kontrak
                            </div>
                            <div class="col-sm-7 col-md-8">
                                <select class="form-control chosen" name="arrKontrak" id="arrKontrak" required>
                                    <option value="0" disabled selected>Select your option</option>                                    
                                </select>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Purchase</h3></div>
            <div class="panel-body">                 
                <div class="table-responsive" style="min-height: 10px; max-height: 500px; overflow: auto">
                    <div class="ajax-loader" style="display: none"><?=loadLanguage("adminmessage",$this->session->userdata("jw_language"),"loading-data")?></div>                          
                    <table class="table table-bordered table-condensed table-hover" id="purchased">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>No. PB</th>
                                <th>No. OP</th>
                                <th>Tanggal OP</th>
                                <th>Qty</th>                            
                                <th>Amount</th>
                                <th>Total</th>
                                <th>Alokasi</th>
                            </tr>
                        </thead>
                        <tbody id="tbDataPurchase">                    
                            <tr class="info"><td class="noData" colspan="12"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> Data Pengeluaran Lain</h3></div>
            <div class="panel-body">                               
                <div class="table-responsive" style="min-height: 10px; max-height: 500px; overflow: auto">
                    <div class="ajax-loader" style="display: none"><?=loadLanguage("adminmessage",$this->session->userdata("jw_language"),"loading-data")?></div>                          
                    <table class="table table-bordered table-condensed table-hover" id="pengeluaranlain">
                        <thead>
                            <tr>                                
                                <th>No. Transaksi</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Amount</th>                                
                                <th>Alokasi</th>
                            </tr>
                        </thead>
                        <tbody id="tbDataPengeluaranLain">                    
                            <tr class="info"><td class="noData" colspan="5"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" name="smtMakeAlokasi" class="btn btn-success pull-right" value="save" style="margin-left:5px;"><i class="fa fa-floppy-o"></i> Save</button>
                <a href="<?= site_url('alokasibarang/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
            </div>
        </div>        
    </div>
</form>