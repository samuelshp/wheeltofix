<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 

$strSearchAction = site_url('purchase_invoice/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchpi.php'); ?>


<div class="col-xs-12"><form name="frmAddPurchaseInvoice" id="frmAddPurchaseInvoice" method="post" action="<?=site_url('purchase_invoice/browse', NULL, FALSE)?>" class="frmShop">

    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('purchase_invoice', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?> Purchase Invoice</th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?> Purchase Invoice</th>
			<th>Nama Proyek</th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></th>
			<th>No. Pembayaran</th>
			<th class="action">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrInvoice)):
			foreach($arrInvoice as $e): ?>
			<tr>
				<td><?=$e['pinv_code']?></td>
				<td><?=formatDate2($e['cdate'],'d F Y')?></td>
				<td><?=$e['kont_name']?></td>
				<td><?=setPrice($e['pinv_finaltotal'])?></td>
				<td>Tidak ada</td>
				<td class="action">
					<a href="<?=site_url('purchase_invoice/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>
					<?php if($e['bolBtnPrint']): ?><a  id="print<?=$e['id']?>" href="<?=site_url('purchase_invoice/view/'.$e['id'].'?print=true', NULL, FALSE)?>" id="Print-Link"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>
				</td>
				<!-- <td class="delete">
					<div class="form-group">
						<button type="submit" style="width:40px;" class="btn btn-danger" value="<?= $e['id'] ?>" name="delPurchaseInvoice" id="delPurchaseInvoice"><i class="fa fa-trash">
						</i></button>
					</div>
				</td> -->
				<input type="hidden" id="accItemId<?= $e['id'] ?>" name="accItemId<?= $e['id'] ?>" value="<?= $e['acc_item_id'] ?>"/>
				<input type="hidden" id="accId<?= $e['id'] ?>" name="accId<?= $e['id'] ?>" value="<?= $e['acc_id'] ?>"/>
			</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
	</form>
</div><!--
--><?/*=site_url('purchase/view/'.$e['id'].'?print=true')*/?>