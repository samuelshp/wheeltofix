<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-folder"></i> Purchase Invoice', 'link' => site_url('purchase_invoice/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<script>console.log('a');</script>

<div class="col-xs-12"><form name="frmChangeInvoice" id="frmChangeInvoice" method="post" action="<?=site_url('purchase_invoice/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data</h3></div>
            <div class="panel-body">
                <div class="col-xs-4">No. PI : </div>
                <div class="col-xs-8"><?=$arrPurchasedInvoice['pinv_code']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Tanggal dibuat : </div>
                <div class="col-xs-8"><?=formatDate2($arrPurchasedInvoice['cdate'],'d F Y')?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Supplier : </div>
                <div class="col-xs-8"><?=$arrPurchasedInvoice['supp_name']?></div>
                <p class="spacer">&nbsp;</p>
                <div class="col-xs-4">Nama Proyek : </div>
                <div class="col-xs-8"><?=$arrPurchasedInvoice['kont_name']?></div>
                <p class="spacer">&nbsp;</p>
                <input type="hidden" name="purchaseInvoiceId" id="purchaseInvoiceId" value="<?=$arrPurchasedInvoice['id']?>" />
                <input type="hidden" name="acceptanceId" id="acceptanceId" value="<?=$arrPurchasedInvoice['pinv_acceptance_id']?>" />
                <input type="hidden" name="acceptanceItemId" id="acceptanceItemId" value="<?=$arrPurchasedInvoice['acit_id']?>" />
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
            <div class="panel-body">
                <?php $intDPP = $arrPurchasedInvoice['pinv_subtotal'] - $arrPurchasedInvoice['pinv_discount'] - $arrPurchasedInvoice['pinv_dp']?>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="subtotal" id="subtotal" value="<?=$arrPurchasedInvoice['pinv_subtotal']?>"/><?=setPrice($arrPurchasedInvoice['pinv_subtotal'])?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="diskon" id="diskon" value="<?=$arrPurchasedInvoice['pinv_discount']?>"/><?=setPrice($arrPurchasedInvoice['pinv_discount'])?></div>
                </div>
                <!-- <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
                    <div class="col-sm-6 tdDesc"><?=setPrice(($arrPurchasedInvoice['pinv_subtotal'])-$arrPurchasedInvoice['pinv_discount'])?></div>
                </div> -->
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle">Potongan DP</div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="dp" id="dp" value="<?=$arrPurchasedInvoice['pinv_dp']?>"/><?=setPrice($arrPurchasedInvoice['pinv_dp']);?>
                        <?php if (!empty($arrHistory)):
                            echo "(";
                            foreach($arrHistory as $i => $history): echo ($history['dpay_code']);
                            if ($i < count($history)) echo ",";
                            endforeach;
                            echo ")";
                            endif; 
                        ?>
                    </div>                    
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle">Total DPP</div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="dpp" id="dpp" value=""/><?=setPrice($intDPP)?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountppn')?></div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="tax" id="tax" value="<?=$arrPurchasedInvoice['pinv_tax']?>"/><?=setPrice(($arrPurchasedInvoice['pinv_tax']*$intDPP)/100)?></div>
                </div>
                <!-- <?php
                    $new_grantotal = ($arrPurchasedInvoice['pinv_grandtotal']+$arrPurchasedInvoice['pinv_costadd'])+(($arrPurchasedInvoice['pinv_tax']*$arrPurchasedInvoice['pinv_grandtotal'])/100)-((($arrPurchasedInvoice['pinv_pph']*$arrPurchasedInvoice['pinv_grandtotal'])/100)-$arrPurchasedInvoice['pinv_amount_ppn_dp']);
                    ?> -->
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountpph')?></div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="pph" id="pph" value="<?=$arrPurchasedInvoice['pinv_pph']?>"/><?=setPrice((($arrPurchasedInvoice['pinv_pph']*$intDPP)/100)-$arrPurchasedInvoice['pinv_amount_pph_dp'])?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle">Biaya Tambahan</div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="tambahan" id="tambahan" value="<?=$arrPurchasedInvoice['pinv_costadd']?>"/><?=setPrice($arrPurchasedInvoice['pinv_costadd'])?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="total_pph" id="total_pph" value="<?=$arrPurchasedInvoice['pinv_grandtotal']?>"/><?=setPrice($arrPurchasedInvoice['pinv_grandtotal'])?></div>
                </div>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle">Nilai Pembulatan</div>
                    <div class="col-sm-6 tdDesc">
                        <?php 
                            if($arrPurchasedInvoice['pinv_status'] != 4){
                                echo "<input type='text' name='pembulatan' id='pembulatan' value='".$arrPurchasedInvoice['pinv_pembulatan']."'/>"; 
                            }
                            else{
                                echo "<div class='col-sm-6 tdDesc'>".setPrice($arrPurchasedInvoice['pinv_pembulatan'])."</div>";
                            }
                        ?>
                    </div>
                </div>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <p class="spacer">&nbsp;</p>
                <div class="row">
                    <div class="col-sm-6 tdTitle">Total Dengan Pembulatan</div>
                    <div class="col-sm-6 tdDesc"><input type="hidden" name="amountTotalPembulatan" id="amountTotalPembulatan" value="<?=str_replace('.',',',$arrPurchasedInvoice['pinv_finaltotal'])?>"/><?=setPrice($arrPurchasedInvoice['pinv_finaltotal'])?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">
    
<?php if($bolBtnEdit): 
endif; ?>  

    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
    <thead>
        <tr style="text-align:center;">
            <!-- <?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>   -->
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
                    <th>Qty OP</th>
                    <th>Qty PI</th>
                    <th>Qty Pengkali</th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-priceunitpay')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
        </tr>
    </thead>
    <tbody><?php
    
$i = 0;
$intTotalItem = 0;
$iditemawal='';
if(!empty($arrPurchasedInvoiceItem)):
    foreach($arrPurchasedInvoiceItem as $e):?>
    <tr style="text-align:center;">
        <td><?=$e['prod_title']?></td>
        <td><?=$e['prci_quantity1']?> <?=$e['satuanOp']?></td>
        <td><?= $e['pinvit_quantity']." ".$e['satuanBayar'] ?></td>
        <input type='hidden' name='qty<?=$i?>' id='qty<?=$i?>' value='<?=$e['pinvit_quantity']?>'/>
        <td>
            <?php
                if($arrPurchasedInvoice['pinv_status'] != 0){
                    echo "<input type='text' name='editPengkali".$i."' id='editPengkali".$i."' value=".$e['pinvit_quantity_pengkali']." />";
                }
                else{
                    echo $e['pinvit_quantity_pengkali'];
                }
            ?>
        </td>
        <td><input type='hidden' name='harga_bayar<?=$i?>' id='harga_bayar<?=$i?>' value='<?=$e['pinvit_price']?>'/><?=setPrice($e['pinvit_price'])?></td>
        <td><?=setPrice($e['pinvit_subtotal'])?></td>
        <input type="hidden" name="bahan<?=$i?>" id="bahan<?=$i?>" value="<?= $e['pinvit_product_id']?>"/>
        <input type="hidden" name="id<?=$i?>" id="id<?=$i?>" value="<?= $e['id']?>"/>
    </tr><?php

        $i++;
    endforeach;
    echo "<input type='hidden' id='jumlah_row' name='jumlah_row' value='".$i."'/> ";
    $iditemawal = $iditemawal.'0';
else: ?>  
        <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php 
endif; ?>  
    </tbody>
    </table></div>
    </div><!--/ Table Selected Items -->
</div>
    <div class="col-xs-12">
        <div class="form-group">
            <?php if($checkPiInDph>0){
                
            }else{?>
            <button type="submit" name="editPurchaseInvoiceItem" id="editPurchaseInvoiceItem" value="Edit Acceptance Item" class="btn btn-warning">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>  
            <button type="submit" name="subDelete" value="Delete" title="Delete" data-toggle="tooltip" onClick="return confirm('<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-confirmdelete')?>');" class="btn btn-danger pull-right"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></button>
            <?php  
            }
            ?>
            <a href="<?= site_url('purchase_invoice/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
        </div>
        <?php
            $rawStatus = $arrPurchasedInvoice['purchinvo_rawstatus'];
            //include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
        ?> 
    </div>
</div>
</div>
</form>