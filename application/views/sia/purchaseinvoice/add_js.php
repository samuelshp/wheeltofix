<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">	

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$('.currency').autoNumeric('init', autoNumericOptionsRupiah, {'mDec': 4});

//$("#tutup_sementara").hide();
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var mode=0;
var count = 0;
var totalItem=0;
var totalItemTambahan = 0;
var numberItem=0;
var totalItemDp= 0;
var totalItemBonus=0;
var numberItemBonus=0;
var selectedItemBefore=[];
var selectedItemBonusBefore=[];
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#totalDpp").autoNumeric('init', autoNumericOptionsRupiah);
$("#txtCostAdd").autoNumeric('init', autoNumericOptionsRupiah);
$("#amountPPN").autoNumeric('init', autoNumericOptionsRupiah);
$("#total_tanpa_ppn").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalCost").autoNumeric('init', autoNumericOptionsRupiah);
$("#txtPpn").autoNumeric('init', autoNumericOptionsRupiah);
$("#amountTotalPembulatan").autoNumeric('init',autoNumericOptionsRupiah);
$("#grandTotal").autoNumeric('init', autoNumericOptionsRupiah);
$("#totalDownPayment").autoNumeric('init', autoNumericOptionsRupiah);
$("#txtPPH").autoNumeric('init', autoNumericOptionsRupiah);
if($("#totalItem").val() != null){
	for(var i = 0; i<$("#totalItem").val(); i++){
		$("#hargasatuan"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#sum_barang"+i).autoNumeric('init', autoNumericOptionsRupiah);
	}
}
$("#selKP").prop('disabled', true);
function reset(){
    totalItem=0;
    numberItem=0;
    totalItemBonus=0;
    numberItemBonus=0;
    selectedItemBefore=[];
    selectedItemBonusBefore=[];
	$("#txtNewItem").val('');
    $("#txtNewItemBonus").val('');
	$("#subTotalNoTax").autoNumeric("set",0);
	$("#subTotalTax").autoNumeric("set",0);
    $("#subTotalWithDisc").autoNumeric("set",0);
};
$("#hide_sementara").hide();

$("#frmAddInvoice").submit(function() {		
	
	if($("#subTotalNoTax").autoNumeric("get") == ''){
		alert("Total harap diisi"); 
		return false;
	}
		
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);			
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));			
		}
	});	
	
	return true;
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});

$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
	$(this).autoNumeric('init', autoNumericOptionsRupiah);
});

$("#selectedItemsBonus").on("keyup","input[type='text'][class*='berubah']",function(){
	var at=this.name.substring(this.id.indexOf('s')+1,this.id.length);

	var jumlah = 0;
	var total_sekarang = 0;
	var total = 0;
	var total_barang = 0;
	var total_tambahan = 0;
	var counter = 0;
	var x = 0;
	var y = 0;
	var j=0;

	while(x < $("#total_item_tambahan").val()) {
		if($("#penanda_amount_tambahan" + x).val() > 0) {
			jumlah = parseFloat($("#amount_plus"+x).autoNumeric('get'));			
			if(isNaN(jumlah)){
				jumlah = 0;
			}
			total_tambahan+=jumlah;
			x++;
		}
	}	
	$("#txtCostAdd").autoNumeric('set',total_tambahan);

});

$("#downpayment").on("keyup","input[type='number'][class*='berubah']",function(){
	var at=this.name.substring(this.id.indexOf('p')+1,this.id.length);

	var jum_dp = $('#jumlahDp').val();
	var dp_now=0;
	var dp_now2 = 0;
	for($i=0;$i<jum_dp;$i++){
		dp_now = $("#untuk_dp"+$i).val();
		if(dp_now > 0){
			dp_now2 = dp_now2 + parseFloat(dp_now);
		}
	}
	checkLimitDP(dp_now2);
	$("#totalDownPayment").autoNumeric('set',dp_now2);

});

function checkLimitDP(currentDP){
	var $subTotalNoTax = ($("#subTotalNoTax").autoNumeric('get'));			
	var $diskon = ($("#dsc4Item").autoNumeric('get'));			
	if (currentDP > $subTotalNoTax-$diskon) {
		alert("Total DP dan Diskon melebihi Total Tanpa Pajak");		
	}
}

function berubah(){
	var jumlah = 0;
	var total_sekarang = 0;
	var total = 0;
	var total_barang = 0;
	var total_barang_temp = 0;
	var subtotal;
	var subtotal_temp = 0;
	var counter = 0;
	var jumlah_temp = 0;
	var x = 0;
	var y = 0;	
	
	subtotal = ($("#subTotalNoTax").autoNumeric('get'));			

	var diskon = ($("#dsc4Item").autoNumeric('get'));

	var harga_diskon = subtotal-diskon;
	var total_harga = harga_diskon-parseFloat($("#totalDownPayment").autoNumeric('get'));

	checkLimitDP($("#totalDownPayment").autoNumeric('get'));

	$("#totalDpp").autoNumeric('set', total_harga);	

	var ppn = ($("#txtPpn").autoNumeric('get'));
	var total_dengan_ppn_baru =0;

	if(isNaN(ppn)){
		ppn = 0;
	}
	
	total_dengan_ppn_baru = (((ppn*total_harga)/100));
	$("#amountPPN").autoNumeric('set', total_dengan_ppn_baru);//nilai ppn
		
	var biaya_tambahan = parseFloat($("#txtCostAdd").autoNumeric('get'));

	//total_dengan_ppn_baru += biaya_tambahan;	

	var pph = ($("#txtPPH").autoNumeric('get'));

	if(isNaN(pph)){
		pph = 0;
	}

	var total_dengan_pph = ((pph*total_harga)/100);
	var pembulatan, total_pembulatan;

	$("#amountPPH").autoNumeric('set', total_dengan_pph);

	$("#total_ppn_dp").val(total_ppn_dp);
	$("#total_pph_dp").val(total_pph_dp);

	latest_grand_total = ((total_harga+biaya_tambahan+(total_dengan_ppn_baru+total_harga))-(total_dengan_pph+total_harga));

	$("#grandTotal").autoNumeric('set', latest_grand_total);

	if($("#amountPembulatan").val() == null || $("#amountPembulatan").val() == ''){
		pembulatan = 0;
	}
	else{
		pembulatan = parseFloat($("#amountPembulatan").val().replace(',','.'));
	}

	total_pembulatan = latest_grand_total+pembulatan;	

	total_pembulatan = (total_pembulatan < 0 ) ? 0 : total_pembulatan;

	$("#amountTotalPembulatan").autoNumeric('set', total_pembulatan);
}

// $("#selectedItems").on("keyup","input[type='text'][class*='berubah']",function(){
// 	var at=this.name.substring(this.id.indexOf('n')+1,this.id.length);
// 	/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

// 	var x = 0;
//     var y = 0;
//     var subtotal = 0;
// 	var grandTotal = 0;
	
//     while(x < $("#totalItem").val()) {
//         if($("#penanda" + y).length > 0) {
// 			if($("#qtypengkali"+y).val() == '' || $("#qtypengkali"+y).val() == null){
// 				var qty_pengkali = 1;
// 			}
// 			else{
// 				var qty_pengkali = $("#qtypengkali"+y).val();
// 			}
// 			// var bayar = $("#qtybayar"+at).autoNumeric('get');
// 			var qty = $("#qtybayar"+at).val();
// 			//var qty = $("#qtybayar"+y).autoNumeric('get');
// 			var price = $("#hargasatuan"+y).autoNumeric('get');
//             var total = qty*price*qty_pengkali;
//             $("#sum_barang"+y).autoNumeric('set',total);
//             subtotal += total;
//             x++;
//         }
//         y++;
// 	}
		
// 	$("#subTotalNoTax").autoNumeric('set', subtotal);

// });

$("#totalDownPayment").bind('input',function(){
	alert("Change");
});

$("#txtPpn").keyup(function(){
	berubah();
});

$("#txtPPH").keyup(function(){
	berubah();
});

$("#dsc4Item").change(function(){
	berubah();
});

$("#amountPembulatan").change(function(){
	berubah();
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemBonusX"+at).val()<0){
            $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
        }
    }
    if($("input[type='radio'][name='radioPO']:checked").val()=='1'){
        var max1=$("#max1ItemBonusX"+at).val();
        var max2=$("#max2ItemBonusX"+at).val();
        var max3=$("#max3ItemBonusX"+at).val();
        var qty1=$("#qty1ItemBonusX"+at).val();
        var qty2=$("#qty2ItemBonusX"+at).val();
        var qty3=$("#qty3ItemBonusX"+at).val();
        var conv1=$("#conv1UnitBonusX"+at).val();
        var conv2=$("#conv2UnitBonusX"+at).val();
        var conv3=$("#conv3UnitBonusX"+at).val();
        var max=(max1)*(conv1)+(max2)*(conv2)+(max3)*(conv3);
        var qty=(qty1)*(conv1)+(qty2)*(conv2)+(qty3)*(conv3);
        if(qty>max ){
            $("#qty1ItemBonusX"+at).val(max1);
            $("#qty2ItemBonusX"+at).val(max2);
            $("#qty3ItemBonusX"+at).val(max3);
        }
    }
});
// $("#dsc4Item").change(function(){
// 	var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
// 	var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
//     var grandTotal = 0;
// 	if(totalwithdisc<=0){
// 		$("#subTotalWithDisc").autoNumeric('set','0');
// 		//$("#subTotalTax").autoNumeric('set','0');
// 		//grandTotal = $("#subTotalCost").autoNumeric('get');
//         $("#subTotalTax").autoNumeric('set', grandTotal);
// 	}else{
// 		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
// 		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
// 		$("#subTotalTax").autoNumeric('set',totalTax);
//         //grandTotal = totalTax + $("#subTotalCost").autoNumeric('get');
//         $("#grandTotal").autoNumeric('set',grandTotal);
// 	}
// })
$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems").append(
			'<tr><td colspan="6">'+'' +
				'<h4 style="text-align:center">'+
				'<?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?>'+
				'</h4>'+
				'</td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});

$('#selBPB').change(function(){
	var id_acceptance = $('#selBPB').val();
	var id_supp;
	var id_kontrak;	
    $.ajax({ 
        type : "GET",
        dataType:'json',
        url: "<?=site_url('api/acceptance_supplier_kontrak/list?h="+userHash+"&filter_t-id=')?>" + $('#selBPB').val(),
        cache : false,
        success : function(result){
            $('#selBPB').html('<option disabled value="0" selected>Select your option</option>');
            if (result.success === true) {            
	            $.each (result.data, function (index,arr) {
	                $('#selBPB').append('<option value="' + arr['acc_id'] + '" selected>' + arr['acce_code'] + '</option>');
					$("#selBPB").trigger("chosen:updated");
					$("#txtKodeKontrak").val(arr['kont_name']);
					$("#txtSupplier").val(arr['supp_name']);
					$("#txtTanggalBPB").val(arr['createAt']);
					$("#idAcceptance").val($('#selBPB').val());
					$("#idOrderPembelian").val(arr['idOrderPembelian']);
					id_supp = arr['prch_supplier_id'];
					id_kontrak = arr['idKontrak'];					
				});
			}
			$.ajax({
				url: "<?=site_url('api/acceptance_detail/list?h="+userHash+"&filter_acit_acceptance_id=', NULL, FALSE)?>"+id_acceptance,
				success: function(result){
					if (result.success === true) {					
					// if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
					// console.log(xmlDoc);
					// var xml = $(xmlDoc);
					// $arrSelectedCost = xml;
					// var display=[];					
						$.each(result.data,function(i,arr){
							var prod_title = arr['prod_title'];
							var id_prod = arr['idProduct'];
							var qty_pb = arr['proi_quantity1'];
							var satuan_pb = arr['title_pb'];
							var qty_terima = arr['acit_quantity1'];
							var qty_bayar = arr['acit_quantity_bayar'];
							var satuan_terima = arr['title_terima'];
							var deskripsi = arr['proi_description'];
							var idsubkon = arr['idSubKontrak'];
							if(deskripsi==''){
								deskripsi = "Tidak ada";
							}
							var lokasi = arr['acit_location'];
							var satuan_bayar = arr['title_bayar'];
							var harga_satuan = arr['prci_price'];
							var terpi = arr['acit_terpi'];
							var ai_id = arr['acci_id'];
							$(".noData").hide();
							$("#selectedItems tbody").append(
								'<tr>'+
									'<td><input type="hidden" name="bahan'+totalItem+'" id="bahan'+totalItem+'" value="'+id_prod+'"/>'+prod_title+'</td>'+
									'<td><input type="hidden" name="qtypb'+totalItem+'" id="qtypb'+totalItem+'" value="'+qty_pb+'"/>'+qty_pb+'</td>'+
									'<td><input type="hidden" name="satuanpb'+totalItem+'" id="satuanpb'+totalItem+'" value="'+satuan_pb+'"/>'+satuan_pb+'</td>'+
									'<td><input type="hidden" name="keterangan'+totalItem+'" id="keterangan'+totalItem+'" value="'+deskripsi+'"/>'+deskripsi+'</td>'+
									'<td><input type="hidden" name="qtyterima'+totalItem+'" id="qtyterima'+totalItem+'" value="'+qty_terima+'"/>'+qty_terima+'</td>'+
									'<td><input type="hidden" name="satuanterima'+totalItem+'" id="satuanterima'+totalItem+'" value="'+satuan_terima+'"/>'+satuan_terima+'</td>'+
									'<td><input type="text" class="currency" name="qtypengkali'+totalItem+'" id="qtypengkali'+totalItem+'" value="" placeholder="1"</td>'+
									'<td><input type="number" step="0.0001" name="qtybayar'+totalItem+'" id="qtybayar'+totalItem+'" value="'+qty_bayar+'" placeholder="0" readonly/></td>'+
									'<td><input type="hidden" name="satuanbayar'+totalItem+'" id="satuanbayar'+totalItem+'" value="'+satuan_bayar+'">'+satuan_bayar+'</td>'+
									'<td><input type="hidden" name="penanda'+totalItem+'" id="penanda'+totalItem+'" value="1"/><input type="text" class="currency" name="hargasatuan'+totalItem+'" placeholder="0" id="hargasatuan'+totalItem+'" value="'+harga_satuan+'" readonly></td>'+
									'<td id="total_item'+totalItem+'" name="total_item'+totalItem+'"><input type="text" class="currency" name="sum_barang'+totalItem+'" id="sum_barang'+totalItem+'" readonly/></td>'+
									'<input type="hidden" name="terpi'+totalItem+'" id=="terpi'+totalItem+'" value="'+terpi+'"/>'+
									'<input type="hidden" name="ai_id'+totalItem+'" id=="ai_id'+totalItem+'" value="'+ai_id+'"/>'+
									'<input type="hidden" name="idsubkon" id="idsubkon" value="'+idsubkon+'"/>'+
								'</tr>');
							// $("#hargasatuan"+totalItem+"").autoNumeric('init', autoNumericOptionsRupiah);
							// $("#sum_barang"+totalItem+"").autoNumeric('init', autoNumericOptionsRupiah);
							//$("#qtybayar"+totalItem).autoNumeric('init', autoNumericOptionsRupiah);
							totalItem++;
						});
						$(".currency").autoNumeric('init',autoNumericOptionsRupiah);
						itemHandler();						
						$("#totalItem").val(totalItem);
					}
				}
			});
			$("#idSupplier").val(id_supp);			
			$.ajax({ 
				type : "GET",
				dataType:'json',
				url: "<?=site_url('api/downpayment_supplier_kontrak/list?h="+userHash+"&filter_dpay_supplier_id=')?>"+id_supp,
				cache : false,
				success : function(result, textStatus, xhr){					
					var jumDp=0;
					$('#selDp').html('<option disabled value="0" selected>Select your option</option>');
					if (result.success === true) {						
						$.each(result.data, function (index,arr) {
							// console.log(JSON.stringify(arr['id']));
							$('#selDp').append('<option value="' + arr['dp_id'] + '">' + arr['dpay_code'] + " - " + arr['supp_name'] +" - "+ arr['kont_name'] + " - "+ arr['dpay_date'] +'</option>');
							jumDp++;
						});
					}
					$("#jumDp").val(jumDp);
					$("#selDp").trigger("chosen:updated");
				}
			});
        }
    });
});

$('#selDp').change(function(){
	$("#idDp").val($("#selDp").val());	
	var idDp = $("#selDp").val();
	var amount=0;
	var count = 0;
	$.ajax({
		url: "<?=site_url('api/downpayment_detail/list?h="+userHash+"&filter_t-id=', NULL, FALSE)?>"+idDp,
		success: function(result, textStatus, xhr){			
			// if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;			
			// var xml = $(xmlDoc);
			// $arrDp = xml;
			var display=[];
			$.each(result.data, function(i, arr){
				if(arr['dp_id'] != ''){
					var dp_code = arr['dpay_code'];
					var dp_id = arr['dp_id'];
					var id_kp = arr['id_kp'];
					var dp_kont_name = arr['kont_name'];
					var dp_amount = arr['dpay_amount'];
					var dp_terbayar = arr['dpay_terbayar'];
					var dp_amount_ppn = arr['dpay_amount_ppn'];
					var dp_amount_pph = arr['dpay_amount_pph'];
					amount = parseFloat(dp_amount);
					var history = arr['pinvhis_dp'];					
					//var dp_sisa =$(val).find('sisa').text();
					//var dp_dipakai = $(val).find('dipakai').text();
					$(".noDataDp").hide();
					$("#downpayment tbody").append(
						'<tr>'+
							'<td><input type="hidden" name="code_dp'+totalItemDp+'" id="code_dp'+totalItemDp+'" value="'+dp_code+'"/>'+dp_code+'</td>'+
							'<td><input type="text" name="amount_dp'+totalItemDp+'" class="required currency" id="amount_dp'+totalItemDp+'" value="0" readonly /></div></td>'+
							'<td><input type="text" name="sisa_dp'+totalItemDp+'" class="required currency" id="sisa_dp'+totalItemDp+'" value="0" readonly /></td>'+
							'<td><input type="number" step="0.01" class="currency berubah" name="untuk_dp'+totalItemDp+'" max="'+dp_terbayar+'" id="untuk_dp'+totalItemDp+'" placeholder="0" value=""/></td>'+							
							'<input type="hidden" name="data_history_dp" id="data_history_dp" value="'+history+'"/>'+
							'<input type="hidden" name="nilai_ppn'+totalItemDp+'" id="nilai_ppn'+totalItemDp+'" value="'+dp_amount_ppn+'"/>'+
							'<input type="hidden" name="nilai_pph'+totalItemDp+'" id="nilai_pph'+totalItemDp+'" value="'+dp_amount_pph+'"/>'+
							'<input type="hidden" name="id_kp'+totalItemDp+'" id="id_kp'+totalItemDp+'" value="'+id_kp+'"/>'+
							'<input type="hidden" name="id_dp'+totalItemDp+'" id="id_dp'+totalItemDp+'" value="'+dp_id+'"/>'+
						'</tr>');
					$('#amount_dp'+totalItemDp+'').autoNumeric('init', autoNumericOptionsRupiah);
					$('#sisa_dp'+totalItemDp+'').autoNumeric('init', autoNumericOptionsRupiah);
					var dp_sisa=0;
					var dp_dipakai=0;
					$.ajax({
						url: "<?=site_url('api/downpayment_used/list?h="+userHash+"&filter_pinvhis_dp="+idDp+"&filter_pinvhis_supplier="+$("#idSupplier").val()+"', NULL, FALSE)?>",
						method: "GET",
						dataType: "JSON",
						success: function(result2, textStatus, xhr){
							// if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;				
							// var xml = $(xmlDoc);
							// $arrDp = xml;							
							var display=[];
							if (result2.success === true) {
								$.each(result2.data, function(index, arr2){																		
									var latestRow = $('#downpayment tbody tr:last');
									dp_dipakai = arr2['total_terpakai'];
									dp_sisa = amount - dp_dipakai;									
									if(dp_dipakai>0){										
										latestRow.find('input[name^="sisa_dp"]').autoNumeric('set', dp_sisa);
									}
									else if(dp_sisa == 0){
										latestRow.find('input[name^="untuk_dp"]').val(amount);
										latestRow.find('input[name^="untuk_dp"]').attr("readonly");
									}
									else{
										latestRow.find('input[name^="sisa_dp"]').autoNumeric('set', dp_sisa);
									}
									count++;
								});
							}
							else{
								dp_sisa = amount;								
								latestRow.find('input[name^="sisa_dp"]').autoNumeric('set', amount);
								if(dp_sisa == 0){
									latestRow.find('input[name^="untuk_dp"]').val(amount);
									latestRow.find('input[name^="untuk_dp"]').attr("readonly");
								}
								count++;
							}
						}
					});
					$('#amount_dp'+totalItemDp+'').autoNumeric('set', amount);
					totalItemDp++;
					$("#jumlahDp").val(totalItemDp);					
				}
			});
		}
	});
});

$('#selPlusCost').change(function(){
	var idAcc = $('#selPlusCost').val();	
	$.ajax({
		url: "<?=site_url('api/jw_account/"+idAcc+"/id?h="+userHash+"', NULL, FALSE)?>",
		method: "GET",
		dataType: "JSON",
		cache: false,	
		success: function(result){
		if (result.success === true){			
			// if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
			// console.log(xmlDoc);
			// var xml = $(xmlDoc);
			// $arrSelectedCost = xml;
			var display=[];
			data = result.data;
			// $.map(xml.find('AddCost').find('item'),function(val,i){
				var id = data['id'];
				var code = data['acco_code'];
				var name = data['acco_name'];
				$(".noDataTambahan").hide();
				$("#selectedItemsBonus tbody").append(
					'<tr>'+
						'<td class="cb"><input type="checkbox" name="cbDeleteX'+totalItemTambahan+'"/></td>'+
						'<input type="hidden" name="id_tambahan'+totalItemTambahan+'" id="id_tambahan'+totalItemTambahan+'" value="'+id+'"/>'+
						'<input type="hidden" name="code_tambahan'+totalItemTambahan+'" id="code_tambahan'+totalItemTambahan+'" value="'+code+'"/>'+
						'<td><input type="hidden" name="name_tambahan'+totalItemTambahan+'" id="name_tambahan'+totalItemTambahan+'" value="'+name+'"/>'+name+'</td>'+
						'<td><input type="text" class="currency berubah" placeholder="0" name="amount_plus'+totalItemTambahan+'" id="amount_plus'+totalItemTambahan+'" value=""/></td>'+
						'<input type="hidden" id="total_item_tambahan" name="total_item_tambahan">'+
						'<input type="hidden" name="penanda_amount_tambahan'+totalItemTambahan+'" id="penanda_amount_tambahan'+totalItemTambahan+'" value="1"/>'+
					'</tr>');
				$("#amount_plus"+totalItemTambahan).autoNumeric('init', autoNumericOptionsRupiah);
				totalItemTambahan++;
			// });
			$('#total_item_tambahan').val(totalItemTambahan);
		}
		}
	});
});

$('#selSupp').change(function(){
    $.ajax({ 
        type : "POST",
        dataType:'json',
        url: "<?=site_url('purchase_ajax/getKPAutoComplete')?>/" + $('#selSupp').val(),
        cache : false,
        success : function(result){
            $('#selKP').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result, function (index,arr) {
                $('#selKP').append('<option value="' + arr['id'] + '">[' + arr['kopb_code'] + '] ' + arr['amount'] + '</option>');
            });
            $("#selKP").trigger("chosen:updated");
        }
    });
});

// Element Not Found
$('#selKP').change(function(){
    $.ajax({ 
        type : "POST",
        dataType:'json',
        url: "<?=site_url('purchase_ajax/getDPbyKPID')?>/" + $('#selKP').val(),
        cache : false,
        success : function(result){
            $('#selKP').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result, function (index,arr) {
                $('#selKP').append('<option value="' + arr['id'] + '">[' + arr['kopb_code'] + '] ' + arr['amount'] + '</option>');
            });
            $("#selKP").trigger("chosen:updated");
        }
    });
});

$("#txtTax").change(function(){
	var totalwithdisc=$("#subTotalWithDisc").autoNumeric('get');
	var totalTax=parseFloat(totalwithdisc)+parseFloat(totalwithdisc*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});

var $arrSelectedCost;
var selectedCost;



$("#txtNewItemBonus").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_invoice_ajax/getAllAddCostItem', NULL, FALSE)?>/"+$("#txtNewItemBonus").val(),
			beforeSend: function() {
				$("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemBonus").html('');
			},
			success: function(data){
				if (typeof data === 'string' || data instanceof String) var xmlDoc = $.parseXML(data); else var xmlDoc = data;
                // console.log($("#txtNewItemBonus").val());
                // console.log(JSON.stringify(xmlDoc));
				var xml = $(xmlDoc);
				$arrSelectedCost = xml;
				var display=[];
				$.map(xml.find('AddCost').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('kategori').text();
					var harga = $(val).find('amount').text();
					display.push({label:strName, value:strName,id:intID});
                });
                response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedCost = $.grep($arrSelectedCost.find('AddCost').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItemBonus;
		if(numberItemBonus=='0'){
			$("#selectedItemsBonus tbody").html('');
		}
        var name = $(selectedCost).find('kategori').text();
		var jumlah = $(selectedCost).find('amount').text();
		$("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
                '<td id="name'+i+'"><input type="hidden" name="namaTambahan'+i+'" id="namaTambahan'+i+'" value="'+name+'">'+name+'</td>'+
                '<td class="qty"><input type="hidden" name="jumlahTambahan'+i+'" id="jumlahTambahan'+i+'" value="'+jumlah+'">'+jumlah+'</td>'+
            '</tr>');

		totalItemBonus++;
		numberItemBonus++;
		$("#totalItemBonus").val(totalItemBonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
	}
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItemBonus--;
	if(numberItemBonus=='0'){
		$("#selectedItemsBonus tbody").append(
			'<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
		return value != $("#idItemBonusX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("change","input[type='number']",function(){
    //change additional cost here
});

function itemHandler() {
	$("#selectedItems input[type='text'][name^='qtypengkali']").on("keyup",function(e){
		if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8) {
			var at=this.name.substring(this.name.indexOf('i')+1,this.name.length);	
			var currentEl = $("#qtypengkali"+at);
			var pengkali = currentEl.autoNumeric('get');
			if(pengkali == '' || pengkali == null){
				var pengkali = 0;
			}
			var bayar = parseFloat(currentEl.parents('tr').find('#qtybayar'+at).val());
			var sat_bayar = parseFloat(currentEl.parents('tr').find('#hargasatuan'+at).autoNumeric('get'));
			var subtotalItem = pengkali*bayar*sat_bayar;			
			currentEl.parents('tr').find("#sum_barang"+at).autoNumeric('set', subtotalItem);
			calculateTotal($(this));
		}
	});
}

function calculateTotal(currentElement) {
	var total = 0;
	$("input[name^=sum_barang]").each(function(i, el){
		var $subtotal = parseFloat($(el).autoNumeric('get'));
		if (!isNaN($subtotal)) total = total + $subtotal;
	});
	$("#subTotalNoTax").autoNumeric('set',total);
	berubah();
}


$('.chosen').chosen({search_contains: true});

</script>