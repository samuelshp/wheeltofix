<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-folder"></i> Purchase Invoice', 'link' => site_url('purchase_invoice/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmAddInvoice" id="frmAddInvoice" method="post" action="<?=site_url('purchase_invoice', NULL, FALSE)?>" class="frmShop">
        <div class="row">
            
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbdata')?> </h3>
                    </div>
                    <div class="panel-body">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?>
                        <input type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
                    </div>
                    <div class="panel-body">
                        <h4 class="PB"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectacceptance')?> <a href="<?=site_url('acceptance', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                        <select type="text" name="BPBID" id="selBPB" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectbpb')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php
                                foreach($arrBPB as $e){
                                    echo "<option value='".$e['id']."'>".$e['acce_code']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="panel-body">
                        <!-- Data BPB -->
                        <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbowner')?></label><input id="txtOwner" type="text" class="form-control" readonly /></div></div> -->
                        <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?></label><input id="txtProject" type="text" class="form-control" readonly /></div></div> -->
                        <div class="form-group"><div class="input-group"><label class="input-group-addon">Nama Proyek</label><input id="txtKodeKontrak" type="text" class="form-control" readonly style="z-index:0;"/></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplier')?></label><input id="txtSupplier" type="text" class="form-control" readonly style="z-index:0;"/></div></div>
                        <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-dateacceptance')?></label><input id="txtTanggalBPB" type="text" class="form-control" readonly style="z-index:0;"/></div></div>
                    </div>
                </div>
            </div>

            <!-- <div id="hide_sementara">
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title Supplier"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplierdata')?> </h3>
                        </div>
                        <div class="panel-body">
                            <h4 class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>" ><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                            <select type="text" name="supplierID" id="selSupp" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>">
                                <option value="0" disabled selected>Select your option</option>
                                <?php foreach($arrSupplier as $e) { ?>
                                    <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="panel-body">
                            <h4 class="KP"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectkp')?> <a href="<?=site_url('kontrak_pembelian', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
                            <select type="text" name="KPID" id="selSupp" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectkp')?>">
                                <option value="0" disabled selected>Select your option</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                        <div class="panel-body">
                            Dibuat
                            <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?=date('Y/m/d')?>" class="form-control" readonly /></div></div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>

        <!-- Selected Items -->
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
            <div class="panel-body">

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-material')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypb')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpb')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?> PB</th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtyter')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-satuanter')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-kali')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtypay')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-unitpay')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-priceunitpay')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr class="info"><td class="noData" colspan="13"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        
        <div class="row">

            <!-- Biaya -->
            <div class="col-md-4"></div>
            
            <!-- <div class="col-md-4"><div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-costadd')?></h3>
                </div>
                <div class="panel-body">
                    <select style="max-width:550px;" type="text" id="selPlusCost" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                        <?php foreach($arrPlusCost as $e) { ?>
                            <option value="<?=$e['id']?>"><?=$e['acco_name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                        <thead>
                        <tr>
                            <!--<th class="cb"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')*/?></th>
                            <th class="Supplier"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-cost')?></th>
                            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="info"><td class="noDataTambahan" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        </tbody>
                    </table>
                </div>

                </div>
                </div> Table Biaya
            </div> -->

            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-downpayment')?></h3></div>
                    
                    <div class="panel-body">
                        <select style="max-width:550px;" type="text" id="selDp" class="form-control chosen" placeholder="Select DP">
                            <option value="0" disabled selected>Select your option</option>
                        </select>
                    </div>
                    <input type="hidden" name="jumlahDp" id="jumlahDp"/>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover" id="downpayment">
                                <thead>
                                    <tr>
                                        <th>No. DP</th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-amount')?></th>
                                        <th>Sisa</th>
                                        <th>Dipakai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr class="info"><td class="noDataDp" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                                </tbody>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalNoTax" class="form-control required currency" id="subTotalNoTax" value="0" readonly /></div>
                            </div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
                            <div class="col-sm-6"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" placeholder="0" class="form-control required currency berubah" id="dsc4Item" value="" /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle">Potongan DP</div>                            
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="totalDownPayment" class="form-control required currency" id="totalDownPayment" value="0" readonly /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totaldpp')?></div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="totalDpp" class="form-control required currency" id="totalDpp" value="0" readonly /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <p class="spacer">&nbsp;</p>
                        <p class="spacer">&nbsp;</p>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle">PPn</div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><input placeholder="0" type="text" name="txtPpn" id="txtPpn" class="form-control berubah required qty" maxlength="3" value="" /><label class="input-group-addon">%</label></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountppn')?></div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="amountPPN" class="form-control required currency" id="amountPPN" value="0" readonly /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pph')?></div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><input type="text" name="txtPPH" placeholder="0" id="txtPPH" class="form-control berubah required qty" maxlength="3" value="" /><label class="input-group-addon">%</label></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-amountpph')?></div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="amountPPH" class="form-control required currency" id="amountPPH" value="0" readonly /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle">Biaya Tambahan</div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input placeholder="0" type="text" name="txtCostAdd" id="txtCostAdd" class="form-control required currency" value="0" readonly/></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
                            <div class="col-sm-6 tdDesc form-group"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" placeholder="0" name="grandTotal" class="form-control required currency" id="grandTotal" value="0" readonly /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle">Nilai Pembulatan</div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" step="0.01" min="-10000" max="10000" name="amountPembulatan" class="form-control required" id="amountPembulatan" placeholder="0" /></div></div>
                        </div>
                        <p class="spacer">&nbsp;</p>
                        <div class="row">
                            <div class="col-sm-6 tdTitle">Total Dengan Pembulatan</div>
                            <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="amountTotalPembulatan" class="form-control required currency" id="amountTotalPembulatan" placeholder="0" readonly/></div></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-group"><button type="submit" name="smtMakeInvoice" value="Make Invoice" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
    </div>

<input type="hidden" id="jumDp" name="jumDp"/>
<input type="hidden" id="idDp" name="idDp"/>
<input type="hidden" id="idSupplier" name="idSupplier"/>
<input type="hidden" id="idOrderPembelian" name="idOrderPembelian"/>
<input type="hidden" id="idAcceptance" name="idAcceptance"/>
<input type="hidden" id="kpID" name="kpID"/>
<input type="hidden" id="total_tanpa_ppn" class="currency" name="total_tanpa_ppn"/>
<input type="hidden" id="totalItem" name="totalItem"/>
<input type="hidden" id="totalItemDp" name="totalItemDp"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
<input type="hidden" id="total_ppn_dp" name="total_ppn_dp"/>
<input type="hidden" id="total_pph_dp" name="total_pph_dp"/>


</form></div>




