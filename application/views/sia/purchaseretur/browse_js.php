<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({dateFormat: "yy/mm/dd"});
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".jwTime").datetimepicker({ dateFormat: "yy/mm/dd", timeOnly: true });

$('a[id*="printtax"]').click(function(){
    var at=this.id.substring(this.id.indexOf('Y')+1,this.id.length);
    var link=$(this).attr("href");
    var text = prompt("Tanggal Nota:", "");
    $.ajax({
        url: "<?=site_url('purchase_retur_ajax/checkPrintLog', NULL, FALSE)?>/" + at,
        async:false,
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var fakturnumber=xml.find('Kode').find('prlo_invoice_number').text();
            if(fakturnumber!=''){
                link=link+"&date="+text+"&kode="+fakturnumber+"&first=0";
            }else{
                var kode = prompt("Kode Faktur Pajak:", "");
                link=link+"&date="+text+"&kode="+kode+"&first=1";
            }
        }
    });
    $(this).attr("href",link);
});
});</script>