<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-repeat"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-purchasereturn'), 'link' => site_url('purchase_retur/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
	<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-lastpurchasereturn')?></label>
	<select name="selJumpTo" class="form-control">
		<option value="">- Jump To -</option><?php
		if(!empty($arrPurchaseList)) foreach($arrPurchaseList as $e):
			$strListTitle = ' title="'.$e['supp_address'].', '.$e['supp_city'].'"'; ?>
		<option value="<?=site_url('purchase_retur/view/'.$e['id'], NULL, FALSE)?>">[<?=$e['prcr_code']/*formatDate2($e['prcr_date'],'d/m/Y')*/?>] <?=$e['supp_name']?></option><?php
		endforeach; ?>
	</select>
</div></div>

<?php $suppid = $arrPurchaseData['supp_id']; ?>
<?php $warehouseid = $arrPurchaseData['prcr_warehouse_id']; ?>
<div class="col-xs-12"><form name="frmChangePurchase" id="frmChangePurchase" method="post" action="<?=site_url('purchase_retur/view/'.$intPurchaseID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-repeat"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-returdata')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-suppliername')?></div>
			<div class="col-xs-8"><?=$arrPurchaseData['supp_name']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></div>
			<div class="col-xs-8"><?=$arrPurchaseData['supp_address']?>, <?=$arrPurchaseData['supp_city']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></div>
			<div class="col-xs-8"><?=$arrPurchaseData['supp_phone']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehouse')?></div>
			<div class="col-xs-8"><?=$arrPurchaseData['ware_name']?></div>
			<p class="spacer">&nbsp;</p>
		</div>
	</div></div>

    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
			<div class="col-xs-8"><?=$arrPurchaseData['prcr_code']?></div>
			<p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
            <div class="col-xs-8"><?=formatDate2($arrPurchaseData['prcr_date'],'d F Y')?></div>
			<p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')?></div>
            <div class="col-xs-8"><?=formatDate2($arrPurchaseData['prcr_exp_date'],'d F Y')?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?></div>
            <div class="col-xs-8"><?=formatDate2($arrPurchaseData['prcr_end_date'],'d F Y')?></div>
            <p class="spacer">&nbsp;</p>
		</div>
	</div></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
    <div class="panel-body">

    	<?php if($bolBtnEdit): ?>
		<div class="form-group"><div class="input-group addNew">
			<input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
		</div></div><?php
        endif; ?>

        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
			<thead>
			<tr>
				<?php if($bolBtnEdit): ?><th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php endif; ?>  
				<th>Stock</th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
				<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
			</tr>
			</thead>
			<tbody><?php
			$i = 0;
			$intTotalItem = 0;
			$iditemawal='';
			$idpurchaseawal='';
			if(!empty($arrPurchaseItem)):
				foreach($arrPurchaseItem as $e):
					$iditemawal=$iditemawal.$e['product_id'].'-';
					$idpurchaseawal=$idpurchaseawal.$e['id'].'-';
					$intTotalItem++; ?>
					<tr><?php
					if($bolBtnEdit): ?>
						<td class="cb"><input type="checkbox" name="cbDeleteAwal[<?=$e['id']?>]" /></td><?php
					endif; ?>
					<td><label id="maxStrAwal<?=$e['id']?>"></label></td>
					<td class="qty"><?php
						if($bolBtnEdit): //if yes ?>
						<div class="form-group">
							<div class="col-xs-4"><input type="text" name="txtItem1Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity1']?>" class="required number form-control input-sm awal" id="txtItem1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit1'])?>" title="<?=formatUnitName($e['prri_unit1'])?>" /></div>
							<div class="col-xs-4"><input type="text" name="txtItem2Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity2']?>" class="required number form-control input-sm awal" id="txtItem2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit2'])?>" title="<?=formatUnitName($e['prri_unit2'])?>" /></div>
							<div class="col-xs-4"><input type="text" name="txtItem3Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity3']?>" class="required number form-control input-sm awal" id="txtItem3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit3'])?>" title="<?=formatUnitName($e['prri_unit3'])?>" /></div>
							<input type="hidden" name="idItem<?=$e['id']?>" value="<?=$e['product_id']?>" id="idItem<?=$e['id']?>" />
							<input type="hidden" id="max1Item<?=$e['id']?>"  />
							<input type="hidden" id="max2Item<?=$e['id']?>"  />
							<input type="hidden" id="max3Item<?=$e['id']?>"  />
						</div><?php

						else: echo
							$e['prri_quantity1'].' '.formatUnitName($e['prri_unit1']).' + '.
							$e['prri_quantity2'].' '.formatUnitName($e['prri_unit2']).' + '.
							$e['prri_quantity3'].' '.formatUnitName($e['prri_unit3']);
						endif; ?>
							<input type="hidden" name="idProiID[<?=$e['id']?>]" value="<?=$e['id']?>" />
							<input type="hidden" name="txtItem1Conv[<?=$e['id']?>]" value="<?=$e['prri_conv1']?>" id="txtItem1Conv<?=$e['id']?>" />
							<input type="hidden" name="txtItem2Conv[<?=$e['id']?>]" value="<?=$e['prri_conv2']?>" id="txtItem2Conv<?=$e['id']?>" />
							<input type="hidden" name="txtItem3Conv[<?=$e['id']?>]" value="<?=$e['prri_conv3']?>" id="txtItem3Conv<?=$e['id']?>" />
                            <input type="hidden" id="itemAt<?=$e['product_id']?>" value="<?=$e['id']?>" />
					</td>
					<td><b><?="(".$e['prod_code'].") ".$e['strName']?></b></td>
					<td class="pr"><?php
						if($bolBtnEdit): ?>
							<div class="input-group"><label class="input-group-addon"><?=str_replace(' 0','',setPrice('','USED'))?></label><input type="text" name="txtItemPrice[<?=$e['id']?>]" class="required currency form-control input-sm awal" id="txtItemPrice<?=$e['id']?>" value="<?=$e['prri_price']?>" /></div><?php

						else:
							echo setPrice($e['prri_price'],'USED');
						endif; ?>
					</td>
					<td class="disc"><?php
						if($bolBtnEdit): ?>
						<div class="form-group">
							<div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem1Disc[<?=$e['id']?>]" class="required number form-control input-sm awal" id="txtItem1Disc<?=$e['id']?>" value="<?=$e['prri_discount1']?>" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>
							<div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem2Disc[<?=$e['id']?>]" class="required number form-control input-sm awal" id="txtItem2Disc<?=$e['id']?>" value="<?=$e['prri_discount2']?>" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>
							<div class="col-xs-4"><div class="input-group"><input type="text" name="txtItem3Disc[<?=$e['id']?>]" class="required number form-control input-sm awal" id="txtItem3Disc<?=$e['id']?>" value="<?=$e['prri_discount3']?>" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>
						</div><?php

						else:
							echo $e['prri_discount1']." % + ".$e['prri_discount2']." % + ".$e['prri_discount3']." %";
						endif; ?>
					</td>
					<td class="subTotal" id="subTotal<?=$e['id']?>"><?=$e['prri_subtotal']?></td>
					</tr><?php

					$i++;
				endforeach;

				$iditemawal = $iditemawal.'0';
				$idpurchaseawal = $idpurchaseawal.'0';
			else: ?>
				<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
			endif; ?>
			</tbody>
		</table></div>

        <div class="row">
            <div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?=$intPurchaseTotal?></div>
        </div>
        <p class="spacer">&nbsp;</p>
        <div class="row">
            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
            <div class="col-sm-2 form-group"><?php
                if($bolBtnEdit): ?>
                    <div class="input-group"><input type="text" name="dsc4" value="<?=$arrPurchaseData['prcr_discount']?>" id="dsc4" class=" subTotal required currency form-control" /></div><?php

                else:
                    echo $arrPurchaseData['prcr_discount'];
                endif; ?></div>
            <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalWithDisc"><?=$arrPurchaseData['subtotal_discounted']?></div>
        </div>
        <p class="spacer">&nbsp;</p><!--
        <input class="required number qty" type="hidden" name="txtTax" value="<?/*=$arrPurchaseData['prcr_tax']*/?>" id="txtTax" class="form-control required number" />-->
        <div class="row">
            <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
            <div class="col-sm-2 tdDesc form-group"><?php
                if($bolBtnEdit): ?>
                    <div class="input-group"><input  type="text" name="txtTax" value="<?=$arrPurchaseData['prcr_tax']?>" maxlength="3" id="txtTax" class="form-control required number" /><label class="input-group-addon">%</label></div><?php

                else:
                    echo $arrPurchaseData['prcr_tax']." %";
                endif; ?></div>
            <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalTax"><?=$arrPurchaseData['subtotal_taxed']?></div>
        </div>
        <p class="spacer">&nbsp;</p>
    </div><!--/ Table Selected Items -->
</div>

<div class="row">

    <div class="col-md-4"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
		<div class="panel-body"><?php
			if($bolBtnEdit): ?>
				<div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrPurchaseData['prcr_description']?></textarea></div><?php
			else: ?>
                <input type="hidden" name="txaDescription" value="<?=$arrPurchaseData['prcr_description']?>" />
				<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrPurchaseData['prcr_description']?></div><?php
			endif; ?>
			<div class="form-group">
				<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')?></label>
				<input type="text" name="txtProgress" value="<?=$arrPurchaseData['prcr_progress']?>" maxlength="64" class="form-control" />
			</div>
		</div>
	</div></div>

    <!-- Bonus -->
    <div class="col-md-8"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
            <div class="panel-body"><?php

                if($bolBtnEdit): ?>
                <div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                    </div></div><?php
                endif; ?>

                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                        <thead>
                        <tr><?php
                            if($bolBtnEdit): ?>
                                <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th><?php
                            endif; ?>
                            <th>Stock</th>
                            <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
                        </tr>
                        </thead>
                        <tbody><?php
                        $i = 0;
                        $intTotalBonusItem = 0;
                        $iditembonusawal='';
                        $idpurchasebonusawal='';
                        if(!empty($arrPurchaseBonusItem)):
                            foreach($arrPurchaseBonusItem as $e):
                                $iditembonusawal=$iditembonusawal.$e['product_id'].'-';
                                $idpurchasebonusawal=$idpurchasebonusawal.$e['id'].'-';
                                $intTotalBonusItem++; ?>
                                <tr><?php
                                if($bolBtnEdit): ?>
                                    <td class="cb"><input type="checkbox" name="cbBonusAwal[<?=$e['id']?>]"/></td><?php
                                endif; ?>
                                <td><label id="maxStrBonusAwal<?=$e['id']?>"></label></td>
                                <td class="qty"><?php
                                    if($bolBtnEdit): //if yes ?>
                                        <div class="form-group">
                                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus1Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity1']?>" class="required number form-control input-sm awalbonus" id="txtItemBonus1Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit1'])?>" title="<?=formatUnitName($e['prri_unit1'])?>" /></div></div>
                                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus2Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity2']?>" class="required number form-control input-sm awalbonus" id="txtItemBonus2Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit2'])?>" title="<?=formatUnitName($e['prri_unit2'])?>" /></div></div>
                                        <div class="col-xs-4"><div class="form-group"><input type="text" name="txtItemBonus3Qty[<?=$e['id']?>]" value="<?=$e['prri_quantity3']?>" class="required number form-control input-sm awalbonus" id="txtItemBonus3Qty<?=$e['id']?>" placeholder="<?=formatUnitName($e['prri_unit3'])?>" title="<?=formatUnitName($e['prri_unit3'])?>" /></div></div>
                                        <input type="hidden" value="<?=$e['product_id']?>" id="idItemBonus<?=$e['id']?>" />
                                        <input type="hidden" id="max1ItemBonus<?=$e['id']?>"  />
                                        <input type="hidden" id="max2ItemBonus<?=$e['id']?>"  />
                                        <input type="hidden" id="max3ItemBonus<?=$e['id']?>"  />
                                        <input type="hidden" id="itemBonusAt<?=$e['product_id']?>" value="<?=$e['id']?>" />
                                        </div><?php

                                    else:
                                        echo $e['prri_quantity1'].' '.formatUnitName($e['prri_unit1']).' + '.$e['prri_quantity2'].' '.formatUnitName($e['prri_unit2']).' + '.$e['prri_quantity3'].' '.formatUnitName($e['prri_unit3']);
                                    endif; ?>
                                    <input type="hidden" value="<?=$e['id']?>" name="idProiIDBonus[<?=$e['id']?>]" />
                                    <input type="hidden" name="txtItemBonus1Conv[<?=$e['id']?>]" value="<?=$e['prri_conv1']?>" id="txtItemBonus1Conv<?=$e['id']?>" />
                                    <input type="hidden" name="txtItemBonus2Conv[<?=$e['id']?>]" value="<?=$e['prri_conv2']?>" id="txtItemBonus2Conv<?=$e['id']?>" />
                                    <input type="hidden" name="txtItemBonus3Conv[<?=$e['id']?>]" value="<?=$e['prri_conv3']?>" id="txtItemBonus3Conv<?=$e['id']?>" />
                                </td>
                                <td><b><?="(".$e['prod_code'].") ".$e['strName']?></b></td>
                                </tr><?php
                                $i++;
                            endforeach;
                            $iditembonusawal=$iditembonusawal.'0';
                            $idpurchasebonusawal=$idpurchasebonusawal.'0';
                        else: ?>
                            <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                        endif; ?>
                        </tbody>
                    </table></div>

            </div><!--/ Table Bonus -->
        </div></div>
</div>

<?php
    $rawStatus = $arrPurchaseData['prcr_rawstatus'];            
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>
    <?php if($bolBtnPrint && !empty($arrPurchaseData['supp_pkp'])): ?><button id="printTaxY<?=$e['id']?>" type="submit" name="subPrintTax" value="Print Tax" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></button><?php endif; ?>


<input type="hidden" id="IDSupp" value="<?=$suppid?>"/>
<input type="hidden" id="WarehouseID" name="WarehouseID" value="<?=$warehouseid?>"/>
<input type="hidden" id="idItemAwal" name="idItemAwal" value="<?=$iditemawal?>"/>
<input type="hidden" id="idPurchaseAwal" name="idPurchaseAwal" value="<?=$idpurchaseawal?>"/>
<input type="hidden" id="idItemBonusAwal" name="idItemBonusAwal" value="<?=$iditembonusawal?>"/>
<input type="hidden" id="idPurchaseBonusAwal" name="idPurchaseBonusAwal" value="<?=$idpurchasebonusawal?>"/>
<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>

</form></div>

