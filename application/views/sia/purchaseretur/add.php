<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-repeat"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-purchasereturn'), 'link' => site_url('purchase_retur/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12"><form name="frmAddPurchase" id="frmAddPurchase" method="post" action="<?=site_url('purchase_retur', NULL, FALSE)?>" class="frmShop">
	<div class="row">
		<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-repeat"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-returdata')?></h3></div>
			<div class="panel-body">
				<h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?> <a href="<?=site_url('adminpage/table/add/36', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
				<div class="input-group"><input type="text" id="selSupp" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectsupplier')?>" /><label class="input-group-addon" id="loadSupp"></label></div>
				<h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?> <a href="<?=site_url('adminpage/table/add/13', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
				<div class="form-group"><div class="input-group"><input type="text" id="txtWarehouse" name="txtWarehouse" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>"/><label class="input-group-addon" id="loadWarehouse"></label></div></div>
			</div>
		</div></div>

		<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
			<div class="panel-body">
				<!-- Dibuat -->
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
				<!-- Jatuh Tempo -->
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?></label><input type="text" name="txtEndDate" value="<?php if(!empty($strEndDate)) echo $strEndDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
				<!-- Expired Date -->
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-expdate')?></label><input type="text" name="txtExpDate" value="<?php if(!empty($strExpDate)) echo $strExpDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
			</div>
		</div></div>
	</div>

	<!-- Selected Items -->
	<div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseitems')?></h3></div>
		<div class="panel-body">

			<div class="form-group"><div class="input-group addNew">
				<input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
			</div></div>

			<div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
				<thead>
				<tr>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
					<th>Stock</th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
				</tr>
				</thead>
				<tbody>
				<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
				</tbody>
			</table></div>

			<div class="row">
				<div class="col-sm-10 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
				<div class="col-sm-2 tdDesc" id="subTotalNoTax">0</div>
			</div>
			<p class="spacer">&nbsp;</p>
			<div class="row">
				<div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></div>
				<div class="col-sm-2"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="dsc4" class="form-control required currency" id="dsc4Item" value="0" /></div></div>
				<div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithdisc')?></div>
				<div class="col-sm-2 tdDesc" id="subTotalWithDisc">0</div>
			</div>
			<p class="spacer">&nbsp;</p><!--
            <input type="hidden" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" />-->
			<div class="row">
				<div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
				<div class="col-sm-2 tdDesc form-group"><div class="input-group"><input type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
				<div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
				<div class="col-sm-2 tdDesc" id="subTotalTax">0</div>
			</div>
			<p class="spacer">&nbsp;</p>
		</div><!--/ Table Selected Items -->
	</div>

    <div class="row">
        <div class="col-md-4"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
            <div class="panel-body">
                <div class="form-group"><textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
            </div>
        </div></div>

        <!-- Bonus -->
        <div class="col-md-8"><div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus-square-o"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-itemsbonus')?></h3></div>
                <div class="panel-body">

					<div class="form-group"><div class="input-group addNew">
                        <input type="text" id="txtNewItemBonus" name="txtNewItemBonus" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItemBonus"></label>
                    </div></div>

                    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
                            <thead>
                            <tr>
                                <th class="cb"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?></th>
                                <th>Stock</th>
                                <th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                            </tbody>
                        </table></div>

                </div><!--/ Table Bonus -->
            </div></div>
    </div>

	<div class="form-group"><button type="submit" name="smtMakePurchase" value="Make Purchase" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

<input type="hidden" id="IDSupp" name="IDSupp"/>
<input type="hidden" id="WarehouseID" name="WarehouseID"/>
<input type="hidden" id="totalItem" name="totalItem"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>

</form></div>