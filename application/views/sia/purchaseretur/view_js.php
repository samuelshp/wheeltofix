<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
var selectedItemBefore=$('#idItemAwal').val().split("-");
var selectedPurchasedBefore=$('#idPurchaseAwal').val().split("-");
var selectedItemBonusBefore=$('#idItemBonusAwal').val().split("-");
var selectedPurchasedBonusBefore=$('#idPurchaseBonusAwal').val().split("-");
var numberitemawal=selectedItemBefore.length-1;
var totalitem=0;
var numberitem=0;
var totalitembonus=0;
var numberitembonus=0;
var numberitembonusawal=selectedItemBonusBefore.length-1;

numberitemawal=$("#totalItemAwal").val();
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmChangePurchase").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});
$("#frmChangePurchase").submit(function() {
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	$('.subTotal').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});


$.ajax({
	url: "<?=site_url('inventory_ajax/getProductStockNew', NULL, FALSE)?>/" + $('#idItemAwal').val()+"/" + $('#idPurchaseAwal').val()+"/" + $("#WarehouseID").val(),
	success: function(data){
		var xmlDoc = $.parseXML(data);
		var xml = $(xmlDoc);
		$arrSelectedPO = xml;
		$.map(xml.find('Stock').find('item'),function(val,i){
			var max=$(val).find('stock').text();
			var id=$(val).find('id').text();
			for(var zz=1;zz<=3;zz++){
				if($("#txtItem"+zz+"Conv"+id).val()!=''){
					var hasil=Math.floor(parseInt(max)/parseInt($("#txtItem"+zz+"Conv"+id).val()));
					if(zz==1){
						var conv = xml.find('prod_conv1').text();
						if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
							$("#maxStrAwal"+id).text(hasil);
							$("#max1Item"+id).val(hasil);
			            } else {
			                $("#maxStrAwal"+id).text('0');
							$("#max1Item"+id).val('0');
			            }
					}
					if(zz==2){
						conv = xml.find('prod_conv2').text();
			            if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItem"+zz+"Conv"+id).val()));
							$("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
							$("#max2Item"+id).val(hasil);
			            } else {
			                $("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | 0");
							$("#max2Item"+id).val('0');
			            }
					}
					if(zz==3){
						$("#maxStrAwal"+id).text($("#maxStrAwal"+id).text()+" | "+ hasil);
						$("#max3Item"+id).val(hasil);
					}
				}
			}
		});
	}
});

$.ajax({
    url: "<?=site_url('inventory_ajax/getProductStockNew', NULL, FALSE)?>/" + $('#idItemBonusAwal').val()+"/" + $('#idPurchaseBonusAwal').val()+"/" + $("#WarehouseID").val(),
    success: function(data){
        var xmlDoc = $.parseXML(data);
        var xml = $(xmlDoc);
        $arrSelectedPO = xml;
        $.map(xml.find('Stock').find('item'),function(val,i){
            var max=$(val).find('stock').text();
            var id=$(val).find('id').text();
            for(var zz=1;zz<=3;zz++){
				if($("#txtItemBonus"+zz+"Conv"+id).val()!=''){
					var hasil=Math.floor(parseInt(max)/parseInt($("#txtItemBonus"+zz+"Conv"+id).val()));
					if(zz==1){
						var conv = xml.find('prod_conv1').text();
						if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItemBonus"+zz+"Conv"+id).val()));
							$("#maxStrBonusAwal"+id).text(hasil);
							$("#max1ItemBonus"+id).val(hasil);
			            } else {
			                $("#maxStrBonusAwal"+id).text('0');
							$("#max1ItemBonus"+id).val('0');
			            }
					}
					if(zz==2){
						conv = xml.find('prod_conv2').text();
			            if(parseInt(conv) > 1) {
							max=parseInt(max)-(parseInt(hasil)*parseInt($("#txtItemBonus"+zz+"Conv"+id).val()));
							$("#maxStrBonusAwal"+id).text($("#maxStrBonusAwal"+id).text()+" | "+ hasil);
							$("#max2ItemBonus"+id).val(hasil);
			            } else {
			                $("#maxStrBonusAwal"+id).text($("#maxStrBonusAwal"+id).text()+" | 0");
							$("#max2ItemBonus"+id).val('0');
			            }
					}
					if(zz==3){
						$("#maxStrBonusAwal"+id).text($("#maxStrBonusAwal"+id).text()+" | "+ hasil);
						$("#max3ItemBonus"+id).val(hasil);
					}
				}
			}
        });
    }
});

$('button[id*="printTax"]').click(function(){
    var text = prompt("Tanggal Nota:", "");
    var at=this.id.substring(this.id.indexOf('Y')+1,this.id.length);
    var link=$("#frmChangePurchase").attr("action");
    $.ajax({
        url: "<?=site_url('purchase_retur_ajax/checkPrintLog', NULL, FALSE)?>/" + at,
        async:false,
        success: function(data){
            var xmlDoc = $.parseXML(data);
            var xml = $(xmlDoc);
            var fakturnumber=xml.find('Kode').find('prlo_invoice_number').text();
            if(fakturnumber!=''){
                link=link+"?date="+text+"&kode="+fakturnumber+"&first=0";
            }else{
                var kode = prompt("Kode Faktur Pajak:", "");
                link=link+"?date="+text+"&kode="+kode+"&first=1";
            }
        }
    });
    $("#frmChangePurchase").attr("action",link);
});

$("#txtTax").change(function(){
	var totalwithdisc=parseFloat( $("#subTotalWithDisc").autoNumeric('get'));
	var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});

$( "#dsc4").change(function(){
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
});
var valuebefore=0;

$( "input[name*='Item']").focus(function(){
	valuebefore=$(this).val();
});

$( ".awal").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var max=0;
	var now=0;
	var temp='';
	for(var zz=1;zz<=3;zz++){
		if($("#txtItem"+zz+"Qty"+at).val()<0){
			$("#txtItem"+zz+"Qty"+at).val($("#txtItem"+zz+"Qty"+at).val()*-1);
		}
		if($("#max"+zz+"Item"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
			max+=$("#txtItem"+zz+"Conv"+at).val()*$("#max"+zz+"Item"+at).val();
		}

		if($("#txtItem"+zz+"Qty"+at).val()>0 && $("#txtItem"+zz+"Conv"+at).val()>0){
			now+=$("#txtItem"+zz+"Qty"+at).val()*$("#txtItem"+zz+"Conv"+at).val();
		}
	}
    for(var zz=0;zz<selectedItemBonusBefore.length;zz++){
        if($("#idItem"+at).val()==selectedItemBonusBefore[zz]){
            var at2=$("#itemBonusAt"+selectedItemBonusBefore[zz]).val();
            if(zz<numberitembonusawal){
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#txtItemBonus"+zzz+"Qty"+at2).val() >0 && $("#txtItemBonus"+zzz+"Conv"+at2).val()>0)
                        now+=$("#txtItemBonus"+zzz+"Conv"+at2).val() * $("#txtItemBonus"+zzz+"Qty"+at2).val();
                }
            }else{
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#qty"+zzz+"ItemBonusX"+at2).val() >0 && $("#conv"+zzz+"BonusUnitX"+at2).val()>0)
                        now+=$("#conv"+zzz+"BonusUnitX"+at2).val() * $("#qty"+zzz+"ItemBonusX"+at2).val();
                }
            }
        }
    }
	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		$(this).val(valuebefore);
	}else{
		if($("#txtItemPrice"+at).autoNumeric('get')<0){
			$("#txtItemPrice"+at).autoNumeric('set',$("#txtItemPrice"+at).autoNumeric('get')*-1);
		}
		var previous=$("#subTotal"+at).autoNumeric('get');
		var price = $("#txtItemPrice"+at).autoNumeric('get');
		var subtotal=$("#txtItem1Qty"+at).val()*price+($("#txtItem2Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem2Conv"+at).val())+($("#txtItem3Qty"+at).val()*price/$("#txtItem1Conv"+at).val()*$("#txtItem3Conv"+at).val());
		/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

		for(var zz=1;zz<=3;zz++){
			subtotal=subtotal-(subtotal*$("#txtItem"+zz+"Disc"+at).val()/100);
		}
		$("#subTotal"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
		if(totalwithdisc<=0){
			$("#subTotalWithDisc").autoNumeric('set','0');
			$("#subTotalTax").autoNumeric('set','0');
		}else{
			$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
			var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
			$("#subTotalTax").autoNumeric('set',totalTax);
		}
	}
});
$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBefore) > -1){

					}else{
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalitem;
		if(numberitem=='0' && numberitemawal=='0'){
			$("#invoiceItemList tbody").html('');
		}
        var id=$(selectedPO).find('id').text();
		$("#invoiceItemList tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteNewX'+i+'"/></td>'+
				'<td><label id="maxStrX'+i+'"></label></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm" id="qty1ItemX'+i+'" value="0" /></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
				'<td class="disc"><div class="form-group">'+
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
				'</div></td>'+
				'<td class="subTotal" id="subTotalX'+i+'">0</td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
                '<input type="hidden" id="itemAt'+id+'">'+
				'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		selectedItemBefore.push(id);
		var max=0;
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
		var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
		$("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
		$("#idItemX"+i).val(id);
        $("#itemAt"+id).val(i);
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
				}
				$.ajax({
					url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
					success: function(data){
						var xmlDoc = $.parseXML(data);
						var xml = $(xmlDoc);
						$arrSelectedPO = xml;
						max =xml.find('Stock').text();
						for(var rr=1;rr<=3;rr++){
							var hasil=0;
							if($("#conv"+rr+"UnitX"+i).val()!=''){
								hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
								max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
								if(rr==1){
									$("#maxStrX"+i).text(hasil);
									$("#max1ItemX"+i).val(hasil);
								}
								if(rr==2){
									$("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
									$("#max2ItemX"+i).val(hasil);
								}
								if(rr==3){
									$("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
									$("#max3ItemX"+i).val(hasil);
								}
							}
						}
					}
				});
			}
		});

		totalitem++;
		numberitem++;
		$("#totalItem").val(totalitem);
	}
});

$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItemBonus"]').val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1){

                    }else{
                        display.push({label: "("+strKode+") "+strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalitembonus;
        if(numberitembonus + numberitembonusawal=='0'){
            $("#selectedItemsBonus tbody").html('');
        }
        var id=$(selectedPO).find('id').text();
        $("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteNewX'+i+'"/></td>'+
                '<td><label id="maxStrBonusX'+i+'"></label></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td><label id="nmeItemBonusX'+i+'"></label></td>'+
                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                '<input type="hidden" id="conv1BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv2BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv3BonusUnitX'+i+'" >'+
                '<input type="hidden" id="max1ItemBonusX'+i+'" >' +
                '<input type="hidden" id="max2ItemBonusX'+i+'" >' +
                '<input type="hidden" id="max3ItemBonusX'+i+'" >'+
                '<input type="hidden" id="itemBonusAt'+id+'">'+
                '</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text("("+prodcode+") "+strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#idItemBonusX"+i).val(id);
        $("#itemBonusAt"+id).val(i);
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"BonusUnitX"+i).val(unitConv[zz-1]);
                    $("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
                }
                $.ajax({
                    url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
                    success: function(data){
                        var xmlDoc = $.parseXML(data);
                        var xml = $(xmlDoc);
                        $arrSelectedPO = xml;
                        max =xml.find('Stock').text();
                        for(var zzz=1;zzz<=3;zzz++){
                            var hasil=0;
                            if($("#conv"+zzz+"BonusUnitX"+i).val()!=''){
                                hasil=Math.floor(parseInt(max)/parseInt($("#conv"+zzz+"BonusUnitX"+i).val()));
                                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+zzz+"BonusUnitX"+i).val()));
                                if(zzz==1){
                                    $("#maxStrBonusX"+i).text(hasil);
                                    $("#max1ItemBonusX"+i).val(hasil);
                                }
                                if(zzz==2){
                                    $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                    $("#max2ItemBonusX"+i).val(hasil);
                                }
                                if(zzz==3){
                                    $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
                                    $("#max3ItemBonusX"+i).val(hasil);
                                }
                            }
                        }
                    }
                });
            }
        });
        totalitembonus++;
        numberitembonus++;
        $("#totalItemBonus").val(totalitembonus);
    }
});

$( "input[name*='cbDeleteAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotal"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitemawal--;
	if((numberitemawal+numberitem)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItem"+at).val();
	});
	$(this).closest("tr").remove();
});
$("#invoiceItemList").on("click","input[type='checkbox'][name*='cbDeleteNew']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberitem--;
	if((numberitem+numberitemawal)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});
$("#invoiceItemList").on("focus","input[type='text'][name*='PriceEffect']",function(){
	valuebefore=$(this).val();
});

$("#invoiceItemList").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var max=0;
	var now=0;
	var temp='';
	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
		if($("#max"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			max+=$("#conv"+zz+"UnitX"+at).val()*$("#max"+zz+"ItemX"+at).val();
		}

		if($("#qty"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			now+=$("#qty"+zz+"ItemX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
		}
	}
    for(var zz=0;zz<selectedItemBonusBefore.length;zz++){
        if($("#idItemX"+at).val()==selectedItemBonusBefore[zz]){
            var at2=$("#itemBonusAt"+selectedItemBonusBefore[zz]).val();
            if(zz<numberitembonusawal){
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#txtItemBonus"+zzz+"Qty"+at2).val() >0 && $("#txtItemBonus"+zzz+"Conv"+at2).val()>0)
                        now+=$("#txtItemBonus"+zzz+"Conv"+at2).val() * $("#txtItemBonus"+zzz+"Qty"+at2).val();
                }
            }else{
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#qty"+zzz+"ItemBonusX"+at2).val() >0 && $("#conv"+zzz+"BonusUnitX"+at2).val()>0)
                        now+=$("#conv"+zzz+"BonusUnitX"+at2).val() * $("#qty"+zzz+"ItemBonusX"+at2).val();
                }
            }
        }
    }
	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		$(this).val(valuebefore);
	}else{
		if($("#prcItemX"+at).autoNumeric('get')<0){
			$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
		}
		var previous=$("#subTotalX"+at).autoNumeric('get');
		var price = $("#prcItemX"+at).autoNumeric('get');
		var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
		/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

		for(var zz=1;zz<=3;zz++){
			subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
		}
		$("#subTotalX"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		var totalwithdisc=totalNoTax-$("#dsc4").autoNumeric('get');
		if(totalwithdisc<=0){
			$("#subTotalWithDisc").autoNumeric('set','0');
			$("#subTotalTax").autoNumeric('set','0');
		}else{
			$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
			var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
			$("#subTotalTax").autoNumeric('set',totalTax);
		}
	}
});

$( "input[name*='cbBonusAwal']").change(function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);
    numberitembonusawal--;
    if((numberitembonus+numberitembonusawal)=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonus"+at).val();
    });
    $(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDeleteNew']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberitembonus--;
    if((numberitembonus+numberitembonusawal)=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="3"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});

$("#selectedItemsBonus").on("focus","input[type='text'][name*='PriceEffect']",function(){
    valuebefore=$(this).val();
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    var max=0;
    var now=0;
    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemBonusX"+at).val()<0){
            $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
        }
        if($("#max"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"BonusUnitX"+at).val()>0){
            max+=$("#conv"+zz+"BonusUnitX"+at).val()*$("#max"+zz+"ItemBonusX"+at).val();
        }

        if($("#qty"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"BonusUnitX"+at).val()>0){
            now+=$("#qty"+zz+"ItemBonusX"+at).val()*$("#conv"+zz+"BonusUnitX"+at).val();
        }
    }
    for(var zz=0;zz<selectedItemBefore.length;zz++){
        if($("#idItemBonusX"+at).val()==selectedItemBefore[zz]){
            var at2=$("#itemAt"+selectedItemBefore[zz]).val();
            if(zz<numberitemawal){
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#txtItem"+zzz+"Qty"+at2).val() >0 && $("#txtItem"+zzz+"Conv"+at2).val()>0)
                        now+=$("#txtItem"+zzz+"Conv"+at2).val() * $("#txtItem"+zzz+"Qty"+at2).val();
                }
            }else{
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#qty"+zzz+"ItemX"+at2).val() >0 && $("#conv"+zzz+"UnitX"+at2).val()>0)
                        now+=$("#conv"+zzz+"UnitX"+at2).val() * $("#qty"+zzz+"ItemX"+at2).val();
                }
            }
        }
    }
    if(max<now){
        alert("jumlah barang yang dimasukkan melebihi stock yang ada");
        $(this).val(valuebefore);
    }
});

$( ".awalbonus").change(function(){
    var at1=this.name.indexOf("[");
    var at2=this.name.indexOf("]");
    var at=this.name.substring(at1+1,at2);
    var max=0;
    var now=0;
    for(var zz=1;zz<=3;zz++){
        if($("#txtItemBonus"+zz+"Qty"+at).val()<0){
            $("#txtItemBonus"+zz+"Qty"+at).val($("#txtItemBonus"+zz+"Qty"+at).val()*-1);
        }
        if($("#max"+zz+"ItemBonus"+at).val()>0 && $("#txtItemBonus"+zz+"Conv"+at).val()>0){
            max+=$("#txtItemBonus"+zz+"Conv"+at).val()*$("#max"+zz+"ItemBonus"+at).val();
        }

        if($("#txtItemBonus"+zz+"Qty"+at).val()>0 && $("#txtItemBonus"+zz+"Conv"+at).val()>0){
            now+=$("#txtItemBonus"+zz+"Qty"+at).val()*$("#txtItemBonus"+zz+"Conv"+at).val();
        }
    }
    for(var zz=0;zz<selectedItemBefore.length;zz++){
        if($("#idItemBonus"+at).val()==selectedItemBefore[zz]){
            var at2=$("#itemAt"+selectedItemBefore[zz]).val();
            if(zz<numberitemawal){
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#txtItem"+zzz+"Qty"+at2).val() >0 && $("#txtItem"+zzz+"Conv"+at2).val()>0)
                        now+=$("#txtItem"+zzz+"Conv"+at2).val() * $("#txtItem"+zzz+"Qty"+at2).val();
                }
            }else{
                for(var zzz=1;zzz<=3;zzz++){
                    if($("#qty"+zzz+"ItemX"+at2).val() >0 && $("#conv"+zzz+"UnitX"+at2).val()>0)
                        now+=$("#conv"+zzz+"UnitX"+at2).val() * $("#qty"+zzz+"ItemX"+at2).val();
                }
            }
        }
    }
    if(max<now){
        alert("jumlah barang yang dimasukkan melebihi stock yang ada");
        $(this).val(valuebefore);
    }
});


});</script>