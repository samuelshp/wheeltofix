<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddPurchase").validate({
	rules:{},
	messages:{},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});
$("#frmAddPurchase").submit(function() {
	if($("#selSupp").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectsupplier')?>"); return false;
	}
	if(numberItem<=0){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pleaseselectitem')?>"); return false;
	}
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-areyousure')?>") == false) return false;
	
	return true;
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewItem").children("option:selected").val() > 0) {
		if($("#hidNewItem").val() == '') $("#hidNewItem").val($("#selNewItem").children("option:selected").val());
		else $("#hidNewItem").val($("#hidNewItem").val() + '; ' + $("#selNewItem").children("option:selected").val());
		$("#selNewItem").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewItem").val() != '') {
		strNewItem = $("#hidNewItem").val(); arrNewItem = strNewItem.split("; ");
		$(this).attr("title",arrNewItem.length + ' Item(s)');
	}
});
var totalitembonus=0;
var numberitembonus=0;
var selectedItemBonusBefore=[];
var selectedItemBefore=[];
var totalitem=0;
var numberItem=0;
$("#subTotalNoTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalWithDisc").autoNumeric('init', autoNumericOptionsRupiah);
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
$("#dsc4Item").autoNumeric('init', autoNumericOptionsRupiah);
$("#selSupp").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getSupplierAutoComplete')?>/" + $("#selSupp").val(),
			beforeSend: function() {
				$("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupp").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
                    var code = $(val).find('supp_code').text();
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ strName,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#IDSupp").val($(selectedPO).find('id').text());
		if($("#WarehouseID").val()!='' && $("#IDSupp").val()!=''){
			$("#txtNewItem").prop('disabled',false);
            $("#txtNewItemBonus").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}else{
			$("#txtNewItem").prop('disabled',true);
            $("#txtNewItemBonus").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}
		selectedItemBefore=[];
		totalitem=0;
		numberItem=0;
		$("#selectedItems tbody").html('');
		/*$("#selectedItemsBonus tbody").html('');*/
		$("#selectedItems tbody").append('<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>');
		/* $("#selectedItemsBonus tbody").append('<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-nodata')?></td></tr>'); */
        $("#loadSupp").html('<i class="fa fa-check"></i>');
    }
});
$("#txtNewItem").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItem"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
					if($.inArray(intID, selectedItemBefore) > -1){

					}else{
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalitem;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selectedPO).find('prod_conv1').text();
        var prod_conv2 = $(selectedPO).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
        var id=$(selectedPO).find('id').text();
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td><label id="maxStrX'+i+'"></label></td>'+
				'<td class="qty"><div class="form-group">'+
				'<div class="col-xs-4"><input type="text" name="qty1PriceEffect'+i+'" class="required number form-control input-sm' + qty1HiddenClass + '" id="qty1ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty2PriceEffect'+i+'" class="required number form-control input-sm' + qty2HiddenClass + '" id="qty2ItemX'+i+'" value="0"/></div>' +
				'<div class="col-xs-4"><input type="text" name="qty3PriceEffect'+i+'" class="required number form-control input-sm" id="qty3ItemX'+i+'" value="0"/></div>'+
				'</div></td>'+
				'<td id="nmeItemX'+i+'"></td>'+
				'<td class="pr"><div class="input-group"><label class="input-group-addon"><?=str_replace(" 0","",setPrice("","USED"))?></label><input type="text" name="prcPriceEffect'+i+'" class="required currency form-control input-sm" id="prcItemX'+i+'" value="0"/></div></td>'+
				'<td class="disc"><div class="form-group">'+
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc1PriceEffect'+i+'" class="required number form-control input-sm" id="dsc1ItemX'+i+'" value="0" placeholder="Discount 1" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc2PriceEffect'+i+'" class="required number form-control input-sm" id="dsc2ItemX'+i+'" value="0" placeholder="Discount 2" title="" /><label class="input-group-addon">%</label></div></div>' +
				'<div class="col-xs-4"><div class="input-group"><input type="text" name="dsc3PriceEffect'+i+'" class="required number form-control input-sm" id="dsc3ItemX'+i+'" value="0" placeholder="Discount 3" title="" /><label class="input-group-addon">%</label></div></div>' +
				'</div></td>'+
				'<td id="subTotalX'+i+'">0</td>'+
				'<input type="hidden" id="idItemX'+i+'" name="idItem'+i+'">'+
				'<input type="hidden" id="prodItemX'+i+'" name="prodItem'+i+'">'+
				'<input type="hidden" id="probItemX'+i+'" name="probItem'+i+'">' +
				'<input type="hidden" id="sel1UnitIDX'+i+'" name="sel1UnitID'+i+'">' +
				'<input type="hidden" id="sel2UnitIDX'+i+'" name="sel2UnitID'+i+'">' +
				'<input type="hidden" id="sel3UnitIDX'+i+'" name="sel3UnitID'+i+'">'+
				'<input type="hidden" id="conv1UnitX'+i+'" >' +
				'<input type="hidden" id="conv2UnitX'+i+'" >' +
				'<input type="hidden" id="conv3UnitX'+i+'" >'+
				'<input type="hidden" id="max1ItemX'+i+'" name="max1Item'+i+'">' +
				'<input type="hidden" id="max2ItemX'+i+'" name="max2Item'+i+'">' +
				'<input type="hidden" id="max3ItemX'+i+'" name="max3Item'+i+'">'+
                '<input type="hidden" id="itemAtX'+id+'">'+
				'</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
		var prodtitle = $(selectedPO).find('prod_title').text();
		var probtitle = $(selectedPO).find('prob_title').text();
		var proctitle = $(selectedPO).find('proc_title').text();
		var strName=$(selectedPO).find('strName').text();
		selectedItemBefore.push(id);
		var max=0;
		$("#nmeItemX"+i).text("("+prodcode+") "+ strName);
		var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
		$("#prcItemX"+i).val($(selectedPO).find('prod_price').text());
		$("#idItemX"+i).val(id);
        $("#itemAtX"+id).val(i);
		$("#prodItemX"+i).val(prodtitle);
		$("#probItemX"+i).val(probtitle);
		$("#subTotalX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#prcItemX"+i).autoNumeric('init', autoNumericOptionsRupiah);
		$("#qty1ItemX"+i).hide();
		$("#qty2ItemX"+i).hide();
		$("#qty3ItemX"+i).hide();
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemX"+i).val()+"/0",
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var unitID=[];
				var unitStr=[];
				var unitConv=[];
				$.map(xml.find('Unit').find('item'),function(val,j){
					var intID = $(val).find('id').text();
					var strUnit=$(val).find('unit_title').text();
					var intConv=$(val).find('unit_conversion').text();
					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
					/* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
				});
				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemX"+i).show();
					$("#sel"+zz+"UnitIDX"+i).val(unitID[zz-1]);
					$("#conv"+zz+"UnitX"+i).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemX"+i).attr("placeholder",unitStr[zz-1]);
				}
                $.ajax({
                    url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
                    success: function(data){
                        var xmlDoc = $.parseXML(data);
                        var xml = $(xmlDoc);
                        $arrSelectedPO = xml;
                        max =xml.find('Stock').text();
                        for(var rr = 1; rr <= 3; rr++) {
							if($("#conv"+rr+"UnitX"+i).val() != '') {
								var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitX"+i).val()));
						        if(rr == 1) {
						            var conv = xml.find('prod_conv1').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
						                $("#maxStrX"+i).text(hasil);
						                $("#max1ItemX"+i).val(hasil);
						            } else {
						                $("#maxStrX"+i).text('0');
						                $("#max1ItemX"+i).val('0');
						            }
						        } else if(rr == 2) {
						            conv = xml.find('prod_conv2').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitX"+i).val()));
						                $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
						                $("#max2ItemX"+i).val(hasil);
						            } else {
						                $("#maxStrX"+i).text($("#maxStrX"+i).text()+" | 0");
						                $("#max2ItemX"+i).val('0');
						            }
						        } else if(rr == 3) {
									$("#maxStrX"+i).text($("#maxStrX"+i).text()+" | "+ hasil);
									$("#max3ItemX"+i).val(hasil);
								}
							}
						}
                    }
                });
			}
		});
		totalitem++;
		numberItem++;
		$("#totalItem").val(totalitem);

		$('#txtNewItem').val(''); return false; // Clear the textbox
	}
});

$("#txtNewItemBonus").autocomplete({
    minLength:3,
    delay:500,
    source: function(request,response) {
        $.ajax({
            url: "<?=site_url('purchase_ajax/getProductAutoCompleteBySupplier', NULL, FALSE)?>/" + $("#IDSupp").val() + "/" + $('input[name="txtNewItemBonus"]').val(),
            beforeSend: function() {
                $("#loadItemBonus").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
            },
            complete: function() {
                $("#loadItemBonus").html('');
            },
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                var display=[];
                $.map(xml.find('Product').find('item'),function(val,i){
                    var intID = $(val).find('id').text();
                    var strName=$(val).find('strNameWithPrice').text();
                    var strKode=$(val).find('prod_code').text();
                    if($.inArray(intID, selectedItemBonusBefore) > -1){

                    }else{
                        display.push({label: "("+strKode+") "+strName, value: '',id:intID});
                    }
                });
                response(display);
            }
        });
    },
    select: function(event, ui) {
        var selectedPO = $.grep($arrSelectedPO.find('Product').find('item'), function(el) {
            return $(el).find('id').text() == ui.item.id;
        });
        var i=totalitembonus;
        if(numberitembonus=='0'){
            $("#selectedItemsBonus tbody").html('');
        }
        var id=$(selectedPO).find('id').text();
        $("#selectedItemsBonus tbody").append(
            '<tr>'+
                '<td class="cb"><input type="checkbox" name="cbDeleteBonusX'+i+'"/></td>'+
                '<td><label id="maxStrBonusX'+i+'"></label></td>'+
                '<td class="qty"><div class="form-group">'+
                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" value="0"/></div>' +
                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" value="0"/></div>'+
                '</div></td>'+
                '<td><label id="nmeItemBonusX'+i+'"></label></td>'+
                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                '<input type="hidden" id="conv1BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv2BonusUnitX'+i+'" >' +
                '<input type="hidden" id="conv3BonusUnitX'+i+'" >'+
                '<input type="hidden" id="max1ItemBonusX'+i+'" >' +
                '<input type="hidden" id="max2ItemBonusX'+i+'" >' +
                '<input type="hidden" id="max3ItemBonusX'+i+'" >'+
                '<input type="hidden" id="itemBonusAtX'+id+'">'+
                '</tr>');
        var prodcode = $(selectedPO).find('prod_code').text();
        var prodtitle = $(selectedPO).find('prod_title').text();
        var probtitle = $(selectedPO).find('prob_title').text();
        var proctitle = $(selectedPO).find('proc_title').text();
        var strName=$(selectedPO).find('strName').text();
        selectedItemBonusBefore.push(id);
        $("#nmeItemBonusX"+i).text("("+prodcode+") "+strName);
        var proddesc = $(selectedPO).find('prod_description').text();
        if(proddesc != '') {
            $("#nmeItemBonusX"+i).append('<i class="fa fa-info-circle text-primary pull-right"></i>');
            $("#nmeItemBonusX"+i+' > i.fa-info-circle').data('toggle', 'tooltip').data('placement', 'top').data('html', 'true').attr('title', proddesc).tooltip();
        }
        $("#idItemBonusX"+i).val(id);
        $("#itemBonusAtX"+id).val(i);
        $("#prodItemBonusX"+i).val(prodtitle);
        $("#probItemBonusX"+i).val(probtitle);
        $("#qty1ItemBonusX"+i).hide();
        $("#qty2ItemBonusX"+i).hide();
        $("#qty3ItemBonusX"+i).hide();
        $.ajax({
            url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemBonusX"+i).val()+"/0",
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                var unitID=[];
                var unitStr=[];
                var unitConv=[];
                $.map(xml.find('Unit').find('item'),function(val,j){
                    var intID = $(val).find('id').text();
                    var strUnit=$(val).find('unit_title').text();
                    var intConv=$(val).find('unit_conversion').text();
                    unitID.push(intID);
                    unitStr.push(strUnit);
                    unitConv.push(intConv);
                    /* $('#selIteMunit'+i).append('<option value="'+intID+'">'+strUnit+'</option>'); */
                });
                for(var zz=1;zz<=unitID.length;zz++){
                    $("#qty"+zz+"ItemBonusX"+i).show();
                    $("#sel"+zz+"UnitBonusIDX"+i).val(unitID[zz-1]);
                    $("#conv"+zz+"BonusUnitX"+i).val(unitConv[zz-1]);
                    $("#qty"+zz+"ItemBonusX"+i).attr("placeholder",unitStr[zz-1]);
                }
                $.ajax({
                    url: "<?=site_url('inventory_ajax/getProductStock', NULL, FALSE)?>/" + id + "/" + $("#WarehouseID").val(),
                    success: function(data){
                        var xmlDoc = $.parseXML(data);
                        var xml = $(xmlDoc);
                        $arrSelectedPO = xml;
                        max =xml.find('Stock').text();
                        for(var rr = 1; rr <= 3; rr++) {
						    if($("#conv"+rr+"UnitBonusX"+i).val() != '') {
						        var hasil=Math.floor(parseInt(max)/parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
						        if(rr == 1) {
						            var conv = xml.find('prod_conv1').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
						                $("#maxStrBonusX"+i).text(hasil);
						                $("#max1ItemBonusX"+i).val(hasil);
						            } else {
						                $("#maxStrBonusX"+i).text('0');
						                $("#max1ItemBonusX"+i).val('0');
						            }
						        } else if(rr == 2) {
						            conv = xml.find('prod_conv2').text();
						            if(parseInt(conv) > 1) {
						                max=parseInt(max)-(parseInt(hasil)*parseInt($("#conv"+rr+"UnitBonusX"+i).val()));
						                $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
						                $("#max2ItemBonusX"+i).val(hasil);
						            } else {
						                $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | 0");
						                $("#max2ItemBonusX"+i).val('0');
						            }
						        } else if(rr == 3) {
						            $("#maxStrBonusX"+i).text($("#maxStrBonusX"+i).text()+" | "+ hasil);
						            $("#max3ItemBonusX"+i).val(hasil);
						        }
						    }
						}
                    }
                });
            }
        });
        totalitembonus++;
        numberitembonus++;
        $("#totalItemBonus").val(totalitembonus);

        $('#txtNewItemBonus').val(''); return false; // Clear the textbox
    }
});

var valuebefore=0;
$("#selectedItems").on("focus","input[type='text'][name*='PriceEffect']",function(){
	valuebefore=$(this).val();
});
$("#selectedItemsBonus").on("focus","input[type='text'][name*='PriceEffect']",function(){
    valuebefore=$(this).val();
});

$("#selectedItems").on("change","input[type='text'][name*='PriceEffect']",function(){
	var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
	var max=0;
	var now=0;
	var temp='';

	for(var zz=1;zz<=3;zz++){
		if($("#qty"+zz+"ItemX"+at).val()<0){
			$("#qty"+zz+"ItemX"+at).val($("#qty"+zz+"ItemX"+at).val()*-1);
		}
		if($("#max"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			max+=$("#conv"+zz+"UnitX"+at).val()*$("#max"+zz+"ItemX"+at).val();
		}

		if($("#qty"+zz+"ItemX"+at).val()>0 && $("#conv"+zz+"UnitX"+at).val()>0){
			now+=$("#qty"+zz+"ItemX"+at).val()*$("#conv"+zz+"UnitX"+at).val();
		}
	}

    for(var zz=0;zz<selectedItemBonusBefore.length;zz++){
        if($("#idItemX"+at).val()==selectedItemBonusBefore[zz]){
            var at2=$("#itemBonusAtX"+selectedItemBonusBefore[zz]).val();
            for(var zzz=1;zzz<=3;zzz++){
                if($("#qty"+zzz+"ItemBonusX"+at2).val() >0 && $("#conv"+zzz+"BonusUnitX"+at2).val()>0)
                    now+=$("#conv"+zzz+"BonusUnitX"+at2).val() * $("#qty"+zzz+"ItemBonusX"+at2).val();
            }
        }
    }

	if(max<now){
		alert("jumlah barang yang dimasukkan melebihi stock yang ada");
		$(this).val(valuebefore);
	}else{
		if($("#prcItemX"+at).autoNumeric('get')<0){
			$("#prcItemX"+at).autoNumeric('set',$("#prcItemX"+at).autoNumeric('get')*-1);
		}
		var previous=$("#subTotalX"+at).autoNumeric('get');
		var price = $("#prcItemX"+at).autoNumeric('get');
		var subtotal=$("#qty1ItemX"+at).val()*price+($("#qty2ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv2UnitX"+at).val())+($("#qty3ItemX"+at).val()*price/$("#conv1UnitX"+at).val()*$("#conv3UnitX"+at).val());
		/* var percent=$('#dscItem'+at).text.substring($('#dscItem'+at).text.length-1,$('#dscItem'+at).text.length); */

		for(var zz=1;zz<=3;zz++){
			subtotal=subtotal-(subtotal*$("#dsc"+zz+"ItemX"+at).val()/100);
		}
		$("#subTotalX"+at).autoNumeric('set',subtotal);
		var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-previous+subtotal;
		$("#subTotalNoTax").autoNumeric('set',totalNoTax);
		var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
		if(totalwithdisc<=0){
			$("#subTotalWithDisc").autoNumeric('set','0');
			$("#subTotalTax").autoNumeric('set','0');
		}else{
			$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
			var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
			$("#subTotalTax").autoNumeric('set',totalTax);
		}
	}
});

$("#selectedItemsBonus").on("change","input[type='text'][name*='PriceEffect']",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    var max=0;
    var now=0;

    for(var zz=1;zz<=3;zz++){
        if($("#qty"+zz+"ItemBonusX"+at).val()<0){
            $("#qty"+zz+"ItemBonusX"+at).val($("#qty"+zz+"ItemBonusX"+at).val()*-1);
        }
        if($("#max"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"BonusUnitX"+at).val()>0){
            max+=$("#conv"+zz+"BonusUnitX"+at).val()*$("#max"+zz+"ItemBonusX"+at).val();
        }

        if($("#qty"+zz+"ItemBonusX"+at).val()>0 && $("#conv"+zz+"BonusUnitX"+at).val()>0){
            now+=$("#qty"+zz+"ItemBonusX"+at).val()*$("#conv"+zz+"BonusUnitX"+at).val();
        }
    }
    for(var zz=0;zz<selectedItemBefore.length;zz++){
        if($("#idItemX"+at).val()==selectedItemBefore[zz]){
            var at2=$("#itemAtX"+selectedItemBefore[zz]).val();
            for(var zzz=1;zzz<=3;zzz++){
                if($("#qty"+zzz+"ItemX"+at2).val() >0 && $("#conv"+zzz+"UnitX"+at2).val()>0)
                    now+=$("#conv"+zzz+"UnitX"+at2).val() * $("#qty"+zzz+"ItemX"+at2).val();
            }
        }
    }
    if(max<now){
        alert("jumlah barang yang dimasukkan melebihi stock yang ada");
        $(this).val(valuebefore);
    }
});

    $("#dsc4Item").change(function(){
	var totalwithouttax=$("#subTotalNoTax").autoNumeric('get');
	var totalwithdisc=totalwithouttax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
})
$("#txtTax").change(function(){
	var totalwithdisc=$("#subTotalWithDisc").autoNumeric('get');
	var totalTax=parseFloat(totalwithdisc)+parseFloat(totalwithdisc*$("#txtTax").val()/100);
	$("#subTotalTax").autoNumeric('set',totalTax);
});
$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var totalNoTax=$("#subTotalNoTax").autoNumeric('get')-$("#subTotalX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',totalNoTax);
	var totalwithdisc=totalNoTax-$("#dsc4Item").autoNumeric('get');
	if(totalwithdisc<=0){
		$("#subTotalWithDisc").autoNumeric('set','0');
		$("#subTotalTax").autoNumeric('set','0');
	}else{
		$("#subTotalWithDisc").autoNumeric('set',totalwithdisc);
		var totalTax=totalwithdisc+(totalwithdisc*$("#txtTax").val()/100);
		$("#subTotalTax").autoNumeric('set',totalTax);
	}
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	selectedItemBefore = jQuery.grep(selectedItemBefore, function(value) {
		return value != $("#idItemX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#txtWarehouse").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedPO).find('id').text());
		if($("#WarehouseID").val()!='' && $("#IDSupp").val()!=''){
			$("#txtNewItem").prop('disabled',false);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}else{
			$("#txtNewItem").prop('disabled',true);
			$("#selectedItems tbody").html('');
			$("#selectedItems tbody").append(
				'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
			);
		}
		selectedItemBefore=[];
		totalitem=0;
		numberItem=0;
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});

$("#selectedItemsBonus").on("click","input[type='checkbox'][name*='cbDelete']",function(){
    var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
    numberitembonus--;
    if(numberitembonus=='0'){
        $("#selectedItemsBonus tbody").append(
            '<tr class="info"><td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
        );
    }
    selectedItemBonusBefore = jQuery.grep(selectedItemBonusBefore, function(value) {
        return value != $("#idItemBonusX"+at).val();
    });
    $(this).closest("tr").remove();
});
$("#txtTax").change(function(){
    var totalwithdisc=$("#subTotalWithDisc").autoNumeric('get');
    var totalTax=parseFloat(totalwithdisc)+parseFloat(totalwithdisc*$("#txtTax").val()/100);
    $("#subTotalTax").autoNumeric('set',totalTax);
});
$("#txtNewItem").prop('disabled',true);
$("#txtNewItemBonus").prop('disabled',true);
});</script>