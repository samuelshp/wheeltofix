<?php
$strPageTitle = 'Rekap Penjualan Per Salesman By Customer';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    //TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
    1 => array('title' => '<i class="fa fa-list"></i> Rekap Penjualan Per Salesman By Customer', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('report_invoice/rekapjualpersalesmanbycustomer', NULL, FALSE)?>" class="form-inline pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
        <label>Cari Data:</label>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Awal</label><input type="text" name="txtStart" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><label class="input-group-addon">Akhir</label><input type="text" name="txtEnd" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
        <div class="form-group"><div class="input-group"><input type="text" id="txtSalesmanName"  class="form-control" placeholder="Pilih Salesman"/><label class="input-group-addon" id="loadSalesman"></label></div></div>

        <div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button><?php
if(!empty($strSalesmanID)):
    $strPrintURL = site_url('report_invoice/rekapjualpersalesmanbycustomer?print=true&salesmanid='.$strSalesmanID.'&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
    <a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
	<a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a><?php
endif; ?> 
		</div>

        <input type="hidden" id="salesmanID" name="salesmanID"/>
        <input type="hidden" id="salesmanCode" name="salesmanCode"/>
        <input type="hidden" id="salesmanName" name="salesmanName"/>

    </form>
    <div class="col-xs-12 col-md-6 pull-left"><div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">Periode</div>
                <div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
                <div class="col-md-3">Hari ini</div>
                <div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">Kode</div>
                <div class="col-md-9">: <?=$strSalesmanCode?></div>
                <div class="col-md-3">Nama</div>
                <div class="col-md-9">: <?=$strSalesmanName?></div>
            </div>
        </div>
    </div></div>
    <p class="spacer">&nbsp;</p>
    <hr />
    <div class="col-xs-12"><?=$strPage?></div>

    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
            <thead><tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama Customer</th>
                <th>Kota</th>
                <th>Q-B</th>
                <th>Q-T</th>
                <th>Q-K</th>
                <th>GrandNetto</th>
            </tr></thead>
            <tbody><?php
            if(!empty($arrItems)):
                foreach($arrItems as $e): ?>
                    <tr>
                        <td><?=$e['nourut']?></td>
                        <td><?=$e['cust_code']?></td>
                        <td><?=$e['cust_name']?></td>
                        <td><?=$e['cust_city']?></td>
                        <td><?=$e['qty1']?></td>
                        <td><?=$e['qty2']?></td>
                        <td><?=$e['qty3']?></td>
                        <td><?=setPrice($e['total'])?></td>
                    </tr>
                <?php endforeach;
            else :?>
                <tr><td class="noData" colspan="8"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
            endif;?>
            </tbody>
        </table></div>
    <?=$strPage?>
</div>
