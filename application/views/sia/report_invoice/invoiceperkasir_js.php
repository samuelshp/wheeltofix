<!-- created by patricklipesik -->
<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#searchForm").submit(function() {
	if($("#userCode").val()=='') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectuser')?>"); return false;
	}
	return true;
});

$("#txtUserCode").autocomplete({
	minLength:1,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceUserAutoComplete', NULL, FALSE)?>/" + $("#txtUserCode").val(),
			beforeSend: function() {
				$("#loadUser").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadUser").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedUser = xml;
				var display=[];
				$.map(xml.find('User').find('item'),function(val,i){
					var code = $(val).find('adlg_login').text();
					var strCode=code;
					var intID = $(val).find('id').text();
					display.push({label: strCode, value: code,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedUser = $.grep($arrSelectedUser.find('User').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$('input[name="userID"]').val($(selectedUser).find('id').text());
		$("#userCode").val($(selectedUser).find('adlg_login').text());
		$("#loadUser").html('<i class="fa fa-check"></i>');
	}
});

$('#btnLockInvoice').click(function() {

});

});</script>