<?php
// created by patricklipesik
$strPageURL = 'invoiceperkasir';
$strPageTitle = 'Invoice Per Kasir';
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
	0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
//TODO update bahasa : 1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
	1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<form name="searchForm" method="post" action="<?=site_url('report_invoice/'.$strPageURL, NULL, FALSE)?>" class="pull-right col-xs-12 col-md-6" style="margin-right:-15px;">
		<label>Cari Data:</label>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Awal</label>
				<input type="text" name="txtStart" value="<?php if(!empty($strStart)) echo $strStart; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<label class="input-group-addon">Akhir</label>
				<input type="text" name="txtEnd" value="<?php if(!empty($strEnd)) echo $strEnd; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<input type="text" id="txtUserCode" class="form-control" placeholder="Pilih Kasir"/>
				<label class="input-group-addon" id="loadUser"></label>
			</div>
		</div>
		<div class="col pull-right" style="margin:10px 0px 0px 10px;">
			<button type="submit" name="subSearch" value="search" class="btn btn-primary">
				<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?>
			</button><?php
$strPrintURL = site_url('report_invoice/'.$strPageURL.'?print=true&userid='.$strUserID.'&start='.$strStart.'&end='.$strEnd, NULL, FALSE);?>
			<a href="<?=$strPrintURL?>" class="btn btn-success"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a>
			<a href="<?=$strPrintURL.'&excel=true'?>" class="btn btn-warning"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-excel')?></a>
		</div>
		
		<input type="hidden" id="userID" name="userID"/>
		<input type="hidden" id="userCode" name="userCode"/>
	</form>
	<div class="col-xs-12 col-md-6 pull-left">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3">Periode</div>
					<div class="col-md-9">: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></div>
					<div class="col-md-3">Hari ini</div>
					<div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3">Kasir</div>
					<div class="col-md-9">: <?=$strUserCode?></div>
				<!--<div class="col-md-3">Nama</div>
					<div class="col-md-9">: <?=$strSalesmanName?></div>-->
				</div>
			</div>
		</div>
	</div>
	<p class="spacer">&nbsp;</p>
	<hr />
	<div class="col-xs-12"><?=$strPage?></div>
	
	<p class="spacer">&nbsp;</p>
	<form name="frmLock" method="post" class="table-responsive">
		<table class="table table-bordered table-condensed table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Kasir</th>
					<th>No Trans</th>
					<th>Tanggal</th>
					<th>Kode</th>
					<th>Nama</th>
					<th>Pembayaran</th>
					<th>Total</th>
				<!--<th>Potongan</th>
					<th>GrandNetto</th>-->
				</tr>
			</thead>
			<tbody><?php
			$arrTotal = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0);
			$sumGrandTotal = 0;
			if(!empty($arrItems)):
				foreach($arrItems as $e):
					if($e['invo_locked'] != 2) $strCheckbox = '<input type="checkbox" name="cbLock[]" value="'.$e['id'].'" checked>';
					else $strCheckbox = ''; ?>
					<tr>
						<td><?=$strCheckbox.$e['nourut']?></td>
						<td><?=$e['adlg_login']?></td>
						<td><a href="<?=site_url('invoice/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=$e['invo_code']?></a></td>
						<td><?=formatDate2($e['cdate'],'d/m/Y h:i:s')?></td>
						<td><?=$e['cust_code']?></td>
						<td><?=$e['cust_name']?></td>
						<td><?php
                            switch($e['invo_payment']) {
                                case 1: echo 'Tunai'; break;
                                case 2: echo 'Kredit'; break;
                                case 3: echo 'Debit'; break;
                                case 4: echo 'Transfer'; break;
                                case 5: echo 'Deposit'; break;
                            } ?>
						</td>
						<td><?=setPrice($e['invo_grandtotal'])?></td>
					<!--<td><?=$e['invo_discount']?></td>
						<td><?=setPrice($e['invo_grandtotal']-$e['invo_discount']-$e['invo_tax'])?></td>-->
					</tr>
				<?php
				$arrTotal[$e['invo_payment']] += $e['invo_grandtotal'];
				$sumGrandTotal += $e['invo_grandtotal'];
				endforeach;
			else :?>
				<tr><td class="noData" colspan="8"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
			endif;?>
			</tbody><?php
			if(!empty($sumGrandTotal)): ?>
			<thead>
				<tr>
					<td colspan="7" style="text-align:right;">Bayar Tunai</td>
					<td><?=setPrice($arrTotal[1])?></td>
				</tr>
				<tr>
					<td colspan="7" style="text-align:right;">Bayar Kredit</td>
					<td><?=setPrice($arrTotal[2])?></td>
				</tr>
				<tr>
					<td colspan="7" style="text-align:right;">Bayar Debit</td>
					<td><?=setPrice($arrTotal[3])?></td>
				</tr>
				<tr>
					<td colspan="7" style="text-align:right;">Bayar Transfer</td>
					<td><?=setPrice($arrTotal[4])?></td>
				</tr>
				<tr>
					<td colspan="7" style="text-align:right;">Bayar Deposit</td>
					<td><?=setPrice($arrTotal[5])?></td>
				</tr>
				<tr>
					<th colspan="7" style="text-align:right;">Grand Total</th>
					<th><?=setPrice($sumGrandTotal)?></th>
				</tr>
			</thead><?php
			endif;?>
		</table>
		<div class="col pull-left"><?=$strPage?></div>
		<div class="col pull-right">
			<button name="subLock" value="lock" type="submit" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-approve')?></button>
		</div>
	</form>
</div>
<style type="text/css">
input[name^=cbLock] {margin-right: 5px; margin-top: 0; display: inline-block; position: relative; top: 2px;}
</style>