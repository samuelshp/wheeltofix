<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><?php
$strSearchAction = site_url('permintaan_pembayaran/browse', NULL, FALSE);
include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchpp.php'); ?>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('permintaan_pembayaran', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead style="text-align: center;"><tr>
			<th>Per. Bayar Code</th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
			<th>Nama Kontrak</th>
			<th>Kategori RAB</th>
			<th>Amount</th>
			<th>No. Pembayaran</th>
			<th class="action" style="text-align: center;">Action</th>
		</tr></thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrPerminBayar)):
			foreach($arrPerminBayar as $e): ?>
			<tr style="text-align: center;">
				<td><?=$e['perbayar_code']?></td>
				<td><?=formatDate2($e['cdate'])?></td>
				<td><?=$e['kont_name']?></td>
                <td><?=$e['kategori']?></td>
				<td><?=setPrice($e['perbayar_amount'])?></td>
				<td></td>
				<td class="action" style="text-align: center;">
					<a href="<?=site_url('permintaan_pembayaran/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>
				</td>
			</tr><?php
			endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</div><!--
--><?/*=site_url('purchase/view/'.$e['id'].'?print=true')*/?>