<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> Permintaan Pembayaran', 'link' => site_url('permintaan_pembayaran/browse', NULL, FALSE)),    
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12">
    <form name="frmAddPerminBayar" id="frmAddPerminBayar" method="post" action="<?=site_url('permintaan_pembayaran', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        
        <div class="col-md-6"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <input type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Owner
                    </h3>
                </div>
                    <div class="panel-body">
                        <select style="max-width:550px;" type="text" id="selOwner" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php foreach($arrOwner as $e) { ?>
                                <option value="<?=$e['id']?>"><?=$e['cust_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> 
                        Nama Proyek
                    </h3>
                </div>
                <div class="panel-body">
                    <select style="max-width:550px;" type="text" id="selProyek" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> 
                        Nama Pekerjaan
                    </h3>
                </div>
                <div class="panel-body">
                    <select style="max-width:550px;" type="text" id="selPekerjaan" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
            </div>
        </div>

        <!-- <div class="col-md-12">
            <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pb-namakontrak')?></label><input id="txtKodeKontrak" type="text" class="form-control" readonly /></div></div>
        </div> -->

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Kategori RAB
                    </h3>
                </div>
                <div class="panel-body">
                    <select style="max-width:550px;" type="text" id="selKategori" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money"></i> 
                        Amount
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="input-group">
                        <label class="input-group-addon">Rp.</label><input type="text" class="currency form-control required" name="amount" id="amount"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <button type="submit" name="smtMakePerminBayar" id="smtMakePerminBayar" value="Make Permin Bayar" class="btn btn-primary">
                    <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                </button>
                <button type="submit" name="cancelMakePerminBayar" value="Cancel Make Purchase" class="btn btn-danger">
                    <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?>
                </button>
            </div>
        </div>

    </div>

    <input type="hidden" id="owner_id" name="owner_id" value=""/>
    <input type="hidden" id="kontrak_id" name="kontrak_id" value=""/>
    <input type="hidden" id="subkontrak_id" name="subkontrak_id"/>
    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
    <input type="hidden" id="kategori_id" name="kategori_id"/>
    <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>
    <input type="hidden" id="idProduct" name="idProduct"/>

</form></div>