<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {

$("#btnUpdatePerbayar").hide();

$("#btnEditPerbayar").click(function(){
    $(this).hide();
    $("#btnUpdatePerbayar").show();
 
    $("#frmEditPerminBayar .form-control").removeAttr('disabled');
    $("#frmEditPerminBayar .chosen").chosen('destroy');    
    $("#frmEditPerminBayar .chosen").chosen();


});

$("#btnUpdatePerbayar").click(function(){
    var msg = "<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'paymentrequest-confirmupdate')?>";
    return confirm(msg);
});

$("#btnDeletePerbayar").click(function(){
    var msg = "<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'paymentrequest-confirmdelete')?>";
    return confirm(msg);
});

$("#amount").autoNumeric('init', autoNumericOptionsRupiah);

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmEditPerminBayar").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmEditPerminBayar").submit(function() {
    var form = $(this);

    $('.currency').each(function(i){
        var self = $(this);
        if(empty(self.autoNumeric('get'))){
            alert("Jumlah amount belum di isi"); return false;
        }
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
	
    return true;
});

$('#selOwner').change(function(){
    console.log($("#selOwner").val());
    
    $("#owner_id").val($('#selOwner').val());
    $.ajax({ 
        type : "POST",
        dataType:'json',
        url: "<?=site_url('permintaan_pembayaran_ajax/getNamaProyekByOwner')?>/" + $('#selOwner').val()+"/6",
        cache : false,
        success : function(result){
            $('#selProyek').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result, function (index,arr) {
                console.log(JSON.stringify(arr['id']));
                $('#selProyek').append('<option value="' + arr['id'] + '">' + arr['kont_name'] +'</option>');
            });
            $("#selProyek").trigger("chosen:updated");
        }
    });

    // Reset Select Option Pekerjaan
    $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
    $("#selPekerjaan").trigger("chosen:updated");

});

$('#selProyek').change(function(){
    console.log($('#selProyek').val()+" id kontrak");
    $("#kontrak_id").val($('#selProyek').val());
    $.ajax({ 
        type : "POST",
        dataType:'json',
        url: "<?=site_url('permintaan_pembayaran_ajax/getNamaPekerjaan')?>/" + $('#selProyek').val(),
        cache : false,
        success : function(result){
            $('#selPekerjaan').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result, function (index,arr) {
                console.log(JSON.stringify(arr['id']));
                $('#selPekerjaan').append('<option value="' + arr['id'] + '">' + arr['job'] +'</option>');
            });
            $("#selPekerjaan").trigger("chosen:updated");
        }
    });
});

$('#selPekerjaan').change(function(){
    console.log('halo');
    $("#subkontrak_id").val($('#selPekerjaan').val());
    $.ajax({ 
        type : "POST",
        dataType:'json',
        url: "<?=site_url('permintaan_pembayaran_ajax/getKategori')?>/" + $('#selPekerjaan').val(),
        cache : false,
        success : function(result){
            $('#selKategori').html('<option disabled value="0" selected>Select your option</option>');
            $.each (result, function (index,arr) {
                console.log(JSON.stringify(arr['id']));
                $('#selKategori').append('<option value="' + arr['id'] + '">' + arr['kategori'] +'</option>');
            });
            $("#selKategori").trigger("chosen:updated");
        }
    });
});

$('#selKategori').change(function(){
    $("#kategori_id").val($('#selKategori').val());
});

$('.chosen').chosen();
});
</script>