<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> Permintaan Pembayaran', 'link' => site_url('permintaan_pembayaran/browse', NULL, FALSE)),    
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />
<div class="col-xs-12">
    <form name="frmEditPerminBayar" id="frmEditPerminBayar" method="post" action="<?=site_url('permintaan_pembayaran', NULL, FALSE)?>" class="frmShop">
    <div class="row">        
        <div class="col-md-6"><div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
                <div class="panel-body">
                    <input disabled type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($arrPerminBayar['cdate'])) echo formatDate2($arrPerminBayar['cdate'], 'Y/m/d'); else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Owner
                    </h3>
                </div>
                    <div class="panel-body">
                        <select disabled style="max-width:550px;" type="text" id="selOwner" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>                            
                            <?php foreach($arrOwner as $e) { ?>
                                <option value="<?=$e['id']?>" <?=($arrPerminBayar['perbayar_ownerid'] == $e['id']) ? "selected" : null; ?> ><?=$e['cust_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> 
                        Nama Proyek
                    </h3>
                </div>
                <div class="panel-body">
                    <select disabled style="max-width:550px;" type="text" id="selProyek" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>                        
                        <?php foreach($arrProyek as $e) { ?>
                            <option value="<?=$e['id']?>" <?=($arrPerminBayar['perbayar_kontrakid'] == $e['id']) ? "selected" : null; ?> ><?=$e['kont_name']?></option>
                        <?php } ?>                        
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i> 
                        Nama Pekerjaan
                    </h3>
                </div>
                <div class="panel-body">                
                    <select disabled style="max-width:550px;" type="text" id="selPekerjaan" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>                        
                        <?php foreach($arrPekerjaan as $e) { ?>
                            <option value="<?=$e['id']?>" <?=($arrPerminBayar['perbayar_subkontrakid'] == $e['id']) ? "selected" : null; ?> ><?=$e['job']?></option>
                        <?php } ?>                        
                    </select>                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user"></i> 
                        Kategori RAB
                    </h3>
                </div>
                <div class="panel-body">                
                    <select disabled style="max-width:550px;" type="text" id="selKategori" class="form-control chosen" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>                                                       
                        <?php foreach($arrKategori as $e) { ?>                                
                            <option value="<?=$e['id']?>" <?=($arrPerminBayar['perbayar_subkontrakid'] == $e['id']) ? "selected" : null; ?> ><?=$e['kategori']?></option>
                        <?php } ?>                        
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money"></i> 
                        Amount
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="input-group">
                        <label class="input-group-addon">Rp.</label><input type="text" class="currency form-control required" name="amount" id="amount" value="<?=$arrPerminBayar['perbayar_amount']?>" disabled/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                
                    <?php if($bolBtnEdit): ?>                        
                    <button type="button" name="editPerbayar" id="btnEditPerbayar" value="Edit Perbayar" class="btn btn-warning">
                        <span class="fa fa-edit" aria-hidden="true"> </span>
                    </button>                        
                    <?php endif; ?>
                    <?php if($bolBtnEdit): ?>                        
                    <button type="submit" name="updatePerbayar" id="btnUpdatePerbayar" value="Update Perbayar" class="btn btn-primary">
                        <span class="fa fa-edit" aria-hidden="true"> </span> 
                    </button>                        
                    <?php endif; ?>                       
                    <?php if($bolBtnApprove && $arrPerminBayar['approvalMgr'] != 2): ?>                        
                    <button type="submit" name="approvePerbayar" id="btnApprovePerbayar" value="Approve Perbayar" class="btn btn-info">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-approve')?>
                    </button>                        
                    <?php endif; ?> 
                    <?php if($bolBtnDisapprove && $arrPerminBayar['approvalMgr'] != 2): ?>                        
                    <button type="submit" name="disapprovePerbayar" id="btnDisapprovePerbayar" value="Disapprove Perbayar" class="btn btn-warning">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-disapprove')?>
                    </button>                        
                    <?php endif; ?>                                   
                    <div class="pull-right">                            
                    <?php if($bolBtnDelete): ?>                        
                        <button type="submit" name="deletePerbayar" id="btnDeletePerbayar" value="Delete Perbayar" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
                        </button>                        
                    <?php endif; ?>                                                  
                    </div>
                    <div style='color:blue;'><?=$strStatus?></div>                    
            </div>
            <?php
               // $rawStatus = $arrAcceptanceData['acce_rawstatus'];
               // include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
            ?>            
        </div>

    </div>

    <input type="hidden" id="owner_id" name="owner_id" value="<?=$arrPerminBayar['perbayar_ownerid']?>"/>
    <input type="hidden" id="kontrak_id" name="kontrak_id" value="<?=$arrPerminBayar['perbayar_kontrakid']?>"/>
    <input type="hidden" id="subkontrak_id" name="subkontrak_id" value="<?=$arrPerminBayar['perbayar_subkontrakid']?>"/>
    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($arrPerminBayar['cdate'])) echo formatDate2($arrPerminBayar['cdate'], 'Y/m/d'); else echo date('Y/m/d'); ?>"/>
    <input type="hidden" id="kategori_id" name="kategori_id" value="<?=$arrPerminBayar['perbayar_kategori']?>"/>    
    <input type="hidden" name="perbayar_id" value="<?=$arrPerminBayar['id']?>"/>

</form></div>