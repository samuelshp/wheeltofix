<script>
let availableOwner = <?= json_encode($availableOwner) ?>;
var object = <?= json_encode($object) ?>;
let availableObject = <?= json_encode($objects) ?>;

// trigger on change rev form input
changeRev = (e) => {
    selectedRev    = e.target.value;
    object         = availableObject.find(rev => rev.id == selectedRev );
    render(object);
}

// render form
render = object => {

    $('#kont-name').val(object.kont_name);
    $('#kont-job').val(object.kont_job);
    $('#kont-date').val(object.kont_date);

    $('#kont-revisi').empty();
    availableObject.map(rev => {
        let selected = '';
        if(rev.kont_revisi == object.kont_revisi) selected = 'selected';
        $('#kont-revisi').append(`<option value="${rev.id}" ${selected}> Rev ${rev.kont_revisi} (${rev.cdate})</option>`)
    })

}

validateForm = (e) => {
    errMsg = [];
    
    if(errMsg.length > 0){
        swal({
            text: errMsg.join(', '),
            icon: 'error'
        })
        return false;
    } else {
        return true;
    }
}

$(function(){

    if(object != null) 
        render(object);
    else
        document.getElementById('kont-date').valueAsDate = new Date();
    
    
    $('#kont-revisi').change(changeRev);
    $('form').on('submit', validateForm)
    
});
</script>