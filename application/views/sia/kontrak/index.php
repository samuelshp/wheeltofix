<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 

    //echo $strMemberLogin;
?>  

<div class="col-xs-12">
    <a href="<?= site_url('kontrak/action/add') ?>">
        <button class="btn btn-primary"><i class="fa fa-plus"></i></button>
    </a>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover data-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Project</th>
                    <th>Owner</th>
                    <th>Tanggal Proyek</th>
                    <th>Nilai Total Kontrak</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            // Display data in the table
            if(!empty($kontrakList)):
                foreach($kontrakList as $key => $kontrak):  
            ?>  
                <tr>
                    <td><?= ($key+1) ?></td>
                    <td><?= $kontrak->kont_name ?></td>
                    <td><?= $kontrak->owner? $kontrak->owner->cust_name : 'Not Set' ?></td>
                    <td><?= $kontrak->kont_date ?></td>
                    <td><?= array_key_exists($kontrak->id, $sum)? setPrice($sum[$kontrak->id]).',00' : 'IDR 0,00' ?></td>
                    <td>
                        <a href="<?= site_url('kontrak/view/'.$kontrak->id) ?>" style="margin: 0">
                            <button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button>
                        </a>
                        <a href="<?= site_url('kontrak/action/edit/'.$kontrak->id) ?>" style="margin: 0">
                            <button type="button" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                        </a>
                        <button type="button" data-id="<?= $kontrak->id ?>" class="btn btn-danger delete-object-button"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
        <?php
                endforeach;
            endif; 
        ?>
            </tbody>
        </table>
    </div>
</div>