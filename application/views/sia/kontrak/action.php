<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> Kontrak', 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<div class="col-xs-12">
    <a href="<?= site_url('kontrak/action/add') ?>">
    </a>
    <p class="spacer">&nbsp;</p>

    <form method="POST" action="<?= $formAction  ?>">
        <div class="row">
            <div class="col-sm-12 col-md-6">

                <div class="form-group">
                    <label>Nama Proyek</label>
                    <input id="kont-name" type="text" class="form-control" name="kont_name" placeholder="Nama Proyek" value="<?= repopulate('kont_name') ?>">
                </div>

                <div class="form-group">
                    <label>Owner</label>
                    <select id="owner-id" class="form-control select2" name="owner_id">
                        <option>-- Pilih Owner --</option>
                        <?php foreach($availableOwner as $owner): ?>
                        <?php $selected = isset($object) && $object->owner_id == $owner->id? 'selected' :  '' ;?>
                        <option value="<?= $owner->id ?>" <?= $selected ?>><?= $owner->cust_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Tanggal Proyek</label>
                    <input id="kont-date" type="date" class="form-control" name="kont_date" placeholder="Nama Proyek" value="<?= repopulate('kont_date') ?>">
                </div>

                <div class="form-group">
                    <label>Nilai Total Kontrak</label>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="kont-value" type="number" class="form-control" name="kont_value" placeholder="Nilai Kontrak" value="<?= $kontrakVal ?>" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label>Nilai Total Kontrak Terbayar</label>
                    <div class="input-group">
                        <div class="input-group-addon">Rp.</div>
                        <input id="terbayar" type="number" class="form-control" placeholder="Nilai Kontrak" value="<?= $terbayar ?>" disabled>
                    </div>
                </div>

            </div>
        </div>
        <br>
        <br>
        <?php if(isset($object)): ?>
        <input type="hidden" name="id" value="<?= $object->id ?>">
        <?php endif; ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success pull-right" style="margin-left:5px;"><i class="fa fa-floppy-o"></i> Save</button>
                    <a href="<?= site_url('kontrak') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>