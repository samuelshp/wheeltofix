<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left">
        <a href="<?= site_url('kontrak') ?>">
            <button class="btn btn-primary"><i class="fa fa-plus"></i></button>
        </a>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover">
            <thead>
                <tr>
                    <th>Nama Project</th>
                    <th>Owner</th>
                    <th>Tanggal Proyek</th>
                    <th>Nilai Total Kontrak</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            // Display data in the table
            if(!empty($kontrakList)):
                foreach($kontrakList as $key => $kontrak):  
            ?>  
                <tr>
                    <td><?=$kontrak['kont_name']?></td>
                    <td><?=$kontrak['cust_name'] ? $kontrak['cust_name'] : 'Not Set' ?></td>
                    <td><?=$kontrak['kont_date']?></td>
                    <td><?=setPrice($kontrak['job_value'])?></td>
                    <td>
                        <a href="<?=site_url(array('kontrak/view',$kontrak['id']))?>" style="margin: 0">
                            <button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button>
                        </a>
                    </td>
                </tr>
        <?php
                endforeach;
            endif; 
        ?>
            </tbody>
        </table>
    </div>
</div>