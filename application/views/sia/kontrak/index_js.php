<script>
deleteObject = (id) => {
    return $.ajax({
        url: "<?= site_url('kontrak/delete') ?>",
        type: 'POST',
        data: {id: id}
    });
}
   
function deleteButtonHandler(e)
{
    swal({
        title: "Warning",
        text: 'Are you sure you want to delete this data? \nthis action will also delete any data related to this data.',
        buttons: {
            cancel: "Cancel",
            delete: {text: "Delete"},
        },
        icon: "warning"
    }).then(data => {
        if(data){
            deleteObject($(this).data('id')).then((resp) => {
                console.log(resp);
                if(resp.status == 'ok'){
                    jwTable
                        .row($(this).parents('tr'))
                        .remove()
                        .draw();

                    swal("Success", "Data successfully deleted", "success");
                }
            }).fail(function(){
                swal("Error", "fail to delete data", "error");
            });     
        }
    });
}

$(function() {
    $('.data-table tbody').on('click', '.delete-object-button', deleteButtonHandler);

});
</script>