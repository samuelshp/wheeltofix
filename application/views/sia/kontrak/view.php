<?php
    $strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
    $arrBreadcrumb = array(
        0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
        1 => array('title' => '<i class="fa fa-list"></i> Kontrak', 'link' => '')
    );

    include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<form class="col-xs-12" method="post">
    <a href="<?= site_url('kontrak') ?>">
    </a>
    <p class="spacer">&nbsp;</p>

    <div class="row">
        <div class="col-sm-12 col-md-6">

            <div class="form-group">
                <label>Nama Proyek</label>
                <input id="kont-name" type="text" class="form-control required" name="kont_name" placeholder="Nama Proyek" value="<?= repopulate('kont_name', !empty($object['kont_name']) ? $object['kont_name'] : '') ?>">
            </div>

            <div class="form-group">
                <label>Owner</label>
                <select id="owner-id" class="form-control required" name="owner_id">
                    <option>-- Pilih Owner --</option><?php
foreach($availableOwner as $owner): ?>
                    <option value="<?=$owner['id']?>"<?=!empty($object['owner_id']) && $object['owner_id'] == $owner['id'] ? 'selected' :  ''?>><?=$owner['cust_name']?></option><?php 
endforeach; ?>
                </select>
            </div>

        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Tanggal Proyek</label>
                <input id="kont-date" type="date" class="form-control required" name="kont_date" placeholder="Tanggal Proyek" value="<?= repopulate('kont_date', !empty($object['kont_date']) ? $object['kont_date'] : '') ?>">
            </div><?php
if(!empty($id)): ?>  
            <div class="form-group">
                <label>Nilai Total Proyek</label>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="kont-value" type="text" class="form-control currency" name="kont_value" placeholder="Nilai Proyek" value="<?=$object['job_value']?>" disabled>
                </div>
            </div>
            <div class="form-group">
                <label>Nilai Total Terbayar</label>
                <div class="input-group">
                    <div class="input-group-addon">Rp.</div>
                    <input id="terbayar" type="text" class="form-control currency" placeholder="Nilai Terbayar" value="<?= $terbayar ?>" disabled>
                </div>
            </div><?php
endif; ?>  
        </div>
    </div>
    <br>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12 text-right"><?php
if(empty($id)): ?>  
                <button type="submit" class="btn btn-success" style="margin-right:5px;" name="subSave" value="save"><i class="fa fa-floppy-o"></i> Save</button><?php
else: ?>  
                <button type="submit" class="btn btn-warning" style="margin-right: 5px" name="subSave" value="edit"><i class="fa fa-pencil-square-o"></i> Edit</button>
                <button type="submit" class="btn btn-danger" style="margin-right: 5px" name="subDelete" value="delete"><i class="fa fa-trash-o"></i> Delete</button><?php
endif; ?>  
                <a href="<?= site_url('kontrak/browse') ?>"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</button></a>
            </div>
        </div>
    </div>
</form>