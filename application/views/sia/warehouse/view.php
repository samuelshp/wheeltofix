<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title'), 'link' => site_url('warehouse/browse')),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <form name="frmViewWarehouse" id="frmViewWarehouse" method="post" action="<?=site_url('warehouse/', NULL, FALSE)?>" class="frmShop"> 
    <div class="row">
        <div class="col-md-12">            
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Detail <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')?></h3></div>
                <div class="panel-body">
                    <input type="hidden" name="intID" value="<?=$intID?>"/>
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-name')?> *  
                        </div>
                        <div class="col-md-8 col-lg-10">                          
                            <input type="text" name="ware_name" class="form-control required input-sm" value="<?=$arrWarehouse['ware_name']?>" disabled/> 
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-stock-warning')?> *  
                        </div>
                        <div class="col-md-8 col-lg-10">                          
                            <label class="radio-inline"><input type="radio" name="ware_stock_warn" id="ware_stock_warn-1" value="1" <?=($arrWarehouse['ware_stock_warning'] == 1 ? "checked" : null)?> disabled> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="ware_stock_warn" id="ware_stock_warn-2" value="2" <?=($arrWarehouse['ware_stock_warning'] == 2 ? "checked" : null)?> disabled> Active</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-status')?>
                        </div>
                        <div class="col-md-8 col-lg-10">
                            <label class="radio-inline"><input type="radio" name="ware_status" id="ware_status-1" value="1" <?=($arrWarehouse['ware_status'] == 1 ? "checked" : null)?> disabled> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="ware_status" id="ware_status-2" value="2" <?=($arrWarehouse['ware_status'] == 2 ? "checked" : null)?> disabled> Active</label>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">            
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Inventori dalam <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')?></h3></div>
                <div class="panel-body">
                    <?=$strPage?>
                    <div class="table-responsive" style="min-height:10px; max-height:300px;overflow:auto"><table class="table table-bordered table-condensed table-hover">
                        <thead><tr>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-product-name')?></th>
                            <?php if($arrWarehouse['ware_stock_warning'] == 2): ?>
                                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-stock-warning')?></th>
                            <?php endif;?>
                            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-status')?></th>
                        </tr></thead>
                        <tbody><?php                    
                    if(!empty($arrInventories)):
                        foreach($arrInventories as $e): ?>  
                            <tr>
                                <td><?=$e['prod_title']?></td>
                                <?php if($arrWarehouse['ware_stock_warning'] == 2): ?>
                                    <td></td>
                                <?php endif; ?>
                                <td><?=($e['prod_status'] == 2 ? "Aktif" : "Non-Aktif")?></td>                                
                            </tr><?php
                        endforeach;
                    else: ?>
                            <tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
                    endif; ?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">                
                <button type="button" id="btnEditWarehouse" class="btn btn-primary"><i class="fa fa-edit"></i></button>                
                <button type="submit" id="btnUpdateWarehouse" name="smtUpdateWarehouse" value="Update WH" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                <button type="submit" id="btnDeleteWarehouse" name="smtDeleteWarehouse" value="Delete WH" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>  
            </div>
        </div>
    </div>

    </form>
</div>