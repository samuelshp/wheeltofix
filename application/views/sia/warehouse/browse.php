<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-warehouse'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('warehouse/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">   
    <div class="input-group" style="max-width:250px;">        
        <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-searchbyname')?>" />
        <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
    </div>
</form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;">       
        <a href="<?=site_url('warehouse/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a>
        <?=$strBrowseMode?>
    </div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-name')?></th>        
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-stock-warning')?></th>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-status')?></th>
        <th class="action"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-action')?></th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrWarehouse)):
    foreach($arrWarehouse as $e): ?>  
        <tr>
            <td><?=$e['ware_name']?></td>
            <td><?=($e['ware_stock_warning'] == 2 ? "Aktif" : "Non-Aktif")?></td>
            <td><?=($e['ware_status'] == 2 ? "Aktif" : "Non-Aktif")?></td>
            <td class="action">
                 <a href="<?=site_url('warehouse/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a>
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>