<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title'), 'link' => site_url('warehouse/browse')),
    2 => array('title' => '<i class="fa fa-eye"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12">
    <form name="frmAddWarehouse" id="frmAddWarehouse" method="post" action="<?=site_url('warehouse/', NULL, FALSE)?>" class="frmShop"> 
    <div class="row">
        <div class="col-md-12">            
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">Add New <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')?></h3></div>
                <div class="panel-body">                   
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-name')?> *  
                        </div>
                        <div class="col-md-8 col-lg-10">                          
                            <input type="text" name="ware_name" class="form-control required input-sm" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-name')?>" /> 
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-stock-warning')?> *  
                        </div>
                        <div class="col-md-8 col-lg-10">
                            <select class="form-control chosen" name="ware_stock_warn">
                                <option value="2">Yes</option>
                                <option value="1">No</option>
                            </select>
                            <!-- <label class="radio-inline"><input type="radio" name="ware_stock_warn" id="ware_stock_warn-1" value="1"> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="ware_stock_warn" id="ware_stock_warn-2" value="2"> Active</label> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-4 col-lg-2 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'), 'warehouse-status')?>
                        </div>
                        <div class="col-md-8 col-lg-10">
                            <label class="radio-inline"><input type="radio" name="ware_status" id="ware_status-1" value="1" > Inactive</label>
                            <label class="radio-inline"><input type="radio" name="ware_status" id="ware_status-2" value="2" > Active</label>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>    

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">                
                <button type="submit" name="smtAddWarehouse" class="btn btn-success pull-right" value="save" style="margin-left:5px;"><i class="fa fa-floppy-o"></i> Save</button>
                <a href="<?= site_url('warehouse/browse') ?>"><button type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Back</button></a>
            </div>
        </div>
    </div>

    </form>
</div>