<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {

var selectedInvoiceID=$('#idItemAwal').val().split(",");
var unSelectedInvoiceID=[];
var totalItem=0;
var numberItem=0;
var totalItemAwal=$("#totalItemAwal").val();
var numberItemAwal=$("#totalItemAwal").val();
var totalBonusItem=$("#totalItemBonusAwal").val();
var numberItemBonus=totalBonusItem;
$(".jwDateTime").calendar({dateFormat: 'DMY/',timeSeparators:[' '],yearRange: '-01:+04'}); /*  Untuk datetime timeSeparators:[' ',':'] */

$("#frmChangeDelivery").submit(function() {
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});

$( "input[name*='cbDeleteAwal']").change(function(){
	var at1=this.name.indexOf("[");
	var at2=this.name.indexOf("]");
	var at=this.name.substring(at1+1,at2);
	var subtotal=$("#subTotal"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',$("#subTotalNoTax").autoNumeric('get')-subtotal);
	numberItemAwal--;
	if((numberItemAwal+numberItem)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	unSelectedInvoiceID.push(at);
	$.ajax({
		url: "<?=site_url('invoice_ajax/getInvoiceItemByDeliveryItemID', NULL, FALSE)?>/" + at,
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$.map(xml.find('Item').find('item'),function(val,i){
				var idItem=$(val).find('id').text();
				var name=$(val).find('name').text();
				for(var zz=0;zz<totalBonusItem;zz++){
					if($("#idItemBonusX"+zz).val()==idItem){
						$("#qty1ItemBonusX"+zz).val(parseInt($("#qty1ItemBonusX"+zz).val())-parseInt($(val).find('qty1').text()));
						$("#qty2ItemBonusX"+zz).val(parseInt($("#qty2ItemBonusX"+zz).val())-parseInt($(val).find('qty2').text()));
						$("#qty3ItemBonusX"+zz).val(parseInt($("#qty3ItemBonusX"+zz).val())-parseInt($(val).find('qty3').text()));
						var text=$("#qty1ItemBonusX"+zz).val()+" "+$("#unit1ItemBonusX"+zz).val()+" + "+$("#qty2ItemBonusX"+zz).val()+" "+$("#unit2ItemBonusX"+zz).val()+" + "+$("#qty3ItemBonusX"+zz).val()+" "+$("#unit3ItemBonusX"+zz).val();
						$("#labelQtyBonusX"+zz).text(text);
						if($("#qty1ItemBonusX"+zz).val() == 0 && $("#qty2ItemBonusX"+zz).val() == 0 && $("#qty3ItemBonusX"+zz).val() ==0){
							$("#qty1ItemBonusX"+zz).closest("tr").remove();
							numberItemBonus--;
							if(numberItemBonus=='0'){
								$("#selectedItemsBonus tbody").append(
									'<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
								);
							}
						}
					}
				}
			});
		}
	});
	selectedInvoiceID = jQuery.grep(selectedInvoiceID, function(value) {
		return value != at;
	});
	$(this).closest("tr").remove();
});

$("#invoiceItemList").on("click","input[type='checkbox'][name*='cbDeleteX']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	var subtotal=$("#totalSaleX"+at).autoNumeric('get');
	$("#subTotalNoTax").autoNumeric('set',$("#subTotalNoTax").autoNumeric('get')-subtotal);
	numberItem--;
	if((numberItemAwal+numberItem)=='0'){
		$("#invoiceItemList tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	$.ajax({
		url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + $("#idSaleX"+at).val(),
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$.map(xml.find('Item').find('item'),function(val,i){
				var idItem=$(val).find('id').text();
				for(var zz=0;zz<totalBonusItem;zz++){
					if($("#idItemBonusX"+zz).val()==idItem){
						$("#qty1ItemBonusX"+zz).val(parseInt($("#qty1ItemBonusX"+zz).val())-parseInt($(val).find('qty1').text()));
						$("#qty2ItemBonusX"+zz).val(parseInt($("#qty2ItemBonusX"+zz).val())-parseInt($(val).find('qty2').text()));
						$("#qty3ItemBonusX"+zz).val(parseInt($("#qty3ItemBonusX"+zz).val())-parseInt($(val).find('qty3').text()));
						var text=$("#qty1ItemBonusX"+zz).val()+" "+$("#unit1ItemBonusX"+zz).val()+" + "+$("#qty2ItemBonusX"+zz).val()+" "+$("#unit2ItemBonusX"+zz).val()+" + "+$("#qty3ItemBonusX"+zz).val()+" "+$("#unit3ItemBonusX"+zz).val();
						$("#labelQtyBonusX"+zz).text(text);
						if($("#qty1ItemBonusX"+zz).val() == 0 && $("#qty2ItemBonusX"+zz).val() == 0 && $("#qty3ItemBonusX"+zz).val() ==0){
							$("#qty1ItemBonusX"+zz).closest("tr").remove();
							numberItemBonus--;
							if(numberItemBonus=='0'){
								$("#selectedItemsBonus tbody").append(
									'<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
								);
							}
						}
					}
				}
			});
		}
	});
	selectedInvoiceID = jQuery.grep(selectedInvoiceID, function(value) {
		return value != $("#idSaleX"+at).val();
	});
	$(this).closest("tr").remove();
});

$("#txtNewSale").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		var unselected='';
		for(var i=0;i<unSelectedInvoiceID.length;i++){
			unselected+=unSelectedInvoiceID[i]+'-';
		}
		unselected+='0';
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewSale"]').val()+"/"+unselected,
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Invoice').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strCode=$(val).find('invo_code').text();
					var strName=strCode+", "+$(val).find('cust_name').text();
					if($.inArray(intID, selectedInvoiceID) > -1){

					}else{
						display.push({label: strName, value:'',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Invoice').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItem;
		if(numberItem + numberItemAwal=='0'){
			$("#invoiceItemList tbody").html('');
		}
		$("#invoiceItemList tbody").append(
			'<tr>'+
				'<td class="cb"><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
                '<td><label id="codeSale'+i+'"></label></td>'+
				'<td><label id="dateSale'+i+'"></label></td>'+
				'<td><label id="nameSale'+i+'"></label></td>'+
				'<td><label id="addressSale'+i+'"></label></td>'+
				'<td><label id="statusSale'+i+'"></label></td>'+
				'<td class="subTotal currency"><?/*=str_replace(" 0","",setPrice("","USED"))*/?><label id="totalSaleX'+i+'" ></label></td>'+
				'<input type="hidden" id="idSaleX'+i+'" name="idSale'+i+'">'+
				'</tr>'
		);

		var date = $(selectedPO).find('cdate').text();
		var name = $(selectedPO).find('cust_name').text();
		var address = $(selectedPO).find('cust_address').text()+", "+$(selectedPO).find('cust_city').text();
		var status=$(selectedPO).find('invoice_status').text();
        var code=$(selectedPO).find('invo_code').text();
		var id = $(selectedPO).find('id').text();
		selectedInvoiceID.push(id);
        $("#codeSale"+i).text(code);
		$("#dateSale"+i).text(date);
		$("#nameSale"+i).text(name);
		$("#addressSale"+i).text(address);
		$("#statusSale"+i).text(status);
		$("#idSaleX"+i).val(id);
		var price=0;
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + id,
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				/* var x=totalItem-1; */
				var total =xml.find('Total').text();
				$("#totalSaleX"+i).text(total);
				$("#totalSaleX"+i).autoNumeric('init', autoNumericOptionsRupiah);
				var grandTotal=parseFloat($("#subTotalNoTax").autoNumeric('get'))+parseFloat(total);
				$("#subTotalNoTax").autoNumeric('set',grandTotal);
				$.map(xml.find('Item').find('item'),function(val,i){
					var idItem=$(val).find('id').text();
					var flag=0;
					for(var zz=0;zz<totalBonusItem;zz++){
						if($("#idItemBonusX"+zz).val()==idItem){
							$("#qty1ItemBonusX"+zz).val(parseInt($("#qty1ItemBonusX"+zz).val())+parseInt($(val).find('qty1').text()));
							$("#qty2ItemBonusX"+zz).val(parseInt($("#qty2ItemBonusX"+zz).val())+parseInt($(val).find('qty2').text()));
							$("#qty3ItemBonusX"+zz).val(parseInt($("#qty3ItemBonusX"+zz).val())+parseInt($(val).find('qty3').text()));
							var text=$("#qty1ItemBonusX"+zz).val()+" "+$("#unit1ItemBonusX"+zz).val()+" + "+$("#qty2ItemBonusX"+zz).val()+" "+$("#unit2ItemBonusX"+zz).val()+" + "+$("#qty3ItemBonusX"+zz).val()+" "+$("#unit3ItemBonusX"+zz).val();
							$("#labelQtyBonusX"+zz).text(text);
							flag=1;
						}
					}
					if(flag==0){
						if(numberItemBonus=='0'){
							$("#selectedItemsBonus tbody").html('');
						}
						var i=totalBonusItem;
						var nameItem=$(val).find('name').text();
						var q1=$(val).find('qty1').text();
						var q2=$(val).find('qty2').text();
						var q3=$(val).find('qty3').text();
						var unit1=$(val).find('unit1name').text();
						var unit2=$(val).find('unit2name').text();
						var unit3=$(val).find('unit3name').text();
						$("#selectedItemsBonus tbody").append(
							'<tr>'+
								'<td class="qty" id="labelQtyBonusX'+i+'"><div class="form-group">'+
								'</div></td>'+
								'<td><label id="nmeItemBonusX'+i+'">'+nameItem+'</label></td>'+
								'<input type="hidden" id="idItemBonusX'+i+'" value="'+idItem+'">'+
								'<input type="hidden" id="qty1ItemBonusX'+i+'" value="'+q1+'">'+
								'<input type="hidden" id="qty2ItemBonusX'+i+'" value="'+q2+'">'+
								'<input type="hidden" id="qty3ItemBonusX'+i+'" value="'+q3+'">'+
								'<input type="hidden" id="unit1ItemBonusX'+i+'" value="'+unit1+'">'+
								'<input type="hidden" id="unit2ItemBonusX'+i+'" value="'+unit2+'">'+
								'<input type="hidden" id="unit3ItemBonusX'+i+'" value="'+unit3+'">'+
								'</tr>');

						numberItemBonus++;
						totalBonusItem++;
						var text=$("#qty1ItemBonusX"+i).val()+" "+$("#unit1ItemBonusX"+i).val()+" + "+$("#qty2ItemBonusX"+i).val()+" "+$("#unit2ItemBonusX"+i).val()+" + "+$("#qty3ItemBonusX"+i).val()+" "+$("#unit3ItemBonusX"+i).val();
						$("#labelQtyBonusX"+i).text(text);
					}
				});
			}
		});
		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);
	}
});

$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".subTotal").autoNumeric('init', autoNumericOptionsRupiah);

});</script>