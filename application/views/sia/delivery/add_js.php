<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('input[name="txtDateSent"]').datetimepicker({
    dateFormat: "yy/mm/dd"
});
var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var mode=0;
var totalItem=0;
var totalItemBonus=0;
var numberItem=0;
var numberItemBonus=0;
var carriedIDItem=[];
var carriedQty1Item=[];
var carriedQty2Item=[];
var carriedQty3Item=[];
var carriedNameItem=[];
var flagdelete=[]
$('tr.txtData').hide();
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
function reset(){
	$("#txtTax").val(0);
	totalItem=0;
	numberItem=0;
	totalItemBonus=0;
	numberItemBonus=0;
}
function setManual(){
	$("#txtDelivererAddress").prop('disabled', true);
	$("#txtDelivererName").prop('disabled', true);
	$("#txtDelivererPhone").prop('disabled', true);
	mode=0; /* 0 manual 1 auto */
	$("#addNewSale").show();
	reset();
	var i=0;
};
setManual();
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddDelivery").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddDelivery").submit(function() {
	if($("#txtWarehouse").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>"); return false;
	}
	if($("#txtDeliverer").val() == ''){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectdeliverer')?>"); return false;
	}
	if(numberItem<1){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-pleaseselectinvoice')?>"); return false;
	}
	/*var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});*/
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
	
	return true;
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewSale").children("option:selected").val() > 0) {
		if($("#hidNewSale").val() == '') $("#hidNewSale").val($("#selNewSale").children("option:selected").val());
		else $("#hidNewSale").val($("#hidNewSale").val() + '; ' + $("#selNewSale").children("option:selected").val());
		$("#selNewSale").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewSale").val() != '') {
		strNewSale = $("#hidNewSale").val(); arrNewSale = strNewSale.split("; ");
		$(this).attr("title",arrNewSale.length + ' Item(s)');
	}
});

var $arrSelectedPO;
var selectedPO;


$("#supplierName").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('purchase_order/getDeliveryOrderSupplierAutoComplete', NULL, FALSE)?>/" + $("#supplierName").val(),
			beforeSend: function() {
				$("#loadSupp").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSupp").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Supplier').find('item'),function(val,i){
					var name = $(val).find('supp_name').text();
					var address = $(val).find('supp_address').text();
					var city = $(val).find('supp_city').text();
                    var code = $(val).find('supp_code').text();
					var strName=name+", "+ address +", "+city;
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ strName, value:"("+code+") "+ name,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Supplier').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#supplierID").text($(selectedPO).find('id').text());
		$("#supplierAddress").text($(selectedPO).find('supp_address').text());
		$("#supplierCity").text($(selectedPO).find('supp_city').text());
		$("#supplierPhone").text($(selectedPO).find('supp_phone').text());
		$('input[name="supplierID"]').val($(selectedPO).find('id').text());
	}
});

$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
	$(this).autoNumeric('init', autoNumericOptionsRupiah);
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	$.ajax({
		url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + $("#idSaleX"+at).val(),
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$arrSelectedPO = xml;
			/* var x=totalItem-1; */
			var total =xml.find('Total').text();
			var grandTotal = parseInt($("#subTotalTax").autoNumeric('get')) - parseInt(total);
			$("#subTotalTax").autoNumeric('set',grandTotal);
			$.map(xml.find('Item').find('item'),function(val,i){
				var idItem=$(val).find('id').text();
				for(var zz=0;zz<carriedIDItem.length;zz++){
					if(carriedIDItem[zz]==idItem){
						carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])-(parseInt($(val).find('qty1').text()));
						carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])-(parseInt($(val).find('qty2').text()));
						carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])-(parseInt($(val).find('qty3').text()));
						if(carriedQty1Item[zz] == 0 && carriedQty2Item[zz] == 0 && carriedQty3Item[zz] ==0){
							flagdelete[zz]=1;
							$("#qty1ItemBonusX"+zz).closest("tr").remove();
							numberItemBonus--;
							if(numberItemBonus=='0'){
								$("#selectedItemsBonus tbody").append(
									'<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
								);
							}
						}else{
							$("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
							$("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
							$("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
						}
					}
				}
			});
            for(var zz=0;zz<carriedIDItem.length;zz++){
                if(flagdelete[zz]==1){
                    carriedIDItem.splice(zz,1);
                    carriedNameItem.splice(zz,1);
                    carriedQty1Item.splice(zz,1);
                    carriedQty2Item.splice(zz,1);
                    carriedQty3Item.splice(zz,1);
                    flagdelete.splice(zz,1);
                    zz--;
                }
            }
		}
	});
	selectedInvoiceID = jQuery.grep(selectedInvoiceID, function(value) {
		return value != $("#idSaleX"+at).val();
	});
	$(this).closest("tr").remove();
});

var selectedInvoiceID=[];
$("#txtNewSale").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewSale"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Invoice').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strCode=$(val).find('invo_code').text();
					var strName=strCode+", "+$(val).find('cust_name').text();
					if($.inArray(intID, selectedInvoiceID) > -1){

					}else{
						display.push({label: strName, value:'',id:intID});
					}
				});
                if(display.length>0){
				    response(display);
                }
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Invoice').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItem;
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td><label id="dateSaleX'+i+'"></label></td>'+
				'<td><label id="nameSaleX'+i+'"></label></td>'+
				'<td><label id="addressSaleX'+i+'"></label></td>'+
				'<td><label id="statusSaleX'+i+'"></label></td>'+
				'<td><?=str_replace(" 0","",setPrice("","USED"))?> <label id="totalSaleX'+i+'" ></label></td>'+
				'<input type="hidden" id="idSaleX'+i+'" name="idSale'+i+'">'+
				'</tr>'
		);

		var date = $(selectedPO).find('cdate').text();
		var name = $(selectedPO).find('cust_name').text();
		var address = $(selectedPO).find('cust_address').text()+", "+$(selectedPO).find('cust_city').text();
		var status=$(selectedPO).find('invoice_status').text();
		var id = $(selectedPO).find('id').text();
		selectedInvoiceID.push(id);
		$("#dateSaleX"+i).text(date);
		$("#nameSaleX"+i).text(name);
		$("#addressSaleX"+i).text(address);
		$("#statusSaleX"+i).text(status);
		$("#idSaleX"+i).val(id);
		var price=0;
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + id,
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				/* var x=totalItem-1; */
				var total =xml.find('Total').text();
				$.map(xml.find('Item').find('item'),function(val,i){
					var idItem=$(val).find('id').text();
					var flag=0;
					for(var zz=0;zz<carriedIDItem.length;zz++){
						if(carriedIDItem[zz]==idItem){
							carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])+(parseInt($(val).find('qty1').text()));
							carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])+(parseInt($(val).find('qty2').text()));
							carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])+(parseInt($(val).find('qty3').text()));
							$("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
							$("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
							$("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
							flag=1;
						}
					}
					if(flag==0){
						var nameItem=$(val).find('name').text();
						var q1=$(val).find('qty1').text();
						var q2=$(val).find('qty2').text();
						var q3=$(val).find('qty3').text();
						carriedIDItem.push(idItem);
						carriedQty1Item.push(q1);
						carriedQty2Item.push(q2);
						carriedQty3Item.push(q3);
						carriedNameItem.push(nameItem);
                        flagdelete.push(0);
						var i=totalItemBonus;
						if(numberItemBonus=='0'){
							$("#selectedItemsBonus tbody").html('');
						}
						$("#selectedItemsBonus tbody").append(
							'<tr>'+
								'<td class="qty"><div class="form-group">'+
								'<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" /></div>' +
								'<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" /></div>' +
								'<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" /></div>'+
								'</div></td>'+
								'<td><label id="nmeItemBonusX'+i+'"></label></td>'+
								'<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
								'<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
								'<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
								'<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
								'<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
								'<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
								'</tr>');
						numberItemBonus++;
						totalItemBonus++;
						$("#qty1ItemBonusX"+i).val(q1);
						$("#qty2ItemBonusX"+i).val(q2);
						$("#qty3ItemBonusX"+i).val(q3);
						$("#nmeItemBonusX"+i).text(nameItem);
					}
				});
				$("#totalSaleX"+i).text(total);
				$("#totalSaleX"+i).autoNumeric('init', autoNumericOptionsRupiah);
				var grandTotal = parseInt($("#subTotalTax").autoNumeric('get')) + parseInt(total);
				$("#subTotalTax").autoNumeric('set',grandTotal);
			}
		});
		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);
	}
});
$("#txtWarehouse").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				if(display.length == 1) {
					setTimeout(function() {
	                    $("#txtWarehouse").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
	                    $('#txtWarehouse').autocomplete('close');
	                }, 100);
                }
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedPO).find('id').text());
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});
setTimeout(function() {
    $("#txtWarehouse").val(primaryWarehouseName);
    $("#txtWarehouse").autocomplete('search');
},500);

$("#txtDeliverer").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getSalesmanAutoComplete', NULL, FALSE)?>/" + $('#txtDeliverer').val(),
			beforeSend: function() {
				$("#loadDeliverer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadDeliverer").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Salesman').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strCode=$(val).find('sals_code').text();
					var strName=$(val).find('sals_name').text();
					display.push({label:"("+strCode+") "+ strName, value:"("+strCode+") "+ strName,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Salesman').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$('tr.txtData').show();
		var id=$(selectedPO).find('id').text();
		var name=$(selectedPO).find('sals_name').text();
		var address=$(selectedPO).find('sals_address').text();
		var phone=$(selectedPO).find('sals_phone').text();
		$("#txtDelivererID").val(id);
		$("#txtDelivererName").val(name);
		$("#txtDelivererAddress").val(address);
		$("#txtDelivererPhone").val(phone);
		$("#delivererID").val(id);
        $("#loadDeliverer").html('<i class="fa fa-check"></i>');
	}
});

});</script>