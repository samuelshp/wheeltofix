<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('account_start', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
            <!-- <th><?//=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-code')?></th> -->            
            <th>
            <a href="<?='?per_page='.$intPerPage.'&order_by='.$strOrderType?>">Periode</a>
            <i class="fa fa-sort-<?=$strOrder?> pull-right"></i>
            </th>
            <th class="action">Action</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrAccountStart)):    
    foreach($arrAccountStart as $e): ?>
        <tr>            
            <td><?=formatDate2($e['acst_period'],'d F Y')?></td>            
            <td class="action">
                <?php if($e['bolAllowView']): ?><a href="<?=site_url('account_start/view/'.$e['acst_period'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
                <?php if($e['bolBtnPrint']): ?><a href="<?=site_url('account_start/view/'.$e['acst_period'].'?print=true', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>  
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
</div>