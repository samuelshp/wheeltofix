<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript">
    
    $("input[name^=amountOfDebet]").on('keyup', calculateDebet);
    $("input[name^=amountOfCredit]").on('keyup', calculateCredit);
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  

    $(document).ready(function(){
        $("#btnUpdateAccountStart").hide();
    });
    
    $("#btnEditAccountStart").click(function(){                
        var input = $("input.currency");
        input.removeAttr('readonly');        
        input.each(function(){            
           $(this).autoNumeric('set',$(this).data('raw'));
        });        
        
        input.blur(function(){
            if($(this).val() == ''){
                $(this).val('0');
            }
        });
        
        $(this).hide();
        $("#btnUpdateAccountStart").show();
    });

    $("#frmUpdateAccountStart").submit(function(){

        var val = $("button[type=submit][clicked=true]").val();
        
        if(val == "Delete Account Start"){
            msg = "Apakah anda yakin akan menghapus Account Start periode ini?";    
        }else if(val == "Update Account Start"){
            msg = "Simpan perubahan untuk Account Start periode ini?";    
        }

        return confirm(msg);
        
    });

    $("#frmUpdateAccountStart button[type=submit]").click(function() {
        $("button[type=submit]", $(this).parents("#frmUpdateAccountStart")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });

    function calculateDebet() {
        var debetTotal = 0;
        $("input[name^=amountOfDebet]").each(function(){
            debetTotal += parseFloat($(this).autoNumeric('get'));
        });

        $("input[name=amountTotalOfDebet]").autoNumeric('set',debetTotal);
    }

    function calculateCredit() {
        var creditTotal = 0;
        $("input[name^=amountOfCredit]").each(function(){
            creditTotal += parseFloat($(this).autoNumeric('get'));
        });

        $("input[name=amountTotalOfCredit]").autoNumeric('set',creditTotal);
    }
</script>