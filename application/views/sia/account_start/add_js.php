<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">$(document).ready(function() {

var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';

$('.currency').autoNumeric('init', autoNumericOptionsRupiah);

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$('.currency').on('blur', function(){
    if($(this).val() == ''){
        $(this).val('0,00');
    }
});

// $("#frmAddPurchase").validate({
// 	rules:{},    
// 	messages:{},
//     showErrors: function(errorMap, errorList) {
//         $.each(this.validElements(), function (index, element) {
//             var $element = $(element);
//             $element.data("title", "").removeClass("error").tooltip("destroy");
//             $element.closest('.form-group').removeClass('has-error');
//         });
//         $.each(errorList, function (index, error) {
//             var $element = $(error.element);
//             $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
//             $element.closest('.form-group').addClass('has-error');
//         });
//     },
// 	errorClass: "input-group-addon error",
// 	errorElement: "label",
// 	highlight:function(element, errorClass, validClass) {
// 		$(element).parents('.control-group').addClass('error');
// 	},
// 	unhighlight: function(element, errorClass, validClass) {
// 		$(element).parents('.control-group').removeClass('error');
// 		$(element).parents('.control-group').addClass('success');
// 	}
// });

$("#frmAddPurchase").submit(function() {
    // if($('#selPB').val() == null) {
    //     alert("PB tidak boleh kosong");
    //     return false;
    // }
    // if($('#selSupp').val() == null) {
    //     alert("Supplier tidak boleh kosong");
    //     return false;
    // }
	// var form = $(this);
    // $('.submited').prop('disabled', false);
	// $("#txtSJCode").prop('disabled', false);
	// $("#txtEkspedition").prop('disabled', false);
	// $('.currency').each(function(i){
	// 	var self = $(this);
	// 	try{
	// 		var v = self.autoNumeric('get');
	// 		self.autoNumeric('destroy');
	// 		self.val(v);
	// 	}catch(err){
	// 		console.log("Not an autonumeric field: " + self.attr("name"));
	// 	}
	// });
	
	// if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
	
	// return true;
});

$('.chosen').chosen();

});</script>