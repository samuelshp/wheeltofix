<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-reply"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-accountstart'), 'link' => site_url('account_start/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">

<div class="col-xs-12"><form name="frmAddAccountStart" id="frmAddAccountStart" method="post" action="<?=site_url('account_start', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <!-- <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div> -->
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-period')?></h3></div>
                <div class="panel-body">
                    <!-- Dibuat -->
                    <!-- <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="date" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div> -->
                    <input type="text" id="txtPeriod" name="txtPeriod" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
                </div>
            </div>
        </div>

    </div>

    <!-- Selected Items -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-accounts')?>
            </h3>
        </div>
        <div class="panel-body">            
            <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
                <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-code')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-name')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-debet')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-credit')?></th>
                </tr>
                </thead>
                <tbody>                    
                    <?php foreach($arrMasterAccount as $e):?>
                    <tr>
                    <td><?=$e['acco_code']?></td>
                    <td><?=$e['acco_name']?></td>
                    <td>                    
                        <div class="input-group">
                            <label class="input-group-addon">Rp.</label>
                            <input type="text" name="amountOfDebet[]" class="form-control required currency" placeholder="0"/>
                        </div>                    
                    </td>
                    <td>
                        <div class="input-group">
                            <label class="input-group-addon">Rp.</label>
                            <input type="text" name="amountOfCredit[]" class="form-control required currency" placeholder="0"/>
                        </div>                    
                    </td>
                    </tr>                    
                    <?php endforeach; ?>                    
                    <!-- <tr class="info"><td class="noData" colspan="9"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr> -->
                </tbody>
            </table></div>

            <p class="spacer">&nbsp;</p>
        </div><!--/ Table Selected Items -->
    </div>	

    <div class="form-group"><button type="submit" name="smtSaveAccountStart" value="Save Account Start" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

<input type="hidden" id="totalItem" name="totalItem"/>
<input type="hidden" id="totalItemBonus" name="totalItemBonus"/>

</form></div>




