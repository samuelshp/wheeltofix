<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-phone"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-accountstart'), 'link' => site_url('account_start/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<div class="col-xs-12">
    <div class="row">
        <div class="col-md-12">
        <form name="frmUpdateAccountStart" id="frmUpdateAccountStart" method="post" action="<?=site_url('account_start', NULL, FALSE)?>" class="frmShop">
            <div class="panel panel-primary">
                <!-- <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div> -->
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa fa-info-circle"></i> Summary</h3></div>
                <div class="panel-body">                    
                    <div class="row">
                        <!-- <div class="col-sm-2 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div> -->
                        <div class="col-sm-2 tdTitle">Periode</div>
                        <div class="col-sm-10 tdDesc">
                            <?=formatDate2($arrAccountStartItems[0]['acst_period'],'d F Y')?>
                            <input type="hidden" name="period" value="<?=$arrAccountStartItems[0]['acst_period']?>"/>
                        </div>                       
                        <p class="spacer">&nbsp;</p> 
                        <div class="col-sm-2 tdTitle">Pembuat</div>
                        <div class="col-sm-10 tdDesc"><?=$arrAccountStartItems[0]['adlg_name']?></div>                                                
                        <p class="spacer">&nbsp;</p>
                        <p class="spacer">&nbsp;</p>
                        <div class="col-sm-12">
                            <div class="table-responsive">                            
                                <table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
                                    <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Nama</th>
                                            <th>Debet</th>
                                            <th>Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total_debit = 0; $total_credit = 0; foreach($arrAccountStartItems as $e):?>
                                            <tr>
                                                <td><?=$e['acco_code']?></td>
                                                <td><?=$e['acco_name']?></td>
                                                <td>
                                                    <input type="text" name="amountOfDebet[]" class="form-control required currency" value="<?=setPrice($e['acst_debet'])?>" data-raw="<?=$e['acst_debet']?>" readonly/>
                                                </td>
                                                <td>
                                                    <input type="text" name="amountOfCredit[]" class="form-control required currency" value="<?=setPrice($e['acst_credit'])?>" data-raw="<?=$e['acst_credit']?>"  readonly/>
                                                </td>
                                            </tr>
                                        <?php $total_debit += (Double) $e['acst_debet']; $total_credit += (Double)$e['acst_credit']; endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td>TOTAL</td>
                                            <td><input type="text" name="amountTotalOfDebet" class="form-control required currency" value="<?=setPrice($total_debit)?>" data-raw="<?=$total_debit?>" disabled/></td>
                                            <td><input type="text" name="amountTotalOfCredit" class="form-control required currency" value="<?=setPrice($total_credit)?>" data-raw="<?=$total_credit?>" disabled/></td>
                                        </tr>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-md-12">  
                            <?php if($arrBtnAllow['bolAllowView']): ?>                        
                            <button type="button" name="editAccountStart" id="btnEditAccountStart" value="Edit Account Start" class="btn btn-warning">
                                <span class="fa fa-edit" aria-hidden="true"> </span>
                            </button>                        
                            <?php endif; ?>
                            <?php if($arrBtnAllow['bolAllowUpdate']): ?>                        
                            <button type="submit" name="updateAccountStart" id="btnUpdateAccountStart" value="Update Account Start" class="btn btn-primary">
                                <span class="fa fa-pencil-square-o" aria-hidden="true"> </span>
                            </button>                        
                            <?php endif; ?>
                            <?php if($arrBtnAllow['bolAllowDelete']): ?>                        
                            <button type="submit" name="deleteAccountStart" id="btnDeleteAccountStart" value="Delete Account Start" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"> </span>
                            </button>                        
                            <?php endif; ?>
                        </div>                        
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>    
</div>