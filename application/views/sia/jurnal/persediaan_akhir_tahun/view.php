<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => site_url('jurnal/'.$strLink.'/browse')),
2 => array('title' => '<i class="fa fa-plus"></i> View', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
<form method="POST" action="<?=site_url('jurnal/'.$strLink.'/', NULL, FALSE)?>" id="frmJurnal">          
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Detail</h3></div>
                <div class="panel-body" >                    
                    <div class="form-group">
                        <div class="col-sm-1 control-label">
                            Proyek : 
                        </div>
                        <div class="col-sm-3">
                            <?=$arrJournal[0]['kont_name'];?>
                        </div>                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive" style="overflow:initial !important ;"><table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>No. Jurnal</th>
                                    <th>Tanggal</th>                                    
                                    <th colspan="2">Amount</th>
                                </tr>                                   
                            </thead>
                            <tbody id="tbodyJurnal">
                                <?php if(!empty($arrJournal)): $i = $intAmountDebet = 0; foreach($arrJournal as $row):?>
                                <tr>
                                <td>
                                    <?=$row['jrnl_code']?>
                                </td>
                                <td><?=formatDate($row['jrnl_date']," d F Y")?></td>                                
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intAmountDebet[]" type="text" value="<?=$row['jrnl_debet']?>" required disabled style="z-index: 0" />
                                    </div>
                                </td>                               
                                <td><button disabled type="button" name="hapusItem" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>
                            </tr>
                                <?php $intAmountDebet+= $row['jrnl_debet'];$i++; endforeach; endif; ?>                                                                           
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intTotalAmountDebet" value="<?=$intAmountDebet?>" type="text" readonly style="z-index: 0" />
                                    </div>
                                </td>
                                </tr>
                            </tfoot>                                
                        </table>
                    </div> 
                </div>
            </div>  
        </div>              
    </div>                 
    <div class="row">
        <div class="col-xs-12">            
            <?php //if($bolAllowUpdate):?>
            <button type="button" name="smtEditDebt" id="smtUpdateJurnal" value="Update Jurnal" class="btn btn-primary">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
            </button> 
            <?php //endif; ?>
            <button type="submit" id="btnUpdate" name="smtUpdateJurnal" value="Update Jurnal" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
            <?php //if($bolBtnPrint):?>
            <button type="submit" id="btnPrint" name="subPrint" value="Print" class="btn btn-success"><i class="fa fa-print"></i></button>
            <?php //endif; ?>
            <?php //if($bolAllowDelete):?>
            <button type="submit" id="btnDelete" name="smtDeleteJurnal" value="Delete Jurnal" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                           
            <?php //endif; ?>
        </div>
    </div>  
</form>
</div>