<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);   
$("input.currency").autoNumeric('set',0);
$("input[name^=intAmountDebet]").on('keyup', calculateDebet);
$("input[name^=intAmountCredit]").on('keyup', calculateCredit);
$("button[name=tambahItem]").click(addRowItem);	

$("form#frmJurnal").submit(function(){	
	$(".currency").autoNumeric('init', autoNumericOptionsRupiah); 
	var totalDebet = parseFloat($("input[name^=intTotalAmountDebet]").autoNumeric('get'));
	var totalKredit = parseFloat($("input[name^=intTotalAmountCredit]").autoNumeric('get'));	
	if (totalDebet !== totalKredit){
		alert("Total Debet dan Kredit tidak sama!");
		return false;		
	}else{		
		var confirmBox = confirm("Apakah data sudah benar? Data yang telah disimpan tidak dapat diubah.");
		return (confirmBox) ?  true : false;
	}
});

function addRowItem(e) {
	var element = '<tr><td><select type="text" name="intCOA[]" class="form-control chosen"><option value="0" disabled selected>Select your option</option><?php foreach($arrCOA as $e) { ?><option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option><?php } ?></select></td>'+
	    '<td><input class="form-control input-sm" name="strKeterangan[]" type="text" required style="z-index: 0" /></td>'+
		'<td><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control currency input-sm" name="intAmountDebet[]" type="text" required style="z-index: 0" value="0" /></div></td>'+
	    '<td><div class="input-group"><div class="input-group-addon">Rp.</div><input class="form-control currency input-sm" name="intAmountCredit[]" type="text" required style="z-index: 0" value="0" /></div></td>'+
	    '<td><button type="button" name="hapusItem" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>'+
		'</tr>';
	$("tbody#tbodyJurnal").append(element);
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $('.chosen').chosen({width: "100%",search_contains: true});
    $("button[name=hapusItem]").click(deleteRowItem);
    $("input[name^=intAmountDebet]").on('keyup', calculateDebet);	
    $("input[name^=intAmountCredit]").on('keyup', calculateCredit);
}

function deleteRowItem(e) {
	$(e.target)
        .parents('tr')
        .hide(150, function(){            
            $(e.target).parents('tr').remove()
            calculateDebet();
            calculateCredit();
        });
}

function calculateDebet() {
	var debetTotal = 0;
	$("input[name^=intAmountDebet]").each(function(){
		debetTotal += parseFloat($(this).autoNumeric('get'));
	});

	$("input[name=intTotalAmountDebet]").autoNumeric('set',debetTotal);
}

function calculateCredit() {
	var creditTotal = 0;
	$("input[name^=intAmountCredit]").each(function(){
		creditTotal += parseFloat($(this).autoNumeric('get'));
	});

	$("input[name=intTotalAmountCredit]").autoNumeric('set',creditTotal);
}

$('.chosen').chosen({width: "100%",search_contains: true});   
</script>