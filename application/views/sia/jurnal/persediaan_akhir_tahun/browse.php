<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<?php
	$strSearchAction = site_url('jurnal/'.$strLink.'/browse', NULL, FALSE);
	include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchjurnal.php'); 
	echo $strBrowseMode;	
	?>
</div>

<div class="col-xs-12"><form name="frmJournal" id="frmJournal" method="post" action="<?=site_url('jurnal/'.$strLink.'/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('jurnal/'.$strLink.'/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead>
			<tr>
				<th>No. Jurnal</th>
				<th>Tanggal</th>				
				<th>Proyek</th>
				<th class="action">Action</th>
			</tr>
		</thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrJournal)):			
			foreach($arrJournal as $e): ?>
			<tr>
				<td><?=$e['jrnl_code']?></td>
				<td><?=formatDate($e['jrnl_date'], 'd F Y')?></td>					
				<td><?=$e['kont_name']?></td>
				<td><a href="<?=site_url('jurnal/'.$strLink.'/view/'.$e['kont_id'])?>"><i class="fa fa-eye"></i></a></td>
			</tr>
			<?php endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="3"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</form>
</div>