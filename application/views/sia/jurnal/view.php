<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
1 => array('title' => '<i class="fa fa-list"></i> '.$strPageTitle, 'link' => site_url('jurnal/'.$strLink.'/browse')),
2 => array('title' => '<i class="fa fa-plus"></i> View', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
<form method="POST" action="<?=site_url('jurnal/'.$strLink.'/', NULL, FALSE)?>" id="frmJurnal">          
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New</h3></div>
                <div class="panel-body" >
                    <div class="form-group">
                        <div class="col-sm-1 control-label">
                            Kode
                        </div>
                        <div class="col-sm-11">
                            <?=$arrJournal[0]['jrnl_code'];?><input type="hidden" name="strCode" value="<?=$arrJournal[0]['jrnl_code'];?>"><input type="hidden" name="intID" value="<?=$arrJournal[0]['id'];?>">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1 control-label">
                            Tanggal
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($arrJournal[0]['jrnl_code'])) echo str_replace("-", "/", $arrJournal[0]['jrnl_date']); else echo date('Y/m/d'); ?>" class="form-control required jwDate" disabled/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                            <div class="col-sm-1 control-label">
                                Proyek
                            </div>
                            <div class="col-sm-3">
                                <select type="text" name="strProyek" class="form-control chosen" disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e) { ?>
                                        <option value="<?=$e['id']?>" <?=($arrJournal[0]['jrnl_ref_id'] == $e['id']) ? "selected" : null ?> ><?=$e['kont_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                     <div class="table-responsive" style="overflow:initial !important ;"><table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>No. COA</th>
                                    <th>Keterangan</th>                                       
                                    <th>Debet</th>                                                        
                                    <th>Kredit</th>                                        
                                    <!-- <th><button disabled type="button" name="tambahItem" class="btn btn-sm btn-link"><i class="fa fa-plus"></i></button> </th>-->
                                </tr>                                   
                            </thead>
                            <tbody id="tbodyJurnal">
                                <?php if(!empty($arrJournal)): $i = $intAmountDebet = $intAmountCredit = 0; foreach($arrJournal as $row):?>                    
                                <tr>
                                <td>
                                    <select type="text" disabled name="intCOA[]" class="form-control chosen">
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrCOA as $e) { ?>
                                            <option value="<?=$e['id']?>" <?=($row['jrnl_acco_id'] == $e['id']) ? "selected" : null ?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <input class="form-control" name="strKeterangan[]" type="text" value="<?=$row['jrnl_keterangan']?>" required disabled style="z-index: 0" />   
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intAmountDebet[]" type="text" value="<?=$row['jrnl_debet']?>" required disabled style="z-index: 0" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intAmountCredit[]" type="text" value="<?=$row['jrnl_kredit']?>" required disabled style="z-index: 0" />
                                   </div>
                                </td>
                                <!-- <td><button disabled type="button" name="hapusItem" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button> </td>-->
                            </tr>
                                <?php $intAmountDebet+= $row['jrnl_debet']; $intAmountCredit+= $row['jrnl_kredit']; $i++; endforeach; endif; ?>                                                                           
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intTotalAmountDebet" value="<?=$intAmountDebet?>" type="text" readonly style="z-index: 0" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control currency" name="intTotalAmountCredit" value="<?=$intAmountCredit?>" type="text" readonly style="z-index: 0" />
                                   </div>
                                </td>                                
                                </tr>
                            </tfoot>                                
                        </table>
                    </div>      
                </div>
            </div>  
        </div>              
    </div>                 
    <div class="row">
        <div class="col-xs-12">            
            <?php if($bolAllowUpdate):?>
            <button type="button" name="smtEditDebt" id="smtUpdateJurnal" value="Update Jurnal" class="btn btn-primary">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
            </button> 
            <?php endif; ?>
            <button type="submit" id="btnUpdate" name="smtUpdateJurnal" value="Update Jurnal" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
            <?php if($bolBtnPrint):?>
            <button type="submit" id="btnPrint" name="subPrint" value="Print" class="btn btn-success"><i class="fa fa-print"></i></button>
            <?php endif; ?>
            <?php if($bolAllowDelete):?>
            <button type="submit" id="btnDelete" name="smtDeleteJurnal" value="Delete Jurnal" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                           
            <?php endif; ?>
        </div>
    </div>  
</form>
</div>