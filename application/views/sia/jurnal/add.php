<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> Jurnal Memorial', 'link' => site_url('jurnal/memorial/browse')),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);
include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); 
?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>">
<div class="col-xs-12">
    <form method="POST" action="<?=site_url('jurnal/'.$strLink.'/', NULL, FALSE)?>" id="frmJurnal" >          
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New</h3></div>
                    <div class="panel-body" >
                        <div class="form-group">
                            <div class="col-sm-1 control-label">
                                Kode
                            </div>
                            <div class="col-sm-11">
                                Auto Generate
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-1 control-label">
                                Tanggal
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="tanggal" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDate"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-1 control-label">
                                Proyek
                            </div>
                            <div class="col-sm-3">
                                <select type="text" name="strProyek" class="form-control chosen">
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrProject as $e) { ?>
                                        <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="table-responsive" style="overflow:initial !important ;"><table class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>No. COA</th>
                                        <th>Keterangan</th>                                       
                                        <th>Debet</th>                                                        
                                        <th>Kredit</th>                                        
                                        <th><button type="button" name="tambahItem" class="btn btn-sm btn-link"><i class="fa fa-plus"></i></button></th>
                                    </tr>                                   
                                </thead>
                                <tbody id="tbodyJurnal">                    
                                    <td>
                                        <select type="text" name="intCOA[]" class="form-control chosen">
                                            <option value="0" disabled selected>Select your option</option>
                                            <?php foreach($arrCOA as $e) { ?>
                                                <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control" name="strKeterangan[]" type="text" required style="z-index: 0" />   
                                    </td>
                                    <td>
                                        <div class="input-group">
                                           <div class="input-group-addon">Rp.</div>
                                           <input class="form-control currency" name="intAmountDebet[]" type="text" required style="z-index: 0" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                           <div class="input-group-addon">Rp.</div>
                                           <input class="form-control currency" name="intAmountCredit[]" type="text" required style="z-index: 0" />
                                       </div>
                                    </td>
                                    <td><button type="button" name="hapusItem" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>                                                                           
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td>
                                        <div class="input-group">
                                           <div class="input-group-addon">Rp.</div>
                                           <input class="form-control currency" name="intTotalAmountDebet" type="text" readonly style="z-index: 0" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                           <div class="input-group-addon">Rp.</div>
                                           <input class="form-control currency" name="intTotalAmountCredit" type="text" readonly style="z-index: 0" />
                                       </div>
                                    </td>
                                    <td></td>
                                    </tr>
                                </tfoot>                                
                            </table>
                        </div>      
                    </div>
                </div>  
            </div>              
        </div>                 
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary" name="btnSaveJurnal" value="saveJurnal"><i class="fa fa-plus"></i></button>
            </div>          
        </div>  
    </form>
</div>