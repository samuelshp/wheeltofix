<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
	<?php
	$strSearchAction = site_url('credit/'.$strLink.'/browse', NULL, FALSE);
	include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchcredit.php'); 
	echo $strBrowseMode;
	?>
</div>

<div class="col-xs-12"><form name="frmCredit" id="frmCredit" method="post" action="<?=site_url('credit/'.$strLink.'/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('credit/'.$strLink.'/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead>
			<tr>
				<th>No. Pembayaran</th>
				<th>Tanggal</th>
				<?php if($intType == 1): ?><th>Customer</th><?php endif; ?>
				<?php if($intType == 3): ?><th>Keterangan</th><?php endif; ?>
				<th>Total Bayar</th>				
				<th class="action">Action</th>
			</tr>
		</thead>
		<tbody><?php
		// Display data in the table
		if(!empty($arrPembayaranPiutang)):			
			foreach($arrPembayaranPiutang as $e): ?>
			<tr>
				<td><?=$e['txin_code']?></td>
				<td><?=formatDate($e['txin_date'], 'd F Y')?></td>	
				<?php if($intType == 1): ?><td><?=$e['cust_name']?></td><?php endif; ?>			
				<?php if($intType == 3): ?><td><?=$e['txid_description']?></td><?php endif; ?>			
				<td><?=setPrice($e['txou_totalbersih'])?></td>
				<td><a href="<?=site_url('credit/'.$strLink.'/view/'.$e['id'])?>"><i class="fa fa-eye"></i></a></td>
			</tr>
			<?php endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="8"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
</form>
</div>