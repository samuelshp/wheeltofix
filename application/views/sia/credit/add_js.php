<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>

<script type="text/javascript">
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency").autoNumeric('init', autoNumericOptionsRupiah);
$(".chosen").chosen({width: "100%",search_contains: true});

$("#intMethod").change(function(){
    var methodType = $(this).val();    
    var field;
    if (methodType == 3) {
        $("#refGiro").removeClass('hide');        
        field = "acco_detail";
    }else{        
        if (methodType == 2) {
            field = "acco_bank";
        }else{
            field = "acco_kas";
        }
        $("#refGiro").addClass('hide');
    }

    $.ajax({
        url: "<?=site_url('api/jw_account/list?h="+userHash+"&filter_"+field+"=2')?>",
        method: "GET",
        dataType: "json",
        success: function(result) {
            $("#intCOA .optionResult").empty();               
            $.each(result.data, function(i, arr){                
                $("#intCOA").append(`
                    <option class="optionResult" value="`+arr['id']+`">`+arr['acco_code']+` - `+arr['acco_name']+`</option>
                `);
            });
            $(".chosen").trigger("chosen:updated");
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        }
    });
});

$('button[name=btnAddLainLain]').click(addrowLainLain);

$("#fieldFormPembayaranPiutang select[name=intCustomer]").change(function(){    
    sessionStorage.clear();
    var mainElement = $(this);
    $.ajax({       
        url: "<?=site_url('api/invoice_list/list?h="+userHash+"&filter_invo_status=3&filter_owner_id=')?>"+$(this).val(),
        dataType: "json",
        success: function(invoiceResult, textStatus, xhr){                
            $("#tDetailInvoiceList .resultInvoice").remove();                
            $("#tDetailInvoiceList tfoot").remove();
            if (invoiceResult.success === true) {                        
                $("#tbDetailInvoiceList .info").hide();   
                var totalAmount = 0;
                $.each(invoiceResult.data, function(i, arr) {
                    $("#tbDetailInvoiceList").append("<tr class='resultInvoice'><td><input type='hidden' name='idInvoice[]' value='"+arr[`invo_id`]+"' />"+arr['invo_code']+"</td><td>"+arr['createAt']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intNominal[]' value='"+arr['sisaBayar']+"' data-v-max='"+arr['sisaBayar']+"' readonly></div></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intAmountRencanaBayar[]' data-v-max='"+arr['invo_grandtotal']+"' value='"+arr['invo_grandtotal']+"'></div></td><td><input type='text' class='form-control input-sm currency' name='intPph[]' value='0.00'></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intAmountTotalAfterPph[]' value='"+arr['invo_grandtotal']+"' readonly></div></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intAmountUMC[]' value='0' readonly></div></td></td><td><button type='button' class='btn btn-link btn-sm' data-toggle='modal' data-target='.umc-modal' data-id='"+arr[`invo_id`]+"'>Pilih Uang Masuk Customer</button><input type='hidden' name='umcInvoiceMapping[]' value=''></td><td><input type='checkbox' name='cbxLunas[]'/></td></tr>");
                    totalAmount += parseFloat(arr['invo_grandtotal']);
                    totalAmountAfterPph = totalAmount;
                    // <input type='checkbox' class='chbxDibayar' name='invoiceBayar[]' data-id='"+arr[`id`]+"'>
                });

                $("#tDetailInvoiceList").append("<tfoot><tr class='result active'><td colspan='3'>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmountInvoice' value='"+totalAmount+"' readonly></div></td colspan='2'><td></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' id='intTotalInvoiceAfterPPH' name='intTotalInvoiceAfterPPH' value='"+totalAmountAfterPph+"'readonly></div></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' id='intTotalInvoiceDibayar' name='intTotalInvoiceDibayar' value='0' readonly></div></td></tr></tfoot>");

                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);                
                
                $("#tbDetailInvoiceList input[name^='intPph']").keyup(function(e){
                    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8) {                    
                        var rawAmount = parseFloat($(this).closest("tr.resultInvoice").find("input[name^='intAmount']").autoNumeric('get'));
                        var pph = parseFloat($(this).closest("tr.resultInvoice").find("input[name^='intPph']").autoNumeric('get'));
                        var amount2 = (rawAmount/1.1);
                        var calculate = rawAmount - (amount2*pph/100);
                        $(".currency").autoNumeric('init', autoNumericOptionsRupiah);                    
                        $(this).closest("tr.resultInvoice").find("input[name^=intAmountTotalAfterPph]").autoNumeric("set",calculate);
                        calculateInvoice();                        
                    }
                });

                $("tr.resultInvoice input[name='intAmountRencanaBayar[]']").keyup(function(e){ 
                    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9) {
                        $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
                        var total = 0;                        
                        $.each($("tr.resultInvoice input[name='intAmountRencanaBayar[]']"), function(i, el) {                            
                            total += parseFloat($(el).autoNumeric('get'));                            
                        });
                        $("#tDetailInvoiceList input[name=intTotalAmountInvoice]").autoNumeric('set',total);                            

                        var pph = $(this).closest("tr.resultInvoice").find("input[name^='intPph']").autoNumeric('get');
                        var thisAmount = $(this).autoNumeric('get');
                        var amount2 = (thisAmount/1.1);
                        var calculate = thisAmount - (amount2*pph/100);

                        $(this).closest("tr.resultInvoice").find("input[name^='intAmountTotalAfterPph']").autoNumeric('set', calculate);
                        calculateInvoice(); 
                        // $.each($(this), function(){
                        //     $("#tDetailInvoiceList input[name^='intTotalAmountInvoice']").autoNumeric('set',$(this).autoNumeric('get'));
                        // });
                    }
                });               

                $("input[name^='invoiceBayar']").click(function(e){
                    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
                    var id = $(this).data('id');
                    var value = $(this).closest("tr.resultInvoice").find("input[name^='intAmountRencanaBayar']").autoNumeric('get');
                    $(this).val(id+"-"+value);

                    if(value != 0){
                        var amountTotal = calculateAmountBayar();                        
                        $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
                        $("#intTotalInvoiceDibayar").autoNumeric('set',amountTotal);                       
                    }else{
                        e.preventDefault();
                        alert("Rencana Bayar tidak boleh kosong atau 0");
                        $(this).closest("tr.resultInvoice").find("input[name^='intAmountRencanaBayar']").focus();
                    }

                });                                             
            
            }else{
                $("#tbDetailInvoiceList .info").show();
            }

            var button;
                $('.umc-modal').on('show.bs.modal', function (event) {                    
                    button = $(event.relatedTarget) // Button that triggered the modal
                    var invoice_id = button.data('invoice-id') // Extract info from data-* attributes
                    var modal = $(this);                    
                    loadUMC(mainElement.val());                    
                   
                    modal.find('input[name=intAmountInvoice]').val(button.closest(".resultInvoice").find("input[name^=intAmountTotalAfterPph]").val());
                    modal.find('input[name=intAmountBelumTerbayar]').val(button.closest(".resultInvoice").find("input[name^=intAmountTotalAfterPph]").val());
                });                                
                $("button[name=btnDonePilihUMC]").click(function(){                    
                    var inputUMCMapping = button.closest(".resultInvoice").find("input[name^=umcInvoiceMapping]");
                    var chbxDianggapLunas = button.closest(".resultInvoice").find("input[name^=cbxLunas]"); 
                    var sisaInvoiceBelumTerbayar = $(".umc-modal input[name='intAmountBelumTerbayar']") .autoNumeric('get');
                    if(sessionStorage){
                        
                        var arrUMC = [];
                        var umcTerpakai = 0;
                        var umcSisa = 0;
                        var umcDianggapLunasOption = 0;

                        $(".umc-modal input.chbxDipakai:checked").each(function(i){
                            umcSisa = $(this).closest("tr.umc").find("input[name^=amountSisa]").val();
                            umcTerpakai = $(this).closest("tr.umc").find("input[name^=amountUMCTerpakai]").val();
                            // var umcAmount = $(this).data('amount');amountSisa[]
                            // terpakai = umcAmount - umcSisa;
                            if(umcSisa > 0 || sisaInvoiceBelumTerbayar > 0) umcDianggapLunasOption = $(this).data('id');
                            arrUMC.push($(this).data('id')+"-"+umcTerpakai+"-"+button.data('id')+"-"+umcDianggapLunasOption+"-"+umcSisa);
                        });
                        // Store data
                        addToLocalStorageObject('umcChosenID', arrUMC);
                        inputUMCMapping.val(arrUMC);
                        if (umcDianggapLunasOption == 0) chbxDianggapLunas.prop("checked", true).prop("disabled",true);
                        if (inputUMCMapping.val() != '') button.closest("tr.resultInvoice").addClass("success");                        
                        button.closest(".resultInvoice").find("input[name^=intAmountUMC]").autoNumeric('set',umcTerpakai);
                        calculateTotalUMCused();
                    }else{
                        alert("Sorry, your browser do not support session storage. Please contact administrator");
                    }  
                });   
        },
        error: function(err){
            alert("Oops. Error");
            console.log(err);
        },
        async: false
    });
});

$("#frmAddCredit").submit(function(e){    
    // e.preventDefault();
    var $intType = $("button#smtMakeCredit").val();    
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    if ($intType == 1){        
        $.each("#tbDetailInvoiceList tr.resultInvoice.success", function(i, el){
            var amountUMC = $("#intTotalUMCPakai").autoNumeric('get');
            var amountInv = $("#intTotalInvoiceDibayar").autoNumeric('get');    
            if(amountUMC !== amountInv){
                return confirm("Total Bayar dan total alokasi tidak sama.");
            }
        });
    }        
    return true;
});

function calculateInvoice() {    
    var amountTotalInvoice = 0; 
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $.each($("input[name^='intAmountTotalAfterPph']"), function(i,el) {        
        amountTotalInvoice += parseFloat($(el).autoNumeric('get'));
    });      
    $("#intTotalInvoiceAfterPPH").autoNumeric('set', amountTotalInvoice);
    calculateTotal();
    // $("input[name=intPph]").closest("tr.resultInvoice").find("input[name=intRencanaBayar]").autoNumeric("set",total);
}

function calculateAmountDipakai() {
   var amountTotalPakai = 0;
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $("input.chbxDipakai:checked").each(function(){     
        amountTotalPakai += parseFloat($(this).closest("tr.result").find("input[name^='intUMCAmount']").autoNumeric('get'));
    });
    return amountTotalPakai;
}

function calculateAmountBayar(){
    var amountTotal = 0;
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $("input.chbxDibayar:checked").each(function(){     
        amountTotal += parseFloat($(this).closest("tr.resultInvoice").find("input[name^='intAmountTotalAfterPph']").autoNumeric('get'));
    });
    return amountTotal;
}

function addrowLainLain() {
    $('#lainlain').append(`
        <tr>
        <td>
            <select class="form-control chosen" name="intCOAdetail[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrCOA as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                <?php endforeach;?>          
            </select>
        </td>                                
        <td>
            <select class="form-control chosen" name="listProyek[]" required>
                <option value="0" disabled selected>Select your option</option>
                <?php foreach($arrProject as $e):?>
                    <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                <?php endforeach;?>                                       
            </select>
        </td>
        <td>
            <div class="input-group">
                <div class="input-group-addon">Rp.</div>
                <input class="form-control input-sm currency" name="intAmount[]" type="text" />
            </div>
        </td>
        <td><input type="text" name="keterangan[]" class="form-control"></td>
        <td><button type="button" name="btnRemoveLainLain" class="btn btn-danger btn-sm remove-row-button"><i class="fa fa-trash"></i></button></td>
        </tr>
    `);
    $('.currency').autoNumeric('init', autoNumericOptionsRupiah);
    $('.chosen').chosen({width:"100%",search_contains: true});
    $('.remove-row-button').click(removeRow);
}

function removeRow(e){    
    $(e.target)
        .parents('tr')
        .hide(200, function(){            
            $(e.target).parents('tr').remove()
        });
}

function loadUMC(id) {    
    $.ajax({        
        url: "<?=site_url('api/uang_masuk_customer/list?h="+userHash+"&filter_umcu_status=lte-3&filter_umcu_cust_id="+id+"')?>",
        method: "GET",
        dataType: "json",
        success: function(result, textStatus, xhr){   
            $(".result.umc").remove();      
            $("#tDetailPembayaranHutang tfoot").remove();             
            if(result.success === true){
                $(".info").hide();                              
                var total = 0;
                var totalSisa = 0;
                checkUsedUMC();
                $.each(result.data, function(i, arr){                      
                    if(checkUsedUMC(arr['id'])){
                        var classStyle = "danger";
                        var disabled = "disabled";
                        var strUsed = "Terpakai";
                    }else{
                        strUsed = "<input type='checkbox' name='umcPakai[]' class='chbxDipakai' data-id='"+arr[`id`]+"' data-amount='"+arr['umcu_amount']+"' data-sisa='"+(arr['umcu_amount']-arr['umcu_terpakai'])+"' value='"+arr[`id`]+"-"+arr['umcu_amount']+"'><input name='amountSisa[]' type='hidden' value='' placeholder='Sisa UMC'><input name='amountUMCTerpakai[]' type='hidden' value='' placeholder='UMC Terpakai'>";
                    }
                    var txtBoxAvailable = (arr['txid_type'] === 'UMC') ? "readonly value='"+arr['raw_amount']+"'" : null;
                    $("#tbDetail").append("<tr class='result umc "+classStyle+"'><td><input type='hidden' name='idUMC[]' value='"+arr[`id`]+"' />"+arr['umcu_code']+"</td><td>"+arr['umcu_date']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intUMCAmount[]' value='"+arr['umcu_amount']+"' readonly></div></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intUMCAmount[]' value='"+(arr['umcu_amount']-arr['umcu_terpakai'])+"' readonly></div></td><td>"+strUsed+"</td></tr>");
                    total += parseFloat(arr['umcu_amount']);
                    totalSisa += parseFloat(arr['umcu_amount']-arr['umcu_terpakai']);
                });  

                $("#tDetailPembayaranHutang").append("<tfoot><tr class='result active'><td colspan='2'>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' id='intTotalUMCPakai' name='intTotalUMCPakai' readonly></div></td><td></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);                               

                $("input[name^='umcPakai']").click(function(e){

                    $(".currency").autoNumeric('init', autoNumericOptionsRupiah); 

                    var umcTerpakai = 0;

                    if($(this).is(":checked")){                               
                        var sisaInvoiceBelumTerbayar  = parseFloat($("input[name=intAmountBelumTerbayar]").autoNumeric('get'));
                        var umcBisaDipakai = parseFloat($(this).data('sisa'));
                        var sisaUMC = umcBisaDipakai - sisaInvoiceBelumTerbayar;   //untuk ditaruh di textbox sisa umc              
                        sisaUMC = sisaUMC >= 0 ? Math.abs(sisaUMC) : 0;
                        umcTerpakai = umcBisaDipakai - sisaUMC;
                    }else{
                        var sisaUMC = parseFloat($(this).data('sisa'));                        
                    }

                    $(this).closest("tr.result").find("input[name^='amountSisa']").val(sisaUMC);
                    $(this).closest("tr.result").find("input[name^='amountUMCTerpakai']").val(umcTerpakai);
                    // $(this).data('sisa',20);
                    calculateCoveredInvoice();
                    
                });

                // $("input[type='text']").on('keyup', function() {
                //     alert("Test");
                // });
                
            }else{
                $("#tDetailPembayaranHutang #tbDetail .info").show();
            }
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        },
        async: false
    });
}

function addToLocalStorageObject(name,values) {    
    // Get the existing data
    var existing = sessionStorage.getItem(name);

    // If no existing data, create an array
    // Otherwise, convert the localStorage string to an array
    existing = existing ? existing.split(',') : [];

    $.each(values, function(key, value){
       // Add new data to localStorage Array      
       var item = value.split("-");
       var id = item[0];
       var amount = item[1];
       existing.push(id);
    });
    
    // Save back to localStorage
    sessionStorage.setItem(name, existing);

};

function checkUsedUMC(id){
    var existing = sessionStorage.getItem('umcChosenID');

    existing = existing ? existing.split(',') : [];
    
    return existing.indexOf(id) > -1 ? true : false ;
}

function calculateCoveredInvoice(){        
    var intAmountUMCTotal = 0;
    var intAmountInvoice = parseFloat($("input[name=intAmountInvoice]").autoNumeric('get'));
    var intAmountBelumTerbayar;
    
    $("input.chbxDipakai:checked").each(function() {        
        intAmountUMC = parseFloat($(this).data('amount'));
        intAmountUMCTotal += intAmountUMC;       
    });   

    intAmountBelumTerbayar = intAmountInvoice - intAmountUMCTotal;
    intAmountBelumTerbayar = intAmountBelumTerbayar >= 0 ? intAmountBelumTerbayar : 0;
    
    $("input[name=intAmountBelumTerbayar]").autoNumeric('set', intAmountBelumTerbayar);
}

function calculateTotalUMCused() {
    var amountTotal = 0;    
    $.each($("tr.resultInvoice input[name='intAmountUMC[]']"), function(i, el) {                            
        amountTotal += parseFloat($(el).autoNumeric('get'));                            
    });
    $("#intTotalInvoiceDibayar").autoNumeric('set',amountTotal);    
}

function calculateTotal() {
    var amountTotal = 0;    
    $.each($("tr.resultInvoice input[name='intAmountTotalAfterPph[]']"), function(i, el) {                            
        amountTotal += parseFloat($(el).autoNumeric('get'));                            
    });
    $("#tDetailInvoiceList input[name=intTotalAmountInvoice]").autoNumeric('set',amountTotal);    
}

</script>