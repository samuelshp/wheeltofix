<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('credit/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmEditCredit" id="frmEditCredit" method="post" action="<?=site_url('credit/'.$strLink.'/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
             <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">                    
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            No. Pembayaran
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control" name="txtCode" type="text" value="<?=$arrCredit['code']?>" readonly/>
                        <input name="intID" type="hidden" value="<?=$arrCredit['id']?>" readonly />  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                          Tgl *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=$arrCredit['date']?>" disabled/>
                        </div>
                        <div class="clearfix"></div>
                    </div>                    

                    <!-- Pembayaran Piutang -->
                    <?php if($intType == 1):?>
                    <div id="fieldFormPembayaranPiutang">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               Customer *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCustomer" id="intCustomer" required disabled>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCustomer as $e):?>
                                        <option value="<?=$e['id']?>" <?=($arrCredit['txin_cust_id'] == $e['id'] ? "selected" : null)?> ><?=$e['cust_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Pembayaran Piutang -->                         

                    <!-- Nota Debet -->
                    <?php if($intType == 2):?>
                    <div id="fieldFormNota">
                        <div class="form-group hide" id="refGiro">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-nopi')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="listPI" id="listPI" required>
                                    <option value="0" disabled selected>Select your option</option>                                        
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>  
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Nota Debet -->

                    <?php if($intType == 3): ?>                   
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required disabled>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1" <?=($arrCredit['metode'] == 1 ? "selected": null )?>>Cash</option>
                                <option value="2" <?=($arrCredit['metode'] == 2 ? "selected": null )?>>Bank</option>
                                <option value="3" <?=($arrCredit['metode'] == 3 ? "selected": null )?>>Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="refGiro" class="hide">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                        </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intCOA" id="intCOA" disabled required>
                                <option value="0" disabled selected>Select your option</option>
                                 <?php foreach($arrCOA as $e):?>
                                    <option value="<?=$e['id']?>" <?=($arrCredit['acco_id'] == $e['id'] ? "selected": null )?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                <?php endforeach;?> 
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php endif; ?>
                </div>                        
                </div>
            </div>                
        </div>

        <!-- Detail Items -->
        <?php if($intType == 1):?>
        <div class="panel panel-primary" id="detailInvoiceList">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailInvoiceList">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailInvoiceList">
                    <thead>
                    <tr>
                        <th>No. Invoice</th>
                        <th>Tgl.</th>                        
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th>Rencana Bayar</th>
                        <th style="width:9%">Pph (%)</th>
                        <th>Total</th>
                        <th>Total UMC Dipakai</th>
                        <th style="width:12%">UMC Dipakai</th>
                        <th>Dianggap Lunas</th>
                    </tr>
                    </thead>
                    <tbody id="tbDetailInvoiceList">
                        <?php if(!empty($arrDetail)): $intGrandtotal = 0; 
                            foreach($arrDetail as $detail):
                            $intGrandtotal += $detail['invo_grandtotal'];
                            $intTotalTerbayar += $detail['txid_totalbersih'];
                        ?>
                            <tr class="resultInvoice">                                
                                <td>
                                    <?=$detail['invo_code']?>
                                    <input type="hidden" name="invpID" value="<?=$detail['invp_id']?>">
                                </td>
                                <td><?=formatDate($detail['cdate'], "d F Y")?></td>                                    
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmountInvoice[]" type="text" value="<?=$detail['invo_grandtotal']?>" disabled data-toggle="tooltip" title="<?=setPrice($detail['invo_grandtotal'], 'BASE', FALSE)?>"/>
                                   </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmountRencanaBayar[]" type="text" value="<?=$detail['txid_amount']?>" disabled data-toggle="tooltip" title="<?=setPrice($detail['txid_amount'], 'BASE', FALSE)?>"/>
                                   </div>
                                </td>
                                <td>
                                    <div class="input-group">                                       
                                       <input class="form-control input-sm currency" name="intPPH[]" type="text" value="<?=$detail['txid_txin_pph']?>" disabled />
                                       <div class="input-group-addon">%</div>
                                    </div>
                                </td>                                
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmountAfterPPH[]" type="text" value="<?=$detail['txid_totalbersih']?>" disabled data-toggle="tooltip" title="<?=setPrice($detail['txid_totalbersih'], 'BASE', FALSE)?>"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmountUMCused[]" type="text" value="<?=$detail['invp_umcu_amount_used']?>" disabled data-toggle="tooltip" title="<?=setPrice($detail['txid_totalbersih'], 'BASE', FALSE)?>"/><br/>                                       
                                    </div>
                                    <small>(Sisa UMC : <?=setPrice(($detail['umcu_amount'] - $arrHistoryUMC['terpakai_sebelumnya']),'BASE',FALSE)?>)</small>
                                </td>
                                <td>
                                    <?=$detail['umcu_code']?>
                                    <small>(Rp <?=setPrice($detail['umcu_amount'],'BASE',FALSE)?>)</small>
                                    <input type="hidden" name="umcInvoiceMapping[]" value="<?=$detail['umcu_id']."-".$detail['terbayar']."-".$detail['id']."-".((boolval($detail['invp_lunas'])) ? $detail['umcu_id'] : 0)."-".($detail['umcu_amount'] - $arrHistoryUMC['terpakai_sebelumnya'])?>" />
                                </td>
                                <td><input type="checkbox" name="cbxLunas[]" disabled <?=(boolval($detail['invp_lunas'])) ? "checked" : ""?> ><input type="hidden" name="sisa[]" value="<?=($detail['umcu_amount'] - $arrHistoryUMC['terpakai_sebelumnya'])?>"></td>
                            </tr> 
                        <?php endforeach; else:?>
                        <tr class="info">
                            <td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>                           
                        </tr>        
                        <?php endif; ?>                
                    </tbody>
                    <tfoot>
                        <tr class="result active">
                            <td colspan="3">Total</td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp.</div>
                                    <input type="text" class="form-control input-sm currency" name="intTotalAmountInvoice" value="<?=$intGrandtotal?>" readonly data-toggle="tooltip" title="<?=setPrice($intGrandtotal, 'BASE', FALSE)?>" >
                                </div>
                            </td>
                            <td></td>                            
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp.</div>
                                    <input type="text" class="form-control input-sm currency" id="intTotalInvoiceDibayar" name="intTotalInvoiceDibayar" value="<?=$intTotalTerbayar?>" readonly data-toggle="tooltip" title="<?=setPrice($intTotalTerbayar, 'BASE', FALSE)?>">
                                </div>
                            </td>
                            <td colspan="3"></td>
                        </tr>
                    </tfoot>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 3):?>
        <div class="panel panel-primary" id="detailPemasukkanLain">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div><table class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th colspan="2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                    </tr>
                    </thead>
                    <tbody id="lainlain">      
                            <?php if(!empty($arrDetail)): foreach($arrDetail as $detail): ?>                  
                            <tr>
                                <td>
                                    <input type="hidden" name="intIDdetail[]" value="<?=$detail['id']?>">
                                    <select class="form-control chosen" name="intCOAdetail[]" required disabled>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrCOAdetail as $e):?>
                                            <option value="<?=$e['id']?>" <?=($detail['txid_acco_id'] == $e['id']) ? "selected" : null ?> ><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                        <?php endforeach;?>          
                                    </select>
                                </td>                                
                                <td>
                                    <select class="form-control chosen" name="listProyek[]" required disabled>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrProject as $e):?>
                                            <option value="<?=$e['id']?>" <?=($detail['txid_ref_id'] == $e['id']) ? "selected" : null ?> ><?=$e['kont_name']?></option>
                                        <?php endforeach;?>                                       
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmount[]" type="text" value="<?=$detail['txid_amount']?>" disabled />
                                   </div>
                                </td>
                                <td><input type="text" name="keterangan[]" class="form-control" value="<?=$detail['txid_description']?>" disabled></td>
                                <td></td>
                            </tr>
                            <?php endforeach; endif; ?>                  
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>        
        <?php if($arrCredit['txin_status_trans'] < 4):?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" name="smtEditCredit" id="smtEditCredit" value="Update Credit" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
                    </button> 
                    <button type="submit" id="btnUpdate" name="smtUpdateCredit" value="Update Credit" class="btn btn-warning" style="display:none"><i class="fa fa-edit"></i></button>
                    <button type="submit" id="btnDelete" name="smtDeleteCredit" value="Delete Credit" class="btn btn-danger pull-right" style="display:none"><i class="fa fa-trash"></i></button>                                               
                </div>                
            </div>
        </div>     
        <?php endif; ?>

</form></div>