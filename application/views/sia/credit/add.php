<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-usd"></i> '.$strPageTitle, 'link' => site_url('credit/'.$strLink.'/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<div class="col-xs-12"><form name="frmAddCredit" id="frmAddCredit" method="post" action="<?=site_url('credit/'.$strLink.'/', NULL, FALSE)?>" class="frmShop">
        <div class="row">            
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> Add New Item</h3></div>
                <div class="panel-body">                    
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            No. Pembayaran
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control" name="txtCode" type="text" value="Auto Generate" disabled />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                          Tgl *
                        </div>
                        <div class="col-sm-7 col-md-9">
                        <input class="form-control jwDateTime" name="txtDate" type="text" value="<?=date('Y-m-d')?>" />
                        </div>
                        <div class="clearfix"></div>
                    </div>                    

                    <!-- Pembayaran Piutang -->
                    <?php if($intType == 1):?>
                    <div id="fieldFormPembayaranPiutang">                        
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                               Customer *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intCustomer" id="intCustomer" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrCustomer as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['cust_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Pembayaran Piutang -->                         

                    <!-- Nota Debet -->
                    <?php if($intType == 2):?>
                    <div id="fieldFormNota">
                        <div class="form-group hide" id="refGiro">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-supplier')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="intSupplier" id="intSupplier" required>
                                    <option value="0" disabled selected>Select your option</option>
                                    <?php foreach($arrSupplier as $e):?>
                                        <option value="<?=$e['id']?>"><?=$e['supp_name']?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-nopi')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                                <select class="form-control chosen" name="listPI" id="listPI" required>
                                    <option value="0" disabled selected>Select your option</option>                                        
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>  
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                            </div>
                            <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <!-- End of Nota Debet -->

                    <?php if($intType == 3): ?>                   
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-method')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intMethod" id="intMethod" required>
                                <option value="0" disabled selected>Select your option</option>
                                <option value="1">Cash</option>
                                <option value="2">Bank</option>
                                <option value="3">Giro</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="refGiro" class="hide">
                        <div class="form-group">
                            <div class="col-sm-5 col-md-3 control-label">
                                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-giroref')?> *
                            </div>
                            <div class="col-sm-7 col-md-9">
                               <input class="form-control" name="txtRefGiro" type="text"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-duedate')?>
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <input class="form-control jwDateTime" name="txtDueDate" type="text"/>
                        </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-md-3 control-label">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?> *
                        </div>
                        <div class="col-sm-7 col-md-9">
                            <select class="form-control chosen" name="intCOA" id="intCOA" required>
                                <option value="0" disabled selected>Select your option</option>                               
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php endif; ?>
                </div>                        
                </div>
            </div>                
        </div>

        <!-- Detail Items -->
        <?php if($intType == 1):?>
        <!-- <div class="panel panel-primary" id="detailPembayaranHutang">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> List Uang Masuk</h3></div>
            <div class="panel-body" id="detailDPH">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailPembayaranHutang">
                    <thead>
                    <tr>
                        <th>No. Invoice</th>
                        <th>Tgl.</th>                        
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th>Gunakan</th>                        
                    </tr>
                    </thead>
                    <tbody id="tbDetail">
                        <tr class="info">
                            <td class="noData" colspan="4"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>                            
                        </tr>                        
                    </tbody>
                </table></div>
                <p class="spacer">&nbsp;</p>
            </div>
        </div> -->

        <div class="panel panel-primary" id="detailInvoiceList">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailInvoiceList">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailInvoiceList">
                    <thead>
                    <tr>
                        <th>No. Invoice</th>
                        <th>Tgl.</th>                        
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th>Rencana Bayar</th>
                        <th>Pph (%)</th>
                        <th>Total</th>
                        <th>Total UMC Dipakai</th>
                        <th>Bayar</th>
                        <th>Dianggap Lunas</th>
                    </tr>
                    </thead>
                    <tbody id="tbDetailInvoiceList">
                        <tr class="info">
                            <td class="noData" colspan="9"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                            <!-- <td></td>
                            <td></td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp.</div>
                                    <input type="text" name="jumlahPiutangDibayar[]" class="form-control currency">
                                </div>
                            </td> -->
                        </tr>                        
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 2):?>
        <div class="panel panel-primary" id="detailNota">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body" id="detailDPH">
                <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nopurchase')?></th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                    </tr>
                    </thead>
                    <tbody >
                        <tr class="info"><td class="noData" colspan="13"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                        
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($intType == 3 ): ?>
        <div class="panel panel-primary" id="detailPemasukkanLain">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pembayaranhutang-detail')?></h3></div>
            <div class="panel-body">
                <div><table class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-coanumber')?></th>                        
                        <th>Nama Proyek</th>
                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                        <th colspan="2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></th>
                    </tr>
                    </thead>
                    <tbody id="lainlain">                        
                            <tr>
                                <td>
                                    <select class="form-control chosen" name="intCOAdetail[]" required>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrCOA as $e):?>
                                            <option value="<?=$e['id']?>"><?=$e['acco_code']." - ".$e['acco_name'];?></option>
                                        <?php endforeach;?>          
                                    </select>
                                </td>                                
                                <td>
                                    <select class="form-control chosen" name="listProyek[]" required>
                                        <option value="0" disabled selected>Select your option</option>
                                        <?php foreach($arrProject as $e):?>
                                            <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                                        <?php endforeach;?>                                       
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                       <div class="input-group-addon">Rp.</div>
                                       <input class="form-control input-sm currency" name="intAmount[]" type="text" />
                                   </div>
                                </td>
                                <td><input type="text" name="keterangan[]" class="form-control"></td>
                                <td><button type="button" name="btnAddLainLain" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button></td>
                            </tr>                       
                    </tbody>
                </table></div>

                <p class="spacer">&nbsp;</p>
            </div><!--/ Table Selected Items -->
        </div>
        <?php endif; ?>

        <?php if($bolAllowInsert):?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="smtMakeCredit" id="smtMakeCredit" value="<?=$intType?>" class="btn btn-primary">
                        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
                    </button>                    
                </div>
            </div>
        </div>     
        <?php endif; ?>

</form></div>

<div class="modal fade umc-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih UMC</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Total Invoice:</label>
            <div class="input-group col-xs-12">
                       <div class="input-group-addon">Rp.</div>
                       <input class="form-control currency" name="intAmountInvoice" type="text" readonly data-id=""/>
            </div>
          </div>               
        </form>
        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="tDetailPembayaranHutang">
            <thead>
            <tr>
                <th>No. Invoice</th>
                <th>Tgl.</th>                        
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-amount')?></th>
                <th>Sisa</th>
                <th>Gunakan</th>                        
            </tr>
            </thead>
            <tbody id="tbDetail">
                <tr class="info">
                    <td class="noData" colspan="5"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>                            
                </tr>                        
            </tbody>
        </table></div>  
        <div class="form-group">
            <label for="recipient-name" class="control-label">Sisa Belum Terbayar:</label>
            <div class="input-group col-xs-12">
                       <div class="input-group-addon">Rp.</div>
                       <input class="form-control input-sm currency" name="intAmountBelumTerbayar" type="text" readonly />     
            </div>
          </div>         
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <button type="button" name="btnDonePilihUMC" class="btn btn-primary" data-dismiss="modal">Pilih</button>
      </div>
    </div>
  </div>
</div>