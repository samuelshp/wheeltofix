<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script> 
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function(){   
    $(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);

    $("input[name^='intPPH']").keyup(function(){  
        var rawAmount = parseFloat($(this).closest("tr.resultInvoice").find("input[name^='intAmountRencanaBayar']").autoNumeric('get'));        
        var pph = parseFloat($(this).closest("tr.resultInvoice").find("input[name^='intPPH']").autoNumeric('get'));
        var amount2 = (rawAmount/1.1);
        var calculate = rawAmount - (amount2*pph/100);
        $(this).closest("tr.resultInvoice").find("input[name^=intAmountAfterPPH]").autoNumeric("set",calculate);
        var umcMapping = $(this).closest("tr.resultInvoice").find("input[name^='umcInvoiceMapping']").val();
        var part = umcMapping.split('-');
        var total = $(this).closest("tr.resultInvoice").find("input[name^=intAmountAfterPPH]").autoNumeric("get");
        $(this).closest("tr.resultInvoice").find("input[name^='umcInvoiceMapping']").val(part[0]+"-"+total+"-"+part[2]);
        calculateInvoice();
    });

     $("tr.resultInvoice input[name='intAmountRencanaBayar[]']").keyup(function(e){ 
        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9) {
            $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
            var total = 0;                        
            $.each($("tr.resultInvoice input[name='intAmountRencanaBayar[]']"), function(i, el) {                            
                total += parseFloat($(el).autoNumeric('get'));                            
            });
            $("#tDetailInvoiceList input[name=intTotalAmountInvoice]").autoNumeric('set',total);                            

            var pph = $(this).closest("tr.resultInvoice").find("input[name^='intPPH']").autoNumeric('get');
            var thisAmount = $(this).autoNumeric('get');
            var amount2 = (thisAmount/1.1);
            var calculate = thisAmount - (amount2*pph/100);

            $(this).closest("tr.resultInvoice").find("input[name^='intAmountAfterPPH']").autoNumeric('set', calculate);
            calculateInvoice(); 
            // $.each($(this), function(){
            //     $("#tDetailInvoiceList input[name^='intTotalAmountInvoice']").autoNumeric('set',$(this).autoNumeric('get'));
            // });
        }
    });
});

 $("#intMethod").change(function(){
    var methodType = $(this).val();    
    if (methodType == 3) { 
        $("#refGiro").removeClass('hide');
    }else{
        $("#refGiro").addClass('hide');
    }
});

$("#frmEditCredit").submit(function(){
    $.each("input[name^='intAmountAfterPPH']", function(i, el){
        var $thisVal = $(el).autoNumeric('get');
        var umc = $(el).closest("tr.resultInvoice").find("input[name^='intAmountUMCused']").autoNumeric('get');
        if ($thisVal !== umc) {
            alert("Total Bayar dan total alokasi tidak sama.");
            return false;
        }
    });
    return true;
});

$("#fieldFormPembayaranHutang select[name=intSupplier]").change(function(){
    $.ajax({
        url: "<?=site_url('debt_ajax/getDPH/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result){   
            $(".result").remove();         
            if(result.length > 0){
                $(".info").hide();                              
                var total = 0;
                $.each(result, function(i, arr) {                    
                    $("#tbDetailDPH").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`id`]+"' />"+arr['dphu_code']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' name='intAmount[]' class='form-control input-sm currency' value="+arr['dphu_amount']+" readonly></div></td></tr>");
                    total += parseFloat(arr['dphu_amount']);
                });
                $("#tDetailPembayaranHutang").append("<tfoot><tr class='result active'><td>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }
        },
        error: function(err) {
            alert("Oops, Error.");
            console.log(err);
        }
    });
});

$("#fieldFormNota select[name=intSupplier]").change(function(){
    $("option.result").remove();
    $.ajax({
        url: "<?=site_url('debt_ajax/getPurchaseInvoiceBySupplier/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result){
            $.each(result, function(i, arr){
                $("#fieldFormNota select[name=listPI]").append(`
                    <option value="`+arr['id']+`" class="result">`+arr['pinv_code']+`</option>
                `);
            });
            $(".chosen").trigger("chosen:updated");
        },
        error: function(err){
            alert("Oops, error.");
        }
    });
});

$("#fieldFormNota select#listPI").change(function(){
    $.ajax({
        url: "<?=site_url('debt_ajax/getPurchaseInvoiceDetail/"+$(this).val()+"')?>",
        method: "POST",
        dataType: "json",
        success: function(result) {
            console.log(result);
            $(".result").remove();         
            if(result.length > 0){
                $(".info").hide();                              
                var total = 0;
                $.each(result, function(i, arr) {                    
                    $("#tbDetailNota").append("<tr class='result'><td><input type='hidden' name='idDPH[]' value='"+arr[`id`]+"' />"+arr['pinv_code']+"</td><td>"+arr['prod_title']+"</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' name='intAmount[]' class='form-control input-sm currency' value="+arr['pinvit_subtotal']+" readonly></div></td></tr>");  
                    total += parseFloat(arr['pinvit_subtotal']);
                });
                $("#tDetailNotaDebet").append("<tfoot><tr class='result active'><td colspan='2'>Total</td><td><div class='input-group'><div class='input-group-addon'>Rp.</div><input type='text' class='form-control input-sm currency' name='intTotalAmount' value="+total+" readonly></div></td></tr></tfoot>");
                $(".currency").autoNumeric('init', autoNumericOptionsRupiah);  
            }else{
                $(".info").show();
            }   
        },
        error: function(err) {
            alert("Oops");
            console.log(err);
        }
    });
});

$("#smtEditCredit").click(function(){    
    $(this).hide();
    $("#btnUpdate").show();
    $("#btnDelete").show();
    $(".form-control,input[type='checkbox']").removeAttr('disabled');    
    $(".chosen").chosen('destroy');    
    $(".chosen").chosen({width:"100%",search_contains: true});
    // handlerPiutang();
    // deleteHandler();    
});

$("#fieldFormNota input[name=intAmount]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($(this).autoNumeric('get'));
    var pph = parseFloat($("#fieldFormNota input[name=intPPN]").autoNumeric('get'));
    console.log(calculateTotalNota(nominal, pph));
    $("input[name='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
});

$("#fieldFormNota input[name=intPPN]").keyup(function() {
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    var nominal = parseFloat($("#fieldFormNota input[name=intAmount]").autoNumeric('get'));
    var pph = parseFloat($(this).autoNumeric('get'));
    console.log(calculateTotalNota(nominal, pph));
    $("input[name='intTotal']").autoNumeric('set', calculateTotalNota(nominal, pph));
}); 

function calculateTotalNota(intNominal, intPPH) {
    return intNominal+(intNominal * intPPH/100);
}

function handlerPiutang() {
    

$("input[name^='intAmount']").keyup(function(){                        
    var total = 0;
    $.each($(this), function() {
        total += $(this).autoNumeric('get');
        $("#tDetailInvoiceList input[name=intTotalAmountInvoice]").autoNumeric('set',total);
    });

    var pph = $(this).closest("tr.resultInvoice").find("input[name^='intPph']").autoNumeric('get');
    var thisAmount = $(this).autoNumeric('get');

    $(this).closest("tr.resultInvoice").find("input[name^='intAmountTotalAfterPph']").autoNumeric('set', thisAmount - (thisAmount*pph/100));
                
    // $.each($(this), function(){
    //     $("#tDetailInvoiceList input[name^='intTotalAmountInvoice']").autoNumeric('set',$(this).autoNumeric('get'));
    // });
});              
}

function calculateInvoice() {    
    var amountTotalInvoice = 0; 
    $(".currency").autoNumeric('init', autoNumericOptionsRupiah);
    $.each($("input[name^='intAmountAfterPPH']"), function() {        
        amountTotalInvoice += $(this).autoNumeric('get');
    });   
    $("#intTotalInvoiceDibayar").autoNumeric('set', amountTotalInvoice);
    // $("input[name=intPph]").closest("tr.resultInvoice").find("input[name=intRencanaBayar]").autoNumeric("set",total);
}

$("input[type='checkbox'][name^='cbxLunas']").click(function(){
    var totalUMCReal = $(this).parents("tr.resultInvoice").find("input[name^='intAmountUMCused']").autoNumeric('get');
    var inputUMCMapping = $(this).parents("tr.resultInvoice").find("input[name^='umcInvoiceMapping']");
    var sisa = parseFloat(inputUMCMapping.val().split("-")[1])-parseFloat(totalUMCReal);
    // var umcID = inputUMCMapping.val().split("-")[0];
    var editedUMCMapping = inputUMCMapping.val().split("-")[0]+"-"+totalUMCReal+"-"+inputUMCMapping.val().split("-")[2]+"-0-"+sisa;
    if(!$(this).is(":checked")){        
        inputUMCMapping.val(editedUMCMapping);
    }
});

$(".chosen").chosen({width: "100%",search_contains: true});
</script>