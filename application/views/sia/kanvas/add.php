<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-dropbox"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvas'), 'link' => site_url('kanvas/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12">
    <form name="frmAddKanvas" id="frmAddKanvas" class="frmShop" method="post" action="<?=site_url('kanvas', NULL, FALSE)?>">
        <!--header-->
        <div class="row">

            <!--tanggal-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar"></i> Tanggal</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label>
                                <input type="text"
                                       name="txtDate"
                                       value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>"
                                       class="form-control required jwDateTime"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon">Muat</label>
                                <input type="text"
                                       name="txtLoadingDate"
                                       value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>"
                                       class="form-control required jwDateTime"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon">Turun</label>
                                <input type="text"
                                       name="txtDischargeDate"
                                       class="form-control jwDateTime"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--tanggal-->

            <!--input gudang-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-thumb-tack"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')?></h4>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text"
                                       id="warehouseName"
                                       name="warehouseName"
                                       class="form-control"
                                       placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectwarehouse')?>"
                                    />
                                <label class="input-group-addon" id="loadWarehouse"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--input gudang-->

            <!--input salesman-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-male"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-selectsalesman')?></h4>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text"
                                       id="salesmanName"
                                       name="salesmanName"
                                       class="form-control"
                                       placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectsalesman')?>"
                                    />
                                <label class="input-group-addon" id="loadSalesman"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></label>
                                <input type="text"
                                       name="salesmanCode"
                                       class="form-control"
                                       disabled 
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--input salesman-->

        </div> <!--header-->

        <div class="row">
            <!--hari muat-->
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-automobile"></i> Muatan pada hari</h3>
                            </div>
                            <div class="panel-body">
                                <input type="radio" name="intHari" id="radSenin" value="1" checked/>
                                <label for="radSenin">Senin&nbsp</label>
                                <input type="radio" name="intHari" id="radSelasa" value="2"/>
                                <label for="radSelasa">Selasa&nbsp</label>
                                <input type="radio" name="intHari" id="radRabu" value="3"/>
                                <label for="radRabu">Rabu&nbsp</label>
                                <input type="radio" name="intHari" id="radKamis" value="4"/>
                                <label for="radKamis">Kamis&nbsp</label>
                                <input type="radio" name="intHari" id="radJumat" value="5"/>
                                <label for="radJumat">Jumat&nbsp</label>
                                <input type="radio" name="intHari" id="radSabtu" value="6"/>
                                <label for="radSabtu">Sabtu&nbsp</label>
                                <input type="radio" name="intHari" id="radSaldo" value="0"/>
                                <label for="radSaldo">Saldo</label>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!--hari muat-->

        </div>

        <!--row 1 : sisa & jual-->
        <div class="row">
            <!--daftar item sisa-->
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-sisa')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div class="form-group">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemSisa"
                                       name="txtNewItemSisa"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                />
                                <label class="input-group-addon"
                                       id="loadItemSisa">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsSisa">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="info">
                                        <td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item sisa-->

            <!--daftar item jual-->
            <div class="col-md-6 hidden">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-jual')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div class="form-group">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemJual"
                                       name="txtNewItemJual"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                />
                                <label class="input-group-addon"
                                       id="loadItemJual">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsJual">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="info">
                                        <td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item jual-->
        </div><!--row 1 : sisa & jual-->

        <!--row 2 : turun & retur-->
        <div class="row hidden">
            <!--daftar item turun-->
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-turun')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div class="form-group">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemTurun"
                                       name="txtNewItemTurun"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemTurun">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsTurun">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item turun-->

            <!--daftar item retur-->
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-retur')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div class="form-group">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemRetur"
                                       name="txtNewItemRetur"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemRetur">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsRetur">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="info">
                                    <td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item jual-->
        </div><!--row 2 : turun & retur-->

        <!--footer-->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-info-circle"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--footer-->

        <div class="form-group"><button type="submit" name="smtMakeKanvas" value="Make Kanvas" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>

        <input type="hidden" id="warehouseID" name="warehouseID"/>
        <input type="hidden" id="salesmanID" name="salesmanID"/>
        <input type="hidden" id="totalItemSisa" name="totalItemSisa"/>
        <input type="hidden" id="totalItemJual" name="totalItemJual"/>
        <input type="hidden" id="totalItemTurun" name="totalItemTurun"/>
        <input type="hidden" id="totalItemRetur" name="totalItemRetur"/>
    </form>
</div>