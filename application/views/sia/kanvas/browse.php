<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-dropbox"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvas'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
    <form name="searchForm" method="post" action="<?=site_url('kanvas/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">
        <div class="input-group" style="max-width:250px;">
            <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="Search By Salesman"/>
            <span class="input-group-btn">
                <button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button>
            </span>
        </div>
    </form>

    <div class="pull-left" style="margin-top: -25px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right; margin-left:10px;"><a href="<?=site_url('kanvas', NULL, FALSE)?>" target="_blank" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive">
        <table class="table table-bordered table-condensed table-hover">
            <thead>
                <tr>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name').' / '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-status')?></th>
                    <th class="action">Action</th>
                </tr>
            </thead>
            <tbody><?php
                if (!empty($arrKanvas)):
                    foreach($arrKanvas as $e): ?>
                <tr>
                    <td><?=$e['kanv_code']?></td>
                    <td><?=formatPersonName('ABBR',$e['sals_name'],$e['sals_address'],"",$e['sals_phone'])?></td>
                    <td><?=formatDate2($e['kanv_date'],'d F Y')?></td>
                    <td><?=$e['kanv_status']?></td>
                    <td class="action">
						<?php if($e['bolAllowView']): ?><a href="<?=site_url('kanvas/view/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>  
						<?php if($e['bolBtnPrint']): ?><a href="<?=site_url('kanvas/view/'.$e['id'].'?print=true', NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-print')?></a><?php endif; ?>  
					</td>
                </tr>
                    <?php
                    endforeach;
                else: ?>
                <tr><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr>
                    <?php
                endif; ?>
            </tbody>
        </table>
    </div>
    <?=$strPage?>
</div>