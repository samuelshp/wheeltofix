<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
var poolItemSisaID = $('#idItemSisaAwal').val().slice(0,-2).split(",");
var poolKaviSisaID = $('#idKaviSisaAwal').val().slice(0,-2).split(",");
var totalItemSisa=0;
var numberItemSisa = $("#totalItemSisa").val();
var totalNewItemSisa = 0;

var poolItemJualID = $('#idItemJualAwal').val().split(",");
var totalItemJual=0;
var numberItemJual = $("#totalItemJual").val();

var poolItemTurunID = $('#idItemTurunAwal').val().slice(0,-1).split(",");
var poolKaviTurunID = $('#idKaviTurunAwal').val().slice(0,-1).split(",");
var totalItemTurun=0;
var numberItemTurun = $("#totalItemTurun").val();
var deletedKaviTurunID = [];
var deletedItemTurunID = [];

var poolItemReturID = $('#idItemReturAwal').val().split(",");
var totalItemRetur=0;
var numberItemRetur = $("#totalItemRetur").val();

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("select[name='selJumpTo']").change(function() {
	if($(this).find("option:selected").val() != '')
		window.location.href = $(this).find("option:selected").val();
});

$("#listDay input:radio").click(function() {
	var intDay = $('input[type="radio"]:checked').val();

	if (!confirmSwitchDay()) {
		return false;
	}


	poolItemSisaID = [];
	$('#idItemSisaAwal').val('');

	poolItemJualID = [];
	$('#idItemJualAwal').val('');
	totalItemJual = 0;
	$('#totalItemJual').val(0);

	poolItemTurunID = [];
	$('#idItemTurunAwal').val('');
	totalItemTurun = 0;
	$('#totalItemTurun').val(0);

	poolItemReturID = [];
	$('#idItemReturAwal').val('');
	totalItemRetur = 0;
	$('#totalItemRetur').val(0);

	$("#listItemsSisa tbody").html('');
	$("#listItemsJual tbody").html('');
	$("#listItemsTurun tbody").html('');
	$("#listItemsRetur tbody").html('');

	if (intDay == 0) {
		$('#addBarangSisa').addClass('hidden');
		$('#addBarangJual').addClass('hidden');
		$('#addBarangTurun').addClass('hidden');
		$('#addBarangRetur').addClass('hidden');
	} else {
		$('#addBarangSisa').removeClass('hidden');
		$('#addBarangJual').removeClass('hidden');
		$('#addBarangTurun').removeClass('hidden');
		$('#addBarangRetur').removeClass('hidden');
	}

	/* tampilkan item sesuai hari yang dipilih */
	$.ajax({
		url: "<?=site_url('kanvas_ajax/getItemsByDay', NULL, FALSE)?>/" + $("#kanvasID").val() + "/0/" + intDay,
		success: function(data) {

			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			var i=0;
			totalNewItemSisa = 0;

			$.map(xml.find('KanvasItems').find('item'),function(val){

				if ($(val).find('product_id').text() != '0') {
                    
                    if (intDay == 0) {
                            $('#listItemsSisa tbody').append(
                                '<tr id="sisaX'+i+'">'+
                                    '<td class="cb">'+
                                    '</td>'+
                                    '<td id="nameItemSisaX'+i+'"></td>'+
                                    '<td class="qty" id="qtyItemSisaX'+i+'">'+

                                    '</td>'+


                                    '<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+

                                    '<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv3UnitSisaX'+i+'" >'+

                                    '<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'">'+ 
                                    '<input type="hidden" name="idKaviSisaID['+$(val).find('id').text()+']" value="'+$(val).find('id').text()+'"/>'+
                                '</tr>'
                            );

                            $("#qtyItemSisaX"+i).append($(val).find('kavi_quantity1').text() + ' ' + $(val).find('prod_unit1').text() + ' + ' +
                                                        $(val).find('kavi_quantity2').text() + ' ' + $(val).find('prod_unit2').text() + ' + ' +
                                                        $(val).find('kavi_quantity3').text() + ' ' + $(val).find('prod_unit3').text());

    
                    } else if (intDay == 1) {
                        $('#listItemsSisa tbody').append(
                            '<tr id="sisaX'+i+'">'+
                                '<td class="cb">'+
                                    '<input type="checkbox" name="cbDeleteSisaX'+i+'" id="cbDeleteSisaX'+i+'"/>'+
                                '</td>'+
                                '<td id="nameItemSisaX'+i+'"></td>'+
                                '<td class="qty" id="qtyItemSisaX'+i+'">'+

    
                                    '<div class="form-group">'+
                                        '<div class="col-xs-4">'+
                                            '<input type="text"'+
                                                   'name="qty1ItemSisaX'+i+'"'+
                                                   'class="required number form-control input-sm"'+
                                                   'id="qty1ItemSisaX'+i+'"/>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<input type="text"'+
                                                   'name="qty2ItemSisaX'+i+'"'+
                                                   'class="required number form-control input-sm"'+
                                                   'id="qty2ItemSisaX'+i+'"/>'+
                                        '</div>'+
                                        '<div class="col-xs-4">'+
                                            '<input type="text"'+
                                                   'name="qty3ItemSisaX'+i+'"'+
                                                   'class="required number form-control input-sm"'+
                                                   'id="qty3ItemSisaX'+i+'"/>'+
                                        '</div>'+
                                    '</div>'+
    
                                '</td>'+

                                '<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
                                '<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
                                '<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+
    
                                '<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
                                '<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
                                '<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
                                '<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
                                '<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
                                '<input type="hidden" id="conv3UnitSisaX'+i+'" >'+
    
                                '<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'">'+ 
                                '<input type="hidden" name="idKaviSisaID['+$(val).find('id').text()+']" value="'+$(val).find('id').text()+'"/>'+
                            '</tr>'

                        );
                    

                    } else {
                        
                        if ($(val).find('saldo').text() == '-1') {
                            $('#listItemsSisa tbody').append(
                                '<tr id="sisaX'+i+'">'+
                                    '<td class="cb">'+
                                        '<input type="checkbox" name="cbDeleteSisaX'+i+'" id="cbDeleteSisaX'+i+'"/>'+
                                    '</td>'+
                                    '<td id="nameItemSisaX'+i+'"></td>'+
                                    '<td class="qty" id="qtyItemSisaX'+i+'">'+


                                        '<div class="form-group">'+
                                            '<div class="col-xs-4">'+
                                                '<input type="text"'+
                                                       'name="qty1ItemSisaX'+i+'"'+
                                                       'class="required number form-control input-sm"'+
                                                       'id="qty1ItemSisaX'+i+'"/>'+
                                            '</div>'+
                                            '<div class="col-xs-4">'+
                                                '<input type="text"'+
                                                       'name="qty2ItemSisaX'+i+'"'+
                                                       'class="required number form-control input-sm"'+
                                                       'id="qty2ItemSisaX'+i+'"/>'+
                                            '</div>'+
                                            '<div class="col-xs-4">'+
                                                '<input type="text"'+
                                                       'name="qty3ItemSisaX'+i+'"'+
                                                       'class="required number form-control input-sm"'+
                                                       'id="qty3ItemSisaX'+i+'"/>'+
                                            '</div>'+
                                        '</div>'+

                                    '</td>'+

                                    '<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+

                                    '<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv3UnitSisaX'+i+'" >'+

                                    '<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'">'+ 
                                    '<input type="hidden" name="idKaviSisaID['+$(val).find('id').text()+']" value="'+$(val).find('id').text()+'"/>'+
                                '</tr>'

                            );
                            
                        } else {                        
                            $('#listItemsSisa tbody').append(
                                '<tr id="sisaX'+i+'">'+
                                    '<td class="cb">'+
                                    '</td>'+
                                    '<td id="nameItemSisaX'+i+'"></td>'+
                                    '<td class="qty" id="qtyItemSisaX'+i+'">'+

                                    '</td>'+


                                    '<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
                                    '<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+

                                    '<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
                                    '<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
                                    '<input type="hidden" id="conv3UnitSisaX'+i+'" >'+

                                    '<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'">'+ 
                                    '<input type="hidden" name="idKaviSisaID['+$(val).find('id').text()+']" value="'+$(val).find('id').text()+'"/>'+
                                '</tr>'
                            );

                            $("#qtyItemSisaX"+i).append($(val).find('kavi_quantity1').text() + ' ' + $(val).find('prod_unit1').text() + ' + ' +
                                                        $(val).find('kavi_quantity2').text() + ' ' + $(val).find('prod_unit2').text() + ' + ' +
                                                        $(val).find('kavi_quantity3').text() + ' ' + $(val).find('prod_unit3').text());

                        };                         

                    };
                    
                    var prodTitle = $(val).find('prod_title').text();
					var probTitle = $(val).find('prob_title').text();
					var strName = $(val).find('strName').text();
					var id = $(val).find('product_id').text();
					poolItemSisaID.push(id);

					var unitItem1 = $(val).find('kavi_unit1').text();
					var unitItem2 = $(val).find('kavi_unit2').text();
					var unitItem3 = $(val).find('kavi_unit3').text();

					$("#nameItemSisaX"+i).text(strName);                    
                    

					$("#idItemSisaX"+i).val($(val).find('product_id').text());
					$("#prodItemSisaX"+i).val(prodTitle);
					$("#probItemSisaX"+i).val(probTitle);
					$("#qty1ItemSisaX"+i).val($(val).find('kavi_quantity1').text());
					$("#qty2ItemSisaX"+i).val($(val).find('kavi_quantity2').text());
					$("#qty3ItemSisaX"+i).val($(val).find('kavi_quantity3').text());                    
                    
                    if (intDay == 1) {
                        $("#asSaldoX"+i).val(0);
                    } else
                        $("#asSaldoX"+i).val(1);
                                            

					if (unitItem1 == '0') {
						$("#qty1ItemSisaX" + i).addClass("hidden");
					} else {
						$("#sel1UnitSisaIDX" + i).val(unitItem1);
					};
					if (unitItem2 == '0') {
						$("#qty2ItemSisaX" + i).addClass("hidden");
					} else {
						$("#sel2UnitSisaIDX" + i).val(unitItem2);
					};
					if (unitItem3 == '0') {
						$("#qty3ItemSisaX" + i).addClass("hidden");
					} else {
						$("#sel3UnitSisaIDX" + i).val(unitItem3);
					};

					/*if (intDay == '0') {
						$("#cbDeleteSisaX" + i).addClass("hidden");
						$("#qty1ItemSisaX"+i).attr("disabled","disabled");
						$("#qty2ItemSisaX"+i).attr("disabled","disabled");
						$("#qty3ItemSisaX"+i).attr("disabled","disabled");
					}*/

					i++;
					$("#totalNewItemSisa").val(i);
				}
			});

			if (i == 0) {
				$('#listItemsSisa tbody').append(
					'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
				);
			}
		}
	});

	$.ajax({
		url: "<?=site_url('kanvas_ajax/getItemsByDay', NULL, FALSE)?>/" + $("#kanvasID").val() + "/1/" + intDay,
		success: function(data) {

			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			var i=0;

			$.map(xml.find('KanvasItems').find('item'),function(val){
				if (($(val).find('product_id').text() != '')) {

					$('#listItemsJual tbody').append(
				        '<tr>'+
				            '<td class="cb">'+
				                '<input type="checkbox" name="cbDeleteJualX'+i+'" id="cbDeleteJualX'+i+'"/>'+
				            '</td>'+
							'<td id="nameItemJualX'+i+'"></td>'+
							'<td class="qty">'+
				                '<div class="form-group">'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty1ItemJualX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty1ItemJualX'+i+'"/>'+
				                    '</div>'+
				                    '<div class="col-xs-4">'+
                                        '<input type="text"'+
				                               'name="qty2ItemJualX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty2ItemJualX'+i+'"/>'+
				                    '</div>'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty3ItemJualX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty3ItemJualX'+i+'"/>'+
                                    '</div>'+
				                '</div>'+
							'</td>'+

							'<input type="hidden" id="idItemJualX'+i+'" name="idItemJual'+i+'"/>'+
							'<input type="hidden" id="prodItemJualX'+i+'" name="prodItemJual'+i+'"/>'+
							'<input type="hidden" id="probItemJualX'+i+'" name="probItemJual'+i+'"/>'+
							'<input type="hidden" id="sel1UnitJualIDX'+i+'" name="sel1UnitJualID'+i+'">'+
							'<input type="hidden" id="sel2UnitJualIDX'+i+'" name="sel2UnitJualID'+i+'">'+
							'<input type="hidden" id="sel3UnitJualIDX'+i+'" name="sel3UnitJualID'+i+'">'+
							'<input type="hidden" id="conv1UnitJualX'+i+'" >'+
							'<input type="hidden" id="conv2UnitJualX'+i+'" >'+
							'<input type="hidden" id="conv3UnitJualX'+i+'" >'+
				        '</tr>'
					);
					var prodTitle = $(val).find('prod_title').text();
					var probTitle = $(val).find('prob_title').text();
					var strName = $(val).find('strName').text();
					var id = $(val).find('id').text();
					poolItemJualID.push(id);

					var unitItem1 = $(val).find('kavi_unit1').text();
					var unitItem2 = $(val).find('kavi_unit2').text();
					var unitItem3 = $(val).find('kavi_unit3').text();

					$("#nameItemJualX"+i).text(strName);

					$("#idItemJualX"+i).val($(val).find('product_id').text());
					$("#prodItemJualX"+i).val(prodTitle);
					$("#probItemJualX"+i).val(probTitle);
					$("#qty1ItemJualX"+i).val($(val).find('kavi_quantity1').text());
					$("#qty2ItemJualX"+i).val($(val).find('kavi_quantity2').text());
					$("#qty3ItemJualX"+i).val($(val).find('kavi_quantity3').text());

					if (unitItem1 == '0') {
						$("#qty1ItemJualX" + i).addClass("hidden");
					} else {
						$("#sel1UnitJualIDX" + i).val(unitItem1);
					};
					if (unitItem2 == '0') {
						$("#qty2ItemJualX" + i).addClass("hidden");
					} else {
						$("#sel2UnitJualIDX" + i).val(unitItem2);
					};
					if (unitItem3 == '0') {
						$("#qty3ItemJualX" + i).addClass("hidden");
					} else {
						$("#sel3UnitJualIDX" + i).val(unitItem3);
					};

					if (intDay == '0') {
						$("#cbDeleteJualX" + i).addClass("hidden");
						$("#qty1ItemJualX"+i).attr("disabled","disabled");
						$("#qty2ItemJualX"+i).attr("disabled","disabled");
						$("#qty3ItemJualX"+i).attr("disabled","disabled");
					}

					i++;
					/*totalItemJual++;
					 numberItemJual++;*/
					$("#totalItemJual").val(i);
				}
			});

			if (i == 0) {
				$('#listItemsJual tbody').append(
					'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
				);
			}
		}
	});

	$.ajax({
		url: "<?=site_url('kanvas_ajax/getItemsByDay', NULL, FALSE)?>/" + $("#kanvasID").val() + "/2/" + intDay,
		success: function(data) {

			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			var i=0;

			$.map(xml.find('KanvasItems').find('item'),function(val){
				if (($(val).find('product_id').text() != '')) {

					if (intDay == 0) {
                            $('#listItemsTurun tbody').append(
                                '<tr id="turunX'+i+'">'+
                                    '<td class="cb">'+
                                    '</td>'+
                                    '<td id="nameItemTurunX'+i+'"></td>'+
                                    '<td class="qty" id="qtyItemTurunX'+i+'">'+

                                    '</td>'+


                                    '<input type="hidden" id="idItemTurunX'+i+'" name="idItemTurun'+i+'"/>'+
                                    '<input type="hidden" id="prodItemTurunX'+i+'" name="prodItemTurun'+i+'"/>'+
                                    '<input type="hidden" id="probItemTurunX'+i+'" name="probItemTurun'+i+'"/>'+

                                    '<input type="hidden" id="sel1UnitTurunIDX'+i+'" name="sel1UnitTurunID'+i+'">'+
                                    '<input type="hidden" id="sel2UnitTurunIDX'+i+'" name="sel2UnitTurunID'+i+'">'+
                                    '<input type="hidden" id="sel3UnitTurunIDX'+i+'" name="sel3UnitTurunID'+i+'">'+
                                    '<input type="hidden" id="conv1UnitTurunX'+i+'" >'+
                                    '<input type="hidden" id="conv2UnitTurunX'+i+'" >'+
                                    '<input type="hidden" id="conv3UnitTurunX'+i+'" >'+

                                    /*'<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'">'+ */
                                    '<input type="hidden" name="idKaviTurunID['+$(val).find('id').text()+']" value="'+$(val).find('id').text()+'"/>'+
                                '</tr>'
                            );

                            $("#qtyItemTurunX"+i).append($(val).find('kavi_quantity1').text() + ' ' + $(val).find('prod_unit1').text() + ' + ' +
                                                        $(val).find('kavi_quantity2').text() + ' ' + $(val).find('prod_unit2').text() + ' + ' +
                                                        $(val).find('kavi_quantity3').text() + ' ' + $(val).find('prod_unit3').text());

    
                    } else {
                    $('#listItemsTurun tbody').append(
						'<tr id="turunX'+i+'">'+
				            '<td class="cb">'+
				                '<input type="checkbox" name="cbDeleteTurunX'+i+'" id="cbDeleteTurunX'+i+'"/>'+
				            '</td>'+
							'<td id="nameItemTurunX'+i+'"></td>'+
				            '<td class="qty">'+
				                '<div class="form-group">'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty1ItemTurunX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                              'id="qty1ItemTurunX'+i+'"/>'+
				                    '</div>'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty2ItemTurunX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                              'id="qty2ItemTurunX'+i+'"/>'+
				                    '</div>'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty3ItemTurunX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty3ItemTurunX'+i+'"/>'+
                                    '</div>'+
				                '</div>'+
							'</td>'+

							'<input type="hidden" id="idItemTurunX'+i+'" name="idItemTurun'+i+'"/>'+
							'<input type="hidden" id="prodItemTurunX'+i+'" name="prodItemTurun'+i+'"/>'+
							'<input type="hidden" id="probItemTurunX'+i+'" name="probItemTurun'+i+'"/>'+
							'<input type="hidden" id="sel1UnitTurunIDX'+i+'" name="sel1UnitTurunID'+i+'">'+
							'<input type="hidden" id="sel2UnitTurunIDX'+i+'" name="sel2UnitTurunID'+i+'">'+
							'<input type="hidden" id="sel3UnitTurunIDX'+i+'" name="sel3UnitTurunID'+i+'">'+
							'<input type="hidden" id="conv1UnitTurunX'+i+'" >'+
							'<input type="hidden" id="conv2UnitTurunX'+i+'" >'+
							'<input type="hidden" id="conv3UnitTurunX'+i+'" >'+
				        '</tr>'
					)};
					var prodTitle = $(val).find('prod_title').text();
					var probTitle = $(val).find('prob_title').text();
					var strName = $(val).find('strName').text();
					var id = $(val).find('id').text();
					if ($.inArray(id, poolItemTurunID) == -1) {
						poolItemTurunID.push(id);
					};

					var unitItem1 = $(val).find('kavi_unit1').text();
					var unitItem2 = $(val).find('kavi_unit2').text();
					var unitItem3 = $(val).find('kavi_unit3').text();

					$("#nameItemTurunX"+i).text(strName);

					$("#idItemTurunX"+i).val($(val).find('product_id').text());
					$("#prodItemTurunX"+i).val(prodTitle);
					$("#probItemTurunX"+i).val(probTitle);
					$("#qty1ItemTurunX"+i).val($(val).find('kavi_quantity1').text());
					$("#qty2ItemTurunX"+i).val($(val).find('kavi_quantity2').text());
					$("#qty3ItemTurunX"+i).val($(val).find('kavi_quantity3').text());

					if (unitItem1 == '0') {
						$("#qty1ItemTurunX" + i).addClass("hidden");
					} else {
						$("#sel1UnitTurunIDX" + i).val(unitItem1);
					};
					if (unitItem2 == '0') {
						$("#qty2ItemTurunX" + i).addClass("hidden");
					} else {
						$("#sel2UnitTurunIDX" + i).val(unitItem2);
					};
					if (unitItem3 == '0') {
						$("#qty3ItemTurunX" + i).addClass("hidden");
					} else {
						$("#sel3UnitTurunIDX" + i).val(unitItem3);
					};

					if (intDay == '0') {
						$("#cbDeleteTurunX" + i).addClass("hidden");
						$("#qty1ItemTurunX"+i).attr("disabled","disabled");
						$("#qty2ItemTurunX"+i).attr("disabled","disabled");
						$("#qty3ItemTurunX"+i).attr("disabled","disabled");
					}

					i++;
					/*totalItemTurun++;
					 numberItemTurun++;*/
					$("#totalItemTurun").val(i);
				}
			});

			if (i == 0) {
				$('#listItemsTurun tbody').append(
					'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
				);
			}

		}
	});

	$.ajax({
		url: "<?=site_url('kanvas_ajax/getItemsByDay', NULL, FALSE)?>/" + $("#kanvasID").val() + "/3/" + intDay,
		success: function(data) {

			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			var i=0;

			$.map(xml.find('KanvasItems').find('item'),function(val){
				if (($(val).find('product_id').text() != '')) {

					$('#listItemsRetur tbody').append(
						'<tr>'+
				            '<td class="cb">'+
				                '<input type="checkbox" name="cbDeleteReturX'+i+'" id="cbDeleteReturX'+i+'"/>'+
							'</td>'+
				            '<td id="nameItemReturX'+i+'"></td>'+
				            '<td class="qty">'+
				                '<div class="form-group">'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty1ItemReturX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                              'id="qty1ItemReturX'+i+'"/>'+
                                    '</div>'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty2ItemReturX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty2ItemReturX'+i+'"/>'+
				                    '</div>'+
				                    '<div class="col-xs-4">'+
				                        '<input type="text"'+
				                               'name="qty3ItemReturX'+i+'"'+
				                               'class="required number form-control input-sm"'+
				                               'id="qty3ItemReturX'+i+'"/>'+
				                    '</div>'+
				                '</div>'+
				            '</td>'+

							'<input type="hidden" id="idItemReturX'+i+'" name="idItemRetur'+i+'"/>'+
							'<input type="hidden" id="prodItemReturX'+i+'" name="prodItemRetur'+i+'"/>'+
							'<input type="hidden" id="probItemReturX'+i+'" name="probItemRetur'+i+'"/>'+
							'<input type="hidden" id="sel1UnitReturIDX'+i+'" name="sel1UnitReturID'+i+'">'+
							'<input type="hidden" id="sel2UnitReturIDX'+i+'" name="sel2UnitReturID'+i+'">'+
							'<input type="hidden" id="sel3UnitReturIDX'+i+'" name="sel3UnitReturID'+i+'">'+
							'<input type="hidden" id="conv1UnitReturX'+i+'" >'+
							'<input type="hidden" id="conv2UnitReturX'+i+'" >'+
							'<input type="hidden" id="conv3UnitReturX'+i+'" >'+
				        '</tr>'
					);
					var prodTitle = $(val).find('prod_title').text();
					var probTitle = $(val).find('prob_title').text();
					var strName = $(val).find('strName').text();
					var id = $(val).find('id').text();
					poolItemReturID.push(id);

					var unitItem1 = $(val).find('kavi_unit1').text();
					var unitItem2 = $(val).find('kavi_unit2').text();
					var unitItem3 = $(val).find('kavi_unit3').text();

					$("#nameItemReturX"+i).text(strName);

					$("#idItemReturX"+i).val($(val).find('product_id').text());
					$("#prodItemReturX"+i).val(prodTitle);
					$("#probItemReturX"+i).val(probTitle);
					$("#qty1ItemReturX"+i).val($(val).find('kavi_quantity1').text());
					$("#qty2ItemReturX"+i).val($(val).find('kavi_quantity2').text());
					$("#qty3ItemReturX"+i).val($(val).find('kavi_quantity3').text());

					if (unitItem1 == '0') {
						$("#qty1ItemReturX" + i).addClass("hidden");
					} else {
						$("#sel1UnitReturIDX" + i).val(unitItem1);
					};
					if (unitItem2 == '0') {
						$("#qty2ItemReturX" + i).addClass("hidden");
					} else {
						$("#sel2UnitReturIDX" + i).val(unitItem2);
					};
					if (unitItem3 == '0') {
						$("#qty3ItemReturX" + i).addClass("hidden");
					} else {
						$("sel3UnitReturIDX" + i).val(unitItem3);
					};

					if (intDay == '0') {
						$("#cbDeleteReturX" + i).addClass("hidden");
						$("#qty1ItemReturX"+i).attr("disabled","disabled");
						$("#qty2ItemReturX"+i).attr("disabled","disabled");
						$("#qty3ItemReturX"+i).attr("disabled","disabled");
					}


					i++;
					/*totalItemRetur++;
					 numberItemRetur++;*/
					$("#totalItemRetur").val(i);
				}
			});

			if (i == 0) {
				$('#listItemsRetur tbody').append(
					'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
				);
			}

		}
	});
});


$("#txtNewItemSisa").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemSisa"]').val(),
			beforeSend: function() {
				$("#loadItemSisa").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemSisa").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItem = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					/*if ($.inArray(intID,poolItemSisaID) > -1) {

					} else {*/
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					/*}*/
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = $("#totalNewItemSisa").val();

		if (numberItemSisa == '0') {
			$("#listItemsSisa tbody").html('');
		};

		$('#listItemsSisa tbody').append(
			'<tr id="sisaX'+i+'">'+
                '<td class="cb">'+
                    '<input type="checkbox" name="cbDeleteSisaX'+i+'"/>'+
				'</td>'+
                '<td id="nameItemSisaX'+i+'"></td>'+
                '<td class="qty">'+
                    '<div class="form-group">'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty1ItemSisaX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                  'id="qty1ItemSisaX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty2ItemSisaX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty2ItemSisaX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty3ItemSisaX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty3ItemSisaX'+i+'"/>'+
                        '</div>'+
                    '</div>'+
				'</td>'+
				'<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
				'<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
				'<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+
				'<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
				'<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
				'<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
				'<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
				'<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
				'<input type="hidden" id="conv3UnitSisaX'+i+'" >'+
                '<input type="hidden" id="asSaldoX'+i+'" name="asSaldoX'+i+'" value="0">'+ 
            '</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemSisaID.push(id);

		$("#nameItemSisaX"+i).text("("+strKode+") "+ strName);

		$("#idItemSisaX"+i).val($(selItem).find('id').text());
		$("#prodItemSisaX"+i).val(prodTitle);
		$("#probItemSisaX"+i).val(probTitle);
		$("#qty1ItemSisaX"+i).hide();
		$("#qty2ItemSisaX"+i).hide();
		$("#qty3ItemSisaX"+i).hide();
        
        /*$("#asSaldoX"+i).val(0);*/

		var i2 = i;
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemSisaX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				var unitStr =[];
				var unitConv = [];
				$.map(xml.find('Unit').find('item'), function(val,i2) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();
					var intConv = $(val).find('unit_conversion').text();

					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
				});

				for(var zz=1;zz<=unitID.length;zz++) {
					$("#qty"+zz+"ItemSisaX"+i2).show();
					$("#sel"+zz+"UnitSisaIDX"+i2).val(unitID[zz-1]);
					$("#conv"+zz+"UnitSisaX"+i2).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemSisaX"+i2).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		i++;
		numberItemSisa++;
		$("#totalNewItemSisa").val(i);
	}
});

$("#txtNewItemJual").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemJual"]').val(),
			beforeSend: function() {
				$("#loadItemJual").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemJual").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItem = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemJualID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = $("#totalItemJual").val();
		/*var i = totalItemJual;*/
		if (numberItemJual == '0') {
			$("#listItemsJual tbody").html('');
		};

		$('#listItemsJual tbody').append(
			'<tr>'+
				'<td class="cb">'+
                    '<input type="checkbox" name="cbDeleteJualX'+i+'"/>'+
				'</td>'+
                '<td id="nameItemJualX'+i+'"></td>'+
                '<td class="qty">'+
                    '<div class="form-group">'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty1ItemJualX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty1ItemJualX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty2ItemJualX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty2ItemJualX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty3ItemJualX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                  'id="qty3ItemJualX'+i+'"/>'+
                        '</div>'+
                    '</div>'+
                '</td>'+
				'<input type="hidden" id="idItemJualX'+i+'" name="idItemJual'+i+'"/>'+
				'<input type="hidden" id="prodItemJualX'+i+'" name="prodItemJual'+i+'"/>'+
				'<input type="hidden" id="probItemJualX'+i+'" name="probItemJual'+i+'"/>'+
				'<input type="hidden" id="sel1UnitJualIDX'+i+'" name="sel1UnitJualID'+i+'">'+
				'<input type="hidden" id="sel2UnitJualIDX'+i+'" name="sel2UnitJualID'+i+'">'+
				'<input type="hidden" id="sel3UnitJualIDX'+i+'" name="sel3UnitJualID'+i+'">'+
				'<input type="hidden" id="conv1UnitJualX'+i+'" >'+
				'<input type="hidden" id="conv2UnitJualX'+i+'" >'+
				'<input type="hidden" id="conv3UnitJualX'+i+'" >'+
            '</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemJualID.push(id);

		$("#nameItemJualX"+i).text("("+strKode+") "+ strName);

		$("#idItemJualX"+i).val($(selItem).find('id').text());
		$("#prodItemJualX"+i).val(prodTitle);
		$("#probItemJualX"+i).val(probTitle);
		$("#qty1ItemJualX"+i).hide();
		$("#qty2ItemJualX"+i).hide();
		$("#qty3ItemJualX"+i).hide();

		var i2 = i;
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemJualX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i2) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemJualX"+i2).show();
					$("#sel"+zz+"UnitJualIDX"+i2).val(unitID[zz-1]);
					$("#qty"+zz+"ItemJualX"+i2).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		/*totalItemJual++;*/
		i++;
		numberItemJual++;
		$("#totalItemJual").val(i);
	}
});

$("#txtNewItemTurun").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemTurun"]').val(),
			beforeSend: function() {
				$("#loadItemTurun").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemTurun").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItem = xml;
				var display = [];
				/*console.log('poolItemTurunID : ' + poolItemTurunID + '||' + 'deletedItemTurunID : ' + deletedItemTurunID);*/
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();

					if ($.inArray(intID,deletedItemTurunID) > -1) {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					} else {
						if ($.inArray(intID,poolItemTurunID) == -1) {
							display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
						}
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = $("#totalItemTurun").val();
		if (numberItemTurun == '0') {
			$("#listItemsTurun tbody").html('');
		};

		$('#listItemsTurun tbody').append(
			'<tr id="turunX'+i+'">'+
				'<td class="cb">'+
                    '<input type="checkbox" name="cbDeleteTurunX'+i+'"/>'+
				'</td>'+
                '<td id="nameItemTurunX'+i+'"></td>'+
                '<td class="qty">'+
                    '<div class="form-group">'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty1ItemTurunX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty1ItemTurunX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty2ItemTurunX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty2ItemTurunX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty3ItemTurunX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty3ItemTurunX'+i+'"/>'+
                        '</div>'+
                    '</div>'+
				'</td>'+
				'<input type="hidden" id="idItemTurunX'+i+'" name="idItemTurun'+i+'"/>'+
				'<input type="hidden" id="prodItemTurunX'+i+'" name="prodItemTurun'+i+'"/>'+
				'<input type="hidden" id="probItemTurunX'+i+'" name="probItemTurun'+i+'"/>'+
				'<input type="hidden" id="sel1UnitTurunIDX'+i+'" name="sel1UnitTurunID'+i+'">'+
				'<input type="hidden" id="sel2UnitTurunIDX'+i+'" name="sel2UnitTurunID'+i+'">'+
				'<input type="hidden" id="sel3UnitTurunIDX'+i+'" name="sel3UnitTurunID'+i+'">'+
				'<input type="hidden" id="conv1UnitTurunX'+i+'" >'+
				'<input type="hidden" id="conv2UnitTurunX'+i+'" >'+
				'<input type="hidden" id="conv3UnitTurunX'+i+'" >'+
            '</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		if ($.inArray(id, poolItemTurunID) == -1) {
			poolItemTurunID.push(id);
		};

		$("#nameItemTurunX"+i).text("("+strKode+") "+ strName);

		$("#idItemTurunX"+i).val($(selItem).find('id').text());
		$("#prodItemTurunX"+i).val(prodTitle);
		$("#probItemTurunX"+i).val(probTitle);
		$("#qty1ItemTurunX"+i).hide();
		$("#qty2ItemTurunX"+i).hide();
		$("#qty3ItemTurunX"+i).hide();

		var i2 = i;
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemTurunX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				var unitStr =[];
				var unitConv = [];
				$.map(xml.find('Unit').find('item'), function(val,i2) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();
					var intConv = $(val).find('unit_conversion').text();

					unitID.push(intID);
					unitStr.push(strUnit);
					unitConv.push(intConv);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemTurunX"+i2).show();
					$("#sel"+zz+"UnitTurunIDX"+i2).val(unitID[zz-1]);
					$("#conv"+zz+"UnitTurunX"+i2).val(unitConv[zz-1]);
					$("#qty"+zz+"ItemTurunX"+i2).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		i++;
		numberItemTurun++;
		$("#totalItemTurun").val(i);
	}
});

$("#txtNewItemRetur").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemRetur"]').val(),
			beforeSend: function() {
				$("#loadItemRetur").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemRetur").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItem = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemReturID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItem.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = $("#totalItemRetur").val();
		if (numberItemRetur == '0') {
			$("#listItemsRetur tbody").html('');
		};

		$('#listItemsRetur tbody').append(
			'<tr>'+
				'<td class="cb">'+
                    '<input type="checkbox" name="cbDeleteReturX'+i+'"/>'+
				'</td>'+
                '<td id="nameItemReturX'+i+'"></td>'+
                '<td class="qty">'+
                    '<div class="form-group">'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty1ItemReturX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                  'id="qty1ItemReturX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty2ItemReturX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty2ItemReturX'+i+'"/>'+
                        '</div>'+
                        '<div class="col-xs-4">'+
                            '<input type="text"'+
                                   'name="qty3ItemReturX'+i+'"'+
                                   'class="required number form-control input-sm"'+
                                   'id="qty3ItemReturX'+i+'"/>'+
                        '</div>'+
				    '</div>'+
				'</td>'+
				'<input type="hidden" id="idItemReturX'+i+'" name="idItemRetur'+i+'"/>'+
				'<input type="hidden" id="prodItemReturX'+i+'" name="prodItemRetur'+i+'"/>'+
				'<input type="hidden" id="probItemReturX'+i+'" name="probItemRetur'+i+'"/>'+
				'<input type="hidden" id="sel1UnitReturIDX'+i+'" name="sel1UnitReturID'+i+'">'+
				'<input type="hidden" id="sel2UnitReturIDX'+i+'" name="sel2UnitReturID'+i+'">'+
				'<input type="hidden" id="sel3UnitReturIDX'+i+'" name="sel3UnitReturID'+i+'">'+
				'<input type="hidden" id="conv1UnitReturX'+i+'" >'+
				'<input type="hidden" id="conv2UnitReturX'+i+'" >'+
				'<input type="hidden" id="conv3UnitReturX'+i+'" >'+
            '</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemReturID.push(id);

		$("#nameItemReturX"+i).text("("+strKode+") "+ strName);

		$("#idItemReturX"+i).val($(selItem).find('id').text());
		$("#prodItemReturX"+i).val(prodTitle);
		$("#probItemReturX"+i).val(probTitle);
		$("#qty1ItemReturX"+i).hide();
		$("#qty2ItemReturX"+i).hide();
		$("#qty3ItemReturX"+i).hide();

		var i2 = i;
		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemReturX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i2) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemReturX"+i2).show();
					$("#sel"+zz+"UnitReturIDX"+i2).val(unitID[zz-1]);
					$("#qty"+zz+"ItemReturX"+i2).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		i++;
		numberItemRetur++;
		$("#totalItemRetur").val(i);
	}
});

$("#listItemsSisa").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemSisa--;
	if(numberItemSisa =='0'){
		$("#listItemsSisa tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemSisaID = jQuery.grep(poolItemSisaID, function(value) {
		return value != $("#idItemSisaX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsJual").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemJual--;
	if(numberItemJual =='0'){
		$("#listItemsJual tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemJualID = jQuery.grep(poolItemJualID, function(value) {
		return value != $("#idItemJualX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsTurun").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemTurun--;
	if(numberItemTurun =='0'){
		$("#listItemsTurun tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	};

	if (this.name.indexOf('X') < 0) {
		deletedKaviTurunID.push(this.name.substring(this.name.indexOf('[')+1, this.name.length - 1));
	};
	if ($.inArray($("#idItemTurun" + this.name.substring(this.name.indexOf('[')+1, this.name.length - 1)).val()) == -1) {
		deletedItemTurunID.push($("#idItemTurun" + this.name.substring(this.name.indexOf('[')+1, this.name.length - 1)).val());
	};
	console.log('deletedKaviTurunID : ' + deletedKaviTurunID);

	poolItemTurunID = jQuery.grep(poolItemTurunID, function(value) {
		return value != $("#idItemTurunX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsRetur").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemRetur--;
	if(numberItemRetur =='0'){
		$("#listItemsRetur tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemReturID = jQuery.grep(poolItemReturID, function(value) {
		return value != $("#idItemReturX"+at).val();
	});

	$(this).closest("tr").remove();
});


function confirmSwitchDay() {
	return confirm("Anda ingin tetap melanjutkan ganti hari tanpa menyimpan perubahan ?\n\nSemua perubahan pada hari ini akan hilang.");
};

function compareSisaToTurun() {

	var isDiff = false;

	/*cek untuk barang sisa yg sudah tersimpan*/
	cntSisa = $("#totalItemSisa").val();
	for (i = 0; i < cntSisa; i++) {
		itemSisaID = poolItemSisaID[i];
		kaviSisaID = poolKaviSisaID[i];

		var itemExists = false;
		var qty1Sisa = 0;
		var qty2Sisa = 0;
		var qty3Sisa = 0;
		var conv1Sisa = 0;
		var conv2Sisa = 0;
		var conv3Sisa = 0;

		if ($("#idItemSisa" + kaviSisaID).length) {
			qty1Sisa = $("#txtItemSisa1Qty" + kaviSisaID).val();
			qty2Sisa = $("#txtItemSisa2Qty" + kaviSisaID).val();
			qty3Sisa = $("#txtItemSisa3Qty" + kaviSisaID).val();

			conv1Sisa = $("#txtItemSisa1Conv" + kaviSisaID).val();
			conv2Sisa = $("#txtItemSisa2Conv" + kaviSisaID).val();
			conv3Sisa = $("#txtItemSisa3Conv" + kaviSisaID).val();
		};

		totalQtySisa = (qty1Sisa * conv1Sisa) + (qty2Sisa * conv2Sisa) + (qty3Sisa * conv3Sisa);

		for (j = 0; j < poolItemTurunID.length; j++) {
			itemTurunID = poolItemTurunID[j];
			kaviTurunID = poolKaviTurunID[j];

			if ($.inArray(kaviTurunID, deletedKaviTurunID) == -1) {

				var qty1Turun = 0;
				var qty2Turun = 0;
				var qty3Turun = 0;
				var conv1Turun = 0;
				var conv2Turun = 0;
				var conv3Turun = 0;

				if (itemTurunID == itemSisaID) {

					itemExists = true;

					if ($("#idItemTurun" + kaviTurunID).length) {
						qty1Turun = $("#txtItemTurun1Qty" + kaviTurunID).val();
						qty2Turun = $("#txtItemTurun2Qty" + kaviTurunID).val();
						qty3Turun = $("#txtItemTurun3Qty" + kaviTurunID).val();

						conv1Turun = $("#txtItemTurun1Conv" + kaviTurunID).val();
						conv2Turun = $("#txtItemTurun2Conv" + kaviTurunID).val();
						conv3Turun = $("#txtItemTurun3Conv" + kaviTurunID).val();

						if (!conv3Turun) {
							conv3Turun = 0;
						};

						if (!qty3Turun) {
							qty3Turun = 0;
						};

						totalQtyTurun = (qty1Turun * conv1Turun) + (qty2Turun * conv2Turun) + (qty3Turun * conv3Turun);

						if (totalQtySisa != totalQtyTurun) {
							if (!isDiff) {
								isDiff = true;
							};

							if ($("#turun" + kaviTurunID).length) {
								$("#turun" + kaviTurunID).addClass('warning');
							};
						} else {
							if ($("#turun" + kaviTurunID).length) {
								$("#turun" + kaviTurunID).removeClass('warning');
							};
						}
					};



				};

			};
		};

		for (j = 0; j < $("#totalItemTurun").val(); j++) {
			var qty1Turun = 0;
			var qty2Turun = 0;
			var qty3Turun = 0;
			var conv1Turun = 0;
			var conv2Turun = 0;
			var conv3Turun = 0;

			if ($("#idItemTurunX" + j).val() == itemSisaID) {
				itemExists = true;

				if ($("#idItemTurunX" + j).length) {
					qty1Turun = $("#qty1ItemTurunX" + j).val();
					qty2Turun = $("#qty2ItemTurunX" + j).val();
					qty3Turun = $("#qty3ItemTurunX" + j).val();

					conv1Turun = $("#conv1UnitTurunX" + j).val();
					conv2Turun = $("#conv2UnitTurunX" + j).val();
					conv3Turun = $("#conv3UnitTurunX" + j).val();

					totalQtyTurun = (qty1Turun * conv1Turun) + (qty2Turun * conv2Turun) + (qty3Turun * conv3Turun);

					if (totalQtySisa != totalQtyTurun) {
						if (!isDiff) {
							isDiff = true;
						};

						if ($("#turunX" + j).length) {
							$("#turunX" + j).addClass('warning');
						};
					} else {
						if ($("#turunX" + j).length) {
							$("#turunX" + j).removeClass('warning');
						};
					}
				};


			}
		};

		if (!itemExists) {
			isDiff = true;
			if ($("#sisa" + kaviSisaID).length) {
				$("#sisa" + kaviSisaID).addClass('warning');
			};
		} else {
			if ($("#sisa" + kaviSisaID).length) {
				$("#sisa" + kaviSisaID).removeClass('warning');
			};
		}
	};

	/*cek untuk barang sisa yang baru ditambahkan*/
	cntNewSisa = $("#totalNewItemSisa").val();
	for (i = 0; i < cntNewSisa; i++) {
		itemSisaID = $("#idItemSisaX" + i).val();

		var itemExists = false;
		var qty1Sisa = 0;
		var qty2Sisa = 0;
		var qty3Sisa = 0;
		var conv1Sisa = 0;
		var conv2Sisa = 0;
		var conv3Sisa = 0;

		if ($("#idItemSisaX" + i).length) {
			qty1Sisa = $("#qty1ItemSisaX" + i).val();
			qty2Sisa = $("#qty2ItemSisaX" + i).val();
			qty3Sisa = $("#qty3ItemSisaX" + i).val();

			conv1Sisa = $("#conv1UnitSisaX" + i).val();
			conv2Sisa = $("#conv2UnitSisaX" + i).val();
			conv3Sisa = $("#conv3UnitSisaX" + i).val();
		};

		totalQtySisa = (qty1Sisa * conv1Sisa) + (qty2Sisa * conv2Sisa) + (qty3Sisa * conv3Sisa);

		for (j = 0; j < poolItemTurunID.length; j++) {
			itemTurunID = poolItemTurunID[j];
			kaviTurunID = poolKaviTurunID[j];

			if ($.inArray(kaviTurunID, deletedKaviTurunID) == -1) {

				var qty1Turun = 0;
				var qty2Turun = 0;
				var qty3Turun = 0;
				var conv1Turun = 0;
				var conv2Turun = 0;
				var conv3Turun = 0;

				if (itemTurunID == itemSisaID) {

					itemExists = true;

					if ($("#idItemTurun" + kaviTurunID).length) {
						qty1Turun = $("#txtItemTurun1Qty" + kaviTurunID).val();
						qty2Turun = $("#txtItemTurun2Qty" + kaviTurunID).val();
						qty3Turun = $("#txtItemTurun3Qty" + kaviTurunID).val();

						conv1Turun = $("#txtItemTurun1Conv" + kaviTurunID).val();
						conv2Turun = $("#txtItemTurun2Conv" + kaviTurunID).val();
						conv3Turun = $("#txtItemTurun3Conv" + kaviTurunID).val();

						if (!conv3Turun) {
							conv3Turun = 0;
						};

						if (!qty3Turun) {
							qty3Turun = 0;
						};

						totalQtyTurun = (qty1Turun * conv1Turun) + (qty2Turun * conv2Turun) + (qty3Turun * conv3Turun);

						if (totalQtySisa != totalQtyTurun) {
							if (!isDiff) {
								isDiff = true;
							};

							if ($("#turun" + kaviTurunID).length) {
								$("#turun" + kaviTurunID).addClass('warning');
							};
						} else {
							if ($("#turun" + kaviTurunID).length) {
								$("#turun" + kaviTurunID).removeClass('warning');
							};
						}
					};

				};

			};
		};

		for (j = 0; j < $("#totalItemTurun").val(); j++) {
			var qty1Turun = 0;
			var qty2Turun = 0;
			var qty3Turun = 0;
			var conv1Turun = 0;
			var conv2Turun = 0;
			var conv3Turun = 0;

			if ($("#idItemTurunX" + j).val() == itemSisaID) {
				itemExists = true;

				if ($("#idItemTurunX" + j).length) {
					qty1Turun = $("#qty1ItemTurunX" + j).val();
					qty2Turun = $("#qty2ItemTurunX" + j).val();
					qty3Turun = $("#qty3ItemTurunX" + j).val();

					conv1Turun = $("#conv1UnitTurunX" + j).val();
					conv2Turun = $("#conv2UnitTurunX" + j).val();
					conv3Turun = $("#conv3UnitTurunX" + j).val();

					totalQtyTurun = (qty1Turun * conv1Turun) + (qty2Turun * conv2Turun) + (qty3Turun * conv3Turun);
					if (totalQtySisa != totalQtyTurun) {

						if (!isDiff) {
							isDiff = true;
						};

						if ($("#turunX" + j).length) {
							$("#turunX" + j).addClass('warning');
						};
					} else {
						if ($("#turunX" + j).length) {
							$("#turunX" + j).removeClass('warning');
						};
					}
				};


			}
		};

		if (!itemExists) {
			isDiff = true;

			if ($("#sisaX" + i).length) {
				$("#sisaX" + i).addClass('warning');
			};
		} else {
			if ($("#sisaX" + i).length) {
				$("#sisaX" + i).removeClass('warning');
			};
		}
	};


	if (isDiff) {
		return false;
		alert('Muatan sisa tidak sesuai dengan muatan turun. Periksa kembali baris yang berwarna kuning.');
	} else {
		return true;
	}
};

$("#frmChangeKanvas").submit(function() {

	if (intDay == 6) {

		if (compareSisaToTurun()) {
			return true;
		} else {
			return false;
		};
	} else {
		return true;
	}
});

});</script>
