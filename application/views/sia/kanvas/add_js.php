<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {

var primaryWarehouseName = '<?=$this->config->item('sia_primary_warehouse_name')?>';
var poolItemSisaID = [];
var totalItemSisa=0;
var numberItemSisa=0;
var poolItemJualID = [];
var totalItemJual=0;
var numberItemJual=0;
var poolItemTurunID = [];
var totalItemTurun=0;
var numberItemTurun=0;
var poolItemReturID = [];
var totalItemRetur=0;
var numberItemRetur=0;

$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddKanvas").validate({
	rules: {},
	message: {},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.data("title", "").removeClass("error").tooltip("destroy");
			$element.closest('.form-group').removeClass('has-error');
		});
		$.each(errorList, function (index, error) {
			var $element = $(error.element);
			$element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
			$element.closest('.form-group').addClass('has-error');
		});
	},
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddKanvas").submit(function() {
	if ($("#warehouseName").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectwarehouse')?>");
		return false;
	}

	if ($("#salesmanName").val() == '') {
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectsalesman')?>");
		return false;
	}
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
	
	return true;
	
});

$("#warehouseName").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete', NULL, FALSE)?>/" + $("#warehouseName").val(),            
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);                
				$arrWarehouse = xml;
				var display = [];
				$.map(xml.find('Warehouse').find('item'),function(val){
					var name = $(val).find('ware_name').text();
					var intID = $(val).find('id').text();
					display.push({label: name, value: name, id: intID});
				});
				if(display.length == 1) {
					setTimeout(function() {
	                    $("#warehouseName").data('ui-autocomplete')._trigger('select', 'autocompleteselect', {item: display[0]});
	                    $('#warehouseName').autocomplete('close');
	                }, 100);
                }
				response(display);
			}
		});
	},
	select: function(event,ui) {
		var selWarehouse = $.grep($arrWarehouse.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		$("#warehouseID").val($(selWarehouse).find('id').text());
	}
});
setTimeout(function() {
    $("#warehouseName").val(primaryWarehouseName);
    $("#warehouseName").autocomplete('search');
},500);

$("#salesmanName").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getSalesmanKanvasAutoComplete', NULL, FALSE)?>/" + $("#salesmanName").val(),
			beforeSend: function() {
				$("#loadSalesman").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadSalesman").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSalesman = xml;
				var display = [];
				$.map(xml.find('Salesman').find('item'), function(val) {
					var name = $(val).find('sals_name').text();
                    var code = $(val).find('sals_code').text();
					var intID = $(val).find('id').text();
					display.push({label:"("+code+") "+ name, value:"("+code+") "+ name, id: intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selSalesman = $.grep($arrSalesman.find('Salesman').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		$("#salesmanID").val($(selSalesman).find('id').text());
		$('input[name="salesmanCode"]').val($(selSalesman).find('sals_code').text());
	}
})

$("#txtNewItemSisa").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemSisa"]').val(),
			beforeSend: function() {
				$("#loadItemSisa").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemSisa").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItemSisa = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemSisaID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItemSisa.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = totalItemSisa;
		if (numberItemSisa == '0') {
			$("#listItemsSisa tbody").html('');
		};

		$('#listItemsSisa tbody').append(
			'<tr>'+
				'<td class="cb">'+
				'<input type="checkbox" name="cbDeleteSisaX'+i+'"/>'+
				'</td>'+
				'<td id="nameItemSisaX'+i+'"></td>'+
				'<td class="qty">'+
				'<div class="form-group">'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty1ItemSisaX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty1ItemSisaX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty2ItemSisaX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty2ItemSisaX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty3ItemSisaX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty3ItemSisaX'+i+'"/>'+
				'</div>'+
				'</div>'+
				'</td>'+

				'<input type="hidden" id="idItemSisaX'+i+'" name="idItemSisa'+i+'"/>'+
				'<input type="hidden" id="prodItemSisaX'+i+'" name="prodItemSisa'+i+'"/>'+
				'<input type="hidden" id="probItemSisaX'+i+'" name="probItemSisa'+i+'"/>'+
				'<input type="hidden" id="sel1UnitSisaIDX'+i+'" name="sel1UnitSisaID'+i+'">'+
				'<input type="hidden" id="sel2UnitSisaIDX'+i+'" name="sel2UnitSisaID'+i+'">'+
				'<input type="hidden" id="sel3UnitSisaIDX'+i+'" name="sel3UnitSisaID'+i+'">'+
				'<input type="hidden" id="conv1UnitSisaX'+i+'" >'+
				'<input type="hidden" id="conv2UnitSisaX'+i+'" >'+
				'<input type="hidden" id="conv3UnitSisaX'+i+'" >'+
				'</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();

		$("#nameItemSisaX"+i).text("("+strKode+") "+ strName);


		$("#idItemSisaX"+i).val($(selItem).find('id').text());
		$("#prodItemSisaX"+i).val(prodTitle);
		$("#probItemSisaX"+i).val(probTitle);
		$("#qty1ItemSisaX"+i).hide();
		$("#qty2ItemSisaX"+i).hide();
		$("#qty3ItemSisaX"+i).hide();

		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemSisaX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemSisaX"+i).show();
					$("#sel"+zz+"UnitSisaIDX"+i).val(unitID[zz-1]);
					$("#qty"+zz+"ItemSisaX"+i).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		totalItemSisa++;
		numberItemSisa++;
		$("#totalItemSisa").val(totalItemSisa);
	}
});

$("#txtNewItemJual").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemJual"]').val(),
			beforeSend: function() {
				$("#loadItemJual").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemJual").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItemJual = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemJualID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItemJual.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = totalItemJual;
		var qty1HiddenClass = '';
        var qty2HiddenClass = '';
        var prod_conv1 = $(selItem).find('prod_conv1').text();
        var prod_conv2 = $(selItem).find('prod_conv2').text();
        if(parseInt(prod_conv1) <= 1) qty1HiddenClass = ' hidden';
        if(parseInt(prod_conv2) <= 1) qty2HiddenClass = ' hidden';
		if (numberItemJual == '0') {
			$("#listItemsJual tbody").html('');
		};

		$('#listItemsJual tbody').append(
			'<tr>'+
				'<td class="cb">'+
				'<input type="checkbox" name="cbDeleteJualX'+i+'"/>'+
				'</td>'+
				'<td id="nameItemJualX'+i+'"></td>'+
				'<td class="qty">'+
				'<div class="form-group">'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty1ItemJualX'+i+'"'+
				'class="required number form-control input-sm' + qty1HiddenClass + '"'+
				'id="qty1ItemJualX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty2ItemJualX'+i+'"'+
				'class="required number form-control input-sm' + qty2HiddenClass + '"'+
				'id="qty2ItemJualX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty3ItemJualX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty3ItemJualX'+i+'"/>'+
				'</div>'+
				'</div>'+
				'</td>'+

				'<input type="hidden" id="idItemJualX'+i+'" name="idItemJual'+i+'"/>'+
				'<input type="hidden" id="prodItemJualX'+i+'" name="prodItemJual'+i+'"/>'+
				'<input type="hidden" id="probItemJualX'+i+'" name="probItemJual'+i+'"/>'+
				'<input type="hidden" id="sel1UnitJualIDX'+i+'" name="sel1UnitJualID'+i+'">'+
				'<input type="hidden" id="sel2UnitJualIDX'+i+'" name="sel2UnitJualID'+i+'">'+
				'<input type="hidden" id="sel3UnitJualIDX'+i+'" name="sel3UnitJualID'+i+'">'+
				'<input type="hidden" id="conv1UnitJualX'+i+'" >'+
				'<input type="hidden" id="conv2UnitJualX'+i+'" >'+
				'<input type="hidden" id="conv3UnitJualX'+i+'" >'+
				'</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemJualID.push(id);

		$("#nameItemJualX"+i).text("("+strKode+") "+ strName);

		$("#idItemJualX"+i).val($(selItem).find('id').text());
		$("#prodItemJualX"+i).val(prodTitle);
		$("#probItemJualX"+i).val(probTitle);
		$("#qty1ItemJualX"+i).hide();
		$("#qty2ItemJualX"+i).hide();
		$("#qty3ItemJualX"+i).hide();

		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemJualX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				/*var unitTitle = [];
				 var unitConv = [];*/
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemJualX"+i).show();
					$("#sel"+zz+"UnitJualIDX"+i).val(unitID[zz-1]);
					$("#qty"+zz+"ItemJualX"+i).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		totalItemJual++;
		numberItemJual++;
		$("#totalItemJual").val(totalItemJual);
	}
});

$("#txtNewItemTurun").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemTurun"]').val(),
			beforeSend: function() {
				$("#loadItemTurun").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemTurun").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItemTurun = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemTurunID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItemTurun.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = totalItemTurun;
		if (numberItemTurun == '0') {
			$("#listItemsTurun tbody").html('');
		};

		$('#listItemsTurun tbody').append(
			'<tr>'+
				'<td class="cb">'+
				'<input type="checkbox" name="cbDeleteTurunX'+i+'"/>'+
				'</td>'+
				'<td id="nameItemTurunX'+i+'"></td>'+
				'<td class="qty">'+
				'<div class="form-group">'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty1ItemTurunX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty1ItemTurunX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty2ItemTurunX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty2ItemTurunX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty3ItemTurunX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty3ItemTurunX'+i+'"/>'+
				'</div>'+
				'</div>'+
				'</td>'+

				'<input type="hidden" id="idItemTurunX'+i+'" name="idItemTurun'+i+'"/>'+
				'<input type="hidden" id="prodItemTurunX'+i+'" name="prodItemTurun'+i+'"/>'+
				'<input type="hidden" id="probItemTurunX'+i+'" name="probItemTurun'+i+'"/>'+
				'<input type="hidden" id="sel1UnitTurunIDX'+i+'" name="sel1UnitTurunID'+i+'">'+
				'<input type="hidden" id="sel2UnitTurunIDX'+i+'" name="sel2UnitTurunID'+i+'">'+
				'<input type="hidden" id="sel3UnitTurunIDX'+i+'" name="sel3UnitTurunID'+i+'">'+
				'<input type="hidden" id="conv1UnitTurunX'+i+'" >'+
				'<input type="hidden" id="conv2UnitTurunX'+i+'" >'+
				'<input type="hidden" id="conv3UnitTurunX'+i+'" >'+
				'</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemTurunID.push(id);

		$("#nameItemTurunX"+i).text("("+strKode+") "+ strName);

		$("#idItemTurunX"+i).val($(selItem).find('id').text());
		$("#prodItemTurunX"+i).val(prodTitle);
		$("#probItemTurunX"+i).val(probTitle);
		$("#qty1ItemTurunX"+i).hide();
		$("#qty2ItemTurunX"+i).hide();
		$("#qty3ItemTurunX"+i).hide();

		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemTurunX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				/*var unitTitle = [];
				 var unitConv = [];*/
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemTurunX"+i).show();
					$("#sel"+zz+"UnitTurunIDX"+i).val(unitID[zz-1]);
					$("#qty"+zz+"ItemTurunX"+i).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		totalItemTurun++;
		numberItemTurun++;
		$("#totalItemTurun").val(totalItemTurun);
	}
});

$("#txtNewItemRetur").autocomplete({
	minLength: 3,
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: "<?=site_url('kanvas_ajax/getProductAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewItemRetur"]').val(),
			beforeSend: function() {
				$("#loadItemRetur").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItemRetur").html('');
			},
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrItemRetur = xml;
				var display = [];
				$.map(xml.find('Product').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strName=$(val).find('strName').text();
                    var strKode=$(val).find('prod_code').text();
					if ($.inArray(intID,poolItemReturID) > -1) {

					} else {
						display.push({label:"("+strKode+") "+ strName, value: '',id:intID});
					}
				});
				response(display);
			}
		})
	},
	select: function(event, ui) {
		var selItem = $.grep($arrItemRetur.find('Product').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});

		var i = totalItemRetur;
		if (numberItemRetur == '0') {
			$("#listItemsRetur tbody").html('');
		};

		$('#listItemsRetur tbody').append(
			'<tr>'+
				'<td class="cb">'+
				'<input type="checkbox" name="cbDeleteReturX'+i+'"/>'+
				'</td>'+
				'<td id="nameItemReturX'+i+'"></td>'+
				'<td class="qty">'+
				'<div class="form-group">'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty1ItemReturX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty1ItemReturX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty2ItemReturX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty2ItemReturX'+i+'"/>'+
				'</div>'+
				'<div class="col-xs-4">'+
				'<input type="text"'+
				'name="qty3ItemReturX'+i+'"'+
				'class="required number form-control input-sm"'+
				'id="qty3ItemReturX'+i+'"/>'+
				'</div>'+
				'</div>'+
				'</td>'+

				'<input type="hidden" id="idItemReturX'+i+'" name="idItemRetur'+i+'"/>'+
				'<input type="hidden" id="prodItemReturX'+i+'" name="prodItemRetur'+i+'"/>'+
				'<input type="hidden" id="probItemReturX'+i+'" name="probItemRetur'+i+'"/>'+
				'<input type="hidden" id="sel1UnitReturIDX'+i+'" name="sel1UnitReturID'+i+'">'+
				'<input type="hidden" id="sel2UnitReturIDX'+i+'" name="sel2UnitReturID'+i+'">'+
				'<input type="hidden" id="sel3UnitReturIDX'+i+'" name="sel3UnitReturID'+i+'">'+
				'<input type="hidden" id="conv1UnitReturX'+i+'" >'+
				'<input type="hidden" id="conv2UnitReturX'+i+'" >'+
				'<input type="hidden" id="conv3UnitReturX'+i+'" >'+
				'</tr>'
		);
		var prodTitle = $(selItem).find('prod_title').text();
		var probTitle = $(selItem).find('prob_title').text();
		var strName = $(selItem).find('strName').text();
        var strKode = $(selItem).find('prod_code').text();
		var id = $(selItem).find('id').text();
		poolItemReturID.push(id);

		$("#nameItemReturX"+i).text("("+strKode+") "+ strName);

		$("#idItemReturX"+i).val($(selItem).find('id').text());
		$("#prodItemReturX"+i).val(prodTitle);
		$("#probItemReturX"+i).val(probTitle);
		$("#qty1ItemReturX"+i).hide();
		$("#qty2ItemReturX"+i).hide();
		$("#qty3ItemReturX"+i).hide();

		$.ajax({
			url: "<?=site_url('inventory_ajax/getUnitsByProductID', NULL, FALSE)?>/" + $("#idItemReturX"+i).val()+"/0",
			success: function(data) {
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);

				var unitID = [];
				/*var unitTitle = [];
				 var unitConv = [];*/
				var unitStr =[];
				$.map(xml.find('Unit').find('item'), function(val,i) {
					var intID = $(val).find('id').text();
					var strUnit = $(val).find('unit_title').text();

					unitID.push(intID);
					unitStr.push(strUnit);
				});

				for(var zz=1;zz<=unitID.length;zz++){
					$("#qty"+zz+"ItemReturX"+i).show();
					$("#sel"+zz+"UnitReturIDX"+i).val(unitID[zz-1]);
					$("#qty"+zz+"ItemReturX"+i).attr("placeholder",unitStr[zz-1]);
				}
			}
		});

		totalItemRetur++;
		numberItemRetur++;
		$("#totalItemRetur").val(totalItemRetur);
	}
});


$("#listItemsSisa").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemSisa--;
	if(numberItemSisa =='0'){
		$("#listItemsSisa tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemSisaID = jQuery.grep(poolItemSisaID, function(value) {
		return value != $("#idItemSisaX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsJual").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemJual--;
	if(numberItemJual =='0'){
		$("#listItemsJual tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemJualID = jQuery.grep(poolItemJualID, function(value) {
		return value != $("#idItemJualX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsTurun").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemTurun--;
	if(numberItemTurun =='0'){
		$("#listItemsTurun tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemTurunID = jQuery.grep(poolItemTurunID, function(value) {
		return value != $("#idItemTurunX"+at).val();
	});

	$(this).closest("tr").remove();
});

$("#listItemsRetur").on("click","input[type='checkbox'][name*='cbDelete']",function() {
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);

	numberItemRetur--;
	if(numberItemRetur =='0'){
		$("#listItemsRetur tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}

	poolItemReturID = jQuery.grep(poolItemReturID, function(value) {
		return value != $("#idItemReturX"+at).val();
	});

	$(this).closest("tr").remove();
});

});</script>
