<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-dropbox"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvas'), 'link' => site_url('kanvas/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

$bolBtnEdit = true;

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-6 col-xs-offset-6">
    <div class="form-group">
        <label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-lastkanvas')?></label>
        <select name="selJumpTo" class="form-control">
            <option value="">- Jump To -</option><?php
            if(!empty($arrKanvasList)) foreach($arrKanvasList as $e):
                $strListTitle = ' title="'.$e['sals_address'].'"'; ?>
            <option value="<?=site_url('kanvas/view/'.$e['id'], NULL, FALSE)?>">[<?=formatDate2($e['kanv_date'],'d/m/Y')?>] <?=$e['sals_name']?></option><?php
            endforeach; ?>
        </select>
    </div>
</div>

<div class="col-xs-12">
    <form name="frmChangeKanvas" id="frmChangeKanvas" method="post" action="<?=site_url('kanvas/view/'.$intKanvasID, NULL, FALSE)?>" class="frmShop">
        <!--header1-->
        <div class="row">
            <!--kode & tanggal buat-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-4">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?>
                        </div>
                        <div class="col-xs-8"><?=$arrKanvasData['kanv_code']?></div>
                        <p class="spacer">&nbsp;</p>

                        <div class="col-xs-4">
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?>
                        </div>
                        <div class="col-xs-8">
                            <?=formatDate2($arrKanvasData['kanv_date'],'d F Y')?>
                        </div>
                        <p class="spacer">&nbsp;</p>

                        <div class="col-xs-4">
                            Muat
                        </div>
                        <div class="col-xs-8">
                            <?=formatDate2($arrKanvasData['kanv_loading_date'],'d F Y')?>
                        </div>
                        <p class="spacer">&nbsp;</p>

                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon">Turun</label>
                                <input type="text"
                                       id="txtDischargeDate"
                                       name="txtDischargeDate"
                                       class="form-control jwDateTime"
                                       value="<?php if($arrKanvasData['kanv_discharge_date'] == 0) : echo ''; else : echo formatDate2($arrKanvasData['kanv_discharge_date'],'Y/m/d'); endif;?>"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--kode & tanggal buat-->

            <!--data gudang-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-thumb-tack"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                        <div class="col-xs-8"><?=$arrKanvasData['ware_name']?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div>
            </div> <!--data gudang-->

            <!--data salesman-->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmandata')?></h3></div>
                    <div class="panel-body">
                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
                        <div class="col-xs-8"><?=$arrKanvasData['sals_code']?></div>
                        <p class="spacer">&nbsp;</p>

                        <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></div>
                        <div class="col-xs-8"><?=$arrKanvasData['sals_name']?></div>
                        <p class="spacer">&nbsp;</p>
                    </div>
                </div>
            </div> <!--data salesman-->
        </div> <!--header1-->

        <!--header2-->
        <div class="row">
            <!--hari muat-->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-automobile"></i> Muatan pada hari</h3>
                            </div>
                            <div class="panel-body" id="listDay">
                                <input type="radio" name="intHari" id="radSenin" value="1" <?php if($arrKanvasData['kanv_day']=='1'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radSenin">Senin&nbsp;</label>
                                <input type="radio" name="intHari" id="radSelasa" value="2" <?php if($arrKanvasData['kanv_day']=='2'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radSelasa">Selasa&nbsp;</label>
                                <input type="radio" name="intHari" id="radRabu" value="3" <?php if($arrKanvasData['kanv_day']=='3'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radRabu">Rabu&nbsp;</label>
                                <input type="radio" name="intHari" id="radKamis" value="4" <?php if($arrKanvasData['kanv_day']=='4'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radKamis">Kamis&nbsp;</label>
                                <input type="radio" name="intHari" id="radJumat" value="5" <?php if($arrKanvasData['kanv_day']=='5'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radJumat">Jumat&nbsp;</label>
                                <input type="radio" name="intHari" id="radSabtu" value="6" <?php if($arrKanvasData['kanv_day']=='6'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radSabtu">Sabtu&nbsp;</label>
                                <input type="radio" name="intHari" id="radSaldo" value="0" <?php if($arrKanvasData['kanv_day']=='0'): ?>checked="true"<?php ; endif;?>/>
                                <label for="radSaldo">Saldo</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--hari muat-->
        </div> <!--header2-->


        <div class="row">
            <!--daftar item sisa-->
            <div class="col-xs-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-sisa')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div id="addBarangSisa" class="form-group addBarang">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemSisa"
                                       name="txtNewItemSisa"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemSisa">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsSisa">
                                <thead>
                                <tr>
                                    <th class="cb"><i class="fa fa-trash-o"></i></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <!--
                                    Isi barang ke tabel.
                                    Item yang ditampilkan hanya yang sesuai dengan Muatan hari dan Tipe muatan (Sisa, Jual, Turun, Retur)
                                -->
                                <?php
                                $i = 0;
                                $intTotalItemSisa = 0;
                                $idItemSisaAwal='';
                                $idKaviSisaAwal='';
                                if(!empty($arrKanvasItemSisa)):
                                    foreach($arrKanvasItemSisa as $e):
                                        $idItemSisaAwal=$idItemSisaAwal.$e['product_id'].',';
                                        $idKaviSisaAwal=$idKaviSisaAwal.$e['id'].',';
                                        $intTotalItemSisa++; ?>
                                        <tr id="sisa<?=$e['id'] ?>">

                                        <td class="cb"><?php if ($e['kavi_day'] == $arrKanvasData['kanv_day']):?><input type="checkbox" name="cbDeleteSisaAwal[<?=$e['id']?>]"/><?php endif; ?></td>
                                        <td><?="(".$e['prod_code'].") ".$e['strName']?></td>

                                        <td class="qty"><?php
                                            if ($e['kavi_day'] == $arrKanvasData['kanv_day'])://($bolBtnEdit): ?>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemSisa1Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity1']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit1']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemSisa1Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit1'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit1'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemSisa2Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity2']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit2']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemSisa2Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit2'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit2'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemSisa3Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity3']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit3']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemSisa3Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit3'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit3'])?>" />
												</div>
                                                <input type="hidden" name="txtItemSisa1Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv1']?>" id="txtItemSisa1Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemSisa2Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv2']?>" id="txtItemSisa2Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemSisa3Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv3']?>" id="txtItemSisa3Conv<?=$e['id']?>" />
                                                <input type="hidden" name="idItemSisa<?=$e['id']?>" value="<?=$e['product_id']?>" id="idItemSisa<?=$e['id']?>" />                                                
                                            </div><?php

                                            else: echo
                                                $e['kavi_quantity1'].' '.formatUnitName($e['kavi_unit1']).' + '.
                                                $e['kavi_quantity2'].' '.formatUnitName($e['kavi_unit2']).' + '.
                                                $e['kavi_quantity3'].' '.formatUnitName($e['kavi_unit3']);
                                            endif; ?>
                                                <input type="hidden" name="idKaviSisaID[<?=$e['id']?>]" value="<?=$e['id']?>" />                                                
                                        </td>
                                        </tr><?php

                                        $i++;
                                    endforeach;

                                    $idItemSisaAwal = $idItemSisaAwal.'0';
                                    $idKaviSisaAwal = $idKaviSisaAwal.'0';
                                else: ?>
                                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!--daftar item sisa -->
            </div>

            <!--daftar item jual-->
            <div class="col-xs-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-jual')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsJual">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-trash-o"></i></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <!--
                                    Isi barang ke tabel.
                                    Item yang ditampilkan hanya yang sesuai dengan Muatan hari dan Tipe muatan (Sisa, Jual, Turun, Retur)
                                -->
                                <?php
                                $i = 0;
                                $intTotalItemJual = 0;
                                $idItemJualAwal = '';
                                if(!empty($arrKanvasItemJual)):
                                    foreach($arrKanvasItemJual as $e):
                                        $idItemJualAwal=$idItemJualAwal.$e['kavi_product_id'].',';
                                        $intTotalItemJual++; ?>
                                        <tr>

                                        <td class="cb"><!--<input type="checkbox" name="cbDeleteJualAwal[<?=$e['id']?>]" />--></td>
                                        <td><?="(".$e['prod_code'].") ".$e['strName']?></td>

                                        <td class="qty"><?php
                                            if(false/*$bolBtnEdit*/): //if yes ?>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemJual1Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity1']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit1']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemJual1Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit1'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit1'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemJual2Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity2']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit2']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemJual2Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit2'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit2'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemJual3Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity3']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit3']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemJual3Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit3'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit3'])?>" />
												</div>
                                                <input type="hidden" name="txtItemJual1Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv1']?>" id="txtItemJual1Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemJual2Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv2']?>" id="txtItemJual2Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemJual3Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv3']?>" id="txtItemJual3Conv<?=$e['id']?>" />
                                                <input type="hidden" name="idItemJual<?=$e['id']?>" value="<?=$e['kavi_product_id']?>" id="idItemJual<?=$e['id']?>" />
                                            </div><?php

                                            else: echo
                                                $e['kavi_quantity1'].' '.formatUnitName($e['kavi_unit1']).' + '.
                                                $e['kavi_quantity2'].' '.formatUnitName($e['kavi_unit2']).' + '.
                                                $e['kavi_quantity3'].' '.formatUnitName($e['kavi_unit3']);
                                            endif; ?>
                                                <input type="hidden" name="idKaviJualID[<?=$e['id']?>]" value="<?=$e['id']?>" />
                                        </td>
                                        </tr><?php

                                        $i++;
                                    endforeach;

                                    $idItemJualAwal = $idItemJualAwal.'0';
                                else: ?>
                                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
            
                        <div>
                            <label>Penjualan diinputkan melalui menu Penjualan.</label>
                        </div>
                        <!--input barang-->                        
                        <!--<div id="addBarangJual" class="form-group addBarang">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemJual"
                                       name="txtNewItemJual"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemJual">
                                </label>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div> <!--daftar item jual-->
        </div>
        <div class="row">
            <!--daftar item turun-->
            <div class="col-xs-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-turun')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div id="addBarangTurun" class="form-group addBarang">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemTurun"
                                       name="txtNewItemTurun"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemTurun">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsTurun">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-trash-o"></i></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <!--
                                    Isi barang ke tabel.
                                    Item yang ditampilkan hanya yang sesuai dengan Muatan hari dan Tipe muatan (Sisa, Jual, Turun, Retur)
                                -->
                                <?php
                                $i = 0;
                                $intTotalItemTurun = 0;
                                $idItemTurunAwal='';
                                $idKaviTurunAwal='';
                                if(!empty($arrKanvasItemTurun)):
                                    foreach($arrKanvasItemTurun as $e):
                                        $idItemTurunAwal=$idItemTurunAwal.$e['kavi_product_id'].',';
                                        $idKaviTurunAwal=$idKaviTurunAwal.$e['id'].',';
                                        $intTotalItemTurun++; ?>
                                        <tr id="turun<?=$e['id'] ?>">

                                        <td class="cb"><input type="checkbox" name="cbDeleteTurunAwal[<?=$e['id']?>]" /></td>
                                        <td><?="(".$e['prod_code'].") ".$e['strName']?></td>

                                        <td class="qty"><?php
                                            if($bolBtnEdit): //if yes ?>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemTurun1Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity1']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit1']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemTurun1Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit1'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit1'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemTurun2Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity2']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit2']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemTurun2Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit2'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit2'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemTurun3Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity3']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit3']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemTurun3Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit3'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit3'])?>" />
                                                </div>
                                                <input type="hidden" name="txtItemTurun1Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv1']?>" id="txtItemTurun1Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemTurun2Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv2']?>" id="txtItemTurun2Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemTurun3Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv3']?>" id="txtItemTurun3Conv<?=$e['id']?>" />
                                                <input type="hidden" name="idItemTurun<?=$e['id']?>" value="<?=$e['kavi_product_id']?>" id="idItemTurun<?=$e['id']?>" />
                                            </div><?php

                                            else: echo
                                                $e['kavi_quantity1'].' '.formatUnitName($e['kavi_unit1']).' + '.
                                                $e['kavi_quantity2'].' '.formatUnitName($e['kavi_unit2']).' + '.
                                                $e['kavi_quantity3'].' '.formatUnitName($e['kavi_unit3']);
                                            endif; ?>
                                                <input type="hidden" name="idKaviTurunID[<?=$e['id']?>]" value="<?=$e['id']?>" />
                                        </td>
                                        </tr><?php

                                        $i++;
                                    endforeach;

                                    $idTtemTurunAwal = $idItemTurunAwal.'0';
                                    $idKaviTurunAwal = $idKaviTurunAwal.'0';
                                else: ?>
                                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item turun-->

            <!--daftar item retur-->
            <div class="col-xs-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasitems-retur')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <!--input barang-->
                        <div id="addBarangRetur" class="form-group">
                            <div class="input-group addNew">
                                <input type="text"
                                       id="txtNewItemRetur"
                                       name="txtNewItemRetur"
                                       placeholder="Tambah Barang"
                                       class="form-control"
                                    />
                                <label class="input-group-addon"
                                       id="loadItemRetur">
                                </label>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-condesed table-hover" id="listItemsRetur">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-trash-o"></i></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-item')?></th>
                                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <!--
                                    Isi barang ke tabel.
                                    Item yang ditampilkan hanya yang sesuai dengan Muatan hari dan Tipe muatan (Sisa, Jual, Turun, Retur)
                                -->
                                <?php
                                $i = 0;
                                $intTotalItemRetur = 0;
                                $idItemReturAwal='';
                                if(!empty($arrKanvasItemRetur)):
                                    foreach($arrKanvasItemRetur as $e):
                                        $idItemReturAwal=$idItemReturAwal.$e['kavi_product_id'].',';
                                        $intTotalItemRetur++; ?>
                                        <tr>

                                        <td class="cb"><input type="checkbox" name="cbDeleteReturAwal[<?=$e['id']?>]" /></td>
                                        <td><?="(".$e['prod_code'].") ".$e['strName']?></td>

                                        <td class="qty"><?php
                                            if($bolBtnEdit): //if yes ?>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemRetur1Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity1']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit1']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemRetur1Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit1'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit1'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemRetur2Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity2']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit2']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemRetur2Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit2'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit2'])?>" />
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text"
                                                           name="txtItemRetur3Qty[<?=$e['id']?>]"
                                                           value="<?=$e['kavi_quantity3']?>"
                                                           class="required number form-control input-sm <?php if($e['kavi_unit3']==0): echo 'hidden'; endif; ?>"
                                                           id="txtItemRetur3Qty<?=$e['id']?>"
                                                           placeholder="<?=formatUnitName($e['kavi_unit3'])?>"
                                                           title="<?=formatUnitName($e['kavi_unit3'])?>" />
                                                </div>
                                                <input type="hidden" name="txtItemRetur1Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv1']?>" id="txtItemRetur1Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemRetur2Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv2']?>" id="txtItemRetur2Conv<?=$e['id']?>" />
                                                <input type="hidden" name="txtItemRetur3Conv[<?=$e['id']?>]" value="<?=$e['kavi_conv3']?>" id="txtItemRetur3Conv<?=$e['id']?>" />
                                                <input type="hidden" name="idItemRetur<?=$e['id']?>" value="<?=$e['kavi_product_id']?>" id="idItem<?=$e['id']?>" />
                                            </div><?php

                                            else: echo
                                                $e['kavi_quantity1'].' '.formatUnitName($e['kavi_unit1']).' + '.
                                                $e['kavi_quantity2'].' '.formatUnitName($e['kavi_unit2']).' + '.
                                                $e['kavi_quantity3'].' '.formatUnitName($e['kavi_unit3']);
                                            endif; ?>
                                                <input type="hidden" name="idKaviReturID[<?=$e['id']?>]" value="<?=$e['id']?>" />
                                        </td>
                                        </tr><?php

                                        $i++;
                                    endforeach;

                                    $idItemReturAwal = $idItemReturAwal.'0';
                                else: ?>
                                    <tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!--daftar item retur-->
        </div>

        <!--footer-->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-info-circle"></i>
                            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <textarea name="txaDescription" class="form-control" rows="7"><?php if(!empty($arrKanvasData['kanv_description'])) echo $arrKanvasData['kanv_description']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--footer-->


<?php
    $rawStatus = 0;
    
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php');
?>  

<input type="hidden" id="kanvasID" name="kanvasID" value="<?=$intKanvasID?>"/>
<input type="hidden" id="warehouseID" name="warehouseID" value="<?=$arrKanvasData['ware_id']?>"/>
<input type="hidden" id="salesmanID" name="salesmanID" value="<?=$arrKanvasData['sals_id']?>"/>
<input type="hidden" id="idItemSisaAwal" name="idItemSisaAwal" value="<?=$idItemSisaAwal?>"/>
<input type="hidden" id="idKaviSisaAwal" name="idKaviSisaAwal" value="<?=$idKaviSisaAwal?>"/>
<input type="hidden" id="idItemJualAwal" name="idItemJualAwal" value="<?=$idItemJualAwal?>"/>
<input type="hidden" id="idItemTurunAwal" name="idItemTurunAwal" value="<?=$idItemTurunAwal?>"/>
<input type="hidden" id="idKaviTurunAwal" name="idKaviTurunAwal" value="<?=$idKaviTurunAwal?>"/>
<input type="hidden" id="idItemReturAwal" name="idItemReturAwal" value="<?=$idItemReturAwal?>"/>
<input type="hidden" id="totalItemSisa" name="totalItemSisa" value="<?=$intTotalItemSisa?>"/>
<input type="hidden" id="totalNewItemSisa" name="totalNewItemSisa" value="0"/>
<input type="hidden" id="totalItemJual" name="totalItemJual" value="<?=$intTotalItemJual?>"/>
<input type="hidden" id="totalItemTurun" name="totalItemTurun" value="<?=$intTotalItemTurun?>"/>
<input type="hidden" id="totalItemRetur" name="totalItemRetur" value="<?=$intTotalItemRetur?>"/>

    </form>
</div>

<!--<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Anda ingin tetap melanjutkan ganti hari tanpa menyimpan perubahan ?<br>Semua perubahan pada hari ini akan hilang.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <a href="#" class="btn btn-primary">OK</a>
            </div>
        </div>
    </div>

</div>-->