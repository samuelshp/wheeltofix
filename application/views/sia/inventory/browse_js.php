<script>$(document).ready(function() {
$('select[name=filterType]').change(function() {
    if($(this).find('option:selected').val() != 'search') {
        $('input[name=txtSearchValue]').attr('disabled', true);
    } else {
        $('input[name=txtSearchValue]').attr('disabled', false);
    }
});<?php

if($bolCalibrate): ?>
if($('.pagination .next').length) {
	window.location.href = $('.pagination .next a').attr('href');
} else {
	window.location.href = '<?=site_url('inventory/browse?done_calibrate=true')?>';
}<?php
endif; ?>

});</script>