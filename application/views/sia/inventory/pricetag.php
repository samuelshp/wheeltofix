<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="Masukkan jumlah tag rak yang ingin anda print dan klik tombol print" id="InStRuCtIoN"></i>';

$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => site_url('inventory/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-list"></i> Print Tag'),
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12" id="bodyContainer">
	<form name="frmTable" id="frmTable" method="post" action="<?=site_url('inventory/print_tag/'.$intProductID, NULL, FALSE)?>">
		<div class="col-xs-12 col-md-4 col-md-offset-4">
			<div class="priceTag"><table cellspacing="0px" border="0px">
				<tr>
					<td class="productName"><?=formatProductName('',$arrProduct['prod_title'])?></td>
				</tr>
				<tr>
					<td class="barcodeNumber"><?=generateBarcodeNumber('',$arrProduct['id'])?></td>
				</tr>
				<tr>
					<td class="price"><span><?=setPrice($arrProduct['prod_price'],$arrProduct['prod_currency'])?></span></td>
				</tr>
				<tr>
					<td class="storeName"><?=$this->config->item('jw_website_name')?></td>
				</tr>
			</table></div>
		</div>
		<div class="col-xs-12 col-md-4 col-md-offset-4">
			<div class="form-group">
				<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-printhowmany')?></label>
				<div class="input-group">
					<input type="text" name="txtPrintNumber" value="1" class="number form-control" />
					<span class="input-group-btn">
						<input type="submit" name="smtPrintPriceTag" id="smtPrintPriceTag" value="Print" class="btn btn-primary btn-sm" />
					</span>
				</div>
			</div>
		</div>
	</form>
</div>