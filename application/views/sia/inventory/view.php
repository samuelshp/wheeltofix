<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => site_url('inventory/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('inventory/browse', NULL, FALSE)?>" class="form-inline pull-right">
    <div class="input-group" style="max-width:250px;">
	   <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-searchbytitle')?>" />
	   <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>
    <h2><?=$arrWarehouse['ware_name']?></h2>
    <p class="spacer">&nbsp;</p>
    <h4><?=$arrProduct['prod_title']?></h4>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive frmShop"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
        <th>Type</th>
        <th class="tdTitle">Quantity</th>
    </tr></thead>
    <tbody>
    <?php
// Display data in the table
if(!empty($arrStock)):
    $intTotalQuantity1 = 0; $intTotalQuantity2 = 0; $intTotalQuantity3 = 0;
    foreach($arrStock as $e): 
        $arrQuantityDisplay = array();
        if($arrProduct['prod_unit1'] != '-') $arrQuantityDisplay[] = $e['trhi_quantity1'].' '.$arrProduct['prod_unit1'];
        if($arrProduct['prod_unit2'] != '-') $arrQuantityDisplay[] = $e['trhi_quantity2'].' '.$arrProduct['prod_unit2'];
        $arrQuantityDisplay[] = $e['trhi_quantity3'].' '.$arrProduct['prod_unit3'];

        if($e['trhi_transtype'] == 1) $strTransType = '(+)';
        else $strTransType = '(-)';
        switch($e['trhi_type']) {
            case 'opname': $strViewURL = 'adjustment'; break;
            case 'mutasi_in': $strViewURL = 'adjustment'; break;
            case 'mutasi_out': $strViewURL = 'adjustment'; break;
            default: $strViewURL = $e['trhi_type']; break;
        } 
        $strViewURL = site_url($strViewURL.'/view/'.$e['code']['header_id'], NULL, FALSE); ?>
        <tr>
            <td><a href="<?=$strViewURL?>" target="_blank"><?=$e['code']['code']?> <i class="fa fa-eye"></i></a></td>
            <td><?=formatDate2($e['trhi_date'],'d F Y H:i')?></td>
            <td><?=$e['trhi_type']?></td>
            <td class="tdTitle"><?=$strTransType?> <?=implode(' | ', $arrQuantityDisplay)?></td>
        </tr><?php
        $intTotalQuantity1 += $e['trhi_transtype'] * $e['trhi_quantity1'];
        $intTotalQuantity2 += $e['trhi_transtype'] * $e['trhi_quantity2'];
        $intTotalQuantity3 += $e['trhi_transtype'] * $e['trhi_quantity3'];
    endforeach; 
        $intSumTotalQuantity = ($intTotalQuantity1 * $e['trhi_conv1']) + ($intTotalQuantity2 * $e['trhi_conv2']) + ($intTotalQuantity3 * $e['trhi_conv3']);


        $arrQuantityDisplay = array();
        if($arrProduct['prod_unit1'] != '' && $e['trhi_conv1'] > 1) {
            $intTotalQuantity1 = floor($intSumTotalQuantity / $e['trhi_conv1']);
            $intSumTotalQuantity -= ($intTotalQuantity1 * $e['trhi_conv1']);
            $arrQuantityDisplay[] = $intTotalQuantity1.' '.$arrProduct['prod_unit1'];
        }
        if($arrProduct['prod_unit2'] != '' && $e['trhi_conv2'] > 1) {
            $intTotalQuantity2 = floor($intSumTotalQuantity / $e['trhi_conv2']);
            $intSumTotalQuantity -= ($intTotalQuantity2 * $e['trhi_conv2']);
            $arrQuantityDisplay[] = $intTotalQuantity2.' '.$arrProduct['prod_unit2'];
        }
        $intTotalQuantity3 = floor($intSumTotalQuantity / $e['trhi_conv3']);
        $arrQuantityDisplay[] = $intTotalQuantity3.' '.$arrProduct['prod_unit3']; ?>
        <tr>
            <td colspan="3" style="text-align:right;"><b>Total</b></td>
            <td class="tdTitle"><?=implode(' | ', $arrQuantityDisplay)?></td>
        </tr><?php
else: ?>
        <tr><td class="noData" colspan="<?=(count($arrWarehouse) + 3)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
</div>