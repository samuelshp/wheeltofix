<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-list"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory'), 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>  

<div class="col-xs-12">
<form name="searchForm" method="post" action="<?=site_url('inventory/browse', NULL, FALSE)?>" class="form-inline pull-right" style="">
    <div class="input-group" style="max-width:250px;">
        <span class="input-group-btn">
            <select class="form-control" name="filterType">
                <option value="search">Cari</option>
                <option value="stocklimit_only">Melewati Batas Stok</option>
            </select>
        </span>
        <input type="text" name="txtSearchValue" value="" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-searchbytitle')?>" />
        <span class="input-group-btn"><button type="submit" name="subSearch" value="search" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-search')?></button></span>
    </div>
<button type="submit" name="subCalibrate" value="calibrate" class="btn btn-warning pull-left" style="margin-right: 5px;" title="Kalibrasi Stok" onclick="return confirm('Apakah anda yakin? Proses ini membutuhkan waktu lebih untuk berjalan. Pastikan anda menunggu proses sampai halaman terakhir.')" tabindex="-1"><i class="fa fa-crosshairs"></i></button>
</form>
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
    <thead><tr>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-title')?></th>
        <th>Rak</th>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-buyprice')?></th>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-baseprice')?></th>
        <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
        <th>+/-</th><?php
foreach($arrWarehouse as $e): ?>  
        <th><?=$e['ware_name']?></th><?php
endforeach; ?>  
        <th class="action">Action</th>
    </tr></thead>
    <tbody><?php
// Display data in the table
if(!empty($arrProduct)):
    foreach($arrProduct as $e): ?>  
        <tr>
            <td><?=formatProductName('ABBR',$e['prod_title'],$e['prob_title'],$e['proc_title'])?></td>
            <td><?=$e['prod_rak']?></td>
            <td><?=setPrice($e['prci_price'])?></td>
            <td><?=setPrice($e['prod_hpp'],$e['prod_currency'])?></td>
            <td><?=setPrice($e['prod_price'])?></td>
            <td><?php
                if(!empty($e['prod_price']) && !empty($e['prod_hpp'])) echo setPrice(floatval($e['prod_price']) - floatval($e['prod_hpp']));
                else echo '-';
            ?></td><?php
        foreach($arrWarehouse as $e2):
            $strTDClass = '';
            if($e['arrStock'][$e2['id']] !== '~' && $e['prod_type'] == '1') {
                $strHTMLStock = formatProductUnit($e['id'],$e['arrStock'][$e2['id']],'separated').' <a href="'.site_url(array('inventory/view',$e['id'],$e2['id']), NULL, FALSE).'" title="click to view"><i class="fa fa-eye"></i></a>';
                if(intval($e2['ware_stock_warning']) == 2 && !empty($e['prod_stocklimit']) && floatval($e['arrStock'][$e2['id']]) <= floatval($e['prod_stocklimit'])) {

                    $strTDClass = 'danger';
                }
            } else $strHTMLStock = '~'; ?>  
            <td class="<?=$strTDClass?>"><?=$strHTMLStock?></td><?php
        endforeach; ?>  
            <td class="action">
                <a href="<?=site_url('inventory/print_barcode/'.$e['id'], NULL, FALSE)?>" target="_blank"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-printbarcode')?></a>
                <a href="<?=site_url('inventory/print_tag/'.$e['id'], NULL, FALSE)?>" target="_blank"><i class="fa fa-tag"></i></a>
            </td>
        </tr><?php
    endforeach;
else: ?>
        <tr class="info"><td class="noData" colspan="<?=(count($arrWarehouse) + 4)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>
    </tbody>
    </table></div>
    <?=$strPage?>
</div>