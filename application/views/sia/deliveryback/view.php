<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-truck"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverystatus'), 'link' => site_url('delivery/backbrowse')),
    2 => array('title' => '<i class="fa fa-eye"></i> View', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<!-- KURANFG DELETE SENG BARU TRUS DI UPDATE D-->
<!-- Jump To -->
<div class="col-xs-6 col-xs-offset-6"><div class="form-group">
	<label><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-lastdeliveryback')?></label>
	<select name="selJumpTo" class="form-control">
		<option value="">- Jump To -</option><?php
		if(!empty($arrDeliveryList)) foreach($arrDeliveryList as $e):
			$strListTitle = ' title="'.$e['supp_address'].', '.$e['supp_city'].'"'; ?>
		<option value="<?=site_url('delivery/backview/'.$e['id'])?>">[<?=$e['deba_code']/*formatDate2($e['deba_date'],'d/m/Y H:i')*/?>] <?=$e['sals_name']?></option><?php
		endforeach; ?>
	</select>
</div></div>

<div class="col-xs-12"><form name="frmChangeDelivery" id="frmChangeDelivery" method="post" action="<?=site_url('delivery/backview/'.$intDeliveryID, NULL, FALSE)?>" class="frmShop">

<!-- Header Faktur -->
<div class="row">
    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivererdata')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverername')?></div>
			<div class="col-xs-8"><?=$arrDeliveryData['sals_name']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivereraddress')?></div>
			<div class="col-xs-8"><?=$arrDeliveryData['sals_address']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivererphone')?></div>
			<div class="col-xs-8"><?=$arrDeliveryData['sals_phone']?></div>
			<p class="spacer">&nbsp;</p>
		</div>
	</div></div>

    <div class="col-md-6"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-share"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')?></h3></div>
		<div class="panel-body">
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></div>
			<div class="col-xs-8"><?=$arrDeliveryData['deli_code']?></div>
			<p class="spacer">&nbsp;</p>
			<div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></div>
			<div class="col-xs-8"><?=formatDate2($arrDeliveryData['deba_date'],'d F Y')?></div>
			<p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivered')?></div>
            <div class="col-xs-8"><?=formatDate2($arrDeliveryData['deli_time'],'d F Y H:i')?></div>
            <p class="spacer">&nbsp;</p>
            <div class="col-xs-4"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveryarrivedback')?></div>
            <div class="col-xs-8"><?=formatDate2($arrDeliveryData['deli_timereturn'],'d F Y H:i')?></div>
            <p class="spacer">&nbsp;</p>
        </div>
	</div></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveredinvoice')?></h3></div>
    <div class="panel-body">

        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="invoiceItemList">
			<thead>
			<tr>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-customername')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-address')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveryarrivedtime')?></th>
                <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></th>
			</tr>
			</thead>
			<tbody><?php
			$i = 0;
			$intTotalItem = 0;
			if(!empty($arrDeliveryItem)):
				foreach($arrDeliveryItem as $e):
					$intTotalItem++; ?>
				<tr>
                    <td><a href="<?=site_url('invoice/view/'.$e['invo_id'], NULL, FALSE)?>"><?=$e['invo_code']?></a></td>
					<td><b><?=$e['cust_name']?></b></td>
					<td><?=$e['cust_address']?>, <?=$e['cust_city']?></td>
                    <?php if($bolBtnEdit): ?>
                        <?php if($e['dbit_invoice_payment']=='3') { ?>
                            <td><?=formatDate2($e['invo_arrive'],'Y/m/d H:i')?></td>
                        <?php } else if($e['dbit_invoice_payment']=='4') { ?>
                            <td><?=formatDate2($e['invo_arrive'],'Y/m/d H:i')?></td>
                        <?php } else { ?>
                            <td><input type="text" name="time[<?=$e['invo_id']?>]" class="form-control timePick" value="<?=formatDate2($e['invo_arrive'],'Y/m/d H:i')?>"></td><!--sini benerno ini sama controller e, buat edit sama delete-->
                        <?php } ?>
                        <div class="form-group">
                        <td>
                        <?php if($e['dbit_invoice_payment']=='1') { ?>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment1X<?=$e['invo_id']?>" value="1" checked>Tunai<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment2X<?=$e['invo_id']?>" value="2">Kredit<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment3X<?=$e['invo_id']?>" value="3">Kembali<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment3X<?=$e['invo_id']?>" value="4">Batal
                            <?php } else if($e['dbit_invoice_payment']=='2') { ?>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment1X<?=$e['invo_id']?>" value="1">Tunai<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment2X<?=$e['invo_id']?>" value="2" checked>Kredit<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment3X<?=$e['invo_id']?>" value="3">Kembali<br>
                                <input type="radio" name="selPayment[<?=$e['invo_id']?>]" id="selPayment3X<?=$e['invo_id']?>" value="4">Batal
                            <?php } else if($e['dbit_invoice_payment']=='3') { ?>
                                Kembali
                            <?php } else if($e['dbit_invoice_payment']=='4') { ?>
                                Batal
                            <?php }?>
                        </td>
                        </div>
                        </div>
                    <?php else: ?>
                        <td><?=$e['invo_arrive']?></td>
                        <input type="hidden" name="selPayment[<?=$e['invo_id']?>]" value="<?=$e['dbit_invoice_payment']?>">
                        <?php if($e['dbit_invoice_payment']=='1'){?>
                            <td>Tunai</td>
                        <?php } else if($e['dbit_invoice_payment']=='2') {?>
                            <td>Kredit</td>
                        <?php } else if($e['dbit_invoice_payment']=='3') {?>
                            <td>Kembali</td>
                        <?php } else if($e['dbit_invoice_payment']=='4') {?>
                            <td>Batal</td>
                        <?php }?>
                    <?php endif; ?>
				</tr>
					<?php

					$i++;
				endforeach;
			else: ?>
				<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr><?php
			endif; ?>
			</tbody>
		</table></div><?php
/*
        if($bolBtnEdit): */?><!--
		<div class="form-group"><div class="input-group addNew">
			<input type="text" id="txtNewSale" name="txtNewSale" placeholder="Tambah Barang" class="form-control" /><label class="input-group-addon" id="loadItem"></label>
		</div></div>--><?php
/*        endif; */?>

        <!--<div class="row">
            <div class="col-sm-10 tdTitle"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')*/?></div>
            <div class="col-sm-2 tdDesc currency" id="subTotalNoTax"><?/*=$intDeliveryTotal*/?></div>
        </div>-->
        <p class="spacer">&nbsp;</p>

    </div><!--/ Table Selected Items -->
</div>

<div class="row">

    <div class="col-md-4"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
		<div class="panel-body"><?php
			if($bolBtnEdit): ?>
				<div class="form-group"><textarea name="txaDescription" class="form-control" rows="7" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>"><?=$arrDeliveryData['deba_description']?></textarea></div><?php
			else: ?>
                <input type="hidden" name="txaDescription" value="<?=$arrDeliveryData['deba_description']?>" />
				<div class="form-group"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?>: </b><?=$arrDeliveryData['deba_description']?></div><?php
			endif; ?>
			<!--<div class="form-group">
				<label><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-ekspeditionnumber')*/?></label>
				<input type="text" name="txtEkspedition" value="<?/*=$arrDeliveryData['deli_ekspedition_number']*/?>" maxlength="64" class="form-control" />
			</div>-->
			<!--<div class="form-group">
				<label><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-progress')*/?></label>
				<input type="text" name="txtProgress" value="<?/*=$arrDeliveryData['deli_progress']*/?>" maxlength="64" class="form-control" />
			</div>-->
		</div>
	</div></div>

    <!-- Bonus -->
    <!--<div class="col-md-8"><div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-carrieditem')*/?></h3></div>
		<div class="panel-body">

			<div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
					<thead>
					<tr>
						<th class="qty"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')*/?></th>
						<th><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')*/?></th>
					</tr>
					</thead>
					<tbody><?php
/*					$i = 0;
					$intTotalBonusItem = 0;
					$iditembonusawal='';
					if(!empty($arrItem)):
						foreach($arrItem as $e):
							$iditembonusawal=$iditembonusawal.$e['id'].',';
							$intTotalBonusItem++; */?>
						<tr>
							<td class="qty" id="labelQtyBonusX<?/*=$i*/?>"><?php
/*									echo $e['qty1'].' '.formatUnitName($e['unit1']).' + '.$e['qty2'].' '.formatUnitName($e['unit2']).' + '.$e['qty3'].' '.formatUnitName($e['unit3']);
								*/?>
							</td>
							<td><b><?/*=$e['name']*/?></b></td>
							<input type="hidden" id="idItemBonusX<?/*=$i*/?>" value="<?/*=$e['id']*/?>">
							<input type="hidden" id="qty1ItemBonusX<?/*=$i*/?>" value="<?/*=$e['qty1']*/?>">
							<input type="hidden" id="qty2ItemBonusX<?/*=$i*/?>" value="<?/*=$e['qty2']*/?>">
							<input type="hidden" id="qty3ItemBonusX<?/*=$i*/?>" value="<?/*=$e['qty3']*/?>">
							<input type="hidden" id="unit1ItemBonusX<?/*=$i*/?>" value="<?/*=formatUnitName($e['unit1'])*/?>">
							<input type="hidden" id="unit2ItemBonusX<?/*=$i*/?>" value="<?/*=formatUnitName($e['unit2'])*/?>">
							<input type="hidden" id="unit3ItemBonusX<?/*=$i*/?>" value="<?/*=formatUnitName($e['unit3'])*/?>">
						</tr><?php
/*							$i++;
						endforeach;
						$iditembonusawal=$iditembonusawal.'0';
					else: */?>
						<tr class="info"><td class="noData" colspan="3"><?/*=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")*/?></td></tr><?php
/*					endif; */?>
					</tbody>
				</table></div>

		</div>--><!--/ Table Bonus -->
	</div></div>

</div>

<?php 
    $rawStatus = $arrDeliveryData['deba_rawstatus'];
    include_once(APPPATH.'/views/'.$strContentViewFolder.'/formviewbutton.php'); 
?>  

<input type="hidden" id="totalItemAwal" name="totalItemAwal" value="<?=$intTotalItem?>"/>
<input type="hidden" id="totalItem" name="totalItem" value="0"/>

</form></div>


