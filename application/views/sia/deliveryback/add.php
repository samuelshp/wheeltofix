<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-truck"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverystatus'), 'link' => site_url('delivery/backbrowse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>

<div class="col-xs-12"><form name="frmAddDelivery" id="frmAddDelivery" method="post" action="<?=site_url('delivery/back', NULL, FALSE)?>" class="frmShop">
	<div class="row">
		<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-datadelivery')?></h3></div>
			<div class="panel-body">
				<h4><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-selectdelivery')?> <a href="<?=site_url('delivery', NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></h4>
				<div class="form-group"><div class="input-group"><input type="text" id="txtDelivery" name="txtDelivery" class="form-control" placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-selectdeliverer')?>" /><label class="input-group-addon" id="loadDeliverer"></label></div></div>
				<input type="hidden" name="txtDeliveryID" id="txtDeliveryID" />
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverername')?></label><input id="txtDelivererName" class="form-control required"/></div></div>
				<!--<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivereraddress')?></label><input id="txtDelivererAddress" class="form-control required"/></div></div>
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivererphone')?></label><input id="txtDelivererPhone" class="form-control required"/></div></div>-->
			</div>
		</div></div>

		<!--<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-thumb-tack"></i> <?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousedata')*/?></h3></div>
			<div class="panel-body">
				<h4><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')*/?> <a href="<?/*=site_url('adminpage/table/add/13', NULL, FALSE)*/?>"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')*/?></a></h4>
				<div class="form-group"><div class="input-group"><input type="text" id="txtWarehouse" name="txtWarehouse" class="form-control" placeholder="<?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectwarehouse')*/?>" /><label class="input-group-addon" id="loadWarehouse"></label></div></div>
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?/*=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-ekspeditionnumber')*/?></label><input type="text" name="txtEkspedition" value="<?php /*if(!empty($strEkspedition)) echo $strEkspedition; */?>" class="form-control required" /></div></div>
			 </div>
		</div></div>-->

		<div class="col-md-6"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3></div>
			<div class="panel-body">
				<!-- Dibuat -->
				<div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></label><input type="text" name="txtDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime" /></div></div>
                <div class="form-group"><div class="input-group"><label class="input-group-addon"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveryarrivedback')?></label><input type="text" name="txtDateHour" value="<?php if(!empty($strDateHour)) echo $strDateHour; else echo date('Y/m/d H:i'); ?>" class="form-control required jwDateTime" /></div></div>
            </div>
		</div></div>
	</div>

	<!-- Selected Items -->
	<div class="panel panel-primary">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveredinvoice')?></h3></div>
		<div class="panel-body">

			<div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItems">
				<thead>
				<tr>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-customername')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-address')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliveryarrivedtime')?></th>
                    <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-payment')?></th>
					<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-total')?></th>
				</tr>
				</thead>
				<tbody>
				<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
				</tbody>
			</table></div>

			<p class="spacer">&nbsp;</p>
			<div class="row">
				<div class="col-sm-6 tdTitle"></div>
				<div class="col-sm-2 tdDesc"></div>
				<div class="col-sm-2 tdTitle">Total Uang Yang Dibawa Kembali (Tunai):</div>
				<div class="col-sm-2 tdDesc"><label id="subTotalTax">0</label></div>
			</div>
			<p class="spacer">&nbsp;</p>
		</div><!--/ Table Selected Items -->
	</div>

	<div class="row">

		<div class="col-md-4"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
			<div class="panel-body">
				<div class="form-group"><textarea name="txaDescription" id="txaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea></div>
			</div>
		</div></div>

		<!-- Bonus -->
		<div class="col-md-8"><div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-carrieditem')?></h3></div>
			<div class="panel-body">
                <p class="text-info">*) Kolom di bawah berisi barang yang dibawa kembali dari customer</p>
				<div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedItemsBonus">
						<thead>
						<tr>
							<th class="qty"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
							<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
						</tr>
						</thead>
						<tbody>
							<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
						</tbody>
					</table></div>

			</div><!--/ Table Bonus -->
		</div></div>

	</div>

	<div class="form-group"><button type="submit" name="smtMakeDelivery" value="Make Delivery" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>


</form></div>

