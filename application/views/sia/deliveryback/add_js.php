<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript">$(document).ready(function() {
$('input[name="txtDateHour"]').datetimepicker({
    dateFormat: "yy/mm/dd"
});

function getFormattedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hour = date.getHours();
    var min= date.getMinutes();
    if(month<10){
        month= '0'+month;
    }
    if(day<10){
        day= '0'+day;
    }
    return year + '/' + month + '/' + day +' '+ hour+':'+min;
}
var mode=0;
var totalItem=0;
var totalItemBonus=0;
var numberItem=0;
var numberItemBonus=0;
var carriedIDItem=[];
var carriedQty1Item=[];
var carriedQty2Item=[];
var carriedQty3Item=[];
var carriedNameItem=[];
var carriedItemTD=[];
var flagdelete=[];
$('tr.txtData').hide();
$("#subTotalTax").autoNumeric('init', autoNumericOptionsRupiah);
function reset(){
	$("#txtTax").val(0);
	totalItem=0;
	numberItem=0;
	totalItemBonus=0;
	numberItemBonus=0;
}
function setManual(){
	$("#txtDelivererAddress").prop('disabled', true);
	$("#txtDelivererName").prop('disabled', true);
	$("#txtDelivererPhone").prop('disabled', true);
	mode=0; /* 0 manual 1 auto */
	$("#addNewSale").show();
	reset();
	var i=0;
};
setManual();
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });

$("#frmAddDelivery").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});

$("#frmAddDelivery").submit(function() {
	if($("#txtDelivery").val() == ''){
		alert("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-pleaseselectdeliverer')?>"); return false;
	}
	/*var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});*/
	
	if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-areyousure')?>") == false) return false;
	
	return true;
});
/*  Batch process */
$("abbr.list").click(function() {
	if($("#selNewSale").children("option:selected").val() > 0) {
		if($("#hidNewSale").val() == '') $("#hidNewSale").val($("#selNewSale").children("option:selected").val());
		else $("#hidNewSale").val($("#hidNewSale").val() + '; ' + $("#selNewSale").children("option:selected").val());
		$("#selNewSale").children("option:selected").remove();
	}
});
$("abbr.list").hover(function() {
	if($("#hidNewSale").val() != '') {
		strNewSale = $("#hidNewSale").val(); arrNewSale = strNewSale.split("; ");
		$(this).attr("title",arrNewSale.length + ' Item(s)');
	}
});

var $arrSelectedPO;
var selectedPO;




$("#selectedItems").on("change","input[type='text'][name*='prcPriceEffect']",function(){
	$(this).autoNumeric('init', autoNumericOptionsRupiah);
});

$("#selectedItems").on("click","input[type='checkbox'][name*='cbDelete']",function(){
	var at=this.name.substring(this.name.indexOf('X')+1,this.name.length);
	numberItem--;
	if(numberItem=='0'){
		$("#selectedItems tbody").append(
			'<tr class="info"><td class="noData" colspan="6"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
		);
	}
	$.ajax({
		url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + $("#idSaleX"+at).val(),
		success: function(data){
			var xmlDoc = $.parseXML(data);
			var xml = $(xmlDoc);
			$arrSelectedPO = xml;
			/* var x=totalItem-1; */
			var total =xml.find('Total').text();
			var grandTotal= parseInt($("#subTotalTax").autoNumeric('get')) - parseInt(total);
			$("#subTotalTax").autoNumeric('set',grandTotal);
			$.map(xml.find('Item').find('item'),function(val,i){
				var idItem=$(val).find('id').text();
				for(var zz=0;zz<carriedIDItem.length;zz++){
					if(carriedIDItem[zz]==idItem){
						carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])-(parseInt($(val).find('qty1').text()));
						carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])-(parseInt($(val).find('qty2').text()));
						carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])-(parseInt($(val).find('qty3').text()));
						if(carriedQty1Item[zz] == 0 && carriedQty2Item[zz] == 0 && carriedQty3Item[zz] ==0){
							carriedIDItem.splice(zz,1);
							carriedNameItem.splice(zz,1);
							carriedQty1Item.splice(zz,1);
							carriedQty2Item.splice(zz,1);
							carriedQty3Item.splice(zz,1);
							$("#qty1ItemBonusX"+zz).closest("tr").remove();
							numberItemBonus--;
							if(numberItemBonus=='0'){
								$("#selectedItemsBonus tbody").append(
									'<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
								);
							}
						}else{
							$("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
							$("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
							$("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
						}
					}
				}
			});
		}
	});
	selectedInvoiceID = jQuery.grep(selectedInvoiceID, function(value) {
		return value != $("#idSaleX"+at).val();
	});
	$(this).closest("tr").remove();
});

var selectedInvoiceID=[];
/*$("#txtNewSale").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceAutoComplete', NULL, FALSE)?>/" + $('input[name="txtNewSale"]').val(),
			beforeSend: function() {
				$("#loadItem").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadItem").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Invoice').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var strCode=$(val).find('invo_code').text();
					var strName=strCode+", "+$(val).find('cust_name').text();
					if($.inArray(intID, selectedInvoiceID) > -1){

					}else{
						display.push({label: strName, value:'',id:intID});
					}
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Invoice').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		var i=totalItem;
		if(numberItem=='0'){
			$("#selectedItems tbody").html('');
		}
		$("#selectedItems tbody").append(
			'<tr>'+
				'<td><input type="checkbox" name="cbDeleteX'+i+'"/></td>'+
				'<td><label id="dateSaleX'+i+'"></label></td>'+
				'<td><label id="nameSaleX'+i+'"></label></td>'+
				'<td><label id="addressSaleX'+i+'"></label></td>'+
				'<td><label id="statusSaleX'+i+'"></label></td>'+
				'<td><?=str_replace(" 0","",setPrice("","USED"))?><label id="totalSaleX'+i+'" ></label></td>'+
				'<input type="hidden" id="idSaleX'+i+'" name="idSale'+i+'">'+
				'</tr>'
		);

		var date = $(selectedPO).find('cdate').text();
		var name = $(selectedPO).find('cust_name').text();
		var address = $(selectedPO).find('cust_address').text()+", "+$(selectedPO).find('cust_city').text();
		var status=$(selectedPO).find('invoice_status').text();
		var id = $(selectedPO).find('id').text();
		selectedInvoiceID.push(id);
		$("#dateSaleX"+i).text(date);
		$("#nameSaleX"+i).text(name);
		$("#addressSaleX"+i).text(address);
		$("#statusSaleX"+i).text(status);
		$("#idSaleX"+i).val(id);
		var price=0;
		$.ajax({
			url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + id,
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				*//* var x=totalItem-1; *//*
				var total =xml.find('Total').text();
				$.map(xml.find('Item').find('item'),function(val,i){
					var idItem=$(val).find('id').text();
					var flag=0;
					for(var zz=0;zz<carriedIDItem.length;zz++){
						if(carriedIDItem[zz]==idItem){
							carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])+(parseInt($(val).find('qty1').text()));
							carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])+(parseInt($(val).find('qty2').text()));
							carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])+(parseInt($(val).find('qty3').text()));
							$("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
							$("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
							$("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
							flag=1;
						}
					}
					if(flag==0){
						var nameItem=$(val).find('name').text();
						var q1=$(val).find('qty1').text();
						var q2=$(val).find('qty2').text();
						var q3=$(val).find('qty3').text();
						carriedIDItem.push(idItem);
						carriedQty1Item.push(q1);
						carriedQty2Item.push(q2);
						carriedQty3Item.push(q3);
						carriedNameItem.push(nameItem);
						var i=totalItemBonus;
						if(numberItemBonus=='0'){
							$("#selectedItemsBonus tbody").html('');
						}
						$("#selectedItemsBonus tbody").append(
							'<tr>'+
								'<td class="qty"><div class="form-group">'+
								'<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" /></div>' +
								'<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" /></div>' +
								'<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" /></div>'+
								'</div></td>'+
								'<td><label id="nmeItemBonusX'+i+'"></label></td>'+
								'<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
								'<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
								'<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
								'<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
								'<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
								'<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
								'</tr>');
						numberItemBonus++;
						totalItemBonus++;
						$("#qty1ItemBonusX"+i).val(q1);
						$("#qty2ItemBonusX"+i).val(q2);
						$("#qty3ItemBonusX"+i).val(q3);
						$("#nmeItemBonusX"+i).text(nameItem);
					}
				});
				$("#totalSaleX"+i).text(total);
				$("#totalSaleX"+i).autoNumeric('init', autoNumericOptionsRupiah);
				var grandTotal=$("#subTotalTax").autoNumeric('get')+total;
				$("#subTotalTax").autoNumeric('set',grandTotal);
			}
		});
		totalItem++;
		numberItem++;
		$("#totalItem").val(totalItem);
	}
});*/
/*$("#txtWarehouse").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('inventory_ajax/getWarehouseComplete, NULL, FALSE + $("#txtWarehouse").val(),
			beforeSend: function() {
				$("#loadWarehouse").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadWarehouse").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Warehouse').find('item'),function(val,i){
					var intID = $(val).find('id').text();
					var name = $(val).find('ware_name').text();
					display.push({label: name, value: name, id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Warehouse').find('item'), function(el) {
			return $(el).find('id').text() == ui.item.id;
		});
		$("#WarehouseID").val($(selectedPO).find('id').text());
        $("#loadWarehouse").html('<i class="fa fa-check"></i>');
	}
});*/

$("#txtDelivery").autocomplete({
	minLength:3,
	delay:500,
	source: function(request,response) {
		$.ajax({
			url: "<?=site_url('delivery_ajax/getDeliveryAutoComplete', NULL, FALSE)?>/" + $('#txtDelivery').val(),
			beforeSend: function() {
				$("#loadDeliverer").html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
			},
			complete: function() {
				$("#loadDeliverer").html('');
			},
			success: function(data){
				var xmlDoc = $.parseXML(data);
				var xml = $(xmlDoc);
				$arrSelectedPO = xml;
				var display=[];
				$.map(xml.find('Delivery').find('item'),function(val,i){
					var intID = $(val).find('deli_id').text();
					var strCode=$(val).find('deli_code').text();
					var strName=strCode+", "+$(val).find('sals_name').text();
					display.push({label: strName, value:strCode,id:intID});
				});
				response(display);
			}
		});
	},
	select: function(event, ui) {
		var selectedPO = $.grep($arrSelectedPO.find('Delivery').find('item'), function(el) {
			return $(el).find('deli_id').text() == ui.item.id;
		});
		$('tr.txtData').show();
		var id=$(selectedPO).find('deli_id').text();
		var name=$(selectedPO).find('sals_name').text();
        var description=$(selectedPO).find('deli_description').text();
		$("#txtDelivererName").val(name);
        $("#txtDeliveryID").val(id);
        $('#txaDescription').val(description);
        $.ajax({
            url: "<?=site_url('delivery_ajax/getInvoiceByDeliveryID', NULL, FALSE)?>/" + id,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                var display=[];
                $("#selectedItems tbody").html('');
                $("#selectedItemsBonus tbody").html('');
                $("#selectedItemsBonus tbody").append('' +
                    '<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>');
                $.map(xml.find('Delivery').find('item'),function(val,i){
                    var invoid = $(val).find('invo_id').text();
                    var invocode = $(val).find('invo_code').text();
                    var invototal = $(val).find('invo_grandtotal').text();
                    var invodate=$(val).find('invo_date').text();
                    var custname=$(val).find('cust_name').text();
                    var custaddress=$(val).find('cust_address').text();
                    var custcity=$(val).find('cust_city').text();
                    var payment=$(val).find('invo_payment').text();
                    $("#selectedItems tbody").append(
                        '<tr>'+
                            '<td>'+invocode+'</td>'+
                            '<td>'+custname+'</td>'+
                            '<td>'+custaddress+'</td>'+
                            '<td><input type="text" name="time['+invoid+']" class="form-control" id="timeX'+invoid+'" ></td>'+
                            '<td><div class="form-group">' +
                            '<input type="radio" name="selPayment['+invoid+']" id="selPayment1X'+invoid+'" value="1" checked>Tunai<br>' +
                            '<input type="radio" name="selPayment['+invoid+']" id="selPayment2X'+invoid+'" value="2">Kredit<br>' +
                            '<input type="radio" name="selPayment['+invoid+']" id="selPayment3X'+invoid+'" value="3">Kembali<br>' +
                            '<input type="radio" name="selPayment['+invoid+']" id="selPayment3X'+invoid+'" value="4">Batal' +
                            '</div></td>'+
                            '<td><?=str_replace(" 0","",setPrice("","USED"))?> <label id="totalSaleX'+invoid+'" ></label></td>'+
                            '</tr>'
                    );
                    $("#totalSaleX"+invoid).text(invototal);
                    $("#totalSaleX"+invoid).autoNumeric('init', autoNumericOptionsRupiah);
                    if(payment=='1'){
                        $("#selPayment1X"+invoid).prop('checked', true);
                        $("#subTotalTax").autoNumeric('set',parseInt($("#subTotalTax").autoNumeric('get')) + parseInt(invototal));
                    }else if(payment=='2'){
                        $("#selPayment2X"+invoid).prop('checked', true);
                    }
                    $("#timeX"+invoid).val(getFormattedDate(new Date()));
                    $("#timeX"+invoid).datetimepicker({
                        dateFormat: "yy/mm/dd"
                    });

                });
            }
        });
        $("#loadDeliverer").html('<i class="fa fa-check"></i>');
	}
});

var paymentbefore=0;

$("#selectedItems").on("focus","input[name*='selPayment']:radio",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    if($("#selPayment1X"+at).is(':checked')){
        paymentbefore=1;
    }else if($("#selPayment2X"+at).is(':checked')){
        paymentbefore=2;
    }else if($("#selPayment3X"+at).is(':checked')){
        paymentbefore=3;
    }
});

$("#selectedItems").on("click","input[name*='selPayment']:radio",function(){
    var at=this.id.substring(this.id.indexOf('X')+1,this.id.length);
    if(paymentbefore==1 && $(this).val()==2){
        //kurangi subtotal
        $("#subTotalTax").autoNumeric('set',
        	parseInt($("#subTotalTax").autoNumeric('get')) - parseInt($("#totalSaleX"+at).autoNumeric('get'))
        );
    }else if(paymentbefore==1 && ($(this).val()==3 || $(this).val()==4)){
        //kurangi subtotal && tambah barang bawaan
        $("#subTotalTax").autoNumeric('set',
        	parseInt($("#subTotalTax").autoNumeric('get')) - parseInt($("#totalSaleX"+at).autoNumeric('get'))
        );
        $.ajax({
            url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + at,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                $.map(xml.find('Item').find('item'),function(val,i){
                    var idItem=$(val).find('id').text();
                    var flag=0;
                    for(var zz=0;zz<carriedIDItem.length;zz++){
                        if(carriedIDItem[zz]==idItem){
                            carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])+(parseInt($(val).find('qty1').text()));
                            carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])+(parseInt($(val).find('qty2').text()));
                            carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])+(parseInt($(val).find('qty3').text()));
                            $("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
                            $("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
                            $("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
                            flag=1;
                        }
                    }
                    if(flag==0){
                        var nameItem=$(val).find('name').text();
                        var q1=$(val).find('qty1').text();
                        var q2=$(val).find('qty2').text();
                        var q3=$(val).find('qty3').text();
                        carriedIDItem.push(idItem);
                        carriedQty1Item.push(q1);
                        carriedQty2Item.push(q2);
                        carriedQty3Item.push(q3);
                        carriedNameItem.push(nameItem);
                        flagdelete.push(0);
                        var i=totalItemBonus;
                        if(numberItemBonus=='0'){
                            $("#selectedItemsBonus tbody").html('');
                        }
                        $("#selectedItemsBonus tbody").append(
                            '<tr>'+
                                '<td class="qty"><div class="form-group">'+
                                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" /></div>' +
                                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" /></div>' +
                                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" /></div>'+
                                '</div></td>'+
                                '<td><label id="nmeItemBonusX'+i+'"></label></td>'+
                                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                                '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                                '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                                '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                                '</tr>');
                        numberItemBonus++;
                        totalItemBonus++;
                        carriedItemTD.push(i);
                        $("#qty1ItemBonusX"+i).val(q1);
                        $("#qty2ItemBonusX"+i).val(q2);
                        $("#qty3ItemBonusX"+i).val(q3);
                        $("#nmeItemBonusX"+i).text(nameItem);
                    }
                });
            }
        });
    }else if(paymentbefore==2 && $(this).val()==1){
        //tambah uang
        $("#subTotalTax").autoNumeric('set',
        	parseInt($("#subTotalTax").autoNumeric('get')) + parseInt($("#totalSaleX"+at).autoNumeric('get'))
        );
    }else if(paymentbefore==2 && ($(this).val()==3 || $(this).val()==4)){
        //tambah barang bawaan
        $.ajax({
            url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + at,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                $.map(xml.find('Item').find('item'),function(val,i){
                    var idItem=$(val).find('id').text();
                    var flag=0;
                    for(var zz=0;zz<carriedIDItem.length;zz++){
                        if(carriedIDItem[zz]==idItem){
                            carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])+(parseInt($(val).find('qty1').text()));
                            carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])+(parseInt($(val).find('qty2').text()));
                            carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])+(parseInt($(val).find('qty3').text()));
                            $("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
                            $("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
                            $("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
                            flag=1;
                        }
                    }
                    if(flag==0){
                        var nameItem=$(val).find('name').text();
                        var q1=$(val).find('qty1').text();
                        var q2=$(val).find('qty2').text();
                        var q3=$(val).find('qty3').text();
                        carriedIDItem.push(idItem);
                        carriedQty1Item.push(q1);
                        carriedQty2Item.push(q2);
                        carriedQty3Item.push(q3);
                        carriedNameItem.push(nameItem);
                        flagdelete.push(0);
                        var i=totalItemBonus;
                        if(numberItemBonus=='0'){
                            $("#selectedItemsBonus tbody").html('');
                        }
                        $("#selectedItemsBonus tbody").append(
                            '<tr>'+
                                '<td class="qty"><div class="form-group">'+
                                '<div class="col-xs-4"><input type="text" name="qty1PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty1ItemBonusX'+i+'" /></div>' +
                                '<div class="col-xs-4"><input type="text" name="qty2PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty2ItemBonusX'+i+'" /></div>' +
                                '<div class="col-xs-4"><input type="text" name="qty3PriceEffectBonus'+i+'" class="required number form-control input-sm" id="qty3ItemBonusX'+i+'" /></div>'+
                                '</div></td>'+
                                '<td><label id="nmeItemBonusX'+i+'"></label></td>'+
                                '<input type="hidden" id="idItemBonusX'+i+'" name="idItemBonus'+i+'">'+
                                '<input type="hidden" id="prodItemBonusX'+i+'" name="prodItemBonus'+i+'">'+
                                '<input type="hidden" id="probItemBonusX'+i+'" name="probItemBonus'+i+'">' +
                                '<input type="hidden" id="sel1UnitBonusIDX'+i+'" name="sel1UnitBonusID'+i+'">' +
                                '<input type="hidden" id="sel2UnitBonusIDX'+i+'" name="sel2UnitBonusID'+i+'">' +
                                '<input type="hidden" id="sel3UnitBonusIDX'+i+'" name="sel3UnitBonusID'+i+'">'+
                                '</tr>');
                        numberItemBonus++;
                        totalItemBonus++;
                        carriedItemTD.push(i);
                        $("#qty1ItemBonusX"+i).val(q1);
                        $("#qty2ItemBonusX"+i).val(q2);
                        $("#qty3ItemBonusX"+i).val(q3);
                        $("#nmeItemBonusX"+i).text(nameItem);
                    }
                });
            }
        });
    }else if((paymentbefore==3 || paymentbefore==4) && $(this).val()==1){
        //tambah uang && kurangi barang bawaan
        $("#subTotalTax").autoNumeric('set',
        	parseInt($("#subTotalTax").autoNumeric('get')) + parseInt($("#totalSaleX"+at).autoNumeric('get'))
        );
        $.ajax({
            url: "<?=site_url('invoice_ajax/getInvoiceTotal')?>/" + at,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                $.map(xml.find('Item').find('item'),function(val,i){
                    var idItem=$(val).find('id').text();
                    for(var zz=0;zz<carriedIDItem.length;zz++){
                        if(carriedIDItem[zz]==idItem){
                            carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])-(parseInt($(val).find('qty1').text()));
                            carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])-(parseInt($(val).find('qty2').text()));
                            carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])-(parseInt($(val).find('qty3').text()));
                            if(carriedQty1Item[zz] <= 0 && carriedQty2Item[zz] <= 0 && carriedQty3Item[zz] <=0){
                                flagdelete[zz]=1;
                                $("#qty1ItemBonusX"+carriedItemTD[zz]).closest("tr").remove();
                                numberItemBonus--;
                                if(numberItemBonus=='0'){
                                    $("#selectedItemsBonus tbody").append(
                                        '<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
                                    );
                                }
                            }else{
                                $("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
                                $("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
                                $("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
                            }
                        }
                    }
                });
                for(var zz=0;zz<carriedIDItem.length;zz++){
                    if(flagdelete[zz]==1){
                        carriedIDItem.splice(zz,1);
                        carriedNameItem.splice(zz,1);
                        carriedQty1Item.splice(zz,1);
                        carriedQty2Item.splice(zz,1);
                        carriedQty3Item.splice(zz,1);
                        carriedItemTD.splice(zz,1);
                        flagdelete.splice(zz,1);
                        zz--;
                    }
                }
            }
        });
    }else if((paymentbefore==3 || paymentbefore==4) && $(this).val()==2){
        //kurangi barang bawaan
        $.ajax({
            url: "<?=site_url('invoice_ajax/getInvoiceTotal', NULL, FALSE)?>/" + at,
            success: function(data){
                var xmlDoc = $.parseXML(data);
                var xml = $(xmlDoc);
                $arrSelectedPO = xml;
                /* var x=totalItem-1; */
                $.map(xml.find('Item').find('item'),function(val,i){
                    var idItem=$(val).find('id').text();
                    for(var zz=0;zz<carriedIDItem.length;zz++){
                        if(carriedIDItem[zz]==idItem){
                            carriedQty1Item[zz]=parseInt(carriedQty1Item[zz])-(parseInt($(val).find('qty1').text()));
                            carriedQty2Item[zz]=parseInt(carriedQty2Item[zz])-(parseInt($(val).find('qty2').text()));
                            carriedQty3Item[zz]=parseInt(carriedQty3Item[zz])-(parseInt($(val).find('qty3').text()));
                            if(carriedQty1Item[zz] <= 0 && carriedQty2Item[zz] <= 0 && carriedQty3Item[zz] <=0){
                                flagdelete[zz]=1;
                                $("#qty1ItemBonusX"+carriedItemTD[zz]).closest("tr").remove();
                                numberItemBonus--;
                                if(numberItemBonus=='0'){
                                    $("#selectedItemsBonus tbody").append(
                                        '<tr class="info"><td class="noData" colspan="2"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>'
                                    );
                                }
                            }else{
                                $("#qty1ItemBonusX"+zz).val(carriedQty1Item[zz]);
                                $("#qty2ItemBonusX"+zz).val(carriedQty2Item[zz]);
                                $("#qty3ItemBonusX"+zz).val(carriedQty3Item[zz]);
                            }
                        }
                    }
                });
                for(var zz=0;zz<carriedIDItem.length;zz++){
                    if(flagdelete[zz]==1){
                        carriedIDItem.splice(zz,1);
                        carriedNameItem.splice(zz,1);
                        carriedQty1Item.splice(zz,1);
                        carriedQty2Item.splice(zz,1);
                        carriedQty3Item.splice(zz,1);
                        carriedItemTD.splice(zz,1);
                        flagdelete.splice(zz,1);
                        zz--;
                    }
                }
            }
        });
    }
});

});</script>