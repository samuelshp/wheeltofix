<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.calendar.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript">
$(document).ready(function() {

$('.timePick').datetimepicker({
    dateFormat: "yy/mm/dd"
});

$(".jwDateTime").calendar({dateFormat: 'DMY/',timeSeparators:[' '],yearRange: '-01:+04'}); /*  Untuk datetime timeSeparators:[' ',':'] */

$("#frmChangeDelivery").submit(function() {
	var form = $(this);
	$('.currency').each(function(i){
		var self = $(this);
		try{
			var v = self.autoNumeric('get');
			self.autoNumeric('destroy');
			self.val(v);
		}catch(err){
			console.log("Not an autonumeric field: " + self.attr("name"));
		}
	});
	return true;
});


});</script>