<?php
$strPageTitle = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax');
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-table"></i> '.$strPageTitle, 'link' => '')
);

include_once('application/views/'.$this->config->item('jw_style').'/js/payroll.php'); ?>

<form name="frmAddPayroll" id="frmAddPayroll" method="post" action="<?=site_url('payroll/do_upload', NULL, FALSE)?>" class="frmShop" enctype="multipart/form-data">
    <div class="col-xs-12"><input type="file" name="userfile" /></div>
    <div class="pull-left"><button type="submit" name="subSave" value="upload" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></button></div>
</form>