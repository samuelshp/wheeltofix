<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-file-text-o"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-title'), 'link' => site_url('quotation/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-eye"></i> Detail', 'link' => '')
);


include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<!-- Baru dibawah -->
<div class="col-xs-12">
    <form name="frmAddQuotation" id="frmAddQuotation" method="post" action="<?=site_url('quotation', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <!-- Nama Gudang -->
        <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '8'?>">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data Pelanggan</h3></div>
                <div class="panel-body form-horizontal">
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="nama">Nama</label></div>
                    <div class="col-md-8"><?=$arrQuotation['cust_name']?></div>
                    <p class="spacer"></p>
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="alamat">Alamat</label></div>
                    <div class="col-md-8"></div>
                    <p class="spacer"></p>
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="kota">Kota</label></div>
                    <div class="col-md-8"></div>
                    <p class="spacer"></p>
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="telepon">Telepon</label></div>
                    <div class="col-md-8"></div>
                    <p class="spacer"></p>
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="jenisOutlet">Jenis Outlet</label></div>
                    <div class="col-md-8"></div>
                    <p class="spacer"></p>
                    <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4' : '2'?>"><label for="limit">Limit</label></div>
                    <div class="col-md-8"></div>
                    <p class="spacer"></p>
                </div>
            </div>
        </div>
        <!-- nama proyek -->
        <?php if(PS_SYSTEM_MODE == 'CONTRACTOR'): ?>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i>                       
                       Data Proyek
                    </h3>
                </div>              
                <div class="panel-body">                                        
                    <div class="form-group">
                        <label>Nama Proyek</label>
                        <p><?=$arrQuotation['kont_name']?></p>
                    </div>
                    <div class="form-group">
                        <label>Nama Kontrak</label>
                        <p><?=$arrQuotation['job']?></p>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-building-o"></i>                       
                        Data Gudang
                    </h3>
                </div>              
                <div class="panel-body">
                    <div class="form-group">
                        <label>Nama Gudang</label>
                        <p><?=$arrQuotation['ware_name']?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Kode</label>
                        <p><?=$arrQuotation['quot_code']?><input type="hidden" name="idQuotation" value="<?=$arrQuotation['id']?>"></p>
                    </div>
                    <div class="form-group">
                        <label>Dibuat</label>
                        <p><?=formatDate($arrQuotation['quot_date'],'d F Y')?></p>
                    </div>                    
                    <div class="form-group">
                        <label>Ekspirasi</label>
                        <p><?=formatDate($arrQuotation['quot_exp_date'],'d F Y')?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-items')?></h3></div>

                <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover" id="selectedBahans">
                        <thead>
                            <tr>
                                <th><i class="fa fa-trash"></th>
                                <th>Bahan</th> 
                                <th style="width: 18%">Harga</th>                       
                                <th style="width: 12%">Qty</th>
                                <th style="width: 10%">Discount</th>
                                <th style="width: 18%">Subtotal</th>
                                <th style="width: 20%">Keterangan</th>                        
                            </tr>
                        </thead>
                        <tbody>
                            <?php $subtotal = 0; foreach($arrItems as $i => $e): ?>
                            <tr name=itemrow'+totalitem+'>
                            <td class="cb"><input type="checkbox" name="cbDeleteX<?=$i?>" class="editable" disabled /></td>
                            <td id="isiBahan<?=$i?>">
                                <input type="hidden" name="idProduct[]" id="idProduct<?=$i?>" value="<?=$e['prod_id']?>"><input type="hidden" name="isiBahanText<?=$i?>" id="isiBahanText<?=$i?>" value="<?=$e['prod_title']?>">
                                <?=$e['prod_title']?>
                            </td>
                            <td id="isiHarga<?=$i?>">
                                <div class="input-group">
                                    <div class="input-group-addon">Rp.</div>
                                    <input class="form-control input-sm currency editable" type="text" id="inputHarga<?=$i?>" value="<?=$e['quoi_price']?>" placeholder="0" name="inputHarga[]" readonly>
                                    <input type="hidden" name="inputAwal<?=$i?>" id="inputAwal<?=$i?>" value="0">
                                </div>
                            </td>
                            <td id="isiQty<?=$i?>">
                                <div class="input-group">
                                    <input class="form-control input-sm qty editable" type="text" id="inputQty<?=$i?>" value="<?=$e['quoi_qty_display']?>" placeholder="0" name="inputQty[]" readonly>
                                    <input type="hidden" name="inputAwal<?=$i?>" id="inputAwal<?=$i?>" value="0">
                                    <div class="input-group-addon"></div>
                                </div>
                            </td>
                            <td id="isiDsc<?=$i?>">
                                <div class="input-group">
                                    <input class="form-control input-sm qty editable" type="text" id="inputDsc<?=$i?>" value="<?=$e['quoi_disc']?>" name="inputDsc[]" readonly>
                                    <div class="input-group-addon">%</div>
                                </div>
                            </td>
                            <td id="isiSubtotal<?=$i?>">
                                <div class="input-group">
                                    <div class="input-group-addon">Rp.</div>
                                    <input class="form-control input-sm qty" type="text" id="inputSubtotal<?=$i?>" value="<?=$e['quoi_subtotal']?>" placeholder="0" name="inputSubtotal[]" readonly>
                                </div>
                            </td>
                            <td class="ket"><input class="form-control input-sm editable" type="text" id="isiKeterangan<?=$i?>" name="isiKeterangan[]" value="<?=$e['quoi_desc']?>" readonly /></td>
                            <input type="hidden" name="isiTerPB[]" id="isiTerPB<?=$i?>" value="0">
                            </tr>
                            <?php $subtotal += $e['quoi_subtotal']; ?>                            
                            <?php endforeach; ?>
                            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">Total</td>
                                <td><div class="input-group"><div class="input-group-addon">Rp.</div><input type="text" name="grandTotal" id="grandTotal" class="form-control input-sm currency" value="<?=$subtotal?>" readonly></div></td>
                                <td>&nbsp;</td>
                            </tr>
                        </tfoot>
                        </table></div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <textarea id="txtaDescription" name="txtaDescription" class="form-control editable" rows="7" readonly><?php if(!empty($arrQuotation['quot_description'])) echo $arrQuotation['quot_description']; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalNoTax" class="form-control required currency" id="subTotalNoTax" value="<?=$subtotal?>" readonly /></div>
                        </div>
                    </div>                                        
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtTax" id="txtTax" class="form-control required qty editable" maxlength="3" value="<?=$arrQuotation['quot_tax']?>" readonly /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalTax" tabindex="-1" class="form-control required currency" id="subTotalTax" value="<?=$arrQuotation['quot_finaltotal']?>" readonly /></div></div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <?php if($bolAllowUpdate && $bolAllowUpdate): ?>
        <button type="button" id="smtEditQuotation" name="smtEditQuotation" value="Edit Quotation" class="btn btn-primary">
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
        </button>
        <div id="editMode" style="display: none">
            <button type="submit" id="smtUpdateQuotation" name="smtUpdateQuotation" value="Update Quotation" class="btn btn-warning">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-edit')?>
            </button>
        <?php endif; if($bolAllowDelete && $bolBtnDelete): ?>         
            <button type="submit" id="smtDeleteQuotation" name="smtDeleteQuotation" value="Delete Quotation" class="btn btn-danger pull-right">
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-delete')?>
            </button>
        <?php endif; ?>
        </div>
    </div>

    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
    <input type="hidden" id="totalItem" name="totalItem" value="<?=($i+1);?>"/>
    <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>    
</form></div>
