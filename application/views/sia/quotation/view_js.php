<link rel="stylesheet" href="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css')?>" />
<script type="text/javascript" src="<?=base_url('asset/jquery/autoNumeric.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/jquery/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?= base_url('asset/chosen/chosen.jquery.min.js') ?>"></script>
<script type="text/javascript">
var totalitem=$("#totalItem").val();
var numberItem=$("#totalItem").val();
var subtotal=0;
var $arrSelectedPO;
var selectedPO;
$("#selectedBahans tbody tr.info").hide();
$(".jwDateTime").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".currency, .qty").autoNumeric('init',autoNumericOptionsRupiah);
$("#frmAddQuotation").submit(function(){
    var form = $(this);
    $('.currency, .qty').each(function(i){
        var self = $(this);
        try{
            var v = self.autoNumeric('get');
            self.autoNumeric('destroy');
            self.val(v);
        }catch(err){
            console.log("Not an autonumeric field: " + self.attr("name"));
        }
    });
    
    if(confirm("<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-areyousure')?>") == false) return false;
    
    return true;
});

$("button#smtEditQuotation").click(function() {
    $(this).hide();
    $("div#editMode").show();    
    $('.editable').each(function(i){
        var self = $(this);
        try{
            self.removeAttr('readonly');
            self.removeAttr('disabled');
        }catch(err){
            console.log("Error occured when trying to edit.");
        }
    });
});

$("#selectedBahans tbody").on('keyup', '.qty, .currency', function(e) {        
    calculateSubtotal(e);
});

$("#txtTax").keyup(function() {
    calculateFinaltotal();
});

$("#selectedBahans tbody").on('click', "input[type='checkbox'][name^='cbDeleteX']", function(e){    
    totalitem--;
    numberItem--;
    $(e.target)
    .parents('tr')
    .hide(1, function(){            
        $(e.target).parents('tr').remove()
        calculateGrandtotal();
    });        
    if(numberItem == 0){
        $("#selectedBahans tbody tr.info").show();
    }    
    $("#totalItem").val(totalitem);

});

function calculateSubtotal(e) {
    var currentRow = $(e.currentTarget).closest('tr');
    var harga = currentRow.find("input[name^='inputHarga']").autoNumeric('get');
    var qty = currentRow.find("input[name^='inputQty']").autoNumeric('get');
    var disc = currentRow.find("input[name^='inputDsc']").autoNumeric('get');

    var amountDisc = (harga*qty)*(disc/100)
    
    subtotal = (harga*qty)-amountDisc;

    currentRow.find("input[name^='inputSubtotal']").autoNumeric('set',subtotal);

    calculateGrandtotal();
}

function calculateGrandtotal() {
    var grandTotal = 0;
    $("input[name^='inputSubtotal']").each(function(i,el) {
        grandTotal += parseFloat($(el).autoNumeric('get'));
    });    
    $("tfoot #grandTotal, #subTotalNoTax").autoNumeric('set',grandTotal);
    calculateFinaltotal();
}

function calculateFinaltotal() {
    var dpp = parseFloat($("#subTotalNoTax").autoNumeric('get'));
    var tax = parseFloat($("#txtTax").autoNumeric('get'));
    finalTotal = dpp + (dpp*tax/100);
    $("#subTotalTax").autoNumeric('set',finalTotal);
}


$('.chosen').chosen({search_contains: true});
</script>