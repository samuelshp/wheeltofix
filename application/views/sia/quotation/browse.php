<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-file-text-o"></i> '.$strPageTitle, 'link' => '')
);

include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?>
<?php
$strSearchAction = site_url('purchase/browse', NULL, FALSE);
//include_once(APPPATH.'/views/'.$strContentViewFolder.'/formsearchquotation.php'); ?>

<div class="col-xs-12"><form name="frmAddQuotation" id="frmAddQuotation" method="post" action="<?=site_url('quotation/browse', NULL, FALSE)?>" class="frmShop">
    <div class="pull-left" style="margin:-20px 10px 0px 0px;"><?=$strPage?></div>
    <div class="col pull-left" style="text-align:right;"><a href="<?=site_url('quotation/add', NULL, FALSE)?>" class="btn btn-primary"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?></a></div>
    <p class="spacer">&nbsp;</p>
    <div class="table-responsive"><table class="table table-bordered table-condensed table-hover">
		<thead><tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-quotdate')?></th>			
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-custname')?></th>
			<?php if(PS_SYSTEM_MODE == 'CONTRACTOR'): ?>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-kontname')?></th>
			<?php endif; ?>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-total')?></th>
			<th class="action" style="width:5%;">Action</th>
		</tr></thead>
		<tbody>
		<?php
		// Display data in the table
		if(!empty($arrQuotation)):			
			foreach($arrQuotation as $e): ?>
			<tr>				
				<td><?=$e['quot_code']?></td>
				<td><?=formatDate2($e['quot_date'],'d F Y')?></td>				
				<td><?=$e['cust_name']?></td>
				<?php if(PS_SYSTEM_MODE == 'CONTRACTOR'): ?>
						<td><?=$e['kont_name']?></td>								
				<?php endif; ?>
				<td><?=setPrice($e['quot_finaltotal'])?></td>				
				<td class="action">
					<?php if($bolAllowView): ?><a style="margin:0" href="<?=site_url('quotation/view/'.$e['id'], NULL, FALSE)?>"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-view')?></a><?php endif; ?>
				</td>
			</tr><?php endforeach;
		else: ?>
			<tr class="info"><td class="noData" colspan="6"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
		endif; ?>
		</tbody>
	</table></div>
    <?=$strPage?>
	</form>
</div>