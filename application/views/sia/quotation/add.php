<?php
$strPageDescription = '<i class="fa fa-info-circle" rel="popover" data-content="'.$strMenuInstruction.'" id="InStRuCtIoN"></i>';
$arrBreadcrumb = array(
    0 => array('title' => '<i class="fa fa-dashboard"></i> Dashboard', 'link' => site_url('adminpage')),
    1 => array('title' => '<i class="fa fa-file-text-o"></i> '.loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-title'), 'link' => site_url('quotation/browse', NULL, FALSE)),
    2 => array('title' => '<i class="fa fa-plus"></i> Add', 'link' => '')
);


include_once(APPPATH.'/views/'.$this->config->item('jw_admin_style').'/contentheader.php'); ?> 
<link rel="stylesheet" href="<?= base_url('asset/chosen/chosen.min.css') ?>" />

<!-- Lama dibawah -->

<!-- Baru dibawah -->
<div class="col-xs-12">
    <form name="frmAddQuotation" id="frmAddQuotation" method="post" action="<?=site_url('quotation', NULL, FALSE)?>" class="frmShop">
    <div class="row">
        <!-- Nama Gudang -->
        <div class="col-md-<?=(PS_SYSTEM_MODE == 'CONTRACTOR') ? '4':'8' ?>">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-user"></i> Data Pelanggan</h3></div>
                <div class="panel-body">
                    <h4>Mohon pilih Pelanggan anda <a href="<?=site_url('adminpage/table/add/33')?>" target="_blank"><i class="fa fa-plus" title="Tambah"></i></a></h4>
                    <div class="radio-group" style="display:none;">
                        <label><input type="radio" name="radioCustomer" value="0" disabled=""> Non-Member</label>
                        <label><input type="radio" name="radioCustomer" value="1" checked="" disabled=""> Member</label>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="customerName" name="customerName" class="form-control input-sm ui-autocomplete-input" placeholder="Mohon pilih Pelanggan anda" autocomplete="off">
                            <label class="input-group-addon" id="loadCustomer"></label>
                            <input type="hidden" name="intCustomerID" id="intCustomerID">
                        </div>
                    </div>

                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Alamat</label><input id="customerAddress" class="form-control input-sm" disabled=""></div></div>
                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Kota</label><input id="customerCity" class="form-control input-sm" disabled=""></div></div>
                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Telepon</label><input id="customerPhone" class="form-control input-sm" disabled=""></div></div>

                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Jenis Outlet</label><input id="customerOutletTypeStr" class="form-control input-sm" disabled=""></div></div>
                    <div class="form-group"><div class="input-group"><label class="input-group-addon">Limit</label><input id="customerLimit" class="form-control input-sm" disabled=""></div></div>
                </div>
            </div>
        </div>
        <!-- nama proyek -->
        <?php if(PS_SYSTEM_MODE == 'CONTRACTOR'): ?>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder"></i>                       
                       Data Proyek
                    </h3>
                </div>              
                <div class="panel-body">                                        
                    <div class="form-group">
                        <h4>Pilih Proyek <a href="<?=site_url('kontrak')?>" target="_blank"><i class="fa fa-plus" title="Tambah"></i></a></h4>
                        <select style="max-width:550px;" type="text" name="selProyek" id="selProyek" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                            <option value="0" disabled selected>Select your option</option>
                            <?php foreach($arrKontrak as $e) { ?>
                                <option value="<?=$e['id']?>"><?=$e['kont_name']?></option>
                            <?php } ?>
                        </select>                    
                    </div>
                    <div class="form-group">
                        <h4>Pilih Kontrak <a href="<?=site_url('subkontrak')?>" target="_blank"><i class="fa fa-plus" title="Tambah"></i></a></h4>
                         <select style="max-width:550px;" type="text" name="selPekerjaan" id="selPekerjaan" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>
                    </select>                    
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-building-o"></i>                       
                        Data Gudang
                    </h3>
                </div>              
                <div class="panel-body">
                    <select style="max-width:550px;" id="selWarehouse" name="selWarehouse" class="form-control chosen" required placeholder="<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-selectproject')?>">
                        <option value="0" disabled selected>Select your option</option>                                                
                    </select>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Dibuat</div>
                            <input type="text" id="txtDateBuat" name="txtDateBuat" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control input-sm required jwDateTime"/>
                        </div>    
                    </div>                    
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Ekspirasi</div>
                            <input type="text" id="txtDateExpired" name="txtDateExpired" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control input-sm required jwDateTime"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-dropbox"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-items')?></h3></div>

                <div class="panel-body">
                    <div class="form-group">                        
                        <div class="input-group addNew">
                            <input type="text" id="txtNewItem" name="txtNewItem" placeholder="Tambah Barang" class="form-control input-sm ui-autocomplete-input" autocomplete="off"><label class="input-group-addon" id="loadItem"></label>
                        </div>
                    </div>
                    
                        <div class="table-responsive"><table class="table table-bordered table-condensed table-hover" id="selectedBahans">
                        <thead>
                            <tr>
                                <th><i class="fa fa-trash"></th>
                                <th>Bahan</th> 
                                <th style="width: 18%">Harga</th>                       
                                <th style="width: 12%">Qty</th>
                                <th style="width: 10%">Discount</th>
                                <th style="width: 18%">Subtotal</th>
                                <th style="width: 20%">Keterangan</th>                        
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info"><td class="noData" colspan="7"><?=loadLanguage("admindisplay",$this->session->userdata("jw_language"),"form-nodata")?></td></tr>                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">Total</td>
                                <td><div class="input-group"><div class="input-group-addon">Rp.</div><input type="text" name="grandTotal" id="grandTotal" class="form-control input-sm currency" value="0" readonly></div></td>
                                <td>&nbsp;</td>
                            </tr>
                        </tfoot>
                        </table></div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <textarea id="txtaDescription" name="txtaDescription" class="form-control" rows="7"><?php if(!empty($strDescription)) echo $strDescription; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-info-circle"></i> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-summary')?></h3></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithouttax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalNoTax" class="form-control required currency" id="subTotalNoTax" value="0" readonly /></div>
                        </div>
                    </div>                                        
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><input onClick="this.select();" type="text" name="txtTax" id="txtTax" class="form-control required qty" maxlength="3" value="0" /><label class="input-group-addon">%</label></div></div>
                    </div>
                    <p class="spacer">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6 tdTitle"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-totalwithtax')?></div>
                        <div class="col-sm-6 tdDesc"><div class="input-group"><label class="input-group-addon">Rp.</label><input type="text" name="subTotalTax" tabindex="-1" class="form-control required currency" id="subTotalTax" value="0" readonly /></div></div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" name="smtMakeQuotation" value="Make Quotation" class="btn btn-primary">
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'form-add')?>
        </button>
    </div>

    <input type="hidden" id="todayDate" name="todayDate" value="<?php if(!empty($strDate)) echo $strDate; else echo date('Y/m/d'); ?>" class="form-control required jwDateTime"/>
    <input type="hidden" id="totalItem" name="totalItem"/>
    <input type="hidden" id="totalItemBonus" name="totalItemBonus"/>    
</form></div>
