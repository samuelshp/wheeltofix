<?php // created by patricklipesik
if(!isset($io)){
	$io = 0;
	$spCurr = 'BASE';
	$spSign = TRUE;
	$spPass = TRUE;
}

// 1a. header versi export excel & print
if($io <= 1){
?>
<table>
	<tr>
		<td colspan="2" class="companyTitle">
			<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
			<b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
			<b><?=$arrCompanyInfo['strOwnerPhone']?></b>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>Laporan</td><td>: Neraca</td>
			</tr>
			<tr>
				<td>Periode</td><td>: <?=formatDate2($strPeriode.'-01','M Y')?></td>
			</tr>
			<tr>
				<td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td>
			</tr>
		</table>
	</tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
	<tr class="tbHeader">
		<th colspan="4">AKTIVA</th>
		<th colspan="4">PASIVA</th>
	</tr>
	<tr class="tbHeader">
		<th>GRUP</th>
		<th>DESKRIPSI</th>
		<th>BULAN LALU</th>
		<th>BULAN INI</th>
		<th>GRUP</th>
		<th>DESKRIPSI</th>
		<th>BULAN LALU</th>
		<th>BULAN INI</th>
	</tr>
<?php
}
// 1a. end

// 1b. header versi default menu
elseif($io >= 2){
?>
<div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">Periode</div>
				<div class="col-md-9">: <?=formatDate2($strPeriode.'-01','M Y')?></div>
				<div class="col-md-3">Hari ini</div>
				<div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
			</div>
		</div>
	</div>
</div>
<p class="spacer">&nbsp;</p>
<div class="table-responsive">
	<table class="table table-bordered table-condensed table-hover">
		<thead>
			<tr>
				<th colspan="4">AKTIVA</th>
				<th colspan="4">PASIVA</th>
			</tr>
			<tr>
				<th>GRUP</th>
				<th>DESKRIPSI</th>
				<th>BULAN LALU</th>
				<th>BULAN INI</th>
				<th>GRUP</th>
				<th>DESKRIPSI</th>
				<th>BULAN LALU</th>
				<th>BULAN INI</th>
			</tr>
		</thead>
		<tbody>
<?php
}
// 1b. end

// 2. isi table laporan
$sumTotal1 = 0;
$sumTotal2 = 0;
$sumTotal3 = 0;
$sumTotal4 = 0;
if(!empty($arrItems)){
	$i = 0;
	foreach($arrItems as $e){
		$namagroupkiri = '';
		if($groupkiri != $e['vcNamaGroupKiri'])
			$namagroupkiri = $e['vcNamaGroupKiri'];
		
		$lalukiri = '';
		$kinikiri = '';
		if(!empty($e['vcNamaAccountKiri']))
		{
			$lalukiri = setPrice($e['decLaluKiri'],$spCurr,$spSign,$spPass);
			$kinikiri = setPrice($e['decKiniKiri'],$spCurr,$spSign,$spPass);
		}
		
		$namagroupkanan = '';
		if($groupkanan != $e['vcNamaGroupKanan'])
			$namagroupkanan = $e['vcNamaGroupKanan'];
		
		$lalukanan = '';
		$kinikanan = '';
		if(!empty($e['vcNamaAccountKanan']))
		{
			$lalukanan = setPrice($e['decLaluKanan'],$spCurr,$spSign,$spPass);
			$kinikanan = setPrice($e['decKiniKanan'],$spCurr,$spSign,$spPass);
		}
?>
	<tr>
		<td><?=$e['vcNamaGroupKiri']?></td>
		<td><?=$e['vcNamaAccountKiri']?></td>
		<td><?=$lalukiri?></td>
		<td><?=$kinikiri?></td>
		<td><?=$e['vcNamaGroupKanan']?></td>
		<td><?=$e['vcNamaAccountKanan']?></td>
		<td><?=$lalukanan?></td>
		<td><?=$kinikanan?></td>
	</tr>
<?php
		$groupkiri  = $e['vcNamaGroupKiri'];
		$groupkanan = $e['vcNamaGroupKanan'];
		$sumTotal1 += $e['decLaluKiri'];
		$sumTotal2 += $e['decKiniKiri'];
		$sumTotal3 += $e['decLaluKanan'];
		$sumTotal4 += $e['decKiniKanan'];
	}
?>
	<tr>
		<td colspan="2" style="text-align:right;">TOTAL ASET</td>
		<td><?=setPrice($sumTotal1,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal2,$spCurr,$spSign,$spPass)?></td>
		<td colspan="2" style="text-align:right;">TOTAL KEWAJIBAN & EKUITAS</td>
		<td><?=setPrice($sumTotal3,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal4,$spCurr,$spSign,$spPass)?></td>
	</tr>
	<tr>
		<td colspan="6" style="text-align:right;">SELISIH BULAN LALU</td>
		<td colspan="2"><?=setPrice(($sumTotal1-$sumTotal3),$spCurr,$spSign,$spPass)?></td>
	</tr>
	<tr>
		<td colspan="6" style="text-align:right;">SELISIH BULAN INI</td>
		<td colspan="2"><?=setPrice(($sumTotal2-$sumTotal4),$spCurr,$spSign,$spPass)?></td>
	</tr>
<?php
}
else {
?>
	<tr>
		<td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td>
	</tr>
<?php
}
// 2. end

// 3. penutup table (yang berbeda untuk masing-masing versi)
if($io <= 1){
?>
</table>
<?php
} elseif($io >= 2){
?>
		</tbody>
	</table>
</div>
<?php
}
?>