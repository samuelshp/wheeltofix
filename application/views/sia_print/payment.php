<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php 
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($dataPrint) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($dataPrint)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-payment')?></h1></td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></td>
				<th><?=$dataHeader['pahe_code']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2" class="companyTitle">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=formatDate2($dataHeader['pahe_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number">No.</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')?></th>
        <th class="title2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmanname')?></th>
		<th class="title2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php
	for($j = ($i * $intItemPerPage); $j < count($dataPrint) && $j < (($i + 1) * $intItemPerPage); $j++) if($j < count($dataPrint)):
        $intTotalPriceSoFar += $dataPrint[$j]['invo_grandtotal']; ?>  
    <tr>
        <td class="number"><b><?=$j + 1?></b></td>
        <td class="code"><b><?=$dataPrint[$j]['paym_code']?></b></td>
        <td class="title2"><b><?=$dataPrint[$j]['cust_name']?> | <?=$dataPrint[$j]['invo_code']?></b></td>
        <td class="title2"><b><?=$dataPrint[$j]['sals_name']?></b></td>
        <td class="title2"><?php 
	    if($dataPrint[$j]['paym_type']=='4') echo 'Tunai';
	    else if($dataPrint[$j]['paym_type']=='6') echo 
	    	'BG | '.
	    	$dataPrint[$j]['paym_bank_id'].' | '.
	    	$dataPrint[$j]['paym_bg_number'].' | 
	    	JT: '.formatDate2($dataPrint[$j]['paym_date'],'d F Y'); 
    	if(!empty($dataPrint[$j]['paym_description'])) echo ' | '.$dataPrint[$j]['paym_description']; ?>  
    	</td>
        <td class="subTotal"><?=setPrice($dataPrint[$j]['invo_grandtotal'])?></td>
    </tr><?php 
	endif;

	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="6">&nbsp;</td></tr><?php
	endfor;

	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
		<td colspan="4">
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Piutang</th><th>Kasir</th><th>&nbsp;</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td colspan="2">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
			</table>
		</td>
	</tr><?php
    else: ?>
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor;