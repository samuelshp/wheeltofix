<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Detail Retur Penjualan Per Salesman By Customer</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td><td>Kode</td><td style="width: 300px">: <?=$strSalesmanCode?></td></tr>
            <tr><td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td><td>Nama</td><td style="width: 300px">: <?=$strSalesmanName?></td></tr>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>No</th>
        <th>No Trans</th>
        <th>Tanggal</th>
        <th>Umur</th>
        <th>Kode</th>
        <th>Nama</th>
        <th>GrandTotal</th>
        <!-- <th>PPn Exc</th>
        <th>GrandNetto</th> -->
    </tr>
    <?php
    if(!empty($arrItems)):
        foreach($arrItems as $e):
			$date = formatDate2($e['cdate'],'m/d/Y');
			$grandtotal = $e['invr_grandtotal']; $tax = $e['invr_taxin']; $netto = $e['invr_grandtotal']-$e['invr_taxin'];
			if(isset($print_mode) && $print_mode == 1){
				$date = formatDate2($e['cdate'],'d/m/Y');
				$grandtotal = setPrice($grandtotal); $tax = setPrice($tax); $netto = setPrice($netto);
			}
	?>
            <tr>
                <td><?=$e['nourut']?></td>
                <td><?=$e['invr_code']?></td>
                <td><?=$date?></td>
                <td></td>
                <td><?=$e['cust_code']?></td>
                <td><?=$e['cust_name']?></td>
                <td><?=$grandtotal?></td>
                <!-- <td><?=$tax?></td>
                <td><?=$netto?></td> -->
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>