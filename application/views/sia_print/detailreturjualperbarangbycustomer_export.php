<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Detail Retur Penjualan Per Barang By Customer</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td><td>Kode</td><td style="width: 300px">: <?=$strProductCode?></td></tr>
            <tr><td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td><td>Nama</td><td style="width: 300px">: <?=$strProductName?></td></tr>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>No</th>
        <th>No Trans</th>
        <th>Tanggal</th>
        <th>Nama</th>
        <th>QtyB</th>
        <th>Sat</th>
        <th>QtyT</th>
        <th>Sat</th>
        <th>QtyK</th>
        <th>Sat</th>
        <th>@Harga</th>
        <th>Disc%</th>
        <th>@NetHarga</th>
    </tr>
    <?php
    if(!empty($arrItems)):
        foreach($arrItems as $e):
			$date = formatDate2($e['cdate'],'m/d/Y');
			$price = $e['inri_price']; $netprice = $e['netprice'];
			if(isset($print_mode) && $print_mode == 1){
				$date = formatDate2($e['cdate'],'d/m/Y');
				$price = setPrice($price); $netprice = setPrice($netprice);
			}
	?>
            <tr>
                <td><?=$e['nourut']?></td>
                <td><?=$e['invr_code']?></td>
                <td><?=$date?></td>
                <td><?=$e['cust_name']?></td>
                <td><?=$e['inri_quantity1']?></td>
                <td><?=$e['prod_unit1']?></td>
                <td><?=$e['inri_quantity2']?></td>
                <td><?=$e['prod_unit2']?></td>
                <td><?=$e['inri_quantity3']?></td>
                <td><?=$e['prod_unit3']?></td>
                <td><?=$price?></td>
                <td><?=$e['inri_discount1']?>+<?=$e['inri_discount2']?>+<?=$e['inri_discount3']?></td>
                <td><?=$netprice?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>