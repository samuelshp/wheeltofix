<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/inventorybarcodeprint3cmx3.css')?>" />
<script type="text/javascript">
setTimeout(function() {
	window.print();
}, 100);
</script>
<div id="barcodePrint"><div class="container">
<ul><?php
$intBarcodeNumber = generateBarcodeNumber('',$arrProduct['id']);
$intPrice = setPrice($arrProduct['prod_price'],$arrProduct['prod_currency']);
$strProductName = formatProductName('',$arrProduct['prod_title']);
for($i = 0; $i < $intPrintNumber; $i++): ?>  
	<li class="barcodeTag"><table cellspacing="0px" border="0px">
		<tr>
			<td class="productName"><?=$strProductName?></td>
		</tr>
		<tr>
			<td class="barcodeID"><?='*'.$intBarcodeNumber.'*'?></td>
		</tr>
		<tr>
			<td class="barcodeNumber"><?=$intBarcodeNumber?> <?=$intPrice?></td>
		</tr>
	</table></li><?php
endfor; ?>  
</ul>
<p style="clear:both;">&nbsp;</p>
</div></div>