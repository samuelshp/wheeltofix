<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php 
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($dataPrint) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($dataPrint)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1>Daftar Faktur dan Tagihan</h1></td>
		    </tr>
            <tr>
                <td>
                    <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-salesmanname').": ".$txtSalesman?>
                </td>
            </tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2" class="companyTitle">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=formatDate2($txtDate,'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number">No</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-customername')?></th>
		<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th>
		<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-duedate')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
		<th style="border-left:#000 1px solid; min-width:4cm;"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></th>
	</tr><?php
	for($j = ($i * $intItemPerPage); $j < count($dataPrint) && $j < (($i + 1) * $intItemPerPage); $j++):
		?>
        
        <?php if($j<count($dataPrint)){
        $intTotalPriceSoFar += $dataPrint[$j]['invo_grandtotal'];?>
        <tr class="border-bottom">
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$dataPrint[$j]['invo_code']?></b></td>
            <td class="title"><b><?=$dataPrint[$j]['cust_name']?></b></td>
            <td><b><?=formatDate2($dataPrint[$j]['invo_arrive'],'d F Y')?></b></td>
            <td><b><?=formatDate2($dataPrint[$j]['invo_jatuhtempo'],'d F Y')?></td>
            <td class="subTotal"><?=setPrice($dataPrint[$j]['invo_grandtotal'])?></td>
            <td style="border-left:#000 1px solid;">&nbsp;</td>
        </tr>
    <?php } ?>


        <?php
	endfor;
	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr class="border-bottom">
		<td colspan="6">&nbsp;</td>
		<td style="border-left:#000 1px solid;">&nbsp;</td>
	</tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader">
		<td colspan="5">
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Mengetahui</th><th>Penagihan</th><th>&nbsp;</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td colspan="2">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
			</table>
		</td>
	</tr><?php
    else: ?>
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor;