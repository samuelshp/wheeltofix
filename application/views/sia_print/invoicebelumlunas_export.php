<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Laporan Faktur Jual Belum Lunas</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d-M-Y')?> s/d <?=formatDate2($strEnd,'d-M-Y')?></td>
            <tr><td>Hari ini</td><td>: <?=date('d-M-Y H:i')?></td>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>NO</th>
        <th>NO TRANS</th>
        <th>TRANS</th>
        <th>TGL TRANS</th>
        <th>TGL KIRIM</th>
        <th>TOP</th>
        <th>TGL JATUH TEMPO</th>
        <th>OD</th>
        <th>KODE SLS</th>
        <th>NAMA SALESMAN </th>
        <th>NOMINAL</th>
    </tr>
    <?php
    $i = 1;
    if(!empty($arrInvoice)):
        foreach($arrInvoice as $e): ?>
            <tr>
                <td><?=$i?></td>
                <td><?=$e['invo_code']?></td>
                <td><?=formatDate2($e['invoice_date'],'d/m/Y')?></td>
                <td><?php if($e['deli_time'] == '1970-01-01 07:00:00') echo ''; else echo formatDate2($e['deli_time'],'d/m/Y'); ?></td>
                <td><?php echo floor(abs(strtotime(formatDate2($e['invo_jatuhtempo'],'Y-m-d')) - strtotime(formatDate2($e['invoice_date'],'Y-m-d')))/(60*60*24)) ?></td>
                <td><?=formatDate2($e['invo_jatuhtempo'],'d/m/Y')?></td>
                <td></td>
                <td><?=$e['sals_code']?></td>
                <td><?=$e['sals_name']?></td>
                <td><?=$e['cust_name']?><td>
                <td><?=setPrice($e['invo_grandtotal'], 'BASE', $bolWithSign, $bolBypass)?></td>
            </tr>
        <?php
        $i++;
        endforeach;
    else: ?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>