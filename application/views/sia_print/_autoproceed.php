<script type="text/javascript">
<?php if(ENVIRONMENT == 'production'): ?>
if ('matchMedia' in window) {
	// For Chrome
	var shown = 0;
    window.matchMedia('print').addListener(function (media) {
    	shown++;
    	if(shown > 1) {
    		window.location.href = "<?=$strAutoProceedURL?>";
    	}
    });
} else {
	// For Firefox
    window.onafterprint = function () {
    	//do before-printing stuff
    	window.location.href = "<?=$strAutoProceedURL?>";
    }
}
<?php endif; ?>
</script>