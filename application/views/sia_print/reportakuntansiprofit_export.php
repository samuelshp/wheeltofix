<!-- created by patricklipesik -->
<table>
	<tr>
		<td colspan="2" class="companyTitle">
			<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
			<b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
			<b><?=$arrCompanyInfo['strOwnerPhone']?></b>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>Laporan</td><td>: Profit Per Barang</td>
			</tr>
			<tr>
				<td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td>
			</tr>
			<tr>
				<td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td>
			</tr>
		</table>
	</tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
	<tr class="tbHeader">
		<th>No</th>
		<th>Produk</th>
		<th>Penjualan</th>
		<th>HPP</th>
		<th>Selisih</th>
	</tr>
	<?php
	$sumGrandTotal1 = 0;
	$sumGrandTotal2 = 0;
	$sumGrandTotal3 = 0;
	if(!empty($arrItems)):
		foreach($arrItems as $e):
			$total1 = $e['subtotal'];
			$total2 = $e['hpp'];
			$total3 = $e['subtotal'] - $e['hpp'];
			if(isset($print_mode) && $print_mode == 1){
				$total1 = setPrice($total1);
				$total2 = setPrice($total2);
				$total3 = setPrice($total3);
			}
			$sumGrandTotal1 += $e['subtotal'];
			$sumGrandTotal2 += $e['hpp'];
			$sumGrandTotal3 += $e['subtotal'] - $e['hpp'];
	?>
			<tr>
				<td><?=$e['nourut']?></td>
				<td><?=$e['produk']?></td>
				<td><?=$total1?></td>
				<td><?=$total2?></td>
				<td><?=$total3?></td>
			</tr>
		<?php endforeach;
	else:?>
		<tr><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
	endif;
	
	if(!empty($sumGrandTotal)):
		if(isset($print_mode) && $print_mode == 1)
		{
			$sumGrandTotal1 = setPrice($sumGrandTotal1);
			$sumGrandTotal2 = setPrice($sumGrandTotal2);
			$sumGrandTotal3 = setPrice($sumGrandTotal3);
		}
	?>
	<tr class="tbHeader">
		<th colspan="2" style="text-align:right;">Grand Total</th>
		<th><?=$sumGrandTotal1?></th>
		<th><?=$sumGrandTotal2?></th>
		<th><?=$sumGrandTotal3?></th>
	</tr><?php
	endif;?>
</table>