<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrInvoiceReturItem)+count($arrInvoiceReturItemBonus)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrInvoiceReturItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturn')?></h1></td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kepada')?></td>
				<th><?=$arrInvoiceReturData['cust_name']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceReturData['cust_address']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceReturData['cust_phone']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
				<th><?=$arrInvoiceReturData['invr_code']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=formatDate2($arrInvoiceReturData['invr_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top">
	    <td colspan="7">
	        <?=$dataPrint['sals_name'].'('.$dataPrint['sals_code'].')'?> | 
            <?=$dataPrint['ware_name']?>
	    </td>
	</tr>
	<tr class="titleList border-top border-bottom">
		<th class="number">No.</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="discount"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php
        $intItemCount=0;
        if(!empty($arrInvoiceReturItem))$intItemCount+=count($arrInvoiceReturItem);
        if(!empty($arrInvoiceReturItemBonus))$intItemCount+=count($arrInvoiceReturItemBonus);
	for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):?>
        <?php if($j<count($arrInvoiceReturItem)){
		$intTotalPriceSoFar += $arrInvoiceReturItem[$j]['inri_subtotal']; ?>  
	<tr>
		<td class="number"><b><?=$j + 1?></b></td>
        <td class="code"><b><?=$arrInvoiceReturItem[$j]['prod_code']?></b></td>
		<td class="title"><b><?=substr($arrInvoiceReturItem[$j]['inri_description'],0,$intProductTitleLength)?></b></td>
		<td class="quantity">
		    <?=$arrInvoiceReturItem[$j]['inri_quantity1'].' '.formatUnitName($arrInvoiceReturItem[$i]['inri_unit1'])?> + 
		    <?=$arrInvoiceReturItem[$j]['inri_quantity2'].' '.formatUnitName($arrInvoiceReturItem[$i]['inri_unit2'])?> + 
		    <?=$arrInvoiceReturItem[$j]['inri_quantity3'].' '.formatUnitName($arrInvoiceReturItem[$i]['inri_unit3'])?>
        </td>
		<td class="price"><?=setPrice($arrInvoiceReturItem[$j]['inri_price'])?></td>
		<td class="discount">
		    <?=$arrInvoiceReturItem[$j]['inri_discount1']?>% + 
		    <?=$arrInvoiceReturItem[$j]['inri_discount2']?>% + 
		    <?=$arrInvoiceReturItem[$j]['inri_discount3']?>%
        </td>
		<td class="subTotal"><?=setPrice($arrInvoiceReturItem[$j]['inri_subtotal'])?></td>
	</tr>
        <?php }else{ ?>
            <tr>
                <td class="number"><b><?=$j + 1?></b></td>
                <td class="code"><b><?=$arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['prod_code']?></b></td>
                <td class="title"><b><?=substr($arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_description'],0,$intProductTitleLength)?> (*)</b></td>
                <td class="quantity">
                    <?=$arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_quantity1'].' '.formatUnitName($arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_unit1'])?> +
                    <?=$arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_quantity2'].' '.formatUnitName($arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_unit2'])?> +
                    <?=$arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_quantity3'].' '.formatUnitName($arrInvoiceReturItemBonus[$j-count($arrInvoiceReturItem)]['inri_unit3'])?>
                </td>
                <td class="price"><?=setPrice(0)?></td>
                <td class="discount">
                    0% +
                    0% +
                    0%</td>
                <td class="subTotal"><?=setPrice(0)?></td>
            </tr>
        <?php } ?>
    <?php
	endfor;
    if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="7">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="5">
	        <table cellspacing="0" border="0">
                <tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrInvoiceReturData['invr_description']?></td></tr>
                <tr class="signHeader">
                    <th>Penerima</th>
                    <th>Penjualan</th>
                    <th>&nbsp;</th>
                </tr>
                <tr class="signBody">
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                </tr>
                <tr class="signFooter">
                    <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
                </tr>
	        </table>
	    </td>
	    <td colspan="2">
	        <table cellspacing="0" border="0">
                <tr>
                    <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
		            <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
                </tr>
	        </table>
	    </td> 
	</tr><?php
    else: ?>
    <tr class="border-top">
        <td colspan="5" class="next-page">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>