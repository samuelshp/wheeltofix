<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint.css')?>"  media="all" />
	<h1 class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'saleorder-saleorderstatus')?></h1>
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td colspan="2"><table cellspacing="0" border="0">
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></td>
				<th><?=formatDate2($arrSaleOrderData['salo_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?></td>
				<th><?=$arrSaleOrderData['cust_name']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-address')?></td>
				<th><?=$arrSaleOrderData['cust_address']?>, <?=$arrSaleOrderData['cust_city']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-phone')?></td>
				<th><?=$arrSaleOrderData['cust_phone']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-warehousename')?></td>
				<th><?=$arrSaleOrderData['ware_name']?></th>
			</tr>
		</table></td>
	</tr></table>
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList">
		<th><?=loadLanguage('display',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th><?=loadLanguage('display',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th><?=loadLanguage('display',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php
$i = 0;
$intTotal = 0;
if(!empty($arrSaleOrderItem)) foreach($arrSaleOrderItem as $arrItemEach): ?>  
	<tr>
		<td class="quantity2"><?=formatProductUnit($arrItemEach['salo_product_id'],$arrItemEach['salo_quantity'])?> / <?=formatProductUnit($arrItemEach['salo_product_id'],$arrItemEach['salo_originalquantity'])?></td>
		<td><b><?=$arrItemEach['prod_title']?></b></td>
		<td class="price"><?=setPrice($arrItemEach['salo_price'])?></td>
		<td class="subTotal"><?=setPrice($arrItemEach['salo_subtotal'])?></td>
	</tr><?php
	$i++;
endforeach; ?>  
	<tr class="subTotalHeader">
		<td colspan="3" class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
		<td class="subTotal"><?=setPrice($intSaleOrderTotal)?></td>
	</tr>
	</table>
	<table cellspacing="0" border="0" id="invoiceFooter">
		<tr>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?></th>
			<th class="sign"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-receiver')?></th>
			<th class="sign"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-deliverer')?></th>
			<th class="sign"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-sign')?></th>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
	</table>
	<h4 style="text-align:center;"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-thankyou')?></h4>