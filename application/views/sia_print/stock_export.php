<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>KODE</th>
        <th>BARCODE</th>
        <th>KATEGORI</th>
        <th>MERK</th>
        <th>NAMA</th>
        <th>Q1</th>        
        <th>Q2</th>
        <th>Q3</th>
        <th>PND1</th>
        <th>PND2</th>
        <th>P3</th>
        <th>TOT1</th>
        <th>TOT2</th>
        <th>TOT3</th>
    </tr>
    <?php
    if(!empty($arrStock)):
        foreach($arrStock as $e): ?>
            <tr>
                <td><?=$e['prod_code']?></td>
                <td><?=$e['prod_barcode']?></td>
                <td><?=$e['proc_title']?></td>
                <td><?=$e['prob_title']?></td>
                <td><?=$e['prod_title']?></td>
                <td><?=$e['qty1']?></td>
                <td><?=$e['qty2']?></td>
                <td><?=$e['qty3']?></td>
                <td><?=$e['pending1']?></td>
                <td><?=$e['pending2']?></td>
                <td><?=$e['pending3']?></td>
				<td><?=$e['qty1']+$e['pending1']?></td>
				<td><?=$e['qty2']+$e['pending2']?></td>
				<td><?=$e['qty3']+$e['pending3']?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="14"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>