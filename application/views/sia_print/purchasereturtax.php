<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrPurchaseItem)+count($arrPurchaseBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrPurchaseItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
    <div class="container">
    <!-- HEADER -->
    <table cellspacing="0" border="0" id="invoiceHeader"><tr>
            <td class="l"><table cellspacing="0" border="0">
                    <tr>
                        <td colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-purchasereturn')?></h1></td>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplier')?></td>
                        <th><?=$arrPurchaseData['supp_name']?></th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <th><?=$arrPurchaseData['supp_address']?></th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <th><?=$arrPurchaseData['supp_phone']?></th>
                    </tr>
                </table></td>
            <td><table cellspacing="0" border="0">
                    <tr>
                        <td colspan="2">
                            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
                            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
                            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
                        <th><?=$arrPurchaseData['prcr_code']?></th>
                    </tr>
                    <tr>
                        <td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
                        <th><?=formatDate2($arrPurchaseData['prcr_date'],'d F Y')?></th>
                    </tr>
                    <tr>
                        <td>Halaman</td>
                        <th><?=($i + 1)?> dari <?=$intTotalPage?></th>
                    </tr>
                </table></td>
        </tr>
    <tr>
        <td>tgl yang dimasukkan manual : <?=$dateTax?></td>
        <td>kode yang dimasukkan manual : <?=$kodeTax?></td>
        <td>supp_pajak : <?=$arrPurchaseData['supp_pajak']?></td>
        <td>supp_npwp : <?=$arrPurchaseData['supp_npwp']?></td>
        <td>supp_pkp : <?=$arrPurchaseData['supp_pkp']?></td>
        <td>supp_tax_address : <?=$arrPurchaseData['supp_tax_address']?></td>
        <td>supp_tax_city : <?=$arrPurchaseData['supp_tax_city']?></td>
        <td>supp_tax_region : <?=$arrPurchaseData['supp_tax_region']?></td>
        <td>supp_tax_zipcode : <?=$arrPurchaseData['supp_tax_zipcode']?></td>
    </tr></table>
    <!-- ITEMS -->
    <table cellspacing="0" border="0" id="invoiceItemList">
        <tr class="titleList border-top">
            <td colspan="7">
                <?=$arrPurchaseData['ware_name']?>
            </td>
        </tr>
        <tr class="titleList border-top border-bottom">
            <th class="number">No.</th>
            <th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
            <th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
            <th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
            <th class="discount"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
            <th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
        </tr<?php
        $intItemCount=0;
        if(!empty($arrPurchaseItem))$intItemCount+=count($arrPurchaseItem);
        if(!empty($arrPurchaseBonusItem))$intItemCount+=count($arrPurchaseBonusItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):?>
            <?php if($j<count($arrPurchaseItem)){
                $intTotalPriceSoFar += $arrPurchaseItem[$j]['prri_subtotal']; ?>
                <tr>
                    <td class="number"><b><?=$j + 1?></b></td>
                    <td class="code"><b><?=$arrPurchaseItem[$j]['prod_code']?></b></td>
                    <td class="title"><b><?=substr($arrPurchaseItem[$j]['strName'],0,$intProductTitleLength)?></b></td>
                    <td class="quantity">
                        <?=$arrPurchaseItem[$j]['prri_quantity1'].' '.formatUnitName($arrPurchaseItem[$i]['prri_unit1'])?> +
                        <?=$arrPurchaseItem[$j]['prri_quantity2'].' '.formatUnitName($arrPurchaseItem[$i]['prri_unit2'])?> +
                        <?=$arrPurchaseItem[$j]['prri_quantity3'].' '.formatUnitName($arrPurchaseItem[$i]['prri_unit3'])?>
                    </td>
                    <td class="price"><?=setPrice($arrPurchaseItem[$j]['prri_price'])?></td>
                    <td class="discount">
                        <?=$arrPurchaseItem[$j]['prri_discount1']?>% +
                        <?=$arrPurchaseItem[$j]['prri_discount2']?>% +
                        <?=$arrPurchaseItem[$j]['prri_discount3']?>%
                    </td>
                    <td class="subTotal"><?=setPrice($arrPurchaseItem[$j]['prri_subtotal'])?></td>
                </tr>
            <?php }else{ ?>
                <tr>
                    <td class="number"><b><?=$j + 1?></b></td>
                    <td class="code"><b><?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prod_code']?></b></td>
                    <td class="title"><b><?=substr($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['strName'],0,$intProductTitleLength)?> (*)</b></td>
                    <td class="quantity">
                        <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_quantity1'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_unit1'])?> +
                        <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_quantity2'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_unit2'])?> +
                        <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_quantity3'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prri_unit3'])?>
                    </td>
                    <td class="price"><?=setPrice(0)?></td>
                    <td class="discount">
                        0% +
                        0% +
                        0%</td>
                    <td class="subTotal"><?=setPrice(0)?></td>
                </tr>
            <?php } ?><?php
        endfor;
        if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>
            <tr><td colspan="7">&nbsp;</td></tr><?php
        endfor;
        if($i == $intTotalPage - 1): ?>
            <!-- FOOTER -->
            <tr class="subTotalHeader border-top">
            <td colspan="5">
                <table cellspacing="0" border="0">
                    <tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrPurchaseData['prcr_description']?></td></tr>
                    <tr class="signHeader">
                        <th>Penerima</th>
                        <th>Pembelian</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr class="signBody">
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    <tr class="signFooter">
                        <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td colspan="2">
                <table cellspacing="0" border="0">
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
                        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
                    </tr>
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount').' '.$arrPurchaseData['prcr_discount'].'%'?></td>
                        <td class="subTotal"><?=$arrPurchaseData['subtotal_discounted']?></td>
                    </tr>
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
                        <td class="subTotal"><?=$arrPurchaseData['prcr_tax']?>%</td>
                    </tr>
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></td>
                        <td class="subTotal"><?=setPrice($intPurchaseGrandTotal)?></td>
                    </tr>
                </table>
            </td>
            </tr><?php
        else: ?>
            <tr class="border-top">
            <td colspan="5" class="next-page">Lanjut Ke Halaman Berikut</td>
            <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
            <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
            </tr><?php
        endif; ?>
    </table>
    </div><?php
    if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>