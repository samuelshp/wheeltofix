<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$strAutoProceedURL = site_url('invoice/index/view/'.$intInvoiceID, NULL, FALSE);
require_once('_autoproceed.php');
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrInvoiceItem)+count($arrInvoiceBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrInvoiceItem)) for($i = 0; $i < $intTotalPage; $i++): ?>  
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
			<tr>
			    <th colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')?></h1></th>
			</tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kepada')?></td>
				<th><?=$arrInvoiceData['cust_name']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceData['cust_address']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceData['cust_phone']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
			<tr>
			    <td colspan="2">
			        <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h4><?=$arrCompanyInfo['strOwnerAddress']?></h4>
		            <h4><?=$arrCompanyInfo['strOwnerPhone']?></h4>
			    </td>
			</tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
				<th><?=generateTransactionCode($arrInvoiceData['invo_date'],$arrInvoiceData['id'],'invoice')?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></td>
				<th><?=formatDate2($arrInvoiceData['invo_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top">
        <td colspan="7">
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?>: <?=formatDate2($dataPrint['invo_jatuhtempo'],'d F Y')?> | 
            <?=$dataPrint['sals_name'].'('.$dataPrint['sals_code'].')'?> | 
            <?=$dataPrint['ware_name']?> | 
            <?=$dataPrint['kota_title'].' / '.$dataPrint['kcmt_title']?> | 
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-type')?>: <?php
                if($dataPrint['invo_jualkanvas'] == 0) echo 'IVC'; else echo 'IVR'; ?>  
        </td>
	</tr>
	<tr class="titleList border-top border-bottom">
		<th class="number"></th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="discount"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php

        $intItemCount=0;
        if(!empty($arrInvoiceItem))$intItemCount+=count($arrInvoiceItem);
        if(!empty($arrInvoiceBonusItem))$intItemCount+=count($arrInvoiceBonusItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
		?>
        <?php if($j<count($arrInvoiceItem)){
        $intTotalPriceSoFar += $arrInvoiceItem[$j]['invi_subtotal'];?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrInvoiceItem[$j]['prod_code']?></b></td>
            <td class="title2"><b><?=substr($arrInvoiceItem[$j]['invi_description'],0,$intProductTitleLength)?></b></td>
            <td class="quantity">
                <?=formatProductUnit($arrInvoiceItem[$j]['invi_product_id'], array('intQty1' => $arrInvoiceItem[$j]['invi_quantity1'], 'intQty2' => $arrInvoiceItem[$j]['invi_quantity2'], 'intQty3' => $arrInvoiceItem[$j]['invi_quantity3']), 'separated')?>
            </td>
            <td class="price"><?=setPrice($arrInvoiceItem[$j]['invi_price'])?></td>
            <td class="discount">
                <?=$arrInvoiceItem[$j]['invi_discount1']?>% +
                <?=$arrInvoiceItem[$j]['invi_discount2']?>% +
                <?=$arrInvoiceItem[$j]['invi_discount3']?>%
            </td>
            <td class="subTotal"><?=setPrice($arrInvoiceItem[$j]['invi_subtotal'])?></td>
        </tr>
        <?php }else{ ?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['prod_code']?></b></td>
            <td class="title"><b><?=substr($arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_description'],0,$intProductTitleLength)?></b></td>
            <td class="quantity">
                <?=formatProductUnit($arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_product_id'], array('intQty1' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity1'], 'intQty2' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity2'], 'intQty3' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity3']), 'separated')?>
            </td>
            <td class="price"><?=setPrice(0)?></td>
            <td class="discount">
                0% +
                0% +
                0%
            </td>
            <td class="subTotal"><?=setPrice(0)?></td>
        </tr>
        <?php } ?>
    <?php
	endfor;
    if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="7">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="5">
			<table cellspacing="0" border="0">
				<tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrInvoiceData['invo_description']?></td></tr>
				<tr class="signHeader2">
					<td rowspan="3" style="width:50%; font-size:0.8em;">
					    Pembayaran dengan BG dianggap lunas bila BG cair.<br />
					    Pelunasan dianggap SAH apabila toko memegang faktur ASLI.<br />
					    Penagihan hanya dilakukan dengan faktur ASLI.<br />
					    Retur barang tidak diperbolehkan tanpa persetujuan.<br />
					    Komplain kekurangan tidak dilayani setelah faktur ditandatangani.
					</td>
					<th width="25%">Pengirim</th>
					<th width="25%">Penerima</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td>
				</tr>
			</table>
		</td>
	    <td colspan="2">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount').' '.setPrice($arrInvoiceData['invo_discount'])?></td>
					<td class="subTotal"><?=setPrice($arrInvoiceData['subtotal_discounted'])?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
		            <td class="subTotal"><?=$arrInvoiceData['invo_tax']?>%</td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></td>
		            <td class="subTotal"><?=setPrice($intInvoiceGrandTotal, 'BASE', TRUE, FALSE, TRUE)?></td>
				</tr>
			</table>
		</td>
	</tr><?php
    else: ?>  
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
	endif; ?>  
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>  
	<?php
        if(!empty($arrPotongan)):
            foreach($arrPotongan as $e):
                echo "> ".$e['poto_item_qty']." ".$e['poto_name']." ,Potongan / Item => ".$e['poto_nominal']."<br>";
            endforeach;
        endif;
    ?>