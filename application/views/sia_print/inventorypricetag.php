<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/inventorypricetagprint.css')?>" />
<div id="priceTagPrint"><div class="container"><?php
for($i = 0; $i < $intPrintNumber; $i++): ?>  
	<div class="priceTag"><table cellspacing="0px" border="0px">
		<tr>
			<td class="productName"><?=formatProductName('',$arrProduct['prod_title'])?></td>
		</tr>
		<tr>
			<td class="barcodeNumber"><?=generateBarcodeNumber('',$arrProduct['id'])?></td>
		</tr>
		<tr>
			<td class="price"><span><?=setPrice($arrProduct['prod_price'],$arrProduct['prod_currency'])?></span></td>
		</tr>
		<tr>
			<td class="storeName"><?=$this->config->item('jw_website_name')?></td>
		</tr>
	</table></div><?php
endfor; ?>  
</div></div>