<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($arrDaftarPembayaranHutangItem) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrDaftarPembayaranHutangItem)) for($i = 0; $i < $intTotalPage; $i++):?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2">
		            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		        </td>
		    </tr>
			<tr>
		        <td colspan="2"><h2>Daftar Pembayaran Hutang</h2></td>
			</tr>				
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-ponumber')?></td>
				<th><?=$arrDaftarPembayaranHutang['dphu_code']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<td><?=formatDate2($arrDaftarPembayaranHutang['dphu_date'],'d F Y')?></td>
			</tr>			
			<tr>
				<td class="t">Supplier</td>
				<th><?=$arrDaftarPembayaranHutang['supp_name']?></th>
			</tr>
		</table></td>
		<!-- <td><table cellspacing="0" border="0">
		    <tr>		    	
				<td colspan="2">&nbsp;</td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kepada')?></td>
			</tr>
			<tr>
				<td><?=$arrPurchaseData['supp_name']?></td>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-suppliercp')?> <b><?=$arrPurchaseData['supp_cp_name']?></b></td>				
			</tr>
			<tr>		    	
				<td colspan="2">&nbsp;</td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></td>
			</tr>
			<tr>
				<td><?=nl2br($arrPurchaseData['prch_description'])?></td>
			</tr>
		</table></td> -->
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="title">No. PI</th>
		<th class="title">Pph</th>		
		<th class="price">Amount Total</th>
		<th class="price">Rencana Bayar</th>
	</tr><?php
	if(!empty($arrDaftarPembayaranHutangItem)):
	$intTotal = 0;
	$intRencanaBayar = 0;
    foreach($arrDaftarPembayaranHutangItem as $e):?>
    <tr style="text-align:center;">
        <td><?=$e['code']?></td>
        <td><?=$e['dummy_pph']?> %</td>
        <td class="price"><?=setPrice($e['total'])?></td>
        <td class="price"><?=setPrice($e['dphi_amount_rencanabayar'])?></td>        
    </tr>
    <?php 
    	$intTotal += $e['total']; 
    	$intRencanaBayar += $e['dphi_amount_rencanabayar']; 
    	endforeach; 
    ?>
        
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">		
		<td colspan="2"></td>			
		<td colspan="2">
			<table border="0">
				<tr>					
					<td class="subTotal"><?=setPrice($intTotal)?></td>
					<td class="subTotal"><?=setPrice($intRencanaBayar)?></td>
				</tr>
			</table>
		</td>		
	</tr>
	<tr class="subTotalHeader">
		<td>
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Dibuat oleh</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span><?=$arrLogin['adlg_name']?></span></td>
				</tr>
			</table>
		</td>
		<td>
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Disetujui</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
    else: ?>
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>