<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrDeliveryItem) / $intItemPerPage));
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrDeliveryItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
    <div class="container">
    <!-- HEADER -->
    <table cellspacing="0" border="0" id="invoiceHeader"><tr>
            <td class="l"><table cellspacing="0" border="0">
                    <tr>
                        <th colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivery')?></h1></th>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-deliverer')?></td>
                        <th><?=$arrDeliveryData['sals_code'].", ".$arrDeliveryData['sals_name']?></th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <th><?=$arrDeliveryData['ware_name']?></th>
                    </tr>
                </table></td>
            <td><table cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" class="companyTitle">
                            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
                            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
                            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
                        <th><?=$arrDeliveryData['deli_code']?></th>
                    </tr>
                    <tr>
                        <td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
                        <th><?=formatDate2($arrDeliveryData['deli_date'],'d F Y')?></th>
                    </tr>
                    <tr>
                        <td>Halaman</td>
                        <th><?=($i + 1)?> dari <?=$intTotalPage?></th>
                    </tr>
                </table></td>
        </tr></table>
    <!-- ITEMS -->
    <?php $flag = 0; ?>
    <table cellspacing="0" border="0" id="invoiceItemList">
        <tr class="titleList border-top border-bottom border-right">
            <th style="border-left:#000 1px solid;">No</th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <!-- <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></th> -->
            <th>
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-name')?> / 
                <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-address')?>
            </th>
            <th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
            <th style="width:1cm;">Benar</th>
            <th style="width:1cm;">Rubah</th>
            <th style="width:1cm;">Tunai</th>
            <th style="width:1cm;">Kredit</th>
            <th style="width:1cm;">Batal</th>
            <th style="width:1cm;">Pend</th>
        </tr><?php
        $intItemCount=0;
        if(!empty($arrDeliveryItem))$intItemCount+=count($arrDeliveryItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
            if($j<count($arrDeliveryItem)) {
            $intTotalPriceSoFar += $arrDeliveryItem[$j]['deli_subtotal']; ?>
        <tr class="border-right border-bottom">
            <td class="number" style="border-left:#000 1px solid;"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrDeliveryItem[$j]['invo_code']?></b></td>
            <!-- <td><?=formatDate2($arrDeliveryItem[$j]['invo_date'],'d F Y')?></td> -->
            <td class="title2" style="overflow-x:auto; overflow-y:hidden;">
                <b><?=$arrDeliveryItem[$j]['cust_name']?></b>
                (<?=$arrDeliveryItem[$j]['cust_address'].', '.$arrDeliveryItem[$j]['cust_city']?>)
            </td>
            <td class="subTotal"><b><?=setPrice($arrDeliveryItem[$j]['deli_subtotal'])?></b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr><?php 
            } 
        endfor;
        if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>
        <tr class="border-right border-bottom">
            <td style="border-left:#000 1px solid;">&nbsp;</td>
            <td>&nbsp;</td>
            <!-- <td>&nbsp;</td> -->
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr><?php
        endfor;
        if($i == $intTotalPage - 1): ?>
            <!-- FOOTER -->
        <tr class="subTotalHeader">
            <td colspan="7">
                <table cellspacing="0" border="0">
                    <tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrDeliveryData['deli_description']?></td></tr>
                    <tr class="signHeader">
                        <th>Mengetahui</th><th>Logistik</th><th>&nbsp;</th>
                    </tr>
                    <tr class="signBody">
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    <tr class="signFooter">
                        <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td colspan="3">
                <table cellspacing="0" border="0">
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
                        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
                    </tr>
                </table>
            </td>
        </tr><?php
        else: ?>
        <tr class="border-top">
            <td colspan="8">Lanjut Ke Halaman Berikut</td>
            <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
            <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
        </tr><?php
        endif; ?>
    </table>
    </div><?php
    if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>
<div class="pageBreak">&nbsp;</div>
<?php
$intTotalPage = (int) ceil((count($arrItem) / $intItemPerPage));
if(!empty($arrItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
    <div class="container">
    <!-- HEADER -->
    <table cellspacing="0" border="0" id="invoiceHeader"><tr>
            <td class="l"><table cellspacing="0" border="0">
                    <tr>
                        <th colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivery')?></h1></th>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-deliverer')?></td>
                        <th><?=$arrDeliveryData['sals_name']?></th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <th><?=$arrDeliveryData['ware_name']?></th>
                    </tr>
                </table></td>
            <td><table cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" class="companyTitle">
                            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
                            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
                            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
                        <th><?=$arrDeliveryData['deli_code']?></th>
                    </tr>
                    <tr>
                        <td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
                        <th><?=formatDate2($arrDeliveryData['deli_date'],'d F Y')?></th>
                    </tr>
                    <tr>
                        <td>Halaman</td>
                        <th><?=($i + 1)?> dari <?=$intTotalPage?></th>
                    </tr>
                </table></td>
        </tr></table>
    <!-- ITEMS -->
    <?$flag=0;?>
    <table cellspacing="0" border="0" id="invoiceItemList">

        <tr class="titleList border-top border-bottom">
            <th class="number">No</th>
            <th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
            <th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
            <th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-quantity')?></th>
        </tr><?php
        for($j = ($i * $intItemPerPage); $j < count($arrItem) && $j < (($i + 1) * $intItemPerPage); $j++):
            ?>
                <tr>
                    <td class="number"><b><?=$j + 1?></b></td>
                    <td class="code"><b><?=$arrItem[$j]['code']?></b></td>
                    <td class="title"><b><?=substr($arrItem[$j]['name'],0,$intProductTitleLength)?> </b></td>
                    <td class="quantity">
                        <?=$arrItem[$j]['qty1'].' '.formatUnitName($arrItem[$j]['unit1'])?> +
                        <?=$arrItem[$j]['qty2'].' '.formatUnitName($arrItem[$j]['unit2'])?> +
                        <?=$arrItem[$j]['qty3'].' '.formatUnitName($arrItem[$j]['unit3'])?>
                    </td>
                </tr>
        <?php
        endfor;
        if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>
            <tr><td colspan="4">&nbsp;</td></tr><?php
        endfor;
        if($i == $intTotalPage - 1): ?>
            <!-- FOOTER -->
            <tr class="subTotalHeader border-top">
            <td colspan="3">
                <table cellspacing="0" border="0">
                    <tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrDeliveryData['deli_description']?></td></tr>
                    <tr class="signHeader">
                        <th>Mengetahui</th><th>Logistik</th><th>&nbsp;</th>
                    </tr>
                    <tr class="signBody">
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    <tr class="signFooter">
                        <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td colspan="2">
                <table cellspacing="0" border="0">
                    <tr>
                        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
                        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
                    </tr>
                </table>
            </td>
            </tr><?php
        else: ?>
            <tr class="border-top">
            <td colspan="5">Lanjut Ke Halaman Berikut</td>
            <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
            <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
            </tr><?php
        endif; ?>
    </table>
    </div><?php
    if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor;