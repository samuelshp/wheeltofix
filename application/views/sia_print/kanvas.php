<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($arrKanvasItemSisa) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrKanvasItemSisa)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
    <table cellspacing="0" border="0" id="invoiceHeader"><tr>
        <td class="l"><table cellspacing="0" border="0">
            <tr>
                <th colspan="2"><h1>BUKTI MUAT KANVAS<!--<?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')?>--></h1></th>                
            </tr>
            <tr>
				<td class="t">Gudang</td>
				<th><?=$arrKanvasData['ware_name']?></th>
			</tr>
			<tr>
				<td>Sales</td>
				<th><?=$arrKanvasData['sals_code']?></th>				
			</tr>
            <tr>
                <td>&nbsp;</td>
                <th><?=$arrKanvasData['sals_name']?></th>
            </tr>
        </table></td>
        <td><table cellspacing="0" border="0">
			<tr>
			    <td colspan="2" class="companyTitle">
			        <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
			    </td>
			</tr>
            <tr>
				<td class="t">No. Trans CV</td>
				<th><?=$arrKanvasData['kanv_code']?></th>
			</tr>
			<tr>
				<td>Tgl Muatan</td>
				<th><?=formatDate2($arrKanvasData['curr_loading_date'],'d F Y')?></th>
			</tr>			
        </table></td>
    </tr></table>    
<!-- ITEMS -->
    <table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number">No</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
	</tr><?php
	for($j = ($i * $intItemPerPage); $j < count($arrKanvasItemSisa) && $j < (($i + 1) * $intItemPerPage); $j++):?>
    <tr>
		<td class="number"><b><?=$j + 1?></b></td>
        <td class="code"><b><?=$arrKanvasItemSisa[$j]['prod_code']?></b></td>
		<td class="title"><b><?=substr($arrKanvasItemSisa[$j]['strName'],0,$intProductTitleLength)?></b></td>
		<td class="quantity">
			<?=$arrKanvasItemSisa[$j]['kavi_quantity1'].' '.formatUnitName($arrKanvasItemSisa[$j]['kavi_unit1'])?> + 
			<?=$arrKanvasItemSisa[$j]['kavi_quantity2'].' '.formatUnitName($arrKanvasItemSisa[$j]['kavi_unit2'])?> + 
			<?=$arrKanvasItemSisa[$j]['kavi_quantity3'].' '.formatUnitName($arrKanvasItemSisa[$j]['kavi_unit3'])?>
		</td>	
		
	</tr><?php
	endfor;
	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="4">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?> 
<!-- FOOTER -->       
    	<tr class="subTotalHeader border-top">
	    <td colspan="7">
			<table cellspacing="0" border="0">
				<tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrKanvasData['kanv_description']?></td></tr>
				<tr class="signHeader2">
					<th>Penjualan</th>
					<th>Gudang</th>
					<th>Sopir</th>
					<th>Penerima</th>
					<th>&nbsp;</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="signFooter">				    
					<td class="a"><span></span></td>
					<td class="a"><span></span></td>
					<td class="a"><span></span></td>
					<td class="a"><span></span></td>					
				</tr>
			</table>
		</td>
		<td colspan="2">
		</td>
	</tr><?php
    else: ?>  
    <tr class="border-top">
        <td colspan="4">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>  
    </table>    
</div>
<?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>