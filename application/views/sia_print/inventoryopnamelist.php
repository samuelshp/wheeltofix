<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php 
$intProductTitleLength = 40; ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1>List Opname</h1></td>
		    </tr>
		    <tr>
                <td class="t2"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-atwarehouse')?></td>
                <th><?=$arrWarehouse['ware_name']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2" class="companyTitle">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=date('d F Y H:i')?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number">No.</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th>Lokasi</th>
		<th>C1</th>
		<th>C2</th>
	</tr><?php
	$intCount = count($arrProduct);
	for($j = 0; $j < $intCount; $j++): ?>  
	<tr class="<?=$j < $intCount - 1 && $arrProduct[$j]['proc_title'] != $arrProduct[$j + 1]['proc_title'] ? ' border-bottom' : ''?>">
		<td class="number"><b><?=$j + 1?></b></td>
        <td class="code"><b><?=$arrProduct[$j]['prod_code']?></b></td>
		<td class="title2"><b><?=substr($arrProduct[$j]['strName'],0,$intProductTitleLength)?></b></td>
		<td class="quantity"><?=countStock($arrProduct[$j]['id'], $arrWarehouse['id'])?></td>
		<td class="code"><?=$arrProduct[$j]['prod_rak']?></td>
		<td class="number" style="border-bottom: #000 1px dashed;">&nbsp;</td>
		<td class="number" style="border-bottom: #000 1px dashed;">&nbsp;</td>
	</tr><?php
	endfor; ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="7">
	        <table cellspacing="0" border="0">
                <tr class="signHeader">
                    <th>Mengetahui</th><th>Pemeriksa 1</th><th>Pemeriksa 2</th>
                </tr>
                <tr class="signBody">
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                </tr>
                <tr class="signFooter">
                    <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td>
                </tr>
            </table>
        </td>
	</tr>
	</table>
</div>