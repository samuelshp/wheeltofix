<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>Tanggal</th>
        <th>Kode</th>
        <th>Kategori</th>
        <th>Merk</th>
        <th>Nama</th>
        <th>Qty 1</th>     
        <th>Qty 2</th>
        <th>Qty 3</th>
        <th>Harga</th>
        <th>Diskon 1</th>
        <th>Diskon 2</th>
        <th>Diskon 3</th>
        <th>Total</th>
    </tr>
    <?php
    if(!empty($arrRetur)):
        foreach($arrRetur as $e):
			$date = formatDate2($e['cdate'],'m/d/Y');
			$price = $e['prri_price']; $subtotal = $e['prri_subtotal'];
			if(isset($print_mode) && $print_mode == 1){
				$date = formatDate2($e['cdate'],'d/m/Y');
				$price = setPrice($price); $subtotal = setPrice($subtotal);
			}
	?>
            <tr>
                <td><?=$date?></td>
				<td><?=$e['prcr_code']?></td>
				<td><?=$e['proc_title']?></td>
				<td><?=$e['prob_title']?></td>
				<td><?=$e['prod_title']?></td>            
				<td><?=$e['prri_quantity1']?></td>
				<td><?=$e['prri_quantity2']?></td>
				<td><?=$e['prri_quantity3']?></td>
				<td><?=$price?></td>
				<td><?=$e['prri_discount1']?></td>
				<td><?=$e['prri_discount2']?></td>
				<td><?=$e['prri_discount3']?></td>
				<td><?=$subtotal?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>