<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Detail Pembelian Per Supplier By Barang</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td><td>Kode</td><td style="width: 300px">: <?=$strSupplierCode?></td></tr>
            <tr><td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td><td>Nama</td><td style="width: 300px">: <?=$strSupplierName?></td></tr>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>No</th>
        <th>No Trans</th>
        <th>Tanggal</th>
        <th>Nama Barang</th>
        <th>Qty</th>
        <th>@Harga</th>
        <th>Disc%</th>
        <th>@NetHarga</th>
		<th>Total</th>
    </tr>
    <?php
    if(!empty($arrItems)):
        foreach($arrItems as $e):
			$date = formatDate2($e['cdate'],'m/d/Y');
			$price = $e['prci_price']; $netprice = $e['netprice']; $total = $e['netprice'] * ($e['prci_quantity1'] + ($e['prci_quantity2'] * ($e['prod_conv2'] / $e['prod_conv1'])) + ($e['prci_quantity3'] * ($e['prod_conv3'] / $e['prod_conv1'])));
			if(isset($print_mode) && $print_mode == 1){
				$date = formatDate2($e['cdate'],'d/m/Y');
				$price = setPrice($price); $netprice = setPrice($netprice); $total = setPrice($total);
			}
	?>
            <tr>
                <td><?=$e['nourut']?></td>
                <td><?=$e['prch_code']?></td>
                <td><?=$date?></td>
                <td><?=$e['prod_title']?></td>
                <td><?=$e['prci_quantity1']?> <?=$e['prod_unit1']?> + <?=$e['prci_quantity2']?> <?=$e['prod_unit2']?> + <?=$e['prci_quantity3']?> <?=$e['prod_unit3']?></td>
                <td><?=$price?></td>
                <td><?=$e['prci_discount1']?>+<?=$e['prci_discount2']?>+<?=$e['prci_discount3']?></td>
                <td><?=$netprice?></td>
				<td><?=$total?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="9"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>