<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Rekap Penjualan Per Salesman By Customer</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td><td>Kode</td><td style="width: 300px">: <?=$strSalesmanCode?></td></tr>
            <tr><td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td><td>Nama</td><td style="width: 300px">: <?=$strSalesmanName?></td></tr>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>No</th>
        <th>Kode</th>
        <th>Nama Customer</th>
        <th>Kota</th>
        <th>Q-B</th>
        <th>Q-T</th>
        <th>Q-K</th>
        <th>GrandNetto</th>
    </tr>
    <?php
    if(!empty($arrItems)):
        foreach($arrItems as $e):
			$subtotal = $e['total'];
			if(isset($print_mode) && $print_mode == 1){
				$subtotal = setPrice($subtotal);
			}
	?>
            <tr>
                <td><?=$e['nourut']?></td>
                <td><?=$e['cust_code']?></td>
                <td><?=$e['cust_name']?></td>
                <td><?=$e['cust_city']?></td>
                <td><?=$e['qty1']?></td>
                <td><?=$e['qty2']?></td>
                <td><?=$e['qty3']?></td>
                <td><?=$subtotal?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="8"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>