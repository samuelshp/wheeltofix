<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($arrDaftarPembayaranHutangItem) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrDebt)):?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2">
		            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		        </td>
		    </tr>
			<tr>
		        <td colspan="2"><h2><?=$strPageTitle?></h2></td>
			</tr>				
			<tr>
				<td class="t">No. Nota</td>
				<th><?=$arrDebt['code']?></th>
			</tr>						
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<td><?=formatDate2($arrDebt['date'],'d F Y')?></td>
			</tr>
			<tr>
				<?php 
				if($arrDebt['metode'] == 1): $strMetode = "Cash";  elseif($arrDebt['metode'] == 2): $strMetode = "Bank";  else: $strMetode = "Giro"; endif;?>
				<td>Metode</td>
				<td><?=$strMetode; echo ($strMetode == "Giro") ? " (Ref. Giro: ".$arrDebt['txou_ref_giro'].")" : null ?></td>
			</tr>
			<?php if ($intType != 2): ?>
			<tr>				
				<td>Akun</td>
				<td><?=$arrDebt['acco_code']." - ".$arrDebt['acco_name']?></td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="t">Supplier</td>
				<th><?=$arrDebt['supp_name']?></th>
			</tr>
			<?php if ($intType == 2): ?>
			<tr>
				<td class="t">Proyek</td>
				<th><?=$arrDetail[0]['kont_name']?></th>
			</tr>
			<?php endif; ?>
		</table></td>
		
	</tr></table>
<!-- ITEMS -->
      
    <?php if($intType == 1): ?>
	<table cellspacing="0" border="0" id="invoiceItemList">
		<tr class="titleList border-top border-bottom">
			<th class="title">No. Nota</th>			
			<th class="price">Amount</th>		
		</tr>
		<?php
			if(!empty($arrDetail)):
			$intTotal = 0;
			$intRencanaBayar = 0;
		    foreach($arrDetail as $e):
		?>
	    
	    <tr style="text-align:center;">
	        <td><?=$e['dphu_code']?></td>        
	        <td class="price"><?=setPrice($e['dphu_amount'])?></td>        
	    </tr>
	    
	    <?php     	
	    	$intRencanaBayar += $e['dphu_amount']; 
	    	endforeach; 
	    ?>
	    <tr class="subTotalHeader border-top">					
			<td colspan="4">
				<table border="0">
					<tr>										
						<td class="subTotal"><?=setPrice($intRencanaBayar)?></td>
					</tr>
				</table>
			</td>		
		</tr>
		<?php endif; ?>
    </table>
	<?php endif; ?>
	
	<?php if($intType == 2):?>
	<table cellspacing="0" border="0" id="invoiceItemList">
		<tr class="titleList border-top border-bottom">
			<th class="title">Keterangan</th>
			<th class="title">DPP</th>
			<th class="title">Ppn (%)</th>			
			<th class="title">Pph (%)</th>			
			<th class="price">Amount</th>		
		</tr>
		<?php
			if(!empty($arrDetail)):
			$intTotal = 0;
			$intRencanaBayar = 0;
		    foreach($arrDetail as $e):
		?>
	    
	    <tr style="text-align:center;">
	        <td><?=$arrDebt['txou_description']?></td>
	        <td><?=setPrice($e['txod_amount'])?></td>
	        <td><?=$e['txod_ppn']?></td>
	        <td><?=$e['txod_pph']?></td>        
	        <td class="price"><?=setPrice($e['txod_totalbersih'])?></td>        
	    </tr>
	    
	    <?php     	
	    	$intRencanaBayar += $e['txod_totalbersih']; 
	    	endforeach; 
	    ?>
	    <tr class="subTotalHeader border-top">					
			<td colspan="5">
				<table border="0">
					<tr>										
						<td class="subTotal"><?=setPrice($intRencanaBayar)?></td>
					</tr>
				</table>
			</td>		
		</tr>
		<?php endif; ?>
    </table>
	<?php endif; ?>

<!-- FOOTER -->
	<table cellspacing="0" border="0" id="invoiceItemList">	
		<tr class="subTotalHeader">
			<td>
				<table cellspacing="0" border="0">
					<tr class="signHeader">
						<th>Dibuat oleh</th>
						<th>Disetujui</th>
					</tr>
					<tr class="signBody">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="signFooter">
						<td class="a"><span><?=$arrLogin['adlg_name']?></span></td>
						<td class="a"><span></span></td>
					</tr>
				</table>
			</td>
			<td></td>
		</tr>
	</table>
	<?php
    else: ?>
    <table cellspacing="0" border="0" id="invoiceItemList">
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr>
	</table>
</div>
<?php endif; ?>