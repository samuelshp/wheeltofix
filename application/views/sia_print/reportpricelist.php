<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/reportdefaultprint.css')?>" />
	<h2 class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-pricelist')?></h2>
	<p class="clearPan">&nbsp;</p>
	<table class="report" cellspacing="0px">
		<tr class="tbHeader">
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		</tr><?php
if(!empty($arrProduct)): 
	foreach($arrProduct as $e): ?>  
		<tr>
			<td><?=generateTransactionCode('',$e['id'],'product')?></td>
			<td><?=formatProductName('ABBR',$e['prod_title'],$e['prob_title'],$e['proc_title'])?></td>
			<td><?=setPrice($e['prod_price'],'USED')?></td>
		</tr><?php
	endforeach;
else: ?>  
		<tr><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
endif; ?>  
	</table>