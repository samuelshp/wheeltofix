<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/reportdefaultprint.css')?>" />
	<h1 class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-profitlossbalance')?></h1>
	<p class="clearPan">&nbsp;</p><?php
if(!empty($strFrom)): ?>  
	<p id="sortModePan"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-from')?> <?=formatDate2($strFrom,'d F Y')?> <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-to')?> <?=formatDate2($strTo,'d F Y')?></p>
	<table class="report" cellspacing="0px">
		<tr class="tbHeader">
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-finance')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-destination')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-description')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-date')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-duedate')?></th>
			<th><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'report-total')?></th>
		</tr><?php
	if(!empty($arrFinance)): 
		foreach($arrFinance as $e): ?>  
		<tr>
			<td><?=generateTransactionCode($e['fina_date'],$e['id'],'finance')?></td>
			<td><?=$e['fina_type']?></td>
			<td><?=$e['fini_to']?></td>
			<td><?=$e['fina_title']?> | <b><?=$e['fini_title']?></b></td>
			<td><?=formatDate2($e['fini_date'],'d F Y')?></td>
			<td><?=formatDate2($e['fini_duedate'],'d F Y')?></td>
			<td class="p"><?=setPrice($e['fini_price'],'USED')?></td>
		</tr><?php
		endforeach;
	else: ?>  
		<tr><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
	endif; ?>  
		<tr><td class="p" colspan="6"><h4 style="text-align:right;">Total</h4></td><td class="p"><?=setPrice($intProfitLoss,'USED')?></td></tr>
	</table><?php
endif; ?>