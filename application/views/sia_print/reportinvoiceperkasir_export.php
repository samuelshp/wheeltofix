<!-- created by patricklipesik -->
<table>
	<tr>
		<td colspan="2" class="companyTitle">
			<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
			<b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
			<b><?=$arrCompanyInfo['strOwnerPhone']?></b>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>Laporan</td><td>: Invoice Per Kasir</td>
			</tr>
			<tr>
				<td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td>
			</tr>
			<tr>
				<td>Kasir</td><td>: <?=$strUserCode?></td>
			</tr>
			<tr>
				<td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td>
			</tr>
		</table>
	</tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
	<tr class="tbHeader">
		<th>No</th>
		<th>Kasir</th>
		<th>No Trans</th>
		<th>Tanggal</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>GrandTotal</th>
	<!--<th>Potongan</th>
		<th>GrandNetto</th>-->
	</tr>
	<?php
	$sumGrandTotal = 0;
	if(!empty($arrItems)):
		foreach($arrItems as $e):
			$date = formatDate2($e['cdate'],'m/d/Y h:i:s');
			$grandtotal = $e['invo_grandtotal']; $netto = $e['invo_grandtotal']-$e['invo_discount']-$e['invo_tax'];
			if(isset($print_mode) && $print_mode == 1){
				$date = formatDate2($e['cdate'],'d/m/Y h:i:s');
				$grandtotal = setPrice($grandtotal); $netto = setPrice($netto);
			}
			$sumGrandTotal += $e['invo_grandtotal'];
	?>
			<tr>
				<td><?=$e['nourut']?></td>
				<td><?=$e['adlg_login']?></td>
				<td><?=$e['invo_code']?></td>
				<td><?=$date?></td>
				<td><?=$e['cust_code']?></td>
				<td><?=$e['cust_name']?></td>
				<td><?=$grandtotal?></td>
			<!--<td><?=$e['invo_discount']?></td>
				<td><?=$netto?></td>-->
			</tr>
		<?php endforeach;
	else:?>
		<tr><td class="noData" colspan="7"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
	endif;
	
	if(!empty($sumGrandTotal)):
		if(isset($print_mode) && $print_mode == 1)
			$sumGrandTotal = setPrice($sumGrandTotal);
	?>
	<tr class="tbHeader">
		<th colspan="6" style="text-align:right;">Grand Total</th>
		<th><?=$sumGrandTotal?></th>
	</tr><?php
	endif;?>
</table>