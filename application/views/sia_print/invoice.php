<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrPurchaseItem)+count($arrPurchaseBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;
?>

<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader">
		<tr>
	        <td class="companyTitle"><h2>No : <?=$arrInvoiceData['invo_code']?></h2></td>
		</tr>
		<tr>
			<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kuitansi')?></td>
		</tr>
	</table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtyorder')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-qtyterima')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
	</tr><?php
    foreach($arrPurchaseItem as $e):?>
    <tr style="text-align:left;">
        <td><?=$e['prci_quantity_pb']?> <?=$e['sat_pb']?></td>
        <td><?=$e['prod_title']?></td>
        <td><?=$e['prci_quantity1']?> <?=$e['sat_bayar']?></td>
        <td class="price"><?=setPrice($e['prci_price'],'BASE',FALSE)?></td>
        <td class="subTotal"><?=setPrice($e['prci_quantity1']*$e['prci_price'],'BASE',FALSE)?></td>
    </tr>
    <?php endforeach; ?>
        
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
		<td colspan="1">
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Dibuat oleh</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span> <?=$arrLogin['adlg_login']?> </span></td>
				</tr>
			</table>
		</td>
		<td colspan="2"></td>
		<td colspan="2">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($arrInvoiceData['prch_subtotal'],'BASE',FALSE)?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></td>
					<td class="subTotal"><?=setPrice($arrInvoiceData['prch_discount'],'BASE',FALSE)?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
					<td class="subTotal"><?=$arrInvoiceData['prch_tax']?>%</td>
				</tr>
				<tr>
					<td class="title"><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></b></td>
					<td class="subTotal"><b><?=setPrice($arrInvoiceData['prch_grandtotal'],'BASE',FALSE)?></b></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
</div>