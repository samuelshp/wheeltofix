<?php // created by patricklipesik
if(!isset($io)){
	$io = 0;
	$spCurr = 'BASE';
	$spSign = TRUE;
	$spPass = TRUE;
}

// 1a. header versi export excel & print
if($io <= 1){
?>
<table>
	<tr>
		<td colspan="2" class="companyTitle">
			<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
			<b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
			<b><?=$arrCompanyInfo['strOwnerPhone']?></b>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>Laporan</td><td>: Laba Rugi</td>
			</tr>
			<tr>
				<td>Periode</td><td>: <?=formatDate2($strPeriode.'-01','M Y')?></td>
			</tr>
			<tr>
				<td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td>
			</tr>
		</table>
	</tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
	<tr class="tbHeader">
		<th>DESKRIPSI</th>
		<th>BULAN LALU</th>
		<th>BULAN INI</th>
		<th>TAHUN INI</th>
	</tr>
<?php
}
// 1a. end

// 1b. header versi default menu
elseif($io >= 2){
?>
<div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">Periode</div>
				<div class="col-md-9">: <?=formatDate2($strPeriode.'-01','M Y')?></div>
				<div class="col-md-3">Hari ini</div>
				<div class="col-md-9">: <?=date('d/m/Y H:i')?></div>
			</div>
		</div>
	</div>
</div>
<p class="spacer">&nbsp;</p>
<div class="table-responsive">
	<table class="table table-bordered table-condensed table-hover">
		<thead>
			<tr>
				<th>DESKRIPSI</th>
				<th>BULAN LALU</th>
				<th>BULAN INI</th>
				<th>TAHUN INI</th>
			</tr>
		</thead>
		<tbody>
<?php
}
// 1b. end

// 2. isi table laporan
$sumTotal1 = 0;
$sumTotal2 = 0;
$sumTotal3 = 0;
$sumGrandTotal1 = 0;
$sumGrandTotal2 = 0;
$sumGrandTotal3 = 0;
if(!empty($arrItems)){
	$i = 0;
	foreach($arrItems as $e){
		if($e['acco_detail'] == 1){
			if($i > 0){
?>
	<tr>
		<td style="text-align:right;">Total</td>
		<td><?=setPrice($sumTotal1,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal2,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal3,$spCurr,$spSign,$spPass)?></td>
	</tr>
<?php
			}
			$sumTotal1 = 0;
			$sumTotal2 = 0;
			$sumTotal3 = 0;
?>
	<tr>
		<td colspan="4"><b><?=$e['Nama']?></b></td>
	</tr>
<?php
			$i++;
		}
?>
	<tr>
		<td><?=$e['Nama']?></td>
		<td><?=setPrice($e['decBulanLalu'],$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($e['decBulanIni'],$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($e['decTahunIni'],$spCurr,$spSign,$spPass)?></td>
	</tr>
<?php
		$sumTotal1 += $e['decBulanLalu'];
		$sumTotal2 += $e['decBulanIni'];
		$sumTotal3 += $e['decTahunIni'];
		$sumGrandTotal1 += $e['decBulanLalu'];
		$sumGrandTotal2 += $e['decBulanIni'];
		$sumGrandTotal3 += $e['decTahunIni'];
	}
	if($i > 0){
?>
	<tr>
		<td style="text-align:right;">Total</td>
		<td><?=setPrice($sumTotal1,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal2,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumTotal3,$spCurr,$spSign,$spPass)?></td>
	</tr>
<?php
	}
?>
	<tr>
		<td style="text-align:right;">GRAND TOTAL</td>
		<td><?=setPrice($sumGrandTotal1,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumGrandTotal2,$spCurr,$spSign,$spPass)?></td>
		<td><?=setPrice($sumGrandTotal3,$spCurr,$spSign,$spPass)?></td>
	</tr>
<?php
}
else {
?>
	<tr>
		<td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td>
	</tr>
<?php
}
// 2. end

// 3. penutup table (yang berbeda untuk masing-masing versi)
if($io <= 1){
?>
</table>
<?php
} elseif($io >= 2){
?>
		</tbody>
	</table>
</div>
<?php
}
?>