<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($arrDaftarPembayaranHutangItem) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrUMC)):?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2">
		            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		        </td>
		    </tr>
			<tr>
		        <td colspan="2"><h2>Uang Masuk Customer</h2></td>
			</tr>				
			<tr>
				<td class="t">No. UMC</td>
				<th><?=$arrUMC['umcu_code']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<td><?=formatDate2($arrUMC['umcu_date'],'d F Y')?></td>
			</tr>						
			</tr>
		</table></td>
		<!-- <td><table cellspacing="0" border="0">
		    <tr>		    	
				<td colspan="2">&nbsp;</td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kepada')?></td>
			</tr>
			<tr>
				<td><?=$arrPurchaseData['supp_name']?></td>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-suppliercp')?> <b><?=$arrPurchaseData['supp_cp_name']?></b></td>				
			</tr>
			<tr>		    	
				<td colspan="2">&nbsp;</td>
		    </tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-description')?></td>
			</tr>
			<tr>
				<td><?=nl2br($arrPurchaseData['prch_description'])?></td>
			</tr>
		</table></td> -->
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">		
		<th class="title">Akun</th>	
		<th class="title">Customer</th>		
		<th class="price">Amount</th>		
	</tr>
	<tr>		
		<td><?=$arrUMC['acco_name']?></td>
		<td><?=$arrUMC['cust_name']?></td>
		<td class="price"><?=setPrice($arrUMC['umcu_amount'])?></td>
	</tr>
        
<!-- FOOTER -->
	
	<tr class="subTotalHeader">
		<td>
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<th>Dibuat oleh</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span><?=$arrLogin['adlg_name']?></span></td>
				</tr>
			</table>
		</td>		
	</tr>	
	</table>
</div>
<?php endif;?>