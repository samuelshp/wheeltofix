<!-- created by patricklipesik -->
<table>
	<tr>
		<td colspan="2" class="companyTitle">
			<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
			<b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
			<b><?=$arrCompanyInfo['strOwnerPhone']?></b>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>Laporan</td><td>: Rekap Pembelian</td>
			</tr>
			<tr>
				<td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td>
			</tr>
			<tr>
				<td>Supplier</td><td>: <?=$strSupplierName?></td>
			</tr>
			<tr>
				<td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td>
			</tr>
		</table>
	</tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
	<tr class="tbHeader">
		<th>No</th>
		<th>Tanggal</th>
		<th>Transaksi</th>
		<th>Supplier</th>
		<th>Total</th>
	</tr>
	<?php
	$sumGrandTotal = 0;
	if(!empty($arrItems)):
		foreach($arrItems as $e):
			$tanggal = formatDate2($e['tanggal'],'m/d/Y h:i:s');
			$total = $e['total'];
			if(isset($print_mode) && $print_mode == 1){
				$tanggal = formatDate2($e['tanggal'],'d/m/Y h:i:s');
				$total = setPrice($total);
			}
			$sumGrandTotal += $e['total'];
	?>
			<tr>
				<td><?=$e['nourut']?></td>
				<td><?=$tanggal?></td>
				<td><?=$e['faktur']?></td>
				<td><?=$e['supplier']?></td>
				<td><?=$total?></td>
			</tr>
		<?php endforeach;
	else:?>
		<tr><td class="noData" colspan="5"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
	endif;
	
	if(!empty($sumGrandTotal)):
		if(isset($print_mode) && $print_mode == 1)
			$sumGrandTotal = setPrice($sumGrandTotal);
	?>
	<tr class="tbHeader">
		<th colspan="4" style="text-align:right;">Grand Total</th>
		<th><?=$sumGrandTotal?></th>
	</tr><?php
	endif;?>
</table>