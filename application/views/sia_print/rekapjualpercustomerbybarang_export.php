<table>
    <tr>
        <td colspan="2" class="companyTitle">
            <h2><?=$arrCompanyInfo['strCompanyName']?></h2>
            <b><?=$arrCompanyInfo['strOwnerAddress']?></b><br>
            <b><?=$arrCompanyInfo['strOwnerPhone']?></b>
        </td>
    </tr>
    <tr>
        <table>
            <tr><td>Laporan</td><td>: Rekap Penjualan Per Customer By Barang</td></tr>
            <tr><td>Periode</td><td>: <?=formatDate2($strStart,'d/m/Y')?> s/d <?=formatDate2($strEnd,'d/m/Y')?></td><td>Kode</td><td style="width: 300px">: <?=$strCustomerCode?></td></tr>
            <tr><td>Hari ini</td><td>: <?=date('d/m/Y H:i')?></td><td>Nama</td><td style="width: 300px">: <?=$strCustomerName?></td></tr>
        </table>
    </tr>
</table>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">
        <th>No</th>
        <th>Kode</th>
        <th>Nama Barang</th>
        <th>QtyB</th>
        <th>Sat</th>
        <th>QtyT</th>
        <th>Sat</th>
        <th>QtyK</th>
        <th>Sat</th>
        <th>Jumlah</th>
    </tr>
    <?php
    if(!empty($arrItems)):
        foreach($arrItems as $e):
			$subtotal = $e['invi_subtotal'];
			if(isset($print_mode) && $print_mode == 1){
				$subtotal = setPrice($subtotal);
			}
	?>
            <tr>
                <td><?=$e['nourut']?></td>
                <td><?=$e['prod_code']?></td>
                <td><?=$e['prod_title']?></td>
                <td><?=$e['trhi_quantity1']?></td>
                <td><?=$e['prod_unit1']?></td>
                <td><?=$e['trhi_quantity2']?></td>
                <td><?=$e['prod_unit2']?></td>
                <td><?=$e['trhi_quantity3']?></td>
                <td><?=$e['prod_unit3']?></td>
				<td><?=$subtotal?></td>
            </tr>
        <?php endforeach;
    else:?>
        <tr><td class="noData" colspan="10"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>