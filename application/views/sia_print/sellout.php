<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/reportdefaultprint.css')?>" />
<h2 class="title">Laporan Sell Out</h2>
<p class="clearPan">&nbsp;</p>
<table class="report" cellspacing="0px">
    <tr class="tbHeader">        
        <th>KODE</th>
        <th>BARCODE</th>
        <th>KATEGORI</th>
        <th>MERK</th>
        <th>NAMA</th>
        <th>QTY 1</th>        
        <th>QTY 2</th>
        <th>QTY 3</th>
        <th>Total</th>
    </tr>
    <?php    
    if(!empty($arrStock)):
        foreach($arrStock as $e): ?>
            <tr>                
                <td><?=$e['prod_code']?></td>
                <td><?=$e['prod_barcode']?></td>
                <td><?=$e['proc_title']?></td>
                <td><?=$e['prob_title']?></td>
                <td><?=$e['prod_title']?></td>
                <td><?=$e['qty1']?></td>
                <td><?=$e['qty2']?></td>
                <td><?=$e['qty3']?></td>
                <td><?=$e['subtotal']?></td>
            </tr>
        <?php        
        endforeach;
    else: ?>
        <tr><td class="noData" colspan="13"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-nodata')?></td></tr><?php
    endif; ?>
</table>