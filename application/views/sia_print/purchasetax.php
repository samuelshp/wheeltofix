<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrPurchaseItem)+count($arrPurchaseBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrPurchaseItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase')?></h1></td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-supplier')?></td>
				<th><?=$arrPurchaseData['supp_name']?></th>
                <th>Kode dan Nomer Pajak :<?=$numberTax?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrPurchaseData['supp_address']?></th>
                <th>NPWP :<?=$dataPrint['supp_npwp']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrPurchaseData['supp_phone']?></th>
                <th>PKP :<?=$dataPrint['supp_pkp']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2" class="companyTitle">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
				<th><?=$arrPurchaseData['prch_code']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=formatDate2($arrPurchaseData['prch_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top">
	    <td colspan="7">
	        <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-enddate')?>: <?=formatDate2($dataPrint['pror_end_date'],'d F Y')?> | 
	        <?=$dataPrint['ware_name']?> | 
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-ekspeditionnumber')?>: <?=$dataPrint['acce_ekspedition_number']?> | 
            <?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedate')?>: <?=formatDate2($dataPrint['acce_date'],'d F Y')?>
	    </td>
	</tr>
	<tr class="titleList border-top border-bottom">
		<th class="number">No.</th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="discount"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php

        $intItemCount=0;
        if(!empty($arrPurchaseItem))$intItemCount+=count($arrPurchaseItem);
        if(!empty($arrPurchaseBonusItem))$intItemCount+=count($arrPurchaseBonusItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
		?>
        
        <?php if($j<count($arrPurchaseItem)){
        $intTotalPriceSoFar += $arrPurchaseItem[$j]['prci_subtotal'];?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrPurchaseItem[$j]['prod_code']?></b></td>
            <td class="title"><b><?=substr($arrPurchaseItem[$j]['strName'],0,$intProductTitleLength)?></b></td>
            <td class="quantity">
                <?=$arrPurchaseItem[$j]['prci_quantity1'].' '.formatUnitName($arrPurchaseItem[$i]['prci_unit1'])?> +
                <?=$arrPurchaseItem[$j]['prci_quantity2'].' '.formatUnitName($arrPurchaseItem[$i]['prci_unit2'])?> +
                <?=$arrPurchaseItem[$j]['prci_quantity3'].' '.formatUnitName($arrPurchaseItem[$i]['prci_unit3'])?>
            </td>
            <td class="price"><?=setPrice($arrPurchaseItem[$j]['prci_price'])?></td>
            <td class="discount">
                <?=$arrPurchaseItem[$j]['prci_discount1']?>% +
                <?=$arrPurchaseItem[$j]['prci_discount2']?>% +
                <?=$arrPurchaseItem[$j]['prci_discount3']?>%</td>
            <td class="subTotal"><?=setPrice($arrPurchaseItem[$j]['prci_subtotal'])?></td>
        </tr>
    <?php }else{ ?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prod_code']?></b></td>
            <td class="title"><b><?=substr($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['strName'],0,$intProductTitleLength)?> (*)</b></td>
            <td class="quantity">
                <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_quantity1'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_unit1'])?> +
                <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_quantity2'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_unit2'])?> +
                <?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_quantity3'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['prci_unit3'])?>
            </td>
            <td class="price"><?=setPrice(0)?></td>
            <td class="discount">
                0% +
                0% +
                0%</td>
            <td class="subTotal"><?=setPrice(0)?></td>
        </tr>
    <?php } ?>
        
        
        <?php
	endfor;
	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="7">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
		<td colspan="5">
			<table cellspacing="0" border="0">
				<tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrPurchaseData['prch_description']?></td></tr>
				<tr class="signHeader">
					<th>&nbsp;</th><th>Pembelian</th><th>Penerima</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td colspan="2">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount').' '.setPrice($arrPurchaseData['prch_discount'])?></td>
					<td class="subTotal"><?=setPrice($arrPurchaseData['subtotal_discounted'])?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
					<td class="subTotal"><?=$arrPurchaseData['prch_tax']?>%</td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></td>
					<td class="subTotal"><?=setPrice($intPurchaseGrandTotal)?></td>
				</tr>
			</table>
		</td>
	</tr><?php
    else: ?>
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>