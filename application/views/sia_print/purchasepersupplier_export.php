<h2 class="title">Laporan Pembelian Per Supplier</h2>
<p class="clearPan">&nbsp;</p>

<?php
if(!empty($arrPurchase)):
    foreach($arrPurchase as $e):?>
        <h3>Kode : <?=$e['supp_code']?></h3>
        <h3>Nama : <?=$e['supp_name']?></h3>
        <br>
        <table class="report" cellspacing="0px">
        <tr class="tbHeader">
            <th>TANGGAL</th>
            <th>KODE</th>
            <th>NAMA</th>
            <th>QTY 1</th>
            <th>QTY 2</th>
            <th>QTY 3</th>
            <th>HARGA</th>
            <th>DISC 1</th>
            <th>DISC 2</th>
            <th>DISC 3</th>                
            <th>TOTAL</th>
            <th>BONUS</th>            
        </tr>
        <?php
        foreach($arrDetail as $d):
            if ($d['prch_supplier_id'] == $e['supp_id']):?>
            
            <tr>
                <td><?=formatDate2($d['cdate'],'d F Y')?></td>
                <td><?=$d['prod_code']?></td>                
                <td><?=$d['prci_description']?></td>
                <td><?=$d['prci_quantity1']?></td>
                <td><?=$d['prci_quantity2']?></td>
                <td><?=$d['prci_quantity3']?></td>
                <td><?=setPrice($d['prci_price'])?></td>
                <td><?=$d['prci_discount1']?></td>
                <td><?=$d['prci_discount2']?></td>
                <td><?=$d['prci_discount3']?></td>                
                <td><?=setPrice($d['prci_subtotal'])?></td>
                <td><?php if($d['prci_bonus']==1): echo "x"; else: echo "-"; endif ?></td>
            </tr>
            
        <?php    
            endif;
        endforeach;?>
        </table>
        <br><br>
        <?php
    endforeach;
endif;
?>