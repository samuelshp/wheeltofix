<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/inventorybarcodeprint.css')?>" />
<div id="barcodePrint"><div class="container"><?php
$intBarcodeNumber = generateBarcodeNumber('',$arrProduct['id']);
$intPrice = setPrice($arrProduct['prod_price'],$arrProduct['prod_currency']);
$strProductName = formatProductName('',$arrProduct['prod_title']);
for($i = 0; $i < $intPrintNumber; $i++): ?>  
	<div class="barcodeTag"><table cellspacing="0px" border="0px">
		<tr>
			<td class="barcodeNumber" rowspan="3">&nbsp;<span><?=$intBarcodeNumber?></span></td>
			<td class="storeName"><?=$this->config->item('jw_website_name')?></td>
			<td class="price" rowspan="3"><span><?=$intPrice?></span></td>
		</tr>
		<tr>
			<td class="barcodeID"><?='*'.$intBarcodeNumber.'*'?></td>
		</tr>
		<tr>
			<td class="productName"><?=$strProductName?></td>
		</tr>
	</table></div><?php
endfor; ?>  
</div></div>