<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$strAutoProceedURL = site_url('invoice/view/'.$intInvoiceID, NULL, FALSE);
require_once('_autoproceed.php');
$intItemPerPage = 16;
$intTotalPage = (int) ceil((count($arrInvoiceItem)+count($arrInvoiceBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrInvoiceItem)) for($i = 0; $i < $intTotalPage; $i++): ?>  
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
			<tr>
			    <th colspan="2"><h1><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')?></h1></th>
			</tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kepada')?></td>
				<th><?=$arrInvoiceData['cust_name']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceData['cust_address']?></th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<th><?=$arrInvoiceData['cust_phone']?></th>
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
			<tr>
			    <td colspan="2">
			        <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
			    </td>
			</tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
				<th><?=generateTransactionCode($arrInvoiceData['invo_date'],$arrInvoiceData['id'],'invoice')?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-date')?></td>
				<th><?=formatDate2($arrInvoiceData['invo_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number"></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
		<th class="code">Rak</th>
	</tr><?php

        $intItemCount=0;
        if(!empty($arrInvoiceItem))$intItemCount+=count($arrInvoiceItem);
        if(!empty($arrInvoiceBonusItem))$intItemCount+=count($arrInvoiceBonusItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
		?>
        <?php if($j<count($arrInvoiceItem)){
        $intTotalPriceSoFar += $arrInvoiceItem[$j]['invi_subtotal'];?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="title2"><b><?=substr($arrInvoiceItem[$j]['invi_description'],0,$intProductTitleLength)?></b></td>
            <td class="quantity">
            	<?=formatProductUnit($arrInvoiceItem[$j]['invi_product_id'], array('intQty1' => $arrInvoiceItem[$j]['invi_quantity1'], 'intQty2' => $arrInvoiceItem[$j]['invi_quantity2'], 'intQty3' => $arrInvoiceItem[$j]['invi_quantity3']), 'separated')?>
            </td>
            <td class="price"><?=setPrice($arrInvoiceItem[$j]['invi_price'])?></td>
            <td class="subTotal"><?=setPrice($arrInvoiceItem[$j]['invi_subtotal'])?></td>
            <td><?=$arrInvoiceItem[$j]['prod_rak']?></td>
        </tr>
        <?php }else{ ?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="code"><b><?=$arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['prod_code']?></b></td>
            <td class="title"><b><?=substr($arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_description'],0,$intProductTitleLength)?></b></td>
            <td class="quantity">
                <?=formatProductUnit($arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_product_id'], array('intQty1' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity1'], 'intQty2' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity2'], 'intQty3' => $arrInvoiceBonusItem[$j-count($arrInvoiceItem)]['invi_quantity3']), 'separated')?>
            </td>
            <td class="price"><?=setPrice(0)?></td>
            <td class="subTotal"><?=setPrice(0)?></td>
            <td><?=$arrInvoiceItem[$j]['prod_rak']?></td>
        </tr>
        <?php } ?>
    <?php
	endfor;
    if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="6">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="3">
			<table cellspacing="0" border="0">
				<tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrInvoiceData['invo_description']?></td></tr>
				<tr class="signHeader2">
					<td rowspan="3" style="width:50%; font-size:0.8em;">&nbsp;</td>
					<th width="25%">Hormat Kami</th>
					<th width="25%">Penerima</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td>
				</tr>
			</table>
		</td>
	    <td colspan="3">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
					<td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-discount')?></td>
					<td class="subTotal"><?=setPrice($arrInvoiceData['invo_discount'])?></td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
		            <td class="subTotal"><?=$arrInvoiceData['invo_tax']?>%</td>
				</tr>
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></td>
		            <td class="subTotal"><?=setPrice($intInvoiceGrandTotal, 'BASE', TRUE, FALSE, TRUE)?></td>
				</tr>
			</table>
		</td>
	</tr><?php
    else: ?>  
    <tr class="border-top">
        <td colspan="4">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
	endif; ?>  
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>  
	<?php
        if(!empty($arrPotongan)):
            foreach($arrPotongan as $e):
                echo "> ".$e['poto_item_qty']." ".$e['poto_name']." ,Potongan / Item => ".$e['poto_nominal']."<br>";
            endforeach;
        endif;
    ?>