<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php 
$intItemPerPage = 8;
$intTotalPage = (int) ceil(count($arrAdjustmentItem) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrAdjustmentItem)) for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader"><tr>
		<td class="l"><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2"><h1><?php 
if($arrAdjustmentItem[0]['adit_adjustment_type'] < 3): 
    echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustment');
else: 
    echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-mutation');
endif; ?></h1></td>
		    </tr>
			<tr>
                <td class="t2"><?php
if($arrAdjustmentItem[0]['adit_adjustment_type'] < 3):
    echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-atwarehouse');
else: 
    echo loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-froMwarehouse');
endif; ?></td>
                <th><?=$arrAdjustmentData['from_ware']?></th>
			</tr><?php 
if($arrAdjustmentItem[0]['adit_adjustment_type'] == 3): ?>  
			<tr>
                <td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-towarehouse')?></td>
                <th><?=$arrAdjustmentData['to_ware']?></th><?php 
endif; ?>  
			</tr>
		</table></td>
		<td><table cellspacing="0" border="0">
		    <tr>
		        <td colspan="2" class="companyTitle">
		            <h1><?=$arrCompanyInfo['strCompanyName']?></h1>
		            <h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
		            <h2><?=$arrCompanyInfo['strOwnerPhone']?></h2>
		        </td>
		    </tr>
			<tr>
				<td class="t"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicenumber')?></td>
				<th><?=$arrAdjustmentData['ajst_code']?></th>
			</tr>
			<tr>
				<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-createddate')?></td>
				<th><?=formatDate2($arrAdjustmentData['ajst_date'],'d F Y')?></th>
			</tr>
			<tr>
				<td>Halaman</td>
				<th><?=($i + 1)?> dari <?=$intTotalPage?></th>
			</tr>
		</table></td>
	</tr></table>
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr class="titleList border-top border-bottom">
		<th class="number">No.</th>
		<th class="code"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-code')?></th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-subtotal')?></th>
	</tr><?php
	for($j = ($i * $intItemPerPage); $j < count($arrAdjustmentItem) && $j < (($i + 1) * $intItemPerPage); $j++):
		$intTotalPriceSoFar += $arrAdjustmentItem[$j]['adit_subtotal']; ?>  
	<tr>
		<td class="number"><b><?=$j + 1?></b></td>
        <td class="code"><b><?=$arrAdjustmentItem[$j]['prod_code']?></b></td>
		<td class="title"><b><?=substr($arrAdjustmentItem[$j]['strName'],0,$intProductTitleLength)?></b></td>
		<td class="quantity">
		    <?=$arrAdjustmentItem[$j]['adit_quantity1'].' '.formatUnitName($arrAdjustmentItem[$j]['adit_unit1'])?> +
            <?=$arrAdjustmentItem[$j]['adit_quantity2'].' '.formatUnitName($arrAdjustmentItem[$j]['adit_unit2'])?> +
            <?=$arrAdjustmentItem[$j]['adit_quantity3'].' '.formatUnitName($arrAdjustmentItem[$j]['adit_unit3'])?>
        </td>
		<td class="subTotal"><?=setPrice($arrAdjustmentItem[$j]['adit_subtotal'])?></td>
	</tr><?php
	endfor;
    if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="5">&nbsp;</td></tr><?php
	endfor; 
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="3">
	        <table cellspacing="0" border="0">
                <tr><td colspan="3" class="note"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-note')?>: <?=$arrAdjustmentData['ajst_description']?></td></tr>
                <tr class="signHeader">
                    <th>Gudang</th><th>Accounting</th><th>&nbsp;</th>
                </tr>
                <tr class="signBody">
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                </tr>
                <tr class="signFooter">
                    <td class="a"><span>&nbsp;</span></td><td class="a"><span>&nbsp;</span></td><td>&nbsp;</td>
                </tr>
            </table>
        </td>
        <td colspan="2">
            <table cellspacing="0" border="0">
				<tr>
					<td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
		            <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
				</tr>
			</table>
        </td>
	</tr><?php
    else: ?>
    <tr class="border-top">
        <td colspan="3" class="next-page">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor;