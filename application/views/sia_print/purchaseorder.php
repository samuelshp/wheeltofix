<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrPurchaseItem)+count($arrPurchaseBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;

if(!empty($arrPurchaseItem)) for($i = 0; $i < $intTotalPage; $i++):?>
<div class="container">
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader">
		<tr>
			<td class="l">
				<table cellspacing="0" border="0">
					<tr>
						<td>
							<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
							<h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
						</td>
						<td class="r">
							<table cellspacing="0" border="0">
								<tr>								
									<td style="float:right"><h2><?=$arrCompanyInfo['strOwnerAddress'].", ".formatDate2($arrPurchaseData['cdate'],'d F Y')?></h2></td>
								</tr>
							</table>						
						</td>						
					</tr>			
					<tr>
						<td>
							<h2><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder')?></h2>
						</td>												
					</tr>
					<tr>
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptanceproject')?> : <?=$arrPurchaseData['kont_name']?></td>									
					</tr>
					<tr>
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbnumber')?> : <?=$arrPurchaseData['pror_code']?></td>									
					</tr>					
				</table>
			</td>		
		</tr>		
	</table>		
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">	
	<tr class="titleList border-top border-bottom">		
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?></th>	
		<th>Satuan</th>	
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>		
		<th>Keterangan</th>		
	</tr>
	<?php		
        $intItemCount=0;
        if(!empty($arrPurchaseItem))$intItemCount+=count($arrPurchaseItem);
        if(!empty($arrPurchaseBonusItem))$intItemCount+=count($arrPurchaseBonusItem);
        for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
		 ?>
    <?php if($j<count($arrPurchaseItem)){
        $intTotalPriceSoFar += $arrPurchaseItem[$j]['proi_subtotal'];?>
        <tr>                        
			<td class="quantity"><?=$arrPurchaseItem[$j]['proi_quantity1']?></td>
			<td><?=$arrPurchaseItem[$j]['sat_pb']?></td>
			<td class="title"><b><?=substr($arrPurchaseItem[$j]['prod_title'],0,$intProductTitleLength)?></b></td>            
			<td><?=$arrPurchaseItem[$j]['proi_description']?></td>            
        </tr>
    <?php }else{ ?>
        <tr>
			<td class="quantity"><?=$arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['proi_quantity1'].' '.formatUnitName($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['proi_unit1'])?></td>            
            <td class="title"><b><?=substr($arrPurchaseBonusItem[$j-count($arrPurchaseItem)]['strName'],0,$intProductTitleLength)?> (*)</b></td>
        </tr>
    <?php } ?>
	<?php
	endfor;
	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="3">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
	    <td colspan="4">
			<table cellspacing="0" border="0">				
				<tr class="signHeader">
					<th style="width:25%;">Pelaksana,</th><th style="width:25%;">Cost Control,</th><th style="width:25%;">Mengetahui PM,</th><th style="width:25%;">Purchasing,</th>
				</tr>
				<tr class="signBody">
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr class="signFooter">
					<td class="a"><span><?=$arrPurchaseData['adlg_name']?></span></td><td class="a"><span><?=$arrPurchaseData['adlg_name_cc']?></span></td><td class="a"><span><?=$arrPurchaseData['adlg_name_pm']?></span></td><td class="a"><span><?=$arrPurchaseData['adlg_name_pr']?></span></td>
				</tr>
			</table>
		</td>	  
	</tr>
	<td>&nbsp;</td>	
	<tr class="signHeader">
		<td>
			<h3><b>Dibuat oleh <?=$arrAcceptanceData['adlg_name']?></b></h3>
		</td>
	</tr>
	<?php
    else: ?>  
    <tr class="border-top">
        <td colspan="5">Lanjut Ke Halaman Berikut</td>
        <td class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
        <td class="subTotal"><?=setPrice($intTotalPriceSoFar)?></td>
    </tr><?php
    endif; ?>  
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>