<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint6cm.css')?>"  media="all" /><?php
$strAutoProceedURL = site_url('invoice/index/cashier', NULL, FALSE);
require_once('_autoproceed.php');
?>
<div class="container">
	<h1 class="title" style="font-size: 13px; text-decoration: underline;"><?=$arrCompanyInfo['strCompanyName']?></h1>
	<!-- <p class="title"><?=$arrCompanyInfo['strOwnerAddress']?></p> -->
	<!-- <p class="title"><?=$arrCompanyInfo['strOwnerPhone']?></p> -->
	<p class="title">
		<?=$arrInvoiceData['invo_code']?><br>
		<?=formatDate2($arrInvoiceData['invo_date'],'d F Y H:i')?>
	</p>
	<table cellspacing="0" border="0" id="invoiceItemList"><?php
$i = 0;
$intTotal = 0;
if(!empty($arrInvoiceItem)) foreach($arrInvoiceItem as $arrItemEach): ?>  
	<tr><td colspan="4" class="title"><?=$arrItemEach['invi_description']?></td></tr>
	<tr>
		<td class="quantity"><?php
			$intQty = convertQuantity(array($arrItemEach['invi_quantity1'], $arrItemEach['invi_quantity2'], $arrItemEach['invi_quantity3']), $arrItemEach['invi_product_id'], 'concluded');
			echo formatProductUnit($arrItemEach['invi_product_id'],$intQty, 'separated');
		?></td>
		<td colspan="<?=!empty($arrItemEach['invi_discount1']) ? 1 : 2?>" class="price"><?=setPrice($arrItemEach['invi_price'], 'BASE', FALSE)?></td><?php
	if(!empty($arrItemEach['invi_discount1'])): ?>  
		<td class="price"><?=$arrItemEach['invi_discount1']?>%</td><?php 
	endif; ?>
		<td class="subTotal"><?=setPrice($arrItemEach['invi_subtotal'], 'BASE', FALSE)?></td>
	</tr><?php
	$i++;
endforeach; ?>  
	<tr class="subTotalHeader">
		<td colspan="3" class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></td>
		<td class="subTotal"><?php
			echo setPrice($intInvoiceGrandTotal, 'BASE', FALSE, FALSE, TRUE);
		?></td>
	</tr><?php
if(1 == 2): ?>
	<tr class="subTotalHeader">
		<td colspan="3" class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-tax')?></td>
		<td class="subTotal"><?=$arrInvoiceData['invo_tax']?>%</td>
	</tr>
	<tr class="subTotalHeader">
		<td colspan="3" class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></td>
		<td class="subTotal"><?=setPrice($intInvoiceGrandTotal, 'BASE', FALSE, FALSE, TRUE)?></td>
	</tr><?php
endif;
if(!empty($arrInvoiceData['invo_discount'])): ?>  
	<tr class="subTotalHeader">
		<td colspan="3" class="title">Discount / DP</td>
		<td class="subTotal"><?=setPrice($arrInvoiceData['invo_discount'], 'BASE', FALSE)?></td>
	</tr><?php
endif;
if(empty($txtPayment) && !empty($arrInvoiceData['invo_payment_value'])) {
	$txtPayment = $arrInvoiceData['invo_payment_value'];
	$txtPaymentChange = abs((int) setPrice($intInvoiceGrandTotal, 'BASE', FALSE, TRUE, TRUE) - (int) $arrInvoiceData['invo_payment_value']);
}
if(!empty($txtPayment)): ?>  
	<tr class="subTotalHeader">
		<td colspan="3" class="title">Pembayaran</td>
		<td class="subTotal"><?=setPrice($txtPayment, 'BASE', FALSE)?></td>
	</tr><?php
endif;
if(!empty($txtPaymentChange)): ?>  
	<tr class="subTotalHeader">
		<td colspan="3" class="title">Kembalian</td>
		<td class="subTotal"><?=setPrice($txtPaymentChange, 'BASE', FALSE)?></td>
	</tr><?php
endif; ?>  
	</table>
	<h4 style="text-align:center;">Terima kasih atas pembelian anda.</h4>
</div>