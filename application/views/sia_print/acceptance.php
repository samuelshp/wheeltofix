<link type="text/css" rel="stylesheet" href="<?=base_url('asset/'.$this->config->item("jw_style").'/invoiceprint21cm.css')?>"  media="all" /><?php
$intItemPerPage = 8;
$intTotalPage = (int) ceil((count($arrAcceptanceItem)+count($arrAcceptanceBonusItem)) / $intItemPerPage);
$intTotalPriceSoFar = 0;
$intProductTitleLength = 40;
$grand_total = 0;

if(!empty($arrAcceptanceItem)) 
for($i = 0; $i < $intTotalPage; $i++): ?>
<div class="container">			
<!-- HEADER -->
	<table cellspacing="0" border="0" id="invoiceHeader">
		<tr>
			<td class="l">
				<table cellspacing="0" border="0">
					<tr>
						<td colspan="3">
							<h2><?=$arrCompanyInfo['strCompanyName']?></h2>
							<h2><?=$arrCompanyInfo['strOwnerAddress']?></h2>
						</td>
					</tr>			
					<tr style="font-size:12px">
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptedfrom')?></td>				
						<td colspan="2"><?=$arrAcceptanceData['supp_name']?></td>				
					</tr>
					<tr style="font-size:12px">
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbproject')?></td>				
						<td><?=$arrAcceptanceData['kont_name']?></td>
						<td>
							<table cellspacing="0" border="0">
								<tr style="font-size:12px; float:right">								
									<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-receiptacceptancenumber')?></td>							
									<td><?=$arrAcceptanceData['acce_code']?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr style="font-size:12px">
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-pbnumber')?></td>				
						<td colspan="2"><?=$arrAcceptanceData['pror_code']?></td>				
					</tr>	
					<tr style="font-size:12px">
						<td><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-ponumber')?></td>				
						<td colspan="2"><?=$arrAcceptanceData['prch_code']?></td>				
					</tr>				
				</table>
			</td>		
		</tr>		
	</table>	
<!-- ITEMS -->
	<table cellspacing="0" border="0" id="invoiceItemList">
	<tr>
		<td colspan="7"><h2><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-receiptacceptancebrowse')?></h2></td>
	</tr>
	<!-- <tr>
			<td colspan="3">Diisi Oleh Adm. Gudang</td>
			<td colspan="2">Diisi Oleh Adm. Penjualan</td>
	</tr> -->
	<tr class="titleList border-top border-bottom">	
		<th class="number">No.</th>	
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> TERIMA</th>
		<th class="quantity"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-qty')?> BAYAR</th>
		<th class="title"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-productname')?></th>
		<th class="price"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-price')?></th>
		<th class="subTotal"><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-total')?></th>
	</tr><?php
	$intItemCount=0;	
	if(!empty($arrAcceptanceItem))$intItemCount+=count($arrAcceptanceItem); 
	for($j = ($i * $intItemPerPage); $j < $intItemCount && $j < (($i + 1) * $intItemPerPage); $j++):
		 ?>
    <?php if($j<count($arrAcceptanceItem)){?>
        <tr>
            <td class="number"><b><?=$j + 1?></b></td>
            <td class="quantity"><?=$arrAcceptanceItem[$j]['acit_quantity1'].' ('.$arrAcceptanceItem[$j]['satuan_terima'].')'?></td>
			<td class="quantity"><?=$arrAcceptanceItem[$j]['acit_quantity_bayar'].' ('.$arrAcceptanceItem[$j]['satuan_bayar'].')'?></td>
            <td><b><?=substr($arrAcceptanceItem[$j]['prod_title'],0,$intProductTitleLength)?></b></td>            
            <td class="price"><?=setPrice($arrAcceptanceItem[$j]['prci_price'],'BASE',FALSE)?></td>           
            <td class="subTotal"><?=setPrice($arrAcceptanceItem[$j]['acit_quantity_bayar']*$arrAcceptanceItem[$j]['prci_price'],'BASE',FALSE)?></td>
			<?php $grand_total += $arrAcceptanceItem[$j]['acit_quantity_bayar']*$arrAcceptanceItem[$j]['prci_price']; $intTotalPriceSoFar = $grand_total;?>
        </tr>
        <?php }
	endfor;
	if($j < $intItemPerPage) for(; $j < $intItemPerPage; $j++): ?>  
	<tr><td colspan="7">&nbsp;</td></tr><?php
	endfor;
	if($i == $intTotalPage - 1): ?>  
<!-- FOOTER -->
	<tr class="subTotalHeader border-top">
		<td colspan="3">
			<table cellspacing="0" border="0">
				<tr class="signHeader">
					<td colspan="2" style="text-align:left">Surabaya, <?=formatDate2($arrAcceptanceData['acce_date'],'d F Y')?></td>
				</tr>
				<tr class="signHeader">
				<?php if($arrAcceptanceData['adlg_name'] == null || $arrAcceptanceData['adlg_name'] == '') $arrAcceptanceData['adlg_name']= "Administrator"?>
					<td colspan="2" style="text-align:left">Dibuat oleh <b><?=$arrAcceptanceData['adlg_name']?></b> </td>
				</tr>
			</table>
		</td>
		<td>&nbsp;</td>		
		<td colspan="4">
			<table cellspacing="0" border="0">
				<tr>
					<td class="title"><h3><b><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-grandtotal')?></b></h3></td>
					<td class="subTotal" ><h3><b><?=setPrice($grand_total,'BASE',FALSE)?></b></h3></td>		
				</tr>				
			</table>
		</td>
	</tr>	
	<?php
    else: ?>
    <tr class="border-top">
		<td colspan="4">Lanjut Ke Halaman Berikut</td>
		<div colspan="4">
			<td class="title" colspan="1"><h3><?=loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-total')?></h3></td>
			<td class="subTotal"><h3><?=setPrice($intTotalPriceSoFar,'BASE',FALSE)?></h3></td>
		</div>
    </tr><?php
    endif; ?>
	</table>
</div><?php
	if($i < $intTotalPage - 1) echo '<div class="pageBreak">&nbsp;</div>';
endfor; ?>