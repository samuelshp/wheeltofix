<?php
$_CI =& get_instance();

# Collect Data
if(empty($content_mode)) {
	$strViewFolder = $this->config->item('jw_style');
	$strContentViewFolder = $strViewFolder;
} else {
	$strViewFolder = $this->config->item('jw_admin_style');
	$strContentViewFolder = $content_mode;
}

switch($strViewFile) {
	case 'administrator/login': 
		$bolNavigation = FALSE; $bolFooter = FALSE;
	break; default:
		$bolNavigation = TRUE; $bolFooter = TRUE;
	break;
}

$arrData = array_merge($_CI->getArrData(),$this->_ci_cached_vars,array(
	'strViewFolder' => $strViewFolder,
	'strContentViewFolder' => $strContentViewFolder,
));

# Head & Meta
if(!empty($strPageTitle)) $this->template->title->set($strPageTitle)->append(' - '.$this->config->item('jw_website_name'));

# Display
$this->template->header->view("{$strViewFolder}/header",$arrData,TRUE);
if($bolNavigation) $this->template->navigation->widget('AdmNavigation',$arrData,TRUE);
$this->template->content->view("{$strContentViewFolder}/{$strViewFile}",$arrData,TRUE);
if($bolFooter) $this->template->footer->view("$strViewFolder/footer",$arrData,TRUE);

# CSS
$this->template->stylesheet->add(base_url('asset/jquery-ui/jquery-ui.min.css'));
$this->template->stylesheet->add(base_url('asset/bootstrap/bootstrap.min.css'));
$this->template->stylesheet->add(base_url("asset/$strViewFolder/layout.css"));
if($strContentViewFolder == 'sia') {
	$this->template->stylesheet->add(base_url("asset/$strViewFolder/layout-sia.css"));
}
switch($strViewFile) {
	case 'main/massmail': 
		$this->template->stylesheet->add(base_url('asset/chosen/chosen.min.css'));
	break; case 'setting/index': 
		$this->template->stylesheet->add(base_url('asset/fancybox/jquery.fancybox.css'));
	break; case 'questionnaire/index': 
		$this->template->stylesheet->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css'));
	break; case 'table/browse': 
		$this->template->stylesheet->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css'));
	break; case 'table/add': case 'table/edit': 
		$this->template->stylesheet->add(base_url('asset/fancybox/jquery.fancybox.css'));
		$this->template->stylesheet->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.css'));
		$this->template->stylesheet->add(base_url('asset/chosen/chosen.min.css'));
	break;
}

# JS
// $this->template->javascript->add(base_url('asset/jquery/jquery.min.js'));
$this->template->javascript->add(base_url('asset/jquery/jquery.cookie.js'));
$this->template->javascript->add(base_url('asset/jquery-ui/jquery-ui.min.js'));
$this->template->javascript->add(base_url('asset/bootstrap/bootstrap.min.js'));
switch($strViewFile) {
	case 'administrator/edit': 
		$this->template->javascript->add(base_url('asset/jquery/jquery.validate.min.js'));
	break; case 'main/massmail': 
		$this->template->javascript->add(base_url('asset/jquery/jquery.validate.min.js'));
		$this->template->javascript->add(base_url('asset/tinymce/tinymce.min.js'));
		$this->template->javascript->add(base_url('asset/chosen/chosen.jquery.min.js'));
	break; case 'setting/index': 
		$this->template->javascript->add(base_url('asset/jquery/jquery.validate.min.js'));
		$this->template->javascript->add(base_url('asset/fancybox/jquery.fancybox.pack.js'));
	break; case 'questionnaire/index': 
		$this->template->javascript->add(base_url('asset/jquery/jquery.validate.min.js'));
		$this->template->javascript->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js'));
	break; case 'table/browse': 
		$this->template->javascript->add(base_url('asset/jquery-table2excel/jquery.table2excel.min.js'));
		$this->template->javascript->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js'));
	break; case 'table/add': case 'table/edit': 
		$this->template->javascript->add(base_url('asset/fancybox/jquery.fancybox.pack.js'));
		$this->template->javascript->add(base_url('asset/tinymce/tinymce.min.js'));
		$this->template->javascript->add(base_url('asset/jquery/jquery.validate.min.js'));
		$this->template->javascript->add(base_url('asset/jquery/jquery.number.js'));
		$this->template->javascript->add(base_url('asset/jquery-ui/jquery-ui-timepicker-addon.js'));
		$this->template->javascript->add(base_url('asset/chosen/chosen.jquery.min.js'));
		$this->template->javascript->add(base_url('asset/adminpage/tabledataoperation.js'));
	break; 
}
if(substr($strViewFile, 0,6) == 'report') $this->template->javascript->add(base_url('asset/jquery-table2excel/jquery.table2excel.min.js'));

# Additional Script
if($strContentViewFolder == 'sia') {
	if(file_exists(APPPATH."/views/{$strContentViewFolder}/{$strViewFile}_js.php"))
		$this->template->customScript->parse("{$strContentViewFolder}/{$strViewFile}_js.php",$arrData);
	if(!empty($arrData['strIncludeJsFile']) && file_exists(APPPATH."/views/{$strContentViewFolder}/{$arrData['strIncludeJsFile']}_js.php")) {
		if(!is_array($arrData['strIncludeJsFile']))
			$arrData['strIncludeJsFile'] = array($arrData['strIncludeJsFile']);
		foreach($arrData['strIncludeJsFile'] as $e)
			$this->template->customScript->parse("{$strContentViewFolder}/{$e}_js.php",$arrData);
	}
} elseif(!empty($arrData['arrTableListData']['custom_file']) && file_exists(APPPATH.'/views/'.$arrData['arrTableListData']['custom_file'])) {
	$this->template->customScript->parse($arrData['arrTableListData']['custom_file'],$arrData);
}

# Publish
$this->template->publish("$strViewFolder/layout");