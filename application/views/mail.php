<?php
$_CI =& get_instance();

# Collect Data
$strViewFolder = 'mail';
$bolSignature = TRUE;

switch($strViewFile) {
	case 'adm/newsletter': 
		$strPageTitle = $strTitle; $bolSignature = FALSE;
	break; case 'adm/invoiceupdate': 
		$strPageTitle = 'New Invoice';
	break; case 'ui/comment': case 'ui/guestbook': 
		$strPageTitle = 'Comment';
	break; case 'ui/invoice': 
		$strPageTitle = 'Invoice';
	break; case 'ui/lostpassword': 
		$strPageTitle = 'Password Reset';
	break; case 'ui/register': 
		$strPageTitle = 'Confirmation Of Registration';
	break; default:
	break;
}

$arrData = array_merge($_CI->getArrData(),$this->_ci_cached_vars,array(
	'strViewFolder' => $strViewFolder
));

# Head & Meta
if(!empty($strPageTitle)) $this->template->title = $strPageTitle;
$this->template->description = '';

# Display
$this->template->header->view("$strViewFolder/header",$arrData,TRUE);
$this->template->content->view("$strViewFolder/$strViewFile",$arrData,TRUE);
if($bolSignature) $this->template->content->view("$strViewFolder/signature",$arrData,FALSE);
$this->template->footer->view("$strViewFolder/footer",$arrData,TRUE);

# CSS

# JS

# Publish
$this->template->publish("$strViewFolder/layout");