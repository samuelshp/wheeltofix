<?php
$_CI =& get_instance();
$_CI->load->model('Madmlinklist','feature');

# Collect Data
$strViewFolder = $this->config->item('jw_style');

$arrInitializedData = $_CI->getArrData();
$arrData = array_merge($arrInitializedData,$this->_ci_cached_vars,array(
	'strViewFolder' => $strViewFolder,
	'bolStatisticAvlb' => $this->feature->getAvailability(101), # Statistic
));

if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-data.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-data.php');

# Head & Meta
if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-head.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-head.php');

# Widgets
if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-widgets.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-widgets.php');

# Display
if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-display.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-display.php');

# CSS
if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-css.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-css.php');

# JS
if(is_file(APPPATH.'/views/'.$strViewFolder.'/theme-js.php')) include_once(APPPATH.'/views/'.$strViewFolder.'/theme-js.php');


# Publish
$this->template->publish("$strViewFolder/layout");