<?xml version="1.0" encoding="UTF-8"?>

<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?=base_url()?></loc> 
        <priority>1.0</priority>
    </url><?php 
function __addNewItem($strLink,$strPriority) { ?>  
    <url>
        <loc><?=$strLink?></loc>
        <priority><?=$strPriority?></priority>
    </url><?php
}
    if(!empty($arrNavBar)) foreach($arrNavBar as $e) 
        if($e['shor_link'] != site_url()) __addNewItem($e['shor_link'],'0.75');
    if(!empty($arrShortcut)) foreach($arrShortcut as $e) 
        if($e['shor_link'] != site_url()) __addNewItem($e['shor_link'],'0.75');
    if(!empty($arrBlogCategory)) foreach($arrBlogCategory as $e) 
        if(!empty($e['bloc_slug'])) __addNewItem(site_url($e['bloc_slug']),'0.5');
    if(!empty($arrProductCategory)) foreach($arrProductCategory as $e) 
        if(!empty($e['proc_slug'])) __addNewItem(site_url($e['proc_slug']),'0.5');
    if(!empty($arrBlog)) foreach($arrBlog as $e) if(!empty($e['blog_slug'])) 
        __addNewItem(site_url($e['blog_slug']),'0.25');
    if(!empty($arrProduct)) foreach($arrProduct as $e) if(!empty($e['prod_slug'])) 
        __addNewItem(site_url($e['prod_slug']),'0.25');
?>  
</urlset>