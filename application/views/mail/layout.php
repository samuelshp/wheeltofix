<!DOCTYPE html>
<html lang="<?=$this->config->item('jw_language_abbr')?>">
<head>
	<meta charset="utf-8" />
	<title><?=$this->template->title->default('Email from '.$this->config->item("jw_website_name"))?></title>
</head>

<style type="text/css"><?php 
	include('asset/mail/layout.css'); ?>  
</style>

<body>
	<div class="container">
		<header>
			<?=$this->template->header?>  
		</header>
		<main>
			<?=$this->template->content?>  
		</main>
		<footer>
			<?=$this->template->footer?>  
		</footer>
	</div>
</body>
</html>