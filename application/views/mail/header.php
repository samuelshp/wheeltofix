<?php
if($this->config->item('jw_website_logo') != ''): ?>  
	<h1>
		<img src="<?=str_replace('<jw:site_url>',base_url(),$this->config->item('jw_website_logo'))?>" title="<?=$this->config->item('jw_website_motto')?>" alt="<?=$this->config->item('jw_website_name')?>" />
	</h1><?php
else: ?>  
	<h1><?=$this->config->item('jw_website_name')?></h1>
	<h4><?=$this->config->item('jw_website_motto')?></h4><?php
endif; ?>  
	<time><?=date('d F Y, H:i (T)')?></time>