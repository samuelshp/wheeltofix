	<h2><?=$strMailTitle?></h2>
	<p>There was a password reset request from your account <b><?=$strMemberLogin?></b></p>
	
	<blockquote>Your new password is: <b><?=$strNewPassword?></b></blockquote>
	
	<p>&nbsp;</p>
	<p>If you didn't register at our website and receive this email accidentally, please inform us via email at <strong><?=$strOwnerEmail?></strong>. Thank you for your notice.</p>