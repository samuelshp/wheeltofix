	<h2><?=$strMailTitle?></h2>
	<p>Your email has been registered as new member of <b><?=$strServerName?></b> with registration data:</p>

	<p>
		Full Name: <b><?=$strMemberName?></b><br />
		Login Name: <b><?=$strMemberLogin?></b>
	</p>

	<blockquote>
		Please confirm it <a href="<?=site_url('member/register_confirm/'.$strMemberLogin.'/'.sha1($strEmailAddress))?>">here</a>.<br />
		Or you can copy and paste the link below to your URL.<br /><br />
		<b><?=site_url('member/register_confirm/'.$strMemberLogin.'/'.sha1($strEmailAddress))?></b>
	</blockquote>
	
	<p>&nbsp;</p>
	<p>If you didn't register at our website and receive this email accidentally, please inform us via email at <strong><?=$strOwnerEmail?></strong>. Thank you for your notice.</p>