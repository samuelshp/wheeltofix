	<h2>Order Confirmation #<?=$intFormattedInvoiceID?></h2>
	<h4><b>Dear <?=$strInvoiceName?>,</b></h4>
	<p>Thank you for shopping with us. Your order is currently awaiting payment via bank transfer.</p>

	<p>&nbsp;</p>
	<h4>Order Details</h4>
	<table cellspacing="0">
	<thead>
		<tr><th>Qty</th><th>Product Name</th><th>Price</th><th>Amount</th></tr>
	</thead>
	<tbody>
		<?=$strInvoiceItem?>
	</tbody>
		<tr><th colspan="3" style="text-align:right;">Total</th><th><?=$intGrandTotal?></th></tr>
	</table>

	<p>Please do re-check your order data below to help us contact you.</p>
	<blockquote>
		Name: <?=$strInvoiceName?><br />
		Email: <?=$strMemberEmail?><br />
		Address: <?=$strInvoiceAddress?><br />
		Phone: <?=$strInvoicePhone?><br />
	</blockquote>

	<p>&nbsp;</p>
	<p>To check your invoice status, you can click <a href="<?=site_url('invoice/index/'.$this->encrypt->encode($intInvoiceID))?>">here</a> or copy paste URL below</p>
	<p style="color:#C89F18;"><?=site_url('invoice/index/'.$this->encrypt->encode($intInvoiceID))?></p>

	<p>&nbsp;</p>
	<p>We will contact you soon for any other detail needed to complete the transaction.</p>
	<p>If you didn't register at our website and receive this email accidentally, please inform us via email at <strong><?=$strOwnerEmail?></strong>. Thank you for your business with us.</p>