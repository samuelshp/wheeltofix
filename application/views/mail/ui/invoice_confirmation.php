<h2>Order Confirmation #<?=$intFormattedInvoiceID?> has been received.</h2>
<h4><b>Dear <?=$strInvoiceName?>,</b></h4>
<p>Thank you for shopping with us. We already received your payment confirmation.</p>

<p>&nbsp;</p>
<p>To check your invoice status, you can click <a href="<?=site_url('invoice/index/'.$this->encrypt->encode($intInvoiceID))?>">here</a> or copy paste URL below</p>
<p style="color:#C89F18;"><?=site_url('invoice/index/'.$this->encrypt->encode($intInvoiceID))?></p>

<p>&nbsp;</p>
<p>We will process your order and contact you soon.</p>
<p>If you didn't register at our website and receive this email accidentally, please inform us via email at <strong><?=$strOwnerEmail?></strong>. Thank you for your business with us.</p>