<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:admin="http://webns.net/mvcb/"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:content="http://purl.org/rss/1.0/modules/content/">

<channel>
	<title><?=$this->config->item('jw_website_name')?></title>
	<link><?=site_url('feed')?></link>
	<description><?=$this->config->item('jw_meta_description')?></description>
	<dc:language><?=$this->config->item('jw_language_abbr')?></dc:language>
	<dc:creator><?=$arrOwnerInfo['strOwnerEmail']?></dc:creator>
	<dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
	<admin:generatorAgent rdf:resource="http://www.tobsite.com/" /><?php
function __addNewItem($strTitle,$strLink,$strDescription,$strDate) { ?>  
    <item>
		<title><?=$strTitle?></title>
		<link><?=$strLink?></link>
		<guid><?=$strLink?></guid>

		<description><![CDATA[ <?=$strDescription?> ]]></description>
		<pubDate><?=$strDate?></pubDate>
	</item><?php
} 
	
	if(!empty($arrBlog)) foreach($arrBlog as $e) 
		__addNewItem($e['blog_title'],$e['blog_slug'],$e['blog_content'],$e['blog_rawdate']);
	if(!empty($arrProduct)) foreach($arrProduct as $e) 
		__addNewItem($e['prod_title'],$e['prod_slug'],$e['prod_description'],$e['prod_rawdate']);

?>  
	
</channel>
</rss>