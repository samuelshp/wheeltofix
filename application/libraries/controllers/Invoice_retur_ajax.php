<?php

class Invoice_retur_ajax extends JW_Controller {

public function getProductAutoComplete($txtData='') {
	# INIT
	$this->load->model('Minvoicereturitem');
	$arrProduct = $this->Minvoicereturitem->getAllLastSaleItems($txtData);
	if(!empty($arrProduct)) {
		for($i = 0; $i < count($arrProduct); $i++) {
			$arrProduct[$i]['strName'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
			$arrProduct[$i]['strNameWithPrice'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['invi_price'],$arrProduct[$i]['prod_currency']));
		}

		ArrayToXml(array('Product' => $arrProduct));
	}
}

public function getLatestPrice($intProductID,$intCustomerID) {
    # INIT
    $this->load->model('Minvoiceitem');
    $arrProduct = $this->Minvoiceitem->getLatestPriceByCustomer($intProductID,$intCustomerID);
    if(!empty($arrProduct))
        ArrayToXml(array('Product' => $arrProduct));

}

public function getSaleOrderCustomerAutoCompleteWithSupplierBindRetur($txtData='',$intSupplier='') {
    #INIT
    $this->load->model('Mcustomer');
    ArrayToXml(array('Customer' => $this->Mcustomer->getAllCustomerWithSupplierBindForRetur($txtData,$intSupplier)));
}

}