<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)
- getDataForPurchase(intID)
- getBonusDataForPurchase(intID) //gak dipake
- getPurchaseOrderAutoComplete(txtData)
- getPurchaseOrderSupplierAutoComplete(txtData)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase_order extends JW_Controller {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
	
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
		$this->load->model('Mpurchaseorder');
		$this->load->model('Mpurchaseorderitem');
	
		$this->_getMenuHelpContent(21,true,'adminpage');
	}
	
	public function index() {
		// WARNING! Don't change the following steps
		if($this->input->post('smtUpdatePurchase') != '' || $this->input->post('smtMakePurchase') != '') {
		}
	
		if($this->input->post('smtMakePurchase') != '') { // Make Purchase
			$lol;	
				// $this->load->model('Munit');
				// $arrPurchase=array(
				// 	'intSupplier' => $this->input->post('IDSupp'),
				// 	'intWarehouse' => $this->input->post('WarehouseID'),
				// 	'intTax' => $this->input->post('txtTax'),
				// 	'strDescription' => $this->input->post('txaDescription'),
				// 	'strDate' => $this->input->post('txtDate'),
				// 	'strExpDate'=> $this->input->post('txtExpDate'),
				// 	'intDisc'=> $this->input->post('dsc4')
				// );
				// $intPurchaseID = $this->Mpurchaseorder->add($arrPurchase['intSupplier'],$arrPurchase['intWarehouse'],$arrPurchase['strDescription'],$arrPurchase['intTax'],0,$arrPurchase['strDate'],$arrPurchase['strExpDate'],$arrPurchase['intDisc']);
			
			
			echo "<script>console.log($intPurchaseID)</script>";

			$arrPurchaseOrder = array(
				// 'txtNoPB' => $this->input->post('txtNoPB'),
				'txtDateBuat' => $this->input->post('txtDateBuat'),
				'txtDescription' => $this->input->post('txtaDescription'),
				'kontrakID' => $this->input->post('kontrakID')
			);
			$intPurchaseID = $this->Mpurchaseorder->addPO($arrPurchaseOrder['txtDateBuat'], $arrPurchaseOrder['txtDescription'], $arrPurchaseOrder['kontrakID']);
			$hasil = generateTransactionCode($date, $intPurchaseID, 'purchase', TRUE);
			$this->Mpurchaseorder->addPurchaseCode($hasil, $intPurchaseID);
			//yang lama -> $intPurchaseID = $this->Mpurchaseorder->add($arrPurchase['intSupplier'],$arrPurchase['intWarehouse'],$arrPurchase['strDescription'],$arrPurchase['intTax'],0,$arrPurchase['strDate'],$arrPurchase['strExpDate'],$arrPurchase['intDisc']);
			$totalItem = $this->input->post('totalItem');
			for($i = 0; $i < $totalItem; $i++) {
				$title = $this->input->post('isiBahanText'.$i);//bahan
				$id = $this->input->post('idProduct'.$i);//id product
				$qty = $this->input->post('inputQty'.$i);//yang di input dan masukkan ke ter PB juga di skm material
				$satuan = $this->input->post('isiSatuanText'.$i);//satuan dari PB
				$keterangan = $this->input->post('isiKeterangan'.$i);// keterangan
				$cdate = $this->input->post('todayDate');//cdate
				$subkontrakID = $this->input->post('subKontrakID');//ganti int puch id
				$noPB = $this->input->post('txtNoPB');
				$this->Mpurchaseorderitem->addToPurchaseOrderItem($intPurchaseID, $id, $keterangan, $qty, $satuan, $cdate);
				$this->Mpurchaseorderitem->updateTerPB($qty, $id);
				$lol;
					// $qty1Item = $this->input->post('qty1PriceEffect'.$i);
					// $qty2Item = $this->input->post('qty2PriceEffect'.$i);
					// $qty3Item = $this->input->post('qty3PriceEffect'.$i);
					// $unit1Item = $this->input->post('sel1UnitID'.$i);
					// $unit2Item = $this->input->post('sel2UnitID'.$i);
					// $unit3Item = $this->input->post('sel3UnitID'.$i);
					// $prcItem = $this->input->post('prcPriceEffect'.$i);
					// $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
					// $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
					// $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
					// $prodItem = $this->input->post('prodItem'.$i);
					// $probItem = $this->input->post('probItem'.$i);
					// $strProductDescription = formatProductName('',$prodItem,$probItem);
					// $idItem = $this->input->post('idItem'.$i);
					// if($strProductDescription!='' || !empty($strProductDescription)) {
					// 	$this->Mpurchaseorderitem->add($intPurchaseID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
					// }
			}
			$lol;
				// $totalItemBonus=$this->input->post('totalItemBonus');
				// for($i = 0; $i < $totalItemBonus; $i++) {
				// 	$qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
				// 	$qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
				// 	$qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
				// 	$unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
				// 	$unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
				// 	$unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
				// 	$prodItemBonus = $this->input->post('prodItemBonus'.$i);
				// 	$probItemBonus = $this->input->post('probItemBonus'.$i);
				// 	$strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
				// 	$idItemBonus = $this->input->post('idItemBonus'.$i);
				// 	if($strBonusProductDescription!='' || !empty($strBonusProductDescription)){
				// 		$this->Mpurchaseorderitem->add($intPurchaseID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
				// 	}
				// }

	
			$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseordermade'));
			redirect('purchase_order/view/'.$intPurchaseID);
		}
	
		// Load the views
		$this->load->view('sia',array(
			'strViewFile' => 'purchaseorder/add',
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addpurchaseorder')
		));
	}
	
	// To display, edit and delete purchase
	public function view($intID = 0) {

		if($this->input->post('backPurchase') != ''){
			redirect('purchase_order/browse');
		}

		if($this->input->post('ccEdit') != ''){
			$prorCode = $this->input->post('prorCode');
			echo "<script>console.log($prorCode)</script>";
			$this->Mpurchaseorder->editCCStatus($prorCode);

			$this->session->set_flashdata('strMessage','Updated status cost control');
			redirect('purchase_order/browse');
		}

		if($this->input->post('pmEdit') != ''){
			$prorCode = $this->input->post('prorCode');
			echo "<script>console.log($prorCode)</script>";
			$this->Mpurchaseorder->editPMStatus($prorCode);

			$this->session->set_flashdata('strMessage','Updated status PM');
			redirect('purchase_order/browse');
		}

		if($this->input->post('unccEdit') != ''){
			$prorCode = $this->input->post('prorCode');
			echo "<script>console.log($prorCode)</script>";
			$this->Mpurchaseorder->uneditCCStatus($prorCode);

			$this->session->set_flashdata('strMessage','Updated status cost control');
			redirect('purchase_order/browse');
		}

		if($this->input->post('unpmEdit') != ''){
			$prorCode = $this->input->post('prorCode');
			echo "<script>console.log($prorCode)</script>";
			$this->Mpurchaseorder->uneditPMStatus($prorCode);

			$this->session->set_flashdata('strMessage','Updated status PM');
			redirect('purchase_order/browse');
		}
	
		$this->load->model('Mpurchase');

	if($this->input->post('subSave') != '' && $intID != '') { # SAVE

		// Purchase
		$arrPurchaseData = $this->Mpurchaseorder->getItemByID($intID);
		if(compareData($arrPurchaseData['pror_status'],array(0))) {
			$this->Mpurchaseorder->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('dsc4'),$this->input->post('selStatus'),$this->input->post('txtTax'),$this->input->post('selEditable'));
			// Load purchase item
			$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetPurchasedItemsIDByID($intID);
			$arrPostQty1 = $this->input->post('txtItem1Qty');
			$arrPostQty2 = $this->input->post('txtItem2Qty');
			$arrPostQty3 = $this->input->post('txtItem3Qty');
			$arrPostPrice = $this->input->post('txtItemPrice');
			$arrPostDisc1 = $this->input->post('txtItem1Disc');
			$arrPostDisc2 = $this->input->post('txtItem2Disc');
			$arrPostDisc3 = $this->input->post('txtItem3Disc');

			$intTotalPrice = 0;
			$arrContainID = $this->input->post('idProiID');
			foreach($arrPurchaseItem as $e) {
				if(!empty($arrContainID)) {
					if (in_array($e['id'], $arrContainID)) {
						$this->Mpurchaseorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);

					} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
				} else $this->Mpurchaseorderitem->deleteByID($e['id']);
				
			}
			$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetPurchasedBonusItemsIDByID($intID);
			$arrPostQty1 = $this->input->post('txtItemBonus1Qty');
			$arrPostQty2 = $this->input->post('txtItemBonus2Qty');
			$arrPostQty3 = $this->input->post('txtItemBonus3Qty');

			$arrContainID = $this->input->post('idProiIDBonus');
			if(!empty($arrPurchaseItem)) foreach($arrPurchaseItem as $e) {
					
				if(!empty($arrContainID)) {
					if(in_array($e['id'], $arrContainID)) {
						$this->Mpurchaseorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
					} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
				} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
			}
			$totalItem=$this->input->post('totalItem');
			for($i = 0; $i < $totalItem; $i++) {
				$qty1Item = $this->input->post('qty1PriceEffect'.$i);
				$qty2Item = $this->input->post('qty2PriceEffect'.$i);
				$qty3Item = $this->input->post('qty3PriceEffect'.$i);
				$unit1Item = $this->input->post('sel1UnitID'.$i);
				$unit2Item = $this->input->post('sel2UnitID'.$i);
				$unit3Item = $this->input->post('sel3UnitID'.$i);
				$prcItem = $this->input->post('prcPriceEffect'.$i);
				$dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
				$dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
				$dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
				$prodItem = $this->input->post('prodItem'.$i);
				$probItem = $this->input->post('probItem'.$i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItem' . $i);
				if($strProductDescription != '' || !empty($strProductDescription)) {
					$this->Mpurchaseorderitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
				}
			}
			$totalItemBonus = $this->input->post('totalItemBonus');
			for($i = 0; $i < $totalItemBonus; $i++) {
				$qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
				$qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
				$qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
				$unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
				$unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
				$unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
				$prodItemBonus = $this->input->post('prodItemBonus'.$i);
				$probItemBonus = $this->input->post('probItemBonus'.$i);
				$strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
				$idItemBonus = $this->input->post('idItemBonus'.$i);
				if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
					$this->Mpurchaseorderitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
				}
			}

		} else $this->Mpurchaseorder->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));

		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderupdated');

	} else if($this->input->post('subDelete') != '' && $intID != '') {
		/* must use this, because item's trigger can't be activated in header trigger */
		$this->Mpurchaseorderitem->deleteByPurchaseID($intID);
		$this->Mpurchaseorder->deleteByID($intID);

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderdeleted'));
		redirect('purchase_order/browse');

	}
	$arrPurchaseData = $this->Mpurchaseorder->getItemByID($intID);
	if(!empty($arrPurchaseData)) {
		$arrPurchased = $this->Mpurchase->Purchase_Order_GetSJByPOID($intID);
		$arrPurchaseData['pror_rawstatus'] = $arrPurchaseData['pror_status'];
		$arrPurchaseData['pror_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrPurchaseData['pror_status']);

		// Load the purchase item
		// punya lama -> $arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetItemsByID($intID);
		$this->load->model('Mpurchaseorderitem');
		$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetItemsByIDBaru($intID);
		echo "<script>alert('".$arrPurchaseItem[0]['id']." ini id');</script>";
		$intPurchaseTotal = 0;
		// $this->load->model('Munit');
		// for($i = 0; $i < count($arrPurchaseItem); $i++) {
		// 	//$arrPurchaseItem[$i]['strName'] = $arrPurchaseItem[$i]['proi_description'].'|'.$arrPurchaseItem[$i]['proc_title'];
		// 	$arrPurchaseItem[$i]['strName'] = $arrPurchaseItem[$i]['proi_description'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit1']);
		// 	$intConv1 = $convTemp[0]['unit_conversion'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit2']);
		// 	$intConv2 = $convTemp[0]['unit_conversion'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit3']);
		// 	$intConv3 = $convTemp[0]['unit_conversion'];
		// 	$arrPurchaseItem[$i]['proi_conv1'] = $intConv1;
		// 	$arrPurchaseItem[$i]['proi_conv2'] = $intConv2;
		// 	$arrPurchaseItem[$i]['proi_conv3'] = $intConv3;
        //     $intPurchaseTotal+=$arrPurchaseItem[$i]['proi_subtotal'];
		// }
		$arrPurchaseData['subtotal_discounted'] = (float) $intPurchaseTotal - (float) $arrPurchaseData['pror_discount'];
		$intPurchaseGrandTotal = (float)$arrPurchaseData['pror_grandtotal'];
        $arrPurchaseData['subtotal_taxed']=(float)$arrPurchaseData['pror_grandtotal']+((float)$arrPurchaseData['pror_grandtotal']*(float)$arrPurchaseData['pror_tax']/100);

		$arrPurchaseItemBonus = $this->Mpurchaseorderitem->Purchase_Order_GetBonusItemsByID($intID);
		if(!empty($arrPurchaseItemBonus)) {
			for($i = 0; $i < count($arrPurchaseItemBonus); $i++) {
				//$arrPurchaseItemBonus[$i]['strName']=$arrPurchaseItemBonus[$i]['proi_description'].'|'.$arrPurchaseItemBonus[$i]['proc_title'];
				$arrPurchaseItemBonus[$i]['strName'] = $arrPurchaseItemBonus[$i]['proi_description'];
			}
		}

	}

	$arrData = array(
		'intPurchaseID' => $intID,
		'intPurchaseTotal' => $intPurchaseTotal,
		'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
		'arrPurchaseItem' => $arrPurchaseItem,
		'arrPurchaseData' => $arrPurchaseData,
		'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
		'arrPurchased' => $arrPurchased,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
	);

	if($this->input->post('editPurchase') != ''){
		$arrClose = $this->input->post('dataClose');
		$arrCloseData = explode(',', $arrClose);
		echo '<script language="javascript">';
		echo 'console.log('.$arrCloseData.')';
		echo '</script>';
		$this->Mpurchaseorder->updateCloseStatus($arrCloseData);
		redirect('purchase_order/view/'.$intID);
	}

	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota

		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'purchaseorder',
		), $arrData));
	} else {
		# Load all other purchase data the user has ever made
		$arrPurchaseList = $this->Mpurchaseorder->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

		$this->load->view('sia',array_merge(array(
	        'strViewFile' => 'purchaseorder/view',
			'arrPurchaseList' => $arrPurchaseList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorderstatus')
	    ), $arrData, $this->admlinklist->getMenuPermission(21,$arrPurchaseData['pror_rawstatus'])));
	}
	
	}
	public function browse($intPage = 0) {
		$this->_getMenuHelpContent(31,true,'adminpage');
		if($this->input->post('subSearch') != '') {
			$noPB = $this->input->post('txtSearchNoPB');
			$noKontrak = $this->input->post('txtSearchNoKontrak');
			echo "<script>console.log($noKontrak);</script>";
			$arrPurchase = $this->Mpurchaseorder->searchBox($noPB, $noKontrak);
	
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('purchase_order/browse', NULL, FALSE).'">[Back]</a>';
			$this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";
		} else {
			$arrPagination['base_url'] = site_url("purchase_order/browse?pagination=true", NULL, FALSE);
			$arrPagination['total_rows'] = $this->Mpurchaseorder->getCount();
			$arrPagination['per_page'] =10;
			$arrPagination['uri_segment'] = 3;
			$this->pagination->initialize($arrPagination);
			$strPage = $this->pagination->create_links();
	
			$strBrowseMode = '';
			if($this->input->get('page') != '') $intPage = $this->input->get('page');
			
			$arrPurchase = $this->Mpurchaseorder->getItemsBaru($intPage,$arrPagination['per_page']);
			$arrApprPO=array();
			array_push($arrApprPO, "");
			$this->load->model('Mpurchaseorderitem');
			//Untuk update status close
			for($i =0; $i<count($arrPurchase); $i++){
				$all_id = $arrPurchase[$i]['id'];
				$po_item = $this->Mpurchaseorderitem->getAllPurchaseOrderItem();
				
				for($j = 0; $j<count($po_item); $j++){
					if($po_item[$j]['subkontrak_terpb'] == $po_item[$j]['jumlah']){
						$this->Mpurchaseorderitem->updateStatusClosed($po_item[$j]['id']);
					}
					if($po_item[$j]['no_pb'] == NULL){
						$no_pb = '123';
					}
					else{
						$no_pb = $po_item[$j]['no_pb'];
					}
				}
				if(!empty($all_id) || $all_id != NULL){
					$hasil_return = $this->Mpurchaseorderitem->checkStatusPO($all_id);
					$id = $hasil_return[0]['id'];
					//echo "<script>alert(".$id.");</script>";
					if($id>0){
						array_push($arrApprPO,"ada");
					}
					else{
						array_push($arrApprPO,"tidak");
					}
				}
			}
			//punya yang lama -> $arrPurchase = $this->Mpurchaseorder->getItems($intPage,$arrPagination['per_page']);
		}
	
		if(!empty($arrPurchase)) for($i = 0; $i < count($arrPurchase); $i++) {
			
			if($arrPurchase[$i]['approvalCC'] == 1){//approve by cost control
				$arrPurchase[$i]['approvalCC'] = '<span class="glyphicon glyphicon-ok"></span>';
			}
			if($arrPurchase[$i]['approvalPM'] == 1){//approve by pm
				$arrPurchase[$i]['approvalPM'] = '<span class="glyphicon glyphicon-ok"></span>';
			}
			$arrPurchase[$i] = array_merge($arrPurchase[$i],$this->admlinklist->getMenuPermission(21,$arrPurchase[$i]['pror_status']));
			// $arrPurchase[$i]['pror_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrPurchase[$i]['pror_status']);
			// $arrPurchase[$i]['pror_total'] = (float) $arrPurchase[$i]['pror_grandtotal']+((float) $arrPurchase[$i]['pror_grandtotal']*(float) $arrPurchase[$i]['pror_tax']/100);
		}
	
		$this->load->model('Mtinydbvo');
		$this->Mtinydbvo->initialize('invoice_status');
	
		$this->load->view('sia',array(
			'strViewFile' => 'purchaseorder/browse',
			'strPage' => $strPage,
			'strBrowseMode' => $strBrowseMode,
			'arrPurchase' => $arrPurchase,
			'arrAccPO' => $arrApprPO,
			'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
			'strSearchKey' => $this->input->post('txtSearchValue'),
			'strSearchDate' => $this->input->post('txtSearchDate'),
			'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
			'intSearchStatus' => $this->input->post('txtSearchStatus'),
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder')
		));
	}
	
	}

/* End of File */