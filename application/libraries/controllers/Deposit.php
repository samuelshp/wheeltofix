<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intDepositID)

PRIVATE FUNCTION:
- __construct()
*/

class Deposit extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mdeposit');

    $this->_getMenuHelpContent(66,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps

	if($this->input->post('smtUpdateDeposit') != '' || $this->input->post('smtMakeDeposit') != '') {
	}

	if($this->input->post('smtMakeDeposit') != '') { // Make Deposit
		$this->load->model('Munit');
		$arrDeposit = array(
			'intCustID' => $this->input->post('customerID'),
			'intPrice' => $this->input->post('txtPrice')
		);

		$intDepositID = $this->Mdeposit->add($arrDeposit['intCustID'],$arrDeposit['intPrice']);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'deposit-depositmade'));
        redirect('deposit/browse');
	}

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'deposit/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'deposit-deposit')
    ));
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrDeposit = $this->Mdeposit->searchByCustomer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('deposit/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Customer with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDeposit) ? count($arrDeposit) : '0')." records).";
    } else {
		$arrPagination['base_url'] = site_url("deposit/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Mdeposit->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrDeposit = $this->Mdeposit->getItems($intPage,$arrPagination['per_page']);
	}

    if(!empty($arrDeposit)) for($i = 0; $i < count($arrDeposit); $i++) {
        $arrDeposit[$i] = array_merge($arrDeposit[$i],$this->admlinklist->getMenuPermission(66,$arrDeposit[$i]['depo_status']));
        $arrDeposit[$i]['depo_rawstatus'] = $arrDeposit[$i]['depo_status'];
        $arrDeposit[$i]['depo_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrDeposit[$i]['depo_status']);
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'deposit/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrDeposit' => $arrDeposit,
        'arrDepositStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'deposit-deposit')
    ));
}

public function delete($intID) {
    $this->Mdeposit->deleteByID($intID);
    $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'deposit-depositdeleted'));
        redirect('deposit/browse');
}

}

/* End of File */