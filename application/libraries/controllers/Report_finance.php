<?php
/* 
PUBLIC FUNCTION:
- invoicebelumlunas($intPage = 0)
# hutangdaftar($intPage = 0)
# hutangkartu($intPage = 0)
# hutangrekap($intPage = 0)
# piutangdaftar($intPage = 0)
# piutangkartu($intPage = 0)
# piutangrekap($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_finance extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mreportfinance', 'MReport');
}

public function invoicebelumlunas($intPage = 0) {
    $this->_getMenuHelpContent(161,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $arrInvoice = $this->MReport->searchInvoiceBelumLunasBySalesman($this->input->get('start'),$this->input->get('end'));

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
		$arrInvoice = $this->MReport->searchInvoiceBelumLunasBySalesman($this->input->post('txtStart'),$this->input->post('txtEnd'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_finance/invoicebelumlunas', NULL, FALSE).'">[Back]</a>';
		//$this->_arrData['strMessage'] = "Search by ... with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	} else {
		$arrPagination['base_url'] = site_url("report_finance/invoicebelumlunas?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountInvoiceBelumLunas();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrInvoice = $this->MReport->getItemsInvoiceBelumLunas($intPage,$arrPagination['per_page']);
	}

    $arrData = array(
        'arrInvoice' => $arrInvoice,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/invoicebelumlunas_export', array_merge(array(
                $bolWithSign => FALSE,
                $bolBypass => TRUE,
            ), $arrData), true),
        'invoice_belum_lunas.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print 
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoicebelumlunas',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_finance/invoicebelumlunas',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
    
}

}