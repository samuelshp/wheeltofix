<?php

class Invoice_ajax extends JW_Controller {

public function NULL() {
    # Dipakai kok
}

public function getSupplierAutoComplete($txtData='') {
    $this->load->model('Msupplier');
    ArrayToXml(array('Supplier' => $this->Msupplier->getAllSuppliers($txtData)));
}

public function getSalesmanAutoComplete($txtData='') {
    # INIT
    $this->load->model('Msalesman');

    ArrayToXml(array('Salesman' => $this->Msalesman->getAllSalesman($txtData)));
}

public function getProductBrand($txtData='') {
    $this->load->model('Mproductbrand');
    ArrayToXml(array('ProductBrand' => $this->Mproductbrand->getAllItems($txtData)));
}

public function getProductBrandByID($txtData='') {
    $this->load->model('Mproductbrand');
    ArrayToXml(array('ProductBrand' => $this->Mproductbrand->getItemByID($txtData)));
}

public function getProductByID($txtData='') {
    $this->load->model('Mproduct');
    ArrayToXml(array('Product' => $this->Mproduct->getItemByID($txtData)));
}

public function getInvoiceOrderAutoComplete($txtData='') {
    $this->load->model('Minvoiceorder');
    ArrayToXml(array('Supplier' => $this->Minvoiceorder->getDataAutoComplete($txtData)));
}

public function getProductPurchasedBySOID($intSOID) {
    ArrayToXml(array('Item' => $this->Minvoiceitem->getAllPurchasedItemBySOID($intSOID)));
}

public function getDataForInvoice($intID = 0) {
    $this->load->model('Minvoiceorderitem');
    $arrItem = $this->Minvoiceorderitem->getItemsByInvoiceID($intID);
    $this->load->model('Munit');
    $arrUnit = array(); $i = 0;
    foreach($arrItem as $e) {
        $arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['inoi_product_id']);
        $i++;
    }

    ArrayToXml(array('orderItem' => $arrItem,'Unit' => $arrUnit));
}

public function getInvoiceAutoComplete($txtData='',$txtUnselect='') {
    # INIT
    $this->load->model('Minvoice');
    if($txtUnselect == ''){
        $arrProduct = $this->Minvoice->getAllActiveInvoice($txtData);
    } else {
        $arrUnselect = explode('-',$txtUnselect);
        $i = 0;
        $arrDeliveryItemID = array();
        foreach($arrUnselect as $e) if($e != 0) {
            $arrDeliveryItemID[$i] = $this->Mdeliveryitem->getInvoiceIDByDeliveryItemID($e); 
            $i++;
        }
        $txtUnselect = '';
        foreach($arrDeliveryItemID as $e) {
            $txtUnselect = $txtUnselect.$e['deit_invoice_id'].',';
        }
        $txtUnselect = $txtUnselect.'0';
        $arrProduct = $this->Minvoice->getAllActiveInvoiceAndUnselect($txtData,$txtUnselect);
    }
    if(!empty($arrProduct)){
        for($i = 0; $i < count($arrProduct); $i++){
            $arrProduct[$i]['invoice_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrProduct[$i]['invo_status']);
        }
    }

    ArrayToXml(array('Invoice' => $arrProduct));
}

public function getInvoiceSalesmanAutoComplete($txtData = '',$intSupplier='') {
    #INIT
    $this->load->model('Msalesman');
    $arrSalesman = $this->Msalesman->getAllSalesman($txtData,$intSupplier);
    ArrayToXml(array('Salesman' => $arrSalesman));
}

public function getInvoiceOutletAutoComplete($txtData = '') {
    #INIT
    $this->load->model('Moutlet');

    ArrayToXml(array('Outlet' => $this->Moutlet->getAllOutlet($txtData)));
}

public function getInvoiceTotal($intID) {
    # INIT
    $this->load->model('Minvoiceitem');
    $this->load->model('Minvoice');
    $this->load->model('Munit');
    $arrDeliverydItem = $this->Minvoiceitem->getItemsByInvoiceID($intID);
    $intDeliveryGrand = $arrDeliverydItem[0]['invo_grandtotal'];
    $arrItem = array();
    $counter = 0;
    for($k = 0; $k < count($arrDeliverydItem); $k++) {
        $flag = 0;
        $l = 0;
        foreach($arrItem as $e) {
            if($e['id'] == $arrDeliverydItem[$k]['product_id']) {
                $arrItem[$l]['qty1'] += $arrDeliverydItem[$k]['invi_quantity1'];
                $arrItem[$l]['qty2'] += $arrDeliverydItem[$k]['invi_quantity2'];
                $arrItem[$l]['qty3'] += $arrDeliverydItem[$k]['invi_quantity3'];
                $flag = 1;
            }
            $l++;
        }
        if($flag == 0 ){
            $arrItem[$counter]['name'] = "(".$arrDeliverydItem[$k]['prod_code'].") ".formatProductName("",$arrDeliverydItem[$k]['prod_title'],$arrDeliverydItem[$k]['prob_title'],$arrDeliverydItem[$k]['proc_title']);
            $arrItem[$counter]['id'] = $arrDeliverydItem[$k]['product_id'];
            $arrItem[$counter]['qty1'] = $arrDeliverydItem[$k]['invi_quantity1'];
            $arrItem[$counter]['qty2'] = $arrDeliverydItem[$k]['invi_quantity2'];
            $arrItem[$counter]['qty3'] = $arrDeliverydItem[$k]['invi_quantity3'];
            $arrItem[$counter]['unit1'] = $arrDeliverydItem[$k]['invi_unit1'];
            $arrItem[$counter]['unit1name'] = formatUnitName($arrDeliverydItem[$k]['invi_unit1']);
            $arrItem[$counter]['unit2'] = $arrDeliverydItem[$k]['invi_unit2'];
            $arrItem[$counter]['unit2name'] = formatUnitName($arrDeliverydItem[$k]['invi_unit2']);
            $arrItem[$counter]['unit3'] = $arrDeliverydItem[$k]['invi_unit3'];
            $arrItem[$counter]['unit3name'] = formatUnitName($arrDeliverydItem[$k]['invi_unit3']);
            $counter++;
        }
    }
    $arrDeliveryBonusItem = $this->Minvoiceitem->getBonusItemsByInvoiceID($intID);
    for($k = 0; $k < count($arrDeliveryBonusItem); $k++) {
        $flag = 0;
        $l = 0;
        foreach ($arrItem as $e) {
            if($e['id'] == $arrDeliveryBonusItem[$k]['product_id']) {
                $arrItem[$l]['qty1'] += $arrDeliveryBonusItem[$k]['invi_quantity1'];
                $arrItem[$l]['qty2'] += $arrDeliveryBonusItem[$k]['invi_quantity2'];
                $arrItem[$l]['qty3'] += $arrDeliveryBonusItem[$k]['invi_quantity3'];
                $flag = 1;
            }
            $l++;
        }
        if($flag == 0) {
            $arrItem[$counter]['name'] = formatProductName('',$arrDeliveryBonusItem[$k]['prod_title'],$arrDeliveryBonusItem[$k]['prob_title'],$arrDeliveryBonusItem[$k]['proc_title']);
            $arrItem[$counter]['id'] = $arrDeliveryBonusItem[$k]['product_id'];
            $arrItem[$counter]['qty1'] = $arrDeliveryBonusItem[$k]['invi_quantity1'];
            $arrItem[$counter]['qty2'] = $arrDeliveryBonusItem[$k]['invi_quantity2'];
            $arrItem[$counter]['qty3'] = $arrDeliveryBonusItem[$k]['invi_quantity3'];
            $arrItem[$counter]['unit1'] = $arrDeliveryBonusItem[$k]['invi_unit1'];
            $arrItem[$counter]['unit2name'] = formatUnitName($arrDeliveryBonusItem[$k]['invi_unit1']);
            $arrItem[$counter]['unit2'] = $arrDeliveryBonusItem[$k]['invi_unit2'];
            $arrItem[$counter]['unit1name'] = formatUnitName($arrDeliveryBonusItem[$k]['invi_unit2']);
            $arrItem[$counter]['unit3'] = $arrDeliveryBonusItem[$k]['invi_unit3'];
            $arrItem[$counter]['unit3name'] = formatUnitName($arrDeliveryBonusItem[$k]['invi_unit3']);
            $counter++;
        }
    }

    ArrayToXml(array('Total' => $intDeliveryGrand,'Item' => $arrItem));
}

public function getInvoiceItemByDeliveryItemID($intID) {
    # INIT
    $this->load->model('Minvoiceitem');
    $this->load->model('Minvoice');
    $intID = $this->Mdeliveryitem->getInvoiceIDByDeliveryItemID($intID);
    $arrDeliverydItem = $this->Minvoiceitem->getItemsByInvoiceID($intID['deit_invoice_id']);
    $arrItem = array();
    $intDeliveryGrand = 0;
    $counter = 0;
    for($k = 0; $k < count($arrDeliverydItem); $k++) {
        $flag = 0; $l = 0;
        foreach ($arrItem as $e) {
            if($e['id'] == $arrDeliverydItem[$k]['product_id']){
                $arrItem[$l]['qty1'] += $arrDeliverydItem[$k]['invi_quantity1'];
                $arrItem[$l]['qty2'] += $arrDeliverydItem[$k]['invi_quantity2'];
                $arrItem[$l]['qty3'] += $arrDeliverydItem[$k]['invi_quantity3'];
                $flag = 1;
            }
            $l++;
        }
        if($flag == 0) {
            $arrItem[$counter]['name'] = formatProductName("",$arrDeliverydItem[$k]['prod_title'],$arrDeliverydItem[$k]['prob_title'],$arrDeliverydItem[$k]['proc_title']);
            $arrItem[$counter]['id'] = $arrDeliverydItem[$k]['product_id'];
            $arrItem[$counter]['qty1'] = $arrDeliverydItem[$k]['invi_quantity1'];
            $arrItem[$counter]['qty2'] = $arrDeliverydItem[$k]['invi_quantity2'];
            $arrItem[$counter]['qty3'] = $arrDeliverydItem[$k]['invi_quantity3'];
            $arrItem[$counter]['unit1'] = $arrDeliverydItem[$k]['invi_unit1'];
            $arrItem[$counter]['unit2'] = $arrDeliverydItem[$k]['invi_unit2'];
            $arrItem[$counter]['unit3'] = $arrDeliverydItem[$k]['invi_unit3'];
            $counter++;
        }
    }
    $arrDeliveryBonusItem = $this->Minvoiceitem->getBonusItemsByInvoiceID($intID['deit_invoice_id']);
    for($k = 0; $k < count($arrDeliveryBonusItem); $k++) {
        $flag = 0; $l = 0;
        foreach($arrItem as $e) {
            if($e['id'] == $arrDeliveryBonusItem[$k]['product_id']) {
                $arrItem[$l]['qty1'] += $arrDeliveryBonusItem[$k]['invi_quantity1'];
                $arrItem[$l]['qty2'] += $arrDeliveryBonusItem[$k]['invi_quantity2'];
                $arrItem[$l]['qty3'] += $arrDeliveryBonusItem[$k]['invi_quantity3'];
                $flag = 1;
            }
            $l++;
        }
        if($flag == 0) {
            $arrItem[$counter]['name'] = formatProductName("",$arrDeliveryBonusItem[$k]['prod_title'],$arrDeliveryBonusItem[$k]['prob_title'],$arrDeliveryBonusItem[$k]['proc_title']);
            $arrItem[$counter]['id'] = $arrDeliveryBonusItem[$k]['product_id'];
            $arrItem[$counter]['qty1'] = $arrDeliveryBonusItem[$k]['invi_quantity1'];
            $arrItem[$counter]['qty2'] = $arrDeliveryBonusItem[$k]['invi_quantity2'];
            $arrItem[$counter]['qty3'] = $arrDeliveryBonusItem[$k]['invi_quantity3'];
            $arrItem[$counter]['unit1'] = $arrDeliveryBonusItem[$k]['invi_unit1'];
            $arrItem[$counter]['unit2'] = $arrDeliveryBonusItem[$k]['invi_unit2'];
            $arrItem[$counter]['unit3'] = $arrDeliveryBonusItem[$k]['invi_unit3'];
            $counter++;
        }
    }

    ArrayToXml(array('Item' =>$arrItem));
}

public function getBonusByPromo($txtProduct = '',$txtBrand = '',$txtQuantity = '',$dateData = '') {
    $dateData = formatDate2($dateData, 'Y-m-d');
    #INIT
    $arrIDProductTemp = explode('-',$txtProduct);
    $arrIDBrandTemp = explode('-',$txtBrand);
    $arrQtyTemp = explode('-',$txtQuantity);
    $arrIDProduct = array();
    $arrIDBrand = array();
    $arrQty = array(); $j = 0; $i = 0;
    foreach($arrIDProductTemp as $e) if($e > 0) {
        $found = array_search($e,$arrIDProduct);
        if(is_numeric($found)) $arrQty[$found]+=$arrQtyTemp[$i];
        else {
            $arrIDProduct[$j] = $e;
            $arrIDBrand[$j] = $arrIDBrandTemp[$i];
            $arrQty[$j] = $arrQtyTemp[$i];
            $j++;
        }
        $i++;
    }
    $this->load->model('Mpromo');
    $this->load->model('Mproduct');
    $arrPromo = $this->Mpromo->GetAllPromoByDate($dateData, implode(',', $arrIDProduct), implode(',', $arrIDBrand));
    $arrBonusItem = array();
    $j = 0;
    if(!empty($arrPromo)) {
        foreach($arrPromo as $e) {
            $i = 0;
            foreach($arrIDProduct as $f) if($f != '0') {
                if($e['prom_brand_id'] == $f) {
                    if($arrQty[$i] >= $e['prom_syarat']) {
                        $arrBonusItem[$j]['type'] = 'merk';
                        $times = (int) $arrQty[$i] / (int) $e['prom_syarat'];
                        $arrBonusItem[$j]['id'] = $e['prom_bonus_brand_id'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty'] * (int) $times;
                        $arrBonusItem[$j]['discount'] = $e['prom_discount'];
                        $name = $this->Mproduct->getBrandNameById($e['prom_bonus_brand_id']);
                        $arrBonusItem[$j]['name'] = $name['prob_title'];
                        $arrBonusItem[$j]['include'] = $e['prom_include_note'];
                        $j++;
                    } else if($arrQty[$i] >= $e['prom_syarat2']) {
                        $arrBonusItem[$j]['type'] = 'merk';
                        $times = (int) $arrQty[$i] / (int) $e['prom_syarat2'];
                        $arrBonusItem[$j]['id'] = $e['prom_bonus_brand_id'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty2'] * (int) $times;
                        $arrBonusItem[$j]['discount'] = $e['prom_discount2'];
                        $name = $this->Mproduct->getBrandNameById($e['prom_bonus_brand_id']);
                        $arrBonusItem[$j]['name'] = $name['prob_title'];
                        $arrBonusItem[$j]['include'] = $e['prom_include_note'];
                        $j++;
                    } else if($arrQty[$i] >= $e['prom_syarat3']) {
                        $arrBonusItem[$j]['type'] = 'merk';
                        $times = (int) $arrQty[$i] / (int) $e['prom_syarat3'];
                        $arrBonusItem[$j]['id'] = $e['prom_bonus_brand_id'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty3'] * (int) $times;
                        $arrBonusItem[$j]['discount'] = $e['prom_discount3'];
                        $name = $this->Mproduct->getBrandNameById($e['prom_bonus_brand_id']);
                        $arrBonusItem[$j]['name'] = $name['prob_title'];
                        $arrBonusItem[$j]['include'] = $e['prom_include_note'];
                        $j++;
                    }
                } elseif($e['prom_product_id'] == $f && empty($e['prom_bonus_product_id'])) {
                    # Diskon
                    if($e['prom_syarat'] > 0 && $arrQty[$i] >= $e['prom_syarat']) {
                        $arrBonusItem[$j]['type'] = 'diskon';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['discount'] = $e['prom_discount'];
                        $j++;
                    } elseif($e['prom_syarat2'] > 0 && $arrQty[$i] >= $e['prom_syarat2']) {
                        $arrBonusItem[$j]['type'] = 'diskon';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['discount'] = $e['prom_discount2'];
                        $j++;
                    } elseif($e['prom_syarat3'] > 0 && $arrQty[$i] >= $e['prom_syarat3']) {
                        $arrBonusItem[$j]['type'] = 'diskon';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['discount'] = $e['prom_discount3'];
                        $j++;
                    }
                } elseif($e['prom_product_id'] == $f && !empty($e['prom_bonus_product_id'])) {
                    # Produk
                    $bolSyaratMemenuhi = FALSE;
                    if($e['prom_syarat'] > 0 && $arrQty[$i] >= $e['prom_syarat']) {
                        $bolSyaratMemenuhi = TRUE;
                        $arrBonusItem[$j]['type'] = 'produk';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['syarat'] = (int) $e['prom_syarat'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty'];
                        $arrBonusItem[$j]['qty_type'] = 1;
                        $arrBonusProduct = $this->Mproduct->getItemByID($e['prom_bonus_product_id']);
                        $arrBonusItem[$j]['bonus'] = $arrBonusProduct['prod_code'];
                        $j++;
                    } elseif($e['prom_syarat2'] > 0 && $arrQty[$i] >= $e['prom_syarat2']) {
                        $bolSyaratMemenuhi = TRUE;
                        $arrBonusItem[$j]['type'] = 'produk';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['syarat'] = (int) $e['prom_syarat2'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty2'];
                        $arrBonusItem[$j]['qty_type'] = 2;
                        $arrBonusProduct = $this->Mproduct->getItemByID($e['prom_bonus_product_id']);
                        $arrBonusItem[$j]['bonus'] = $arrBonusProduct['prod_code'];
                        $j++;
                    } elseif($e['prom_syarat3'] > 0 && $arrQty[$i] >= $e['prom_syarat3']) {
                        $bolSyaratMemenuhi = TRUE;
                        $arrBonusItem[$j]['type'] = 'produk';
                        $arrBonusItem[$j]['id'] = $e['prom_product_id'];
                        $arrBonusItem[$j]['syarat'] = (int) $e['prom_syarat3'];
                        $arrBonusItem[$j]['qty'] = (int) $e['prom_bonus_qty3'];
                        $arrBonusItem[$j]['qty_type'] = 3;
                        $arrBonusProduct = $this->Mproduct->getItemByID($e['prom_bonus_product_id']);
                        $arrBonusItem[$j]['bonus'] = $arrBonusProduct['prod_code'];
                        $j++;
                    }
                }
                $i++;
            }
        }
        ArrayToXml(array('Bonus' => $arrBonusItem));
    }
}

public function getBonusAutoCompleteByPromo($strAllowedBrand,$txtData = '') {
    #INIT
    $this->load->model('Mproduct');
    $strAllowedBrand = strtr($strAllowedBrand,array('-'=>','));
    $arrProduct = $this->Mproduct->getAllItemAutoCompleteByPromo($strAllowedBrand,$txtData);
    if (!empty($arrProduct)) {
        for($i = 0; $i < count($arrProduct); $i++) {
            $arrProduct[$i]['strName'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
        }
    }

    ArrayToXml(array('Product' => $arrProduct));
}

public function getSaleOrderCustomerAutoComplete($txtData='') {
    #INIT
    $this->load->model('Mcustomer');

    ArrayToXml(array('Customer' => $this->Mcustomer->getAllCustomer($txtData)));
}

public function getCustomerCreditByCustomerID($txtID='') {
    #INIT
    $this->load->model('Minvoice');

    ArrayToXml(array('Kredit' => $this->Minvoice->getCustomerCredit($txtID)));
}

public function getSaleOrderCustomerAutoCompleteWithSupplierBind($txtData='',$intSupplier='') {
    #INIT
    $this->load->model('Mcustomer');
    $Costumer=$this->Mcustomer->getAllCustomerWithSupplierBind($txtData,$intSupplier);
    $i=0;
    foreach($Costumer AS $e){
        $Costumer[$i]['paid']=$this->Mcustomer->isCustomerPaidAllPerSupplier($e['id'],$intSupplier);
        $i++;
    }

    ArrayToXml(array('Customer' => $Costumer));
}

public function checkPrintLog($intID='0') {
    $this->load->model('Mprintlog');
    ArrayToXml(array('Kode' => $this->Mprintlog->getInvoiceNumber('invoice',$intID)));
}

public function checkCustomerBill($intCostumer='',$intSupplier='') {
    #INIT
    $this->load->model('Mcustomer');
    $Costumer=$this->Mcustomer->Invoice_Order_IsCustomerPaidAll($intCostumer,$intSupplier);
    ArrayToXml(array('Customer' => $Costumer));
}

public function checkCustomerBillPerSupplier($intCostumer='',$intSupplier='') {
    #INIT
    $this->load->model('Mcustomer');
    $Costumer=$this->Mcustomer->Invoice_Order_IsCustomerPaidAllPerSupplier($intCostumer,$intSupplier);
    ArrayToXml(array('Customer' => $Costumer));
}

public function getLastProductPrice() {
    $this->load->model('Minvoiceitem');

    $arrItems = $this->Minvoiceitem->getLastProductPrice($this->input->get('product_id'));
    foreach($arrItems as $i => $e) {
        $arrItems[$i]['strDate'] = formatDate2($e['cdate'], 'd F Y');
        $arrItems[$i]['strPrice'] = setPrice($e['invi_price']);
    }

    echo json_encode(array(
        'Items' => $arrItems,
    ));
}

// patricklipesik begin
public function getInvoiceUserAutoComplete($txtData = '') {
    #INIT
    $this->load->model('Madminlogin');
    $arrUser = $this->Madminlogin->getAllUser($txtData);
    
    ArrayToXml(array('User' => $arrUser));
}

public function getInvoiceCustomerAutoComplete($txtData = '') {
    #INIT
    $this->load->model('Mcustomer');
    $arrCustomer = $this->Mcustomer->getAllCustomer($txtData);
    
    ArrayToXml(array('Customer' => $arrCustomer));
}
// patricklipesik end

}

/* End of File */