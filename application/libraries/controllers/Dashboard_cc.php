<?php

class Dashboard_cc extends JW_Controller {

public function __construct() {
	parent::__construct();
	
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	//$this->load->model('Mdashboard');

    $this->_getMenuHelpContent(238,true,'adminpage');
}

public function index(){

}

public function view(){
	$this->load->view('sia',array(
        'strViewFile' => 'dashboard_cc/browse',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
    ));
}

public function browse(){
	$this->load->view('sia',array(
        'strViewFile' => 'dashboard_cc/browse',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
    ));
}

}

?>