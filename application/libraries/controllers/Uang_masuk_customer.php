<?php 

use App\Models\{Euangmasukcustomer, Ecustomer, Eaccount};
if(!class_exists('Euangmasukcustomer')) require_once(APPPATH.'models/Euangmasukcustomer.php');
if(!class_exists('Ecustomer')) require_once(APPPATH.'models/Ecustomer.php');
if(!class_exists('Eaccount')) require_once(APPPATH.'models/Eaccount.php');
use App\Models\DBFacadesWrapper as DB;

class Uang_masuk_customer extends JW_Controller
{
    public function __construrct()
    {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    }

    public function index()
    {
        $this->_getMenuHelpContent(235,true,'adminpage');


        $arrPagination['base_url'] = site_url("uang_masuk_customer/?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = Euangmasukcustomer::count();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $umcList = Euangmasukcustomer::where('status', 0)->get();
        
        $strBrowseMode = '';

        $this->load->view('sia',array(
            'strViewFile' => 'uang_masuk_customer/index',
            'strPage' => $strPage,
            'strPageTitle' => 'Uang Masuk Customer',
            'strBrowseMode' => $strBrowseMode,
            'umcList' => $umcList,
        ));
    }

    public function action($action, $id = null)
    {
        $data = [];
        $availableCustomer = Ecustomer::all();
        $availableAccount  = Eaccount::all();

        // page action hanya membutuhkan object(singular) jika tidak menggunakan revisi/versi
        if($action == 'add')
        {
            $object  = null;
        }
        elseif($action == 'edit')
        {
            $object = Euangmasukcustomer::with(['customer', 'account'])
                ->where('id', $id)
                ->first();
        }
        // dd(site_url('subkontrak/'.$action));
        $data = array_merge($data, [
            'strViewFile'       => 'uang_masuk_customer/action',
            'strPageTitle'      => 'Uang Masuk Customer',
            'formAction'        => site_url('uang_masuk_customer/'.$action),
            'availableCustomer' => $availableCustomer,
            'availableAccount'  => $availableAccount,
            'object'            => $object,
        ]);
        
        $this->load->view('sia', $data); 

    }

    public function add()
    {
        generateRepopulate();

        $uangMasukCustomer              = new Euangmasukcustomer;
        $uangMasukCustomer->kode        = $this->input->post('kode');
        $uangMasukCustomer->customer_id = $this->input->post('customer_id');
        $uangMasukCustomer->coa_id      = $this->input->post('coa_id');
        $uangMasukCustomer->type        = $this->input->post('type');
        $uangMasukCustomer->no_giro     = $this->input->post('no_giro');
        $uangMasukCustomer->tanggal_jt  = $this->input->post('tanggal_jt');
        $uangMasukCustomer->amount      = $this->input->post('amount');
        $uangMasukCustomer->status      = 0;
        $uangMasukCustomer->cdate       = date("Y-m-d H:i:s");
        $uangMasukCustomer->save();

        $this->session->set_flashdata('strMessage','Data Uang Masuk Customer has been created successfully.');
        redirect('uang_masuk_customer/', 'refresh');

    }

    public function edit()
    {
        generateRepopulate();

        $uangMasukCustomer              = Euangmasukcustomer::where('id', $this->input->post('id'))->first();
        $uangMasukCustomer->kode        = $this->input->post('kode');
        $uangMasukCustomer->customer_id = $this->input->post('customer_id');
        $uangMasukCustomer->coa_id      = $this->input->post('coa_id');
        $uangMasukCustomer->type        = $this->input->post('type');
        $uangMasukCustomer->no_giro     = $this->input->post('no_giro');
        $uangMasukCustomer->tanggal_jt  = $this->input->post('tanggal_jt');
        $uangMasukCustomer->amount      = $this->input->post('amount');
        $uangMasukCustomer->status      = 0;
        $uangMasukCustomer->mdate       = date("Y-m-d H:i:s");
        $uangMasukCustomer->save();

        $this->session->set_flashdata('strMessage','Data Uang Masuk Customer has been updated.');
        redirect('uang_masuk_customer/', 'refresh');

    }

    public function  delete()
    {
        $data = $this->cfg;

        try
        {
            $id   = $this->input->post('id');
            $object = Euangmasukcustomer::where('id', $id)->update(['status' => 2]);



            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                        'status' => 'ok',
                        'text' => 'Data succesfully deleted',
                        'type' => 'success'
                    ])
                );

        }
        catch (\Exception $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
        catch (QueryException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        } 
        catch (\PDOException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
    }
}