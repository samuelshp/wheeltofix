
<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Acceptance extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Macceptance');
	$this->load->model('Macceptanceitem');

    $this->_getMenuHelpContent(23, true, 'adminpage');
}

public function index() {
	if($this->input->post('smtMakeAcceptance') != '') { // Make Acceptance
        $this->load->model('Munit');
        $days=$this->input->post('txtJatuhTempo');
        $date=$this->input->post('txtDate');
        $date = strtotime("+".$days." days", strtotime($date));
        $jatuhtempo = date("Y-m-d", $date);
        $jatuhtempo = str_replace("-","/",$jatuhtempo);
        $totalItem = $this->input->post('totalItem');
        for($i = 0; $i < $totalItem; $i++){
            //$intPurchaseID = $this->input->post('PurchaseID');
            $intSuppID = $this->input->post('supplierID');
            $intWarehouse = $this->input->post('WarehouseID');
            $strEkspedition = $this->input->post('txtEkspedition');
            $strDescription = $this->input->post('txaDescription');
            $intTax= $this->input->post('txtTax');
            $intDiscount= $this->input->post('dsc4');
            $strDate= $this->input->post('txtDate');
            $intTipeBayar = $this->input->post('intTipeBayar');
            $strJatuhTempo = $jatuhtempo;
            $strCustomCode = $this->input->post('txtCustomCode');
            $int_purchase = $this->input->post('purchase_id'.$i);
            $int_supplier = $this->input->post('supplier_item'.$i);
            $int_acc_code = "123ABC";
            $c_date = $this->input->post('tanggal_hari_ini');
            if($int_supplier == NULL){
                $int_supplier = 0;
            }
            $intAcceptanceID = $this->Macceptance->add(0,$int_supplier,$intWarehouse,$strDescription,$intTax=0,$intDiscount=0,2,$c_date,$intTipeBayar="Tunai",$strJatuhTempo,$int_acc_code);
        }
        $arrAcceptance = array(
            'intPurchaseID' => $this->input->post('PurchaseID'),
            'intSuppID'=> $this->input->post('supplierID'),
            'intWarehouse' => $this->input->post('WarehouseID'),
            'strEkspedition' => $this->input->post('txtEkspedition'),
            'strDescription' => $this->input->post('txaDescription'),
            'intTax'=> $this->input->post('txtTax'),
            'intDiscount'=> $this->input->post('dsc4'),
            'strDate'=> $this->input->post('txtDate'),
            'intTipeBayar' => $this->input->post('intTipeBayar'),
            'strJatuhTempo' => $jatuhtempo,
            'strCustomCode' => $this->input->post('txtCustomCode'),
        );
        $this->load->model('Mpurchaseitem');
        for($i = 0; $i < $totalItem; $i++) {
            $nomor_po = $this->input->post('nomor_po'.$i);
            $name_bahan = $this->input->post('nameItemX'.$i);
            $satuan = $this->input->post('satuan_po'.$i);
            $jumlah = $this->input->post('jumlah_po_fill'.$i);
            $jumlah_tetap = $this->input->post('jumlah_awal'.$i);
            $jumlah_penerimaan = $this->input->post('qty1ItemX'.$i);
            $satuan_penerimaan = $this->input->post('inputan_satuan'.$i);
            $keterangan_penerimaan = $this->input->post('inputan_keterangan'.$i);
            $purchase_id_lol = $this->input->post('purchase_id_input'.$i);
            $exist = $this->input->post('existItem'.$i);
            $purchase_item_id = $this->input->post('purchase_item_id'.$i);
            $qty1Item = $this->input->post('qty1PriceEffect'.$i);
            $qty2Item = $this->input->post('qty2PriceEffect'.$i);
            $qty3Item = $this->input->post('qty3PriceEffect'.$i);
            $prcItem = $this->input->post('prcPriceEffect'.$i);
            $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
            $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
            $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
            $prodItem = $this->input->post('prodItem'.$i);
            $probItem = $this->input->post('probItem'.$i);
            $unit1Item = $this->input->post('sel1UnitID'.$i);
            $unit2Item = $this->input->post('sel2UnitID'.$i);
            $unit3Item = $this->input->post('sel3UnitID'.$i);
            $prodId = $this->input->post('product_id'.$i);
            $strProductDescription = 'Something';
            $idItem = $this->input->post('idItem'.$i);
            if($prodId == NULL){
                $prodId = 0;
            }
            if($keterangan_penerimaan != '' || !empty($keterangan_penerimaan)) {
                //update jumlah barang pada PO
                //echo "<script type='text/javascript'>alert('Masuk sini ".$keterangan_penerimaan." ".$jumlah_tetap." ".$jumlah_penerimaan." ');</script>";
                $this->load->model('Macceptanceitem');
                $jumlah_baru = $jumlah_tetap - $jumlah_penerimaan;
                $this->Macceptanceitem->add($intAcceptanceID,$prodId,$keterangan_penerimaan,$jumlah_baru,$jumlah_penerimaan,$satuan_penerimaan='',$purchase_id_lol, $lokasi_penerimaan="Surabaya");
                $this->load->model('Mpurchaseitem');
                $this->Mpurchaseitem->updateJumlahBarang($jumlah_baru, $jumlah_penerimaan, $prodId, $purchase_item_id);
                //$this->Mpurchase->updateQtyPB($jumlah,$purchase_item_id);
                // if($int_purchase>0){
                //     $this->load->model('Mpurchaseitem');
                //     $this->Mpurchaseitem->UpdateAcceptedItem($int_purchase,$prodId,$jumlah_penerimaan,0,0,0);
                // }
            }
        }
            // $totalItemBonus = $this->input->post('totalItemBonus');
            // for($i = 0; $i < $totalItemBonus; $i++) {
            //     $existBonus = $this->input->post('existBonusItem'.$i);
            //     $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
            //     $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
            //     $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
            //     $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
            //     $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
            //     $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
            //     $prodItemBonus = $this->input->post('prodItemBonus'.$i);
            //     $probItemBonus = $this->input->post('probItemBonus'.$i);
            //     $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
            //     $idItemBonus = $this->input->post('idItemBonus'.$i);
            //     if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
            //         $this->Macceptanceitem->add($intAcceptanceID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1,$existBonus);
            //         if($arrAcceptance['intPurchaseID']>0){
            //             $this->Mpurchaseitem->UpdateAcceptedItem($arrAcceptance['intPurchaseID'],$idItemBonus,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,1);
            //         }
            //     }
            // }
        if($arrAcceptance['intPurchaseID']>0){
            $different=0;
            $arrCompare=$this->Mpurchaseitem->getItemsByPurchaseID($arrAcceptance['intPurchaseID']);
            if(!empty($arrCompare)) {
                for($i = 0; $i < count($arrCompare); $i++) {
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit1']);
                    $intConv1 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit2']);
                    $intConv2 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit3']);
                    $intConv3 = $convTemp[0]['unit_conversion'];
                    $quantity=(int)$arrCompare[$i]['prci_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['prci_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['prci_quantity3']*(int)$intConv3;
                    $purchased=(int)$arrCompare[$i]['prci_arrived1']*(int)$intConv1+(int)$arrCompare[$i]['prci_arrived2']*(int)$intConv2+(int)$arrCompare[$i]['prci_arrived3']*(int)$intConv3;
                    $different+=(int)$quantity-(int)$purchased;
                }
            }
            $arrCompare=$this->Mpurchaseitem->getBonusItemsByPurchaseID($arrAcceptance['intPurchaseID']);
            if(!empty($arrCompare)) {
                for($i = 0; $i < count($arrCompare); $i++) {
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit1']);
                    $intConv1 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit2']);
                    $intConv2 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit3']);
                    $intConv3 = $convTemp[0]['unit_conversion'];
                    $quantity=(int)$arrCompare[$i]['prci_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['prci_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['prci_quantity3']*(int)$intConv3;
                    $purchased=(int)$arrCompare[$i]['prci_arrived1']*(int)$intConv1+(int)$arrCompare[$i]['prci_arrived2']*(int)$intConv2+(int)$arrCompare[$i]['prci_arrived3']*(int)$intConv3;
                    $different+=(int)$quantity-(int)$purchased;
                }
            }
            if($different==0){
                //sini ubah status order jadi 4
                $this->load->model('Mpurchase');
                $this->Mpurchase->editStatusByID($arrAcceptance['intPurchaseID'],4);
            }
        }
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptancemade');
        redirect('acceptance/view/'.$intAcceptanceID);
    }
    if($this->input->post('subSearchSupply') != '') {
        //$arrAcceptance = $this->Macceptance->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));
        //Tambah pagination disini
        
        $this->load->model('Mpurchaseitem');
        $arrAcceptance = $this->Mpurchaseitem->getAllPurchaseItemBySupplier($this->input->post('txtSearchSupplier'));
        $strPage = '';
		$strBrowseMode = '<a href="'.site_url('acceptance', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrAcceptance) ? count($arrAcceptance) : '0')." records).";
        $this->load->model('Mtinydbvo');
        $this->Mtinydbvo->initialize('invoice_status');
        $this->load->view('sia',array(
            'strViewFile' => 'acceptance/add',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'arrDataPO' => $arrAcceptance,
            'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
            'strSearchKey' => $this->input->post('txtSearchSupplier'),
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
        ));

    }

    $this->load->model('Mpurchaseitem');
    $arrDataPO = $this->Mpurchaseitem->getAllPurchaseItem();
    $jumlahData = sizeof($arrDataPO);
    $cek = $arrDataPO[0]['id'];
    //echo "<script>alert($cek);</script>";
	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addacceptance'),
        'arrDataPO' => $arrDataPO,
        'jumlahData' => $jumlahData
    ));
    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/add',
        'arrDataPO' => $arrDataPO,
        'jumlahData' => $jumlahData
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
	$arrReturn = $this->session->userdata('arrReturn');

	if($this->input->post('subSave') != '' && $intID != '') { # SAVE
		// Acceptance
		$arrAcceptanceData = $this->Macceptance->getItemByID($intID);
        $intPurchaseID = $this->input->post('PurchaseID');
        $this->load->model('Macceptanceitem');
		if(compareData($arrAcceptanceData['acce_status'],array(0))) {
			    $this->Macceptance->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'),$this->input->post('dsc4'),$this->input->post('txtTax'));
			// Load purchase item
			$arrAcceptanceItem = $this->Macceptanceitem->getItemsByAcceptanceID($intID);
            //Seharusnya dari purchaseitem
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrAwal1 = $this->input->post('awal1Item');
            $arrAwal2 = $this->input->post('awal2Item');
            $arrAwal3 = $this->input->post('awal3Item');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');
            $arrIdItem = $this->input->post('idItem');
            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
			foreach($arrAcceptanceItem as $e) {
				// Search return quantity
				
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Macceptanceitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);
                        if($intPurchaseID>0){
                            $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                            $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                            $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                            if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                $this->Macceptanceitem->UpdateArrivedItemView($intPurchaseID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,0);
                            }
                        }
                    } else $this->Macceptanceitem->deleteByID($e['id']);
					
                } else $this->Macceptanceitem->deleteByID($e['id']);
                
			}
            $arrAcceptanceItem = $this->Macceptanceitem->getBonusItemsByAcceptanceID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');
            $arrAwal1 = $this->input->post('awal1ItemBonus');
            $arrAwal2 = $this->input->post('awal2ItemBonus');
            $arrAwal3 = $this->input->post('awal3ItemBonus');
            $arrIdItem = $this->input->post('idItemBonus');
            $arrContainID = $this->input->post('idProiIDBonus');
            if(!empty($arrAcceptanceItem)){
                foreach($arrAcceptanceItem as $e) {
                    // Search return quantity
                    
                    if(!empty($arrContainID)) {
                        if (in_array($e['id'], $arrContainID)) {
                            $this->Macceptanceitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
                            if($intPurchaseID>0){
                                $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                                $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                                $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                                if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                    $this->Macceptanceitem->UpdateArrivedItemView($intPurchaseID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,1);
                                }
                            }
                        } else $this->Macceptanceitem->deleteByID($e['id']);
						
                    } else $this->Macceptanceitem->deleteByID($e['id']);
                    
                }
            }
            if($intPurchaseID==0){
                $totalItem = $this->input->post('totalItem');
                for($i = 0; $i < $totalItem; $i++) {
                    $qty1Item = $this->input->post('qty1PriceEffect'.$i);
                    $qty2Item = $this->input->post('qty2PriceEffect'.$i);
                    $qty3Item = $this->input->post('qty3PriceEffect'.$i);
                    $unit1Item = $this->input->post('sel1UnitID'.$i);
                    $unit2Item = $this->input->post('sel2UnitID'.$i);
                    $unit3Item = $this->input->post('sel3UnitID'.$i);
                    $prcItem = $this->input->post('prcPriceEffect'.$i);
                    $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
                    $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
                    $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
                    $prodItem = $this->input->post('prodItem'.$i);
                    $probItem = $this->input->post('probItem'.$i);
                    $strProductDescription = formatProductName('',$prodItem,$probItem);
                    $idItem = $this->input->post('idItem'.$i);
                    if($strProductDescription != '' || !empty($strProductDescription)) {
                        $this->Macceptanceitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0,1);
                    }
                }
                // $totalItemBonus = $this->input->post('totalItemBonus');
                // for($i = 0; $i < $totalItemBonus; $i++) {
                //     $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
                //     $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
                //     $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
                //     $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
                //     $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
                //     $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
                //     $prodItemBonus = $this->input->post('prodItemBonus'.$i);
                //     $probItemBonus = $this->input->post('probItemBonus'.$i);
                //     $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
                //     $idItemBonus = $this->input->post('idItemBonus'.$i);
                //     if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
                //         $this->Macceptanceitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1,1);
                //     }
                // }
            }

			// Edit finance data
			//$this->load->model('Mfinance'); $this->Mfinance->editAutomatic(2,$intID,$intTotalPrice);
			
		} else $this->Macceptance->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));
		
		
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptanceupdated');
		
	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
		$this->Macceptanceitem->deleteByAcceptanceID($intID);
		$this->Macceptance->deleteByID($intID);
		
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptancedeleted'));
		redirect('acceptance/browse');
		
    }
    
    // if($this->input->post('delete_from_pb') != ''){
    //     $id_acce_item = $this->input->post('delete_from_pb');
    //     $jumlah_terima = $this->input->post('jumlah_terima['.$id_acce_item.']');
    //     $jumlah_dari_purchase = $this->input->post('qty_inputan['.$id_acce_item.']');
    //     $acc_id = $this->input->post('acceptance_id['.$id_acce_item.']');

    //     $purchase_item_id_delete = $this->input->post('purchase_item_id_edit['.$id_acce_item.']');
    //     $product_id_delete = $this->input->post('product_id_edit['.$id_acce_item.']');

    //     $jumlah_baru_delete = $jumlah_terima + $jumlah_dari_purchase;

    //     $this->Macceptanceitem->deleteByID($id_acce_item);
    //     $this->Macceptanceitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);

    //     redirect('acceptance/view/'.$acc_id);
    // }

    $arrAcceptanceData = $this->Macceptance->getItemByID($intID);
    $test = $arrAcceptanceData['idKontrak'];
    echo "<script>console.log($test+'a');</script>";
	if(!empty($arrAcceptanceData)) {
		$arrAcceptanceData['acce_rawstatus'] = $arrAcceptanceData['acce_status'];
		$arrAcceptanceData['acce_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrAcceptanceData['acce_status']);
		
        // Load the purchase item
        $this->load->model('Macceptanceitem');
        $arrAcceptanceItem = $this->Macceptanceitem->getItemsByAcceptanceID($intID);
        $test = count($arrAcceptanceItem[0]);
        echo "<script>console.log($test);</script>";
		$intAcceptanceTotal = 0;
        $this->load->model('Munit');
        $this->load->model('Macceptanceitem');
		for($i = 0; $i < count($arrAcceptanceItem); $i++) {
            if($arrAcceptanceData['acce_purchase_id']>0){
                $arrAcceptanceItem[$i]['exist'] = $arrAcceptanceItem[$i]['acit_exist'];
                $arrAcceptanceItem[$i]['maxData'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit1'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit2'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit3'] = '';
                if($arrAcceptanceItem[$i]['exist']=='1') {
                    $arrAcceptanceItem[$i]['maxData'] = $this->Macceptanceitem->getMaxItemsByAcceptedItemIDAndProdID($arrAcceptanceData['acce_purchase_id'],$arrAcceptanceItem[$i]['product_id']);
                    $arrAcceptanceItem[$i]['maxData']['unit1'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit1']);
                    $arrAcceptanceItem[$i]['maxData']['unit2'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit2']);
                    $arrAcceptanceItem[$i]['maxData']['unit3'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit3']);
                }
            }
            $arrAcceptanceItem[$i]['strName'] = $arrAcceptanceItem[$i]['acit_description'].'|'.$arrAcceptanceItem[$i]['proc_title'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intAcceptanceTotal += $arrAcceptanceItem[$i]['acit_subtotal'];
            $arrAcceptanceItem[$i]['acit_conv1'] = $intConv1;
            $arrAcceptanceItem[$i]['acit_conv2'] = $intConv2;
            $arrAcceptanceItem[$i]['acit_conv3'] = $intConv3;
		}

        $arrAcceptanceData['subtotal_discounted'] = (float) $arrAcceptanceData['acce_grandtotal'];
        $intAcceptanceGrandTotal = (float) $arrAcceptanceData['subtotal_discounted'] + ($arrAcceptanceData['subtotal_discounted'] * (float) $arrAcceptanceData['acce_tax'] / 100);

        $arrAcceptanceItemBonus = $this->Macceptanceitem->getBonusItemsByAcceptanceID($intID);
        if(!empty($arrAcceptanceItemBonus)) {
            for($i = 0; $i < count($arrAcceptanceItemBonus); $i++) {
                if($arrAcceptanceData['acce_purchase_id']>0){
                    $arrAcceptanceItemBonus[$i]['exist'] = $arrAcceptanceItemBonus[$i]['acit_exist'];
                    $arrAcceptanceItemBonus[$i]['maxData'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit1'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit2'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit3'] = '';
                    if($arrAcceptanceItemBonus[$i]['exist'] == '1') {
                        $arrAcceptanceItemBonus[$i]['maxData'] = $this->Macceptanceitem->getMaxBonusItemsByAcceptedItemIDAndProdID($arrAcceptanceData['acce_purchase_id'],$arrAcceptanceItemBonus[$i]['product_id']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit1'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit1']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit2'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit2']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit3'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit3']);
                    }
                }
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrAcceptanceItemBonus[$i]['strName'] = $arrAcceptanceItemBonus[$i]['acit_description'].'|'.$arrAcceptanceItemBonus[$i]['proc_title'];
                $arrAcceptanceItemBonus[$i]['acit_conv1'] = $intConv1;
                $arrAcceptanceItemBonus[$i]['acit_conv2'] = $intConv2;
                $arrAcceptanceItemBonus[$i]['acit_conv3'] = $intConv3;
            }
        }

	} else {
		$arrAcceptanceItem = array(); $arrAcceptanceList = array();
    }

	// Load all other purchase data the user has ever made

    $arrData = array(
        'intAcceptanceID' => $intID,
        'intAcceptanceTotal' => $intAcceptanceTotal,
        'intAcceptanceGrandTotal' => $intAcceptanceGrandTotal,
        'arrAcceptanceItem' => $arrAcceptanceItem,
        'arrAcceptanceData' => $arrAcceptanceData,
        'arrAcceptanceBonusItem' => $arrAcceptanceItemBonus,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedata'),
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        //$dataPrint=$this->Macceptance->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'acceptance',
        ), $arrData));
    } else if($this->input->get('print2') != '') {//print e pajak
        //$dataPrint=$this->Macceptance->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'acceptancetax',
        ),$arrData));
    }
    else{}
    if($this->input->post('editAcceptanceItem') != ''){
        $jumlah_delete = $this->input->post('jumlah_delete');
        $jumlah_item = $this->input->post('total_edit_item');
        $accept_id = $this->input->post('acceptance_id');
        echo "<script>console.log('$jumlah_item hahhaha');</script>";
        //echo "<script>console.log('$kumpulansId[0] hahaha');</script>";
        $kumpulanId = $this->input->post('kumpulan_id');
        if($jumlah_delete == 0){
            if($jumlah_item > 1){
                $kumpulansId = explode(",", $kumpulanId);
                for($i=0; $i<$jumlah_item; $i++){
                    $jumlah_terima_baru = $this->input->post('jumlah_terima['.$kumpulansId[$i].']');
                    $keterangan_terima_baru = $this->input->post('keterangan_terima['.$kumpulansId[$i].']');
                    $qty_inputan = $this->input->post('qty_inputan['.$kumpulansId[$i].']');
                    $prod_id = $this->input->post('purchase_id_edit['.$kumpulansId[$i].']');
                    $purch_id = $this->input->post('product_id_edit['.$kumpulansId[$i].']');
                    $purchi_id = $this->input->post('purchase_item_id_edit['.$kumpulansId[$i].']');
                    $this->Macceptanceitem->editItemsById($jumlah_terima_baru, $keterangan_terima_baru, $qty_inputan, $kumpulansId[$i]);
                    $this->Macceptanceitem->editItem($purchi_id, $qty_inputan);
                    $this->load->model('Mpurchaseitem');
                    $qty_baru = $qty_inputan-$jumlah_terima_baru;
                    $this->Mpurchaseitem->editJumlahBarangById($purchi_id, $qty_baru);
                    //echo "<script>alert('$keterangan_terima_baru');</script>";
                }
                redirect('acceptance/browse');
            }
            else if($jumlah_item == 1){
                //echo "<script>console.log('Masuk yang satu');</script>";
                $jumlah_terima_baru = $this->input->post('jumlah_terima['.$kumpulanId.']');
                //echo "<script>alert('a');</script>";
                $keterangan_terima_baru = $this->input->post('keterangan_terima['.$kumpulanId.']');
                $qty_inputan = $this->input->post('qty_inputan['.$kumpulanId.']');
                $prod_id = $this->input->post('purchase_id_edit['.$kumpulanId.']');
                $purch_id = $this->input->post('product_id_edit['.$kumpulanId.']');
                $purchi_id = $this->input->post('purchase_item_id_edit['.$kumpulanId.']');
                //$this->load->model('Macceptanceitem');
                //echo "<script>alert('a');</script>";
                $this->Macceptanceitem->editItemsById($jumlah_terima_baru, $keterangan_terima_baru, $qty_inputan, $kumpulanId);
                $this->Macceptanceitem->editItem($purchi_id, $qty_inputan);
                $this->load->model('Mpurchaseitem');
                $qty_baru = $qty_inputan-$jumlah_terima_baru;
                $this->Mpurchaseitem->editJumlahBarangById($purchi_id, $qty_baru);
                redirect('acceptance/browse');
            }
            else{}
        }
        else{
            $jumlah_awal = $this->input->post('jumlah_item');
            if($jumlah_delete == $jumlah_awal){
                $this->session->set_flashdata('strMessage','Data detail tidak boleh kosong');
                redirect('acceptance/view/'.$accept_id);
            }
            else{
                $kumpulan_delete_id = $this->input->post('kumpulan_delete_id');
                $kumpulansId = explode(",", $kumpulan_delete_id);
                if(count($kumpulansId) > 1){//jika jumlah lebih dari satu
                    for($i = 0; $i<count($kumpulansId); $i++){
                        $id_acce_item = $kumpulansId[0];
                        $jumlah_terima = $this->input->post('jumlah_terima['.$id_acce_item.']');
                        $jumlah_dari_purchase = $this->input->post('qty_inputan['.$id_acce_item.']');
                        $acc_id = $this->input->post('acceptance_id['.$id_acce_item.']');

                        $purchase_item_id_delete = $this->input->post('purchase_item_id_edit['.$id_acce_item.']');
                        $product_id_delete = $this->input->post('product_id_edit['.$id_acce_item.']');

                        $jumlah_baru_delete = $jumlah_terima + $jumlah_dari_purchase;

                        $this->Macceptanceitem->deleteByID($id_acce_item);
                        $this->Macceptanceitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);
                    }
                }
                else{
                    $id_acce_item = $kumpulansId[0];
                    $jumlah_terima = $this->input->post('jumlah_terima['.$id_acce_item.']');
                    $jumlah_dari_purchase = $this->input->post('qty_inputan['.$id_acce_item.']');
                    $acc_id = $this->input->post('acceptance_id['.$id_acce_item.']');

                    $purchase_item_id_delete = $this->input->post('purchase_item_id_edit['.$id_acce_item.']');
                    $product_id_delete = $this->input->post('product_id_edit['.$id_acce_item.']');

                    $jumlah_baru_delete = $jumlah_terima + $jumlah_dari_purchase;
                    echo "<script>alert(".$id_acc.");</script>";
                    $this->Macceptanceitem->deleteByID($id_acce_item);
                    $this->Macceptanceitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);
                }
                redirect('acceptance/view/'.$accept_id);
            }
        }
    } 

    // Untuk PM
    if($this->input->post('pmEdit') != ''){
        $prorCode = $this->input->post('prorCode');
        echo "<script>console.log($prorCode)</script>";
        $this->Mpurchaseorder->editPMStatus($prorCode);

        $this->session->set_flashdata('strMessage','Updated status PM');
        redirect('acceptance/browse');
    }

    if($this->input->post('unpmEdit') != ''){
        $prorCode = $this->input->post('prorCode');
        echo "<script>console.log($prorCode)</script>";
        $this->Mpurchaseorder->uneditPMStatus($prorCode);

        $this->session->set_flashdata('strMessage','Updated status PM');
        redirect('purchase_order/browse');
    }

    else {
        $arrAcceptanceList = $this->Macceptance->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'acceptance/view',
        ), $arrData, $this->admlinklist->getMenuPermission(23,$arrAcceptanceData['acce_rawstatus'])));
    }
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrAcceptance = $this->Macceptance->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));
		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('acceptance/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrAcceptance) ? count($arrAcceptance) : '0')." records).";

    } else {
		$arrPagination['base_url'] = site_url("acceptance/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Macceptance->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 10;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrAcceptance = $this->Macceptance->getItemsBaru($intPage,$arrPagination['per_page']);
        //Harus ambil purchase invoice
    }

    if($this->input->post('delAcceptance') != ''){
        $idAcceptance = $this->input->post('delAcceptance');
        //Alurnya, ambil data dulu, dari setiap detail acceptance, kemudian data dikembalikan
        //Setelah itu, data di hapus
        $this->load->model('Minvoice');
        //Check apakah BPB sudah dibuat PI, jika belum, maka bisa dihapus, jika tidak
        //Tidak bisa dihapus
        $this->load->model('Macceptanceitem');
        $arrAccItem = $this->Macceptanceitem->getItemsByAcceptanceID($idAcceptance);
        //echo "<script>alert(".count($arrAccItem).");</script>";
        for($i=0;$i<count($arrAccItem);$i++){
            //echo "<script>alert('".$arrAccItem[$i]['acit_arrived1']." ".$arrAccItem[$i]['acit_purchaseid']." ".$arrAccItem[$i]['product_id_baru']." ".$arrAccItem[$i]['prci_quantity1']."');</script>";
            $qty = $arrAccItem[$i]['prci_quantity1'];
            $arrived = $arrAccItem[$i]['acit_arrived1'];
            $prci_update = $qty+$arrived;
            $prod_id = $arrAccItem[$i]['product_id_baru'];
            $acce_id = $arrAccItem[$i]['acit_purchaseid'];
            echo "<script>alert($prci_lama);</script>";
            $this->load->model('Mpurchaseitem');
            $this->Mpurchaseitem->updatePurchaseItemAfterDelAcc($prod_id, $prci_update, $acce_id);
        }
        $this->load->model('Macceptance');
        $this->Macceptance->deleteByID($idAcceptance);
        $this->load->model('Macceptanceitem');
        $this->Macceptanceitem->deleteByID($idAcceptance);
        $this->session->set_flashdata('strMessage','Penerimaan barang dengan ID '.$idAcceptance.' telah berhasil dihapus');
		redirect('acceptance/browse');
    }

	if(!empty($arrAcceptance)) for($i = 0; $i < count($arrAcceptance); $i++) {

		$arrAcceptance[$i] = array_merge($arrAcceptance[$i],$this->admlinklist->getMenuPermission(23,$arrAcceptance[$i]['acce_status']));
		// $arrAcceptance[$i]['acce_code'] = $arrAcceptance[$i]['acce_status'];
		// $arrAcceptance[$i]['cdate'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrAcceptance[$i]['acce_status']);
        // $arrAcceptance[$i]['idKontrak'] =(float)$arrAcceptance[$i]['acce_grandtotal']+((float)$arrAcceptance[$i]['acce_grandtotal']*(float)$arrAcceptance[$i]['acce_tax']/100);
    }
    
    // if($this->input->post('delete_from_pb') != ''){
    //     echo "<script>console.log('Sesuatu');</script>";
    // }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrAcceptance' => $arrAcceptance,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
    ));
}

}

/* End of File */