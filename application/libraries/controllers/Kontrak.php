<?php 

use App\Models\{Ekontrak, Esubkontrak, Ecustomer};
if(!class_exists('Ekontrak')) require_once(APPPATH.'models/Ekontrak.php');
if(!class_exists('Esubkontrak')) require_once(APPPATH.'models/Esubkontrak.php');
if(!class_exists('Ecustomer')) require_once(APPPATH.'models/Ecustomer.php');
use App\Models\DBFacadesWrapper as DB;
use Illuminate\Database\Query\Expression;

class Kontrak extends JW_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

        $this->load->database();
        $this->cfg['model'] = Ekontrak::class;
    }

    /*
    *   index()
    *   request type GET
    *   return index page containing all data
    */
    public function index()
    {
        $this->_getMenuHelpContent(216,true,'adminpage');

        $kontrakList = Ekontrak::with(['owner', 'subkontraks' => function($q){
                $q->where('status', 0);
            }])
            ->where('latest_rev', 1)
            ->where('status', 0)
            ->groupBy('kont_code')
            ->get();

        $sum = [];
        foreach($kontrakList as $kontrak){
            if($kontrak->subkontrak){
                $sum[$kontrak->id] = $kontrak->subkontraks->sum('job_value');
            }
        }

        
        $this->load->view('sia',array(
            'strViewFile'  => 'kontrak/index',
            'kontrakList'  => $kontrakList,
            'strPageTitle' => 'Kontrak',
            'sum'          => $sum
        ));
        
    }

    /*
    *   view()
    *   request type GET
    *   return view page containing 1 record data
    */
    public function view($id)
    {
        $data           = [];
        $availableOwner = Ecustomer::all();

        $kontrak = Ekontrak::where('kont_code', $id);
        $objects = $kontrak->get();
        $object  = $kontrak->where('latest_rev', 1)->first();

        $kontrakVal = Esubkontrak::where('kont_code', $id)->sum('job_value');

        $data = array_merge($data, [
            'strViewFile'    => 'kontrak/view',
            'strPageTitle'   => 'Kontrak',
            'availableOwner' => $availableOwner,
            'objects'        => $objects,
            'object'         => $object,
            'kontrakVal'     => $kontrakVal,
        ]);

        $this->load->view('sia', $data); 
    }

    /*
    *   action()
    *   request type GET
    *   return add / edit page
    */
    public function action($action, $id = null)
    {
        $data           = [];
        $availableOwner = Ecustomer::all();
        $kontrakVal     = 0;

        if($action == 'add')
        {
            $object  = null;
            $objects = null;
        }
        elseif($action == 'edit')
        {
            $kontrak = Ekontrak::where('kont_code', $id);
            $objects = $kontrak->get();
            $object  = $kontrak->where('latest_rev', 1)->first();
            
            $kontrakVal = Esubkontrak::where('kont_code', $id)->sum('job_value');
        }
        
        $data = array_merge($data, [
            'strViewFile'    => 'kontrak/action',
            'strPageTitle'   => 'Kontrak',
            'formAction'     => base_url() . 'kontrak/' . $action,
            'availableOwner' => $availableOwner,
            'objects'        => $objects,
            'object'         => $object,
            'kontrakVal'     => $kontrakVal,
        ]);

        $this->load->view('sia', $data); 

    }

    /*
    *   add()
    *   request type POST
    *   store data to DB
    */
    public function add()
    {
        generateRepopulate();

        $kontrak = new Ekontrak;
        $kontrak->kont_code   = $this->input->post('kont_code');
        $kontrak->owner_id    = $this->input->post('owner_id');
        $kontrak->kont_name   = $this->input->post('kont_name');
        $kontrak->kont_date   = $this->input->post('kont_date');
        $kontrak->kont_revisi = $this->input->post('kont_revisi');
        $kontrak->latest_rev  = 1;
        $kontrak->cdate       = date("Y-m-d H:i:s");
        $kontrak->save();


        $this->session->set_flashdata('strMessage','Data kontrak has been created successfully.');
        redirect('kontrak/', 'refresh');

    }

    public function edit()
    {
        generateRepopulate();

        DB::transaction(function() 
        {
            // Get value revisi terakhir
            $lastKontrak = Ekontrak::with('kontrak_termin')
                            ->where('kont_code', $this->input->post('kont_code'))
                            ->orderBy('cdate', 'desc')
                            ->first();

            $lastKontrak->latest_rev = 0;
            $lastKontrak->save();

            // create new record, increment revisi
            $kontrak = new Ekontrak;
            $kontrak->kont_code   = $this->input->post('kont_code');
            $kontrak->owner_id    = $this->input->post('owner_id');
            $kontrak->kont_name   = $this->input->post('kont_name');
            $kontrak->kont_date   = $this->input->post('kont_date');
            $kontrak->kont_revisi = $lastKontrak->kont_revisi + 1;
            $kontrak->cdate       = date("Y-m-d H:i:s");
            $kontrak->save();

        });

        $this->session->set_flashdata('strMessage','Data kontrak has been updated.');
        redirect('kontrak/', 'refresh');

    }

    public function delete()
    {
        $data = $this->cfg;

        try
        {
            $code   = $this->input->post('id');
            $object = Ekontrak::where('kont_code', $code)->update(['status' => 2]);



            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                        'status' => 'ok',
                        'text' => 'Data succesfully deleted',
                        'type' => 'success'
                    ])
                );

        }
        catch (\Exception $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
        catch (QueryException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        } 
        catch (\PDOException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }

    }

    public function get($code)
    {
        $kontrak = Ekontrak::with('owner')
            ->where('kont_code', $code)
            ->where('latest_rev', 1)
            ->first();

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($kontrak));
    }

}