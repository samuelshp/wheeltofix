<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Kontrak_Pembelian extends JW_Controller {

public function __construct() {
	parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mkontrakpembelian');

    $this->_getMenuHelpContent(218,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtSaveKontrakPembelian') != '') { // Save Kontrak Pembelian
        $arrKontrakPembelian = array(
            'strKPCode' => $this->input->post('txtKPCode'),
            'strDate' => $this->input->post('txtDate'),
            'intSupplierID' => $this->input->post('intSupplierID'),
            'intProductID' => $this->input->post('intProductID'),
            'intAmount' => $this->input->post('txtAmount'),
            'strDescription' => $this->input->post('txtDescription')
        );
        $this->Mkontrakpembelian->add($arrKontrakPembelian['strKPCode'],$arrKontrakPembelian['strDate'],$arrKontrakPembelian['intSupplierID'],$arrKontrakPembelian['intProductID'],$arrKontrakPembelian['intAmount'],$arrKontrakPembelian['strDescription']);

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'kontrakpembelian-save');
        redirect('kontrak_pembelian/browse');
	}
    $strKPCode = 'KP/'.date('y').'/'.date('m').'/';
    $lastCode = $this->Mkontrakpembelian->getLastKPCode($strKPCode);
    $newCode = $lastCode['code']+1;
    $strKPCode .= str_pad($newCode, 4 , '0', STR_PAD_LEFT);

    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getAllItems();
    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'kontrakpembelian/add',
        'strKPCode' => $strKPCode,
        'intField' => $intField,
        'arrProduct' => $arrProduct,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-add')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
	if($this->input->post('subSave') != '' && !empty($intID)) { # SAVE
		
	} else if($this->input->post('subDelete') != '' && !empty($intID)) {
		
	} else if($this->input->post('subPrint') != '' && !empty($intID)) {
		
	}

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'kontrakpembelian/view',
        'intBookingID' => $intID,
        'arrBookingList' => $arrKontrakPembelianList,
        'arrBookingData' => $arrKontrakPembelianData,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingdata')
    ), $this->admlinklist->getMenuPermission(46,$arrKontrakPembelianData['book_rawstatus'])));

}

public function browse($intPage = 0) {
    $arrKontrakPembelian = $this->Mkontrakpembelian->getAllKontrakPembelian();

    $this->load->view('sia',array(
        'strViewFile' => 'kontrakpembelian/browse',
        'arrKontrakPembelian' => $arrKontrakPembelian,
		'intPage' => $intPage,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-browse')
    ));
}


/* AUTOCOMPLETE */

}

/* End of File */