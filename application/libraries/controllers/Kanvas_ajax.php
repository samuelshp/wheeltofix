<?php

class Kanvas_ajax extends JW_Controller {

public function getProductAutoComplete($txtData = '') {

	$this->load->model('Mproduct');
	$arrProduct = $this->Mproduct->getAllItems($txtData);
	for ($i = 0; $i < count($arrProduct); $i++) {
		$arrProduct[$i]['strName'] = formatProductName("",$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
	}

	ArrayToXml(array('Product' => $arrProduct));
}
    


public function getItemsByDay($intID,$intRecType,$intDay) {
	/*
	 * Untuk muatan sisa (intRecType = 0), ditampilkan semua barang yang tersisa dari hari sebelumnya,
	 * ditambah dengan barang-barang yang baru dimuatkan pada hari terpilih.
	 */


	$arrItems = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID,$intRecType,$intDay);
    

	if (!empty($arrItems)) {
		for ($i = 0; $i < count($arrItems); $i++) {
            if ($arrItems[$i]['saldo'] > -1) {
                if (array_key_exists('conv1', $arrItems[$i]) && array_key_exists('conv2', $arrItems[$i]) && array_key_exists('conv3', $arrItems[$i])) {
                    if ($arrItems[$i]['conv1'] == 0) {
                        $arrItems[$i]['kavi_quantity1'] = 0;
                        $modQty1 = 0;
                    } else {
                        $arrItems[$i]['kavi_quantity1'] = (int) ($arrItems[$i]['saldo'] / $arrItems[$i]['conv1']);
                        $modQty1 = $arrItems[$i]['saldo'] % $arrItems[$i]['conv1'];
                    };

                    if ($arrItems[$i]['conv2'] == 0 ) {
                        $arrItems[$i]['kavi_quantity2'] = 0;
                        $modQty2 = 0;
                    } else {
                        $arrItems[$i]['kavi_quantity2'] = (int) ($modQty1 / $arrItems[$i]['conv2']);
                        $modQty2 = $modQty1 % $arrItems[$i]['conv2'];
                    }

                    $arrItems[$i]['kavi_quantity3'] = $modQty2;
                }
			}
		};

/*
		if ($intRecType == 0) {
			$arrNewItems = $this->Mkanvasitem->getItemsByKanvasIDByDayForEditing($intID,$intRecType,$intDay);
			for ($i = 0; $i < count($arrItems); $i++) {
				for ($j = 0; $j < count($arrNewItems); $j++) {
					if ($arrItems[$i]['product_id'] == $arrNewItems[$j]['product_id']) {

						$arrItems[$i]['kavi_quantity1'] = $arrNewItems[$j]['kavi_quantity1'];
						$arrItems[$i]['kavi_quantity2'] = $arrNewItems[$j]['kavi_quantity2'];
						$arrItems[$i]['kavi_quantity3'] = $arrNewItems[$j]['kavi_quantity3'];
						break;
					}
				}
			}
		};
*/


		for ($i = 0; $i < count($arrItems); $i++) {
			$arrItems[$i]['strName'] = formatProductName("",
				$arrItems[$i]['prod_title'],
				$arrItems[$i]['prob_title'],
				$arrItems[$i]['proc_title']);
		}
	}

	ArrayToXml(array('KanvasItems' => $arrItems));
}

public function getSalesmanKanvasAutoComplete($txtData = '') {

    $this->load->model('Msalesman');
    $arrSalesman = $this->Msalesman->getSalesmanKanvas($txtData);

    ArrayToXml(array('Salesman' => $arrSalesman));

}

}