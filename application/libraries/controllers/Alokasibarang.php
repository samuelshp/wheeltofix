<?php

class Alokasibarang extends JW_Controller {

public function __construct() {
	parent::__construct();
	
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	//$this->load->model('Mdashboard');

	$this->load->model('Malokasibarang');

    $this->_getMenuHelpContent(223,true,'adminpage');
}

public function index(){
	$this->load->view('sia',array(
		'strViewFile' => 'alokasibarang/browse',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
    ));
}

public function browse($intPage = 0){

	$dataPurchase = $this->Malokasibarang->getAllData();

	$this->load->view('sia',array(
		'strViewFile' => 'alokasibarang/browse',
		'intPage' => $intPage,
		'dataPurchase' => $dataPurchase,
        'strPageTitle' => 'Alokasi barang'
	));
}

}

?>