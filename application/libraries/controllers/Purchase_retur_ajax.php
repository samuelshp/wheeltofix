<?php

class Purchase_retur_ajax extends JW_Controller {

public function getDataForPurchase($intID = 0) {
	$arrItem = $this->Mpurchasereturitem->getItemsByPurchaseID($intID);
	$this->load->model('Munit');
	$arrUnit = array();
	$i = 0;
	foreach($arrItem as $e) {
		$arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['prri_product_id']);
		$i++;
	}

	ArrayToXml(array('returItem' => $arrItem,'Unit' => $arrUnit));
}

public function getPurchaseReturAutoComplete($txtData = '') {
	ArrayToXml(array('Supplier' => $this->Mpurchaseretur->getDataAutoComplete($txtData)));
}
public function getPurchaseReturSupplierAutoComplete($txtData = '') {
    $this->load->model('Msupplier');

    ArrayToXml(array('Supplier' => $this->Msupplier->getAllSuppliers($txtData)));
}

public function checkPrintLog($intID='0') {
    $this->load->model('Mprintlog');
    ArrayToXml(array('Kode' => $this->Mprintlog->getInvoiceNumber('purchase_retur',$intID)));
}

}