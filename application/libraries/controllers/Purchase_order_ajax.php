<?php

//Function
// - getKontrakData
// - getBahan
// - getSatuanPB

class Purchase_order_ajax extends JW_Controller {

public function getDataForPurchase($intID = 0) {
	$arrItem = $this->Mpurchasereturitem->getItemsByPurchaseID($intID);
	$this->load->model('Munit');
	$arrUnit = array();
	$i = 0;
	foreach($arrItem as $e) {
		$arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['prri_product_id']);
		$i++;
	}

	ArrayToXml(array('returItem' => $arrItem,'Unit' => $arrUnit));
}

public function getPurchaseReturAutoComplete($txtData = '') {
	ArrayToXml(array('Supplier' => $this->Mpurchaseretur->getDataAutoComplete($txtData)));
}
public function getPurchaseReturSupplierAutoComplete($txtData = '') {
    $this->load->model('Msupplier');

    ArrayToXml(array('Supplier' => $this->Msupplier->getAllSuppliers($txtData)));
}

public function checkPrintLog($intID='0') {
    $this->load->model('Mprintlog');
    ArrayToXml(array('Kode' => $this->Mprintlog->getInvoiceNumber('purchase_retur',$intID)));
}

public function getOwnerData($namaOwner, $idKontrak = ''){
	$this->load->model('Mpurchaseorder');
    ArrayToXml(array('Owner' => $this->Mpurchaseorder->getAllOwner($namaOwner, $idKontrak)));
}

public function getNamaProyek($idCust=''){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('NamaProyek' => $this->Mpurchaseorder->getNamaProyek($idCust)));
}

public function getNamaPekerjaan($namaProyek='', $customerCode='', $kontrakID=''){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('NamaPekerjaan' => $this->Mpurchaseorder->getNamaPekerjaan($namaProyek,$customerCode,$kontrakID)));
}

public function getAllBahanAutoComplete($bahan, $kontrak_id){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('Bahan' => $this->Mpurchaseorder->getAllBahanAutoComplete2($bahan, $kontrak_id)));
}

}