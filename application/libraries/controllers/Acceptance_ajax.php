<?php

class Acceptance_ajax extends JW_Controller {

public function getAllPOData() {
	$this->load->model('Macceptance');
	ArrayToXml(array('PurchaseOrder' => $this->Macceptance->getAllPOData()));
}

public function searchBySupplier(){
	$this->load->model('Macceptance');
	ArrayToXml(array('PurchaseOrder' => $this->Macceptance->searchBySupplier()));
}

}