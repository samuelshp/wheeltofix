<?php
/* 
PUBLIC FUNCTION:
- price_list()

PRIVATE FUNCTION:
- __construct()
*/

class Report extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
}

public function price_list() {
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getAllData();

	if($this->input->post('smtProcessType') == 'Print')
		$this->load->view('sia_print',array(
			'strPrintFile' => 'reportpricelist',
			'arrProduct' => $arrProduct
		));
	else $this->load->view('sia',array(
        'strViewFile' => 'report/pricelist',
		'arrProduct' => $arrProduct
    ));
}

}

/* End of File */