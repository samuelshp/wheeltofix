<?php
/*
PUBLIC FUNCTION:
- index()
- view(intID)
- browse(intPage)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase_retur extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mpurchaseretur');
	$this->load->model('Mpurchasereturitem');

    $this->_getMenuHelpContent(24,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtMakePurchase') != '') { // Make Purchase
		$this->load->model('Munit');
		$arrPurchase=array(
			'intSupplier' => $this->input->post('IDSupp'),
			'intTax' => $this->input->post('txtTax'),
			'strDescription' => $this->input->post('txaDescription'),
			'strDate' => $this->input->post('txtDate'),
			'strEndDate'=> $this->input->post('txtEndDate'),
			'strExpDate'=> $this->input->post('txtExpDate'),
			'intWarehouseID'=> $this->input->post('WarehouseID'),
			'intDisc'=> $this->input->post('dsc4')
		);
		$intPurchaseID = $this->Mpurchaseretur->add($arrPurchase['intSupplier'],$arrPurchase['intWarehouseID'],$arrPurchase['strDescription'],$arrPurchase['intTax'],2,$arrPurchase['strDate'],$arrPurchase['strEndDate'],$arrPurchase['strExpDate'],$arrPurchase['intDisc']);
		$totalItem=$this->input->post('totalItem');
		for($i = 0; $i < $totalItem; $i++) {
			$qty1Item=$this->input->post('qty1PriceEffect' . $i);
			$qty2Item=$this->input->post('qty2PriceEffect' . $i);
			$qty3Item=$this->input->post('qty3PriceEffect' . $i);
			$unit1Item=$this->input->post('sel1UnitID' . $i);
			$unit2Item=$this->input->post('sel2UnitID' . $i);
			$unit3Item=$this->input->post('sel3UnitID' . $i);
			$prcItem=$this->input->post('prcPriceEffect' . $i);
			$dsc1Item=$this->input->post('dsc1PriceEffect' . $i);
			$dsc2Item=$this->input->post('dsc2PriceEffect' . $i);
			$dsc3Item=$this->input->post('dsc3PriceEffect' . $i);
			$prodItem=$this->input->post('prodItem' . $i);
			$probItem=$this->input->post('probItem' . $i);
			$strProductDescription = formatProductName('',$prodItem,$probItem);
			$idItem=$this->input->post('idItem' . $i);
			if($strProductDescription!='' || !empty($strProductDescription)){
				$this->Mpurchasereturitem->add($intPurchaseID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
			}
		}
        $totalItemBonus=$this->input->post('totalItemBonus');
        for($i = 0; $i < $totalItemBonus; $i++) {
            $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
            $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
            $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
            $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
            $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
            $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
            $prodItemBonus = $this->input->post('prodItemBonus'.$i);
            $probItemBonus = $this->input->post('probItemBonus'.$i);
            $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
            $idItemBonus = $this->input->post('idItemBonus'.$i);
            if($strBonusProductDescription!='' || !empty($strBonusProductDescription)){
                $this->Mpurchasereturitem->add($intPurchaseID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
            }
        }
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'return-returnmade'));
		redirect('purchase_retur/view/'.$intPurchaseID);
	}

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'purchaseretur/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addpurchaseretur')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	$this->load->model('Mpurchase');
	if($this->input->post('subSave') != '' && $intID != '') { # SAVE

		// Purchase
		$arrPurchaseData = $this->Mpurchaseretur->getItemByID($intID);
		if(compareData($arrPurchaseData['prcr_status'],array(0))) {
			$this->Mpurchaseretur->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('dsc4'),$this->input->post('selStatus'),$this->input->post('txtTax'),$this->input->post('selEditable'));
			// Load purchase item
			$arrPurchaseItem = $this->Mpurchasereturitem->getItemsByPurchaseID($intID);
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');

            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
            if(!empty($arrPurchaseItem)){
                foreach($arrPurchaseItem as $e) {
                    // Search return quantity
                    if(!empty($arrContainID)) {
                        if (in_array($e['id'], $arrContainID)) {
                            $this->Mpurchasereturitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);

                        } else $this->Mpurchasereturitem->deleteByID($e['id']);

                    } else $this->Mpurchasereturitem->deleteByID($e['id']);
                }
            }
            $arrPurchaseItem = $this->Mpurchasereturitem->getItemsBonusByPurchaseID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');

            $arrContainID = $this->input->post('idProiIDBonus');
            if(!empty($arrPurchaseItem)){
                foreach($arrPurchaseItem as $e) {
                    // Search return quantity
                    if(!empty($arrContainID)) {
                        if (in_array($e['id'], $arrContainID)) {
                            $this->Mpurchasereturitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);

                        } else $this->Mpurchasereturitem->deleteByID($e['id']);

                    } else $this->Mpurchasereturitem->deleteByID($e['id']);
                }
            }
			$totalItem=$this->input->post('totalItem');
			for($i = 0; $i < $totalItem; $i++) {
				$qty1Item=$this->input->post('qty1PriceEffect' . $i);
				$qty2Item=$this->input->post('qty2PriceEffect' . $i);
				$qty3Item=$this->input->post('qty3PriceEffect' . $i);
				$unit1Item=$this->input->post('sel1UnitID' . $i);
				$unit2Item=$this->input->post('sel2UnitID' . $i);
				$unit3Item=$this->input->post('sel3UnitID' . $i);
				$prcItem=$this->input->post('prcPriceEffect' . $i);
				$dsc1Item=$this->input->post('dsc1PriceEffect' . $i);
				$dsc2Item=$this->input->post('dsc2PriceEffect' . $i);
				$dsc3Item=$this->input->post('dsc3PriceEffect' . $i);
				$prodItem=$this->input->post('prodItem' . $i);
				$probItem=$this->input->post('probItem' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem=$this->input->post('idItem' . $i);
				if($strProductDescription!='' || !empty($strProductDescription)){
					$this->Mpurchasereturitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item);
				}
			}
            $totalItemBonus=$this->input->post('totalItemBonus');
            for($i = 0; $i < $totalItemBonus; $i++) {
                $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
                $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
                $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
                $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
                $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
                $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
                $prodItemBonus = $this->input->post('prodItemBonus'.$i);
                $probItemBonus = $this->input->post('probItemBonus'.$i);
                $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
                $idItemBonus = $this->input->post('idItemBonus'.$i);
                if($strBonusProductDescription!='' || !empty($strBonusProductDescription)){
                    $this->Mpurchasereturitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
                }
            }
		} else $this->Mpurchaseretur->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));


		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'return-returnupdated');

	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
        $this->Mpurchasereturitem->deleteByPurchaseID($intID);
		$this->Mpurchaseretur->deleteByID($intID);

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'return-returndeleted'));
		redirect('purchase_retur/browse');

	}
	$arrPurchaseData = $this->Mpurchaseretur->getItemByID($intID);
	if(!empty($arrPurchaseData)) {
		$arrPurchaseData['prcr_rawstatus'] = $arrPurchaseData['prcr_status'];
		$arrPurchaseData['prcr_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrPurchaseData['prcr_status']);
		$arrPurchaseData['prcr_seleditable'] = translateDataIntoHTMLStatements(
			array('title' => 'selEditable', 'field_type' => 'SELECT','description' => 'editable', 'allow_null' => 0, 'validation' => ''),
			$arrPurchaseData['prcr_editable']);

		// Load the purchase item

        $arrPurchaseItem = $this->Mpurchasereturitem->getItemsByPurchaseID($intID);
        $this->load->model('Munit');
        $intPurchaseTotal=0;
        if(!empty($arrPurchaseItem)){
            for($i = 0; $i < count($arrPurchaseItem); $i++) {
                $arrPurchaseItem[$i]['strName']=$arrPurchaseItem[$i]['prri_description'].'|'.$arrPurchaseItem[$i]['proc_title'];
                $convTemp=$this->Munit->getConversion($arrPurchaseItem[$i]['prri_unit1']);
                $intConv1=$convTemp[0]['unit_conversion'];
                $convTemp=$this->Munit->getConversion($arrPurchaseItem[$i]['prri_unit2']);
                $intConv2=$convTemp[0]['unit_conversion'];
                $convTemp=$this->Munit->getConversion($arrPurchaseItem[$i]['prri_unit3']);
                $intConv3=$convTemp[0]['unit_conversion'];
                $arrPurchaseItem[$i]['prri_conv1']=$intConv1;
                $arrPurchaseItem[$i]['prri_conv2']=$intConv2;
                $arrPurchaseItem[$i]['prri_conv3']=$intConv3;
                $intPurchaseTotal+=$arrPurchaseItem[$i]['prri_subtotal'];
            }
        }
        $arrPurchaseData['subtotal_discounted']=(float)$intPurchaseTotal-(float)$arrPurchaseData['prcr_discount'];
        $intPurchaseGrandTotal = (float)$arrPurchaseData['prcr_grandtotal'];
        $arrPurchaseData['subtotal_taxed']=(float)$intPurchaseGrandTotal+($intPurchaseGrandTotal*(float)$arrPurchaseData['prcr_tax']/100);


        $arrPurchaseBonusItem = $this->Mpurchasereturitem->getItemsBonusByPurchaseID($intID);
        if(!empty($arrPurchaseBonusItem)){
            for($i = 0; $i < count($arrPurchaseBonusItem); $i++) {
                $arrPurchaseBonusItem[$i]['strName']=$arrPurchaseBonusItem[$i]['prri_description'].'|'.$arrPurchaseBonusItem[$i]['proc_title'];
                $convTemp=$this->Munit->getConversion($arrPurchaseBonusItem[$i]['prri_unit1']);
                $intConv1=$convTemp[0]['unit_conversion'];
                $convTemp=$this->Munit->getConversion($arrPurchaseBonusItem[$i]['prri_unit2']);
                $intConv2=$convTemp[0]['unit_conversion'];
                $convTemp=$this->Munit->getConversion($arrPurchaseBonusItem[$i]['prri_unit3']);
                $intConv3=$convTemp[0]['unit_conversion'];
                $arrPurchaseBonusItem[$i]['prri_conv1']=$intConv1;
                $arrPurchaseBonusItem[$i]['prri_conv2']=$intConv2;
                $arrPurchaseBonusItem[$i]['prri_conv3']=$intConv3;
            }
        }
	}

    $arrData = array(
		'intPurchaseID' => $intID,
		'intPurchaseTotal' => $intPurchaseTotal,
		'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
		'arrPurchaseList' => $arrPurchaseList,
		'arrPurchaseItem' => $arrPurchaseItem,
        'arrPurchaseBonusItem' => $arrPurchaseBonusItem,
        'arrPurchaseData' => $arrPurchaseData,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchaseretur',
            // 'dataPrint' => $this->Mpurchase->getPrintDataByID($intID),
        ), $arrData));
    } else if(($this->input->post('subPrintTax') != '') || ($this->input->get('print2') != '')) {//print e pajak
        $this->load->model('Mprintlog');
        $first=$this->input->get('first');
        $kode=$this->input->get('kode');
        $date=$this->input->get('date');
        if($first == '1') $idPrintLog = $this->Mprintlog->add($kode,'purchase_retur',$intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchasereturtax',
            'arrPurchaseList' => $arrPurchaseList,
            // 'dataPrint' => $this->Mpurchase->getPrintDataByID($intID),
            'dateTax' => $date,
            'kodeTax' => $kode
        ), $arrData));
    }  else {
        # Load all other purchase data the user has ever made
        $arrPurchaseList = $this->Mpurchaseretur->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'purchaseretur/view',
			'arrPurchaseList' => $arrPurchaseList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-purchasereturnstatus')
        ), $arrData, $this->admlinklist->getMenuPermission(24,$arrPurchaseData['prcr_rawstatus'])));
	}
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrPurchase = $this->Mpurchaseretur->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('purchase_retur/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";
    } else {
		$arrPagination['base_url'] = site_url("purchase_retur/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Mpurchaseretur->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrPurchase = $this->Mpurchaseretur->getItems($intPage,$arrPagination['per_page']);
	}

	if(!empty($arrPurchase)) for($i = 0; $i < count($arrPurchase); $i++) {
		$arrPurchase[$i] = array_merge($arrPurchase[$i],$this->admlinklist->getMenuPermission(33,$arrPurchase[$i]['prcr_status']));
		$arrPurchase[$i]['prcr_rawstatus'] = $arrPurchase[$i]['prcr_status'];
		$arrPurchase[$i]['prcr_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrPurchase[$i]['prcr_status']);//tiny dp invoice_status
		$arrPurchase[$i]['prcr_total'] =(float)$arrPurchase[$i]['prcr_grandtotal']+((float)$arrPurchase[$i]['prcr_grandtotal']*(float)$arrPurchase[$i]['prcr_tax']/100);
	}

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'purchaseretur/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrPurchase' => $arrPurchase,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-purchasereturn')
    ));
}

}

/* End of File */