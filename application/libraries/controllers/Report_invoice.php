<?php

/* 
PUBLIC FUNCTION:
- detailjualperbarangbycustomer($intPage = 0)
- detailjualpercustomerbybarang($intPage = 0)
- detailjualpersalesmanbybarang($intPage = 0)
- detailjualpersalesmanbycustomer($intPage = 0)
- rekapjualpersalesmanbycustomer($intPage = 0)
- rekapjualpercustomerbybarang($intPage = 0)
- rekapjualpersalesmanbybarang($intPage = 0)

- top_selling()
- invoiceperkasir($intPage = 0)

- penjualanrekap($intPage = 0)
# penjualandetail($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_invoice extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mreportinvoice', 'MReport');
}

public function detailjualperbarangbycustomer($intPage = 0) {
    $this->_getMenuHelpContent(135,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strProductID = $this->input->post('productID');
    $strProductCode = $this->input->post('productCode');
    $strProductName = $this->input->post('productName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('productid') != '')
            $arrItems = $this->MReport->searchDetailJualPerBarangByCustomer($this->input->get('productid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';
		
		$strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strProductID = $this->input->get('productid');

        $this->load->model('Mproduct');
        $product = $this->Mproduct->getItemByID($strProductID);
        $product['strName'] = formatProductName('',$product['prod_title'],$product['prob_title'],$product['proc_title']);


        $strProductCode = $product['prod_code'];
        $strProductName = $product['strName'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('productID') != '')
            $arrItems = $this->MReport->searchDetailJualPerBarangByCustomer($this->input->post('productID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));

        else $arrItems = '';

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/detailjualperbarangbycustomer', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/detailjualperbarangbycustomer?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailJualPerBarangByCustomer();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailJualPerBarangByCustomer($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['invi_price'] - ($arrItems[$i]['invi_price'] * $arrItems[$i]['invi_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strProductID' => $strProductID,
        'strProductCode' => $strProductCode,
        'strProductName' => $strProductName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailjualperbarangbycustomer_export', $arrData, true),
        'detail_penjualan_per_barang_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailjualperbarangbycustomer',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'arrItems' => $arrItems,
            'strPageTitle' => 'Laporan',
        ), $arrData));
    }
}

public function detailjualpercustomerbybarang($intPage = 0) {
    $this->_getMenuHelpContent(132,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strCustomerID = $this->input->post('customerID');
    $strCustomerCode = $this->input->post('customerCode');
    $strCustomerName = $this->input->post('customerName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('custid') != '')
            $arrItems = $this->MReport->searchDetailJualPerCustomerByBarang($this->input->get('custid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strCustomerID = $this->input->get('custid');

        $this->load->model('Mcustomer');
        $customer = $this->Mcustomer->getItemByID($strCustomerID);

        $strCustomerCode = $customer['cust_code'];
        $strCustomerName = $customer['cust_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('customerID') != '')
			$arrItems = $this->MReport->searchDetailJualPerCustomerByBarang($this->input->post('customerID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/detailjualpercustomerbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/detailjualpercustomerbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailJualPerCustomerByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailJualPerCustomerByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['invi_price'] - ($arrItems[$i]['invi_price'] * $arrItems[$i]['invi_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strCustomerID' => $strCustomerID,
        'strCustomerCode' => $strCustomerCode,
        'strCustomerName' => $strCustomerName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel($this->load->view($this->config->item('jw_style').'_print/detailjualpercustomerbybarang_export',$arrData,true),'detail_penjualan_per_barang_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view($this->config->item('jw_style').'_print/detailjualpercustomerbybarang',$arrData);
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/detailjualpercustomerbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function detailjualpersalesmanbybarang($intPage = 0) {
    $this->_getMenuHelpContent(136,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchDetailJualPerSalesmanByBarang($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchDetailJualPerSalesmanByBarang($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/detailjualpersalesmanbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_nvoice/detailjualpersalesmanbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailJualPerSalesmanByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailJualPerSalesmanByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['invi_price'] - ($arrItems[$i]['invi_price'] * $arrItems[$i]['invi_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailjualpersalesmanbybarang_export', $arrData, true),
        'detail_jual_per_salesman_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailjualpersalesmanbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/detailjualpersalesmanbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function detailjualpersalesmanbycustomer($intPage = 0) {
    $this->_getMenuHelpContent(145,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchDetailJualPerSalesmanByCustomer($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchDetailJualPerSalesmanByCustomer($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/detailjualpersalesmanbycustomer', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/detailjualpersalesmanbycustomer?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailJualPerSalesmanByCustomer();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailJualPerSalesmanByCustomer($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailjualpersalesmanbycustomer_export', $arrData, true),
        'detail_jual_per_salesman_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailjualpersalesmanbycustomer',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/detailjualpersalesmanbycustomer',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function rekapjualpersalesmanbycustomer($intPage = 0) {
    // $this->_getMenuHelpContent(91,true,'adminpage');

    

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchRekapJualPerSalesmanByCustomer($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';

    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchRekapJualPerSalesmanByCustomer($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/rekapjualpersalesmanbycustomer', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/rekapjualpersalesmanbycustomer?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountRekapJualPerSalesmanByCustomer();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsRekapJualPerSalesmanByCustomer($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/rekapjualpersalesmanbycustomer_export', $arrData, true),
        'rekap_jual_per_salesman_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'rekapjualpersalesmanbycustomer',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/rekapjualpersalesmanbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }


}

public function rekapjualpercustomerbybarang($intPage = 0) {
    // $this->_getMenuHelpContent(92,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strCustomerID = $this->input->post('customerID');
    $strCustomerCode = $this->input->post('customerCode');
    $strCustomerName = $this->input->post('customerName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('custid') != '')
            $arrItems = $this->MReport->searchRekapJualPerCustomerByBarang($this->input->get('custid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strCustomerID = $this->input->get('custid');

        $this->load->model('Mcustomer');
        $customer = $this->Mcustomer->getItemByID($strCustomerID);

        $strCustomerCode = $customer['cust_code'];
        $strCustomerName = $customer['cust_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('customerID') != '')
			$arrItems = $this->MReport->searchRekapJualPerCustomerByBarang($this->input->post('customerID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/rekapjualpercustomerbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/rekapjualpercustomerbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountRekapJualPerCustomerByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsRekapJualPerCustomerByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strCustomerID' => $strCustomerID,
        'strCustomerCode' => $strCustomerCode,
        'strCustomerName' => $strCustomerName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/rekapjualpercustomerbybarang_export', $arrData, true),
        'rekap_jual_per_customer_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'rekapjualpercustomerbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/rekapjualpercustomerbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }

}

public function rekapjualpersalesmanbybarang($intPage = 0) {
    // $this->_getMenuHelpContent(93,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchRekapJualPerSalesmanByBarang($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchRekapJualPerSalesmanByBarang($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/rekapjualpersalesmanbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_invoice/rekapjualpersalesmanbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountRekapJualPerSalesmanByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsRekapJualPerSalesmanByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/rekapjualpersalesmanbybarang_export', $arrData, true),
        'rekap_jual_per_salesman_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'rekapjualpersalesmanbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/rekapjualpersalesmanbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }

}

public function top_selling($intWarehouseID = 0) {
    $this->_getMenuHelpContent(61,true,'adminpage');
    
    $this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getNonServiceData();
    $this->load->model('Mwarehouse'); $arrWarehouse = $this->Mwarehouse->getAllData();
    $this->load->model('Minvoice');
    
    // Get last stock update?
    $this->load->model('Mtinydbvo','defaultconfig'); $this->defaultconfig->initialize('jwdefaultconfig');
    $strLastStockUpdate = formatDate2($this->defaultconfig->getSingleData('JW_STOCK_UPDATE'),'Y-m-d H:i:s');
    
    if(empty($intWarehouseID)) $intWarehouseID = $arrWarehouse[0]['id'];
    
    $arrInvoiceIndex = array();
    for($i = 0; $i < count($arrProduct); $i++) for($j = 0; $j < count($arrWarehouse); $j++) {
        $arrProduct[$i][$j] = $this->Minvoice->getStockSold($arrProduct[$i]['id'],$arrWarehouse[$j]['id'],$strLastStockUpdate);
        if($arrWarehouse[$j]['id'] == $intWarehouseID) $arrInvoiceIndex[$i] = $arrProduct[$i][$j];
    }
    
    array_multisort($arrInvoiceIndex,SORT_DESC,$arrProduct);
    
    $this->load->view('sia',array(
        'strViewFile' => 'report_invoice/topselling',
        'arrProduct' => $arrProduct,
        'arrWarehouse' => $arrWarehouse,
        'intWarehouseID' => $intWarehouseID,
        'strLastStockUpdate' => $strLastStockUpdate
    ));
}

// patricklipesik begin
public function invoiceperkasir($intPage = 0) {
    $this->_getMenuHelpContent(133,true,'adminpage');

    if($this->input->post('subLock') == 'lock') {
        $this->load->model('Minvoice');
        $this->Minvoice->lockItemsByID(implode(',', $this->input->post('cbLock')));
        $this->session->set_flashdata('strMessage', 'Anda telah berhasil mengunci penjualan anda');
        redirect('report_invoice/invoiceperkasir');
    }

    $cname = 'invoiceperkasir';
    
    // 1. persiapan variabel untuk menampung filter
    $strStart = $this->input->post('txtStart');
    if(empty($strStart))
        $strStart = date('Y/m/d');
    
    $strEnd = $this->input->post('txtEnd');
    if(empty($strEnd))
        $strEnd = date('Y/m/d');
    
    $strUserID = $this->input->post('userID');
    if(empty($strUserID))
        $strUserID = '';
    
    $strUserCode = $this->input->post('userCode');
    if(empty($strUserCode))
        $strUserCode = '';
    // 1. end
    
    // 2. ketika tombol print diklik
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        // 2a. persiapan variabel untuk menampung filter dengan cara GET
        $strStart = $this->input->get('start');
        if(empty($strStart))
            $strStart = date('Y/m/d');
        
        $strEnd = $this->input->get('end');
        if(empty($strEnd))
            $strEnd = date('Y/m/d');
        
        if($this->input->get('userid') != '')
        {
            $strUserID = $this->input->get('userid');
            
            $this->load->model('Madminlogin');
            $user = $this->Madminlogin->getItemByID($strUserID);
            $strUserCode = $user['adlg_login'];
        }
        else
        {
            $strUserID = '';
            $strUserCode = '';
        }
        // 2a. end
        
        // 2b. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchInvoicePerKasir($strStart,$strEnd,$strUserID);
        $strPage = '';
        // 2b. end
        
    }
    // 2. end
    
    // 3. ketika tombol filter diklik
    else if($this->input->post('subSearch') != '') {
        
        // 3a. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchInvoicePerKasir($strStart,$strEnd,$strUserID);
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/'.$cname, NULL, FALSE).'">[Back]</a>';
        // 3a. end
        
    }
    // 3. end
    
    // 4. ketika pertama kali menu dibuka
    else {
        
        // 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
        $arrPagination['base_url'] = site_url("report_invoice/".$cname."?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountInvoicePerKasir();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        
        $strBrowseMode = '';
        if($this->input->get('page') != '')
            $intPage = $this->input->get('page');
        // 4a. end
        
        // 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
        $arrItems = $this->MReport->getItemsInvoicePerKasir($intPage,$arrPagination['per_page']);
        // 4b. end
        
    }
    // 4.end
    
    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strUserID' => $strUserID,
        'strUserCode' => $strUserCode,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // 5a. tampilan excel (views\jwshop1\print\vreport..._export)
    if($this->input->get('excel') != '') {
        HTMLToExcel(
            $this->load->view('sia_print/report'.$cname.'_export', $arrData, true),
        $cname.'.xls');
    }
    // 5a. end

    // 5b. tampilan print (views\jwshop1\print\vreport...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'report'.$cname,
        ), $arrData));
    }
    // 5b. end
    
    // 5c. tampilan default menu (views\jwshop1\vreport...)
    else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/'.$cname,
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
    // 5c. end
    
}

public function penjualanrekap($intPage = 0) {
    $this->_getMenuHelpContent(131,true,'adminpage');

    $cname = 'penjualanrekap';
    
    // 1. persiapan variabel untuk menampung filter
    $strStart = $this->input->post('txtStart');
    if(empty($strStart))
        $strStart = date('Y/m/d');
    
    $strEnd = $this->input->post('txtEnd');
    if(empty($strEnd))
        $strEnd = date('Y/m/d');
    
    $strCustomerID = $this->input->post('customerID');
    if(empty($strCustomerID))
        $strCustomerID = '';
    $strCustomerCode = $this->input->post('customerCode');
    if(empty($strCustomerCode))
        $strCustomerCode = '';
    $strCustomerName = $this->input->post('customerName');
    if(empty($strCustomerName))
        $strCustomerName = '';
    
    $strSalesmanID = $this->input->post('salesmanID');
    if(empty($strSalesmanID))
        $strSalesmanID = '';
    $strSalesmanCode = $this->input->post('salesmanCode');
    if(empty($strSalesmanCode))
        $strSalesmanCode = '';
    $strSalesmanName = $this->input->post('salesmanName');
    if(empty($strSalesmanName))
        $strSalesmanName = '';
    // 1. end
    
    // 2. ketika tombol print diklik
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        // 2a. persiapan variabel untuk menampung filter dengan cara GET
        $strStart = $this->input->get('start');
        if(empty($strStart))
            $strStart = date('Y/m/d');
        
        $strEnd = $this->input->get('end');
        if(empty($strEnd))
            $strEnd = date('Y/m/d');
        
        if($this->input->get('customerid') != '')
        {
            $strCustomerID = $this->input->get('customerid');
            
            $this->load->model('Mcustomer');
            $user = $this->Mcustomer->getItemByID($strCustomerID);
            $strCustomerCode = $user['cust_code'];
            $strCustomerName = $user['cust_name'];
        }
        else
        {
            $strCustomerID = '';
            $strCustomerCode = '';
            $strCustomerName = '';
        }
        
        if($this->input->get('salesmanid') != '')
        {
            $strSalesmanID = $this->input->get('salesmanid');
            
            $this->load->model('Msalesman');
            $user = $this->Msalesman->getItemByID($strSalesmanID);
            $strSalesmanCode = $user['sals_code'];
            $strSalesmanName = $user['sals_name'];
        }
        else
        {
            $strSalesmanID = '';
            $strSalesmanCode = '';
            $strSalesmanName = '';
        }
        // 2a. end
        
        // 2b. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchPenjualanRekap($strStart,$strEnd,$strCustomerName,$strSalesmanName);
        $strPage = '';
        // 2b. end
        
    }
    // 2. end
    
    // 3. ketika tombol filter diklik
    else if($this->input->post('subSearch') != '') {
        
        // 3a. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchPenjualanRekap($strStart,$strEnd,$strCustomerName,$strSalesmanName);
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_invoice/'.$cname, NULL, FALSE).'">[Back]</a>';
        // 3a. end
        
    }
    // 3. end
    
    // 4. ketika pertama kali menu dibuka
    else {
        
        // 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
        $arrPagination['base_url'] = site_url("report_invoice/".$cname."?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountPenjualanRekap();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        
        $strBrowseMode = '';
        if($this->input->get('page') != '')
            $intPage = $this->input->get('page');
        // 4a. end
        
        // 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
        $arrItems = $this->MReport->getItemsPenjualanRekap($intPage,$arrPagination['per_page']);
        // 4b. end
        
    }
    // 4.end
    
    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strCustomerID' => $strCustomerID,
        'strCustomerCode' => $strCustomerCode,
        'strCustomerName' => $strCustomerName,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    // 5a. tampilan excel (views\jwshop1\print\vreport..._export)
    if($this->input->get('excel') != '') {
        HTMLToExcel($this->load->view($this->config->item('jw_style').'_print/report'.$cname.'_export',$arrData,true),$cname.'.xls');
    }
    // 5a. end
    
    // 5b. tampilan print (views\jwshop1\print\vreport...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $this->load->view($this->config->item('jw_style').'_print/report'.$cname,$arrData);
    }
    // 5b. end
    
    // 5c. tampilan default menu (views\jwshop1\vreport...)
    else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/'.$cname,
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
    // 5c. end
    
}

}