<?php 

use App\Models\{Ekontrak, Esubkontrak, Esubkontrakteam, Esubkontraktermin, Esubkontrakmaterial, Esubkontraknonmaterial, Ejabatan, Ebahan, Epegawai};
if(!class_exists('Ekontrak')) require_once(APPPATH.'models/Ekontrak.php');
if(!class_exists('Esubkontrak')) require_once(APPPATH.'models/Esubkontrak.php');
if(!class_exists('Esubkontrakteam')) require_once(APPPATH.'models/Esubkontrakteam.php');
if(!class_exists('Esubkontraktermin')) require_once(APPPATH.'models/Esubkontraktermin.php');
if(!class_exists('Esubkontrakmaterial')) require_once(APPPATH.'models/Esubkontrakmaterial.php');
if(!class_exists('Esubkontraknonmaterial')) require_once(APPPATH.'models/Esubkontraknonmaterial.php');
if(!class_exists('Ejabatan')) require_once(APPPATH.'models/Ejabatan.php');
if(!class_exists('Ebahan')) require_once(APPPATH.'models/Ebahan.php');
if(!class_exists('Epegawai')) require_once(APPPATH.'models/Epegawai.php');
use App\Models\DBFacadesWrapper as DB;

class Subkontrak extends JW_Controller
{
    public function __construrct()
    {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    }

    public function index()
    {
        $this->_getMenuHelpContent(217,true,'adminpage');


        $arrPagination['base_url'] = site_url("subkontrak/?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = Esubkontrak::count();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $subkontrakList = Esubkontrak::with(['kontrak', 'subkontrak_termin','subkontrak_team.staff' => function($q){
                $q->where('adlg_jabatan', 7);
            }])
            ->where('status', 0)
            ->get();
        // dd($subkontrakList);
        
        $strBrowseMode = '';

        $this->load->view('sia',array(
            'strViewFile' => 'subkontrak/index',
            'strPage' => $strPage,
            'strPageTitle' => 'Subkontrak',
            'strBrowseMode' => $strBrowseMode,
            'subkontrakList' => $subkontrakList,
        ));
    }

    public function view($id)
    {
        $this->load->model('Mtinydbvo');
        $this->Mtinydbvo->initialize('jwsettingpic');

        $availableJabatan = $this->Mtinydbvo->getAllData();
        $availableBahan   = Ebahan::all();
        $availablePegawai = Epegawai::all();

        $object = Esubkontrak::with([
                'kontrak.owner',
                'subkontrak_termin',
                'subkontrak_team.staff', 
                'subkontrak_material.bahan.satuan_bayar', 
                'subkontrak_nonmaterial'
            ])
            ->where('id', $id)
            ->first();
        
        $material    = $object->subkontrak_material->sum('jumlah');
        $nonmaterial = $object->subkontrak_nonmaterial->sum('amount');
        $hpp         = $material + $nonmaterial;

        if($_SESSION['strAdminPriviledge'] == 4 || $_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 7)
            $piutangPriv = true;
        else
            $piutangPriv = false;

        $this->load->view('sia',array(
            'strViewFile'      => 'subkontrak/view',
            'strPageTitle'     => 'SubKontrak',
            'availableJabatan' => $availableJabatan,
            'availablePegawai' => $availablePegawai,
            'availableBahan'   => $availableBahan,
            'object'           => $object,
            'hpp'              => $hpp,
            'piutangPriv'      => $piutangPriv,
        ));
    }

    public function action($action, $id = null)
    {
        $this->load->model('Mtinydbvo');
        $this->Mtinydbvo->initialize('jwsettingpic');

        $data = [];
        $availableJabatan = $this->Mtinydbvo->getAllData();
        $availableBahan   = Ebahan::all();
        $availablePegawai = Epegawai::all();
        $hpp = 0;

        if($_SESSION['strAdminPriviledge'] == 4 || $_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 7)
            $piutangPriv = true;
        else
            $piutangPriv = false;

        $availableKontrak = Ekontrak::where('latest_rev', 1)
                ->where('status', 0)
                ->where('latest_rev', 1)
                ->get();

        // page action hanya membutuhkan object(singular) jika tidak menggunakan revisi/versi
        if($action == 'add')
        {

            $object  = null;
        }
        elseif($action == 'edit')
        {

            $object = Esubkontrak::with([
                    'kontrak.owner',
                    'subkontrak_termin',
                    'subkontrak_team.staff', 
                    'subkontrak_material.bahan.satuan_bayar', 
                    'subkontrak_nonmaterial'
                ])
                ->where('id', $id)
                ->first();
                
            $material    = $object->subkontrak_material->sum('jumlah');
            $nonmaterial = $object->subkontrak_nonmaterial->sum('amount');
            $hpp         = $material + $nonmaterial;
        }
        
        $data = array_merge($data, [
            'strViewFile'      => 'subkontrak/action',
            'strPageTitle'     => 'SubKontrak',
            'formAction'       => site_url('subkontrak/'.$action),
            'object'           => $object,
            'availableJabatan' => $availableJabatan,
            'availablePegawai' => $availablePegawai,
            'availableBahan'   => $availableBahan,
            'availableKontrak' => $availableKontrak,
            'hpp'              => $hpp,
            'piutangPriv'      => $piutangPriv,
        ]);
        
        $this->load->view('sia', $data); 

    }

    public function add()
    {
        generateRepopulate();

        DB::transaction(function() 
        {
            $subkontrak               = new Esubkontrak;
            $subkontrak->kontrak_id   = $this->input->post('kontrak_id');
            $subkontrak->kont_code    = $this->input->post('kont_code');
            $subkontrak->job          = $this->input->post('job');
            $subkontrak->job_value    = $this->input->post('job_value');
            $subkontrak->subkont_date = $this->input->post('subkont_date');
            $subkontrak->save();

            if(count($this->input->post('name')) > 0)
            {
                foreach($this->input->post('name') as $key => $item)
                {
                    $team                = new Esubkontrakteam;
                    $team->subkontrak_id = $subkontrak->id;
                    $team->name          = $this->input->post('name')[$key];
                    $team->jabatan_id    = $this->input->post('jabatan_id')[$key];
                    $team->save();
                }
            }

            if(count($this->input->post('jenis')) > 0)
            {
                foreach($this->input->post('jenis') as $key => $value)
                { 
                    $termin                = new Esubkontraktermin; 
                    $termin->subkontrak_id = $subkontrak->id; 
                    $termin->jenis         = $value; 
                    $termin->amount        = $this->input->post('termin_amount')[$key]; 
                    $termin->due_date      = $this->input->post('due_date')[$key]; 
                    $termin->save(); 
                }
            }

            if(count($this->input->post('material')) > 0)
            {
                foreach($this->input->post('material') as $key => $item)
                {
                    $material                = new Esubkontrakmaterial;
                    $material->subkontrak_id = $subkontrak->id;
                    $material->material      = $this->input->post('material')[$key];
                    $material->qty           = $this->input->post('qty')[$key];
                    $material->keterangan    = $this->input->post('keterangan')[$key];
                    $material->hpp           = $this->input->post('hpp')[$key];
                    $material->jumlah        = $this->input->post('jumlah')[$key]? $this->input->post('jumlah')[$key] : ($this->input->post('qty')[$key] * $this->input->post('hpp')[$key]);
                    $material->save();
                }
            }

            if(count($this->input->post('kategori')) > 0)
            {
                foreach($this->input->post('kategori') as $key => $item)
                {
                    $nonmaterial                = new Esubkontraknonmaterial;
                    $nonmaterial->subkontrak_id = $subkontrak->id;
                    $nonmaterial->kategori      = $this->input->post('kategori')[$key];
                    $nonmaterial->amount        = $this->input->post('amount')[$key];
                    $nonmaterial->save();
                }
            }
        });

        $this->session->set_flashdata('strMessage','Data kontrak has been created successfully.');
        redirect('subkontrak/', 'refresh');

    }

    public function edit()
    {
        generateRepopulate();

        DB::transaction(function() 
        {
            $subkontrak = Esubkontrak::where('id', $this->input->post('id'))->first();
            $subkontrak->kontrak_id   = $this->input->post('kontrak_id');
            $subkontrak->kont_code    = $this->input->post('kont_code');
            $subkontrak->job          = $this->input->post('job');
            $subkontrak->job_value    = $this->input->post('job_value');
            $subkontrak->subkont_date = $this->input->post('subkont_date');
            $subkontrak->save();

            if(count($this->input->post('name')) > 0)
            {
                $subkontrak_team = Esubkontrakteam::where('subkontrak_id', $subkontrak->id);
                if($subkontrak_team->count() > 0) $subkontrak_team->delete();
                foreach($this->input->post('name') as $key => $item)
                {
                    $team                = new Esubkontrakteam;
                    $team->subkontrak_id = $subkontrak->id;
                    $team->name          = $this->input->post('name')[$key];
                    $team->jabatan_id    = $this->input->post('jabatan_id')[$key];
                    $team->save();
                }
            }

            if(count($this->input->post('jenis')) > 0)
            {
                $subkontrak_material = Esubkontraktermin::where('subkontrak_id', $subkontrak->id);
                if($subkontrak_material->count() > 0) $subkontrak_material->delete();
                foreach($this->input->post('jenis') as $key => $value)
                { 
                    $termin                = new Esubkontraktermin; 
                    $termin->subkontrak_id = $subkontrak->id; 
                    $termin->jenis         = $value; 
                    $termin->amount        = $this->input->post('termin_amount')[$key]; 
                    $termin->due_date      = $this->input->post('due_date')[$key]; 
                    $termin->save(); 
                }
            } 

            if(count($this->input->post('material')) > 0)
            {
                $subkontrak_material = Esubkontrakmaterial::where('subkontrak_id', $subkontrak->id);
                if($subkontrak_material->count() > 0) $subkontrak_material->delete();
                foreach($this->input->post('material') as $key => $item)
                {
                    $material                = new Esubkontrakmaterial;
                    $material->subkontrak_id = $subkontrak->id;
                    $material->material      = $this->input->post('material')[$key];
                    $material->qty           = $this->input->post('qty')[$key];
                    $material->keterangan    = $this->input->post('keterangan')[$key];
                    $material->hpp           = $this->input->post('hpp')[$key];
                    $material->jumlah        = $this->input->post('jumlah')[$key]? $this->input->post('jumlah')[$key] : ($this->input->post('qty')[$key] * $this->input->post('hpp')[$key]);
                    $material->save();
                }
            }

            if(count($this->input->post('kategori')) > 0)
            {
                $subkontrak_nonmaterial = Esubkontraknonmaterial::where('subkontrak_id', $subkontrak->id);
                if($subkontrak_nonmaterial->count() > 0) $subkontrak_nonmaterial->delete();
                foreach($this->input->post('kategori') as $key => $item)
                {
                    $nonmaterial = new Esubkontraknonmaterial;
                    $nonmaterial->subkontrak_id = $subkontrak->id;
                    $nonmaterial->kategori      = $this->input->post('kategori')[$key];
                    $nonmaterial->amount        = $this->input->post('amount')[$key];
                    $nonmaterial->save();
                }
            }
        });

        $this->session->set_flashdata('strMessage','Data kontrak has been updated.');
        redirect('subkontrak/', 'refresh');

    }

    public function  delete()
    {
        $data = $this->cfg;

        try
        {
            $id   = $this->input->post('id');
            $object = Esubkontrak::where('id', $id)->update(['status' => 2]);



            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                        'status' => 'ok',
                        'text' => 'Data succesfully deleted',
                        'type' => 'success'
                    ])
                );

        }
        catch (\Exception $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
        catch (QueryException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        } 
        catch (\PDOException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
    }
}