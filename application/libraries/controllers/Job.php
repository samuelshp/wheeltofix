<?php


class Job extends JW_Controller {

    public function __construct() {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

        $this->_getMenuHelpContent(121,true,'adminpage');
    }

    public function index() {$this->browse();}

    public function browse($intPage = 0) {

        // Last stock update?
        $this->load->model('Mtinydbvo','defaultconfig');
        $this->defaultconfig->initialize('jwdefaultconfig');
        $strLastStockUpdate = $this->defaultconfig->getSingleData('JW_STOCK_UPDATE');
        
        $this->load->model('Mproject'); 
        $arrProject = $this->Mproject->getAllData();
        
        $this->load->model('MJob'); // Load product

        $bolCalibrate = FALSE;
        if($this->input->post('subSearch') != '') {
            if($this->input->post('filterType') == 'stocklimit_only') {
                $arrProduct = $this->Mproduct->getStockLimitItems();
                $strPage = '';
                $strBrowseMode = '<a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';
                $this->_arrData['strMessage'] = 'Display Stock Limited Products <a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';

            } else {
                $arrProduct = $this->Mproduct->searchByTitle($this->input->post('txtSearchValue'));
                $strPage = '';
                $strBrowseMode = '<a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';
                $this->_arrData['strMessage'] = "Search by Product Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
            }
        } else {
            if($this->input->post('subCalibrate') != '' || $this->input->get('calibrate') != '') {
                $arrPagination['base_url'] = site_url("inventory/browse?pagination=true&calibrate=true", NULL, FALSE);
                $bolCalibrate = TRUE;
            } else {
                $arrPagination['base_url'] = site_url("inventory/browse?pagination=true", NULL, FALSE);
            }
            $arrPagination['total_rows'] = $this->Mproduct->getCount();
            $arrPagination['per_page'] = $this->config->item('jw_item_count');
            $arrPagination['uri_segment'] = 3;
            $this->pagination->initialize($arrPagination);
            $strPage = $this->pagination->create_links();

            $strBrowseMode = '';
            if($this->input->get('page') != '') $intPage = $this->input->get('page');

            if($bolCalibrate) {
                $this->load->model('Mstock');
                $this->Mstock->calibrate($intPage,$arrPagination['per_page']);
            }
            $arrProduct = $this->Mproduct->getItems($intPage,$arrPagination['per_page']);
        }

        if(!empty($arrProduct)) for($i = 0; $i < count($arrProduct); $i++)
            for($j = 0; $j < count($arrWarehouse); $j++) {
            $arrProduct[$i]['arrStock'][$arrWarehouse[$j]['id']] = countStock($arrProduct[$i]['id'],$arrWarehouse[$j]['id']);
        }

        if($this->input->get('done_calibrate') != '') {
            $this->_arrData['strMessage'] = 'Proses Kalibrasi Stok sudah selesai';
        }

        // Load the views
        $this->load->view('sia',array(
            'strViewFile' => 'job/browse',
            'strLastStockUpdate' => $strLastStockUpdate,
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'arrProduct' => $arrProduct,
            'arrWarehouse' => $arrWarehouse,
            'bolCalibrate' => $bolCalibrate,
            // 'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory')
            'strPageTitle' => 'Job List'
        ));
    }

    public function view($intProductID,$intWarehouseID) {
        // Last stock update?
        $this->load->model('Mtinydbvo','defaultconfig');
        $this->defaultconfig->initialize('jwdefaultconfig');
        $strLastStockUpdate = $this->defaultconfig->getSingleData('JW_STOCK_UPDATE');
        $intTotalStock = 0;
        
        // Load product
        $this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getItemByID($intProductID);
        $this->load->model('Mstock'); $arrStock = $this->Mstock->getHistoryByProductID($intProductID,$intWarehouseID);
        $i=0;
        if(!empty($arrStock)) foreach($arrStock as $e) {
            //di sini kalok uda ada id e seng disimpen di transaction_history
            $arrStock[$i]['code'] = $this->Mstock->getDynamicCode($e['trhi_type'],$e['trhi_transaction_id']);
            $i++;
        }
        // Load warehouse
        $this->load->model('Mwarehouse'); $arrWarehouse = $this->Mwarehouse->getItemByID($intWarehouseID);
        
        // Load the views
        $this->load->view('sia',array(
            'strViewFile' => 'job/view',
            'arrProduct' => $arrProduct,
            'arrStock' => $arrStock,
            'arrWarehouse' => $arrWarehouse,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory')
        ));
    }

}

/* End of File */