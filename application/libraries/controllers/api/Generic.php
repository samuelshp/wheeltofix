<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'core/JW_REST_Controller.php';

class Generic extends JW_REST_Controller {

private $_strTable, $_strSelect, $_strCustomQuery;
private $_arrWhere, $_arrOrder;
private $_arrAdmTable, $_arrAdmFields, $_arrAdmFieldIndex, $_arrFieldPreException;
private $_arrResponse;

public function __construct() {
	parent::__construct();
	$this->load->model('Mdbvo', '_Mtable');
	$this->load->model('Mdbvo', 'AdmTable');
	$this->AdmTable->initialize('adm_table_list');
	$this->load->model('Mdbvo', 'AdmFields');
	$this->AdmFields->initialize('adm_field_list');
	$this->_strTable = $this->_strSelect = $this->_strCustomQuery = '';
	$this->_arrWhere = $this->_arrOrder = $this->_arrAdmTable = $this->_arrAdmFields = $this->_arrAdmFieldIndex = [];
	$this->_arrFieldPreException = ['id', 'cby', 'cdate', 'mby', 'mdate', 'meta_title', 'meta_description', 'meta_keyword'];
}

public function initialize($strTable) {
	$this->_detectTable($strTable);
	if(!$this->_Mtable->initialize($this->_strTable, $this->_strCustomQuery)) $this->_Mtable->initialize('jw_'.$this->_strTable, $this->_strCustomQuery);
	$this->AdmTable->search($this->_Mtable->getTableName(), 'title');
	$this->_arrAdmTable = $this->AdmTable->getNextRecord('Array');
	$this->_strPre = $this->_arrAdmTable['pre'];
}

public function index_get($strTable, $intPageNo = 0) {
	$this->initialize($strTable);
	$this->_parseQuery();

	$strWhere = $this->_translateWhere();
	$intTotalRows = $this->_Mtable->dbCount($strWhere);

	if(intval($intPageNo) > 0) $this->_Mtable->dbSelect($this->_strSelect, $strWhere, $this->_translateOrder(), $this->config->item('jw_item_count'), (intval($intPageNo) - 1) * $this->config->item('jw_item_count'));
	else $this->_Mtable->dbSelect($this->_strSelect, $strWhere, $this->_translateOrder());

	if($intCount = $this->_Mtable->getNumRows()) $this->_showResponse(true, [
			'count' => $this->_Mtable->getNumRows(),
			'total_rows' => $intTotalRows,
			'data' => $this->_getTableRows(),
		]);
	$this->_showResponse(false);
}

public function item_get($strTable, $strKey, $strField = 'id') {
	$this->initialize($strTable);

	$bolForceReturnMultiDimension = false;
	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;
	switch($strTable) {
		case 'product_category_per_supplier':
			$this->_arrWhere[] = "s.$strField = '$strKey'";
			$this->_arrOrder[] = 't.id ASC';
			$bolForceReturnMultiDimension = true;
		break; default:
			$this->_arrWhere[] = "$strField = '$strKey'";
		break;
	}

	$this->_parseQuery();
	$strWhere = $this->_translateWhere();
	$strOrder = $this->_translateOrder();

	$intTotalRows = $this->_Mtable->dbCount($strWhere);
	$this->_Mtable->dbSelect($this->_strSelect, $strWhere, $strOrder);

	if($intCount = $this->_Mtable->getNumRows()) $this->_showResponse(true, [
		    'count' => $intCount,
		    'total_rows' => $intTotalRows,
		    'data' => ($bolForceReturnMultiDimension || intval($intCount) > 1) ? $this->_getTableRows() : $this->_getTableRow(),
		]);
	$this->_showResponse(false);
}

public function item_post($strTable, $strKey = '', $strField = 'id') {
	$this->initialize($strTable);
	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;

	$arrData = [];
	$arrPost = $this->post();
	if(empty($arrPost)) $arrPost = $this->put();
	foreach($arrPost as $i => $e) {
		if($this->_isFieldMustAppendPre($i)) $i = $this->_strPre.'_'.$i;
		$arrData[$i] = $e;
	}

	$this->_setFieldList($arrData);
	foreach($arrData as $i => $e) if(!empty($arrData[$i]) && !empty($this->_arrAdmFieldIndex[$i]) && !empty($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]])) {
		$arrData[$i] = translateDataIntoSafeStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]], $arrData[$i]);
	}

	if(count($_FILES) > 0) {
		$strUploadPath = 'upload/'.$strTable.'/';
		$config = ['upload_path' => $strUploadPath];
		if($this->query('thumbnail')) {
			$config['thumbnail'] = $this->query('thumbnail');
			$config['thumbnail_dimension'] = $this->query('thumbnail_dimension');
		}
		if($this->query('max_dimension')) $config['max_dimension'] = $this->query('max_dimension');
		$this->load->library('upload', $config);
		foreach($_FILES as $i => $e) {
			if($arrUploadData = $this->upload->do_upload($i)) {
				$strPath = translateDataIntoSafeStatements(['field_type' => 'FILE'], base_url($strUploadPath.$arrUploadData['file_name']));
				$strFieldTitle = $this->_strPre.'_'.rtrim($i, '0123456789');
				if(empty($arrData[$strFieldTitle])) $arrData[$strFieldTitle] = $strPath;
				else $arrData[$strFieldTitle] .= '; '.$strPath;
			}
		}
	}

	if(!empty($arrData)) {
		$this->_arrWhere[] = "$strField = '$strKey'";
		$this->_parseQuery();
		$strWhere = $this->_translateWhere();

		if(empty($strKey) && $intID = $this->_detectInsert($strTable, $arrData)) $this->_showResponse(true, [
			    'action' => $this->lang->jw('UI-API-CREATED'),
			    'id' => $intID,
			    'data' => $arrData,
			]);
		else if($intCount = $this->_Mtable->dbUpdate($arrData, $strWhere)) $this->_showResponse(true, [
			    'action' => $this->lang->jw('UI-API-UPDATED'),
			    'count' => $intCount,
			    'data' => $arrData,
			]);
	}

	$this->_showResponse(false);
}
public function item_put($strTable, $strKey = '', $strField = 'id') { $this->item_post($strTable, $strKey, $strField); }

public function item_delete($strTable, $strKey, $strField = 'id') {
	$this->initialize($strTable);
	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;
	$this->_arrWhere[] = "$strField = '$strKey'";

	$this->_parseQuery();
	$strWhere = $this->_translateWhere();

	if($intCount = $this->_Mtable->dbDelete($strWhere)) $this->_showResponse(true, [
		    'action' => $this->lang->jw('UI-API-DELETED'),
		    'count' => $intCount,
		]);
	$this->_showResponse(false);
}

private function _detectTable($strTable) {
	switch($strTable) {
		default:
			$this->_strTable = $strTable;
			$this->_strCustomQuery = '';
		break;
	}
}

private function _detectInsert($strTable, $arrData) {
	switch($strTable) {
		case 'cart':
			$intProductID = $arrData['cart_product_id'];
			$intMemberID = $arrData['cart_member_id'];
			$strSQL = "cart_member_id = $intMemberID AND cart_product_id = $intProductID";
			if(intval($this->_Mtable->dbCount($strSQL)) > 0) $this->_Mtable->dbUpdate($arrData, $strSQL);
			else $this->_Mtable->dbInsert($arrData);
			return TRUE;
		break; default:
			return $this->_Mtable->dbInsert($arrData);
		break;
	}
}

private function _parseQuery() {
	foreach($this->query() as $i => $e)
		if(is_array($e)) foreach($e as $f) $this->_doParseQuery($i, $f);
		else $this->_doParseQuery($i, $e);
}
private function _doParseQuery($i, $e) {
	if(strpos($i, 'sort_') === 0) {
		$strOrder = '';
		$i = str_replace(['sort_', '-'], ['', '.'], $i);

		if($this->_isFieldMustAppendPre($i)) $strOrder .= $this->_strPre.'_';
		$strOrder .= $i.' ';

		if(intval($e) < 0 || $e == 'desc') $strOrder .= 'DESC';
		else $strOrder .= 'ASC';
		$this->_arrOrder[] = $strOrder;
	} elseif(strpos($i, 'filter_') === 0) {
		$strWhere = '';
		$i = str_replace(['filter_', '-'], ['', '.'], $i);

		if($this->_isFieldMustAppendPre($i)) $strWhere .= $this->_strPre.'_';
		$strWhere .= $i.' ';

		if(strpos($e, ',') !== FALSE) $strWhere .= "IN ($e)";
		elseif(strpos($e, '*') !== FALSE) $strWhere .= str_replace('*', '%', "LIKE '$e'");
		elseif(strpos($e, 'lt-') === 0) $strWhere .= '< '.str_replace('lt-', '', $e);
		elseif(strpos($e, 'lte-') === 0) $strWhere .= '<= '.str_replace('lte-', '', $e);
		elseif(strpos($e, 'gt-') === 0) $strWhere .= '> '.str_replace('gt-', '', $e);
		elseif(strpos($e, 'gte-') === 0) $strWhere .= '>= '.str_replace('gte-', '', $e);
		else $strWhere .= " = '$e'";
		$this->_arrWhere[] = $strWhere;
	}
}

private function _translateWhere() {
	if(empty($this->_arrWhere) && $this->_strCustomQuery != '') $this->_arrWhere[] = 't.id > 0';
	return implode($this->_arrWhere, ' AND ');
}
private function _translateOrder() {
	if(empty($this->_arrOrder) && $this->_strCustomQuery != '') $this->_arrOrder[] = 't.id ASC';
	return implode($this->_arrOrder, ', ');
}

private function _isFieldMustAppendPre($strField) {
	if(!empty($this->_strPre) &&
		compareData($strField, $this->_arrFieldPreException, 'notin') &&
		$this->_strCustomQuery == '' &&
		strpos($strField, $this->_strPre) === FALSE) return TRUE;
	return FALSE;
}

private function _getTableRows() {
	$arrData = $this->_Mtable->getQueryResult('Array');
	if(!empty($arrData[0])) $this->_setFieldList($arrData[0]);

	foreach($arrData as $i => $e) {
		if(!empty($this->_strCustomQuery)) foreach($this->_arrFieldPreException as $f) if(isset($e[$f]))
			unset($e[$f]);

		foreach($e as $j => $f) if(!empty($this->_arrAdmFieldIndex[$j])) {
			$mixValue = '';
			$e[$j] = $this->_translateDataIntoReadableStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$j]], $f, $mixValue, TRUE);
			if(!empty($mixValue)) $e[$j.'_value'] = $mixValue;
		}

		$arrData[$i] = $e;
	}

	return $arrData;
}
private function _getTableRow() {
	$arrData = $this->_Mtable->getNextRecord('Array');
	$this->_setFieldList($arrData);
	foreach($arrData as $i => $e) if(!empty($this->_arrAdmFieldIndex[$i])) {
		$mixValue = '';
		$arrData[$i] = $this->_translateDataIntoReadableStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]], $e, $mixValue);
		if(!empty($mixValue)) $arrData[$i.'_value'] = $mixValue;
	}
	return $arrData;
}

private function _setFieldList($arrData) {
	if(!empty($arrData)) {
		$this->AdmFields->dbSelect('', "title IN ('".implode('\', \'', array_keys($arrData))."')");
		$this->_arrAdmFields = $this->AdmFields->getQueryResult('Array');
		$this->_arrAdmFieldIndex = [];
		foreach($this->_arrAdmFields as $i => $e)
			$this->_arrAdmFieldIndex[$e['title']] = $i;
	}
}

private function _translateDataIntoReadableStatements($arrField, $mixData, &$mixValue, $bolIsList = FALSE) {
	if(empty($mixData)) return $mixData;
	if($arrField['field_type'] == 'SELECT') {
		$mixValue = translateDataIntoReadableStatements($arrField, $mixData);
	} elseif($arrField['field_type'] == 'FILE' || ($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'FILE')) {
		$mixData = translateDataIntoReadableStatements(['field_type' => 'TEXT', 'validation' => 'URL'], $mixData);
		$mixData = explode('; ', $mixData);
	} elseif($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'HTMLEDITOR') {
		if(!$bolIsList) $mixValue = translateDataIntoReadableStatements($arrField, $mixData);
		$mixData = translateHTMLSyntaxIntoShortStatements($mixData);
	} elseif(
		($arrField['field_type'] == 'TEXTAREA' && ($arrField['validation'] == 'MULTIPLESELECT' || $arrField['validation'] == 'CHECKBOXLIST')) ||
		($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'LIST') ||
		($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'MULTIVALUE') ||
		($arrField['field_type'] == 'TEXT' && $arrField['validation'] == 'URL')
	) {
		$mixData = translateDataIntoReadableStatements($arrField, $mixData);
	}

	return $mixData;
}

}