<?php

class Dashboard_cc_ajax extends JW_Controller {

    public function getAllData($subkontrak_id){
        $this->load->model('Mdashboardcc');
        ArrayToXml(array('Keuangan' => $this->Mdashboardcc->getAllData($subkontrak_id)));
    }

    public function getAllDataByNamaBahan($id_bahan){
        $this->load->model('Mdashboardcc');
        ArrayToXml(array('KeuanganBahan' => $this->Mdashboardcc->getAllDataByNamaBahan($id_bahan)));
    }
}