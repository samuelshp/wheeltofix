<?php

class Autocomplete extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect('administrator');
	
	# Load the models
	$this->load->model('Mdbvo','admtablelist');
	$this->admtablelist->initialize('adm_table_list');
	$this->load->model('Mdbvo','admfieldlist');
	$this->admfieldlist->initialize('adm_field_list');
	$this->load->model('Mdbvo','tableloaded');

	$this->output->enable_profiler(FALSE);
}

public function index() { redirect(lastPageURL()); }

# Get all data in a field
public function getData($intFieldID) {
	# Identify the field
	$this->admfieldlist->search($intFieldID);
	$arrFieldList = $this->admfieldlist->getNextRecord('Array');
	# Identify the table
	$this->admtablelist->search($arrFieldList['table_id']);
	$arrTableList = $this->admtablelist->getNextRecord('Array');

	# Load the table
	$this->tableloaded->initialize($arrTableList['title']);
	$this->tableloaded->dbSelect('id AS strKey,'.$arrFieldList['title'].' AS strData',
        $arrFieldList['title']." LIKE '%".$this->input->get('strKey')."%'");
	$arrTableData = $this->tableloaded->getQueryResult('Array');
    
	$this->load->helper('jwexport');
    ArrayToXml($arrTableData);
}

# Get all id and data in a field
public function getIDData($intFieldID) {
	# Identify the field
	$this->admfieldlist->search($intFieldID);
	$arrFieldList = $this->admfieldlist->getNextRecord('Array');
	# Identify the table
	$this->admtablelist->search($arrFieldList['table_id']);
	$arrTableList = $this->admtablelist->getNextRecord('Array');

	# Load the table
	$this->tableloaded->initialize($arrTableList['title']);
	$this->tableloaded->dbSelect('id,'.$arrFieldList['title']);
	$arrTableData = $this->tableloaded->getQueryResult('Array');
	
	# Display the data
	foreach($arrTableData as $e) echo $e['id'].'|'.$e[$arrFieldList['title']].'\n';
}

}

/* End of File */