<?php

class Report extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect('administrator');
}

# To display, edit and delete invoice
public function invoice($intInvoiceID = '') {
	
	# Load the invoice
	$this->load->model('Mdbvo','invoice');
	$this->invoice->initialize('invoice');
	
	$this->invoice->dbSelect("","id = '$intInvoiceID'");
	$arrInvoiceData = $this->invoice->getNextRecord('Array');
	
	if($this->input->post('subSave') != '' && $intInvoiceID != '') {
		if($this->input->post('selStatus') > 1) {
			$this->load->library('email');
			$arrOwnerInfo = $this->_arrData['arrOwnerInfo'];

			$strSubject = '['.$this->config->item("jw_website_name").'] Update status of your invoice';
			$strMessage = $this->load->view('mail',array(
				'strViewFile' => 'adm/invoiceupdate',
				'strServerName' => $_SERVER['SERVER_NAME'], 
				'intInvoiceID' => $intInvoiceID,
                'strOwnerEmail' => $arrOwnerInfo['strOwnerEmail']
			),TRUE);
			
			$this->email->sendEmail($arrOwnerInfo,$$arrInvoiceData['invo_member_email'],$strSubject,$strMessage); # Send email to member
		}

		$this->invoice->dbUpdate(array(
			'invo_delivery' => $this->input->post('txtDeliveryStatus'),
			'invo_status' => $this->input->post('selStatus')),
			"id = '$intInvoiceID'");
	} else if($this->input->post('subDelete') != '' && $intInvoiceID != '') {
		$this->invoice->initialize('invoice_item');
		$this->invoice->dbDelete("invi_invoice_id = '$intInvoiceID'");
		$this->invoice->initialize('invoice');
		$this->invoice->dbDelete("id = '$intInvoiceID'");
		$intInvoiceID = '';
	}
	
	if($intInvoiceID == '') $this->invoice->dbSelect('',"",'invo_date DESC',1);
	else $this->invoice->dbSelect('',"id = '$intInvoiceID'");
	if($this->invoice->getNumRows() > 0) {
		$arrInvoiceData = $this->invoice->getNextRecord('Array');
		$arrInvoiceData['invo_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 1, 'validation' => ''),$arrInvoiceData['invo_status']);
		$intInvoiceID = $arrInvoiceData['id'];
		
		# Load all other invoice data the user has ever made
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		else $intPage = 0;
		
		$this->invoice->dbSelect();
		$arrPagination['base_url'] = site_url("member/invoice/$intInvoiceID?pagination=true");
		$arrPagination['total_rows'] = $this->invoice->getNumRows();
		$arrPagination['per_page'] = ($this->config->item('jw_item_count') < 25)? 25 : $this->config->item('jw_item_count');
		$arrPagination['page_query_string'] = TRUE;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();
		
		$this->invoice->dbSelect('id,invo_member_name,invo_status',"",'invo_date DESC',$arrPagination['per_page'],$intPage);
		$arrInvoiceList = $this->invoice->getQueryResult('Array');
		
		# Load the invoice item
		$this->invoice->initialize('invoice_item');
		$this->invoice->dbSelect('',"invi_invoice_id = '$intInvoiceID'");
		$arrInvoiceItem = $this->invoice->getQueryResult('Array');
		$intInvoiceTotal = 0;
		for($i = 0; $i < count($arrInvoiceItem); $i++) {
			$arrInvoiceItem[$i]['invi_subtotal'] = setPrice($arrInvoiceItem[$i]['invi_price'] * 
				$arrInvoiceItem[$i]['invi_quantity'],'USED');
			$intInvoiceTotal += $arrInvoiceItem[$i]['invi_price'] * $arrInvoiceItem[$i]['invi_quantity'];
			$arrInvoiceItem[$i]['invi_price'] = setPrice($arrInvoiceItem[$i]['invi_price'],'USED');
		}
		
		$intInvoiceTotal = setPrice($intInvoiceTotal,'USED');
	} else {
		$intInvoiceID = 0; $arrInvoiceData = array(); $arrInvoiceItem = array(); $arrInvoiceList = array(); $intInvoiceTotal = 0; $strPage = '';
	}
	
	$this->load->view('adminpage',array(
		'strViewFile' => 'report/invoice',
		'intHelpLinkID' => 71,
		'intInvoiceID' => $intInvoiceID,
		'arrInvoiceData' => $arrInvoiceData,
		'arrInvoiceItem' => $arrInvoiceItem,
		'arrInvoiceList' => $arrInvoiceList,
		'intInvoiceTotal' => $intInvoiceTotal,
		'strPage' => $strPage,
		'strPageTitle' => 'Invoice'
	));
}

}

/* End of File */