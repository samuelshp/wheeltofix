<?php

class Table extends JW_Controller {

# Constructor
public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect('administrator');
	
	# Set the data that will be passed to the view
	$this->_arrData['strPath'] = '<b>:: ADMINISTRATOR</b>';
}

# Default fuction
public function index() { redirect(); }

# Browse the table
public function browse($intTableID = 0) {
    $arrTableList = array(); $arrFieldList = array();
    $this->_initializeBrowse($intTableID,$arrTableList,$arrFieldList);

	$arrURLQuery = $this->input->get();
	$arrResultURLQuery = array();
	$strWhere = '';
	foreach($arrURLQuery as $i => $e) if(strpos($i, 'filter_') === 0 && $e != '') {
		$arrResultURLQuery[$i] = $e;
		if(!empty($strWhere)) $strWhere .= ' AND ';
		$strWhere .= str_replace('filter_', '', $i)." LIKE '%$e%'";
	} elseif(strpos($i, 'search_') === 0 && $e != '') {
		$arrResultURLQuery[$i] = $e;
		if(!empty($strWhere)) $strWhere .= ' AND ';
		$strWhere .= $this->table->buildSearchQuery($e, str_replace('search_', '', $i));
	} elseif(strpos($i, 'order_') === FALSE && strpos($i, 'page') === FALSE) {
		$arrResultURLQuery[$i] = $e;
	}

	$strURLQuery = '';
	foreach($arrResultURLQuery as $i => $e) {
		if(!empty($strURLQuery)) $strURLQuery .= '&';
		$strURLQuery .= $i.'='.$e;
	}
	if(!empty($strURLQuery)) $strURLQuery .= '&';

	# Pagination
	if($this->input->get('order_by') != '') $strOrderBy = $this->input->get('order_by');
	else {
		$strOrderBy = 'id';
		# Sort by _date if available
		$arrColumnData = $this->table->getColumnData();
		foreach($arrColumnData as $e) if($e['Field'] == $arrTableList['pre'].'_date') {
			$strOrderBy = $arrTableList['pre'].'_date'; break;
		}
	}

	if($this->input->get('order_mode') == 'DESC') { $strOrderMode = 'DESC'; $strReverseOrderMode = 'ASC'; }
	else if($this->input->get('order_mode') == 'ASC') { $strOrderMode = 'ASC'; $strReverseOrderMode = 'DESC'; }
	else { $strOrderMode = 'DESC'; $strReverseOrderMode = 'ASC'; }

	if($this->input->get('page') != '') $intStartNo = $this->input->get('page');
	else $intStartNo = 0;

	if($this->input->get('per_page') != '') $intPerPage = $this->input->get('per_page');
	else if($this->config->item('jw_item_count') < 25) $intPerPage = 25;
	else $this->config->item('jw_item_count');

	$arrPagination['base_url'] = site_url("table/browse/$intTableID?".$strURLQuery."per_page=$intPerPage&order_by=$strOrderBy&order_mode=$strOrderMode");

	# Generate the records in the table
	# Retrieve all data that match the parameters
	// 'id,'.$arrTableList['display_fields']
	$arrPagination['per_page'] = $intPerPage;
	if($this->input->get('subExcel') == 'Excel')
		$arrTableData = $this->table->getData($strWhere,$strOrderBy.' '.$strOrderMode,'','',true);
	else $arrTableData = $this->table->getData($strWhere,$strOrderBy.' '.$strOrderMode,$arrPagination['per_page'],$intStartNo,true);
	$arrPagination['total_rows'] = $this->table->getNumRows($strWhere);

	# Change data into readable type
	if(!empty($arrTableData)) for($i = 0; $i < count($arrTableData); $i++)
		$arrTableData[$i] = $this->table->getReadableFields($arrTableData[$i]);

	# Pagination
	$arrPagination['page_query_string'] = TRUE;
	$this->pagination->initialize($arrPagination);
	$strPage = $this->pagination->create_links();

	# Save the uri to redirect after an action done
    lastPageURL($_SERVER['QUERY_STRING'] ?
    	$this->uri->uri_string().'?'.$_SERVER['QUERY_STRING'] :
    	$this->uri->uri_string()
	);

	# Load the views
	$this->load->view('adminpage',array(
		'strViewFile' => 'table/browse',
		'arrTableData' => $arrTableData,
		'arrTableListData' => $arrTableList,
		'arrFieldList' => $arrFieldList,
		'strPage' => $strPage,
		'arrPageParams' => array(
			'intTotal' => $arrPagination['total_rows'],
			'intStart' => $intStartNo,
			'intCount' => count($arrTableData)),
		'strURLQuery' => $strURLQuery,
		'arrResultURLQuery' => $arrResultURLQuery,
		'intPerPage' => $intPerPage,
		'strOrderBy' => $strOrderBy,
		'strOrderMode' => $strOrderMode,
		'strBrowseMode' => 'Browse',
		'strPath' => $this->_arrData['strPath'].' &gt; '.$arrTableList['name'].' &gt; Browse',
		'strPageTitle' => $arrTableList['name']
	));
}

# Function portal: after admin clicked post button, this function is called and distribute it to the right handler
public function dataoperation($intTableID) {
	if($this->input->post('smtProcessType') == 'Browse' || $this->input->post('smtProcessType') == 'Back') {
		redirect(lastPageURL());
	}

    if($this->input->post('smtProcessType') != 'Publish') {
        $arrPost = $this->input->post();
        $arrPostKey = preg_grep("/^cbRecord\d+$/", array_keys($arrPost));
        $arrProcessedDataID = array();
        foreach($arrPostKey as $e) if(isset($arrPost[$e])) $arrProcessedDataID[] = $arrPost[$e]; 
        
    } else $arrProcessedDataID = array();
    
    $this->_initializeDataOperation($intTableID,$this->input->post('smtProcessType'),$arrProcessedDataID);
	
	# Redirect to the right function to handle the action request
	switch($this->input->post('smtProcessType')) {
		case('View'): $this->_view(); break;
		case('Add'): $this->_add(); break;	
		case('Edit'): $this->_edit(); break;
		case('Delete'): $this->_delete(); break;
		case('Publish'): $this->_publish(); break;
		case('Save'): $this->_save(); break;
		default: redirect(lastPageURL()); break;
	}
}

public function view($intTableID,$intDataID) {
    $this->_initializeDataOperation($intTableID,'View',array($intDataID));
    $this->_view();
}

public function add($intTableID) {
    $this->_initializeDataOperation($intTableID,'Add');
    $this->_add();
}

public function edit($intTableID,$intDataID) {
    // lastPageURL('table/view/'.$intTableID.'/'.$intDataID);
    $this->_initializeDataOperation($intTableID,'Edit',array($intDataID));
    $this->_edit();
}

# PRIVATE FUNCTIONS

private function _initializeBrowse($intTableID,&$arrTableList,&$arrFieldList) {
    $this->_getMenuHelpContent($intTableID,false,'',$this->session->userdata('strAdminPriviledge'));
    
    # Load the table
	$this->load->model('Madmlinklist');
	$this->load->library('table');
	$this->table->initialize($intTableID);
	$arrTableList = $this->table->getTableProperties();
	$this->table->initialize($intTableID,explode(',',$arrTableList['display_fields']));
	$arrFieldList = $this->table->getFieldProperties();
    
    # Add the privilege data
	$arrTableList['allow_insert'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),1);
	$arrTableList['allow_update'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),2);
	$arrTableList['allow_delete'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),3);
} 

private function _initializeDataOperation($intTableID,$strProcessType,$arrProcessedDataID = array()) {
    $this->_getMenuHelpContent($intTableID);
    
	$this->load->model('Madmlinklist');
    if(!$this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'))) { # Privilege
		$this->session->set_flashdata('strMessage',$this->lang->jw('no_privilege'));
		redirect();
	}
    
    # Load the table properties
	$this->load->library('table');
	$this->table->initialize($intTableID);
	$arrTableList = $this->table->getTableProperties();
    
    # Add the privilege data
	$arrTableList['allow_insert'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),1);
	$arrTableList['allow_update'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),2);
	$arrTableList['allow_delete'] = $this->Madmlinklist->getAvailability($this->_arrData['intHelpLinkID'],$this->session->userdata('strAdminPriviledge'),3);
    
    # Load the table (except add, it don't need to load any table)
    $strProcessedDataID = ''; $arrTableData = array();
    
	if($strProcessType != 'Add') {
		# Detect which data must be processed and fetch the data (except publish, it don't need to fetch any data)
		if($strProcessType != 'Publish') {
            $strProcessedDataID = implode(',',$arrProcessedDataID);
            
			# Edit and View, Retrieve all data that match the parameters
			if($strProcessType == 'Edit' || $strProcessType == 'View') {
				# Hacked because the library auto detect getNextRecord or getQueryResult
				if($this->table->getNumRows("id IN ($strProcessedDataID)") == 1) 
					$arrTableData[0] = $this->table->getData("id IN ($strProcessedDataID)");
				else $arrTableData = $this->table->getData("id IN ($strProcessedDataID)");
				
			} else $arrTableData = array();
				
		}
	} 
	
	# Generate the fields
	# Except delete, because it doesn't need to display anything
	if($strProcessType != 'Delete') {
		$arrFieldList = $this->table->getFieldProperties();
		$this->_arrData['arrFieldList'] = $arrFieldList;
	}
    
    $this->_arrData = array_merge($this->_arrData, array(
        'intTableID' => $intTableID,
        'arrTableListData' => $arrTableList,
        'strTableName' => $arrTableList['name'],
        'strProcessedDataID' => $strProcessedDataID,
        'arrProcessedDataID' => $arrProcessedDataID,
        'arrTableData' => $arrTableData
    ));
}

# Display the add form
private function _add() {
	# Load the views
	$this->load->view('adminpage',array(
		'strViewFile' => 'table/add',
		'arrTableData' => $this->table->getHTMLFields(), # Change all field into HTML statements
		'strPath' => $this->_arrData['strPath'].' &gt; '.$this->_arrData['strTableName'].' &gt; Add',
        'strPageTitle' => $this->_arrData['strTableName'].' Add'
	));
}

# Handle viewing a record detail
private function _view() {	
	# Change the data into readable form
	$arrTableData = $this->_arrData['arrTableData'];
	for($i = 0; $i < count($arrTableData); $i++)
		$arrTableData[$i] = $this->table->getReadableFields($arrTableData[$i]);

	# Get Link To Other Data
	$arrTableList = $this->table->getTableProperties();
	$this->load->model('Mdbvo', 'admFieldList');
	$this->admFieldList->setQuery("
SELECT tl.id AS table_id, tl.name AS table_name, fl.name AS field_name, fl.title AS filter_name, '".$arrTableList['id']."' AS filter_key
FROM adm_field_list AS fl
LEFT JOIN adm_table_list AS tl ON fl.table_id = tl.id
WHERE fl.field_type = 'SELECT' AND fl.description = ".$arrTableList['id']);
	$arrDependantList = $this->admFieldList->getQueryResult('Array');

	# Load the views
	$this->load->view('adminpage',array(
		'strViewFile' => 'table/view',
		'arrTableData' => $arrTableData,
		'arrDependantList' => $arrDependantList,
		'strPath' => $this->_arrData['strPath'].' &gt; '.$this->_arrData['strTableName'].' &gt; View',
        'strPageTitle' => $this->_arrData['strTableName'].' View'
	));
}

# Display the edit form
private function _edit() {
	# Change all field into form view
	$arrTableData = $this->_arrData['arrTableData'];
	$arrConvertedTableData = $arrTableData;
	for($i = 0; $i < count($arrTableData); $i++)
		$arrConvertedTableData[$i] = $this->table->getHTMLFields($arrTableData[$i],$i);
	
	# Load the views
	$this->load->view('adminpage',array(
		'strViewFile' => 'table/edit',
		'arrTableData' => $arrConvertedTableData,
		'strPath' => $this->_arrData['strPath'].' &gt; '.$this->_arrData['strTableName'].' &gt; Edit',
        'strPageTitle' => $this->_arrData['strTableName'].' Edit'
	));
}

# Process delete action
private function _delete() {
	# Delete all data that match the parameters
	$this->table->delete(explode(',',$this->_arrData['strProcessedDataID']));
	
	redirect(lastPageURL());
}

# Process add action
private function _publish() {
	# Collect the POST data into an array for insertion
	$this->table->publish($this->input->post());
	
	# After finish, redirect it back to the last page
	redirect(lastPageURL());
}

# Process edit action
private function _save() {
	# Collect the POST data into an array for update
	$this->table->save($this->_arrData['arrProcessedDataID'],$this->input->post(),count($this->_arrData['arrProcessedDataID']));
	
	# After finish, redirect it back to the last page
	redirect(lastPageURL());
}

}

/* End of File */