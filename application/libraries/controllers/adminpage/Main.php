<?php

class Main extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect('administrator');
}

public function index() { $this->home(); }

public function home() {
	# Generate the alert
	$this->load->model('Madmlinklist');
	$this->load->model('Mdbvo','table');
	$this->load->model('Mdbvo','tableList');
	$this->tableList->initialize('adm_table_list');
	$arrAlert = array();
	$arrLinkList = $this->Madmlinklist->getAlert();
	for($i = 0; $i < count($arrLinkList); $i++) {
		if($arrLinkList[$i]['link_type'] == 'table') {
			# Get Table Info
			$this->tableList->dbSelect('title,pre',"id = '".$arrLinkList[$i]['link_description']."'");
			$arrTableList = $this->tableList->getNextRecord('Array');
			# Get count
			$this->table->initialize($arrTableList['title']);
			$this->table->dbSelect('count(id) AS number',$arrTableList['pre']."_status = '0'");
            
            $intCount = $this->table->getNextRecord('Object')->number;
            if($intCount > 0) {
    			$arrAlert[$i]['count'] = $intCount;
    			$arrAlert[$i]['link'] = 'table/browse/'.$arrLinkList[$i]['link_description'];
                $arrAlert[$i]['name'] = $arrLinkList[$i]['name'];
            }
		} else if($arrLinkList[$i]['link_type'] == 'page') {
			$arrAlert[$i]['count'] = '0';
			$arrAlert[$i]['link'] = $arrLinkList[$i]['link_description'];
            $arrAlert[$i]['name'] = $arrLinkList[$i]['name'];
		}
	}
	
	# Load the views
	$this->load->view('adminpage',array(
		'strViewFile' => 'main/home',
		'arrAlert' => $arrAlert,
		'strPageTitle' => $this->lang->jw('dashboard')
	));
}

public function mass_mail($intMailID = '') {
	# Get all availability
	$this->load->model('Madmlinklist','feature');
	
	$this->load->model('Mdbvo','mail');
	$this->mail->initialize('mail_template');

	# Get all username list
	$this->mail->setQuery("SELECT * FROM jw_member_newsletter WHERE mnws_status > 1");
	$arrMemberList = $this->mail->getQueryResult('Array');
	
	if($this->input->post('subSend') != '' || $this->input->post('subSave') != '') {
		$strMode = $this->input->post('hidMode');
		$strSubject = $this->input->post('txtSubject');
		$strContent = $this->input->post('txaContent');
		
		# Save mail
		if($strMode == 'Add') {
			$this->mail->dbInsert(array(
				'mail_title' => $strSubject,
				'mail_content' => $strContent));
			
			$intMailID = $this->mail->getInsertID();
		} else if($strMode == 'Edit') {
			$this->mail->dbUpdate(array(
				'mail_title' => $strSubject,
				'mail_content' => $strContent),"id = '$intMailID'");
		}
		
		# Send mail
		if($this->input->post('subSend') != '') {
			$this->load->library('email');
			$arrOwnerInfo = $this->_getOwnerInfo();
			$arrTo = explode(', ',$this->input->post('txaTo'));
			$bolSuccess = TRUE;
			foreach($arrTo as $strTo) {
				$arrNewsletterData = searchInArray($arrMemberList,'mnws_email',$strTo);
				if(!empty($arrNewsletterData['mnws_name'])) $strName = $arrNewsletterData['mnws_name'];
				else $strName = '';
				$strMessage = $this->load->view('mail',array(
					'strViewFile' => 'adm/newsletter',
					'strTitle' => $strSubject,
					'strEmail' => $strTo,
					'strContent' => str_replace(
						array('{name}','{email}'),
						array($strName,$strTo),
						$strContent
					)
				),TRUE);
				
				if(!$this->email->sendEmail($arrOwnerInfo,$strTo,$strSubject,$strMessage)) $bolSuccess = FALSE;
			}

			if($bolSuccess) $this->session->set_flashdata('strMessage',$this->lang->jw('sent_mail'));
			else $this->session->set_flashdata('strMessage',$this->lang->jw('sent_mail_failed'));
		}
		
	} else if($this->input->post('subDelete') != '') {
		$this->mail->dbDelete("id = '$intMailID'");
		$intMailID = '';
	} 

	# Get all mail list
	$this->mail->dbSelect('id,mail_title');
	$arrMailList = $this->mail->getQueryResult('Array');
	
	if($intMailID != '') { # Edit mode
		$this->mail->dbSelect('',"id = '$intMailID'");
		$arrMailContent = $this->mail->getNextRecord('Array');
		$strMode = 'Edit';
	} else { # Add new mode
		$arrMailContent = array(); $strMode = 'Add';
	}
	
	$this->load->view('adminpage',array(
		'strViewFile' => 'main/massmail',
		'strMode' => $strMode,
		'intHelpLinkID' => 55,
		'intMailID' => $intMailID,
		'arrMailList' => $arrMailList,
		'arrMemberList' => $arrMemberList,
		'arrMailContent' => $arrMailContent,
		'strPageTitle' => $this->lang->jw('MAIL-send_mass')
	));
}

# Only display live chat widget
public function chat() {
	$this->load->view('adminpage',array(
		'strViewFile' => 'main/chat'
	));
}

public function change_language($strLanguage) {
	$this->session->set_userdata('jw_admin_language',$strLanguage);
	$this->session->set_flashdata('strMessage',$this->lang->jw('active_language',$strLanguage));
	redirect(str_ireplace(site_url().'/','',$_SERVER['HTTP_REFERER']));
}

public function clear_cache() {
	$this->cache->{JW_CACHE_TYPE}->clean();
	$this->session->set_flashdata('strMessage','Cache cleared');
	redirect(str_ireplace(site_url().'/','',$_SERVER['HTTP_REFERER']));
}

}

/* End of File */