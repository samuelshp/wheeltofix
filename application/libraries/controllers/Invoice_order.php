<?php
/*
PUBLIC FUNCTION:
- index()
- cashier()
- browse(intPage)
- view(intInvoiceID)

PRIVATE FUNCTION:
- __construct()
*/

class Invoice_order extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Minvoiceorder');
	$this->load->model('Minvoiceorderitem');

    $this->_getMenuHelpContent(42,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtUpdateInvoice') != '' || $this->input->post('smtMakeInvoice') != '') { 
	}

	if($this->input->post('smtMakeInvoice') != '') { // Make Invoice

        $this->load->model('Munit');
        //$days=$this->input->post('txtExpDate');
        //$date=$this->input->post('txtDate');
        //$date = strtotime("+".$days." days", strtotime($date));
        /*$jatuhtempo = date("Y-m-d", $date);
        $jatuhtempo=str_replace("-","/",$jatuhtempo);*/
        $arrInvoice = array(
            'intOutletID' => $this->input->post('outletID'),
            'intJualKanvas' => $this->input->post('intJualKanvas'),
            'intCustID' => $this->input->post('customerID'),
            'intSalesmanID' => $this->input->post('salesmanID'),
            'intSuppID' => $this->input->post('supplierID'),
            'intWarehouse' => $this->input->post('WarehouseID'),
            'strDescription' => $this->input->post('txaDescription'),
            'intTax' => $this->input->post('txtTax'),
            'strDate' => $this->input->post('txtDate'),
            'strExpDate' => $this->input->post('txtExpDate'),
            'intTipeBayar' => $this->input->post('intTipeBayar'),/*
            'strJatuhTempo' => $jatuhtempo,*/
            'intDisc' => $this->input->post('dsc4')
        );

        $intInvoiceID = $this->Minvoiceorder->add($arrInvoice['intOutletID'],$arrInvoice['intCustID'],$arrInvoice['intSalesmanID'],$arrInvoice['intSuppID'],$arrInvoice['intWarehouse'],$arrInvoice['strDescription'],$arrInvoice['intTax'],2,$arrInvoice['strDate'],$arrInvoice['intDisc'],$arrInvoice['intTipeBayar'],$arrInvoice['strExpDate'],$arrInvoice['intJualKanvas']);

        $totalItem = $this->input->post('totalItem');
        for ($i = 0; $i < $totalItem; $i++) {
            $qty1Item = $this->input->post('qty1PriceEffect'.$i);
            $qty2Item = $this->input->post('qty2PriceEffect'.$i);
            $qty3Item = $this->input->post('qty3PriceEffect'.$i);
            $unit1Item = $this->input->post('sel1UnitID'.$i);
            $unit2Item = $this->input->post('sel2UnitID'.$i);
            $unit3Item = $this->input->post('sel3UnitID'.$i);

            $prcItem = $this->input->post('prcPriceEffect'.$i);

            $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
            $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
            $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);

            $prodItem = $this->input->post('prodItem'.$i);
            $probItem = $this->input->post('probItem'.$i);
            
            $strProductDescription = formatProductName('',$prodItem,$probItem);
            $idItem = $this->input->post('idItem'.$i);
            
            if($strProductDescription != '' || !empty($strProductDescription)) {
                $this->Minvoiceorderitem->add($intInvoiceID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
            }
        }
        $totalItemBonus = $this->input->post('totalItemBonus');
        for ($i = 0; $i < $totalItemBonus; $i++) {
            $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
            $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
            $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
            $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
            $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
            $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
            $prodItemBonus = $this->input->post('prodItemBonus'.$i);
            $probItemBonus = $this->input->post('probItemBonus'.$i);
            $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
            $idItemBonus = $this->input->post('idItemBonus'.$i);
            if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
                $this->Minvoiceorderitem->add($intInvoiceID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
            }
        }
        $arrPotBrand = $this->input->post('potonganBrand');
        $arrPotName = $this->input->post('potonganName');
        $arrPotDisc = $this->input->post('potonganDisc');
        $arrPotQty = $this->input->post('potonganQty');
        if(!empty($arrPotBrand)){
            $this->load->model('Mpotonganorder');
            foreach($arrPotBrand as $key => $e){
                $this->Mpotonganorder->add($intInvoiceID,$arrPotQty[$key],$arrPotName[$key],$arrPotDisc[$key]);
            }
        }
		
        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoiceorder-invoiceordermade'));
        redirect('invoice_order/view/'.$intInvoiceID);
	}

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'invoiceorder/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoiceorder-addinvoiceorder')
    ));
}

// To display, edit and delete invoice
public function view($intID = '') {
	# INIT
	$this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwdefaultconfig');

	if($this->input->post('subSave') != '' && $intID != '') {
		// Invoice
        $arrInvoiceData = $this->Minvoiceorder->getItemByID($intID);
        if(compareData($arrInvoiceData['inor_status'],array(0))) {
            $this->Minvoiceorder->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('dsc4'),$this->input->post('selStatus'),$this->input->post('txtTax'));
            // Load purchase item
            $arrPurchaseItem = $this->Minvoiceorderitem->getItemsByInvoiceID($intID);
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');

            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
            foreach($arrPurchaseItem as $e) {
                // Search return quantity
                if(!empty($arrContainID)) {
                    if(in_array($e['id'], $arrContainID)) {
                        $this->Minvoiceorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);

                    } else $this->Minvoiceorderitem->deleteByID($e['id']);

                } else $this->Minvoiceorderitem->deleteByID($e['id']);
                
            }
            $arrPurchaseItem = $this->Minvoiceorderitem->getBonusItemsByInvoiceID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');

            $arrContainID = $this->input->post('idProiIDBonus');
            if(!empty($arrPurchaseItem)) {
                foreach($arrPurchaseItem as $e) {
                    // Search return quantity
					
                    if(!empty($arrContainID)) {
                        if(in_array($e['id'], $arrContainID)) {
                            $this->Minvoiceorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
                        
						} else $this->Minvoiceorderitem->deleteByID($e['id']);
                        
                    } else $this->Minvoiceorderitem->deleteByID($e['id']);
                    
                }
            }
			
            $totalItem = $this->input->post('totalItem');
            for($i = 0; $i < $totalItem; $i++) {
                $qty1Item = $this->input->post('qty1PriceEffect'.$i);
                $qty2Item = $this->input->post('qty2PriceEffect'.$i);
                $qty3Item = $this->input->post('qty3PriceEffect'.$i);
                $unit1Item = $this->input->post('sel1UnitID'.$i);
                $unit2Item = $this->input->post('sel2UnitID'.$i);
                $unit3Item = $this->input->post('sel3UnitID'.$i);
                $prcItem = $this->input->post('prcPriceEffect'.$i);
                $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
                $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
                $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
                $prodItem = $this->input->post('prodItem'.$i);
                $probItem = $this->input->post('probItem'.$i);
                $strProductDescription = formatProductName('',$prodItem,$probItem);
                $idItem = $this->input->post('idItem'.$i);
                if($strProductDescription != '' || !empty($strProductDescription)) {
                    $this->Minvoiceorderitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
                }
            }
            $totalItemBonus = $this->input->post('totalItemBonus');
            for($i = 0; $i < $totalItemBonus; $i++) {
                $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
                $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
                $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
                $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
                $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
                $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
                $prodItemBonus = $this->input->post('prodItemBonus'.$i);
                $probItemBonus = $this->input->post('probItemBonus'.$i);
                $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
                $idItemBonus = $this->input->post('idItemBonus'.$i);
                if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
                    $this->Minvoiceorderitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
                }
            }
            $intChange = $this->input->post('change');
            if($intChange == '1'){
                $arrPotBrand = $this->input->post('potonganBrand');
                $arrPotName = $this->input->post('potonganName');
                $arrPotDisc = $this->input->post('potonganDisc');
                $arrPotQty = $this->input->post('potonganQty');
                if(!empty($arrPotBrand)){
                    $this->load->model('Mpotongan');
                    $this->Mpotongan->deleteByInvoiceID($intID);
                    foreach($arrPotBrand as $key => $e){
                        $this->Mpotongan->add($intID,$arrPotQty[$key],$arrPotName[$key],$arrPotDisc[$key]);
                    }
                }
            }

        } else $this->Minvoiceorder->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));


        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoiceorder-invoiceorderupdated');

	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
		$this->Minvoiceorderitem->deleteByInvoiceID($intID);
		$this->Minvoiceorder->deleteByID($intID);
		
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoiceorder-invoiceorderdeleted'));
		redirect('invoice_order/browse');
	}
	
	$arrInvoiceData = $this->Minvoiceorder->getItemByID($intID);
	if(!empty($arrInvoiceData)) {
		$arrInvoiceData['inor_rawstatus'] = $arrInvoiceData['inor_status'];
		$arrInvoiceData['inor_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrInvoiceData['inor_status']);
		$intID = $arrInvoiceData['id'];
        $temp=explode(' ',$arrInvoiceData['inor_date']);
        $arrInvoiceData['date']=$temp[0];
		
		// Load the invoice item
        $arrInvoiceItem = $this->Minvoiceorderitem->getItemsByInvoiceID($intID);
        $intInvoiceTotal = 0;
        $tambahan = array();
        $countertambahan = 0;
        $this->load->model('Munit');
        for($i = 0; $i < count($arrInvoiceItem); $i++) {
            $arrInvoiceItem[$i]['strName'] = $arrInvoiceItem[$i]['inoi_description'].'|'.$arrInvoiceItem[$i]['proc_title'];
            $intInvoiceTotal+=$arrInvoiceItem[$i]['inoi_subtotal'];
            $intQty1 = $arrInvoiceItem[$i]['inoi_quantity1'];
            $intQty2 = $arrInvoiceItem[$i]['inoi_quantity2'];
            $intQty3 = $arrInvoiceItem[$i]['inoi_quantity3'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['inoi_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['inoi_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['inoi_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $arrInvoiceItem[$i]['inoi_conv1'] = $intConv1;
            $arrInvoiceItem[$i]['inoi_conv2'] = $intConv2;
            $arrInvoiceItem[$i]['inoi_conv3'] = $intConv3;
            if(!empty($tambahan)) {
                $flag = 0;
                for($j = 0; $j < count($tambahan); $j++) {
                    if($tambahan[$j]['id'] == $arrInvoiceItem[$i]['inoi_product_id']) {
                        $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $flag = 1;
                        break;
                    }
                }
                if($flag == 0) {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['inoi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            } else {
                $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['inoi_product_id'];
                $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                $countertambahan++;
            }
        }
        $pengurangan = 0;
        $arrKreditCustomer = $this->Minvoiceorder->getCustomerCredit($arrInvoiceData['cust_id']);
        for($ii = 0; $ii < count($arrKreditCustomer); $ii++) {
            $pengurangan += (float) $arrKreditCustomer[$ii]['inor_grandtotal'];
        }
        if(is_numeric($pengurangan) && $pengurangan>0) {
            $arrInvoiceData['cust_limitkredit'] = (float)$arrInvoiceData['cust_limitkredit']-(float)$pengurangan+(float)$arrInvoiceData['inor_grandtotal'];
        }
        $arrInvoiceData['subtotal_discounted'] =  (float) $arrInvoiceData['inor_grandtotal'];
        $intInvoiceGrandTotal = (float) $arrInvoiceData['subtotal_discounted'] + ($arrInvoiceData['subtotal_discounted'] * (float) $arrInvoiceData['inor_tax'] / 100);
        $arrInvoiceItemBonus = $this->Minvoiceorderitem->getBonusItemsByInvoiceID($intID);
        if(!empty($arrInvoiceItemBonus)) {
            for($i = 0; $i < count($arrInvoiceItemBonus); $i++) {
                $arrInvoiceItemBonus[$i]['strName'] = $arrInvoiceItemBonus[$i]['inoi_description'].'|'.$arrInvoiceItemBonus[$i]['proc_title'];
                $intQty1 = $arrInvoiceItemBonus[$i]['inoi_quantity1'];
                $intQty2 = $arrInvoiceItemBonus[$i]['inoi_quantity2'];
                $intQty3 = $arrInvoiceItemBonus[$i]['inoi_quantity3'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['inoi_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['inoi_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['inoi_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrInvoiceItemBonus[$i]['inoi_conv1'] = $intConv1;
                $arrInvoiceItemBonus[$i]['inoi_conv2'] = $intConv2;
                $arrInvoiceItemBonus[$i]['inoi_conv3'] = $intConv3;
                if(!empty($tambahan)) {
                    $flag = 0;
                    for($j = 0; $j < count($tambahan); $j++) {
                        if($tambahan[$j]['id'] == $arrInvoiceItemBonus[$i]['inoi_product_id']) {
                            $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                            $flag = 1;
                            break;
                        }
                    }
                    if($flag == 0) {
                        $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['inoi_product_id'];
                        $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $countertambahan++;
                    }
                } else {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['inoi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            }
        }
        $tambahanId = '';
        $tambahanQty = '';
        for($i = 0;$i < count($tambahan); $i++) {
            $tambahanId = $tambahanId.$tambahan[$i]['id'].'-';
            $tambahanQty = $tambahanQty.$tambahan[$i]['qty'].'-';
            for($j = 0; $j < count($arrInvoiceItem); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItem[$j]['inoi_product_id']) {
                    $stock = countStock($arrInvoiceItem[$j]['inoi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                    $arrInvoiceItem[$j]['inoi_max1'] = (int) ($stock / $arrInvoiceItem[$j]['inoi_conv1']);
                    $stock = $stock - ($arrInvoiceItem[$j]['inoi_max1'] * $arrInvoiceItem[$j]['inoi_conv1']);
                    $arrInvoiceItem[$j]['inoi_max2'] = (int) ($stock / $arrInvoiceItem[$j]['inoi_conv2']);
                    $stock = $stock - ($arrInvoiceItem[$j]['inoi_max2'] * $arrInvoiceItem[$j]['inoi_conv2']);
                    $arrInvoiceItem[$j]['inoi_max3'] = (int) ($stock / $arrInvoiceItem[$j]['inoi_conv3']);
                    break;
                }
            }
            for($j = 0;$j < count($arrInvoiceItemBonus); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItemBonus[$j]['inoi_product_id']) {
                    $stock = countStock($arrInvoiceItemBonus[$j]['inoi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                    $arrInvoiceItemBonus[$j]['inoi_max1'] = (int) ($stock / $arrInvoiceItemBonus[$j]['inoi_conv1']);
                    $stock = $stock - ($arrInvoiceItemBonus[$j]['inoi_max1'] * $arrInvoiceItemBonus[$j]['inoi_conv1']);
                    $arrInvoiceItemBonus[$j]['inoi_max2'] = (int) ($stock / $arrInvoiceItemBonus[$j]['inoi_conv2']);
                    $stock = $stock - ($arrInvoiceItemBonus[$j]['inoi_max2'] * $arrInvoiceItemBonus[$j]['inoi_conv2']);
                    $arrInvoiceItemBonus[$j]['inoi_max3'] = (int) ($stock / $arrInvoiceItemBonus[$j]['inoi_conv3']);
                    break;
                }
            }
        }
        $tambahanId = $tambahanId.'0';
        $tambahanQty = $tambahanQty.'0';
        $this->load->model('Mpotongan');
        $arrPotongan= $this->Mpotongan->getAllPotonganByInvoiceID($intID);
		
	} else {
		$arrInvoiceItem = array(); $arrInvoiceList = array(); $arrReturnItem = array(); $intInvoiceTotal = 0; $intInvoiceGrandTotal = 0;
	}

    $arrData = array(
        'arrInvoiceData' => $arrInvoiceData,
        'arrInvoiceItem' => $arrInvoiceItem,
        'intInvoiceID' => $intID,
        'intInvoiceTotal' => $intInvoiceTotal,
        'arrInvoiceBonusItem' => $arrInvoiceItemBonus,
        'strTambahanId' => $tambahanId,
        'strTambahanQty' => $tambahanQty,
        'intInvoiceGrandTotal' => $intInvoiceGrandTotal,
        'arrPotongan' => $arrPotongan,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->get('process') == 'cashier') && $intID != '') {
        if($this->input->get('process') == 'cashier') {
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'),
                'dataPrint' => $this->Minvoiceorder->getPrintDataByID($intID),
            ), $arrData));
        } else {
            //$this->Minvoiceorder->editAfterPrint($intID);
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => 'invoiceorder',
                // 'dataPrint' => $this->Minvoiceorder->getPrintDataByID($intID),
            ), $arrData));
        }

    }else if(($this->input->post('subPrintTax') != '') || ($this->input->get('print2') != '')) {//print e pajak
        // $this->load->model('Mprintlog');
        // $first=$this->input->get('first');
        // $kode=$this->input->get('kode');
        // $date=$this->input->get('date');
        // if($first=='1'){
        //     $idPrintLog=$this->Mprintlog->add($kode,'invoice',$intID);
        // }
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoiceordertax',
            'dataPrint' => $this->Minvoiceorder->getPrintDataByID($intID),
            'dateTax' => $date,
            'kodeTax' => $kode,
        ), $arrData));
    } else {
        # Load all other invoice data the user has ever made
        $arrInvoiceList = $this->Minvoiceorder->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'invoiceorder/view',
            'arrInvoiceList' => $arrInvoiceList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoiceorder-invoiceorderstatus')
        ), $arrData, $this->admlinklist->getMenuPermission(42,$arrInvoiceData['inor_rawstatus'])));
    }
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrInvoice = $this->Minvoiceorder->searchByCustomer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('invoice_order/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrInvoice) ? count($arrInvoice) : '0')." records).";
    } else {
		$arrPagination['base_url'] = site_url("invoice_order/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Minvoiceorder->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrInvoice = $this->Minvoiceorder->getItems($intPage,$arrPagination['per_page']);
	}

    if(!empty($arrInvoice)) for($i = 0; $i < count($arrInvoice); $i++) {
        $arrInvoiceItem = $this->Minvoiceorderitem->getItemsByInvoiceID($arrInvoice[$i]['id'],'COUNT_PRICE');
        $intInvoiceTotal = 0;
        $this->load->model('Munit');
        for($j = 0; $j < count($arrInvoiceItem); $j++) {
            $txtDiscount1 = $arrInvoiceItem[$j]['inoi_discount1'];
            $txtDiscount2 = $arrInvoiceItem[$j]['inoi_discount2'];
            $txtDiscount3 = $arrInvoiceItem[$j]['inoi_discount3'];
            $intQty1 = $arrInvoiceItem[$j]['inoi_quantity1'];
            $intQty2 = $arrInvoiceItem[$j]['inoi_quantity2'];
            $intQty3 = $arrInvoiceItem[$j]['inoi_quantity3'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$j]['inoi_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$j]['inoi_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$j]['inoi_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrInvoiceItem[$j]['inoi_price'];
            if((float) $intConv1 * (float) $intConv2 * (float) $intQty2 <= 0) $tmpTotalPriceConv2 = 0;
            else $tmpTotalPriceConv2 = ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2);
            if((float) $intConv1 * (float) $intConv3 * (float) $intQty3 <= 0) $tmpTotalPriceConv3 = 0;
            else $tmpTotalPriceConv3 = ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);
            
			$intInvoiceTotalTemp = ((float) $intPrice * (float) $intQty1) + $tmpTotalPriceConv2 + $tmpTotalPriceConv3;
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount1 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount2 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount3 / 100);
            $intInvoiceTotal += $intInvoiceTotalTemp;
        }
		
		$arrInvoice[$i] = array_merge($arrInvoice[$i],$this->admlinklist->getMenuPermission(42,$arrInvoice[$i]['inor_status']));
		$arrInvoice[$i]['inor_rawstatus'] = $arrInvoice[$i]['inor_status'];
        $arrInvoice[$i]['inor_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrInvoice[$i]['inor_status']);
        $arrInvoice[$i]['inor_total'] = (float) $intInvoiceTotal - (float) $arrInvoice[$i]['inor_discount'];
        $arrInvoice[$i]['inor_total'] = $arrInvoice[$i]['inor_total'] + ($arrInvoice[$i]['inor_total'] * $arrInvoice[$i]['inor_tax'] / 100);
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'invoiceorder/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrInvoice' => $arrInvoice,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoiceorder-invoiceorder')
    ));
}

}

/* End of File */