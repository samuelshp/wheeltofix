<?php
/*
PUBLIC FUNCTION:

- detailreturjualperbarangbycustomer($intPage = 0)
- detailreturjualpercustomerbybarang($intPage = 0)
- detailreturjualpersalesmanbybarang($intPage = 0)
- detailreturjualpersalesmanbycustomer($intPage = 0)
x rekapreturjualpersalesmanbycustomer($intPage = 0)
x rekapreturjualpercustomerbybarang($intPage = 0)
x rekapreturjualpersalesmanbybarang($intPage = 0)

x detailreturbeliperbarangbysupplier($intPage = 0)
x detailreturbelipersupplierbybarang($intPage = 0)
x rekapreturbelipersupplierbybarang($intPage = 0)

- purchaseretur($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_retur extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mreportretur', 'MReport');
}

public function detailreturjualperbarangbycustomer($intPage = 0) {

    $this->_getMenuHelpContent(142,true,'adminpage');


    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strProductID = $this->input->post('productID');
    $strProductCode = $this->input->post('productCode');
    $strProductName = $this->input->post('productName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('productid') != '')
            $arrItems = $this->MReport->searchDetailReturJualPerBarangByCustomer($this->input->get('productid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strProductID = $this->input->get('productid');

        $this->load->model('Mproduct');
        $product = $this->Mproduct->getItemByID($strProductID);
        $product['strName'] = formatProductName('',$product['prod_title'],$product['prob_title'],$product['proc_title']);


        $strProductCode = $product['prod_code'];
        $strProductName = $product['strName'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('productID') != '')
			$arrItems = $this->MReport->searchDetailReturJualPerBarangByCustomer($this->input->post('productID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		else $arrItems = '';

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_retur/detailreturjualperbarangbycustomer', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_retur/detailreturjualperbarangbycustomer?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailReturJualPerBarangByCustomer();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailReturJualPerBarangByCustomer($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

			if(empty($arrItems[$i]['invi_discount2']))
				$arrItems[$i]['invi_discount2'] = 0;
			if(empty($arrItems[$i]['invi_discount3']))
				$arrItems[$i]['invi_discount3'] = 0;
            $arrItems[$i]['netprice'] = $arrItems[$i]['inri_price'] - ($arrItems[$i]['inri_price'] * $arrItems[$i]['inri_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['invi_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strProductID' => $strProductID,
        'strProductCode' => $strProductCode,
        'strProductName' => $strProductName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailreturjualperbarangbycustomer_export', $arrData, true),
        'detail_retur_jual_per_barang_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailreturjualperbarangbycustomer',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_retur/detailreturjualperbarangbycustomer',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }

}

public function detailreturjualpercustomerbybarang($intPage = 0) {
    $this->_getMenuHelpContent(143,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strCustomerID = $this->input->post('customerID');
    $strCustomerCode = $this->input->post('customerCode');
    $strCustomerName = $this->input->post('customerName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('custid') != '')
            $arrItems = $this->MReport->searchDetailReturJualPerCustomerByBarang($this->input->get('custid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strCustomerID = $this->input->get('custid');

        $this->load->model('Mcustomer');
        $customer = $this->Mcustomer->getItemByID($strCustomerID);

        $strCustomerCode = $customer['cust_code'];
        $strCustomerName = $customer['cust_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('customerID') != '')
			$arrItems = $this->MReport->searchDetailReturJualPerCustomerByBarang($this->input->post('customerID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_retur/detailreturjualpercustomerbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_retur/detailreturjualpercustomerbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailReturJualPerCustomerByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailReturJualPerCustomerByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['inri_price'] - ($arrItems[$i]['inri_price'] * $arrItems[$i]['inri_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['inri_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['inri_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strCustomerID' => $strCustomerID,
        'strCustomerCode' => $strCustomerCode,
        'strCustomerName' => $strCustomerName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailreturjualpercustomerbybarang_export', $arrData, true),
        'detail_retur_jual_per_customer_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailreturjualpercustomerbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_retur/detailreturjualpercustomerbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function detailreturjualpersalesmanbybarang($intPage = 0) {
    $this->_getMenuHelpContent(144,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchDetailReturJualPerSalesmanByBarang($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchDetailReturJualPerSalesmanByBarang($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_retur/detailreturjualpersalesmanbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_retur/detailreturjualpersalesmanbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailReturJualPerSalesmanByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailReturJualPerSalesmanByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            if(empty($arrItems[$i]['invi_price']))
				$arrItems[$i]['invi_price'] = 0;
			if(empty($arrItems[$i]['invi_discount1']))
				$arrItems[$i]['invi_discount1'] = 0;
			if(empty($arrItems[$i]['invi_discount2']))
				$arrItems[$i]['invi_discount2'] = 0;
			if(empty($arrItems[$i]['invi_discount3']))
				$arrItems[$i]['invi_discount3'] = 0;
            $arrItems[$i]['netprice'] = $arrItems[$i]['inri_price'] - ($arrItems[$i]['inri_price'] * $arrItems[$i]['inri_discount1'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['inri_discount2'] / 100);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['inri_discount3'] / 100);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailreturjualpersalesmanbybarang_export', $arrData, true),
        'detail_retur_jual_per_salesman_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailreturjualpersalesmanbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_retur/detailreturjualpersalesmanbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function detailreturjualpersalesmanbycustomer($intPage = 0) {
    $this->_getMenuHelpContent(145,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSalesmanID = $this->input->post('salesmanID');
    $strSalesmanCode = $this->input->post('salesmanCode');
    $strSalesmanName = $this->input->post('salesmanName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('salesmanid') != '')
            $arrItems = $this->MReport->searchDetailReturJualPerSalesmanByCustomer($this->input->get('salesmanid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSalesmanID = $this->input->get('salesmanid');

        $this->load->model('Msalesman');
        $salesman = $this->Msalesman->getItemByID($strSalesmanID);


        $strSalesmanCode = $salesman['sals_code'];
        $strSalesmanName = $salesman['sals_name'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('salesmanID') != '')
			$arrItems = $this->MReport->searchDetailReturJualPerSalesmanByCustomer($this->input->post('salesmanID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		else $arrItems = '';

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_retur/detailreturjualpersalesmanbycustomer', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_retur/detailreturjualpersalesmanbycustomer?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailReturJualPerSalesmanByCustomer();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailReturJualPerSalesmanByCustomer($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSalesmanID' => $strSalesmanID,
        'strSalesmanCode' => $strSalesmanCode,
        'strSalesmanName' => $strSalesmanName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailreturjualpersalesmanbycustomer_export', $arrData, true),
        'detail_retur_jual_per_salesman_by_customer.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailreturjualpersalesmanbycustomer',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_retur/detailreturjualpersalesmanbycustomer',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }

}

public function purchaseretur($intPage = 0) {
    $this->_getMenuHelpContent(141,true,'adminpage');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        if($this->input->get('supplier') != '') 
            $arrRetur = $this->MReport->searchPurchaseReturPerSupplier($this->input->get('supplier'));
        else 
            $arrRetur = $this->MReport->getItemsPurchaseRetur();
        
    } else if($this->input->post('subSearch') != '') {
		$arrRetur = $this->MReport->searchPurchaseReturBySupplier($this->input->post('txtSearchValue'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_retur/purchaseretur', NULL, FALSE).'">[Back]</a>';
		$this->_arrData['strKeyword'] = $this->input->post('txtSearchValue');
		$this->_arrData['strMessage'] = "Search by Supplier with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	
    } else {
		$arrPagination['base_url'] = site_url("report_retur/purchaseretur?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountPurchaseRetur();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		
		$arrRetur = $this->MReport->getItemsPurchaseRetur($intPage,$arrPagination['per_page']);
	}

    $this->load->model('Munit');
    if (!empty($arrRetur)) {
        for($i = 0; $i < count($arrRetur); $i++) {
            $txtDiscount1 = $arrRetur[$i]['prri_discount1'];
            $txtDiscount2 = $arrRetur[$i]['prri_discount2'];
            $txtDiscount3 = $arrRetur[$i]['prri_discount3'];
            $intQty1 = $arrRetur[$i]['prri_quantity1'];
            $intQty2 = $arrRetur[$i]['prri_quantity2'];
            $intQty3 = $arrRetur[$i]['prri_quantity3'];
            $convTemp = $this->Munit->getConversion($arrRetur[$i]['prri_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrRetur[$i]['prri_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrRetur[$i]['prri_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrRetur[$i]['prri_price'];
            $intPurchaseTotalTemp=0;

            if($intConv1 > 0) $intPurchaseTotalTemp = ((float) $intPrice * (float) $intQty1);
            if($intConv2 > 0) $intPurchaseTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2);
            if($intConv3 > 0) $intPurchaseTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);

            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount1 / 100);
            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount2 / 100);
            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount3 / 100);

            $arrRetur[$i]['prri_subtotal'] = $intPurchaseTotalTemp;
            $arrRetur[$i]['prri_conv1'] = $intConv1;
            $arrRetur[$i]['prri_conv2'] = $intConv2;
            $arrRetur[$i]['prri_conv3'] = $intConv3;
        }
    }

    $arrData = array(
        'arrRetur' => $arrRetur,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/reportpurchasereturprintout_export', $arrData, true),
        'purchase_retur_printout.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print 
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'reportpurchasereturprintout',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_retur/purchaseretur',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
        ), $arrData));
    }
}

}