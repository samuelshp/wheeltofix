<?php
/*
PUBLIC FUNCTION:
- index()
- view(intID)
- browse(intPage)
- mutasi()
- opname()

PRIVATE FUNCTION:
- __construct()
*/

class Adjustment extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();
	
	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
	$this->load->model('Madjustment');
    $this->load->model('Madjustmentitem');
}

public function index() {
	$this->_getMenuHelpContent(31,true,'adminpage');
	
	$arrAdjustment = $this->session->userdata('arrAdjustment');
	if(!empty($arrAdjustment)) $arrItem = $arrAdjustment['arrItem'];
	else $arrItem = array();

	$this->load->model('Munit');
	$this->load->model('Mtinydbvo','adjType'); $this->adjType->initialize('adjustment_type');
	$arrAdjType = $this->adjType->getAllData();

	if($this->input->post('smtUpdateAdjustment') != '' || $this->input->post('smtMakeAdjustment') != '') {
		
	}
	
	if($this->input->post('smtMakeAdjustment') != '') {  // Make Adjustment
        $toWare = 0;
        $intCount = $this->input->post('totalItem');
        $intAdjustmentID = $this->Madjustment->add($this->input->post('WarehouseID'),$toWare,$this->input->post('txaDescription'),$this->input->post('txtDate'));
		for($i = 0; $i < $intCount; $i++) {
            $prodItem = $this->input->post('prodItem'.$i);
            $probItem = $this->input->post('probItem'.$i);
            $strProductDescription = formatProductName('',$prodItem,$probItem);

            if($strProductDescription!= '' || !empty($strProductDescription)) {
                $intAdjustmentItemID = $this->Madjustmentitem->add(
                    $intAdjustmentID,
                    $this->input->post('selAdjust'.$i),
                    $this->input->post('idItem'.$i),
                    $strProductDescription,
                    $this->input->post('qty1PriceEffect'.$i),
                    $this->input->post('qty2PriceEffect'.$i),
                    $this->input->post('qty3PriceEffect'.$i),
                    $this->input->post('sel1UnitID'.$i),
                    $this->input->post('sel2UnitID'.$i),
                    $this->input->post('sel3UnitID'.$i),
                    $this->input->post('prcPriceEffect'.$i)
                );
            }
        }
	
		$this->session->unset_userdata('arrAdjustment');
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentadded'));
		redirect('adjustment/view/'.$intAdjustmentID);
	}
	
	if(!empty($arrAdjustment)) $this->_arrData = array_merge($this->_arrData,$arrAdjustment);
	
	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'adjustment/add',
		'arrAdjType' => $arrAdjType,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-addadjustment')
    ));
}

public function mutasi() {
	$this->_getMenuHelpContent(32,true,'adminpage');

	$arrAdjustment = $this->session->userdata('arrAdjustment');
	if(!empty($arrAdjustment)) $arrItem = $arrAdjustment['arrItem'];
	else $arrItem = array();

	$this->load->model('Munit');
	$this->load->model('Mtinydbvo','adjType'); $this->adjType->initialize('adjustment_type');
	$arrAdjType = $this->adjType->getAllData();

	if($this->input->post('smtMakeAdjustment') != '') {  // Make Adjustment
		$toWare = 0;
		$intCount = $this->input->post('totalItem');
		$intAdjustmentID = $this->Madjustment->add($this->input->post('WarehouseID'),$this->input->post('WarehouseID2'),$this->input->post('txaDescription'),$this->input->post('txtDate'));
		
		for($i = 0; $i < $intCount; $i++) {
			$prodItem = $this->input->post('prodItem'.$i);
			$probItem = $this->input->post('probItem'.$i);
			$strProductDescription = formatProductName('',$prodItem,$probItem);

			if($strProductDescription != '' || !empty($strProductDescription)) {
				$intAdjustmentItemID = $this->Madjustmentitem->add(
					$intAdjustmentID,
					'3',
					$this->input->post('idItem'.$i),
					$strProductDescription,
					$this->input->post('qty1PriceEffect'.$i),
					$this->input->post('qty2PriceEffect'.$i),
					$this->input->post('qty3PriceEffect'.$i),
					$this->input->post('sel1UnitID'.$i),
					$this->input->post('sel2UnitID'.$i),
					$this->input->post('sel3UnitID'.$i),
					$this->input->post('prcPriceEffect'.$i)
				);
			}
		}

		$this->session->unset_userdata('arrAdjustment');
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentadded'));
		redirect('adjustment/view/'.$intAdjustmentID);
	}

	if(!empty($arrAdjustment)) $this->_arrData = array_merge($this->_arrData,$arrAdjustment);

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'adjustment/mutasi',
		'arrAdjType' => $arrAdjType,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-mutation')
    ));
}

// To display, edit and delete purchase
public function opname() {
    // WARNING! Don't change the following steps
    $this->_getMenuHelpContent(33,true,'adminpage');

    $intCount = $this->input->post('totalItem');
    if(intval($intCount) > 0) { // Make Adjustment
        $toWare = 0;
        $intAdjustmentID = $this->Madjustment->add(
            $this->input->post('WarehouseID'),
            $toWare,
            $this->input->post('txaDescription'),
            $this->input->post('txtDate'));
        for($i = 0; $i < $intCount; $i++) {
            $prodItem = $this->input->post('prodItem'.$i);
            $probItem = $this->input->post('probItem'.$i);
            $strProductDescription = formatProductName('',$prodItem,$probItem);
            $diff1 = (int) $this->input->post('1different'.$i);
            $diff2 = (int) $this->input->post('2different'.$i);
            $diff3 = (int) $this->input->post('3different'.$i);
            $flag = 1;
            if($diff1 < 0 ||$diff2 < 0 || $diff3 < 0) {
                $diff1=abs($diff1);
                $diff2=abs($diff2);
                $diff3=abs($diff3);
                $flag = 2;
            }
            echo $this->input->post('sel1UnitID'.$i);
            if($strProductDescription != '' || !empty($strProductDescription)) {
                $intAdjustmentItemID = $this->Madjustmentitem->add(
                    $intAdjustmentID,
                    $flag,
                    $this->input->post('idItem'.$i),
                    $strProductDescription,
                    $diff1,
                    $diff2,
                    $diff3,
                    $this->input->post('sel1UnitID'.$i),
                    $this->input->post('sel2UnitID'.$i),
                    $this->input->post('sel3UnitID'.$i),
                    $this->input->post('priceItem'.$i)
                );
            }
        }
        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentadded'));
        redirect('adjustment/browse');
    }

    // Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'adjustment/opname',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-opname')
    ));
}
public function view($intID = 0) {
    $this->_getMenuHelpContent(31,true,'adminpage');
    
    if($this->input->get('m') == 'transaction_history') {
        $intID = $this->Madjustmentitem->getHeaderIDByID($intID);
        if(empty($intID)) {
            $this->session->set_flashdata('strMessage','Wrong Transaction ID');
            redirect('adjustment/browse');
        }
    }

    if($this->input->post('subSave') != '' && $intID != '') { # SAVE
        // Adjustment
        $arrAdjustmentData = $this->Madjustment->getItemByID($intID);
        if(compareData($arrAdjustmentData['ajst_status'],array(0))) {
            $this->Madjustment->editByID2($intID,$this->input->post('WarehouseID'),$this->input->post('WarehouseID2'),$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'),$this->input->post('selEditable'));
            // Load purchase item
            $arrAdjustmentItem = $this->Madjustmentitem->getItemsByAdjustmentID($intID);
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostSelAdjust = $this->input->post('selAdjust');

            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idAditID');
            foreach($arrAdjustmentItem as $e) {
                // Search return quantity
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Madjustmentitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostSelAdjust[$e['id']]);
						
                    } else $this->Madjustmentitem->deleteByID($e['id']);
					
                } else $this->Madjustmentitem->deleteByID($e['id']);
            }

            $totalItem = $this->input->post('totalItem');
            for($i = 0; $i < $totalItem; $i++) {
                $prodItem = $this->input->post('prodItem'.$i);
                $probItem = $this->input->post('probItem'.$i);
                $strProductDescription = formatProductName('',$prodItem,$probItem);
                if($strProductDescription != '' || !empty($strProductDescription)) {
                    $intAdjustmentItemID = $this->Madjustmentitem->add(
                        $intID,
                        $this->input->post('selAdjust'.$i),
                        $this->input->post('idItem'.$i),
                        $strProductDescription,
                        $this->input->post('qty1PriceEffect'.$i),
                        $this->input->post('qty2PriceEffect'.$i),
                        $this->input->post('qty3PriceEffect'.$i),
                        $this->input->post('sel1UnitID'.$i),
                        $this->input->post('sel2UnitID'.$i),
                        $this->input->post('sel3UnitID'.$i),
                        $this->input->post('prcPriceEffect'.$i)
                    );
                }
            }

        } else $this->Madjustment->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));


        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentupdated');

    } else if($this->input->post('subDelete') != '' && $intID != '') {
        $this->Madjustment->deleteByID($intID);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentdeleted'));
        redirect('adjustment/browse');

    }
    $arrAdjustmentData = $this->Madjustment->getItemByID($intID);
    if(!empty($arrAdjustmentData)) {
        $arrAdjustmentData['ajst_rawstatus'] = $arrAdjustmentData['ajst_status'];
        $arrAdjustmentData['ajst_status'] = translateDataIntoHTMLStatements(
            array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
            $arrAdjustmentData['ajst_status']);
        $arrAdjustmentData['ajst_seleditable'] = translateDataIntoHTMLStatements(
            array('title' => 'selEditable', 'field_type' => 'SELECT','description' => 'editable', 'allow_null' => 0, 'validation' => ''),
            $arrAdjustmentData['ajst_editable']);

        // Load the purchase item
        $arrAdjustmentItem = $this->Madjustmentitem->getItemsByAdjustmentID($intID);
        $intAdjustmentTotal = 0;
        $this->load->model('Munit');
        for($i = 0; $i < count($arrAdjustmentItem); $i++) {
            $arrAdjustmentItem[$i]['strName'] = /*$arrAdjustmentItem[$i]['adit_description'].' | '.*/$arrAdjustmentItem[$i]['prod_title'];
            $intQty1 = $arrAdjustmentItem[$i]['adit_quantity1'];
            $intQty2 = $arrAdjustmentItem[$i]['adit_quantity2'];
            $intQty3 = $arrAdjustmentItem[$i]['adit_quantity3'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$i]['adit_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$i]['adit_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$i]['adit_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrAdjustmentItem[$i]['adit_price'];
            $intAdjustmentTotalTemp = 0;
            if($intConv1 > 0) $intAdjustmentTotalTemp = ((float) $intPrice * (float) $intQty1);
            if($intConv2 > 0) $intAdjustmentTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2);
            if($intConv3 > 0) $intAdjustmentTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);
			
            $intAdjustmentTotal+=$intAdjustmentTotalTemp;
            $arrAdjustmentItem[$i]['adit_subtotal']=$intAdjustmentTotalTemp;
            $arrAdjustmentItem[$i]['adit_conv1']=$intConv1;
            $arrAdjustmentItem[$i]['adit_conv2']=$intConv2;
            $arrAdjustmentItem[$i]['adit_conv3']=$intConv3;
        }
    }

    $arrData = array(
        'intAdjustmentID' => $intID,
        'intAdjustmentTotal' => $intAdjustmentTotal,
        'arrAdjustmentItem' => $arrAdjustmentItem,
        'arrAdjustmentData' => $arrAdjustmentData,
    );

    // Load all other purchase data the user has ever made
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'adjustment',
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        ), $arrData));
    } else {
        $arrAdjustmentList = $this->Madjustment->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'adjustment/view',
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustmentdata')
        ), $arrData, $this->admlinklist->getMenuPermission(31,$arrAdjustmentData['ajst_rawstatus'])));
    }
}

public function browse($intPage = 0) {
    $this->_getMenuHelpContent(31,true,'adminpage');

    if($this->input->post('subSearch') != '') {
        $arrAdjustment = $this->Madjustment->searchByWarehouse($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('adjustment/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrAdjustment) ? count($arrAdjustment) : '0')." records).";
    } else {
        $arrPagination['base_url'] = site_url("adjustment/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Madjustment->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrAdjustment = $this->Madjustment->getItems($intPage,$arrPagination['per_page']);
    }
    if(!empty($arrAdjustment)) for($i = 0; $i < count($arrAdjustment); $i++) {
        $arrAdjustmentItem = $this->Madjustmentitem->getItemsByAdjustmentID($arrAdjustment[$i]['id'],'COUNT_PRICE');
        $intAdjustmentTotal = 0;
        $this->load->model('Munit');
        for($j = 0; $j < count($arrAdjustmentItem); $j++) {
            $intQty1 = $arrAdjustmentItem[$j]['adit_quantity1'];
            $intQty2 = $arrAdjustmentItem[$j]['adit_quantity2'];
            $intQty3 = $arrAdjustmentItem[$j]['adit_quantity3'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$j]['adit_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$j]['adit_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAdjustmentItem[$j]['adit_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrAdjustmentItem[$j]['adit_price'];
            $intAdjustmentTotalTemp = 0;
            if($intConv1 > 0) $intAdjustmentTotalTemp = ((float) $intPrice * (float) $intQty1);
            if($intConv2 > 0) $intAdjustmentTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2);
            if($intConv3 > 0) $intAdjustmentTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);
            
            $intAdjustmentTotal += $intAdjustmentTotalTemp;
        }
		
		$arrAdjustment[$i] = array_merge($arrAdjustment[$i],$this->admlinklist->getMenuPermission(31,$arrAdjustment[$i]['ajst_status']));
		$arrAdjustment[$i]['ajst_rawstatus'] = $arrAdjustment[$i]['ajst_status'];
        $arrAdjustment[$i]['ajst_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrAdjustment[$i]['ajst_status']);
        $arrAdjustment[$i]['ajst_total'] = (float)$intAdjustmentTotal;
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'adjustment/browse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrAdjustment' => $arrAdjustment,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'adjustment-adjustment')
    ));
}

}

/* End of File */