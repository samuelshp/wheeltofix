<?php
/*
PUBLIC FUNCTION:
- index(strMode)
- browse(intPage)
- view(intInvoiceID)

PRIVATE FUNCTION:
- __construct()
*/

class Invoice extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Minvoice');
	$this->load->model('Minvoiceitem');

    $this->_getMenuHelpContent(225,true,'adminpage');
}

public function index($strMode = 'normal') {
    // WARNING! Don't change the following steps
    $bolKanvasAvlb = $this->admlinklist->getAvailability(54,$this->session->userdata('strAdminPriviledge'));

	if($this->input->post('smtUpdateInvoice') != '' || $this->input->post('smtMakeInvoice') != '') { 
	}

	if($this->input->post('smtMakeInvoice') != '') { // Make Invoice

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoicemade'));
        if(!empty($_POST['txtPayment'])) $this->session->set_flashdata('txtPayment', $_POST['txtPayment']);
        if(!empty($_POST['txtPaymentChange'])) $this->session->set_flashdata('txtPaymentChange', $_POST['txtPaymentChange']);
        if($strMode == 'cashier' || $this->input->post('smtMakeInvoice') == 'Make Cashier Invoice') redirect('invoice/view/'.$intInvoiceID.'?print=true&process=cashier');
        else redirect('invoice/view/'.$intInvoiceID);
	
    }
    $this->load->model('Macceptance');
    $arrBPB = $this->Macceptance->getAllBPBData();

    $this->load->view('sia',array(
        'strViewFile' => 'invoice/add',
        'arrBPB' => $arrBPB,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-addinvoice')
    ));

}

// To display, edit and delete invoice
public function view($intID = '') {
	# INIT
	$this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwsettingowner');

	if($this->input->post('subSave') != '' && $intID != '') {
		
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoiceupdated');

	} else if($this->input->post('subDelete') != '' && $intID != '') {
		/* must use this, because item's trigger can't be activated in header trigger */
        $this->Minvoiceitem->deleteByInvoiceID($intID);
		$this->Minvoice->deleteByID($intID);

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoicedeleted'));
		redirect('invoice/browse');
	}

	$arrInvoiceData = $this->Minvoice->getItemByID($intID);
	if(!empty($arrInvoiceData)) {
		$arrInvoiceData['invo_rawstatus'] = $arrInvoiceData['invo_status'];
		$arrInvoiceData['invo_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrInvoiceData['invo_status']);
		$intID = $arrInvoiceData['id'];
        $temp=explode(' ',$arrInvoiceData['invo_date']);
        $arrInvoiceData['date']=$temp[0];

		// Load the invoice item
        $arrInvoiceItem = $this->Minvoiceitem->getItemsByInvoiceID($intID);
        $intInvoiceTotal = 0;
        $tambahan = array();
        $countertambahan = 0;
        $this->load->model('Munit');
        for($i = 0; $i < count($arrInvoiceItem); $i++) {
            $txtDiscount1 = $arrInvoiceItem[$i]['invi_discount1'];
            $txtDiscount2 = $arrInvoiceItem[$i]['invi_discount2'];
            $txtDiscount3 = $arrInvoiceItem[$i]['invi_discount3'];
            $intQty1 = $arrInvoiceItem[$i]['invi_quantity1'];
            $intQty2 = $arrInvoiceItem[$i]['invi_quantity2'];
            $intQty3 = $arrInvoiceItem[$i]['invi_quantity3'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrInvoiceItem[$i]['invi_price'];
            $intInvoiceTotalTemp = ((float) $intPrice * (float) $intQty1) + ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2) + ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * $intQty3);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount1 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount2 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount3 / 100);
            $intInvoiceTotal += $intInvoiceTotalTemp;
            $arrInvoiceItem[$i]['invi_subtotal'] = $intInvoiceTotalTemp;
            $arrInvoiceItem[$i]['invi_conv1'] = $intConv1;
            $arrInvoiceItem[$i]['invi_conv2'] = $intConv2;
            $arrInvoiceItem[$i]['invi_conv3'] = $intConv3;
            if(!empty($tambahan)) {
                $flag = 0;
                for($j = 0; $j < count($tambahan); $j++) {
                    if($tambahan[$j]['id'] == $arrInvoiceItem[$i]['invi_product_id']) {
                        $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $flag = 1;
                        break;
                    }
                }
                if($flag == 0) {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['invi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            } else {
                $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['invi_product_id'];
                $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                $countertambahan++;
            }
        }
        $pengurangan = 0;
        $arrKreditCustomer = $this->Minvoice->getCustomerCredit($arrInvoiceData['cust_id']);
        for($ii = 0; $ii < count($arrKreditCustomer); $ii++) {
            $pengurangan += (float) $arrKreditCustomer[$ii]['invo_grandtotal'];
        }
        if(is_numeric($pengurangan) && $pengurangan>0) {
            $arrInvoiceData['cust_limitkredit'] = (float)$arrInvoiceData['cust_limitkredit']-(float)$pengurangan+(float)$arrInvoiceData['invo_grandtotal'];
        }
        $InvoiceTotal = (float) $intInvoiceTotal - (float) $arrInvoiceData['invo_discount'];
        $arrInvoiceData['subtotal_discounted'] = $InvoiceTotal;
        $intInvoiceGrandTotal = (float) $InvoiceTotal + ($InvoiceTotal * (float) $arrInvoiceData['invo_tax'] / 100);
        $arrInvoiceItemBonus = $this->Minvoiceitem->getBonusItemsByInvoiceID($intID);
        if(!empty($arrInvoiceItemBonus)) {
            for($i = 0; $i < count($arrInvoiceItemBonus); $i++) {
                $arrInvoiceItemBonus[$i]['strName'] = $arrInvoiceItemBonus[$i]['invi_description'].'|'.$arrInvoiceItemBonus[$i]['proc_title'];
                $intQty1 = $arrInvoiceItemBonus[$i]['invi_quantity1'];
                $intQty2 = $arrInvoiceItemBonus[$i]['invi_quantity2'];
                $intQty3 = $arrInvoiceItemBonus[$i]['invi_quantity3'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrInvoiceItemBonus[$i]['invi_conv1'] = $intConv1;
                $arrInvoiceItemBonus[$i]['invi_conv2'] = $intConv2;
                $arrInvoiceItemBonus[$i]['invi_conv3'] = $intConv3;
                if(!empty($tambahan)) {
                    $flag = 0;
                    for($j = 0; $j < count($tambahan); $j++) {
                        if($tambahan[$j]['id'] == $arrInvoiceItemBonus[$i]['invi_product_id']) {
                            $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                            $flag = 1;
                            break;
                        }
                    }
                    if($flag == 0) {
                        $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['invi_product_id'];
                        $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $countertambahan++;
                    }
                } else {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['invi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            }
        }
        $tambahanId = '';
        $tambahanQty = '';
        if($arrInvoiceData['invo_soid']!='0'){
            $this->load->model('Minvoiceorderitem');
            $masterStock=$this->Minvoiceorderitem->getItemsByInvoiceID($arrInvoiceData['invo_soid']);
            $masterStockBonus=$this->Minvoiceorderitem->getBonusItemsByInvoiceID($arrInvoiceData['invo_soid']);
        }
        for($i = 0;$i < count($tambahan); $i++) {
            $tambahanId = $tambahanId.$tambahan[$i]['id'].'-';
            $tambahanQty = $tambahanQty.$tambahan[$i]['qty'].'-';
            for($j = 0; $j < count($arrInvoiceItem); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItem[$j]['invi_product_id']) {
                    if($arrInvoiceData['invo_soid']=='0'){
                        $stock = countStock($arrInvoiceItem[$j]['invi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                        $arrInvoiceItem[$j]['invi_max1'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max1'] * $arrInvoiceItem[$j]['invi_conv1']);
                        $arrInvoiceItem[$j]['invi_max2'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max2'] * $arrInvoiceItem[$j]['invi_conv2']);
                        $arrInvoiceItem[$j]['invi_max3'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv3']);
                        break;
                    }else{
                        //sini
                        $stock=0;
                        foreach($masterStock as $e) {
                            if($e['inoi_product_id']== $arrInvoiceItem[$j]['invi_product_id']){
                                $stock+=($e['inoi_quantity1']-$e['inoi_selled1'])*$arrInvoiceItem[$j]['invi_conv1'];
                                $stock+=($e['inoi_quantity2']-$e['inoi_selled2'])*$arrInvoiceItem[$j]['invi_conv2'];
                                $stock+=($e['inoi_quantity3']-$e['inoi_selled3'])*$arrInvoiceItem[$j]['invi_conv3'];
                                $stock+=$tambahan[$i]['qty'];
                            }
                        }
                        $arrInvoiceItem[$j]['invi_max1'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max1'] * $arrInvoiceItem[$j]['invi_conv1']);
                        $arrInvoiceItem[$j]['invi_max2'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max2'] * $arrInvoiceItem[$j]['invi_conv2']);
                        $arrInvoiceItem[$j]['invi_max3'] = (int) ($stock / $arrInvoiceItem[$j]['invi_conv3']);
                    }

                }
            }
            for($j = 0;$j < count($arrInvoiceItemBonus); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItemBonus[$j]['invi_product_id']) {
                    if($arrInvoiceData['invo_soid']=='0'){
                        $stock = countStock($arrInvoiceItemBonus[$j]['invi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                        $arrInvoiceItemBonus[$j]['invi_max1'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max1'] * $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $arrInvoiceItemBonus[$j]['invi_max2'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max2'] * $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $arrInvoiceItemBonus[$j]['invi_max3'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv3']);
                        break;
                    }else{
                        //sini
                        $stock=0;
                        foreach($masterStockBonus as $e) {
                            if($e['inoi_product_id']== $arrInvoiceItemBonus[$j]['invi_product_id']){
                                $stock+=($e['inoi_quantity1']-$e['inoi_selled1'])*$arrInvoiceItemBonus[$j]['invi_conv1'];
                                $stock+=($e['inoi_quantity2']-$e['inoi_selled2'])*$arrInvoiceItemBonus[$j]['invi_conv2'];
                                $stock+=($e['inoi_quantity3']-$e['inoi_selled3'])*$arrInvoiceItemBonus[$j]['invi_conv3'];
                                $stock+=$tambahan[$i]['qty'];
                            }
                        }
                        $arrInvoiceItemBonus[$j]['invi_max1'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max1'] * $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $arrInvoiceItemBonus[$j]['invi_max2'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max2'] * $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $arrInvoiceItemBonus[$j]['invi_max3'] = (int) ($stock / $arrInvoiceItemBonus[$j]['invi_conv3']);
                    }
                }
            }
        }
        $tambahanId = $tambahanId.'0';
        $tambahanQty = $tambahanQty.'0';
        $this->load->model('Mpotongan');
        $arrPotongan= $this->Mpotongan->getAllPotonganByInvoiceID($intID);

	} else {
		$arrInvoiceItem = array(); $arrInvoiceList = array(); $arrReturnItem = array(); $intInvoiceTotal = 0; $intInvoiceGrandTotal = 0;
	}

    $arrData = array(
        'arrInvoiceData' => $arrInvoiceData,
        'arrInvoiceItem' => $arrInvoiceItem,
        'intInvoiceID' => $intID,
        'intInvoiceTotal' => $intInvoiceTotal,
        'arrInvoiceBonusItem' => $arrInvoiceItemBonus,
        'strTambahanId' => $tambahanId,
        'strTambahanQty' => $tambahanQty,
        'intInvoiceGrandTotal' => $intInvoiceGrandTotal,
        'arrPotongan' => $arrPotongan,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    $this->load->model('Mprintlog');

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->get('process') == 'cashier') && $intID != '') {
        if($this->input->get('process') == 'cashier') {
            if($this->session->flashdata('txtPayment') != '') $txtPayment = $this->session->flashdata('txtPayment');
            if($this->session->flashdata('txtPaymentChange') != '') $txtPaymentChange = $this->session->flashdata('txtPaymentChange');
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'),
                'dataPrint' => $this->Minvoice->getPrintDataByID($intID),
                'txtPayment' => !empty($txtPayment) ? $txtPayment : '',
                'txtPaymentChange' => !empty($txtPaymentChange) ? $txtPaymentChange : '',
            ), $arrData));
            $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'));

        } else {
            //$this->Minvoice->editAfterPrint($intID);
            $strLastTemplate = $this->Mprintlog->getLastTemplate('invoice', $intID);
            if(empty($strLastTemplate)) {
                if($this->input->post('subPrint') == 'Print + Surat Jalan') {
                    $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE').'_suratjalan';
                } else {
                    $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE');
                }
            }
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $strLastTemplate,
                'dataPrint' => $this->Minvoice->getPrintDataByID($intID),
            ), $arrData));
            $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $strLastTemplate);
        }

    }else if(($this->input->post('subPrintTax') != '') || ($this->input->get('print2') != '')) {//print e pajak
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoicetax',
            'dataPrint' => $this->Minvoice->getPrintDataByID($intID),
            'dateTax' => $date,
            'kodeTax' => $kode,
        ), $arrData));
        $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, 'invoicetax');

    } else {
    	# Load all other invoice data the user has ever made
        $arrInvoiceList = $this->Minvoice->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'invoice/view',
            'arrInvoiceList' => $arrInvoiceList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicestatus')
        ), $arrData, $this->admlinklist->getMenuPermission(43,$arrInvoiceData['invo_rawstatus'])));
    }
}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {
        $arrInvoice = $this->Minvoice->searchByCustomer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('invoice/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrInvoice) ? count($arrInvoice) : '0')." records).";
    } else {
        $arrPagination['base_url'] = site_url("invoice/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Minvoice->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrInvoice = $this->Minvoice->getItems($intPage,$arrPagination['per_page']);
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'invoice/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
        'arrInvoice' => $arrInvoice,
		'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoice')
    ));
}

}

/* End of File */