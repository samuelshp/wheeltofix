<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Downpayment extends JW_Controller {

public function __construct() {
	parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mdownpayment');

    $this->_getMenuHelpContent(238,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtSaveDownpayment') != '') { // Save Kontrak Pembelian
        $strDPCode = 'DP/'.date('y').'/'.date('m').'/';
        $lastCode = $this->Mdownpayment->getLastCode($strDPCode);
        $newCode = 1;
        if(!empty($lastCode))
            $newCode = $lastCode['code']+1;
        $strDPCode .= str_pad($newCode, 4 , '0', STR_PAD_LEFT);
        $arrDownpayment = array(
            'strDPCode' => $strDPCode,
            'strDate' => $this->input->post('txtDate'),
            'intSupplierID' => $this->input->post('intSupplierID'),
            'intKPID' => $this->input->post('intKPID'),
            'intAmount' => $this->input->post('txtAmount'),
            'strDescription' => $this->input->post('txtDescription')
        );
        $this->Mdownpayment->add($arrDownpayment['strDPCode'],$arrDownpayment['strDate'],$arrDownpayment['intSupplierID'],$arrDownpayment['intAmount'],$arrDownpayment['strDescription']);

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'downpayment-save');
        redirect('downpayment/browse');
	}
    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'downpayment/add',
        'intField' => $intField,
        'arrProduct' => $arrProduct,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-add')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
	if($this->input->post('subSave') != '' && !empty($intID)) { # SAVE
		
	} else if($this->input->post('subDelete') != '' && !empty($intID)) {
		
	} else if($this->input->post('subPrint') != '' && !empty($intID)) {
		
	}

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'downpayment/view',
        'intBookingID' => $intID,
        'arrBookingList' => $arrDownpaymentList,
        'arrBookingData' => $arrDownpaymentData,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingdata')
    ), $this->admlinklist->getMenuPermission(46,$arrDownpaymentData['book_rawstatus'])));

}

public function browse($intPage = 0) {
    $arrDownpayment = $this->Mdownpayment->getAllDownpayment();
    
    $this->load->view('sia',array(
        'strViewFile' => 'downpayment/browse',
        'arrDownpayment' => $arrDownpayment,
		'intPage' => $intPage,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-browse')
    ));
}

public function getKontrakPembelianBySuppID($intSupplierID) {
    $this->load->model('Mkontrakpembelian');
    $arrKontrakPembelian = $this->Mkontrakpembelian->getDataBySupplierID($intSupplierID);

    return $this->output->set_content_type('application/json')->set_output(json_encode($arrKontrakPembelian));
}


/* AUTOCOMPLETE */

}

/* End of File */