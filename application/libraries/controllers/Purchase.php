<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mpurchase');
	$this->load->model('Mpurchaseitem');

    $this->_getMenuHelpContent(22,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtUpdatePurchase') != '' || $this->input->post('smtMakePurchase') != '') {
	}

	if($this->input->post('smtMakePurchase') != '') { // Make Purchase
        $strPurchaseCode = '/'.date('m').'/'.date('y').'/OP';
        $lastCode = $this->MPurchase->getLastPurchaseCode();
        $newCode = $lastCode['code']+1;
        $strPurchaseCode = str_pad($newCode, 4 , '0', STR_PAD_LEFT) . $strPurchaseCode;
        $arrPurchase = array(
            'strPurchaseCode' => $strPurchaseCode,
            'strDate'=> $this->input->post('txtDate'),
            'intPBID' => $this->input->post('pbID'),
            'intSupplierID' => $this->input->post('supplierID'),
            'intKPID' => $this->input->post('kpID'),
            'intSubTotal'=> $this->input->post('subTotalNoTax'),
            'intDiscount'=> $this->input->post('dsc4'),
            'intTax'=> $this->input->post('txtTax'),
            'intCostAdd'=> $this->input->post('subTotalCost'),
            'intGrandTotal'=> $this->input->post('grandTotal'),
        );

        $intPurchaseID = $this->Mpurchase->add($arrPurchase['strPurchaseCode'],$arrPurchase['strDate'],$arrPurchase['intPBID'],$arrPurchase['intSupplierID'],$arrPurchase['intKPID'],$arrPurchase['intSubTotal'],$arrPurchase['intDiscount'],$arrPurchase['intTax'],$arrPurchase['intCostAdd'],$arrPurchase['intGrandTotal']);
        $totalItem = $this->input->post('totalItem');
        $this->load->model('Mpurchaseitem');
        for($i = 0; $i < $totalItem; $i++) {
            $id = $this->input->post('productID'.$i);
            $qtyBayar = $this->input->post('qtyBayarX'.$i);//data untuk PO
            $hargaBayar = $this->input->post('hargaBayarx'.$i);//data untuk PO
            $this->Mpurchaseitem->add($intPurchaseID,$id,'',$qtyBayar,0,0,0,0,0,$hargaBayar,0,0,0,0,0,0);
            $this->Mpurchaseitem->updatePurchaseOrderItem($qtyBayar, $arrPurchase['intPBID'], $id);// update terpo purchase order material
            // $this->Mpurchaseorderitem->UpdatePurchasedItem($arrPurchase['intPO'],$idItem,$qty1Item,$qty2Item,$qty3Item,0);
        }
        $totalItemBonus = $this->input->post('totalItemBonus');
        for($i = 0; $i < $totalItemBonus; $i++) {
            $id = $this->input->post('accoID'.$i);
            $hargaBayar = $this->input->post('amountX'.$i);
            $this->Mpurchaseitem->add($intPurchaseID,0,'',1,0,0,0,0,0,$hargaBayar,0,0,0,0,0,$id);
        }
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchasemade');
        redirect('purchase/view/'.$intPurchaseID);
	}
    $this->load->model('Mcustomer');
    $arrOwner = $this->Mcustomer->getAllCustomer();

    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'purchase/add',
        'arrOwner' => $arrOwner,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addpurchase')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
	$arrReturn = $this->session->userdata('arrReturn');

	if($this->input->post('subSave') != '' && $intID != '') { # SAVE
		// Purchase
		$arrPurchaseData = $this->Mpurchase->getItemByID($intID);
        $POID = $this->input->post('POID');
        $this->load->model('Mpurchaseorderitem');
        $this->load->model('Munit');
		if(compareData($arrPurchaseData['prch_status'],array(0))) {

            $this->Mpurchase->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'),$this->input->post('dsc4'),$this->input->post('txtTax'));
			// Load purchase item
			$arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrAwal1 = $this->input->post('awal1Item');
            $arrAwal2 = $this->input->post('awal2Item');
            $arrAwal3 = $this->input->post('awal3Item');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');
            $arrIdItem = $this->input->post('idItem');
            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
			foreach($arrPurchaseItem as $e) {
				// Search return quantity
				
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Mpurchaseitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);
                        if($POID>0){
                            $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                            $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                            $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                            if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                $this->Mpurchaseorderitem->UpdatePurchasedItemVIew($POID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,0);
                            }
                        }
                    } else $this->Mpurchaseitem->deleteByID($e['id']);
					
                } else $this->Mpurchaseitem->deleteByID($e['id']);
                
			}
            $arrPurchaseItem = $this->Mpurchaseitem->getBonusItemsByPurchaseID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');
            $arrAwal1 = $this->input->post('awal1ItemBonus');
            $arrAwal2 = $this->input->post('awal2ItemBonus');
            $arrAwal3 = $this->input->post('awal3ItemBonus');
            $arrIdItem = $this->input->post('idItemBonus');
            $arrContainID = $this->input->post('idProiIDBonus');
            if(!empty($arrPurchaseItem)){
                foreach($arrPurchaseItem as $e) {
                    // Search return quantity
                    
                    if(!empty($arrContainID)) {
                        if (in_array($e['id'], $arrContainID)) {
                            $this->Mpurchaseitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
                            if($POID>0){
                                $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                                $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                                $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                                if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                    $this->Mpurchaseorderitem->UpdatePurchasedItemVIew($POID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,1);
                                }
                            }
                        } else $this->Mpurchaseitem->deleteByID($e['id']);
						
                    } else $this->Mpurchaseitem->deleteByID($e['id']);
                    
                }
            }
            if($POID==0){
                $totalItem = $this->input->post('totalItem');
                for($i = 0; $i < $totalItem; $i++) {
                    $qty1Item = $this->input->post('qty1PriceEffect'.$i);
                    $qty2Item = $this->input->post('qty2PriceEffect'.$i);
                    $qty3Item = $this->input->post('qty3PriceEffect'.$i);
                    $unit1Item = $this->input->post('sel1UnitID'.$i);
                    $unit2Item = $this->input->post('sel2UnitID'.$i);
                    $unit3Item = $this->input->post('sel3UnitID'.$i);
                    $prcItem = $this->input->post('prcPriceEffect'.$i);
                    $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
                    $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
                    $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
                    $prodItem = $this->input->post('prodItem'.$i);
                    $probItem = $this->input->post('probItem'.$i);
                    $strProductDescription = formatProductName('',$prodItem,$probItem);
                    $idItem = $this->input->post('idItem'.$i);
                    if($strProductDescription != '' || !empty($strProductDescription)) {
                        $this->Mpurchaseitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0,1);
                    }
                }
                $totalItemBonus = $this->input->post('totalItemBonus');
                for($i = 0; $i < $totalItemBonus; $i++) {
                    $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
                    $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
                    $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
                    $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
                    $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
                    $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
                    $prodItemBonus = $this->input->post('prodItemBonus'.$i);
                    $probItemBonus = $this->input->post('probItemBonus'.$i);
                    $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
                    $idItemBonus = $this->input->post('idItemBonus'.$i);
                    if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
                        $this->Mpurchaseitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1,1);
                    }
                }
            }else{
                $different=0;
                $arrCompare=$this->Mpurchaseorderitem->getItemsByPurchaseID($POID);
                if(!empty($arrCompare)) {
                    for($i = 0; $i < count($arrCompare); $i++) {
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit1']);
                        $intConv1 = $convTemp[0]['unit_conversion'];
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit2']);
                        $intConv2 = $convTemp[0]['unit_conversion'];
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit3']);
                        $intConv3 = $convTemp[0]['unit_conversion'];
                        $quantity=(int)$arrCompare[$i]['proi_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['proi_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['proi_quantity3']*(int)$intConv3;
                        $purchased=(int)$arrCompare[$i]['proi_purchased1']*(int)$intConv1+(int)$arrCompare[$i]['proi_purchased2']*(int)$intConv2+(int)$arrCompare[$i]['proi_purchased3']*(int)$intConv3;
                        $different=(int)$quantity-(int)$purchased;
                    }
                }
                $arrCompare=$this->Mpurchaseorderitem->getBonusItemsByPurchaseID($POID);
                if(!empty($arrCompare)) {
                    for($i = 0; $i < count($arrCompare); $i++) {
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit1']);
                        $intConv1 = $convTemp[0]['unit_conversion'];
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit2']);
                        $intConv2 = $convTemp[0]['unit_conversion'];
                        $convTemp = $this->Munit->getConversion($arrCompare[$i]['proi_unit3']);
                        $intConv3 = $convTemp[0]['unit_conversion'];
                        $quantity=(int)$arrCompare[$i]['proi_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['proi_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['proi_quantity3']*(int)$intConv3;
                        $purchased=(int)$arrCompare[$i]['proi_purchased1']*(int)$intConv1+(int)$arrCompare[$i]['proi_purchased2']*(int)$intConv2+(int)$arrCompare[$i]['proi_purchased3']*(int)$intConv3;
                        $different=(int)$quantity-(int)$purchased;
                    }
                }
                if($different==0){
                    //sini ubah status order jadi 4
                    $this->load->model('Mpurchaseorder');
                    $this->Mpurchaseorder->editStatusByID($POID,4);
                }else{
                    if($arrPurchaseData['prch_status']==4){
                        $this->Mpurchaseorder->editStatusByID($POID,3);
                    } //sini
                }
            }

			// Edit finance data
			//$this->load->model('Mfinance'); $this->Mfinance->editAutomatic(2,$intID,$intTotalPrice);
			
		} else $this->Mpurchase->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));
		
		
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseupdated');
		
	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
		$this->Mpurchaseitem->deleteByPurchaseID($intID);
		$this->Mpurchase->deleteByID($intID);
		
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchasedeleted'));
		redirect('purchase/browse');
		
	}

    $this->load->model('Macceptance');
	$arrPurchaseData = $this->Mpurchase->getItemByID($intID);
	if(!empty($arrPurchaseData)) {
        $arrArrived = $this->Macceptance->getArrivedByPurchaseID($intID);
		$arrPurchaseData['prch_rawstatus'] = $arrPurchaseData['prch_status'];
		$arrPurchaseData['prch_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrPurchaseData['prch_status']);
		
		// Load the purchase item
		$arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
		$intPurchaseTotal = 0;
        $this->load->model('Munit');
        $this->load->model('Mpurchaseorderitem');
		for($i = 0; $i < count($arrPurchaseItem); $i++) {
            if($arrPurchaseData['prch_po_id']>0){
                $arrPurchaseItem[$i]['exist'] = $arrPurchaseItem[$i]['prci_exist'];
                $arrPurchaseItem[$i]['maxData'] = '';
                $arrPurchaseItem[$i]['maxData']['unit1'] = '';
                $arrPurchaseItem[$i]['maxData']['unit2'] = '';
                $arrPurchaseItem[$i]['maxData']['unit3'] = '';
                if($arrPurchaseItem[$i]['exist']=='1') {
                    $arrPurchaseItem[$i]['maxData'] = $this->Mpurchaseorderitem->getMaxItemsByPurchaseItemIDAndProdID($arrPurchaseData['prch_po_id'],$arrPurchaseItem[$i]['product_id']);
                    $arrPurchaseItem[$i]['maxData']['unit1'] = $this->Munit->getName($arrPurchaseItem[$i]['maxData']['proi_unit1']);
                    $arrPurchaseItem[$i]['maxData']['unit2'] = $this->Munit->getName($arrPurchaseItem[$i]['maxData']['proi_unit2']);
                    $arrPurchaseItem[$i]['maxData']['unit3'] = $this->Munit->getName($arrPurchaseItem[$i]['maxData']['proi_unit3']);
                }
            }
            $arrPurchaseItem[$i]['strName'] = $arrPurchaseItem[$i]['prci_description'].'|'.$arrPurchaseItem[$i]['proc_title'];

            $convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['prci_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['prci_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['prci_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];

            $intPurchaseTotal += $arrPurchaseItem[$i]['prci_subtotal'];
            $arrPurchaseItem[$i]['prci_conv1'] = $intConv1;
            $arrPurchaseItem[$i]['prci_conv2'] = $intConv2;
            $arrPurchaseItem[$i]['prci_conv3'] = $intConv3;
		}

        $arrPurchaseData['subtotal_discounted'] = $arrPurchaseData['prch_grandtotal'];
        $intPurchaseGrandTotal = (float) $arrPurchaseData['prch_grandtotal'] + ($arrPurchaseData['prch_grandtotal'] * (float) $arrPurchaseData['prch_tax'] / 100);

        $arrPurchaseItemBonus = $this->Mpurchaseitem->getBonusItemsByPurchaseID($intID);
        if(!empty($arrPurchaseItemBonus)) {
            for($i = 0; $i < count($arrPurchaseItemBonus); $i++) {
                if($arrPurchaseData['prch_po_id']>0){
                    $arrPurchaseItemBonus[$i]['exist'] = $arrPurchaseItemBonus[$i]['prci_exist'];
                    $arrPurchaseItemBonus[$i]['maxData'] = '';
                    $arrPurchaseItemBonus[$i]['maxData']['unit1'] = '';
                    $arrPurchaseItemBonus[$i]['maxData']['unit2'] = '';
                    $arrPurchaseItemBonus[$i]['maxData']['unit3'] = '';
                    if($arrPurchaseItemBonus[$i]['exist'] == '1') {
                        $arrPurchaseItemBonus[$i]['maxData'] = $this->Mpurchaseorderitem->getMaxBonusItemsByPurchaseItemIDAndProdID($arrPurchaseData['prch_po_id'],$arrPurchaseItemBonus[$i]['product_id']);
                        $arrPurchaseItemBonus[$i]['maxData']['unit1'] = $this->Munit->getName($arrPurchaseItemBonus[$i]['maxData']['proi_unit1']);
                        $arrPurchaseItemBonus[$i]['maxData']['unit2'] = $this->Munit->getName($arrPurchaseItemBonus[$i]['maxData']['proi_unit2']);
                        $arrPurchaseItemBonus[$i]['maxData']['unit3'] = $this->Munit->getName($arrPurchaseItemBonus[$i]['maxData']['proi_unit3']);
                    }
                }
                $convTemp = $this->Munit->getConversion($arrPurchaseItemBonus[$i]['prci_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrPurchaseItemBonus[$i]['prci_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrPurchaseItemBonus[$i]['prci_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrPurchaseItemBonus[$i]['strName'] = $arrPurchaseItemBonus[$i]['prci_description'].'|'.$arrPurchaseItemBonus[$i]['proc_title'];
                $arrPurchaseItemBonus[$i]['prci_conv1'] = $intConv1;
                $arrPurchaseItemBonus[$i]['prci_conv2'] = $intConv2;
                $arrPurchaseItemBonus[$i]['prci_conv3'] = $intConv3;
            }
        }

	} else {
		$arrPurchaseItem = array(); $arrPurchaseList = array();
	}

    $arrData = array(
        'intPurchaseID' => $intID,
        'intPurchaseTotal' => $intPurchaseTotal,
        'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
        'arrPurchaseItem' => $arrPurchaseItem,
        'arrPurchaseData' => $arrPurchaseData,
        'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
        'arrArrived' => $arrArrived,
        'numberTax' => $this->input->get('number'),
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        //$dataPrint=$this->Mpurchase->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchase',
        ), $arrData));
    } else if($this->input->get('print2') != '') { # print e pajak
        $dataPrint = $this->Mpurchase->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchasetax',
            'dataPrint' => $dataPrint,
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        ), $arrData));
    } else {
        # Load all other purchase data the user has ever made
        $arrPurchaseList = $this->Mpurchase->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'purchase/view',
            'arrPurchaseList' => $arrPurchaseList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchasestatus')
        ), $arrData, $this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus'])));
    }
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrPurchase = $this->Mpurchase->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('purchase/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";

    } else {
		$arrPagination['base_url'] = site_url("purchase/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Mpurchase->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrPurchase = $this->Mpurchase->getItems($intPage,$arrPagination['per_page']);
    }
    
    if($this->input->post('delPurchase') != ''){
        $idPurchase = $this->input->post('delPurchase');
        $hasil = $this->Mpurchase->checkBPB($idPurchase);
        if($hasil[0]['jumlah_data']){
            $this->session->set_flashdata('strMessage','Data digunakan oleh Purchase Invoice, tidak bisa dihapus');
        }
        else{
            $this->load->model('Mpurchaseorderitem');
            $arrPurchItem = $this->Mpurchaseorderitem->getAllPurchaseOrderItemById($idPurchase);
            //echo "<script>alert(".count($arrAccItem).");</script>";
            for($i=0;$i<count($arrPurcItem);$i++){
                //echo "<script>alert('".$arrAccItem[$i]['acit_arrived1']." ".$arrAccItem[$i]['acit_purchaseid']." ".$arrAccItem[$i]['product_id_baru']." ".$arrAccItem[$i]['prci_quantity1']."');</script>";
                $qty = $arrpurchItem[$i]['prci_quantity1'];
                //$arrived = $arrpurchItem[$i]['acit_arrived1'];
                //$prci_update = $qty+$arrived;
                $prod_id = $arrpurchItem[$i]['prci_product_id'];
                $purche_id = $arrpurchItem[$i]['prci_purchase_id'];
                $po_id = $arrPurchItem[$i]['prch_po_id'];
                echo "<script>alert($prci_lama);</script>";
                $this->load->model('Mpurchaseorderitem');
                $this->Mpurchaseorderitem->updatePurchaseOrderItemAfterDelPurch($prod_id, $qty, $po_id);
            }
            $this->load->model('Mpurchase');
            $this->Mpurchase->deleteByID($idPurchase);
            $this->load->model('Mpurchaseitem');
            $this->Mpurchaseitem->deleteByID($idPurchase);
            $this->session->set_flashdata('strMessage','Order pembelian dengan ID '.$idPurchase.' telah berhasil dihapus');
        }
		redirect('purchase/browse');
    }

	if(!empty($arrPurchase)) for($i = 0; $i < count($arrPurchase); $i++) {


		$arrPurchase[$i] = array_merge($arrPurchase[$i],$this->admlinklist->getMenuPermission(22,$arrPurchase[$i]['prch_status']));
		$arrPurchase[$i]['prch_rawstatus'] = $arrPurchase[$i]['prch_status'];
		$arrPurchase[$i]['prch_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrPurchase[$i]['prch_status']);
        $arrPurchase[$i]['prch_total'] = (float)$arrPurchase[$i]['prch_grandtotal']+((float)$arrPurchase[$i]['prch_grandtotal']*(float)$arrPurchase[$i]['prch_tax']/100);
	}

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'purchase/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrPurchase' => $arrPurchase,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase')
    ));
}

}

/* End of File */