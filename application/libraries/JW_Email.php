<?php

require_once(APPPATH.'libraries/Emogrifier.php');

class JW_Email extends CI_Email {

public $config;

# mail / sendmail / smtp
public function init($mode = 'sendmail') {
	$_CI =& get_instance();
	$_CI->load->config('jwemail');
	$config['useragent']		= $_CI->config->item('useragent');
	$config['charset']			= $_CI->config->item('charset');

	if($mode == 'mail' || $mode == 'sendmail') {
		$config['protocol']		= $mode;
	} else {
		$config['protocol']		= $_CI->config->item('protocol');
		$config['smtp_host']	= $_CI->config->item('smtp_host');
		$config['smtp_port']	= $_CI->config->item('smtp_port');
		$config['smtp_timeout']	= $_CI->config->item('smtp_timeout');
		$config['smtp_user']	= $_CI->config->item('smtp_user');
		$config['smtp_pass']	= $_CI->config->item('smtp_pass');
		$config['newline']		= $_CI->config->item('newline');
		$config['validate']		= $_CI->config->item('validate');
	}

	$this->config = $config;
}

public function sendEmail($arrOwnerInfo, $strEmailAddress, $strSubject, $strMessage, $strMode = 'html', $arrAttachment = array()) {
	if(constant('ENVIRONMENT') == 'development') return TRUE;

	if(empty($this->config)) $this->init(JW_SEND_EMAIL_MODE);

	$emogrifier = new \Pelago\Emogrifier();
	$emogrifier->setHtml($strMessage);
	$emogrifier->setCss(@file_get_contents('./asset/mail/layout.css'));
	$strMessage = $emogrifier->emogrify();

	$this->config['mailtype'] = $strMode;
	$this->initialize($this->config);

	# Header
	$this->from('no-reply@'.str_replace('www.','',$_SERVER['SERVER_NAME']), $arrOwnerInfo['strWebsiteName']);
	// $this->reply_to($arrOwnerInfo['strOwnerEmail'], $arrOwnerInfo['strOwnerName']); # Spam Indication
	$this->to($strEmailAddress);

	# Content
	$this->subject(mb_convert_encoding($strSubject, $this->config['charset']));
	$this->message(mb_convert_encoding($strMessage, $this->config['charset']));
	foreach($arrAttachment as $arrEach) $this->attach($arrEach);

	# Process
    if(!$this->send(FALSE)) {
    	return $this->print_debugger();
    } else {
    	$this->clear();
    	return TRUE;
    }
}

public function sendPushNotification($strTitle, $strMessage, $arrUserIDs = '', $strURL = '', $strImg = '') {
	$_CI =& get_instance();
	$_CI->load->config('jwemail');

	$arrFields = array(
		'app_id' => $_CI->config->item('pushnotification_api_id'),
		'headings' => array(
			'en' => $strTitle,
		),
		'contents' => array(
			'en' => $strMessage,
		),
	);

	if(!empty($arrUserIDs)) {
		if(!is_array($arrUserIDs)) $arrUserIDs = array($arrUserIDs);
		$arrFields['include_player_ids'] = $arrUserIDs;
	} else $arrFields['included_segments'] = array('All');
	if(!empty($strURL)) $arrFields['url'] = $strURL;
	if(!empty($strImg)) $arrFields['chrome_web_icon'] = $strImg;

	$ch = curl_init();
	$arrData = array(
	    CURLOPT_URL => 'https://onesignal.com/api/v1/notifications', 
	    CURLOPT_HTTPHEADER => array(
    		'Content-Type: application/json; charset=utf-8',
    		'X-Requested-With: XMLHttpRequest',
    		'Authorization: Basic '.$_CI->config->item('pushnotification_api_key')
    	),
    	CURLOPT_RETURNTRANSFER => TRUE,
    	CURLOPT_HEADER => FALSE, 
    	CURLOPT_POST => TRUE,
    	CURLOPT_POSTFIELDS => json_encode($arrFields),
    	CURLOPT_SSL_VERIFYPEER => FALSE
	);

	curl_setopt_array($ch, $arrData);
	$response = curl_exec($ch);

	curl_close($ch);
	return $response;
}

}

# End of file: JW_Encrypt.php
# Location: ./application/helpers/MY_Encrypt.php  