<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JW_Upload extends CI_Upload {

private $_config;

public function __construct($props = array()) {
	$this->_config = [
		'upload_path' 			=> 'upload/',
		'allowed_types' 		=> 'gif|jpg|jpeg|png',
		'max_size' 				=> 300,
		'max_dimension' 		=> FALSE,
		'thumbnail'				=> FALSE,
		'thumbnail_dimension'	=> [],
	];
	foreach($props as $i => $e) $this->_config[$i] = $e;

	if (!is_dir($this->_config['upload_path'])) mkdir($this->_config['upload_path'], 0777, TRUE);

	$this->_config['thumbnail_path'] = str_replace('upload/', 'thumb/', $this->_config['upload_path']);
	if (!is_dir($this->_config['thumbnail_path'])) mkdir($this->_config['thumbnail_path'], 0777, TRUE);

	parent::__construct($this->_config);
}

public function do_upload($field = 'userfile') {
	if(parent::do_upload($field)) {
		$data = $this->data();

		if(!empty($this->_config['max_dimension'])) {
			$dim = explode('x', $this->_config['max_dimension']);
			if(empty($dim[1])) $dim[1] = $dim[0];

			if($data['image_width'] > $dim[0] && $data['image_height'] > $dim[1])
				$this->do_thumbnail('self', $data, $this->_config['max_dimension']);
		}

		if(!empty($this->_config['thumbnail']) && !empty($this->_config['thumbnail_dimension'])) {
			$master_thumb = [0 => 0, 1 => 0];
			foreach($this->_config['thumbnail_dimension'] as $e) {
				$dim = explode('x', $e);
				if($dim[0] > $master_thumb[0]) $master_thumb[0] = $dim[0];
				if(!empty($dim[1]) & $dim[1] > $master_thumb[1]) $master_thumb[1] = $dim[1];
			}
			$this->do_thumbnail('thumb', $data, implode($master_thumb, 'x'), $this->_config['thumbnail_path']);

			foreach($this->_config['thumbnail_dimension'] as $e)
				$this->do_thumbnail($this->_config['thumbnail'], $data, $e, $this->_config['thumbnail_path']);
		}
		return $data;
	}else{
		return parent::display_errors();
	}
}

public function do_thumbnail($mode, &$source_image, $dimension, $new_path = '') {
	$_CI =& get_instance();
	$_CI->load->library('image_lib');
	$config = [
		'image_library'		=> 'gd2',
		'source_image'		=> $source_image['full_path'],
		'new_image'			=> $new_path,
	];
	$dim = explode('x', $dimension);
	$dim_width = intval($dim[0]);
	if(!empty($dim[1])) $dim_height = intval($dim[1]);
	else $dim_height = $dim_width;

	if($mode == 'resize') $config = array_merge($config, [
			'maintain_ratio'	=> TRUE,
			'create_thumb'		=> TRUE,
			'thumb_marker'		=> '-'.$dimension,
			'width'				=> $dim_width,
			'height'			=> $dim_height,
		]);

	elseif($mode == 'self') {
		$config = array_merge($config, [
			'maintain_ratio'	=> TRUE,
			'create_thumb'		=> FALSE,
			'thumb_marker'		=> '',
			'width'				=> $dim_width,
			'height'			=> $dim_height,
		]);
		$mode = 'resize';

	} elseif($mode == 'thumb') {
		$config = array_merge($config, [
			'maintain_ratio'	=> TRUE,
			'create_thumb'		=> TRUE,
			'thumb_marker'		=> '',
			'width'				=> $dim_width,
			'height'			=> $dim_height,
			'new_image'			=> $this->_config['thumbnail_path'].$source_image['file_name'],
		]);

		$source_image['file_path'] = str_replace('upload/', 'thumb/', $source_image['file_path']);
		$source_image['full_path'] = str_replace('upload/', 'thumb/', $source_image['full_path']);
		$mode = 'resize';

	} elseif($mode == 'crop') {
		$config = array_merge($config, [
			'maintain_ratio'	=> FALSE,
			'new_image'			=> $this->_config['thumbnail_path'].str_replace('.', '-'.$dimension.'.', $source_image['file_name']),
		]);

		if(intval($source_image['image_width']) < (intval($dim_width))) {
			$config['x_axis'] = 0;
			$config['width'] = intval($source_image['image_width']);
		} else {
			$config['x_axis'] = (intval($source_image['image_width']) / 2) - (intval($dim_width) / 2);
			$config['width'] = intval($dim_width);
		}
		if(intval($source_image['image_height']) < (intval($dim_height))) {
			$config['y_axis'] = 0;
			$config['height'] = intval($source_image['image_height']);
		} else {
			$config['y_axis'] = (intval($source_image['image_height']) / 2) - (intval($dim_height) / 2);
			$config['height'] = intval($dim_height);
		}
	}

	$_CI->image_lib->initialize($config);
	if($_CI->image_lib->$mode()) {
		if($mode != 'crop') {
			list($width, $height, $type, $attr) = getimagesize($source_image['full_path']);
			$source_image['image_width'] = $width;
			$source_image['image_height'] = $height;
		}
		return TRUE;
	}
	return FALSE;
}

public function do_delete($strPath) {
	if(file_exists($strPath) && empty(unlink($strPath))) return TRUE;
	return FALSE;
}

}

/* End of File */