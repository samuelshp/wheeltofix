<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class JW_Table {
# Private Variables
private $_CI;
private $_arrTableList, $_arrFieldList; # Never Call This Directly
private $_strTableName, $_intTableID;

public function __construct() {
	$this->_CI =& get_instance();
	$this->_arrTableList = array();
	$this->_arrFieldList = array();
	$this->_strTableName = '';
	$this->_intTableID = 0;
}

public function initialize($intTableID, $bolForceUpdate = FALSE) {
	# Initialize
	$arrTableList = $this->getTableProperties();
	if(empty($arrTableList) || $bolForceUpdate) {
		$this->_CI->load->model('Mdbvo','admtablelist');
		$this->_CI->admtablelist->initialize('adm_table_list');
		$this->_CI->admtablelist->dbSelect('',"id = $intTableID");
		$arrTableList = $this->_CI->admtablelist->getNextRecord('Array');
		$this->setTableProperties($arrTableList);
	}
	$this->_intTableID = $arrTableList['id'];
	$this->_strTableName = $arrTableList['title'];
	if(!empty($arrTableList['custom_query'])) $this->setQueryData($arrTableList['custom_query']);

	$arrFieldList = $this->getFieldProperties();
	if(empty($arrFieldList) || $bolForceUpdate) {
		$this->_CI->load->model('Mdbvo','admfieldlist');
		$this->_CI->admfieldlist->initialize('adm_field_list');
		$this->_CI->admfieldlist->dbSelect('',"table_id = $intTableID");
		$this->setFieldProperties($this->_CI->admfieldlist->getQueryResult('Array'));
	}
}

private function _initializeActiveTable($strLanguage = '') {
	$arrTableList = $this->getTableProperties();
	$arrFieldList = $this->getFieldProperties();
	$strTableName = $arrTableList['title'];
	if(!empty($strLanguage)) $strTableName .= '_'.$strLanguage;

	if(!$this->_CI->load->is_model_loaded('ActiveTable')) $this->_CI->load->model('Mdbvo','ActiveTable');
	if(!empty($arrTableList['custom_query'])) $bolTableExists = $this->_CI->ActiveTable->initialize($strTableName,$this->_arrTableList['custom_query']);
	else $bolTableExists = $this->_CI->ActiveTable->initialize($strTableName);

	return $bolTableExists;
}

public function getTableProperties() {
	if(empty($this->_arrTableList))
		$this->_arrTableList = $this->_CI->cache->{JW_CACHE_TYPE}->get('Table_'.$this->_intTableID);
	return $this->_arrTableList;
}
public function setTableProperties($arrData) {
	$this->_arrTableList = $arrData;
	return $this->_CI->cache->{JW_CACHE_TYPE}->save('Table_'.$arrData['id'], $arrData, JW_CACHE_EXP_1_DAY);
}

public function getFieldProperties($arrSelectedList = array()) {
	if(empty($this->_arrFieldList))
		$this->_arrFieldList = $this->_CI->cache->{JW_CACHE_TYPE}->get('Field_'.$this->_intTableID);

	if(!empty($arrSelectedList)) {
		$arrFieldList = array();
		foreach($this->_arrFieldList as $arrField) if(compareData($arrField['title'], $arrSelectedList)) $arrFieldList[] = $arrField;

	} else $arrFieldList = $this->_arrFieldList;

	return $arrFieldList;
}
public function setFieldProperties($arrData) {
	$this->_arrFieldList = $arrData;
	return $this->_CI->cache->{JW_CACHE_TYPE}->save('Field_'.$this->_intTableID, $arrData, JW_CACHE_EXP_1_DAY);
}

# Set to load custom table by adm_query_list
public function setQueryData($arrQueryData) { $this->_arrQueryData = $arrQueryData; }

public function getColumnData() {
	$this->_initializeActiveTable();

	return $this->_CI->ActiveTable->getColumnData();
}

public function getNumRows($strWhere = '') {
	$this->_initializeActiveTable();

	$this->_CI->ActiveTable->dbSelect('id',$strWhere);
	return $this->_CI->ActiveTable->getNumRows();
}

public function getSelectedData($strSelect = '',$strWhere = '',$strOrderBy = '',$intNumRows = '',$intLimitOffset = '',$bolForceQueryResult = false) {
	$this->_initializeActiveTable();

	$this->_CI->ActiveTable->dbSelect($strSelect,$strWhere,$strOrderBy,$intNumRows,$intLimitOffset);
	if($this->_CI->ActiveTable->getNumRows() > 1 || $bolForceQueryResult) return $this->_CI->ActiveTable->getQueryResult('Array');
	else if($this->_CI->ActiveTable->getNumRows() == 1) return $this->_CI->ActiveTable->getNextRecord('Array');
	else return false;
}

public function getData($strWhere = '',$strOrderBy = '',$intNumRows = '',$intLimitOffset = '',$bolForceQueryResult = false) {
	$this->_initializeActiveTable();
	$arrTableList = $this->getTableProperties();
	$arrCIData = $this->_CI->getArrData();

	$this->_CI->ActiveTable->dbSelect('',$strWhere,$strOrderBy,$intNumRows,$intLimitOffset);

	if($this->_CI->ActiveTable->getNumRows() > 1 || $bolForceQueryResult) {
		$arrData = $this->_CI->ActiveTable->getQueryResult('Array');

		# Apply for other language
		foreach ($arrCIData['arrLangAvlb'] as $e) if($this->_CI->config->item('jw_language') != $e['strKey']) 
			if($this->_CI->ActiveTable->checkTableExists($arrTableList['title'].'_'.$e['strData2'])) {

			$this->_initializeActiveTable($e['strData2']);
			$this->_CI->ActiveTable->dbSelect('',$strWhere,$strOrderBy,$intNumRows,$intLimitOffset);

			$arrLanguageData = $this->_CI->ActiveTable->getQueryResult('Array');

			for ($i=0; $i < count($arrData); $i++) for($j = 0; $j < count($arrLanguageData); $j++) 
				if($arrData[$i]['id'] == $arrLanguageData[$j]['id']) {
				$arrChangedLanguageData = array();
				foreach($arrLanguageData[$j] as $k => $e2) if($k != 'id') # Translate field key according to language
					$arrChangedLanguageData[$k.'_'.$e['strData2']] = $e2;

				$arrData[$i] = array_merge($arrData[$i],$arrChangedLanguageData);

				break;
			}
		}

	} else if($this->_CI->ActiveTable->getNumRows() == 1) {
		$arrData = $this->_CI->ActiveTable->getNextRecord('Array');

		# Apply for other language
		foreach ($arrCIData['arrLangAvlb'] as $e) if($this->_CI->config->item('jw_language') != $e['strKey'])
			if($this->_CI->ActiveTable->checkTableExists($arrTableList['title'].'_'.$e['strData2'])) {

			$this->_initializeActiveTable($e['strData2']);
			$this->_CI->ActiveTable->dbSelect('',$strWhere,$strOrderBy,$intNumRows,$intLimitOffset);

			$arrLanguageData = $this->_CI->ActiveTable->getNextRecord('Array');

			$arrChangedLanguageData = array();
			foreach($arrLanguageData as $k => $e2) if($k != 'id') # Translate field key according to language
				$arrChangedLanguageData[$k.'_'.$e['strData2']] = $e2;

			$arrData = array_merge($arrData,$arrChangedLanguageData);
		}

	} else $arrData = false;

	return $arrData;
}

public function getQueryData($strQuery,$bolForceQueryResult = false) {
	$this->_initializeActiveTable();

	$this->_CI->ActiveTable->setQuery($strQuery);

	if($this->_CI->ActiveTable->getNumRows() > 1 || $bolForceQueryResult) return $this->_CI->ActiveTable->getQueryResult('Array');
	else if($this->_CI->ActiveTable->getNumRows() == 1) return $this->_CI->ActiveTable->getNextRecord('Array');
	else return false;
}

public function getForeignData($intDataID = 0) {
	$this->_initializeActiveTable();
	$arrTableList = $this->getTableProperties();

	if(!empty($intDataID)) {
		$this->_CI->ActiveTable->dbSelect($arrTableList['fk_display_fields'],"id = '$intDataID'");
		return $this->_CI->ActiveTable->getNextRecord('Array');
	} else {
		$this->_CI->ActiveTable->dbSelect($arrTableList['fk_display_fields']);
		return $this->_CI->ActiveTable->getQueryResult('Array');
	}
}

# This function must process multiple data
public function getReadableFields($arrData) {
	$arrFieldList = $this->getFieldProperties();
	$arrCIData = $this->_CI->getArrData();

	# Get Fields in arrData
	foreach($arrFieldList as $e) {
		if($e['field_type'] == 'SELECT' && is_numeric($e['description'])) {
			$mixTableData = translateDataIntoReadableStatements($e,array_column($arrData,$e['title']));
			foreach($arrData as $j => $f)
				if(!empty($f[$e['title']]) && !empty($mixTableData[$f[$e['title']]])) $arrData[$j][$e['title']] = $mixTableData[$f[$e['title']]];
				else $arrData[$j][$e['title']] = '';

		} else foreach($arrData as $j => $f) if(isset($f[$e['title']])) {
			$f[$e['title']] = translateDataIntoReadableStatements($e,$f[$e['title']]);

			# Apply for other language
			foreach($arrCIData['arrLangAvlb'] as $g) if(!empty($f[$e['title'].'_'.$g['strData2']])) {
				if(
					($e['field_type'] == 'TEXT' && compareData($e['validation'],array(''))) ||
					($e['field_type'] == 'TEXTAREA' && compareData($e['validation'],array('HTMLEDITOR','LIST','DESCRIPTION','')))
				) { # Filter fields
					$e2 = $e;
					$e2['title'] = $e['title'].'_'.$g['strData2'];
					$f[$e2['title']] = translateDataIntoReadableStatements($e2,$f[$e2['title']]);

				} else unset($f[$e['title'].'_'.$g['strData2']]);
			}

			$arrData[$j] = $f;
		}
	}

	return $arrData;
}

# This function can't process multiple data, must process one by one
public function getHTMLFields($arrData = array(),$seq = -1) {
if(!function_exists('___translate')) {
	function ___translate($arrField,$arrData,$seq = -1) {
		# Edit
		if($seq >= 0) {
			if($arrField['field_type'] == 'TEXTAREA' && in_array($arrField['validation'], array('CHECKBOXLIST', 'MULTIPLESELECT')))
				return translateDataIntoHTMLStatements($arrField,$arrData,$seq);
			elseif($arrField['field_type'] == 'TEXTAREA' && in_array($arrField['validation'], array('MULTIVALUE', 'LIST')))
				return translateDataIntoHTMLStatements($arrField,$arrData[$arrField['title']],$seq);
			elseif(isset($arrData[$arrField['title']]))
				return translateDataIntoHTMLStatements($arrField,$arrData[$arrField['title']],$seq);
			else return translateDataIntoHTMLStatements($arrField,'',$seq);
		# Add
		} elseif(isset($arrData[$arrField['title']]))
			return translateDataIntoHTMLStatements($arrField,$arrData[$arrField['title']]);
		else return translateDataIntoHTMLStatements($arrField);
	}
} # End of ___translate
	$arrFieldList = $this->getFieldProperties();
	$arrCIData = $this->_CI->getArrData();

	foreach($arrFieldList as $arrField) {
		$arrData[$arrField['title']] = ___translate($arrField,$arrData,$seq);

		# Apply for other language
		if($seq >= 0) foreach($arrCIData['arrLangAvlb'] as $e) if(!empty($arrData[$arrField['title'].'_'.$e['strData2']])) {
			if(
				($arrField['field_type'] == 'TEXT' && compareData($arrField['validation'],array(''))) ||
				($arrField['field_type'] == 'TEXTAREA' && compareData($arrField['validation'],array('HTMLEDITOR','LIST','DESCRIPTION','')))
			) { # Filter fields
				$arrFieldTemp = $arrField;
				$arrFieldTemp['title'] = $arrField['title'].'_'.$e['strData2'];
				$arrData[$arrFieldTemp['title']] = ___translate($arrFieldTemp,$arrData,$seq);
			} else {
				unset($arrData[$arrField['title'].'_'.$e['strData2']]);
			}
		}
	}

	return $arrData;
}

public function buildSearchQuery($strSearchKey,$strSearchBy) {
	if(empty($strSearchBy) || empty($strSearchKey)) return false;
	$arrTableList = $this->getTableProperties();
	$arrFieldList = $this->getFieldProperties();

	foreach($arrFieldList as $e) if($e['title'] == $strSearchBy) {
		$arrFieldList = $e; break;
	}

	$strWhere = '';
	if($arrFieldList['field_type'] == 'SELECT' && is_numeric($arrFieldList['description']) === TRUE ||
		($arrFieldList['field_type'] == 'TEXTAREA' && ($arrFieldList['validation'] == 'MULTIPLESELECT' || $arrFieldList['validation'] == 'CHECKBOXLIST'))
	) { # Select Table
		# Get where statements
		$this->_CI->load->model('Mdbvo','foreignTable');
		if(!empty($arrTableList['fk_query']))
			$this->_CI->foreignTable->initialize($arrTableList['title'],$arrTableList['fk_query']);
		else $this->_CI->foreignTable->initialize($arrTableList['title']);
		$this->_CI->foreignTable->dbSelect('strKey',"strData LIKE '%$strSearchKey%'");
		$arrForeignTable = $this->_CI->foreignTable->getQueryResult('Array');

		if($arrFieldList['field_type'] == 'SELECT' && !empty($arrForeignTable)) {
			foreach($arrForeignTable as $e) if(empty($strWhere)) $strWhere .= $e['strKey'];
			else $strWhere .= ', '.$e['strKey'];
			$strWhere = "$strSearchBy IN ($strWhere)";

		} else if($arrFieldList['field_type'] == 'TEXTAREA' && ($arrFieldList['validation'] == 'MULTIPLESELECT' || $arrFieldList['validation'] == 'CHECKBOXLIST') && !empty($strWhere)) {
			foreach($arrForeignTable as $e) if(empty($strWhere)) $strWhere .= '[[:<:]]'.$e['strKey'].'; ';
			else $strWhere .= '|[[:<:]]'.$e['strKey'].'; ';

			$strWhere = "CONCAT($strSearchBy,'; ') REGEXP '$strWhere'"; 

		}

	} else if($arrFieldList['field_type'] == 'SELECT' && is_numeric($arrFieldList['description']) === FALSE) { # Foreign key is a tinydb

		$this->_CI->load->model('Mtinydbvo','foreignTinyDB');
		$this->_CI->foreignTinyDB->initialize($arrFieldList['description']);
		$arrFK = $this->_CI->foreignTinyDB->getAllData();

		# Get where statements
		$strWhere = '';
		foreach($arrFK as $e) if(stristr($e['strData'],$strSearchKey) !== FALSE)
			if(empty($strWhere)) $strWhere = $e['strKey'];
			else $strWhere .= ', '.$e['strKey'];

		if(!empty($strWhere)) $strWhere = "$strSearchBy IN ($strWhere)"; 
		else $strWhere = '1 = 0';

	} else if($arrFieldList['field_type'] == 'TEXT' && compareData($arrFieldList['validation'], array('DATETIME', 'DATE', 'TIME'))) {
		if($strSecondKey = $this->_CI->input->get('second_'.$strSearchBy)) $strWhere = "$strSearchBy >= '$strSearchKey' AND $strSearchBy <= '$strSecondKey'";
		elseif($arrFieldList['validation'] == 'DATE') $strWhere = "$strSearchBy >= '$strSearchKey 00:00:00' AND $strSearchBy <= '$strSearchKey 23:59:59'";
		elseif($arrFieldList['validation'] == 'TIME') $strWhere = "$strSearchBy LIKE '% $strSearchKey'";
		else $strWhere = "$strSearchBy = '$strSearchKey'";

	} else { # Text / Textarea
		$strWhere = "$strSearchBy LIKE '%$strSearchKey%'";
	}

	return $strWhere;
}

public function publish($arrData) {
	$this->_initializeActiveTable();
	$arrFieldList = $this->getFieldProperties();

	$arrInsertData = array();
	foreach($arrFieldList as $arrField) if(isset($arrData[$arrField['title']])) {
		// if($arrData[$arrField['title']] !== '' && $arrField['field_type'] == 'PASSWORD') continue;
		$arrInsertData[$arrField['title']] = translateDataIntoSafeStatements($arrField,$arrData[$arrField['title']],'add');
	}

	$this->_CI->session->set_flashdata('strMessage',"Data added!");
	return $this->_CI->ActiveTable->dbInsert($arrInsertData);
}

public function save($arrDataID,$arrData,$intRecordCount = -1) {
	$arrFieldList = $this->getFieldProperties();
	$arrCIData = $this->_CI->getArrData();

	if(!is_array($arrDataID)) {
		$arrDataID = array(0 => $arrDataID);
		foreach($arrData as $strKey => $strData) $arrData[$strKey.'0'] = $strData;
	}
	if($intRecordCount == -1) $intRecordCount = 1;

	# Apply for all language
	$intDataEdited = 0;
	foreach($arrCIData['arrLangAvlb'] as $e) {
		if($this->_CI->config->item('jw_language') != $e['strKey']) $bolTableExists = $this->_initializeActiveTable($e['strData2']);
		else $bolTableExists = $this->_initializeActiveTable();

		# Processing data
		if($bolTableExists) for($i = 0; $i < $intRecordCount; $i++) if(!empty($arrDataID[$i])) {
			$arrUpdateData = array();

			foreach($arrFieldList as $arrField) if(isset($arrData[$arrField['title'].$i])) {
				// if($arrData[$arrField['title'].$i] !== '' && $arrField['field_type'] == 'PASSWORD') continue;
				if($this->_CI->config->item('jw_language') != $e['strKey']) 
					$strLanguageFieldName = $arrField['title'].'_'.$e['strData2'];
				else $strLanguageFieldName = $arrField['title'];
				# If data not available in other language use default
				# Because not all field type can be edited in multilanguage
				if(!empty($arrData[$strLanguageFieldName.$i])) $strUpdateData = $arrData[$strLanguageFieldName.$i];
				else $strUpdateData = $arrData[$arrField['title'].$i];

				# If edit mode and password is empty, it means that the user don't want to change the password
				# The password isn't visible in the edit form
				if($arrField['field_type'] != 'PASSWORD')
					$arrUpdateData[$arrField['title']] = translateDataIntoSafeStatements($arrField,$strUpdateData,'edit');
				elseif($strUpdateData != '')
					$arrUpdateData[$arrField['title']] = translateDataIntoSafeStatements($arrField,$strUpdateData,'edit');
			}

			$intDataEdited += $this->_CI->ActiveTable->dbUpdate($arrUpdateData,"id = '".$arrDataID[$i]."'");
		}
	}

	$this->_CI->session->set_flashdata('strMessage',"$intDataEdited data edited!");
	return $intDataEdited;
}

public function delete($arrDataID) {
	$arrTableList = $this->getTableProperties();

	if(!is_array($arrDataID)) $arrDataID = array($arrDataID);

	# Look for another table that use the table deleted as foreign key
	$this->_CI->load->model('Mdbvo','admTableList');
	$this->_CI->admTableList->initialize('adm_table_list');
	$this->_CI->load->model('Mdbvo','admFieldList');
	$this->_CI->admFieldList->initialize('adm_field_list');
	$this->_CI->load->model('Mdbvo','admForeignTable');

	# Search for the SELECT type
	$this->_CI->admFieldList->dbSelect('table_id,title',"field_type = 'SELECT' AND description = '".$arrTableList['id']."'");
	if($this->_CI->admFieldList->getNumRows() > 0) {
		# Search for table name
		$arrFieldList = $this->_CI->admFieldList->getQueryResult('Array');

		foreach($arrFieldList as $e) {
			$this->_CI->admTableList->dbSelect('title',"id = '".$e['table_id']."'");

			if($this->_CI->admTableList->getNumRows() > 0) {
				$strTableName = $this->_CI->admTableList->getNextRecord('Object')->title;
				$this->_CI->admForeignTable->initialize($strTableName);

				foreach($arrDataID as $e3) $this->_CI->admForeignTable->dbDelete($e['title']." = '$e3'");

			}
		}
	}

	# Search for the MULTIPLESELECT and CHECKBOXLIST type
	$arrFieldList = $this->getFieldProperties();
	for($i = 0; $i < count($arrFieldList); $i++) if(!empty($arrFieldList[$i]['id'])) if($i == 0) $strFieldList = $arrFieldList[$i]['id'];
	else $strFieldList .= ','.$arrFieldList[$i]['id'];

	$this->_CI->admFieldList->dbSelect('table_id,title',"field_type = 'TEXTAREA' AND validation IN ('MULTIPLESELECT','CHECKBOXLIST') AND description IN ($strFieldList)");
	if($this->_CI->admFieldList->getNumRows() > 0) {
		# Search for table name
		$arrFieldList = $this->_CI->admFieldList->getQueryResult('Array');

		foreach($arrFieldList as $e) {
			$this->_CI->admTableList->dbSelect('title',"id = '".$e['table_id']."'");

			if($this->_CI->admTableList->getNumRows() > 0) {
				$strTableName = $this->_CI->admTableList->getNextRecord('Object')->title;
				$this->_CI->admForeignTable->initialize($strTableName);

				foreach($arrDataID as $e3) {
					$this->_CI->admForeignTable->dbSelect('id,'.$e['title'],"CONCAT(".$e['title'].",'; ') LIKE '%$e3; %'");
					$arrForeignTable = $this->_CI->admForeignTable->getQueryResult('Array');

					foreach($arrForeignTable as $e2) if(!empty($e2[$e['title']])) {
						$strFieldResult = translateDataIntoSafeStatements(
							array('field_type' => 'TEXTAREA','validation' => 'MULTIPLESELECT'),
							str_replace($e3.'; ','',$e2[$e['title']].'; '),
							'edit');

						if(empty($strFieldResult)) $this->_CI->admForeignTable->dbDelete("id = '".$e2['id']."'");
						else $this->_CI->admForeignTable->dbUpdate(array($e['title'] => $strFieldResult),"id = '".$e2['id']."'");
					}
				}
			}
		}
	}

	# Delete the data
	$this->_initializeActiveTable();
	$strDataID = implode(',',$arrDataID);
	$this->_CI->session->set_flashdata('strMessage',"Data (and other data dependant on it) deleted!");
	return $this->_CI->ActiveTable->dbDelete("id IN ($strDataID)");
}

}

/* End of File */