<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'core/REST_Controller.php';

class JW_REST_Controller extends REST_Controller {

protected $_arrData = array();
protected $_strLastQuery;

public function __construct() {
	parent::__construct();

	if($this->uri->segment(2) != 'my' && $this->uri->segment(3) != 'login') {
		if($this->query('h') != '') {
			$this->load->model('Mdbvo', 'Madmlogin');
			$this->Madmlogin->initialize('adm_login');
			$this->Madmlogin->dbSelect('', 'adlg_login = "'.$this->query('h').'"');
			if($this->_arrData['arrMember'] = $this->Madmlogin->getNextRecord('Array')) {
				if($this->session->userdata('strAdminUserName') == '' || $this->session->userdata('strAdminUserName') != $this->query('h'))
					$this->session->set_userdata('strAdminUserName', $this->query('h'));
			} else $this->response([
			    'status' => FALSE,
			    'message' => 'Wrong hash key'
			], REST_Controller::HTTP_NOT_FOUND);

		} elseif($this->query('token') != '') {
			if(!empty(session_id($this->query('token')))) {
				session_destroy();
				session_id($this->query('token'));
				session_start();
			} else $this->response([
			    'status' => FALSE,
			    'message' => 'Wrong token key'
			], REST_Controller::HTTP_NOT_FOUND);

		} else $this->response([
		    'status' => FALSE,
		    'message' => 'no key provided'
		], REST_Controller::HTTP_NOT_FOUND);
	}

}

public function setArrData($arrData) { $this->_arrData = $arrData; }
public function getArrData() { return $this->_arrData; }

protected function _showResponse($bolSuccess, $arrData = []) {
	$arrResponse = [
	    'success' => $bolSuccess,
	];
	if($this->query('debug')) $arrResponse['sql_query'] = str_replace(PHP_EOL, ' ', $this->_strLastQuery);
	$this->response(
		array_merge($arrResponse, $arrData),
		$bolSuccess ? REST_Controller::HTTP_OK : REST_Controller::HTTP_OK );
}

}

/* End of File */