<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JW_Lang extends CI_Lang {
	
public function __construct() {
	parent::__construct();
}

public function jw() {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','MLanguage');
	$_CI->MLanguage->initialize('language');

	$arrArgs = func_get_args();
	$strKey = array_shift($arrArgs);

	# Ada kemungkinan key kembar. Karena itu ada identifier berupa sector
	# Format: [SECTOR]-[key]
	if(strpos($strKey,'-') > 0) {
		$arrKey = explode('-',$strKey);
		if(count($arrKey) == 2) { $strCategory = ''; $strSector = $arrKey[0]; $strKey = $arrKey[1]; } 
		else if(count($arrKey) == 3) { $strCategory = $arrKey[0]; $strSector = $arrKey[1]; $strKey = $arrKey[2]; }
	} else {
		$strCategory = ''; $strSector = '';
	}

	# Tentukan bahasa
	if($_CI->uri->segment(1) == 'adminpage') {
		$strLanguage = $_CI->session->userdata('jw_admin_language');
		if(!empty($strLanguage)) $strLanguage = 'lang_'.$strLanguage;
		else $strLanguage = 'lang_indonesia';
		if(empty($strCategory)) $strCategory = 'ADM';
	} else {
		$strLanguage = $_CI->session->userdata('jw_language');
		if(!empty($strLanguage)) $strLanguage = 'lang_'.$strLanguage;
		else $strLanguage = 'lang_indonesia';
		if(empty($strCategory)) $strCategory = 'UI';
	}

	# Query
	if(!empty($strCategory)) {
		switch($strCategory) {
			case 'ADM': $strWhereCategory = " AND lang_category IN (0,1)"; break;
			case 'UI': $strWhereCategory = " AND lang_category IN (0,2)"; break;
			default: $strWhereCategory = ""; break;
		}

	} else $strWhereCategory = '';

	if(!empty($strSector)) $strWhereSector = " AND lang_sector = '$strSector'";
	else $strWhereSector = '';

	$_CI->MLanguage->setQuery(
"SELECT $strLanguage AS lang
FROM language
WHERE lang_key = \"$strKey\"{$strWhereSector}{$strWhereCategory}");

	# Proses
	if($_CI->MLanguage->getNumRows() > 0) {
		$strLang = $_CI->MLanguage->getNextRecord('Object')->lang;
		if(empty($strLang)) $strLang = $strKey;

		$intCount = substr_count($strLang, '%s');
		for ($i=0; $i < $intCount; $i++) { 
			$intPos = strpos($strLang,'%s');
			if(!empty($arrArgs[$i])) $strLang = substr($strLang,0,$intPos).$arrArgs[$i].substr($strLang,$intPos + 2);
			else $strLang = substr($strLang,0,$intPos).''.substr($strLang,$intPos + 2);
		}

		if(!empty($strLang)) return $strLang;

	} else {
		switch ($strCategory) {
			case 'ADM': $strCategory = 1; break;
			case 'UI': $strCategory = 2; break;
			default: $strCategory = 0; break;
		}

		$_CI->MLanguage->dbInsert(array(
			'lang_category' => $strCategory,
			'lang_sector' => $strSector,
			'lang_key' => $strKey,
		));
	}
	return $strKey;
}

}