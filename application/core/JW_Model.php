<?php

class JW_Model extends CI_Model {
# Private Variables
private $_intLastInsertID, $_intLastAffectedRows;
private $_strTableName, $_strQuery, $_strCustomQuery, $_strPK;
private $_arrColumnData;
private $_mixQueryResult;

# Constructor
public function __construct() {
	parent::__construct();
	$this->_intLastInsertID = 0;
	$this->_intLastAffectedRows = 0;
	$this->_strTableName = '';
	$this->_strPK = '';
	$this->_strQuery = '';
	$this->_strCustomQuery = '';
	$this->_mixQueryResult = '';
	$this->_arrColumnData = array();
}

# For Mdbvo initialization, generally must be called after the declaration to define the table name
public function initialize($strTableName,$strCustomQuery = '') {
	$this->_intLastInsertID = 0;
	$this->_intLastAffectedRows = 0;

	$this->_setTableName($strTableName);

	if(!empty($strCustomQuery)) $this->_strCustomQuery = $strCustomQuery;
	else $this->_strCustomQuery = '';

	# Search for Table Keys setting
	if($this->db->table_exists($strTableName)) {
		$this->_setColumnData();
		return TRUE;
	}

	return FALSE;
}

# Getter and Setter for the Mdbvo private variables

# Getter and Setter for the Table Object's Name
public function getTableName() { return $this->_strTableName; }

private function _setTableName($strTableName) { $this->_strTableName = $strTableName; }

# Do The customized query string
public function setQuery($sql, $binds = FALSE, $return_object = TRUE) {
	$this->_mixQueryResult = $this->db->query($sql, $binds, $return_object);
}

# Retreive a query string as a string query
public function getQueryString() { return $this->_strQuery = $this->db->last_query(); }

# Retreive the query result depending on the parameter
# Param = "Array" To get query result as array
# Param = "Object" To get query result as class object
# Param = default To get query as query resource
public function getQueryResult($param = '') { 
	switch ($param) {
		case 'Array' : 
		case 'Arr' : 
			return $this->_mixQueryResult->result_array();
		case 'Object' : 
		case 'Obj' : 
			return $this->_mixQueryResult->result_object();
		default : 
			return $this->_mixQueryResult;
	}
}

# Getter and Setter of the table's fields parameter
# [Field, Type, Null, Key, Default, Extra]
public function getColumnData() { return $this->_arrColumnData; }
private function _setColumnData() {
	$this->setQuery('SHOW COLUMNS FROM `'.$this->getTableName().'`');
	# Fill thie ColumnData
	$this->_arrColumnData = $this->getQueryResult('Array'); # setColumnData
	# Search for the Primary Key
	foreach($this->_arrColumnData as $e) if($e['Key'] == 'PRI') {
		$this->_strPK = $e['Field']; # setStrPK
		break;
	}
}

public function dbSelect($strSelect = '',$strWhere = '',$strOrderBy = '',$intNumRows = '',$intLimitOffset = '',$arrJoin = array()) {
	$_CI =& get_instance();

	# SELECT
	if(empty($strSelect)) $strSelect = '*';

	# WHERE
	if(empty($strWhere))
		if(!empty($arrJoin)) $strWhere = 't.'.$this->_strPK.' > 0';
		else $strWhere = $this->_strPK.' > 0';

	# ORDER
	if(empty($strOrderBy))
		if(!empty($arrJoin)) $strOrderBy = 't.'.$this->_strPK.' ASC';
		else $strOrderBy = $this->_strPK.' ASC';

	# LIMIT
	if(empty($intNumRows)) $strLimit = '0,18446744073709551615';
	else if(!empty($intLimitOffset)) $strLimit = $intLimitOffset.','.$intNumRows;
		else $strLimit = '0,'.$intNumRows;

	# TABLE
	$arrCIData = $_CI->getArrData();
	$strTableName = $this->getTableName();

	if($this->uri->segment(1) != 'adminpage' && $this->session->userdata('jw_language') != $this->config->item('jw_language') && !empty($arrCIData['arrLangAvlb'])) {
		foreach($arrCIData['arrLangAvlb'] as $e) if($e['strKey'] == $this->session->userdata('jw_language')) {
			if($this->checkTableExists($this->getTableName().'_'.$e['strData2']))
				$strTableName = $this->getTableName().'_'.$e['strData2'];
			break;
		}
	}

	if(!empty($arrJoin)) $strTableName .= ' AS t';
	# Do The Select Query
	if(!empty($this->_strCustomQuery))
		$this->setQuery(str_replace(
			array('<jw:sql_select>','<jw:sql_table>','<jw:sql_join>','<jw:sql_where>','<jw:sql_order>','<jw:sql_limit>'), #Search
			array($strSelect,$strTableName,implode(PHP_EOL,$arrJoin),$strWhere,$strOrderBy,$strLimit), #Replace
			$this->_strCustomQuery));
	else $this->setQuery("
SELECT $strSelect
FROM $strTableName".PHP_EOL.
implode(PHP_EOL,$arrJoin).PHP_EOL.
"WHERE $strWhere
ORDER BY $strOrderBy
LIMIT $strLimit");
}

public function dbCount($strWhere = '') {
	$_CI =& get_instance();

	# WHERE
	if(empty($strWhere))
		if(!empty($arrJoin)) $strWhere = 't.'.$this->_strPK.' > 0';
		else $strWhere = $this->_strPK.' > 0';

	# TABLE
	$arrCIData = $_CI->getArrData();
	$strTableName = $this->getTableName();

	if($this->uri->segment(1) != 'adminpage' && $this->session->userdata('jw_language') != $this->config->item('jw_language') && !empty($arrCIData['arrLangAvlb'])) {
		foreach($arrCIData['arrLangAvlb'] as $e) if($e['strKey'] == $this->session->userdata('jw_language')) {
			if($this->checkTableExists($this->getTableName().'_'.$e['strData2']))
				$strTableName = $this->getTableName().'_'.$e['strData2'];
			break;
		}
	}

	# Do The Select Query
	if(!empty($this->_strCustomQuery))
		$this->setQuery(str_replace(
			array('<jw:sql_select>','<jw:sql_table>','<jw:sql_where>','ORDER BY <jw:sql_order>','LIMIT <jw:sql_limit>'), #Search
			array('COUNT(*) AS c',$strTableName,$strWhere,'',''), #Replace
			$this->_strCustomQuery));

	else $this->setQuery(
"SELECT COUNT(*) AS c
FROM $strTableName
WHERE $strWhere");

	return $this->getNextRecord('Object')->c;
}

public function dbInsert($arrInsertedData) {
	$_CI =& get_instance();

	# Add created by and created date data
	foreach($this->_arrColumnData as $e) if($e['Field'] == 'cby' && !isset($arrInsertedData['cby'])) {
		$this->setQuery("SELECT id FROM adm_login WHERE adlg_login = '".$_CI->session->userdata('strAdminUserName')."'");

		if($this->getNumRows() > 0) $arrInsertedData = array_merge($arrInsertedData,array('cby' => $this->getNextRecord('Object')->id));

	} else if($e['Field'] == 'cdate' && !isset($arrInsertedData['cdate'])) $arrInsertedData = array_merge($arrInsertedData,array('cdate' => date('Y-m-d H:i:s')));

	foreach($arrInsertedData as $i => $e) {
		if(is_array($e) && count($e) == 2) $this->db->set($i, $e[0], $e[1]);
		else $this->db->set($i, $e);
	}
	$this->db->insert($this->getTableName());
	$intInsertID = $this->db->insert_id();

	if(constant('ENVIRONMENT') == 'production' && $this->checkTableExists('sql_log'))
		$this->db->insert('sql_log', array(
			'sqlg_text' => $this->db->last_query(),
			'sqlg_result' => $intInsertID
		));

	# Apply for other language
	$arrCIData = $_CI->getArrData();
	if(!empty($arrCIData['arrLangAvlb'])) foreach($arrCIData['arrLangAvlb'] as $e) if($this->config->item('jw_language') != $e['strKey']) 
		if($this->checkTableExists($this->getTableName().'_'.$e['strData2'])) {
		$arrInsertedData['id'] = $intInsertID;
		foreach($arrInsertedData as $i => $e) {
			if(is_array($e) && count($e) == 2) $this->db->set($i, $e[0], $e[1]);
			else $this->db->set($i, $e);
		}
		$this->db->insert($this->getTableName().'_'.$e['strData2']);
	}

	$this->_intLastInsertID = $intInsertID;
	return $this->getInsertID();
}
public function dbUpdate($arrUpdatedData,$strWhere) {
	$_CI =& get_instance();

	# Add modified by and modified date data
	foreach($this->_arrColumnData as $e) if($e['Field'] == 'mby' && !isset($arrUpdatedData['mby'])) {
		$this->setQuery("SELECT id FROM adm_login WHERE adlg_login = '".$_CI->session->userdata('strAdminUserName')."'");

		if($this->getNumRows() > 0) $arrUpdatedData = array_merge($arrUpdatedData,array('mby' => $this->getNextRecord('Object')->id));

	} else if($e['Field'] == 'mdate' && !isset($arrUpdatedData['mdate'])) $arrUpdatedData = array_merge($arrUpdatedData,array('mdate' => date('Y-m-d H:i:s')));

	foreach($arrUpdatedData as $i => $e) {
		if(is_array($e) && count($e) == 2) $this->db->set($i, $e[0], $e[1]);
		else $this->db->set($i, $e);
	}
	$this->db->where($strWhere,NULL,FALSE);
	$this->db->update($this->getTableName());
	$this->_intLastAffectedRows = $this->db->affected_rows();

	if(constant('ENVIRONMENT') == 'production' && $this->checkTableExists('sql_log'))
		$this->db->insert('sql_log', array(
			'sqlg_text' => $this->db->last_query(),
			'sqlg_result' => $this->_intLastAffectedRows
		));

	return $this->getAffectedRows();
}

public function dbDelete($strWhere) {
	$_CI =& get_instance();

	$this->db->where($strWhere,NULL,FALSE);
	$this->db->delete($this->getTableName());

	# Apply for other language
	$arrCIData = $_CI->getArrData();
	foreach($arrCIData['arrLangAvlb'] as $e) if($this->config->item('jw_language') != $e['strKey']) 
		if($this->checkTableExists($this->getTableName().'_'.$e['strData2'])) {
		$this->db->where($strWhere,NULL,FALSE);
		$this->db->delete($this->getTableName().'_'.$e['strData2']);
	}

	$this->_intLastAffectedRows = $this->db->affected_rows();
	return $this->getAffectedRows();
}

# Session handler for invalid query operation
public function createSession() {
	$arrColumnData = $this->getColumnData();
	$arrRecord = $this->getQueryResult('Array');
	foreach($arrColumnData as $e)
		$_SESSION[$e['Field']] = $arrRecord[$e['Field']];
}

public function destroySession() {
	$arrColumnData = $this->getColumnData();
	foreach($arrColumnData as $e)
		unset($_SESSION[$e['Field']]);
}

# Another retreive functions
# Get a row selected by the row number
public function getSelectedRow($param = '', $iRowNum = 0) {
	$mixQueryResult = $this->getQueryResult();
	switch ($param) {
		case 'Array' : return $mixQueryResult->row_array($iRowNum); 
		case 'Object' : return $mixQueryResult->row($iRowNum); 
		default : return $mixQueryResult;
	}
}

# Get next record like mysql_fetch_row()
public function getNextRecord($param = '') { return $this->getSelectedRow($param); }

# Search by ID
public function search($strValue,$strKey = 'id') {
	$this->_mixQueryResult = $this->db->query('SELECT * FROM '.$this->getTableName()." WHERE $strKey='$strValue'");
}

# Database helpers
# Count the number of result records
public function getNumRows() {
	$mixQueryResult = $this->getQueryResult();
	return $mixQueryResult->num_rows();
}

# Count all record that affected in update or delete query like mysql_affected_rows()
public function getAffectedRows() { return $this->_intLastAffectedRows; }

# Retrieve the last insert ID like mysql_insert_id()
public function getInsertID() { return $this->_intLastInsertID; }

# Check table exists
public function checkTableExists($strTableName) { return $this->db->table_exists($strTableName); }

}

/* End of File */