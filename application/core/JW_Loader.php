<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JW_Loader extends CI_Loader {

public function __construct() {
	parent::__construct();
}

function view($view, $vars = array(), $return = FALSE) {
	$_CI =& get_instance();
	if(constant('ENVIRONMENT') == 'development') $_CI->output->enable_profiler(TRUE);

	$strHTMLOutput = parent::view($view, $vars, TRUE);

	# Obfuscate the HTML Output
	// if(constant('ENVIRONMENT') == 'production' && $this->uri->segment(1) != 'adminpage')
	// 	$strHTMLOutput = str_replace(array("\r\n","\n","  ","	"),array('','',' ',''),$strHTMLOutput);

	# Some hack
	// $strHTMLOutput = str_replace(array("/r/n"),array("\r\n"),$strHTMLOutput);

	if($return) return $strHTMLOutput;
	else echo $strHTMLOutput;
}

function model($model, $name = '', $db_conn = FALSE) {
	if(!empty($name) && !$this->is_model_loaded($name))
		return parent::model($model, $name, $db_conn);
	if(!$this->is_model_loaded($model))
		return parent::model($model, $name, $db_conn);
}

/**
 * Returns true if the model with the given name is loaded; false otherwise.
 *
 * @param   string  name for the model
 * @return  bool
 */
public function is_model_loaded($name) 
{
    return in_array($name, $this->_ci_models, TRUE);
}

}

/* End of File */