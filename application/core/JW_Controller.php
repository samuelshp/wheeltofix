<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class JW_Controller extends CI_Controller {
# Private Variables
protected $_arrData = array();

public function __construct() { 
	parent::__construct();
	$this->_checkExpiry();
	if(constant('ENVIRONMENT') == 'development' && strpos($this->router->fetch_class(), 'ajax') === FALSE) {
		$this->output->enable_profiler(TRUE);
	}

	$this->_initializeDefaultProperties();
	if($this->uri->segment(1) != 'adminpage') $this->_initializeUserProperties();
}

# ********************************* GETTER ********************************************* /
public function getArrData() { return $this->_arrData; }
private function _getBrowserIPAddress() { return $_SERVER['REMOTE_ADDR']; }
private function _getBrowserAgent() { return $_SERVER['HTTP_USER_AGENT']; }
protected function _getOwnerInfo() {
	$arrData = array();
	$this->load->model('Mtinydbvo','ownerInfo');
	$this->ownerInfo->initialize('jwsettingstore');
	$arrData['strWebsiteName'] = $this->ownerInfo->getSingleData('JW_WEBSITE_NAME');
	
	$this->ownerInfo->initialize('jwsettingowner');
	$arrData['strOwnerName'] = $this->ownerInfo->getSingleData('JW_OWNER_NAME');
	$arrData['strOwnerEmail'] = $this->ownerInfo->getSingleData('JW_OWNER_EMAIL');
	$arrData['strOwnerPhone'] = $this->ownerInfo->getSingleData('JW_OWNER_PHONE');
	$arrData['strOwnerAddress'] = $this->ownerInfo->getSingleData('JW_OWNER_ADDRESS');
	$intDataCount = $this->ownerInfo->getSingleData('JW_OWNER_DATACOUNT');
	for ($i=1; $i <= $intDataCount; $i++) 
		$arrData['strOwnerData'.$i] = $this->ownerInfo->getSingleData('JW_OWNER_DATA'.$i);

	return $arrData;
}

private function _getLanguageAvlb() {
	if(empty($this->_arrData['arrLangAvlb'])) {
		$this->load->model('Mtinydbvo','language');
		$this->language->initialize('language');
		$this->_arrData['arrLangAvlb'] = $this->language->getAllData();
	}
	
	return $this->_arrData['arrLangAvlb'];
}

protected function _getMenuHelpContent($intID,$bolCheckLinkMode = false,$strRedirectTo = -1,$intPrivilegeID = 0,$intMode = 0) {
	$this->load->model('Madmlinklist');
	
	if($bolCheckLinkMode && $arrHelp = $this->Madmlinklist->getHelp($intID,true)) { # Check link
	
		if($strRedirectTo != -1 && !$this->Madmlinklist->getAvailability($intID,$intPrivilegeID,$intMode)) { # Check availability
			$this->session->set_flashdata('strMessage',$this->lang->jw('no_privilege'));
			redirect($strRedirectTo);
		}
	
		$this->_arrData['intHelpLinkID'] = $arrHelp->id;
		$this->_arrData['strMenuInstruction'] = $arrHelp->instruction;
	} else if($arrHelp = $this->Madmlinklist->getHelp($intID)) { # Check table
	
		if($strRedirectTo != -1 && !$this->Madmlinklist->getAvailability($arrHelp->id,$intPrivilegeID,$intMode)) { # Check availability
			$this->session->set_flashdata('strMessage',$this->lang->jw('no_privilege'));
			redirect($strRedirectTo);
		}
	
		$this->_arrData['intHelpLinkID'] = $arrHelp->id;
		$this->_arrData['strMenuInstruction'] = $arrHelp->instruction;
	} else {
		$this->_arrData['intHelpLinkID'] = 0;
		$this->_arrData['strMenuInstruction'] = '-- No Data --';
	}
}

# ********************************* SETTER ********************************************* /
private function _updateVisitorCounter() {}
public function setArrData($arrData) { $this->_arrData = $arrData; }
public function insertData($strName,$mixData) { $this->_arrData[$strName] = $mixData; }
public function updateData($strName,$mixData) { $this->insertData($strName,$mixData); }
public function deleteData($strName) { 
	if(empty($this->_arrData[$strName])) return FALSE;
	
	$mixValue = $this->_arrData[$strName];
	unset($this->_arrData[$strName]);
	$this->_arrData = array_values($this->_arrData);
	return $mixValue;
}

protected function _checkCaptcha($strRedirect = '') { checkCaptcha($strRedirect); } # Pindah ke Helper
protected function _checkExpiry() {
	if(constant('ENVIRONMENT') == 'expired') try {
		echo (@file_get_contents("http://www.tobsite.com/home/expiry?url=".urlencode(base_url())) == 'Online') ? 
				'' : show_error('Website is offline. Please contact us for more information.');
	} catch (Exception $e) {}
}

# ********************************* INITIALIZE ***************************************** /
private function _initializeDefaultProperties() {
	$this->load->model('Mtinydbvo','defaultconfig');

	# Default Config
	$this->defaultconfig->initialize('jwdefaultconfig');
	
    $this->config->set_item('jw_language',$this->defaultconfig->getSingleData('JW_LANGUAGE'));
	$this->config->set_item('jw_adm_style',$this->defaultconfig->getSingleData('JW_ADMIN_TEMPLATE'));
	$this->config->set_item('jw_ui_style',$this->defaultconfig->getSingleData('JW_UI_TEMPLATE'));
	if($this->uri->segment(1) == 'adminpage')
		$this->config->set_item('jw_style',$this->config->item('jw_adm_style'));
	else {
		$this->config->set_item('jw_admin_style',$this->config->item('jw_adm_style'));
		$this->config->set_item('jw_style',$this->config->item('jw_ui_style'));
	}

	# Language
	if($this->uri->segment(1) == 'adminpage') {
		if($this->session->userdata('jw_admin_language') == '')
			$this->session->set_userdata('jw_admin_language',$this->defaultconfig->getSingleData('JW_LANGUAGE',2)); # tiny_data2
		
	} else {
		if($this->session->userdata('jw_language') == '')
			$this->session->set_userdata('jw_language',$this->config->item('jw_language'));
		
	}

	$this->defaultconfig->initialize('language');
	if($this->uri->segment(1) != 'adminpage') {
		if($this->session->userdata('jw_language_abbr') == '') {
			$strLanguageAbbr = $this->defaultconfig->getSingleData($this->session->userdata('jw_language'),2);
			$this->session->set_userdata('jw_language_abbr',!empty($strLanguageAbbr) ? $strLanguageAbbr : 'en');
		}
	}
	
	$this->_arrData['arrLangAvlb'] = $this->_getLanguageAvlb();
	
	# Website Config
	$this->defaultconfig->initialize('jwsettingstore');
	$this->config->set_item('jw_website_name',$this->defaultconfig->getSingleData('JW_WEBSITE_NAME'));
	$this->config->set_item('jw_website_motto',$this->defaultconfig->getSingleData('JW_WEBSITE_MOTTO'));
	$strWebsiteLogo = $this->defaultconfig->getSingleData('JW_WEBSITE_LOGO');
	if(strpos($strWebsiteLogo,'<jw:site_url>') === 0) $strWebsiteLogo = str_replace('<jw:site_url>',base_url(),$strWebsiteLogo);
	$this->config->set_item('jw_website_logo',$strWebsiteLogo);
	$strWebsiteIcon = $this->defaultconfig->getSingleData('JW_WEBSITE_ICON');
	if(strpos($strWebsiteIcon,'<jw:site_url>') === 0) $strWebsiteIcon = str_replace('<jw:site_url>',base_url(),$strWebsiteIcon);
	$this->config->set_item('jw_website_icon',$strWebsiteIcon);
	$this->config->set_item('jw_product_count',$this->defaultconfig->getSingleData('JW_PRODUCT_COUNT'));
	$this->config->set_item('jw_post_count',$this->defaultconfig->getSingleData('JW_POST_COUNT'));
	$this->config->set_item('jw_item_count',$this->defaultconfig->getSingleData('JW_POST_COUNT'));
	$this->config->set_item('jw_home_count',$this->defaultconfig->getSingleData('JW_HOME_COUNT'));
	$this->config->set_item('jw_sidebar_count',$this->defaultconfig->getSingleData('JW_SIDEBAR_COUNT'));
	
	# Meta Config
	$this->defaultconfig->initialize('jwsettingmeta');
	$this->config->set_item('jw_meta_keyword',$this->defaultconfig->getSingleData('JW_META_KEYWORD'));
	$this->config->set_item('jw_meta_description',$this->defaultconfig->getSingleData('JW_META_DESCRIPTION'));
	$this->config->set_item('jw_meta_robot',$this->defaultconfig->getSingleData('JW_META_ROBOT'));
	$this->config->set_item('jw_meta_googlebot',$this->defaultconfig->getSingleData('JW_META_GOOGLEBOT'));
	$this->config->set_item('jw_meta_revisit',$this->defaultconfig->getSingleData('JW_META_REVISIT'));
	
	$this->load->model('Mwarehouse');
	$arrPrimaryWarehouse = $this->Mwarehouse->getItemByID(1);
	$this->config->set_item('sia_primary_warehouse_name', $arrPrimaryWarehouse['ware_name']);
	$this->load->model('Mbranch');
	$arrPrimaryBranch = $this->Mbranch->getItemByID(1);
	$this->config->set_item('sia_primary_branch_name', $arrPrimaryBranch['bran_name']);
	$this->load->model('Mcustomer');
	$arrPrimaryCustomer = $this->Mcustomer->getItemByID(1);
	$this->config->set_item('sia_primary_customer_name', $arrPrimaryCustomer['cust_name']);

	# Set the data that will be passed to the view
	if($this->session->flashdata('strMessage') != '')
		$this->insertData('strMessage',$this->session->flashdata('strMessage'));

	$this->_arrData['arrOwnerInfo'] = $this->_getOwnerInfo();
	$this->_arrData['arrCompanyInfo'] = array(
		'strCompanyName' => $this->config->item('jw_website_name'),
		'strCompanyLogo' => $this->config->item('jw_website_logo'),
		'strCompanyIcon' => $this->config->item('jw_website_icon'),
		'strOwnerName' => $this->_arrData['arrOwnerInfo']['strOwnerName'],
		'strOwnerEmail' => $this->_arrData['arrOwnerInfo']['strOwnerEmail'],
		'strOwnerPhone' => $this->_arrData['arrOwnerInfo']['strOwnerPhone'],
		'strOwnerAddress' => $this->_arrData['arrOwnerInfo']['strOwnerAddress']
	);
}

# Show some information about the user
private function _initializeUserProperties() {
	# Hack about login by cookie
	if(isset($_COOKIE['strMemberLogin']) && ($this->session->userdata('strMemberLogin') == '' || $this->session->userdata('strMemberLogin') == 'guest') && uri_string() != 'member/getIn') {
		$this->load->library('encrypt');
		redirect('member/getIn?a=cookie&p='.$this->encrypt->encode('HACK_LOGIN_BY_COOKIE'));
	}

	# Get all availability
	$this->load->model('Madmlinklist','feature');
	
	$arrData = array();
	
	# User login data
	if($this->session->userdata('strMemberLogin') == '') $this->session->set_userdata('strMemberLogin','guest');
	else {
		$arrData['strMemberLastLogin'] = $this->session->userdata('strMemberLastLogin');
	}
	
	$arrData['strMemberLogin'] = $this->session->userdata('strMemberLogin');
	if($arrData['strMemberLogin'] != 'guest') {
		$this->load->model('Mmember');
		$arrData['arrMemberLoginData'] = $this->Mmember->getItemByLogin($arrData['strMemberLogin']);
	}

	$this->_arrData = array_merge($this->_arrData,$arrData);
}

}

/* End of File */