<?php

class UINavigation extends Widget {

public function display($arrData) {
	$this->load->model('Mshortcut');
	
    $this->view($arrData['strViewFolder'].'/widgets/navigation', array_merge(array(
		'arrShortcut' => $this->Mshortcut->getNavigationItems()
    ),$arrData));
}
    
}