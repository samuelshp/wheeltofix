<?php

class UICalendarLatest extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	$this->Mblog->setType('Calendar');
	$arrCalendarLatest = $this->Mblog->getLatestByCategoryType(3,$this->config->item('jw_sidebar_count'));
	$this->Mblog->setType();
	
    $this->view($arrData['strViewFolder'].'/widgets/calendarlatest', array_merge(array(
		'arrCalendarLatest' => $arrCalendarLatest
    ),$arrData));
}
    
}