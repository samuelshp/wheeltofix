<?php

class UIShortcut extends Widget {

public function display($arrData) {
	$this->load->model('Mshortcut');
	
    $this->view($arrData['strViewFolder'].'/widgets/shortcut', array_merge(array(
		'arrShortcut' => $this->Mshortcut->getShortcutItems()
    ),$arrData));
}
    
}