<?php

class UIBannerAdvertising extends Widget {

public function display($arrData) {
	$this->load->model('Mbanner');
	
    $this->view($arrData['strViewFolder'].'/widgets/banneradvertising', array_merge(array(
		'arrBannerAdvertising' => $this->Mbanner->getItems(2,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}