<?php

class UIShoutLatest extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	$this->Mblog->setType('Shout');
	$arrShoutLatest = $this->Mblog->getLatestByCategoryType(7,$this->config->item('jw_sidebar_count'));
	$this->Mblog->setType();
	
    $this->view($arrData['strViewFolder'].'/widgets/shoutlatest', array_merge(array(
		'arrShoutLatest' => $arrShoutLatest
    ),$arrData));
}
    
}