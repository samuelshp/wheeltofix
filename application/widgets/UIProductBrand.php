<?php

class UIProductBrand extends Widget {

public function display($arrData) {
	$this->load->model('Mproductbrand');
	
    $this->view($arrData['strViewFolder'].'/widgets/productbrand', array_merge(array(
		'arrProductBrand' => $this->Mproductbrand->getItems()
    ),$arrData));
}
    
}