<?php

class UIProductCategory extends Widget {

public function display($arrData) {
	$this->load->model('Mproductcategory');
	
    $this->view($arrData['strViewFolder'].'/widgets/productcategory', array_merge(array(
		'arrProductCategoryWidget' => $this->Mproductcategory->getItems()
    ),$arrData));
}
    
}