<?php

class UIBannerSidebar extends Widget {

public function display($arrData) {
	$this->load->model('Mbanner');
	
    $this->view($arrData['strViewFolder'].'/widgets/bannersidebar', array_merge(array(
		'arrBannerSidebar' => $this->Mbanner->getItems(2,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}