<?php

class UIBlogComment extends Widget {

public function display($arrData) {
	$this->load->model('Mblogcomment');
	
    $this->view($arrData['strViewFolder'].'/widgets/blogcomment', array_merge(array(
		'arrBlogComment' => $this->Mblog->getLatestComment($this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}