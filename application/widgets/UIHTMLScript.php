<?php

class UIHTMLScript extends Widget {

public function display($arrData) {
	$this->load->model('Mhtmlscript');
	
    $this->view($arrData['strViewFolder'].'/widgets/htmlscript', array_merge(array(
		'arrHTMLScript' => $this->Mhtmlscript->getItems()
    ),$arrData));
}
    
}