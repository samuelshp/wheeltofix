<?php

class UICalendarFull extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	$this->Mblog->setType('Calendar');
	$earlyMonth = date('Y-m').'-01';
	$endMonth = date('Y-m-d',strtotime($earlyMonth. ' + 1 months')); //2016-12-01
	$endMonth = date('Y-m-d',strtotime($endMonth. ' - 1 days'));
	$arrCalendarFull = $this->Mblog->getItemsByDate($earlyMonth,$endMonth);
	$this->Mblog->setType();
	
    $this->view($arrData['strViewFolder'].'/widgets/calendarfull', array_merge(array(
		'arrCalendarFull' => $arrCalendarFull
    ),$arrData));
}
    
}