<?php

class UIBannerBig extends Widget {

public function display($arrData) {
	$this->load->model('Mbanner');
	
    $this->view($arrData['strViewFolder'].'/widgets/bannerbig', array_merge(array(
		'arrBannerBig' => $this->Mbanner->getItems(1,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}