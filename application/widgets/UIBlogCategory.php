<?php

class UIBlogCategory extends Widget {

public function display($arrData) {
	$this->load->model('Mblogcategory');
	
    $this->view($arrData['strViewFolder'].'/widgets/blogcategory', array_merge(array(
		'arrBlogCategory' => $this->Mblogcategory->getItems()
    ),$arrData));
}
    
}