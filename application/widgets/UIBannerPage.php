<?php

class UIBannerPage extends Widget {

public function display($arrData) {
	$this->load->model('Mbanner');
	
    $this->view($arrData['strViewFolder'].'/widgets/bannerpage', array_merge(array(
		'arrBannerPage' => $this->Mbanner->getItems(3,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}