<?php

class UITestimonialLatest extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	
    $this->view($arrData['strViewFolder'].'/widgets/testimoniallatest', array_merge(array(
		'arrTestimonialLatest' => $this->Mblog->getLatestByCategoryType(6,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}