<?php

class UIBlogLatest extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	
    $this->view($arrData['strViewFolder'].'/widgets/bloglatest', array_merge(array(
		'arrBlogLatest' => $this->Mblog->getLatestByCategoryType(1,$this->config->item('jw_sidebar_count'))
    ),$arrData));
}
    
}