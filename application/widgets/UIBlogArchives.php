<?php

class UIBlogArchives extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	
    $this->view($arrData['strViewFolder'].'/widgets/blogarchives', array_merge(array(
		'arrBlogArchives' => $this->Mblog->getArchivesList()
    ),$arrData));
}
    
}