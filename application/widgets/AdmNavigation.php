<?php

class AdmNavigation extends Widget {

public function display($arrData) {
    $this->view($arrData['strViewFolder'].'/widgets/navigation', array_merge(array(
		'arrLinkList' => $this->getArrLinkList()
    ),$arrData));
}

private function getArrLinkList() {
	$this->load->model('Madmlinklist');
	return $this->Madmlinklist->get($this->session->userdata('strAdminPriviledge'));
}

}