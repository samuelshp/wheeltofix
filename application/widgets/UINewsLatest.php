<?php

class UINewsLatest extends Widget {

public function display($arrData) {
	$this->load->model('Mblog');
	$this->Mblog->setType('News');
	$arrNewsLatest = $this->Mblog->getLatestByCategoryType(5,$this->config->item('jw_sidebar_count'));
	$this->Mblog->setType();
	
    $this->view($arrData['strViewFolder'].'/widgets/newslatest', array_merge(array(
		'arrNewsLatest' => $arrNewsLatest
    ),$arrData));
}
    
}