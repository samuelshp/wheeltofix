<?php

$lang = array(	
	'administrator-permissiondenied' => 'Permission denied!',
	'administrator-priviledgechanged' => 'Administrator priviledge changed',
	
	'file-erroroccured' => 'Error occured when uploading file! %s',
	'file-uploadsuccess' => 'Upload success! You can copy paste your file as <b>%s</b>',
	'file-filedeleted' => 'File %s has been deleted',
	'file-foldermade' => 'Folder %s has been made',
	'file-foldercannotmade' => 'Folder %s cannot be made!',
	'file-folderexist' => 'Folder %s is already exist. Please choose different name!',
	'file-folderdeleted' => 'Folder %s has been deleted!',
	'file-foldercannotdeleted' => 'Folder %s cannot be deleted!',
	'file-foldernotempty' => 'Folder %s is not empty. Please delete the folder content!',
	
	'login-passwordinvalid' => 'Password invalid!',
	'login-usernameorpasswordinvalid' => 'Username or password invalid!',
	
	'main-activelanguage' => 'Active language is %s',
	'main_2' => 'Your mail has been sent',
	'main_3' => 'Your mail failed to be sent, however it has been saved',

	'table-datadeleted' => '%s data deleted!',
	'table-dataadded' => '1 data added!',
	'table-dataedited' => '%s data edited!',
	'table-inputsearchparameter' => 'Please input the search parameters correctly!',
	'table-nopriviledgeaccess' => 'You don\'t have priviledge to access that page!',
	
	'setting-companydatachanged' => 'Company data has been changed',
	'setting-miscsettingchanged' => 'Miscellaneous setting has been changed',
	'setting-themechanged' => 'Theme has been changed',
	
/* --- */

    'adjustment-adjustmentadded' => 'The adjustment has been added',
	'adjustment-adjustmentupdated' => 'The adjustment has been updated',
	'adjustment-adjustmentdeleted' => 'The adjustment has been deleted',

	'accountstart-accountstartmade' => 'Beginning Balance of Account has been added',
	'accountstart-accountstartfailtomade' => 'Beginning Balance of Account failed to save',
	'accountstart-accountstartdeleted' => 'Beginning Balance of Account has been deleted',
	'accountstart-accountstartfailtodeleted' => 'Beginning Balance of Account failed to delete',

    'booking-bookingadded' => 'Booking has been added',
    'booking-bookingupdated' => 'Booking has been updated',
    'booking-bookingdeleted' => 'Booking has been deleted',

    'deposit-depositmade' => 'Deposit has been made',
	'deposit-depositdeleted' => 'Deposit has been delete',

    'delivery-deliveryadded' => 'The delivery has been added',
    'delivery-deliveryupdated' => 'The delivery has been updated',
    'delivery-deliverydeleted' => 'The delivery has been deleted',

    'delivery-deliverybackadded' => 'The delivery back has been added',
    'delivery-deliverybackupdated' => 'The delivery back has been updated',
    'delivery-deliverybackdeleted' => 'The delivery back has been deleted',
	
	'finance-financeupdated' => 'Finance transaction has been updated',
	'finance-financedeleted' => 'Finance transaction has been deleted',
	'finance-financemade' => 'Finance transaction has been made',
	'finance-selecttransaction' => 'Please select transaction',
	'finance-claimmade' => 'Invoice Claim has been made',
	'finance-receivablemade' => 'Invoice Receivable has been made',
    'finance-claimupdated' => 'Invoice Claim has been updated',
	'finance-receivableupdated' => 'Invoice Receivable has been updated',

	'inventory-stockconcluded' => 'All of item stock has been concluded',

    'invoiceorder-invoiceorderupdated' => 'The invoice order has been updated',
    'invoiceorder-invoiceorderdeleted' => 'The invoice order has been deleted',
    'invoiceorder-invoiceordermade' => 'The invoice order has been made',

	'invoice-alertcorrectdate' => 'Please enter correct date!',
	'invoice-alertcorrectprice' => 'Please enter correct price!',
	'invoice-quantityexceed' => 'The quantity you submitted exceed available stock!',
	'invoice-wrongbarcode' => 'Wrong barcode number, please try again!',
	'invoice-saleorderquantityexceed' => 'The quantity you submitted exceed sale order quantity!',
	'invoice-invoiceupdated' => 'The invoice has been updated',
	'invoice-invoicedeleted' => 'The invoice has been deleted',
	'invoice-invoicemade' => 'The invoice has been made',
	'invoice-saleorderresetinvoice' => 'The invoice has been resetted because you changed the sale order',
	'invoice-loginaswarehouseuser' => 'Please login as warehouse user',
	
	'purchase-purchaseupdated' => 'The purchase has been updated',
	'purchase-purchasedeleted' => 'The purchase has been deleted',
	'purchase-purchasemade' => 'The purchase has been made',

    'purchase-purchaseorderupdated' => 'The purchase order has been updated',
    'purchase-purchaseorderdeleted' => 'The purchase order has been deleted',
    'purchase-purchaseordermade' => 'The purchase order has been made',

    'purchase-acceptanceupdated' => 'The acceptance has been updated',
    'purchase-acceptancedeleted' => 'The acceptance has been deleted',
    'purchase-acceptancemade' => 'The acceptance has been made',
	
	'return-returnmade' => 'Return has been made',
    'return-returnupdated' => 'Return has been updated',
    'return-returndeleted' => 'Return has been deleted',
	'return-returnexceeded' => 'Return quantity exceeded item quantity',	
	
	'saleorder-saleorderupdated' => 'Sale order has been updated',
	'saleorder-saleorderdeleted' => 'Sale order has been deleted',
	'saleorder-saleordermade' => 'Sale order has been made',

    'kanvas-kanvasmade' => 'Kanvas has been made',

    'pemakaian-pemakaianmade' => 'The usage order has been made',

	'' => ''
);

$lang['main_1'] = 'Active language is %s';
$lang['main_2'] = 'Your mail has been sent';
$lang['main_3'] = 'Your mail failed to be sent, however it has been saved';

$lang['table_1'] = '%s data deleted!';

$lang['login_1'] = 'Password Invalid!';
$lang['login_2'] = 'Username or Password Invalid!';

$lang['file_1'] = 'Error occured when uploading file! %s';
$lang['file_2'] = 'Upload success! You can copy paste your file as <b>%s</b>';
$lang['file_3'] = 'File %s has been deleted!';
$lang['file_4'] = 'Folder %s has been made!';
$lang['file_5'] = 'Folder %s cannot be made!';
$lang['file_6'] = 'Folder %s is already exist. Please choose different name!';
$lang['file_7'] = 'Folder %s has been deleted!';
$lang['file_8'] = 'Folder %s cannot be deleted!';
$lang['file_9'] = 'Folder %s is not empty. Please delete the folder content!';

/* End of file adminmessage_lang.php */
/* Location: ./system/application/language/english/adminmessage_lang.php */