<?php

$lang = array(	
	'administrator-permissiondenied' => 'Anda tidak punya hak akses ke halaman tersebut!',
	'administrator-priviledgechanged' => 'Hak akses telah diubah',
	
	'file-erroroccured' => 'Terjadi error saat mengupload file! %s',
	'file-uploadsuccess' => 'Upload berhasil. Anda dapat memasukkan file anda sebagai <b>%s</b>',
	'file-filedeleted' => 'File %s telah dihapus',
	'file-foldermade' => 'Folder %s telah dibuat',
	'file-foldercannotmade' => 'Folder %s tidak dapat dibuat!',
	'file-folderexist' => 'Folder %s telah ada. Silakan pilih nama lain!',
	'file-folderdeleted' => 'Folder %s telah dihapus',
	'file-foldercannotdeleted' => 'Folder %s tidak dapat dihapus!',
	'file-foldernotempty' => 'Folder %s tidak kosong. Mohon hapus semua isi folder!',
	
	'login-passwordinvalid' => 'Password salah!',
	'login-usernameorpasswordinvalid' => 'Username atau Password salah!',

	'main-activelanguage' => 'Bahasa aktif adalah bahasa %s',
	'main_2' => 'Pesan anda telah dikirim',
	'main_3' => 'Pesan anda tidak dapat dikirim, namun telah disimpan',

	'setting-companydatachanged' => 'Data perusahaan telah diubah',
	'setting-miscsettingchanged' => 'Pengaturan lain telah diubah',
	'setting-themechanged' => 'Template telah diubah',
	
	'table-datadeleted' => '%s data telah dihapus',
	'table-dataadded' => 'Satu data telah ditambahkan',
	'table-dataedited' => '%s data diubah',
	'table-inputsearchparameter' => 'Masukkan parameter pencarian yang benar!',
	'table-nopriviledgeaccess' => 'Anda tidak punya hak akses ke halaman tersebut!',

/* --- */
	
	'kontrak-added' => 'Proyek berhasil disimpan',
	'kontrak-failedadded' => 'Proyek gagal disimpan',
	'kontrak-updated' => 'Proyek berhasil diperbaharui',
	'kontrak-failedupdated' => 'Proyek berhasil diperbaharui',
	'kontrak-deleted' => 'Proyek berhasil dihapus',
	'kontrak-faileddeleted' => 'Proyek gagal dihapus',

    'adjustment-adjustmentadded' => 'Penyesuaian telah dibuat',
	'adjustment-adjustmentupdated' => 'Penyesuaian telah diubah',
	'adjustment-adjustmentdeleted' => 'Penyesuaian telah dihapus',

	'accountstart-accountstartmade' => 'Saldo Awal Account Berhasil Disimpan',
	'accountstart-accountstartfailtomade' => 'Saldo Awal Account Gagal Disimpan',
	'accountstart-accountstartupdated' => 'Saldo Awal Account Berhasil Diperbaharui',
	'accountstart-accountstartdeleted' => 'Saldo Awal Account Berhasil Dihapus',
	'accountstart-accountstartfailtodeleted' => 'Saldo Awal Account Gagal Dihapus',

    'booking-bookingadded' => 'Booking telah dibuat',
    'booking-bookingupdated' => 'Booking telah diubah',
    'booking-bookingdeleted' => 'Booking telah dihapus',

    'deposit-depositmade' => 'Deposit telah dibuat',
	'deposit-depositdeleted' => 'Deposit telah dihapus',

    'delivery-deliveryadded' => 'Pengiriman telah dibuat',
    'delivery-deliveryupdated' => 'Pengiriman telah diubah',
    'delivery-deliverydeleted' => 'Pengiriman telah dihapus',

    'delivery-deliverybackadded' => 'Pengiriman kembali telah dibuat',
    'delivery-deliverybackupdated' => 'Pengiriman kembali telah diubah',
    'delivery-deliverybackdeleted' => 'Pengiriman kembali telah dihapus',
	
	'finance-financeupdated' => 'Transaksi keuangan telah diubah',
	'finance-financedeleted' => 'Transaksi keuangan telah dihapus',
	'finance-financemade' => 'Transaksi keuangan telah dibuat',
	'finance-selecttransaction' => 'Mohon pilih transaksi',
    'finance-claimmade' => 'Tagihan Piutang telah dibuat',
	'finance-receivablemade' => 'Pelunasan Piutang telah dibuat',
    'finance-claimupdated' => 'Tagihan Piutang telah diubah',
	'finance-receivableupdated' => 'Pelunasan Piutang telah diubah',
	
	'inventory-stockconcluded' => 'Seluruh stok barang telah dirangkum',

    'invoiceorder-invoiceorderupdated' => 'Faktur pesanan penjualan telah diubah',
    'invoiceorder-invoiceorderdeleted' => 'Faktur pesanan penjualan telah dihapus',
    'invoiceorder-invoiceordermade' => 'Faktur pesanan penjualan telah dibuat',
    'invoiceorder-invoiceordermadefailed' => 'Faktur pesanan penjualan gagal dibuat',
    'invoiceorder-invoiceorderupdatedfailed' => 'Faktur pesanan penjualan gagal diubah',
    'invoiceorder-invoiceorderdeletedfailed' => 'Faktur pesanan penjualan gagal dihapus',

	'invoice-alertcorrectdate' => 'Masukkan tanggal yang benar!',
	'invoice-alertcorrectprice' => 'Masukkan harga yang benar!',
	'invoice-quantityexceed' => 'Jumlah yang anda masukkan melebihi stok yang ada!',
	'invoice-wrongbarcode' => 'Nomor barcode salah, silakan ulangi sekali lagi!',
	'invoice-saleorderquantityexceed' => 'Jumlah yang anda masukkan melebihi jumlah yang ada pada pesanan penjualan!',
	'invoice-invoiceupdated' => 'Faktur penjualan telah diubah',
	'invoice-invoicedeleted' => 'Faktur penjualan telah dihapus',
	'invoice-invoicemade' => 'Faktur penjualan telah dibuat',
	'invoice-saleorderresetinvoice' => 'Penjualan direset karena data sale order berubah',
	'invoice-loginaswarehouseuser' => 'Mohon login sebagai user gudang',

	'kontrakpembelian-update' => 'Kontrak Pembelian berhasil diperbaharui',
	'kontrakpembelian-deleted' => 'Kontrak Pembelian berhasil dihapus',
	'kontrakpembelian-faildeleted' => 'Kontrak Pembelian gagal dihapus',

	'paymentrequest-savedone' => 'Permintaan Pembayaran Berhasil diperbaharui',
	'paymentrequest-savefailed' => 'Permintaan Pembayaran Gagal diperbaharui',
	'paymentrequest-updatedone' => 'Permintaan Pembayaran Berhasil diperbaharui',
	'paymentrequest-updatefailed' => 'Permintaan Pembayaran Gagal diperbaharui',
	'paymentrequest-deletedone' => 'Permintaan Pembayaran Berhasil dihapus',
	'paymentrequest-deletefailed' => 'Permintaan Pembayaran Gagal dihapus',
	'paymentrequest-approvedone' => 'Permintaan Pembayaran disetujui',
	'paymentrequest-disapprovedone' => 'Permintaan Pembayaran ditolak',
	
	'purchase-purchaseupdated' => 'Pembelian telah diubah',
	'purchase-purchasedeleted' => 'Pembelian telah dihapus',
	'purchase-purchasemade' => 'Pembelian telah dibuat',

    'purchase-purchaseorderupdated' => 'Pesanan pembelian telah diubah',
    'purchase-purchaseorderdeleted' => 'Pesanan pembelian telah dihapus',
    'purchase-purchaseordermade' => 'Pesanan pembelian telah dibuat',

    'purchase-acceptanceupdated' => 'Penerimaan barang telah diubah',
    'purchase-acceptancedeleted' => 'Penerimaan barang telah dihapus',
    'purchase-acceptancemade' => 'Penerimaan barang telah dibuat',
	
	'return-returnmade' => 'Retur telah dibuat',
    'return-returnupdated' => 'Retur telah diubah',
    'return-returndeleted' => 'Retur telah dihapus',
	'return-returnexceeded' => 'Jumlah retur melebihi jumlah barang',
	
	'saleorder-saleorderupdated' => 'Permintaan penjualan telah diubah',
	'saleorder-saleorderdeleted' => 'Permintaan penjualan telah dihapus',
	'saleorder-saleordermade' => 'Permintaan penjualan telah dibuat',

    'kanvas-kanvasmade' => 'Kanvas telah dibuat',

    'loading-data' => 'Memuat data...',

    'peamakaian-pemakaianmade' => 'Pesanan pemakaian telah dibuat',

	'' => ''
);

$lang['main_1'] = 'Bahasa aktif adalah bahasa %s';
$lang['main_2'] = 'Pesan anda telah dikirim';
$lang['main_3'] = 'Pesan anda tidak dapat dikirim, namun telah disimpan';

$lang['table_1'] = '%s data telah dihapus!';

$lang['login_1'] = 'Password Salah!';
$lang['login_2'] = 'Username atau Password Salah!';

$lang['file_1'] = 'Terjadi error saat mengupload file! %s';
$lang['file_2'] = 'Upload berhasil! Anda dapat memasukkan file anda sebagai <b>%s</b>';
$lang['file_3'] = 'File %s telah dihapus!';
$lang['file_4'] = 'Folder %s telah dibuat!';
$lang['file_5'] = 'Folder %s tidak dapat dibuat!';
$lang['file_6'] = 'Folder %s telah ada. Silakan pilih nama lain!';
$lang['file_7'] = 'Folder %s telah dihapus!';
$lang['file_8'] = 'Folder %s tidak dapat dihapus!';
$lang['file_9'] = 'Folder %s tidak kosong. Mohon hapus semua isi folder!';

/* End of file adminmessage_lang.php */
/* Location: ./system/application/language/indonesia/adminmessage_lang.php */