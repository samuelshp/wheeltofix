<?php

function breadcrumbs() {
	$arrArgs = func_get_args();
	$strLink = '<p class="breadcrumbs">';
	for($i = 0; $i < count($arrArgs); $i++) {
		if(!empty($arrArgs[$i + 1])) $strLink .= '<a href="'.$arrArgs[$i + 1].'">'.$arrArgs[$i].'</a>';
		else $strLink .= '<b>'.$arrArgs[$i].'</b>';
		if($i + 2 < count($arrArgs)) $strLink .= ' &gt;&gt; ';
		$i++;
	}
	return $strLink .= '</p>';
}

function br2nl($str) {
	return preg_replace('/\<br(\s*)?\/?\>/i', "/r/n", $str);
}

function detectThumbnail($strFilePath) {
	if(empty($strFilePath)) return false;
	if(!strpos($strFilePath,base_url())) return $strFilePath;
	
	$strFilePath = str_replace(base_url(),'',$strFilePath);
	
	$arrFileData = pathinfo($strFilePath);
	if(empty($arrFileData['extension'])) return false;

	$strThumbPath = str_replace('.'.$arrFileData['extension'],'_t.'.$arrFileData['extension'],$strFilePath);
	if(is_file($strThumbPath)) $strFilePath = $strThumbPath;
	
	return base_url($strFilePath);
}

function extractInstruction(&$strData,$strKey = 'InStRuCtIoN') {
	$intStartPos = strpos($strData,"<$strKey>");	
	$intEndPos = strpos($strData,"</$strKey>") + strlen("</$strKey>");
	//$strResult = str_replace(array("<$strKey>","</$strKey>"),array('<div class="'.$strKey.'">','</div>'),substr($strData, $intStartPos, $intEndPos));
    $strResult = str_replace(array('"'),array('\''),substr($strData, $intStartPos, $intEndPos));
	$strData = substr_replace($strData, '', $intStartPos, $intEndPos - $intStartPos);
	return $strResult;
}

function valueExist($mixValue)
{
	if ($mixValue == '') return "-";
	else return $mixValue;
}

function shortenFilename($strPath, $intLen = 12){
	$str = explode("/", $strPath);
	$filename = $str[count($str) - 1];

	$shortenFilename = str_replace(substr($filename, $intLen), "...", $filename);
	return $shortenFilename;
}

/* End of File */