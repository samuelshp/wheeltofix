<?php

function site_url($uri = '', $protocol = NULL, $force_adminpage = TRUE) {
	$_CI =& get_instance();

	if($_CI->uri->segment(1) == 'adminpage' && $force_adminpage) {
		if(is_array($uri) && !in_array('adminpage',$uri)) array_unshift($uri,'adminpage');
		else if(strpos($uri, 'adminpage') === FALSE) $uri = 'adminpage/'.$uri;

		return $_CI->config->site_url($uri, $protocol);
	}

	$strDefaultLanguage = $_CI->config->item('jw_language');
	$strActiveLanguage = $_CI->session->userdata('jw_language');

	// if($strDefaultLanguage != $strActiveLanguage)
	// 	if(is_array($uri))
	// 		$uri = array_merge(array($_CI->session->userdata('jw_language_abbr')),$uri);
	// 	else $uri = array($_CI->session->userdata('jw_language_abbr'),$uri);

	return $_CI->config->site_url($uri, $protocol);
}

function theme_url($uri = '', $protocol = NULL) {
	$_CI =& get_instance();

	if(!is_array($uri)) $uri = array($uri);
	$uri = array_merge(array('asset', $_CI->config->item('jw_style')), $uri);
	return base_url($uri, $protocol);
}

function translateLink($arrLink) {
    if(!empty($arrLink['link_icon'])) $arrLink['name'] = $arrLink['link_icon'].' '.$arrLink['name'];
    $arrClass = array(); $arrAttributes = array(); $strTarget = '';

    switch ($arrLink['link_type']) {
    	case 'page': $strHref = site_url($arrLink['link_description'], NULL, FALSE); break;
    	case 'external': $strHref = $arrLink['link_description']; $strTarget = '_blank'; break;
    	case 'table': case 'report': $strHref = site_url('adminpage/table/browse/'.$arrLink['link_description'], NULL, FALSE); break;
    	case 'query': $strHref = site_url('adminpage/table/browse/'.$arrLink['link_description'].'/'.$arrLink['id'], NULL, FALSE); break;
    	default: $strHref = '#'; $arrLink['name'] .= ' <b class="caret pull-right"></b>'; $arrClass[] = 'dropdown-toggle'; $arrAttributes[] = 'data-toggle="dropdown"'; break;
    }
    if($strHref == site_url(uri_string(), NULL, FALSE)) $arrClass[] = 'current';

    return '<a href="'.$strHref.'"'.($strTarget ? ' target="'.$strTarget.'"' : '').
    	(count($arrClass > 0) ? ' class="'.implode(' ', $arrClass).'"' : '').
    	(count($arrAttributes > 0) ? ' '.implode(' ', $arrAttributes) : '').'>'.
    $arrLink['name'].'</a>';
}

function lastPageURL($strURL = '', $bolRefresh = false) {
	$_CI =& get_instance();

	if(!empty($strURL)) $_CI->session->set_flashdata('strLastPage',$strURL);
	elseif($bolRefresh) $_CI->session->keep_flashdata('strLastPage');
	else {
		if(!empty($_POST['lastPage'])) return $_POST['lastPage'];
		else return $_CI->session->flashdata('strLastPage');
	}
}

function cURL($strURL, $arrPostData = array(), $arrHeader = array()) {
	$ch = curl_init();
	$arrData = array(
	    CURLOPT_URL => $strURL,
	    CURLOPT_HTTPHEADER => array_merge(array(
    		'Content-Type: application/json; charset=utf-8',
    		'X-Requested-With: XMLHttpRequest',
    	), $arrHeader),
    	CURLOPT_RETURNTRANSFER => TRUE,
    	CURLOPT_HEADER => FALSE,
    	CURLOPT_SSL_VERIFYPEER => FALSE
	);

	if(!empty($arrPostData)) {
		$arrData[CURLOPT_POST] = TRUE;
		$arrData[CURLOPT_POSTFIELDS] = json_encode($arrPostData);
	}

	curl_setopt_array($ch, $arrData);
	$response = curl_exec($ch);

	curl_close($ch);
	return $response;
}