<?php

function applyPriceFormula($intInitialPrice, $strPriceFormula = '+0') {
	$strSign = strval($strPriceFormula[0]);
	$intPriceFormula = substr($strPriceFormula,1,strlen($strPriceFormula));
	switch($strSign) {
		case '+': $intNewPrice = $intInitialPrice + $intPriceFormula; break;
		case '-': $intNewPrice = $intInitialPrice - $intPriceFormula; break;
		case '*': $intNewPrice = $intInitialPrice * $intPriceFormula; break;
		case '/': $intNewPrice = $intInitialPrice / $intPriceFormula; break;
		default: $intNewPrice = $intInitialPrice;
	}
	return $intNewPrice;
}

function formatPriceCurrency($intValue, $strCurrency, $strNewCurrency) {
	$_CI =& get_instance();
	$_CI->load->model('Mcurrency');
	
	$arrCurrency = $_CI->Mcurrency->getItemByCode($strCurrency);
	if(!empty($arrCurrency))  $intFirstValue = $arrCurrency['stcu_rate'];
	else $intFirstValue = 1;
	
	$arrNewCurrency = $_CI->Mcurrency->getItemByCode($strNewCurrency);
	if(!empty($arrNewCurrency)) $intSecondValue = $arrNewCurrency['stcu_rate'];
	else $intSecondValue = 1;
	
	return $intValue * $intFirstValue / $intSecondValue;
}

function setPrice($intValue, $strCurrency = 'BASE', $bolWithSign = TRUE) {
    if(!settype($intValue,'float')) return '-';

	$strSign = ''; $strThousand = '.'; $strDecimal = ',';

    if($bolWithSign) {
		$_CI =& get_instance();
		$_CI->load->model('Mcurrency');
		$arrCurrency = $_CI->Mcurrency->getItemByCode($strCurrency);

		if(!empty($arrCurrency)) {
			$strSign = $arrCurrency['stcu_sign'];
			$strThousand = $arrCurrency['stcu_thousand'];
			$strDecimal = $arrCurrency['stcu_decimal'];
		}
    }

    $strReturn = number_format($intValue,2,$strDecimal,$strThousand);
    $strReturn = rtrim(rtrim($strReturn,"0"),$strDecimal); # Remove decimal zeroes
    return (!empty($strSign) ? $strSign.' ' : '').$strReturn;
}

/* End of File */