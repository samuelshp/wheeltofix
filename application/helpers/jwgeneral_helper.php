<?php

function getPostByID($intID) {
    $_CI =& get_instance();
	$_CI->load->model('Mblog');
	$_CI->Mblog->setType('Page');
    $arrData = $_CI->Mblog->getItemByID($intID);
	$_CI->Mblog->setType();

	$arrData['stac_content'] = replaceContent($arrData['stac_content']);

	return $arrData;
}

function getConfig($strName) {
    $_CI =& get_instance();
    return $_CI->config->item($strName);
}

function getSetting($strName, $strCategory = 'jwdefaultconfig') {
    $_CI =& get_instance();
	$_CI->load->model('Mtinydbvo');
	$_CI->Mtinydbvo->initialize($strCategory);
	return $_CI->Mtinydbvo->getSingleData($strName);
}

function getTinyDbValue($strCategory, $strKey) {
	$CI = &get_instance();
	$CI->load->model('Mtinydbvo', 'setting');
	$CI->setting->initialize($strCategory);
	return $CI->setting->getSingleData($strKey);
}

function generateUserFieldList($intTableID) {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','MAdmFieldList');
	$_CI->MAdmFieldList->setQuery(
"SELECT * 
FROM adm_field_list AS afl 
LEFT JOIN (
	SELECT id,ui_display_fields
	FROM adm_table_list
) AS atl ON atl.id = afl.table_id
WHERE afl.table_id = $intTableID
	AND CONCAT(REPLACE(atl.ui_display_fields,',','; '),'; ') REGEXP CONCAT('[[:<:]]',afl.title,'; ')
ORDER BY afl.id ASC");
	
}

function loadLanguage($strFileName,$strLanguage,$strKey,$arrArgs = array()) {
	$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)
	
	if(isset($_CI->lang->is_loaded)) for($i=0; $i<=sizeof($_CI->lang->is_loaded); $i++)
		unset($_CI->lang->is_loaded[$i]);

	$_CI->lang->load($strFileName, $strLanguage);
	
	$strLanguage = $_CI->lang->line($strKey);
	if(!empty($arrArgs)) foreach($arrArgs as $strArgsEach) {
		$intPos = strpos($strLanguage,'%s');
		if($intPos !== FALSE) $strLanguage = substr($strLanguage,0,$intPos).$strArgsEach.substr($strLanguage,$intPos + 2);
	}
    
    echo ''; // Hack to make the flashdata appear
	return $strLanguage;
}

/* End of File */