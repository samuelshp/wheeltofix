<?php

# Translate the form data into HTML statements
function translateDataIntoHTMLStatements($arrFieldList,$mixTableData = '',$intNumber = '') {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','MAdmTableList');
	$_CI->MAdmTableList->initialize('adm_table_list');
	$_CI->load->model('Mdbvo','MForeignTable');
	$_CI->load->model('Mtinydbvo','MForeignTinyDB');

	if(empty($arrFieldList['name'])) $arrFieldList['name'] = '';

	$strHTMLStatement = '';
	if($arrFieldList['allow_null'] == 2) { // SHOW Only
		$strHTMLStatement = '<p>'.$mixTableData.'</p>';

	} else if($arrFieldList['allow_null'] == 3 || $arrFieldList['field_type'] == 'DEFAULT') { // Default Value, usually used as a runtime processing variables
		$mixTableData = empty($mixTableData) ? $arrFieldList['description'] : $mixTableData;
		$strHTMLStatement = '<input type="hidden" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'" />';

	# To change the foreign key
	} else if($arrFieldList['field_type'] == 'SELECT')	{
		# If the foreign key is a table
		if(is_numeric($arrFieldList['description']) == TRUE) {
			# Search the table name
			$_CI->MAdmTableList->search($arrFieldList['description']);
			$admTableListProperties = $_CI->MAdmTableList->getNextRecord('Array');

			# Custom query?
			if(!empty($admTableListProperties['fk_query'])) {
				if(!empty($arrFieldList['custom_query']))
					$admTableListProperties['fk_query'] = parseAdmQuery(
						$admTableListProperties['fk_query'],$arrFieldList['custom_query'],
						$arrFieldList,$mixTableData
					);
				$_CI->MForeignTable->initialize($admTableListProperties['title'],$admTableListProperties['fk_query']);
			} else $_CI->MForeignTable->initialize($admTableListProperties['title']);

			# Generate all data in the foreign table to build select statements
			$_CI->MForeignTable->dbSelect($admTableListProperties['fk_display_fields']);
			$arrFK = $_CI->MForeignTable->getQueryResult('Array');

			# RADIO mode
			if($arrFieldList['validation'] == 'RADIO') {
				$bolDefaultValueSelected = FALSE;
				$strCheckedFirst = '';

				if($arrFieldList['allow_null'] == '1') {
					if(empty($mixTableData)) {
						$strCheckedFirst = ' checked="checked"';
						$bolDefaultValueSelected = TRUE;
					}
					$strHTMLStatement .= '<label class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-0" value="0"'.$strCheckedFirst.' /> NONE</label>';
				}
				$i = 0;
				foreach($arrFK as $e) {
					$strCheckedFirst = '';
					if($i == 0 && $mixTableData == '' && !$bolDefaultValueSelected) {
						$strCheckedFirst = ' checked="checked"';
						$bolDefaultValueSelected = TRUE;
					}

					if($mixTableData == '' || $mixTableData != $e['strKey']) $strHTMLStatement .= '<label class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-'.($i + 1).'" value="'.$e['strKey'].'"'.$strCheckedFirst.' /> '.$e['strData'].'</label>';

					else $strHTMLStatement .= '<label class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-'.($i + 1).'" value="'.$e['strKey'].'" checked="checked" /> '.$e['strData'].'</label>';
					$i++;
				}

			# SELECT mode
			} else {
				$strHTMLStatement = '<select name="'.$arrFieldList['title'].$intNumber.'" class="form-control" placeholder="'.$arrFieldList['name'].'" data-placeholder="Choose '.$arrFieldList['name'].'">';

				if($arrFieldList['allow_null'] == '1') $strHTMLStatement .= '<option value="0">NONE</option>';
				foreach($arrFK as $e) {
					if($mixTableData == '' || $mixTableData != $e['strKey']) $strHTMLStatement .= '<option value="'.$e['strKey'].'">'.$e['strData'].'</option>';
					else $strHTMLStatement .= '<option value="'.$e['strKey'].'" selected="selected">'.$e['strData'].'</option>';
				}
				$strHTMLStatement .= '</select>';
			}

		# If the foreign key is a tinydb
		} else {
			# Generate the data
			$_CI->MForeignTinyDB->initialize($arrFieldList['description']);
			$arrFK = $_CI->MForeignTinyDB->getAllData();

			# RADIO mode
			if($arrFieldList['validation'] == 'RADIO') {
				$bolDefaultValueSelected = FALSE;
				$strCheckedFirst = '';

				if($arrFieldList['allow_null'] == '1') {
					if(empty($mixTableData)) {
						$strCheckedFirst = ' checked="checked"';
						$bolDefaultValueSelected = TRUE;
					}
					$strHTMLStatement .= '<span class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-0" value="0"'.$strCheckedFirst.' /> NONE</span>';
				}
				$i = 0;
				foreach($arrFK as $e) {
					$strCheckedFirst = '';
					if($i == 0 && $mixTableData == '' && !$bolDefaultValueSelected) {
						$strCheckedFirst = ' checked="checked"';
						$bolDefaultValueSelected = TRUE;
					}

					if($mixTableData == '' || $mixTableData != $e['strKey']) $strHTMLStatement .= '<span class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-'.($i + 1).'" value="'.$e['strKey'].'"'.$strCheckedFirst.' /> '.$e['strData'].'</span>';

					else $strHTMLStatement .= '<span class="radio-inline"><input type="radio" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'-'.($i + 1).'" value="'.$e['strKey'].'" checked="checked" /> '.$e['strData'].'</span>';
					$i++;
				}

			} else {
				$strHTMLStatement = '<select name="'.$arrFieldList['title'].$intNumber.'" class="form-control" placeholder="'.$arrFieldList['name'].'" data-placeholder="Choose '.$arrFieldList['name'].'">';
				if($arrFieldList['allow_null'] == '1') $strHTMLStatement .= '<option value="0">NONE</option>';
				foreach($arrFK as $e)
					if($mixTableData == '' || $mixTableData != $e['strKey']) $strHTMLStatement .= '<option value="'.$e['strKey'].'">'.$e['strData'].'</option>';
					else $strHTMLStatement .= '<option value="'.$e['strKey'].'" selected="selected">'.$e['strData'].'</option>';
				$strHTMLStatement .= '</select>';
			}

		}

		$arrTableData[$arrFieldList['title']] = $strHTMLStatement;

	# TEXT
	} else if($arrFieldList['field_type'] == 'TEXT') {
		$strHTMLClass = ''; $arrHTMLClass = array();
		# Determine the input class
		if($arrFieldList['allow_null'] == '0') $arrHTMLClass[] = 'required';

		switch($arrFieldList['validation']) {
			case 'DATETIME': $arrHTMLClass[] = 'jwDateTime half'; break;
			case 'DATE': $arrHTMLClass[] = 'jwDate half'; break;
			case 'TIME': $arrHTMLClass[] = 'jwTime half'; break;
			case 'DIGITS': $arrHTMLClass[] = 'digits'; break;
			case 'NUMBER': $arrHTMLClass[] = 'number'; break;
			case 'FLOAT': $arrHTMLClass[] = 'float'; break;
			case 'CURRENCY': $arrHTMLClass[] = 'currency'; break;
			case 'LOGIN': $arrHTMLClass[] = 'login'; break;
			case 'EMAIL': $arrHTMLClass[] = 'email'; break;
			case 'URL': $arrHTMLClass[] = 'url'; break;
		}

		if(empty($arrFieldList['description'])) $arrFieldList['description'] = 256;
        if(is_numeric($arrFieldList['description']) == TRUE && $arrFieldList['description'] <= 32) $arrHTMLClass[] = 'half';

		$strHTMLClass = ' class="form-control '.implode(' ',$arrHTMLClass).'"'.' placeholder="'.$arrFieldList['name'].'"';

		# Change with form input statements
		if(compareData($arrFieldList['validation'],array('DATETIME','DATE','TIME'))) {
			if(empty($mixTableData)) $mixTableData = date('Y/m/d H:i:s');
			else switch($arrFieldList['validation']) {
				case 'DATETIME': $mixTableData = formatDate2($mixTableData,'Y/m/d H:i:s'); $strInputType = 'datetime-local'; break;
				case 'DATE': $mixTableData = formatDate2($mixTableData,'Y/m/d'); $strInputType = 'date'; break;
				case 'TIME': $mixTableData = formatDate2($mixTableData,'H:i:s'); $strInputType = 'time'; break;
			}
			$strInputType = "text";
			$strInstruction = '<br />Ex: 1999/12/31 23:59';
			$strHTMLStatement = '<input type="'.$strInputType.'" name="'.$arrFieldList['title'].$intNumber.'"  value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="19" placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'NUMBER' || $arrFieldList['validation'] == 'DIGITS') {
			$strInstruction = '<br />Use only digits (0-9)';
			$strHTMLStatement = '<input type="number" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'FLOAT') {
			$strInstruction = '<br />Use only decimal type (0-9.)';
			$strHTMLStatement = '<input type="text" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'CURRENCY') {
			$strInstruction = '<br />Use only number character (0-9)';
			$strHTMLStatement = '<input type="text" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'LOGIN') {
			$strInstruction = '<br />Use UNIQUE ID by Mixing Lower Case(a-z), Upper Case(A-Z), Numbers(0-9) and Signs(!@#$%^&*)';
			$strHTMLStatement = '<input type="text" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' minlength="6" maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" /><span class="error"></span>';

		} else if($arrFieldList['validation'] == 'EMAIL') {
			$strInstruction = '<br />Use valid email character (a-z, A-Z, 0-9, -[dash], _[underscore],.[dot]) and form it properly (ex: my.email-1@mysite.com)';
			$strHTMLStatement = '<input type="email" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'PHONE') {
			$strInstruction = '<br />Use valid phone character (0-9, +) and form it properly (ex: +62312345678)';
			$strHTMLStatement = '<input type="tel" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' placeholder="'.$arrFieldList['name'].'" />';

		} else if($arrFieldList['validation'] == 'URL') {
			$strInstruction = '<br />Use valid URL(ex:http:#www.example.com) and URL character (a-z, A-Z, 0-9, -[dash], _[underscore])';
			$mixTableData = str_replace('<jw:site_url>',base_url(),$mixTableData);
			$strHTMLStatement = '<input type="url" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		} else {
			$strHTMLStatement = '<input type="text" name="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'"'.$strHTMLClass.' maxlength="'.$arrFieldList['description'].'" placeholder="'.$arrFieldList['name'].'" />';

		}

	# TEXTAREA
	} else if($arrFieldList['field_type'] == 'FILE' || ($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'FILE')) {
       $strInstruction = '<br />To add new file, use the File Manager and click "add" button.<br />To delete, choose the image number, and click "delete" button.';

       if($mixTableData != '') {
			$arrHTMLContent = explode('; ',$mixTableData);
			for($intHTMLIndex = 0; $intHTMLIndex < count($arrHTMLContent); $intHTMLIndex++)
				$arrHTMLContent[$intHTMLIndex] = str_replace('<jw:site_url>',base_url(),$arrHTMLContent[$intHTMLIndex]);

			$mixTableData = implode('; ',$arrHTMLContent);
		}

        $strName = $arrFieldList['title'].$intNumber;
        $strID = str_replace(array('[', ']'), array('-', ''), $strName);
        if($arrFieldList['description'] == 'IMAGE') $strType = '1';
        else $strType = '2';

		$strHTMLStatement = '
<div class="imgList" id="imglist-'.$strID.'"></div>
<div class="imgListDesc">
	<p class="pull-right"><span class="btn btn-default"><i class="fa fa-exchange"></i></span></p>
</div>
<textarea name="'.$strName.'" id="'.$strID.'" class="taFileList'.(($arrFieldList['allow_null'] == '0')? ' required' : '').'">'.$mixTableData.'</textarea>
<input type="hidden" id="countfile-'.$strID.'" value="'.$arrFieldList['description'].'" />
<div class="input-group">
    <input type="text" class="form-control txtFileAdd" id="inptext-'.$strID.'" placeholder="Add New File" />
    <span class="input-group-btn">
        <a class="btn btn-info btnFileBrowse" href="'.base_url('asset/responsivefilemanager/dialog.php').'?type='.$strType.'&field_id=inptext-'.$strID.'" type="button">Browse</a>
        <button class="btn btn-primary btnFileAdd" type="button" id="inpbut-'.$strID.'">Add</button>
    </span>
</div>';

    } else if($arrFieldList['field_type'] == 'TEXTAREA') {
		$arrHTMLClass = array();
		$strHTMLClass = '';
		if($arrFieldList['allow_null'] == '0') $arrHTMLClass[] = 'required';
		if($arrFieldList['validation'] == 'HTMLEDITOR') $arrHTMLClass[] = 'jwHTMLEditor';
		if($arrFieldList['validation'] == 'AUTOCOMPLETE' || $arrFieldList['validation'] == 'AUTOCOMPLETEID') 
            $arrHTMLClass[] = 'jwAutoComplete';
		if(count($arrHTMLClass) > 0) $strHTMLClass = ' class="form-control '.implode(" ",$arrHTMLClass).'"';

		if($arrFieldList['validation'] == 'CHECKBOXLIST' || $arrFieldList['validation'] == 'MULTIPLESELECT') {
			# Search the table name
			$_CI->MAdmTableList->search($arrFieldList['description']);
			$admTableListProperties = $_CI->MAdmTableList->getNextRecord('Array');

			# Custom query?
			if(!empty($admTableListProperties['fk_query'])) {
				if(!empty($arrFieldList['custom_query']))
					$admTableListProperties['fk_query'] = parseAdmQuery(
						$admTableListProperties['fk_query'],$arrFieldList['custom_query'],
						$arrFieldList,$mixTableData
					);
				$_CI->MForeignTable->initialize($admTableListProperties['title'],$admTableListProperties['fk_query']);
			} else $_CI->MForeignTable->initialize($admTableListProperties['title']);

			# Generate all data in the foreign table
			$_CI->MForeignTable->dbSelect($admTableListProperties['fk_display_fields']);
			$arrFK = $_CI->MForeignTable->getQueryResult('Array');

			# Because the mixTableData contains all of the table data, we must convert it
			if(!empty($mixTableData)) {
				$mixTableData = $mixTableData[$arrFieldList['title']];
				$arrMixTableData = explode('; ',$mixTableData);
			} else { $arrMixTableData = array(); }

			if($arrFieldList['validation'] == 'CHECKBOXLIST') {
				$strHTMLStatement = '
<input type="hidden" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'" />
<input type="hidden" name="checkboxNum'.$arrFieldList['title'].$intNumber.'" id="checkboxNum'.$arrFieldList['title'].$intNumber.'" value="'.count($arrFK).'" />
<label><input type="checkbox" name="selectAll'.$intNumber.'" id="checkboxSelectAll'.$intNumber.'" value="'.$arrFieldList['title'].$intNumber.'" class="cblAll" /> All</label>';

				$i = 0;
				foreach($arrFK as $e) {
					if(array_search($e['strKey'],$arrMixTableData) === FALSE) $strChecked = '';
                    else $strChecked = ' checked="checked"';

					$strHTMLStatement .= '<label><input type="checkbox" name="'.$arrFieldList['title'].$intNumber.'_'.$i.'" id="checkbox'.$arrFieldList['title'].$intNumber.'-'.$i.'" value="'.$e['strKey'].'"'.$strChecked.' class="cbl" data-targetid="'.$arrFieldList['title'].$intNumber.'" /> '.$e['strData'].'</label>';

					$i++;
				}

			} else if($arrFieldList['validation'] == 'MULTIPLESELECT') {
				$strInstruction = '<br />Select multiple items by hold CTRL button and click desired item';

				$strHTMLStatement = '<input type="hidden" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'" value="'.$mixTableData.'" /><select id="'.$arrFieldList['title'].$intNumber.'-list" multiple="multiple" class="form-control multipleSelect" placeholder="'.$arrFieldList['name'].'" data-placeholder="Choose '.$arrFieldList['name'].'">';

				foreach($arrFK as $e) {
					if(array_search($e['strKey'],$arrMixTableData) === FALSE) $strSelected = '';
                    else $strSelected = ' selected="selected"';

                    $strHTMLStatement .= '<option value="'.$e['strKey'].'"'.$strSelected.' />'.$e['strData'].'</option>';
				}
				$strHTMLStatement .= '</select>';
			}

		} else if($arrFieldList['validation'] == 'HTMLEDITOR') {
			$strInstruction = '<br />To get help about how to use this Text Editor <a href="http:#tinymce.moxiecode.com" target="_blank">click here</a><br />To use new line instead of new paragraph, use shift + enter';
			$mixTableData = str_replace('<jw:site_url>',base_url(),$mixTableData);
			$strHTMLStatement = '<textarea name="'.$arrFieldList['title'].$intNumber.'"'.$strHTMLClass.' id="taHTMLEditor'.$arrFieldList['title'].$intNumber.'" placeholder="'.$arrFieldList['name'].'">'.$mixTableData.'</textarea>';

		} else if($arrFieldList['validation'] == 'AUTOCOMPLETE' || $arrFieldList['validation'] == 'AUTOCOMPLETEID') {
			$strInstruction = '<br />After type a word, select one of the suggestions available';

            if($arrFieldList['validation'] == 'AUTOCOMPLETEID') $strSaveAs = ' data-saveas="id"';
            else $strSaveAs = ' data-saveas="text"';

			$strHTMLStatement = '
<div class="row">
    <div class="col-md-6"><div class="input-group">
        <input type="text" data-targetid="'.$arrFieldList['title'].$intNumber.'"'.$strHTMLClass.$strSaveAs.' data-link="'.base_url('adminpage/autocomplete/getData/'.$arrFieldList['description']).'" placeholder="'.$arrFieldList['name'].'" />
        <span class="input-group-addon"></span>
    </div></div>
    <div class="col-md-6">
        <textarea type="text" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'" class="form-control" onkeyup="checkSemicolon(this.id,event)" />'.$mixTableData.'</textarea>
    </div>
</div>';

		} else if($arrFieldList['validation'] == 'LIST') {
			if(!empty($arrFieldList['description'])) $arrTempFieldList = json_decode($arrFieldList['description'], true);
			if(!empty($mixTableData)) $arrTempTableData = json_decode($mixTableData, true);

			$strHTMLStatement = '
<div class="table-responsive jwTableDescriptionList">
	<table class="table">
		<thead>
			<tr>
				<th class="counter"></th>';
			if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $e) {
				$strHTMLStatement .= '<th>'.$e['name'].'</th>';
			}
			$strHTMLStatement .= '
				<th class="action">Action</th>
			</tr>
		</thead>
		<tfoot>
			<tr class="added">
				<td class="counter"></td>';
			if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $i => $e) {
				$e['title'] = $arrFieldList['title'].$intNumber.'['.$i.'][]';
				$strHTMLStatement .= '<td>'.translateDataIntoHTMLStatements($e, '').'</td>';
			}
			$strHTMLStatement .= '
				<td class="action"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button></td>
			</tr>
		</tfoot>
		<tbody>';
			if(!empty($arrTempTableData)) foreach($arrTempTableData as $e) {
				$strHTMLStatement .= '
			<tr>
				<td class="counter"></td>';
				if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $j => $f) {
					$f['title'] = $arrFieldList['title'].$intNumber.'['.$j.'][]';
					$strHTMLStatement .= '<td>'.translateDataIntoHTMLStatements($f, $e[$j]).'</td>';
				}
				$strHTMLStatement .= '
				<td class="action"><button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></button></td>
			</tr>';
			}
			$strHTMLStatement .= '
		</tbody>
	</table>
</div>';

		} else if($arrFieldList['validation'] == 'MULTIVALUE') {
			if(!empty($arrFieldList['description'])) $arrTempFieldList = json_decode($arrFieldList['description'], true);
			if(!empty($mixTableData)) $arrTempTableData = json_decode($mixTableData, true);

			$strHTMLStatement = '';
			if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $i => $e) {
				$e['title'] = $arrFieldList['title'].$intNumber.'['.$i.']';
				$strHTMLStatement .= '
<div class="form-group">
	<label>'.$e['name'].' '.(empty($e['allow_null']) ? '*' : '').'</label>
	'.translateDataIntoHTMLStatements($e, $arrTempTableData[$i]).'
	<div class="clearfix"></div>
</div>';
			}

		} else {
			$strHTMLStatement = '<textarea name="'.$arrFieldList['title'].$intNumber.'"'.$strHTMLClass.' class="form-control" placeholder="'.$arrFieldList['name'].'">'.html_entity_decode($mixTableData).'</textarea>';
		}

	# PASSWORD

	} else if($arrFieldList['field_type'] == 'PASSWORD') {

		if($intNumber != "") $strInstruction = '<br />Leave blank if you don\'t want to change';

        $strHTMLStatement = 
        	'<input type="password" name="'.$arrFieldList['title'].$intNumber.'" id="'.$arrFieldList['title'].$intNumber.'" placeholder="'.$arrFieldList['name'].'" class="form-control" />'.
        	'<input type="password" name="pwdReType'.$arrFieldList['title'].$intNumber.'" id="pwdReType'.$arrFieldList['title'].$intNumber.'" placeholder="Confirm '.$arrFieldList['name'].'" class="form-control" equalTo="#'.$arrFieldList['title'].$intNumber.'" />';

	}

	# Instruction
	if(!empty($arrFieldList['instruction'])) { 
		if(!empty($strInstruction)) $strInstruction = $arrFieldList['instruction'].$strInstruction; 
		else $strInstruction = $arrFieldList['instruction'];
	} else if(empty($strInstruction)) $strInstruction = '';

	if(!empty($strInstruction)) $strInstruction = '<InStRuCtIoN>'.$strInstruction.'</InStRuCtIoN>';

	return $strHTMLStatement.$strInstruction;
}

# Translate the form data into readable statements
function translateDataIntoReadableStatements($arrFieldList,$mixTableData = '') {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','MAdmTableList');
	$_CI->MAdmTableList->initialize('adm_table_list');
	$_CI->load->model('Mdbvo','MForeignTable');
	$_CI->load->model('Mtinydbvo','MForeignTinyDB');
	$strReadableStatement = '';

	# To change the foreign key
	if($arrFieldList['field_type'] == 'SELECT') {
		# If the foreign key is a table
		if(is_numeric($arrFieldList['description']) == TRUE) {
			# Search the table name
			$_CI->MAdmTableList->search($arrFieldList['description']);
			$admTableListProperties = $_CI->MAdmTableList->getNextRecord('Array');

			# Custom query?
			if(!empty($admTableListProperties['fk_query'])) {
				if(!empty($arrFieldList['custom_query']))
					$admTableListProperties['fk_query'] = parseAdmQuery(
						$admTableListProperties['fk_query'],$arrFieldList['custom_query'],
						$arrFieldList,$mixTableData
					);
				$_CI->MForeignTable->initialize($admTableListProperties['title'],$admTableListProperties['fk_query']);
			} else $_CI->MForeignTable->initialize($admTableListProperties['title']);

			# Generate all data in the foreign table to build select statements
			$strReadableStatement = '';
			if(!empty($mixTableData)) if(is_array($mixTableData)) {
				$_CI->MForeignTable->dbSelect($admTableListProperties['fk_display_fields'],"id IN (".implode(',',$mixTableData).")");
				$mixTableData = array();
				foreach($_CI->MForeignTable->getQueryResult('Array') as $e) $mixTableData[$e['strKey']] = $e['strData'];
				$strReadableStatement = $mixTableData;
			} else {
				$_CI->MForeignTable->dbSelect($admTableListProperties['fk_display_fields'],"id = ".$mixTableData);
				$arrFK = $_CI->MForeignTable->getNextRecord('Array');
				if(!empty($arrFK['strData'])) $strReadableStatement = $arrFK['strData'];
			}

		# If the foreign key is a tinydb
		} else {
			$_CI->MForeignTinyDB->initialize($arrFieldList['description']);
			$strReadableStatement =  $_CI->MForeignTinyDB->getSingleData($mixTableData);
		}

	# To display the image
	} else if($arrFieldList['field_type'] == 'FILE' || ($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'FILE')) {
		if(!is_array($mixTableData)) $arrImageFileName = explode('; ',$mixTableData);
		else $arrImageFileName = $mixTableData;
		$strReadableStatement = array();
		foreach($arrImageFileName as $e)
			$strReadableStatement[] = str_replace('<jw:site_url>',base_url(),$e);

	# To display the html script
	} else if($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'HTMLEDITOR')
		$strReadableStatement = str_replace('<jw:site_url>',base_url(),$mixTableData);

	else if($arrFieldList['field_type'] == 'TEXTAREA' && ($arrFieldList['validation'] == 'MULTIPLESELECT' || $arrFieldList['validation'] == 'CHECKBOXLIST')) {
		if(!empty($mixTableData)) {
			# Search the table name
			$_CI->MAdmTableList->search($arrFieldList['description']);
			$admTableListProperties = $_CI->MAdmTableList->getNextRecord('Array');

			# Custom query?
			if(!empty($admTableListProperties['fk_query'])) {
				if(!empty($arrFieldList['custom_query']))
					$admTableListProperties['fk_query'] = parseAdmQuery(
						$admTableListProperties['fk_query'],$arrFieldList['custom_query'],
						$arrFieldList,$mixTableData
					);
				$_CI->MForeignTable->initialize($admTableListProperties['title'],$admTableListProperties['fk_query']);
			} else $_CI->MForeignTable->initialize($admTableListProperties['title']);

			# Generate all data in the foreign table
			$strWhere = str_replace('; ',', ',$mixTableData);
			$_CI->MForeignTable->dbSelect($admTableListProperties['fk_display_fields'],"id IN ($strWhere)");
			$arrFK = $_CI->MForeignTable->getQueryResult('Array');

			foreach($arrFK as $e) if(empty($strReadableStatement)) $strReadableStatement = $e['strData'];
			else $strReadableStatement .= ', '.$e['strData'];

		} else $strReadableStatement = '';

	} else if($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'LIST') {
		if(!empty($arrFieldList['description'])) $arrTempFieldList = json_decode($arrFieldList['description'], true);
		if(!empty($mixTableData)) $arrTempTableData = json_decode($mixTableData, true);

		$strReadableStatement = array();
		if(!empty($arrTempTableData)) foreach($arrTempTableData as $i => $e)
			if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $j => $f)
				$strReadableStatement[$i][$j] = translateDataIntoReadableStatements($f, $e[$j]);

	} else if($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'MULTIVALUE') {
		if(!empty($arrFieldList['description'])) $arrTempFieldList = json_decode($arrFieldList['description'], true);
		if(!empty($mixTableData)) $arrTempTableData = json_decode($mixTableData, true);

		$strReadableStatement = array();
		if($arrTempFieldList) foreach($arrTempFieldList as $i => $e)
			$strReadableStatement[$i] = translateDataIntoReadableStatements($e, $arrTempTableData[$i]);

	} else if($arrFieldList['field_type'] == 'TEXTAREA') $strReadableStatement = nl2br($mixTableData);

	# If the value is a date
	else if($arrFieldList['field_type'] == 'TEXT' && $arrFieldList['validation'] == 'DATETIME')
		$strReadableStatement = formatDate2($mixTableData,'d F Y H:i');
	else if($arrFieldList['field_type'] == 'TEXT' && $arrFieldList['validation'] == 'DATE')
		$strReadableStatement = formatDate2($mixTableData,'d F Y');
	else if($arrFieldList['field_type'] == 'TEXT' && $arrFieldList['validation'] == 'TIME')
		$strReadableStatement = formatDate2($mixTableData,'H:i');

	# If the value is a url
	else if($arrFieldList['field_type'] == 'TEXT' && $arrFieldList['validation'] == 'URL')
		$strReadableStatement = str_replace('<jw:site_url>',base_url(),$mixTableData);

	else if($arrFieldList['field_type'] == 'TEXT' && $arrFieldList['validation'] == 'CURRENCY')
		$strReadableStatement = number_format($mixTableData,2,',','.');

	else $strReadableStatement = $mixTableData;

	return $strReadableStatement;
}

# Translate the form data into safe statements
function translateDataIntoSafeStatements($arrFieldList,$mixData = '',$strType = 'add') {
	$strSafeStatement = $mixData;

	if($arrFieldList['field_type'] == 'TEXT') {
		if($arrFieldList['validation'] == 'URL') {
			$strSafeStatement = str_replace(base_url(),'<jw:site_url>',$mixData);
		} else if($arrFieldList['validation'] == 'CURRENCY') {
			$strSafeStatement = str_replace(',','',$mixData);
		}
	} else if($arrFieldList['field_type'] == 'FILE' || ($arrFieldList['field_type'] == 'TEXTAREA' && $arrFieldList['validation'] == 'FILE')) {
			$strSafeStatement = str_replace(base_url(),'<jw:site_url>',$mixData);
	} else if($arrFieldList['field_type'] == 'TEXTAREA') {
		if($arrFieldList['validation'] == 'HTMLEDITOR') {
			$mixData = str_replace(base_url(),'<jw:site_url>',$mixData);
			$strSafeStatement = str_replace('../','',$mixData);
		} else if($arrFieldList['validation'] == 'MULTIPLESELECT' || $arrFieldList['validation'] == 'CHECKBOXLIST') {
			if(substr($mixData,strlen($mixData) - 2,2) == '; ') $strSafeStatement = substr($mixData,0,strlen($mixData) - 2);

		} else if($arrFieldList['validation'] == 'LIST') {
			if(!empty($arrFieldList['description'])) $arrTempFieldList = json_decode($arrFieldList['description'], true);
			$arrTempTableData = array();

			for ($i = 0; $i < count($mixData[key($mixData)]); $i++) {
				$arrTempData = array();
				if(!empty($arrTempFieldList)) foreach($arrTempFieldList as $j => $f)
					$arrTempData[$j] = translateDataIntoSafeStatements($f, $mixData[$j][$i]);
				$arrTempTableData[] = $arrTempData;
			}
			$strSafeStatement = json_encode($arrTempTableData);

		} else if($arrFieldList['validation'] == 'MULTIVALUE') {
			$arrTempFieldList = json_decode($arrFieldList['description'], true);
			$arrTempTableData = array();
			foreach($arrTempFieldList as $i => $e) {
				$arrTempTableData[$i] = translateDataIntoSafeStatements($e, $mixData[$i]);
			}
			$strSafeStatement = json_encode($arrTempTableData);

		} else $strSafeStatement = htmlentities($mixData);
	} else if($arrFieldList['field_type'] == 'PASSWORD') {
		$strSafeStatement = md5($mixData);
	}
	return $strSafeStatement;
}

function translateHTMLSyntaxIntoShortStatements($strHTMLSyntax,$intWordShowed = 50,$strSuffix = '') {
	# If short statements use page break
	if($intWordShowed == -1) {
		$intEndPos = strpos($strHTMLSyntax,'<hr />');
		if($intEndPos > 0) $strHTMLSyntax = substr($strHTMLSyntax,0,$intEndPos);
		return strip_tags($strHTMLSyntax,'<br><br />');
	}
	# If short statements don't use page break
	$strWords = strip_tags($strHTMLSyntax,'<br><br />');
	$intWordCount = str_word_count($strWords,0);
	$arrWords = str_word_count($strWords,2);
	if($intWordShowed < $intWordCount) {
		$arrKeys = array_keys($arrWords);
		$intEndPos = $arrKeys[$intWordShowed] - 1;
		$strWords = substr($strWords,0,$intEndPos).$strSuffix;
	}

	return str_replace(PHP_EOL, ' ', $strWords);
}

# Ex: <code sql="where"> AND prod_category_id = this.proc_parent_id</code>
function parseAdmQuery($strQuery,$strCustomQuery,$arrFieldList,$mixTableData) {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','MAdmTableList');
	$_CI->MAdmTableList->initialize('adm_table_list');
	$_CI->load->model('Mdbvo','MForeignTable');
	$arrDOM = array();
	$mDOM = new DOMDocument;
	$mDOM->loadHTML($strCustomQuery);
	foreach($mDOM->getElementsByTagName('code') as $node) {
		$eDOM = array();
		if($node->hasAttributes()) {
			foreach ($node->attributes as $attr) {
	            $eDOM[$attr->nodeName] = $attr->nodeValue;
	        }
		}
		$eDOM['text'] = strip_tags($mDOM->saveHTML($node));
		$arrDOM[] = $eDOM;
	}
	foreach($arrDOM as $e) {
		$strQuery = str_replace('<jw:sql_'.$e['sql'].'>', '<jw:sql_'.$e['sql'].'> '.$e['text'], $strQuery);
	}
	$_CI->MAdmTableList->search($arrFieldList['table_id']);
	$arrForeignTableList = $_CI->MAdmTableList->getNextRecord('Array');
	$_CI->MForeignTable->initialize($arrForeignTableList['title']);
	$_CI->MForeignTable->search($mixTableData['id']);
	$arrFK = $_CI->MForeignTable->getNextRecord('Array');
	if(!empty($arrFK)) {
		foreach($arrFK as $k => $e) {
			$strQuery = str_replace('{'.$k.'}', $e, $strQuery);
		}
	} else {
		$strQuery = preg_replace('\{.\}', '0', $strQuery);
	}

	return $strQuery;
}

function base64toImage($strBase64, $strTable = '', $strPre = '')
{
	if($strTable != '') $strUploadPath = 'upload/'.$strTable.'/';
	else $strUploadPath = 'upload/';
	if (!is_dir($strUploadPath)) mkdir('./'.$strUploadPath, 0777, true);
	define('UPLOAD_DIR', $strUploadPath);

    $image_base64 = base64_decode($strBase64);
    $file = UPLOAD_DIR .$strPre."_".uniqid() . '.png';    
    file_put_contents($file, $image_base64);

    return $file;
}