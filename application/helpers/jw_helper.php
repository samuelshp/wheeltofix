<?php

# This is to replace some text with programming
# Needed in getPostByID
# Put the needle vs haystack in the arrReplacement
# E.g. '[icon-phone]' => '<i class="fa fa-phone"></i>'
function getReplacementArray() {
	return array();
}

function replaceContent($strContent) {
	$strContent = preg_replace('/\[fa-(.+)\]/i', '<i class="fa fa-$1"></i>', $strContent);

	if($arrReplacement = getReplacementArray()) {
		$strContent = str_replace(array_keys($arrReplacement), array_values($arrReplacement), $strContent);
	}
	return $strContent;
}

function differOddWord($str) {
	$arr = explode(' ', $str);
	foreach($arr as $i => $e) if($i % 2 == 1) {
		$arr[$i] = '<span>'.$e.'</span>';
	}

	return implode(' ', $arr);
}

function generateCategoryBreadcrumb($intCategoryID, $bolAsHTML = TRUE) {
	$_CI =& get_instance();
	$_CI->load->model('Mproductcategory');
	$arr = array();

	while(!empty($intCategoryID)) {
		$_CI->Mproductcategory->dbSelect('proc_parent_id,proc_title', "id = $intCategoryID");
		if($_CI->Mproductcategory->getNumRows() > 0) $arrProductCategory = $_CI->Mproductcategory->getNextRecord('Array');
		if(!empty($arrProductCategory['proc_parent_id'])) {
			$intCategoryID = $arrProductCategory['proc_parent_id'];
			$arr[] = $arrProductCategory['proc_title'];

		} else $intCategoryID = 0;
	}

	if($bolAsHTML) return '<span>'.implode('</span><span>', array_reverse($arr)).'</span>';
	else return array_reverse($arr);
}