<?php

function compareData($strData,$arrCompareData,$strMode = 'in') {
	foreach($arrCompareData as $e)
		if($strMode == 'in' && $strData == $e) return true;
		else if($strMode == 'notin' && $strData == $e) return false;
	if($strMode == 'in') return false;
	else if($strMode == 'notin') return true;
}

function formatDate($strDateOnDatabase, $strFormat = 'd F Y H:i', $strDateType = '') {
	if(!empty($strDateType)) $strFormat = $strDateType;

	switch($strFormat) {
		case 'DATE': return formatDate2($strDateOnDatabase,'d F Y'); break;
		case 'TIME': return formatDate2($strDateOnDatabase,'H:i'); break;
		case 'DATETIME': return formatDate2($strDateOnDatabase,'d F Y H:i'); break;
		default: return formatDate2($strDateOnDatabase,$strFormat); break;
	}
}

function formatDate2($strAnyDate,$strFormat = 'd F Y H:i') {
	if(empty($strAnyDate)) return '';
	return date($strFormat,strtotime($strAnyDate));
}

function searchInArray($arrData,$strSearchedIndex,$strSearchedValue) {
	foreach ($arrData as $e) if ($e[$strSearchedIndex] === $strSearchedValue) {
		return $e;
	}

	return false;
}

function searchAllValuesInArray($arrData,$strSearchedIndex) {
	$arrReturn = array();
	foreach ($arrData as $e) if(!empty($e[$strSearchedIndex])) $arrReturn[] = $e[$strSearchedIndex];

	return array_unique($arrReturn);
}

function strposa($haystack, $needles=array(), $offset=0) {
	$chr = array();
	foreach($needles as $needle) {
		$res = strpos($haystack, $needle, $offset);
		if ($res !== false) $chr[$needle] = $res;
	}
	if(empty($chr)) return false;
	return min($chr);
}

function findInJson($haystack, $needle) {
	$arr = json_decode($haystack, true);
	return $arr[$needle];
}

/* End of File */