<?php

function ArrayToCsv($arrData,$strFileName = 'data.csv') {
    $_CI =& get_instance();
    $_CI->load->helper('file');
    $_CI->load->helper('download');
    
    $strTitle = ''; $strData = ''; $i = 0;
    
    foreach($arrData as $a) {
        $s = '';
        foreach($a as $t => $d) {
            if($i == 0) if(empty($strTitle)) $strTitle .= $t; else $strTitle .= ';'.$t;
                
            if(empty($s)) $s .= $d; else $s .= ';'.$d;
        }
        
        $strData .= $s."\r\n";
        $i++;
    }
    
    force_download($strFileName, $strTitle."\r\n".$strData);
}

function ArrayToExcel($arrData,$strTitle = 'Data',$strFileName = 'data.xls') {
    $_CI =& get_instance();
    $_CI->load->helper('download');

    $strData = _prepareData($arrData,$strTitle);

    //header("Content-Length: ".(strlen($strData) * 8));
    header('Content-Type: application/vnd.ms-excel');
    //header('Content-Disposition: attachment; filename='.$strFileName);

    force_download($strFileName, $strData);
}

# ATTENTION: To use this function please extract mpdf files from backup/resource/php to application/library folder
function ArrayToPdf($arrData,$strTitle = 'Data',$strFileName = 'data.pdf') {
    $_CI =& get_instance();
    // $_CI->load->library('mpdf');
    
    // $strData = _prepareData($arrData,$strTitle);
    
    // $_CI->mpdf->WriteHTML($strData);
    // $_CI->mpdf->Output();
}

function ArrayToXml($arrData) {
    $dataXML = new SimpleXMLElement("<channel></channel>");
    
    _prepareXMLData($arrData,$dataXML);
    
    header("Content-Type: application/xml");
    print $dataXML->asXML();
}

function HTMLToExcel($strHTML,$strFileName = 'data.xls') {
    $_CI =& get_instance();
    $_CI->load->helper('download');

    //header("Content-Length: ".(strlen($strData) * 8));
    header('Content-Type: application/vnd.ms-excel');
    //header('Content-Disposition: attachment; filename='.$strFileName);

    force_download($strFileName, $strHTML);
}

function HTMLToPDF($strHTML,$strFileName = 'data.pdf') {
    $_CI =& get_instance();
    // $_CI->load->library('mpdf');
    
    // $_CI->mpdf->WriteHTML($strHTML);
    // $_CI->mpdf->Output();
}

function _prepareData($arrData,$strTitle) {
    $intLength = count($arrData[0]);

    $strData = '
<table border="1" cellspacing="0">
    <tr><td style="background-color:#aaa;" colspan="'.$intLength.'"><h1>'.$strTitle.'</h1></td></tr>
    <tr class="header">';

    foreach($arrData[0] as $t => $d) $strData .= '
        <td style="background-color:#ccc;">'.$t.'</td>';

    $strData .= '
    </tr>';

    foreach($arrData as $a) {
        $strData .= '
    <tr>';
        foreach($a as $t => $d) $strData .= '
        <td>'.$d.'</td>';

        $strData .= '
    </tr>';
    }

    return $strData . '
</table>';
}

function _prepareXMLData($dataPHP,&$dataXML) {
    foreach($dataPHP as $key => $value) {
        if(is_array($value)) {
            $key = is_numeric($key) ? "item" : $key;
            $subnode = $dataXML->addChild("$key");
            _prepareXMLData($value, $subnode);
        }
        else {
            $key = is_numeric($key) ? "item" : $key;
            $child = $dataXML->addChild("$key");
            $child->value = $value;;
        }
    }
}