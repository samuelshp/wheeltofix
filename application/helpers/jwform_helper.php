<?php

/*
|--------------------------------------------------------------------------
| RECAPTCHA - How To Use
|--------------------------------------------------------------------------
|
| <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
| <div class="g-recaptcha" data-sitekey="<?=RECAPTCHA_SITE_KEY?>"></div>
|
*/
function checkCaptcha($strRedirect = '') {
    $_CI =& get_instance();

	generateRepopulate();

	if(isset($_POST['g-recaptcha-response']) && constant('ENVIRONMENT') != 'development') {
		require_once('asset/recaptchalib.php');
		$response = null;
		$reCaptcha = new ReCaptcha(RECAPTCHA_SECRET_KEY);
		$response = $reCaptcha->verifyResponse($_SERVER['REMOTE_ADDR'],$_CI->input->post('g-recaptcha-response'));

		if(empty($response) || !$response->success) {
			$_CI->session->set_flashdata('strMessage',$_CI->lang->jw('wrong_captcha'));

			if(empty($strRedirect)) redirect(str_replace(site_url(),'',$_SERVER['HTTP_REFERER']));
			else redirect($strRedirect);
		}
	}
}

function generateRepopulate() {
	$_CI =& get_instance();
	$arrPostData = $_CI->input->post(); 
	$arrRepopulate = array();

    foreach($arrPostData as $k => $e) $arrRepopulate[$k] = $e;

    $_CI->session->set_flashdata('arrRepopulate',$arrRepopulate);
}

function repopulate($strSearchedIndex,$strDefaultValue = '') {
    $_CI =& get_instance();
    $arrRepopulate = $_CI->session->flashdata('arrRepopulate');

    if(isset($arrRepopulate[$strSearchedIndex])) return $arrRepopulate[$strSearchedIndex];
    return $strDefaultValue;
}

function upload($strPath = '', $arrConfig = array()) {
    $_CI =& get_instance();
	$arrResult = array();

	if(count($_FILES) > 0) {
		$strPath = 'upload/'.$strPath;
		if (!is_dir($strPath)) mkdir('./'.$strPath, 0777, true);
		$arrConfig = array_merge(array('upload_path' => $strPath), $arrConfig);
		$_CI->load->library('upload', $arrConfig);
		foreach($_FILES as $i => $e) {
			if($arrUploadData = $_CI->upload->do_upload($i))
				$arrResult[$i] = translateDataIntoSafeStatements(array('field_type' => 'FILE'), base_url($strPath.'/'.$arrUploadData['file_name']));
		}
	}

	return $arrResult ? $arrResult : false;
}

# Replace upload function above for Pillarsoft
function uploadAttachment($attachmentType, $intTransactionReferalID)
{		
	if (count($_FILES) > 0) {
		$_CI =& get_instance();
		$strUploadPath = 'upload/attachment';
		$_CI->load->model('Mattachment');
	    $_CI->load->library('upload',array('upload_path' => $strUploadPath, 'max_size' => 1000, 'file_name' => $attachmentType."_".uniqid()));
	    $_CI->load->library('upload', $arrConfig);
	    $fileUpload = $_CI->upload->do_upload('attachedFile');

	    $arrFileData = array(
	        'attachment_type' => $attachmentType,
	        'ref_id' => $intTransactionReferalID,
	        'path' => translateDataIntoSafeStatements(['field_type' => 'FILE'], base_url($strUploadPath."/".$fileUpload['file_name']))
	    );
	    
	    $dbInsert = $_CI->Mattachment->upload($arrFileData);
	    
	    if (count($fileUpload) > 1) {

	        if (!$dbInsert) {
	            $_CI->upload->do_delete($fileUpload['full_path']);
	            return ['success' => false, 'msg' => 'Lampiran Gagal diupload, terjadi kesalahan menyimpan pada database'];
	        } else {	            
	            return ['success' => true, 'msg' => 'Lampiran Berhasil diupload'];
	        }
	        
	    }else{	        
	        return ['success' => false, 'msg' => $upload];
	    }
	}
}