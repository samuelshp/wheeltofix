<?php
/*
FUNCTION:
- formatUnitName($intUnitID)
- formatProductUnit(intProductID,intQuantity,strMode)
- formatProductName(strMode,strProductName,strBrandName,strCategoryName,intPrice,intStock)
- formatPersonName(strMode,strName,strAddress,strCity,strPhone)
- countSubTotal(intPrice,intQuantity,intDiscount,intTax)
- countStock(intProductID,intWarehouseID)
- convertQuantity(intQuantity,intProductID,strMode)
- compileItems(arrItem)
- breakdownItems(strItems)
- generateBarcodeNumber(strPrefix,intProductID)
- decodeBarcodeNumber(strNumber)
- generateTransactionCode(strRawDate,intInvoiceID,strMode)
*/

function formatUnitName($intUnitID) {
    $_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)

    $_CI->load->model('Munit'); $strUnit = $_CI->Munit->getName($intUnitID);

    if(!empty($strUnit) && $strUnit != '-') return $strUnit;
    else return '';
}

function formatProductUnit($intProductID,$intQuantity,$strMode = 'concluded') {
	$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)
	
	$_CI->load->model('Munit'); 

	if($strMode == 'separated') {
		$arrUnit = $_CI->Munit->getUnitsForCalculation($intProductID);
		if(!is_array($intQuantity)) $arrQuantity = convertQuantity($intQuantity,$intProductID,'display');
		else $arrQuantity = $intQuantity;

		$arrQty = array();		
		$arrQty[] = $arrQuantity['intQty'].' '.$arrUnit[2]['unit_title'];

		return implode(' + ', $arrQty);		
		
	} else {
		$strUnit = $_CI->Munit->getBaseUnit($intProductID);
		
		if(!empty($strUnit)) return $intQuantity.' '.$strUnit;
		else return $intQuantity;
	}
}

function formatProductName($strMode = 'NORMAL',$strProductName,$strBrandName = '',$strCategoryName = '',$intPrice = 0,$intStock = 0) {
	//if(!empty($strBrandName)) $strProductName = $strBrandName.' '.$strProductName;
	
	$strDescription = '';
	
	if($strMode == 'ABBR') {
		if(!empty($strCategoryName)) $strDescription .= ' &diams; '.$strCategoryName;
		if(!empty($intPrice)) $strDescription .= ' &#164; '.$intPrice;
		if(!empty($intStock)) $strDescription .= ' &#10004; '.$intStock;
		
		return '<abbr title="'.$strDescription.'">'.$strProductName.'</abbr>';
	} else {
		//if(!empty($strCategoryName)) $strDescription .= ' | '.$strCategoryName;
		//if(!empty($intPrice)) $strDescription .= ' | '.$intPrice;
		//if(!empty($intStock)) $strDescription .= ' | '.$intStock;
	
		return $strProductName.$strDescription;
	}
}

function formatPersonName($strMode = 'NORMAL',$strName = '',$strAddress = '',$strCity = '',$strPhone = '') {
	$strDescription = '';
	
	if($strMode == 'ABBR') {
		if(!empty($strAddress) && !empty($strCity)) $strDescription .= ' &#127968; '.$strAddress.', '.$strCity.'. ';
		if(!empty($strPhone)) $strDescription .= ' &#9742; '.$strPhone.'. ';
		
		return '<abbr title="'.$strDescription.'">'.$strName.'</abbr>';
	} else {
		if(!empty($strAddress) && !empty($strCity)) $strDescription .= ' | '.$strAddress.', '.$strCity.'. ';
		if(!empty($strPhone)) $strDescription .= ' | '.$strPhone.'. ';
	
		return $strName.$strDescription;
	}
}

function countSubTotal($intPrice,$intQuantity,$intDiscount = 'NODATA',$intTax = 'NODATA',$isPercent='NODATA') {
	$intSubtotal = $intPrice * $intQuantity;
	if($intDiscount != 'NODATA'){
        if($isPercent!='NODATA'){
            $intSubtotal = $intSubtotal-(($intSubtotal * $intDiscount) / 100);
        }
        else{
            $intSubtotal = $intSubtotal - $intDiscount;
        }
    }
	if($intTax != 'NODATA') $intSubtotal = $intSubtotal+(($intSubtotal * $intTax) / 100);
	
	return $intSubtotal;
}

function countStock($intProductID,$intWarehouseID = 0, $arrDateRange = []) {
	$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)

    $_CI->load->model('Mstock');
    if (!empty($arrDateRange)) {
    	$intTotalStock = $_CI->Mstock->getStockHistory($intProductID,$intWarehouseID,$arrDateRange);
    }else{
    	$intTotalStock = $_CI->Mstock->getQuantityByProductID($intProductID,$intWarehouseID);	
    }
    

	return $intTotalStock;
}

function convertQuantity($intQuantity,$intProductID,$strMode = 'concluded',$strSatuanType = '') {
	$_CI =& get_instance(); // This must be called in every function (don't place it in constructor because PHP4 create the instance right after the controller created)
	
    $_CI->load->model('Munit');
	if($strMode == 'display') { // $strMode == 'separated'
		return round($_CI->Munit->convertToDisplayQuantity($strSatuanType,$intQuantity,$intProductID),2); // convertToSeparatedQuantity($intQuantity,$intProductID)
	} elseif($strMode == 'concluded') {		
		return round($_CI->Munit->convertToBaseQuantity($strSatuanType,$intQuantity,$intProductID),2);

	} else return false;
}

function compileItems($arrItem) {
	// Format data
	$strItems = '';
	for($i = 0; $i < count($arrItem); $i++) {
		// Select product
		$strItems .= $arrItem[$i]['intKey'].'('.$arrItem[$i]['intData'].')';
		if($i < count($arrItem) - 1) $strItems .= '; ';
	}
	return $strItems;
}

function breakdownItems($strItems) {
	if(empty($strItems)) return false;
	
	$arrItem = explode('; ',$strItems);
	for($i = 0; $i < count($arrItem); $i++) {
		$arrItemTemp = explode('(',$arrItem[$i]);
		$arrItemTemp[1] = str_replace(')','',$arrItemTemp[1]);
		$arrItem[$i] = array('intKey' => $arrItemTemp[0], 'intData' => $arrItemTemp[1]); 
	}
	return $arrItem;
}

function generateBarcodeNumber($strPrefix = '',$intProductID) {
	return $strPrefix.str_pad($intProductID,6,'0',STR_PAD_LEFT);
}

function decodeBarcodeNumber($strNumber) {
	return preg_replace(array('/\D/','/^[0]*/'),'',$strNumber);
}

function generateTransactionCode($strRawDate,$intTransactionID = '',$strMode = 'invoice',$bolManualInsert = FALSE) {
	$strConfigCodeFormat = getTinyDbValue('jwdefaultconfig', 'JW_CODE_FORMAT');
	if($strConfigCodeFormat == 'trios' && $bolManualInsert) return '';

	$strDate = formatDate2($strRawDate,'y/m/d');

	switch($strMode) {
		case 'sale_order': case 'invoice_order': $strMode = 'SO'; $strTable = 'invoice_order'; $strFieldPre = 'inor'; break;
		case 'quotation': $strMode = 'QT'; $strTable = 'quotation'; $strFieldPre = 'quot'; break;
		case 'invoice': $strMode = 'INV'; $strTable = 'invoice'; $strFieldPre = 'invo'; break;
		case 'purchase_invoice': $strMode = 'PI'; $strTable = 'purchase_invoice'; $strFieldPre = 'pinv'; break;
		case 'purchase': $strMode = 'OP'; $strTable = 'purchase'; $strFieldPre = 'prch'; break;
        case 'purchase_order': $strMode = 'PB'; $strTable = 'purchase_order'; $strFieldPre = 'pror'; break;
		case 'acceptance' : $strMode = 'ACC'; $strTable = 'acceptance'; $strFieldPre = 'acce'; break;
		case 'adjustment': $strMode = 'AJ'; $strTable = 'adjustment'; $strFieldPre = 'ajst'; break;
		case 'kontrak' : $strMode = 'KON'; $strTable = 'kontrak'; $strFieldPre = 'kont'; break;
		case 'kontrak_pembelian' : $strMode = 'KP'; $strTable = 'kontrak_pembelian'; $strFieldPre = 'kopb'; break;
		case 'downpayment' : $strMode = 'DP'; $strTable = 'downpayment'; $strFieldPre = 'dpay'; break;
		case 'permintaan_pembayaran' : $strMode = 'PP'; $strTable = 'permintaan_pembayaran'; $strFieldPre = 'perbayar'; break;
		case 'daftar_pembayaran_hutang' : $strMode = 'DPH'; $strTable = 'daftar_pembayaran_hutang'; $strFieldPre = 'dphu'; break;
		case 'pembayaran_hutang' : $strMode = 'PHU'; $strTable = 'transaction_out'; $strFieldPre = 'txou'; break;
		case 'pemasukkan_lain_lain' : $strMode = 'PML'; $strTable = 'transaction_in'; $strFieldPre = 'txin'; break;
		case 'pengeluaran_lain_lain' : $strMode = 'PGL'; $strTable = 'transaction_out'; $strFieldPre = 'txou'; break;
		case 'nota_kredit_supplier' : $strMode = 'NKS'; $strTable = 'transaction_out'; $strFieldPre = 'txou'; break;
		case 'nota_debet_supplier' : $strMode = 'NDS'; $strTable = 'transaction_out'; $strFieldPre = 'txou'; break;		
		case 'uang_masuk_customer' : $strMode = 'UMC'; $strTable = 'uang_masuk_customer'; $strFieldPre = 'umcu'; break;
		case 'pembayaran_piutang' : $strMode = 'PPI'; $strTable = 'transaction_in'; $strFieldPre = 'txin'; break;
		case 'jurnal_memorial': $strMode = 'JM'; $strTable = 'jurnal'; $strFieldPre = 'jrnl'; break;
		case 'jurnal_penyesuaian': $strMode = 'JP'; $strTable = 'jurnal'; $strFieldPre = 'jrnl'; break;
		case 'jurnal_persediaan_akhir_tahun': $strMode = 'JPAT'; $strTable = 'jurnal'; $strFieldPre = 'jrnl'; break;
		case 'pemakaian': $strMode = 'USAGE'; $strTable = 'pemakaian'; $strFieldPre = 'pemakaian'; break;
		case 'picklist': $strMode = 'PCLS'; $strTable = 'pick_list'; $strFieldPre = 'pcls'; break;
		default: $strMode = 'PJ'; break;
	}

	if($strMode == 'product') return str_pad($intTransactionID,4,'0',STR_PAD_LEFT);
	elseif($strConfigCodeFormat == 'simple+date')
		return $strMode.'-'.$strDate.'/'.str_pad($intTransactionID,6,'0',STR_PAD_LEFT);
	else {
		//return $strMode.'-'.str_pad($intTransactionID,6,'0',STR_PAD_LEFT); ini yang lama ko, aku pakai bawah untuk SBM
		$date_baru = explode('/', $strDate);
		$tahun = $date_baru[0];
		$bulan = $date_baru[1];

		if(empty($intTransactionID)) {
			$CI =& get_instance();
			$CI->load->model('Mdbvo', 'Mtransaction');
			$CI->Mtransaction->setQuery("SELECT ".$strFieldPre."_code AS code FROM $strTable WHERE ".$strFieldPre."_code LIKE '$strMode/$tahun/$bulan/%' ORDER BY code DESC");
			if($CI->Mtransaction->getNumRows() > 0)
				$strCode = $CI->Mtransaction->getNextRecord('Object')->code;
			else $strCode = 0;
			$intLastID = str_replace("$strMode/$tahun/$bulan/", '', $strCode);
			$intNewID = intval(ltrim($intLastID, '0')) + 1;
			return $strMode.'/'.$tahun.'/'.$bulan.'/'.str_pad($intNewID,6,'0',STR_PAD_LEFT);
		} else return $strMode.'/'.$tahun.'/'.$bulan.'/'.str_pad($intTransactionID,6,'0',STR_PAD_LEFT);
	}
}

function statusDPH($strBPBDate)
{		
	$strBPBDateTwoWeek = date('Y-m-d H:i:s', strtotime($strBPBDate . ' +14 day'));
	$today = date('Y-m-d H:i:s');
	if($today > $strBPBDateTwoWeek){
		$strStatus = "<b>Jatuh Tempo</b>";
	}else{
		$strStatus = "";
	}

	return $strStatus;	
}

function statusGiro($intStatus)
{
	switch ($intStatus) {
		case '1':
			$strStatus = "Bounce";
			break;
		case '2':
			$strStatus = "Kliring";
			break;
		default:
			$strStatus = "Outstanding";
			break;
	}

	return $strStatus;
}

function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
}
 
function terbilang($nilai) {
	if($nilai<0) {
		$hasil = "minus ". trim(penyebut($nilai));
	} else {
		$hasil = trim(penyebut($nilai));
	}     		
	return $hasil;
}

function getProjectInTeam($userID) {
	$_CI =& get_instance();
	$_CI->load->model('Mdbvo','Mpriviledge');
	$_CI->Mpriviledge->setQuery("SELECT subkontrak_id FROM subkontrak_team WHERE NAME = $userID GROUP BY subkontrak_id");
	if($_CI->Mpriviledge->getNumRows()) {
		$result = [];
		foreach ($_CI->Mpriviledge->getQueryResult('Array') as $key => $value) array_push($result,$value['subkontrak_id']);
		return implode(',',$result);
	}
	else return false;
}

function checkTeamPermission($table,$intID) {
	$_CI =& get_instance();
	$userID = $_CI->session->userdata('strAdminID');
	$_CI->load->model('Mdbvo','Mpriviledge');
	if($table != 'Subkontrak') {
		$_CI->Mpriviledge->setQuery("SELECT jabatan_id
			FROM $table
			LEFT JOIN subkontrak sk ON sk.id = $table.idSubKontrak
			LEFT JOIN subkontrak_team skt ON sk.id = skt.subkontrak_id
			WHERE $table.id = $intID AND NAME = $userID");
	} else {
		$_CI->Mpriviledge->setQuery("SELECT jabatan_id
			FROM subkontrak sk
			LEFT JOIN subkontrak_team skt ON sk.id = skt.subkontrak_id
			WHERE sk.id = $intID AND NAME = $userID");
	}
	if($_CI->Mpriviledge->getNumRows() > 0) {
		return true;
	} else {
		$_CI->session->set_flashdata('strMessage',$_CI->lang->jw('no_priviledge'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	// used for complex permission setting
	// $settingPermission = array('Purchase_order' => array('pb','idSubKontrak'),
	// 					'Purchase' => array('op','idSubKontrak'),
	// 					'Acceptance' => array('bpb','idSubKontrak'),
	// 					'Purchase_invoice' => array('pi','idSubKontrak'),
	// 					'Subkontrak' => array('sk','idSubkontrak'),
	// 					'Subkontrak_termin' => array('sktr','subkontrak_id'),
	// 					'Subkontrak_team' => array('skt','subkontrak_id'),
	// 					'Subkontrak_material' => array('skm','subkontrak_id'),
	// 					'Subkontrak_nonmaterial' => array('skn','subkontrak_id'));

	// if(!empty($_CI->session->userdata('strAdminID'))) {
	// 	$short = $settingPermission[$table][0];
	// 	$_CI->load->model('Mdbvo','Mpriviledge');
	// 	$userID = $_CI->session->userdata('strAdminID');
	// 	$priviledgeID = $_CI->session->userdata('strAdminPriviledge');
	// 	$_CI->Mpriviledge->setQuery("SELECT tiny_data1
	// 		FROM tinydb WHERE tiny_category = 'priv_$short' AND tiny_key like '$priviledgeID-$mode'");
	// 	if($_CI->Mpriviledge->getNumRows() > 0) {
	// 		$priviledge = $_CI->Mpriviledge->getSelectedRow('Array');
	// 		if($priviledge['tiny_data1'] == 'all') return true;
	// 		if($priviledge['tiny_data1'] == 'own') {
	// 			$_CI->Mpriviledge->setQuery("SELECT created_at
	// 			FROM $table WHERE id = $intID");
	// 			if($_CI->Mpriviledge->getNumRows() > 0) {
					
	// 			}
	// 		}
	// 	}
	// 	$_CI->Mpriviledge->setQuery("SELECT jabatan_id
	// 		FROM $table
	// 		LEFT JOIN subkontrak sk ON sk.id = $table.idSubKontrak
	// 		LEFT JOIN subkontrak_team skt ON sk.id = skt.subkontrak_id
	// 		WHERE $table.id = $intID AND NAME = $userID");
	// 	if($_CI->Mpriviledge->getNumRows() > 0) {
	// 		$priviledge = $_CI->Mpriviledge->getSelectedRow('Array');
	// 		$priviledge = $priviledge['jabatan_id'];
	// 		$_CI->Mpriviledge->setQuery("SELECT tiny_data1
	// 		FROM tinydb WHERE tiny_category = 'priv_$short' AND tiny_key like '$priviledge-$mode'");
	// 	} else {

	// 	}
	// }
}

function statusInvoice($intStatus)
{
	switch ($intStatus) {
		case 4:
			$strLabel = ' <span class="label label-success"> Lunas</span>';
			break;
		default:
			$strLabel = ' <span class="label label-default">Belum Lunas</span>';
			break;
	}

	return $strLabel;
}

/* End of File */