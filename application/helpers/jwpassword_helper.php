<?php

function generateNumericPassword($intCharacterCount = 6) {
	$strWord = '';
	$arrChars = array('9','8','7','6','5','4','3','2','1','0');
	for($i = 0; $i < $intCharacterCount; $i++) {
		$strWord .= $arrChars[mt_rand(0,count($arrChars) - 1)];
	}
	
	return ($strWord);
}

function generatePassword($intCharacterCount = 8) {
	$strWord = '';
	$arrChars = array('9','8','7','6','5','4','3','2','1','0','a','A','b','B','c','C','d','D','e','E','f','F','g','G','h','H','i','I','j','J','k','K','l','L','m','M','n','N','o','O','p','P','q','Q','r','R','s','S','t','T','u','U','v','V','w','W','x','X','y','Y','z','Z','1','2','3','4','5','6','7','8','9','0');
	for($i = 0; $i < $intCharacterCount; $i++) {
		$strWord .= $arrChars[mt_rand(0,count($arrChars) - 1)];
	}
	
	return ($strWord);
}

function formatID($strID, $count = 4) {
	return str_pad($strID,$count,'0',STR_PAD_LEFT);
}