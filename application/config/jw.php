<?php defined('BASEPATH') OR exit('No direct script access allowed');

defined('LOG_THRESHOLD') OR define('LOG_THRESHOLD', 0);
defined('JW_CACHE_TYPE') OR define('JW_CACHE_TYPE', 'file'); # 'file' for cache ON or 'dummy' for cache OFF
defined('JW_CACHE_EXP_1_DAY') OR define('JW_CACHE_EXP_1_DAY', 86400);
defined('JW_CACHE_EXP_5_MIN') OR define('JW_CACHE_EXP_5_MIN', 300);
defined('JW_SEND_EMAIL_MODE') OR define('JW_SEND_EMAIL_MODE', 'smtp');
defined('RECAPTCHA_SITE_KEY') OR define('RECAPTCHA_SITE_KEY', '6LcYtwcUAAAAANrndBlhwHEJ7kbyKId_nDH1i4I5');
defined('RECAPTCHA_SECRET_KEY') OR define('RECAPTCHA_SECRET_KEY', '6LcYtwcUAAAAAGzMhGOahAGY1wAidNvDoFvJT1sk');
defined('STATUS_OUTSTANDING') OR define('STATUS_OUTSTANDING', 0);
defined('STATUS_REJECTED') 	OR define('STATUS_REJECTED', 1);
defined('STATUS_APPROVED') OR define('STATUS_APPROVED', 2);
defined('STATUS_WAITING_FOR_FINISHING') OR define('STATUS_WAITING_FOR_FINISHING', 3);
defined('STATUS_FINISHED') OR define('STATUS_FINISHED', 4);
defined('STATUS_DELETED') OR define('STATUS_DELETED', -1);

$config['author'] = 'tobsite.com';