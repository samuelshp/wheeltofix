<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Booking extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mbooking');

    $this->_getMenuHelpContent(46,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtMakeBooking') != '') { // Make Booking
        $strCustName = $this->input->post('customerName');
        if($this->input->post('customerAddress') != '') $strCustName .= ', '.$this->input->post('customerAddress');
        if($this->input->post('customerCity') != '') $strCustName .= ', '.$this->input->post('customerCity');

        if($this->input->post('txtTotal') > 0) $intStatus = 2;
        else $intStatus = 0;

        $arrBooking = array(
            'intLapanganID' => $this->input->post('intLapanganID'),
            'intProductID' => $this->input->post('intProductID'),
            'intTotal' => $this->input->post('txtTotal'),
            'intStatus' => $intStatus,
            'intCustID' => $this->input->post('customerID'),
            'strCustName'=> $strCustName,
            'strCustCity' => $this->input->post('customerCity'),
            'strCustPhone' => $this->input->post('customerPhone'),
            'strDescription' => $this->input->post('txaDescription'),
            'strDateFrom'=> $this->input->post('txtFrom'),
            'strDateUntil'=> $this->input->post('txtUntil'),
            'strDate'=> $this->input->post('txtDate')
        );
        $intBookingID = $this->Mbooking->add($arrBooking['intLapanganID'],$arrBooking['intProductID'],$arrBooking['intCustID'],$arrBooking['intTotal'],$arrBooking['strCustName'],$arrBooking['strCustPhone'],$arrBooking['strDateFrom'],$arrBooking['strDateUntil'],$arrBooking['strDescription'],$arrBooking['intStatus'],$arrBooking['strDate']);


        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'booking-bookingemade');
        redirect('booking/browse');
        //redirect('boooking/view/'.$intBookingID);
	}
    $this->load->model('Mtinydbvo','FieldNumber'); $this->FieldNumber->initialize('jwdefaultconfig');
    $intField = $this->FieldNumber->getSingleData('field_number');
    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getServiceData();

    $this->load->model('Mtinydbvo','BookingStatus'); $this->BookingStatus->initialize('booking_status');
    $arrBookingStatus = $this->BookingStatus->getAllData();
	// Load the views

    $this->load->view('sia',array(
        'strViewFile' => 'booking/add',
        'arrBookingStatus' => $arrBookingStatus,
        'intField' => $intField,
        'arrProduct' => $arrProduct,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-addbooking')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
	if($this->input->post('subSave') != '' && !empty($intID)) { # SAVE
		// Purchase
		$arrBookingData = $this->Mbooking->getItemByID($intID);
		if(compareData($arrBookingData['book_status'],array(0))) {
			    $this->Mbooking->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('selStatus'),$this->input->post('intTotal'),$this->input->post('txtFrom'),$this->input->post('txtUntil'));
		} else $this->Mbooking->editByID($intID,$this->input->post('selStatus'));

		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'booking-bookingupdated');

	} else if($this->input->post('subDelete') != '' && !empty($intID)) {
		$this->Mbooking->deleteByID($intID);

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'booking-bookingdeleted'));
		redirect('booking/browse');
	
	} else if($this->input->post('subPrint') != '' && !empty($intID)) {
		$this->Mbooking->editByID($intID,3);
		redirect('invoice?a=booking&id='.$intID);
	}

	$arrBookingData = $this->Mbooking->getItemByID($intID);
	if(!empty($arrBookingData)) {
        $arrBookingData['book_product_id'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => '31'), $arrBookingData['book_status']);
		$arrBookingData['book_rawstatus'] = $arrBookingData['book_status'];
		$arrBookingData['book_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'booking_status', 'allow_null' => 0, 'validation' => ''),
			$arrBookingData['book_status']);

	} else {
		$arrBookingItem = array(); $arrBookingList = array();
	}
    $this->load->model('Mtinydbvo','BookingStatus'); $this->BookingStatus->initialize('booking_status');
    $arrBookingData['book_strstatus'] = $this->BookingStatus->getSingleData($arrBookingData['book_rawstatus']);
    
    $pengurangan = 0;
    $this->load->model('Minvoice');
    $arrKreditCustomer = $this->Minvoice->getCustomerCredit($arrBookingData['cust_id']);
    for($ii = 0; $ii < count($arrKreditCustomer); $ii++) {
        $pengurangan += (float) $arrKreditCustomer[$ii]['invo_grandtotal'];
    }
    if(is_numeric($pengurangan) && $pengurangan>0) {
        $arrBookingData['cust_limitkredit'] = (float)$arrBookingData['cust_limitkredit']-(float)$pengurangan+(float)$arrBookingData['book_total'];
    }
    $arrBookingList = $this->Mbooking->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'booking/view',
        'intBookingID' => $intID,
        'arrBookingList' => $arrBookingList,
        'arrBookingData' => $arrBookingData,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingdata')
    ), $this->admlinklist->getMenuPermission(46,$arrBookingData['book_rawstatus'])));

}

public function browse($intPage = 0) {
    $this->load->model('Mtinydbvo','FieldNumber'); $this->FieldNumber->initialize('jwdefaultconfig');
    $intField = $this->FieldNumber->getSingleData('field_number');
    $this->load->model('Mtinydbvo','BookingStatus'); $this->BookingStatus->initialize('booking_status');

	if($this->input->post('subSearch') != '') {
        $strBrowseMode = '';
        $time = strtotime($this->input->post('txtSearchValue'));
        $current_day = new DateTime(date('Y-m-d H:i:s',$time));
        $current_day->setTime(0, 0);
        $day_of_week = date('N', $time);
        $sunday = clone $current_day;

        if($day_of_week==7) $sunday->sub(new DateInterval('P0D'));
        else $sunday->sub(new DateInterval('P'.$day_of_week.'D'));

        $strSearchedDate = $this->input->post('txtSearchValue');

    } else {
		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
        $interval=7;
        $now_at= $interval * $intPage;
        $real_current_day = new DateTime('now');
        $real_current_day->setTime(0, 0);
        $current_day = clone $real_current_day;
		
        if($now_at>0) $current_day->add(new DateInterval('P'.$now_at.'D')); // P1D means a period of 1 day
        else{
            $now_at=abs($now_at);
            $current_day->sub(new DateInterval('P'.$now_at.'D')); // P1D means a period of 1 day
        }

        $day_of_week = date('N', time());
        $sunday = clone $current_day;
        if($day_of_week==7) $sunday->sub(new DateInterval('P0D'));
        else $sunday->sub(new DateInterval('P'.$day_of_week.'D'));

        $strSearchedDate = '';
	}

	$temp=clone $sunday;
    $saturday = clone $sunday;
    $saturday->add(new DateInterval('P7D'));
    $sunday = $sunday->format('Y-m-d H:i:s');
    $saturday = $saturday->format('Y-m-d H:i:s');
    $arrBooking = $this->Mbooking->getItems($sunday,$saturday);

    if(!empty($arrBooking)) for ($i=0; $i < count($arrBooking); $i++) { 
    	$arrBooking[$i]['book_rawstatus'] = $arrBooking[$i]['book_status'];
		$arrBooking[$i]['book_status'] = $this->BookingStatus->getSingleData($arrBooking[$i]['book_rawstatus']);
		if(empty($arrBooking[$i]['cust_name'])) {
            $strKeterangan = $arrBooking[$i]['book_lapangan_id'].': '.$arrBooking[$i]['book_customer_name'];
            if(!empty($arrBooking[$i]['book_customer_phone'])) $strKeterangan .= ' ('.$arrBooking[$i]['book_customer_phone'].')';

            $arrBooking[$i]['keterangan'] = $strKeterangan;
        } else {
            $arrBooking[$i]['keterangan'] = $arrBooking[$i]['book_lapangan_id'].': '.$arrBooking[$i]['cust_name'];
            if(!empty($arrBooking[$i]['cust_phone'])) $strKeterangan .= ' ('.$arrBooking[$i]['cust_phone'].')';
            
            $arrBooking[$i]['keterangan'] = $strKeterangan;
        }
    }

    //$arrHari[0]=$temp->format('Y-m-d');
    for($i = 0; $i < 7; $i++) {
        $arrHari[$i]['strDate'] = $temp->format('Y-m-d');
	    for ($j = 0; $j < 24; $j++) for($k = 0; $k < 60; $k += 15) { // Initialize
	    	$strNow = formatDate2($arrHari[$i]['strDate'].' '.$j.':'.$k,'Y-m-d H:i:s');
	    	for($l = 1; $l <= $intField; $l++) {
	    		$arrHari[$i][$j][$k][$l] = -1;
	    		if(!empty($arrBooking)) foreach($arrBooking as $key => $e) if($e['book_lapangan_id'] == $l)
	    			if($strNow >= $e['book_from_round'] && $strNow < $e['book_until_round'])
	    				$arrHari[$i][$j][$k][$l] = $key;
	    	}

	    }
        $temp = $temp->add(new DateInterval('P1D'));
    }
	
    $arrBookingList = $this->Mbooking->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

    $this->load->view('sia',array(
        'strViewFile' => 'booking/browse',
        'arrBookingList' => $arrBookingList,
		'intPage' => $intPage,
        'arrHari' => $arrHari,
		'strBrowseMode' => $strBrowseMode,
		'arrBooking' => $arrBooking,
        'intField' => $intField,
        'strSearchedDate' => $strSearchedDate,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'booking-bookingbrowse')
    ));
}


/* AUTOCOMPLETE */

}

/* End of File */