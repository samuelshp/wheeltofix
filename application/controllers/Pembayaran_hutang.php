<?php

class Pembayaran_hutang extends JW_Controller{

    public function __construct() {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();
        $this->load->model('Mpembayaranhutang');    
        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
        
        $this->_getMenuHelpContent(225,true,'adminpage');    
    }

    public function index()
    {
        if ($this->input->post('smtMakePembayaranHutang') != '') {
            
        }
    }

    public function add($intType)
    {
        $this->load->model('Msupplier');        
        $arrSupplier = $this->Msupplier->getAllData(); 
        $arrCOA = $this->Mpembayaranhutang->getChartOfAccount();

        if ($intType == 1) {
            $strPageTitle = "Pembayaran Hutang";
        }elseif($intType == 2){
            $strPageTitle = "Nota Kredit/Debit Supplier";
        }elseif($intType == 3){
           $strPageTitle = "Pengeluaran Lain-Lain";
        }else{
            redirect('');
        }
    
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'pembayaranhutang/add',
            'strPageTitle' => $strPageTitle,
            'arrSupplier' => $arrSupplier,
            'intType' => $intType,
            'arrCOA' => $arrCOA
        ),$this->admlinklist->getMenuPermission(61,1)));
    }

    public function browse()
    {
        $arrPagination['base_url'] = site_url("pembayaran_hutang/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpembayaranhutang->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 10;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        $arrPembayaranHutang = [];

        $this->load->view('sia', array(
            'strViewFile' => 'pembayaranhutang/browse',
            'strPageTitle' => 'Pembayaran Hutang',
            'strPage' => $strPage,
            'arrPembayaranHutang' => $arrPembayaranHutang
        ));
    }
}


?>