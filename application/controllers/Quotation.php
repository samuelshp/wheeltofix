<?php
/**
 * 
 */
class Quotation extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_CI =& get_instance();
		if($this->session->userdata('strAdminUserName') == '') redirect();
		
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
		$this->load->model('Mpurchaseorder');
		$this->load->model('Mquotation');
		$this->load->model('Mquotationitem');
	
		$this->_getMenuHelpContent(41,true,'adminpage');
	}

	public function index()
	{		

		if($this->input->post('smtMakeQuotation') != ''){
			$arrData = array(
				'txtDateBuat' => $this->input->post('txtDateBuat'),
				'txtDateExpired' => $this->input->post('txtDateExpired'),
				'txtaDescription' => $this->input->post('txtaDescription'),
				'intCustID' => $this->input->post('intCustomerID'),
				'selProyek' => $this->input->post('selProyek'),
				'selPekerjaan' => $this->input->post('selPekerjaan'),
				'selWarehouse' => $this->input->post('selWarehouse'),
				'intTax' => $this->input->post('txtTax'),
				'intGrandTotal' => $this->input->post('subTotalTax')
			);

			$intInsert = $this->Mquotation->add($arrData);

			$this->Mquotationitem->add($intInsert,$this->input->post());
			
			redirect('quotation/view/'.$intInsert);
		}

		if($this->input->post('smtUpdateQuotation') != ''){
			$idQuotation = $this->input->post('idQuotation');	
			
			$arrData = array(
				'txtaDescription' => $this->input->post('txtaDescription'),
				'intTax' => $this->input->post('txtTax'),
				'intGrandTotal' => $this->input->post('subTotalTax')
			);

			$this->Mquotation->update($idQuotation, $arrData);
			
			$intUpdate = $this->Mquotationitem->update($this->input->post(),$idQuotation);		

			redirect('quotation/view/'.$idQuotation);
		}		

		if($this->input->post('smtDeleteQuotation') != ''){
			$idQuotation = $this->input->post('idQuotation');		
			$intDelete = $this->Mquotation->delete($idQuotation);			
			if ($intDelete) {
				redirect('quotation/browse/');
			}else{
				redirect('quotation/view/'.$idQuotation);	
			}
			
		}		
	}

	public function add()
	{		
		$this->load->model('Mkontrak');
		$arrKontrak = $this->Mkontrak->getKontrak();
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'quotation/add',			
			'strPageTitle' => loadlanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-title'),
		)));
	}

	public function view($intPage)
	{
		$arrQuotation = $this->Mquotation->getItem($intPage);
		$arrItems = $this->Mquotationitem->getItem($intPage);
				
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'quotation/view',		
			'arrQuotation' => $arrQuotation,	
			'arrItems' => $arrItems,
			'strPageTitle' => loadlanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-title'),
		),$this->admlinklist->getMenuPermission(41,$arrQuotation['quot_status'])));
	}

	public function browse($intPage = 0)
	{
		if($this->input->post('subSearch') != '') {
	        $arrPurchase = $this->Mpurchase->searchBySupplier($this->input->post('txtSearchProyek'), $this->input->post('txtSearchNoOp'), $this->input->post('txtSearchNoSupplier'));
	        $strPage = '';
	        $strBrowseMode = '<a href="'.site_url('purchase/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";

	    } else {
	        $arrPagination['base_url'] = site_url("quotation/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mquotation->getCount();
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();

	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');

	        $arrQuotation = $this->Mquotation->getItems($intPage,$arrPagination['per_page']);
	    }
	    
	    $this->load->view('sia',array_merge(array(
	        'strViewFile' => 'quotation/browse',
	        'strPage' => $strPage,
	        'strBrowseMode' => $strBrowseMode,
	        'arrQuotation' => $arrQuotation,	        
	        'strSearchKey' => $this->input->post('txtSearchValue'),
	        'strSearchDate' => $this->input->post('txtSearchDate'),
	        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
	        'intSearchStatus' => $this->input->post('txtSearchStatus'),
	        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'quotation-title')
	    ),$this->admlinklist->getMenuPermission(41,2)));
	}
}