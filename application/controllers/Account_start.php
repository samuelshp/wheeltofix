<?php

class Account_start extends JW_Controller
{
    public function __construct() {
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
	
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
        $this->load->model('Maccount');
        $this->load->model('Maccountstart');
	
		// $this->_getMenuHelpContent(21,true,'adminpage');
    }

    public function index()
    {
        
        $arrMasterAccount = $this->Maccount->getAllAccount();

        # Save Account Start for a period
        if ($this->input->post('smtSaveAccountStart') != ''){

            $arrDebet = $this->input->post('amountOfDebet');
            $arrCredit = $this->input->post('amountOfCredit');
			
			$arrAccountToInsert = [];
            for ($i=0; $i < sizeof($arrMasterAccount); $i++) { 
                $arrAccountToInsert[$i] = array(
                    'acse_account_id' => $arrMasterAccount[$i]['id'],
                    'acst_period' => formatDate2(str_replace('/','-',$this->input->post('txtPeriod')),'Y-m-d'),
                    'acst_debet' => str_replace(',','.',str_replace('.','', $arrDebet[$i])),
                    'acst_credit' => str_replace(',','.',str_replace('.','', $arrCredit[$i])),
                );
	            $this->Maccountstart->add($arrAccountToInsert[$i]);
            }  
            
			$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'accountstart-accountstartmade');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
			redirect('account_start/browse');    
			
        }elseif($this->input->post('updateAccountStart') != '') {			

			$arrDebet = $this->input->post('amountOfDebet');
			$arrCredit = $this->input->post('amountOfCredit');
			$period = $this->input->post('period');
			$arrAccountStartItems = $this->Maccountstart->getByPeriod($period);
			
			$arrAccountToUpdate = [];
            for ($i=0; $i < sizeof($arrAccountStartItems); $i++) { 
                $arrAccountToUpdate[$i] = array(
					'acse_account_id' => $arrAccountStartItems[$i]['acse_account_id'],
					'acst_period' => $period,                
                    'acst_debet' => str_replace(',','.',str_replace('.','', $arrDebet[$i])),
                    'acst_credit' => str_replace(',','.',str_replace('.','', $arrCredit[$i])),
                );
				$this->Maccountstart->update($arrAccountToUpdate[$i]);								
			}
			
			$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'accountstart-accountstartupdated');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
			redirect('account_start/view/'.$period);

		}elseif($this->input->post('deleteAccountStart') != '') {
			$period = $this->input->post('period');
			$intDelete = $this->Maccountstart->delete($period);

			if($intDelete){
				$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'accountstart-accountstartdeleted');
				$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
				redirect('account_start/browse');
			}else{
				$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'accountstart-accountstartfailtodeleted');
				$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
				redirect('account_start/view/'.$period);
			}
		}

		// print_r($arrAccountToInsert);
        $this->load->view('sia',array(
            'strViewFile' => 'account_start/add',
			'arrMasterAccount' => $arrMasterAccount,       
			'arrAccountToUpdate' => $arrAccountToInsert,                        
            'strPageTitle' => loadLanguage('admindisplay', $this->session->userdata('jw_language'), 'accountstart-accountstart')
        ));
    }

    public function browse($intPage = 0) {        

		if($this->input->post('subSearch') != '') {
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('purchase_order/browse', NULL, FALSE).'">[Back]</a>';
			$this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";
		} else {

			$arrPagination['total_rows'] = $this->Maccountstart->getCount();
			$arrPagination['per_page'] = 12;
			$arrPagination['uri_segment'] = 3;
			
			if($this->input->get('order_by') != ''){
				 if($this->input->get('order_by') == 'ASC'){
					$strOrder = 'ASC';
					$strOrderType = 'DESC';
				 }else{
					$strOrder = 'DESC';
					$strOrderType = 'ASC';
				 }

				 $strLink = "&per_page=".$arrPagination['per_page']."&order_by=".$strOrder;

			}else{
				$strOrder = $this->input->get('order_by');
				$strOrderType = 'DESC';
				$strLink = "";
			}

			if($this->input->get('page') != '') $intPage = $this->input->get('page');			
						
			$arrPagination['base_url'] = site_url("account_start/browse?pagination=true$strLink", NULL, FALSE);
			$this->pagination->initialize($arrPagination);
            $strPage = $this->pagination->create_links();                    
	
			$strBrowseMode = '';

            $arrAccountStart = $this->Maccountstart->getPerPeriod($strOrder,$intPage,$arrPagination['per_page']);						
		}
        
        if(!empty($arrAccountStart)) for($i = 0; $i < count($arrAccountStart); $i++) {            
                $arrAccountStart[$i] = array_merge($arrAccountStart[$i],$this->admlinklist->getMenuPermission(242,0));                            
        }			
		
		$this->load->view('sia',array(
			'strViewFile' => 'account_start/browse',
			'strPage' => $strPage,
			'intPerPage' => $arrPagination['per_page'],
			'strOrder' => $strOrder,
			'strOrderType' => $strOrderType,
			'strBrowseMode' => $strBrowseMode,
			'arrAccountStart' => $arrAccountStart,						
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-accountstart')            
		));
	}

	public function view($strPeriod = '')
	{
		if($strPeriod != ''){
			$arrAccountStartItems = $this->Maccountstart->getByPeriod($strPeriod);
		}else{
			redirect('account_start/browse');
		}

		if(!empty($arrAccountStartItems)){
			$arrBtnAllow = $this->admlinklist->getMenuPermission(242,0);
		}

		$this->load->view('sia',array(
			'strViewFile' => 'account_start/view',			
			'strBrowseMode' => $strBrowseMode,
			'arrAccountStartItems' => $arrAccountStartItems,			
			'arrBtnAllow' => $arrBtnAllow,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'accountstart-detailaccountstart')            
		));
	}


	//Saldo Awal Penerimaan - Pengeluaran Proyek
	public function sappproyek($type = '', $intPage = 0)
	{
		
		if ($type == 'add') {

			$this->load->model('Mkontrak');
			$arrProject = $this->Mkontrak->getKontrak();

			$this->load->view('sia',array(
				'strViewFile' => 'saldoawal_penerimaanpengeluaranproyek/add',
				'arrProject' => $arrProject,						
	            'strPageTitle' => "Saldo Awal Penerimaan - Pengeluaran Proyek"
			));
		}

		if ($type == 'view') {

			$this->load->model('Mkontrak');
			$arrProject = $this->Mkontrak->getKontrak();
			$arrDetail = $this->Maccountstart->sappView($intPage);


			$this->load->view('sia',array(
				'strViewFile' => 'saldoawal_penerimaanpengeluaranproyek/view',
				'arrProject' => $arrProject,
				'arrDetail' => $arrDetail,						
	            'strPageTitle' => "Saldo Awal Penerimaan - Pengeluaran Proyek"
			));
		}

		if ($type == 'browse') {
			$arrData = $this->Maccountstart->sappBrowse();

			$this->load->view('sia',array(
				'strViewFile' => 'saldoawal_penerimaanpengeluaranproyek/browse',
				'arrData' => $arrData,						
	            'strPageTitle' => "Saldo Awal Penerimaan - Pengeluaran Proyek"
			));
		}

		if ($type == '') {

			if ($this->input->post('btnSave') != '') {
				$arrData = array(
					'tanggal' => formatDate2(str_replace('/','-',$this->input->post('txtDateBuat')),'Y-m-d'),
					'kontrak_id' => $this->input->post('strProyek'),
					'prestasi_before_nominal' => $this->input->post('prestasibefore'),
					'penerimaan_before_nominal' => $this->input->post('penerimaanbefore'),
					'biaya_before_nominal' => $this->input->post('biayabefore')
				);
				
				$insert = $this->Maccountstart->sappInsert($arrData);
				if ($insert) {				
					$this->session->set_flashdata('strMessage', 'Berhasil disimpan');
					redirect('account_start/sappproyek/browse'); 
				}else{				
					$this->session->set_flashdata('strMessage', 'Gagal menyimpan.');
					redirect('account_start/sappproyek/add'); 
				}
			}

			if ($this->input->post('smtUpdate')) {
				$intPage = $this->input->post('intPage');
				$arrData = array(
					'tanggal' => formatDate2(str_replace('/','-',$this->input->post('txtDateBuat')),'Y-m-d'),
					'kontrak_id' => $this->input->post('strProyek'),
					'prestasi_before_nominal' => $this->input->post('prestasibefore'),
					'penerimaan_before_nominal' => $this->input->post('penerimaanbefore'),
					'biaya_before_nominal' => $this->input->post('biayabefore')
				);

				$update = $this->Maccountstart->sappUpdate($arrData, $intPage);
				if ($update) {				
					$this->session->set_flashdata('strMessage', 'Berhasil diupdate');
					redirect('account_start/sappproyek/view/'.$intPage); 
				}else{				
					$this->session->set_flashdata('strMessage', 'Gagal diupdate.');
					redirect('account_start/sappproyek/view/'.$intPage); 
				}
			}

			if ($this->input->post('smtDelete')) {
				$intPage = $this->input->post('intPage');
				$delete = $this->Maccountstart->sappDelete($intPage);
				if ($delete) {				
					$this->session->set_flashdata('strMessage', 'Berhasil dihapus');
					redirect('account_start/sappproyek/browse'); 
				}else{				
					$this->session->set_flashdata('strMessage', 'Gagal dihapus.');
					redirect('account_start/sappproyek/browse'); 
				}
			}
			
		}

	}

}


?>