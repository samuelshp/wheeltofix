<?php
/**
 * 
 */
class Credit extends JW_Controller
{
	private $_CI;

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
    	$this->_CI =& get_instance();
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

		$this->load->model('Maccount');
		$this->load->model('Mkontrak');
		$this->load->model('Mcustomer');
		$this->load->model('Mcredit');
		$this->load->model('Mtransin');
		$this->load->model('Mtransindetail');
	}

	# Uang Masuk Customer (Check Again)
	public function uangmasukcustomer($type = '', $intID = 0)
	{
		$this->load->model('Muangmasukcustomer');
		if ($type == 'add') {
			$this->_getMenuHelpContent(235,true,'adminpage');	        
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'uang_masuk_customer/add',
	            'strPageTitle' => 'Uang Masuk Customer',
	            'strLink' => __FUNCTION__,
	            'arrCOA' => $this->Maccount->getAllAccount(),
	            'arrCustomer' => $this->Mcustomer->getAllCustomer()
	        ),$this->admlinklist->getMenuPermission(235,1)));
		}

		if ($type == 'view') {								
			$this->_getMenuHelpContent(235,true,'adminpage');               	        			
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'uang_masuk_customer/view',
	            'strPageTitle' => 'Uang Masuk Customer',
	            'strLink' => __FUNCTION__,
	            'intID' => $intID,
	            'arrCOA' => $this->Maccount->getAllAccount(),
	            'arrCustomer' => $this->Mcustomer->getAllCustomer(),
	            'arrUangMasukDetail' => $this->Muangmasukcustomer->getDetail($intID)
	        ),$this->admlinklist->getMenuPermission(235,2)));
		}

		if ($type == 'browse') {
			$this->_getMenuHelpContent(235,true,'adminpage');
			
			if ($this->input->post('subSearch') != '') {
	            $arrDataSearch = array(
	                'umcu_date_start' => $this->input->post('txtSearchDate'),
	                'umcu_date_end' => $this->input->post('txtSearchDate2'),
	                'umcu_code' => $this->input->post('txtSearchNo'),
	                'umcu_cust' => $this->input->post('txtSearch'),
	            );

	            $arrUangMasuk = $this->Muangmasukcustomer->searchBy($arrDataSearch);
	            $strPage = '';

	            $strBrowseMode = '<a href="'.site_url('credit/'.__FUNCTION__.'/browse', NULL, FALSE).'" class="btn btn-link">[Back]</a>';
	            $this->_arrData['strMessage'] = "Search result (".(!empty($arrUangMasuk) ? count($arrUangMasuk) : '0')." records).";
	        }else{			
		        $arrPagination['base_url'] = site_url("credit/".__FUNCTION__."/browse?pagination=true", NULL, FALSE);
		        $arrPagination['total_rows'] = $this->Muangmasukcustomer->getCount();
		        $arrPagination['per_page'] = $this->config->item('jw_item_count');
		        $arrPagination['uri_segment'] = 3;
		        $this->pagination->initialize($arrPagination);
		        $strPage = $this->pagination->create_links();

		        $strBrowseMode = '';
				if($this->input->get('page') != '') $intID = $this->input->get('page');
		        $arrUangMasuk = $this->Muangmasukcustomer->getListOfData($intID, $arrPagination['per_page']);

		        $strBrowseMode = '';
		    }
	        $this->load->view('sia',array(
	            'strViewFile' => 'uang_masuk_customer/browse',
	            'strPage' => $strPage,
	            'strLink' => __FUNCTION__,
	            'strPageTitle' => 'Uang Masuk Customer',
	            'strBrowseMode' => $strBrowseMode,
	            'arrUangMasuk' => $arrUangMasuk,
	        ));
		}

		if ($type == 'index') {

			$this->load->model('Muangmasukcustomer');

			if ($this->input->post('smtMakeUangMasukCustomer') != '') {
            	$this->_CI->db->trans_start();
	            $arrData = array(	
	            	'umcu_code' => generateTransactionCode($this->input->post('txtDate'),'','uang_masuk_customer'),                
	                'umcu_date' => $this->input->post('txtDate'),
	                'umcu_cust_id' => $this->input->post('intCustomer'),
	                'umcu_acco_id' => $this->input->post('intCOA'),
	                'umcu_type' => $this->input->post('intMethod'),
	                'umcu_ref_giro' => $this->input->post('txtRefGiro'),
	                'umcu_jatuhtempo' => $this->input->post('txtDueDate'),
	                'umcu_amount' => $this->input->post('txtAmount'),
	            );

	            $intAddUangMasukCustomer = $this->Muangmasukcustomer->add($arrData);	            

	            $this->load->model('Mjournal');
	        	$this->Mjournal->insertPiutangUMC($intAddUangMasukCustomer);
				$this->Mjournal->postTransaction('umc',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();

	            if ($intAddUangMasukCustomer > 0) {
	                redirect('credit/'.__FUNCTION__.'/browse', 'refresh');
	            }else{
	                redirect('credit/'.__FUNCTION__.'/add','refresh');
	            }
	        }

	        if ($this->input->post('smtUpdateUangMasukCustomer') != '') {
	        	$this->_CI->db->trans_start();	        	
	            $arrData = array(               	               
	                'umcu_date' => $this->input->post('txtDate'),
	                'umcu_cust_id' => $this->input->post('intCustomer'),
	                'umcu_acco_id' => $this->input->post('intCOA'),
	                'umcu_type' => $this->input->post('intMethod'),
	                'umcu_ref_giro' => ($this->input->post('intMethod') == 3) ? $this->input->post('txtRefGiro') : null,
	                'umcu_jatuhtempo' => ($this->input->post('intMethod') == 3) ? $this->input->post('txtDueDate') : null,
	                'umcu_amount' => $this->input->post('txtAmount'),
	            );

	            $intUpdateTrans = $this->Muangmasukcustomer->update($arrData, $intID);

				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				$this->Mjournal->postTransaction('umc',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();
	            if ($intUpdateTrans > 0) {
	                redirect('credit/'.__FUNCTION__.'/view/'.$intID, 'refresh');
	            }else{
	                redirect('credit/'.__FUNCTION__.'/view/'.$intID,'refresh');
	            }
	        }

	        if ($this->input->post('smtDeleteUangMasukCustomer') != '') {
	        	$intID = $this->input->post('intID');			
	        	$this->_CI->db->trans_start();
	        	$intDetail = $this->Muangmasukcustomer->delete($intID);
				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				$this->Mjournal->postTransaction('umc',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();
	        	if($intDetail > 0){
	                redirect('credit/'.__FUNCTION__.'/browse', 'refresh');
	        	}else{
	                redirect('credit/'.__FUNCTION__.'/view/'.$intID,'refresh');
	        	}	        


	        }

	        if($this->input->post('subPrint') != '' || $this->input->get('print') != '' ) {	

				$intID = ($this->input->post('intID') != '') ? $this->input->post('intID') : $this->input->get('print');				
				$arrUMC = $this->Muangmasukcustomer->getDetail($intID);			
				
				$this->load->model('Mdbvo','MAdminLogin');
			    $this->MAdminLogin->initialize('adm_login');

			    $this->MAdminLogin->dbSelect('','id =' . $arrUMC['cby']);
			    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
			    
				$this->load->view('sia_print',array(
		            'strPrintFile' => 'uangmasukcustomer',
		            'arrUMC' => $arrUMC,					
					'arrLogin' => $arrLogin,
					'arrDaftarPembayaranHutangItem' => null
		        ));		       
			}

	        
		}
	}

	# Pembayaran Piutang
	public function pembayaran_piutang($type = '', $intID = 0)
	{
		$this->_getMenuHelpContent(63,true,'adminpage');		

		if ($type == "add") {
			$this->load->view('sia', array_merge(array(
				'strViewFile' => 'credit/add',
				'strPageTitle' => "Pembayaran Piutang",
				'strLink' => __FUNCTION__,
				'intType' => 1,
				'arrCOA' => $this->Maccount->getAllAccount(),
				'arrCustomer' => $this->Mcustomer->getAllCustomer()
			),$this->admlinklist->getMenuPermission(63,1)));
		}

		if ($type == 'view') {								
			$this->_getMenuHelpContent(63,true,'adminpage');               	        			
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'credit/view',
	            'strPageTitle' => 'Detail Pembayaran Piutang',
	            'strLink' => __FUNCTION__,
	            'intType' => 1,
	            'intID' => $intID,
	            'arrCOA' => $this->Maccount->getAllAccount(),
	            'arrCustomer' => $this->Mcustomer->getAllCustomer(),
	            'arrCredit' => $this->Mtransin->getTransactionByID($intID),
	            'arrDetail' => $this->Mtransin->getDetail($intID),
	            'arrHistoryUMC' => $this->Mcredit->historyUMC($intID)
	        ),$this->admlinklist->getMenuPermission(63,2)));
		}

		if ($type == "browse") {
			if($this->input->post('subSearch') != '') {		
				$arrDataSearch = array(
	        		'txin_date' => $this->input->post('txtSearchTgl'),
	        		'txin_code' => $this->input->post('txtSearchNo'),
	        		'txin_customer' => $this->input->post('txtSearchCustomer')
	        	);

	        	$arrPembayaranPiutang = $this->Mtransin->searchBy('PPI',$arrDataSearch);
		        $strPage = '';
		        $strBrowseMode = '<a href="'.site_url('credit/'.__FUNCTION__.'/browse', NULL, FALSE).'">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPembayaranPiutang) ? count($arrPembayaranPiutang) : '0')." records).";
		    }else{
		    	$arrPagination['base_url'] = site_url("credit/pembayaran_piutang/browse?pagination=true", NULL, FALSE);
		        $arrPagination['total_rows'] = $this->Mtransin->getCount('PPI');
		        $arrPagination['per_page'] = $this->config->item('jw_item_count');
		        $arrPagination['uri_segment'] = 10;
		        $this->pagination->initialize($arrPagination);
		        $strPage = $this->pagination->create_links();
		        $intPage = 0;
		        $strBrowseMode = '';
		        if($this->input->get('page') != '') $intPage = $this->input->get('page');
		        $arrPembayaranPiutang = $this->Mtransin->getAllTransaction('PPI',$intPage,$arrPagination['per_page']);
		    }			

	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'credit/browse',
	            'strPageTitle' => 'Pembayaran Piutang',
	            'strPage' => $strPage,
	            'strBrowseMode' => $strBrowseMode,
	            'intType' => 1,
	            'strLink' => __FUNCTION__,
	            'arrPembayaranPiutang' => $arrPembayaranPiutang
	        ),$this->admlinklist->getMenuPermission(63,1)));
		}

		if ($type == ""){
			
			if ($this->input->post('smtMakeCredit') != ''){	
					
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(
					'txin_code' => generateTransactionCode($this->input->post('txtDate'),'','pembayaran_piutang'),			
					'txin_date' => $this->input->post('txtDate'),
					'txin_acco_id' => $this->input->post('intCOA'),
					'txin_metode' => $this->input->post('intMethod'),	
					'txin_cust_id' => $this->input->post('intCustomer')
				);

				$intTransin = $this->Mtransin->addTransIn($arrDataTransaksi);
			
				$arrDataDetail = array(
					'txid_txin_id' => $intTransin,
					'txid_txin_pph' => $this->input->post('intPph'),
					'txid_amount' => $this->input->post('intAmountRencanaBayar'),
					'txid_totalbersih' => $this->input->post('intAmountTotalAfterPph'),
					'txid_ref_giro' => $this->input->post('txtRefGiro'),
					'txid_jatuhtempo' => $this->input->post('txtDueDate'),
					'txid_type' => 'PPI',
					'txid_ref_id' => $this->input->post('idInvoice'),
					'txid_filter_by' => $this->input->post('umcInvoiceMapping')
				);				
				
				$intAddTransDetail = $this->Mtransindetail->add($arrDataDetail, true);				

				#PROSES UMC DIPAKAI BAYAR & INVOICE TERBAYAR				
				$this->Mcredit->insertInvoicePaid($intTransin, $this->input->post('umcInvoiceMapping'),$this->input->post('cbxLunas'));				
				$this->Mcredit->updateUMC($this->input->post('umcInvoiceMapping'),$this->input->post('cbxLunas'));
				$intUpdateInvoice = $this->Mcredit->updateInvoice($this->input->post('umcInvoiceMapping'));	

				$this->load->model('Mjournal');
				$detailTransIn = $this->Mcredit->getInvoicePaidItemsByID($intTransin);
				foreach ($detailTransIn as $e) {
					$this->Mjournal->insertPiutangINVP($e['id']);
					$this->Mjournal->insertPiutangUMCP($e['id']);
				}
				$this->Mjournal->postTransaction('kasbankgiro_masuk',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();
				if ($intUpdateInvoice > 0) {
	                redirect('credit/'.__FUNCTION__.'/browse', 'refresh');
	            }else{
	                redirect('credit/'.__FUNCTION__.'/add','refresh');
	            }								
			}

			if ($this->input->post('smtUpdateCredit') != ''){				
				$intID = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(
					'txin_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txin_cust_id' => $this->input->post('intCustomer'),
				);

				$intTransin = $this->Mtransin->updateTransIn($arrDataTransaksi, $intID);
				
				$arrDataDetail = array(						
					'txid_txin_id' => $this->input->post('intIDdetail'),					
					'txid_txin_pph' => $this->input->post('intPPH'),
					'txid_amount' => $this->input->post('intAmount'),
					'txid_totalbersih' => $this->input->post('intAmountAfterPPH'),					
					'txid_umcu_mapping' => $this->input->post('umcInvoiceMapping')
				);

				$intTransinDetail = $this->Mtransindetail->update($arrDataDetail);			
				$this->Mcredit->updateInvoicePaid($this->input->post('umcInvoiceMapping'), $this->input->post('invpID'), $this->input->post('cbxLunas'));		
				$this->Mcredit->updateUMC($this->input->post('umcInvoiceMapping'),$this->input->post('cbxLunas'));

				$intUpdateInvoice = $this->Mcredit->updateInvoice($this->input->post('umcInvoiceMapping'));	

				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				$this->_CI->db->trans_complete();
                redirect('credit/'.__FUNCTION__.'/view/'.$intID, 'refresh');
				
			}

			if ($this->input->post('smtDeleteCredit') != '') {
				$intID = $this->input->post('intID');
				$this->db->trans_begin();
				$this->Mtransin->deleteTransIn($intID);
				$bolRollback = $this->Mcredit->rollbackAfterDelete($intID);				
				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$this->session->set_flashdata('strMessage','Pelunasan Piutang gagal dihapus');
					redirect('credit/'.__FUNCTION__.'/view/'.$intID,'refresh');
				}else {
					$this->db->trans_commit();
					$this->session->set_flashdata('strMessage','Pelunasan Piutang berhasil dihapus');
					redirect('credit/'.__FUNCTION__.'/browse','refresh');
				}						
			}

		}
	}

	public function notadebetcustomer($type = '', $intID = 0)
	{
		# code...
	}

	public function pemasukkan_lain($type = '', $intID = 0) # ganti id row u/ priviledge
	{
		$this->_getMenuHelpContent(250,true,'adminpage');		

		if ($type == 'add') {
			$this->load->view('sia', array_merge(array(
				'strViewFile' => 'credit/add',
				'strPageTitle' => "Pemasukkan Lain",
				'strLink' => __FUNCTION__,
				'intType' => 3,			
				'arrCOA' => $this->Maccount->getAllAccount(),
				'arrProject' => $this->Mkontrak->getKontrak()
			),$this->admlinklist->getMenuPermission(250,1)));
		}

		if ($type == 'view') {
			$arrCOA = $this->Maccount->getAllAccount();
			$arrCOAdetail = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
			$arrProject = $this->Mkontrak->getKontrak();
	        $arrCredit = $this->Mtransin->getTransactionByID($intID);	     
	        $arrDetail = $this->Mtransin->getDetail($intID);	    	
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'credit/view',
	            'strPageTitle' => "Pemasukkan Lain",
	            'strLink' => __FUNCTION__,
	            'intType' => 3,	           
	            'arrProject' => $arrProject,
	            'arrCOA' => $arrCOA,
	            'arrCOAdetail' => $arrCOAdetail,
	            'arrCredit' => $arrCredit,
	            'arrDetail' => $arrDetail
	        ),$this->admlinklist->getMenuPermission(250,1)));
		}

		if ($type == 'browse') {
			$arrPagination['base_url'] = site_url("credit/pemasukkan_lain/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mtransin->getCount('PML');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();	        
	        $intPage = -1;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrDataPemasukkan = $this->Mtransin->getAllTransaction('PML', $intPage,$arrPagination['per_page']);

	        if ($this->input->post('subSearch') != '') {

	        	$arrDataSearch = array(
	        		'txin_date' => $this->input->post('txtSearchTgl'),
	        		'txin_code' => $this->input->post('txtSearchNo')
	        	);

	        	$arrDataPemasukkan = $this->Mtransin->searchBy('PML',$arrDataSearch);
		        $strPage = '';

		        $strBrowseMode = '<a href="'.site_url('credit/pemasukkan_lain/browse', NULL, FALSE).'" class="btn btn-link">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDataPemasukkan) ? count($arrDataPemasukkan) : '0')." records).";
	        }	       
  
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'credit/browse',
	            'strPageTitle' => 'Pemasukkan Lain',
	            'strPage' => $strPage,
	            'strBrowseMode' => $strBrowseMode,
	            'intType' => 3,	
	            'strLink' => __FUNCTION__,
	            'arrPembayaranPiutang' => $arrDataPemasukkan
	        ),$this->admlinklist->getMenuPermission(250,1)));
		}

		if ($type == '') {
			
			if ($this->input->post('smtMakeCredit') != '') {
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(
					'txin_code' => generateTransactionCode($this->input->post('txtDate'),'','pemasukkan_lain_lain'),
					'txin_date' => $this->input->post('txtDate'),
					'txin_acco_id' => $this->input->post('intCOA'),
					'txin_metode' => $this->input->post('intMethod'),
					'txin_ref_giro' => $this->input->post('txtRefGiro'),
					'txin_jatuhtempo' => $this->input->post('txtDueDate'),
					'txin_cust_id' => $this->input->post('intCustomer')	
				);
				
				$intTransin = $this->Mtransin->addTransIn($arrDataTransaksi);				
				
				$arrDataDetail = array(
					'txid_txin_id' => $intTransin,
					'txid_ppn' => $this->input->post('intPPN'),
					'txid_pph' => $this->input->post('intPPH'),
					'txid_amount' => $this->input->post('intAmount'),
					'txid_totalbersih' => $this->input->post('intAmount'),
					'txid_coa' => $this->input->post('intCOAdetail'),
					'txid_ref_giro' => $this->input->post('txtRefGiro'),
					'txid_jatuhtempo' => $this->input->post('txtDueDate'),
					'txid_type' => 'PML',
					'txid_ref_id' => $this->input->post('listProyek'),
					'txid_description' => $this->input->post('keterangan')
				);
				$intTransinDetail = $this->Mtransindetail->add($arrDataDetail);
				// echo var_dump($intTransinDetail);
				// exit();
				// $this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);		

				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_masuk',$this->input->post('txtDate'));			
				$this->_CI->db->trans_complete();
				if ($intTransinDetail){
					$this->session->set_flashdata('strMessage','Pemasukkan Lain berhasil disimpan');
					redirect('credit/pemasukkan_lain/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pemasukkan Lain gagal disimpan');
					redirect('credit/pemasukkan_lain/add');		
				}
			}

			if ($this->input->post('smtUpdateCredit') != '') {				
					
				$intID = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(					
					'txin_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txin_acco_id' => $this->input->post('intCOA'),
					'txin_metode' => $this->input->post('intMethod'),					
				);

				$intTransin = $this->Mtransin->updateTransIn($arrDataTransaksi, $intID);
				
				$arrDataDetail = array(						
					'txid_txin_id' => $this->input->post('intIDdetail'),
					'txid_ppn' => $this->input->post('intPPN'),
					'txid_pph' => $this->input->post('intPPH'),
					'txid_amount' => $this->input->post('intAmount'),
					'txid_totalbersih' => $this->input->post('intAmount'),
					'txid_coa' => $this->input->post('intCOAdetail'),
					'txid_ref_giro' => $this->input->post('txtRefGiro'),
					'txid_jatuhtempo' => $this->input->post('txtDueDate'),
					'txid_type' => 'PML',
					'txid_ref_id' => $this->input->post('listProyek'),
					'txid_description' => $this->input->post('keterangan')
				);

				$intTransinDetail = $this->Mtransindetail->update($arrDataDetail);
				// $this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);					
				
				// foreach ($this->input->post('intAmount') as $key => $value) {
				// 	$intID = $this->input->post('intIDdetail')[$key];
				// 	$arrDataDetail = array(						
				// 		'txid_ppn' => str_replace(",", ".", str_replace(".", "", $this->input->post('intPPN'))),
				// 		'txid_pph' => str_replace(",", ".", str_replace(".", "", $this->input->post('intPPH'))),
				// 		'txid_amount' => str_replace(",", ".", str_replace(".", "", $value)),
				// 		'txid_totalbersih' => str_replace(",", ".", str_replace(".", "", $value)),
				// 		'txid_coa' => $this->input->post('intCOAdetail')[$key],
				// 		'txid_ref_giro' => $this->input->post('txtRefGiro'),
				// 		'txid_jatuhtempo' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDueDate'))), "Y-m-d"),
				// 		'txid_type' => 'PML',
				// 		'txid_ref_id' => $this->input->post('listProyek')[$key],
				// 		'txid_description' => $this->input->post('keterangan')[$key]
				// 	);

				// 	$intTransinDetail = $this->Mtransindetail->update($arrDataDetail, $intID);
				// 	// $this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);					
				// }

				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_masuk',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();
				if ($intTransinDetail){
					$this->session->set_flashdata('strMessage','Pemasukkan Lain berhasil disimpan');
					redirect('credit/pemasukkan_lain/view/'.$intID);
				}else{
					$this->session->set_flashdata('strMessage','Pemasukkan Lain gagal disimpan');
					redirect('credit/pemasukkan_lain/add');		
				}				

			}

			if ($this->input->post('smtDeleteCredit') != '') {
				$intID = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$intDelete = $this->Mtransin->deleteTransIn($intID);
				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_masuk',$this->input->post('txtDate'));
				$this->_CI->db->trans_complete();
				if ($intDelete) {
					$this->session->set_flashdata('strMessage','Pemasukkan Lain berhasil dihapus');
					redirect('credit/pemasukkan_lain/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pemasukkan Lain gagal dihapus');
					redirect('credit/pemasukkan_lain/view/'.$intID);
				}

			}

		}
	}
}