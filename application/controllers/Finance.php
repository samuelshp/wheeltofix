<?php
/*
PUBLIC FUNCTION:
- index()
- payment()
- paymentbrowse()
- repayment()
- repaymentbrowse()
- billing()
- tax()

PRIVATE FUNCTION:
- __construct()
*/

class Finance extends JW_Controller {

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();
    
    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
    
}

public function payment($intPage = 0) {
    $this->_getMenuHelpContent(63,true,'adminpage');
    $this->load->model('Maccountadjustment');
    if($this->input->post('subSave') != '') { 

        $this->load->model('Mpayment');
        $this->load->model('Mpaymentheader');
        $arrPayment = $this->input->post('paid');
        $arrRetur = $this->input->post('retured');
        if(!empty($arrPayment) || !empty($arrRetur)){
            $arrDate = $this->input->post('paidDate');
            $arrDescription = $this->input->post('paidDescription');
            $arrKeterangan = $this->input->post('paidKeterangan');
            $arrKeteranganRetur = $this->input->post('paidKeteranganRetur');
            $arrSalesman = $this->input->post('salsid');
            $arrSalesmanRetur = $this->input->post('salsidretur');
            $arrAccount=$this->input->post('paidAccount');
            $arrBank=$this->input->post('paidBank');
            $strDate=$this->input->post('txtDate');
            $intPaymentID=$this->Mpaymentheader->add($strDate);
            foreach($arrPayment as $key => $e){
                $this->Mpayment->add($intPaymentID,$arrPayment[$key],$arrSalesman[$key],$arrDate[$key],$key,$arrAccount[$key],$arrBank[$key],$arrDescription[$key],$arrKeterangan[$key],2);
            }
            foreach($arrRetur as $key => $e){
                $this->Mpayment->add($intPaymentID,$arrRetur[$key],$arrSalesmanRetur[$key],'',$key,'','',$arrKeteranganRetur[$key],2);
            }
            $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'finance-financemade'));
            redirect('finance/paymentbrowse');
        }

    }
    $this->load->model('Minvoice');
    if($this->input->post('subSearch') != '') {
        $arrBillingCash = $this->Minvoice->getUnpaidInvoiceByText($this->input->post('txtSearchValue'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/payment', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code or Salesman Name or Costumer Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
    } else {
        $arrPagination['base_url'] = site_url("finance/payment?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Minvoice->getCount2();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrBillingCash = $this->Minvoice->getItemsForPayment($intPage,$arrPagination['per_page']);
    }
    $arrAkun=$this->Maccountadjustment->getAllPerkiraan();

    $this->load->view('sia',array(
        'strViewFile' => 'finance/payment',
        'arrAkun'=> $arrAkun,
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrBilling' => $arrBillingCash
    ));
}

public function paymentbrowse($intID=0) {
    // WARNING! Don't change the following steps
    $this->_getMenuHelpContent(63,true,'adminpage');
    $this->load->model('Mpayment');
    
    if($this->input->post('subSave') != '' && $intID != '') {
        $status = $this->Mpayment->getStatusPaymentByID($intID);
        if(compareData($status['paym_status'],array(0))) {
            $description=$this->input->post('txtDescription'.$intID);
            
            $this->Mpayment->editByID2($intID,$description,$this->input->post('selStatus'));
        }else{
            $this->Mpayment->editByID($intID,$this->input->post('selStatus'));
        }
        redirect('finance/paymentbrowse');
    }
    else if($this->input->post('subDelete') != '' && $intID != '') {
        $this->Mpayment->deleteByID($intID);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchasedeleted'));
        redirect('finance/paymentbrowse');
    }

    if($this->input->post('subSearch') != '') {
        $arrBilling = $this->Mpayment->getAllPayment($this->input->post('txtSearchValue'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/paymentbrowse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code or Salesman Name or Customer Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
    
    } else {
        $arrPagination['base_url'] = site_url("finance/paymentbrowse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpayment->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        else $intPage = 0;
        $arrBilling =  $this->Mpayment->getAllPayment('',$arrPagination['per_page'],$intPage);
    }

    $i=0;
    if(!empty($arrBilling)){
        foreach($arrBilling as $e){
            $arrBilling[$i]=array_merge($arrBilling[$i],$this->admlinklist->getMenuPermission(63,$arrBilling[$i]['paym_status']));
            $i++;
        }
    }

    if($this->input->post('subPrint') != '' && $intID != '') {
        $headerID=$this->Mpayment->getHeaderIDByID($intID);
        $this->load->view('sia_print',array(
            'strPrintFile' => 'payment',
            'intPaymentID' => $intID,
            'dataHeader' => $this->Mpayment->getPrintDataHeaderByID($headerID['paym_header_id']),
            'dataPrint' => $this->Mpayment->getPrintDataByHeaderID($headerID['paym_header_id']),
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        ));
    } else{
        $this->load->view('sia',array(
            'strViewFile' => 'finance/paymentbrowse',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'arrBilling' => $arrBilling
        ));
    }
}

public function repayment($intPage = 0) {
    $this->_getMenuHelpContent(61,true,'adminpage');

    $this->load->model('Maccountadjustment');
    if($this->input->post('smtMakeRepayment') != '') { // Make Purchase
        $this->load->model('Mrepayment');
        $this->load->model('Mrepaymentheader');
        $arrRepayment = $this->input->post('paid');
        $arrRetur = $this->input->post('retured');
        if(!empty($arrRepayment) || !empty($arrRetur)){
            $arrDate = $this->input->post('paidDate');
            $arrDescription = $this->input->post('paidDescription');
            $arrKeterangan = $this->input->post('paidKeterangan');
            $arrKeteranganRetur = $this->input->post('paidKeteranganRetur');
            $arrAccount=$this->input->post('paidAccount');
            $arrBank=$this->input->post('paidBank');
            $strDate=$this->input->post('txtDate');
            $intRepaymentID=$this->Mrepaymentheader->add($strDate);
            foreach($arrRepayment as $key => $e){
                $this->Mrepayment->add($intRepaymentID,$arrRepayment[$key],$arrDate[$key],$key,$arrAccount[$key],$arrBank[$key],$arrDescription[$key],$arrKeterangan[$key],2);
            }
            foreach($arrRetur as $key => $e){
                $this->Mrepayment->add($intRepaymentID,$arrRetur[$key],'',$key,'','',$arrKeteranganRetur[$key],2);
            }
            $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'finance-financemade'));
            redirect('finance/repaymentbrowse');
        }
    }
    $this->load->model('Mpurchase');
    if($this->input->post('subSearch') != '') {
        $arrBilling = $this->Mpurchase->getUnpaidPurchaseByText($this->input->post('txtSearchValue'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/repayment', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code or Supplier Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";

    } else {
        $arrPagination['base_url'] = site_url("finance/repayment?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpurchase->getCount2();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrBilling = $this->Mpurchase->getItemsForRepayment($intPage,$arrPagination['per_page']);
    }
    $arrAkun=$this->Maccountadjustment->getAllPerkiraan();

    $this->load->view('sia',array(
        'strViewFile' => 'finance/repayment',
        'arrAkun'=> $arrAkun,
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrBilling' => $arrBilling
    ));
}

public function repaymentbrowse($intID=0) {
    // WARNING! Don't change the following steps
    $this->_getMenuHelpContent(61,true,'adminpage');
    $this->load->model('Mrepayment');
    if($this->input->post('subSave') != '' && $intID != '') {
        $status=$this->Mrepayment->getStatusRepaymentByID($intID);
        if(compareData($status['repa_status'],array(0))) {
            $description=$this->input->post('txtDescription'.$intID);
            $this->Mrepayment->editByID2($intID,$description,$this->input->post('selStatus'));
        }else{
            $this->Mrepayment->editByID($intID,$this->input->post('selStatus'));
        }
        redirect('finance/repaymentbrowse');
    }
    else if($this->input->post('subDelete') != '' && $intID != '') {
        $this->Mrepayment->deleteByID($intID);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchasedeleted'));
        redirect('finance/repaymentbrowse');
    }

    if($this->input->post('subSearch') != '') {
        $arrBilling = $this->Mrepayment->getAllRepayment($this->input->post('txtSearchValue'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/repaymentbrowse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code Supplier Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
    
    } else {
        $arrPagination['base_url'] = site_url("finance/repaymentbrowse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mrepayment->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        else $intPage = 0;
        $arrBilling =  $this->Mrepayment->getAllRepayment('',$arrPagination['per_page'],$intPage);
    }

    $i=0;
    if(!empty($arrBilling)){
        foreach($arrBilling as $e){
            $arrBilling[$i]=array_merge($arrBilling[$i],$this->admlinklist->getMenuPermission(61,$arrBilling[$i]['repa_status']));
            $i++;
        }
    }
    if($this->input->post('subPrint') != '' && $intID != '') {
        $headerID=$this->Mrepayment->getHeaderIDByID($intID);
        $this->load->view('sia_print',array(
            'strPrintFile' => 'repayment',
            'intRepaymentID' => $intID,
            'dataHeader' => $this->Mrepayment->getPrintDataHeaderByID($headerID['repa_header_id']),
            'dataPrint' => $this->Mrepayment->getPrintDataByID($headerID['repa_header_id']),
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        ));
    } else{
        $this->load->view('sia',array(
            'strViewFile' => 'finance/repaymentbrowse',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'arrBilling' => $arrBilling
        ));
    }
}

public function billing($intPage = 0) {
    $this->_getMenuHelpContent(62,true,'adminpage');
    $this->load->model('Minvoice');

    if($this->input->post('subSearch') != '') {
        $arrBilling = $this->Minvoice->getInvoiceForBilling($this->input->post('txtSearchValue'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/billing', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code or Supplier Name or Costumer Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
    } else {
        $arrPagination['base_url'] = site_url("finance/billing?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Minvoice->getCountForBilling();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrBilling = $this->Minvoice->getItemsForBilling($intPage,$arrPagination['per_page']);
    }
    $i=0;
    if(!empty($arrBilling)){
        foreach($arrBilling as $e){
            $arrBilling[$i]=array_merge($arrBilling[$i],$this->admlinklist->getMenuPermission(62,$arrBilling[$i]['invo_status']));
            $i++;
        }
    }
    if($this->input->post('smtMakeBilling') != '') {
        $listID='0';
        $arrID = $this->input->post('print');
        foreach($arrID as $e){
            $listID=$listID.','.$e;
        }
        $this->load->view('sia_print',array(
            'strPrintFile' => 'billing',
            'dataPrint' => $this->Minvoice->getInvoiceDataForPrintByArray($listID),
            'txtSalesman' => $this->input->post('txtSalesman'),
            'txtDate' => date('Y/m/d'),
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        ));
    } else {
        $this->load->view('sia',array(
            'strViewFile' => 'finance/billing',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
            'arrBilling' => $arrBilling
        ));
    }
}

public function tax() {
    $this->_getMenuHelpContent(64,true,'adminpage');

    $this->load->model('Mtax');
    //pembelian, penjualan, retur pmbelian, retur penjualan, payment,repayment, account_adjustment
    if($this->input->post('subSave') != '' || $this->input->post('smtMakeTax') != '') {

        $arrTaxed= $this->input->post('taxed1');
        $arrChange= $this->input->post('change1');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setPurchaseTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed2');
        $arrChange= $this->input->post('change2');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setPurchaseReturTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed3');
        $arrChange= $this->input->post('change3');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setInvoiceTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed4');
        $arrChange= $this->input->post('change4');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setInvoiceReturTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed5');
        $arrChange= $this->input->post('change5');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setPaymentTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed6');
        $arrChange= $this->input->post('change6');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setRepaymentTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        $arrTaxed= $this->input->post('taxed7');
        $arrChange= $this->input->post('change7');
        if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                $this->Mtax->setAccountAdjustmentTaxedOrNot($key,$arrTaxed[$key]);
            }
        }
        /*if(!empty($arrChange)){
            foreach($arrChange as $key => $e){
                if($arrType[$key]=='1')
                    $this->Mtax->setPurchaseTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='2')
                    $this->Mtax->setPurchaseReturTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='3')
                    $this->Mtax->setInvoiceTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='4')
                    $this->Mtax->setInvoiceReturTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='5')
                    $this->Mtax->setPaymentTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='6')
                    $this->Mtax->setRepaymentTaxedOrNot($key,$arrTaxed[$key]);
                if($arrType[$key]=='7')
                    $this->Mtax->setAccountAdjustmentTaxedOrNot($key,$arrTaxed[$key]);
            }*/
            $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'tax-taxmade'));
            redirect('tax');
        //}
    }
    if($this->input->post('subSearch') != '') {
        $arrTax = $this->Mtax->getAllDataForTax($this->input->post('txtSearchValue'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('finance/tax', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";

    } else {
        $arrPagination['base_url'] = site_url("finance/tax/?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mtax->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        else $intPage=0;

        $arrTax=$this->Mtax->getAllDataForTax('','','',$intPage,$arrPagination['per_page']);
    }

    $this->load->view('sia',array(
        'strViewFile' => 'finance/tax',
        'strPage' => $strPage,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'tax-tax'),
        'strBrowseMode' => $strBrowseMode,
        'arrTax' => $arrTax
    ));
}

}


/* End of File */