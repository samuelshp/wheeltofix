<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Kontrak_Pembelian extends JW_Controller {

public function __construct() {
	parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mkontrakpembelian');

    $this->_getMenuHelpContent(218,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtSaveKontrakPembelian') != '') { // Save Kontrak Pembelian
        $strKPCode = 'KP/'.date('y/m',strtotime($this->input->post('txtDate'))).'/';
        $lastCode = $this->Mkontrakpembelian->getLastKPCode($strKPCode);
        $newCode = $lastCode['code']+1;
        $strKPCode .= str_pad($newCode, 4 , '0', STR_PAD_LEFT);

        $arrKontrakPembelian = array(
            'strKPCode' => $strKPCode,
            'intProyekID' => $this->input->post('intProyekID'),
            'strDate' => date('Y-m-d',strtotime($this->input->post('txtDate'))),            
            'intSupplierID' => $this->input->post('intSupplierID'),
            'intProductID' => $this->input->post('intProductID'),
            'doubleQty' => $this->input->post('txtQty'),
            'intPrice' => $this->input->post('txtPrice'),
            'doubleAmount' => $this->input->post('txtAmount'),
            'doublePO' => $this->input->post('txtPO'),
            'strDescription' => $this->input->post('txtDescription')
        );
        $this->Mkontrakpembelian->add($arrKontrakPembelian['strKPCode'], $arrKontrakPembelian['intProyekID'],$arrKontrakPembelian['strDate'],$arrKontrakPembelian['intSupplierID'],$arrKontrakPembelian['intProductID'],$arrKontrakPembelian['doubleQty'],$arrKontrakPembelian['intPrice'],$arrKontrakPembelian['doubleAmount'],$arrKontrakPembelian['doublePO'],$arrKontrakPembelian['strDescription']);

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'kontrakpembelian-save');
        redirect('kontrak_pembelian/browse');
    }

    if($this->input->post('smtUpdateKontrakPembelian') != '') { # Update        
        
        $intID = $this->input->post('intID');
        $txtDate = $this->input->post('txtDate'); 
        $intProyekID = $this->input->post('intProyekID'); 
        $intSupplierID = $this->input->post('intSupplierID');
        $intProductID = $this->input->post('intProductID');
        $txtQty = $this->input->post('txtQty');
        $txtPrice = $this->input->post('txtPrice');
        $txtAmount = $this->input->post('txtAmount');
        $txtPO = $this->input->post('txtPO');
        $txtDescription = $this->input->post('txtDescription');    

        $this->Mkontrakpembelian->update($intID, $txtDate, $intProyekID, $intSupplierID, $intProductID, $txtQty, $txtPrice, $txtAmount, $txtPO, $txtDescription);

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'kontrakpembelian-update');
        $this->session->set_flashdata('strMessage',$this->_arrData['strMessage']);
        redirect('kontrak_pembelian/browse');

	}

    $this->load->model('Mkontrak');
    $arrProyek = $this->Mkontrak->getKontrak();    
    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getAllItemWithSatuan();
    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'kontrakpembelian/add',
        'strKPCode' => $strKPCode,
        'intField' => $intField,
        'arrProyek' => $arrProyek,
        'arrProduct' => $arrProduct,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-add')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
	# INIT
    if($this->input->post('smtSaveKontrakPembelian') != '' && !empty($intID)) { # SAVE        
        $txtDate = $this->post->input('txtDate'); 
        $intProyekID = $this->post->input('txtDate'); 
        $intSupplierID = $this->post->input('intSupplierID');
        $intProductID = $this->post->input('intProductID');
        $txtQty = $this->post->input('txtQty');
        $txtPrice = $this->post->input('txtPrice');
        $txtAmount = $this->post->input('txtAmount');
        $txtPO = $this->post->input('txtPO');
        $txtDescription = $this->post->input('txtDescription');

        $this->Mkontrakpembelian->udpate($intID, $txtDate, $intProyekID, $intSupplierID, $intProductID, $txtQty, $txtPrice, $txtAmount, $txtPO, $txtDescription);
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'kontrakpembelian-update');
        redirect('kontrak_pembelian/browse');

	} else if($this->input->post('subDelete') != '' && !empty($intID)) {
		
	} else if($this->input->post('subPrint') != '' && !empty($intID)) {
		
    }

    $arrKontrakPembelianData = $this->Mkontrakpembelian->getKontrakPembelianByID($intID);

    $this->load->model('Mkontrak');
    $arrProyek = $this->Mkontrak->getKontrak();    
    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getAllItemWithSatuan();
    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();
    
    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'kontrakpembelian/view',
        'intBookingID' => $intID,
        'arrBookingList' => $arrKontrakPembelianList,
        'arrBookingData' => $arrKontrakPembelianData,
        'arrProyek' => $arrProyek,
        'arrProduct' => $arrProduct,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-browse')
    ), $this->admlinklist->getMenuPermission(218,0)));

}

public function browse($intPage = 0) {
    $arrKontrakPembelian = $this->Mkontrakpembelian->getAllKontrakPembelian();     
    $this->load->view('sia', array(
        'strViewFile' => 'kontrakpembelian/browse',
        'arrKontrakPembelian' => $arrKontrakPembelian,
        'intPage' => $intPage,
        'bolButton' => $this->admlinklist->getMenuPermission(218,0),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kontrakpembelian-browse')
    ));
}


/* AUTOCOMPLETE */

}

/* End of File */