<?php

class Delivery_ajax extends JW_Controller {

public function getDeliveryAutoComplete($txtData='') {
	$this->load->model('Mdelivery');
    ArrayToXml(array('Delivery' => $this->Mdelivery->getAllActiveDelivery($txtData)));
}

public function getInvoiceByDeliveryID($txtData='') {
	$this->load->model('Mdelivery');
    ArrayToXml(array('Delivery' => $this->Mdeliveryitem->getItemsByDeliveryID($txtData)));
}

}