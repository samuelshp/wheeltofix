<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'core/JW_REST_Controller.php';

class Generic extends JW_REST_Controller {

private $_strTable, $_strSelect, $_strCustomQuery;
private $_arrWhere, $_arrOrder, $_arrJoin;
private $_arrAdmTable, $_arrAdmFields, $_arrAdmFieldIndex, $_arrFieldPreException;
private $_arrResponse;

public function __construct() {
	parent::__construct();
	$this->load->model('Mdbvo', '_Mtable');
	$this->load->model('Mdbvo', 'AdmTable');
	$this->AdmTable->initialize('adm_table_list');
	$this->load->model('Mdbvo', 'AdmFields');
	$this->AdmFields->initialize('adm_field_list');
	$this->load->model('Mapicustom');
	$this->initialize('');
}

public function initialize($strTable) {
	$this->_strTable = $this->_strSelect = $this->_strCustomQuery = '';
	$this->_arrWhere = $this->_arrOrder = $this->_arrJoin = $this->_arrAdmTable = $this->_arrAdmFields = $this->_arrAdmFieldIndex = [];
	$this->_arrFieldPreException = ['id', 'cby', 'cdate', 'mby', 'mdate', 'meta_title', 'meta_description', 'meta_keyword'];

	if(!empty($strTable)) {
		$this->Mapicustom->detectTable($strTable, $this->_strTable, $this->_strCustomQuery);
		if(!$this->_Mtable->initialize($this->_strTable, $this->_strCustomQuery)) $this->_Mtable->initialize('jw_'.$this->_strTable, $this->_strCustomQuery);
		$this->AdmTable->search($this->_Mtable->getTableName(), 'title');
		$this->_arrAdmTable = $this->AdmTable->getNextRecord('Array');
		$this->_strPre = $this->_arrAdmTable['pre'];
	}
}

public function index_get($strTable, $intPageNo = 0) {
	$this->initialize($strTable);
	$this->_parseQuery();

	$strWhere = $this->_translateWhere();
	$intTotalRows = $this->_Mtable->dbCount($strWhere, $this->_arrJoin);

	if(intval($intPageNo) > 0) $this->_Mtable->dbSelect($this->_strSelect, $strWhere, $this->_translateOrder(), $this->config->item('jw_item_count'), (intval($intPageNo) - 1) * $this->config->item('jw_item_count'), $this->_arrJoin);
	else $this->_Mtable->dbSelect($this->_strSelect, $strWhere, $this->_translateOrder(), '', '', $this->_arrJoin);
	$this->_debug();

	if($intCount = $this->_Mtable->getNumRows()) $this->_showResponse(true, [
			'count' => $this->_Mtable->getNumRows(),
			'total_rows' => $intTotalRows,
			'data' => $this->_getTableRows(),
		]);
	$this->_showResponse(false);
}

public function item_get($strTable, $strKey, $strField = 'id') {
	$this->initialize($strTable);

	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;
	$bolAllowMultiDimension = false;
	$this->Mapicustom->itemGetConditionals($strTable, $strField, $strKey, $this->_arrWhere, $this->_arrOrder, $bolAllowMultiDimension);

	$this->_parseQuery();
	$strWhere = $this->_translateWhere();
	$strOrder = $this->_translateOrder();

	$intTotalRows = $this->_Mtable->dbCount($strWhere, $this->_arrJoin);
	$this->_Mtable->dbSelect($this->_strSelect, $strWhere, $strOrder, '', '', $this->_arrJoin);
	$this->_debug();

	if($intCount = $this->_Mtable->getNumRows()) $this->_showResponse(true, [
		    'count' => ($bolAllowMultiDimension && intval($intCount) > 1) ? $intCount : 1,
		    'total_rows' => $intTotalRows,
		    'data' => ($bolAllowMultiDimension && intval($intCount) > 1) ? $this->_getTableRows() : $this->_getTableRow(),
		]);
	$this->_showResponse(false);
}

public function item_post($strTable, $strKey = '', $strField = 'id') {
	$arrPost = $this->post();
	if(empty($arrPost)) $arrPost = $this->put();
	$arrResponse = $this->_processPost($strTable, $arrPost, $strKey, $strField);
	$this->_showResponse($arrResponse['success'], $arrResponse);
}
public function item_put($strTable, $strKey = '', $strField = 'id') { $this->item_post($strTable, $strKey, $strField); }

public function batch_post() {
	$arrPost = $this->post();
	if(empty($arrPost)) $arrPost = $this->put();

	$arrResponse = ['success' => true, 'action' => '', 'id' => [], 'count' => 0];
	$this->db->trans_begin();

	$strFKField = false; $intReturnedInsertID = 0;
	foreach($arrPost as $e) {
		$arrData = []; $strTable = ''; $strKey = ''; $strField = 'id'; $strFKFieldTemp = false;
		foreach($e as $j => $f) switch ($j) {
			case 'table': $strTable = $f; break;
			case 'key': $strKey = $f; break;
			case 'field': $strField = $f; break;
			case 'fk_field': $strFKFieldTemp = $f; break;
			default: $arrData[$j] = $f; break;
		}

		if(!empty($strFKField)) {
			$this->_Mtable->setQuery("SHOW COLUMNS FROM {$strTable} WHERE Field LIKE '{$strFKField}'");
			if($this->_Mtable->getNumRows() > 0) $arrData[$strFKField] = $intReturnedInsertParentID;
		}
		$arrTempResponse = $this->_processPost($e['table'], $arrData, $strKey, $strField, $intReturnedInsertID);
		if(!empty($strFKFieldTemp)) {
			$strFKField = $strFKFieldTemp;
			$intReturnedInsertParentID = $intReturnedInsertID;
		}

		if($arrTempResponse['success'] === false) {
			$arrResponse = array_merge($arrResponse, $arrTempResponse);
			break;
		} else {
			$arrResponse['action'] = $arrTempResponse['action'];
			if(!empty($arrTempResponse['id'])) $arrResponse['id'][] = $arrTempResponse['id'];
			if(!empty($arrTempResponse['count'])) $arrResponse['count'] += $arrTempResponse['count'];
			if(!empty($arrTempResponse['afterPost'])) $arrResponse['afterPost'][] = $arrTempResponse['afterPost'];
		}
	}

	if ($this->db->trans_status() === FALSE || $arrTempResponse['success'] === FALSE) $this->db->trans_rollback();
	else $this->db->trans_commit();

	$this->_showResponse($arrResponse['success'], $arrResponse);
}
public function batch_put() { $this->batch_post(); }

private function _processPost($strTable, $arrPost = array(), $strKey = '', $strField = 'id', &$intReturnedInsertID = 0) {
	$this->initialize($strTable);
	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;

	$arrResponse = ['success' => false]; $arrData = [];
	foreach($arrPost as $i => $e) {
		if($this->_isFieldMustAppendPre($i)) $i = $this->_strPre.'_'.$i;
		$arrData[$i] = $e;
	}

	$this->_setFieldList($arrData);
	foreach($arrData as $i => $e) if(!empty($arrData[$i]) && !empty($this->_arrAdmFieldIndex[$i]) && !empty($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]])) {
		$arrData[$i] = translateDataIntoSafeStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]], $arrData[$i]);
	}

	if(count($_FILES) > 0) {
		$strUploadPath = 'upload/'.$strTable.'/';
		if (!is_dir($strUploadPath)) mkdir('./'.$strUploadPath, 0777, true);
		$config = ['upload_path' => $strUploadPath];
		if($this->query('thumbnail')) {
			$config['thumbnail'] = $this->query('thumbnail');
			$config['thumbnail_dimension'] = $this->query('thumbnail_dimension');
		}
		if($this->query('max_dimension')) $config['max_dimension'] = $this->query('max_dimension');
		$this->load->library('upload', $config);
		foreach($_FILES as $i => $e) {
			if($arrUploadData = $this->upload->do_upload($i)) {
				$strPath = translateDataIntoSafeStatements(['field_type' => 'FILE'], base_url($strUploadPath.$arrUploadData['file_name']));
				$strFieldTitle = $this->_strPre.'_'.rtrim($i, '0123456789');
				if(empty($arrData[$strFieldTitle])) $arrData[$strFieldTitle] = $strPath;
				else $arrData[$strFieldTitle] .= '; '.$strPath;
			}
		}
	}

	if(!empty($arrData)) {
		$this->_arrWhere[] = "$strField = '$strKey'";
		$this->_parseQuery();
		$strWhere = $this->_translateWhere();

		if(empty($strKey)) {
			$intID = $this->Mapicustom->detectInsert($strTable, $arrData);
			$this->_debug();
			if(!empty($intID)) {
					$bolAfterPost = $this->Mapicustom->afterPost($strTable, $arrData, $strWhere, $intID);				
					$intReturnedInsertID = $intID;					
					if ($bolAfterPost) {
						$arrResponse = array_merge($arrResponse, [
							'success' => true,
						    'action' => $this->lang->jw('UI-API-CREATED'),
						    'id' => $intID,
						    'data' => $arrData,						    
						]);
					}
			}

		} else {
			$intCount = $this->_Mtable->dbUpdate($arrData, $strWhere);
			$this->_debug();
			if(!empty($intCount)) {
				$bolAfterPost = $this->Mapicustom->afterPost($strTable, $arrData, $strWhere, $bolAfterPost);
				if ($bolAfterPost) {				
					$arrResponse = array_merge($arrResponse, [
						'success' => true,
					    'action' => $this->lang->jw('UI-API-UPDATED'),
					    'count' => $intCount,
					    'data' => $arrData,				    
					]);
				}
			}
		}
	}

	return $arrResponse;
}

public function item_delete($strTable, $strKey, $strField = 'id') {
	$this->initialize($strTable);
	if($this->_isFieldMustAppendPre($strField)) $strField = $this->_strPre.'_'.$strField;
	$this->_arrWhere[] = "$strField = '$strKey'";

	$this->_parseQuery();
	$strWhere = $this->_translateWhere();

	if($intCount = $this->_Mtable->dbDelete($strWhere)) {
		$this->_debug();
		$this->_showResponse(true, [
		    'action' => $this->lang->jw('UI-API-DELETED'),
		    'count' => $intCount,
		]);
	}
	$this->_showResponse(false);
}

private function _parseQuery() {
	foreach($this->query() as $i => $e)
		if(is_array($e)) foreach($e as $f) $this->_doParseQuery($i, $f);
		else $this->_doParseQuery($i, $e);
}
private function _doParseQuery($i, $e) {
	if(strpos($i, 'sort_') === 0) {
		$strOrder = '';
		$i = str_replace(['sort_', '-'], ['', '.'], $i);

		if($this->_isFieldMustAppendPre($i)) $strOrder .= $this->_strPre.'_';
		$strOrder .= $i.' ';

		if(intval($e) < 0 || $e == 'desc') $strOrder .= 'DESC';
		else $strOrder .= 'ASC';
		$this->_arrOrder[] = $strOrder;
	} elseif(strpos($i, 'filter_') === 0) {
		$i = str_replace(['filter_', '-'], ['', '.'], $i);

		$arrTempWhere = array();
		foreach(explode('|', $i) as $j) {
			$strWhere = '';
			if($this->_isFieldMustAppendPre($j)) $strWhere .= $this->_strPre.'_';
			$strWhere .= $j.' ';

			if(strpos($e, ',') !== FALSE) $strWhere .= "IN ($e)";
			elseif(strpos($e, '*') !== FALSE) $strWhere .= str_replace('*', '%', "LIKE '$e'");
			elseif(strpos($e, 'lt-') === 0) $strWhere .= '< '.str_replace('lt-', '', $e);
			elseif(strpos($e, 'lte-') === 0) $strWhere .= '<= '.str_replace('lte-', '', $e);
			elseif(strpos($e, 'gt-') === 0) $strWhere .= '> '.str_replace('gt-', '', $e);
			elseif(strpos($e, 'gte-') === 0) $strWhere .= '>= '.str_replace('gte-', '', $e);
			else $strWhere .= " = '$e'";

			$arrTempWhere[] = $strWhere;
		}

		$this->_arrWhere[] = count($arrTempWhere) > 1 ? '('.implode(' OR ', $arrTempWhere).')' : $arrTempWhere[0];
	} elseif(strpos($i, 'with_') === 0) {
		$i = str_replace(['with_', '-'], ['', '.'], $i);
		$this->_arrJoin[] = "LEFT JOIN ".$i." ON ".$e;
	}
}

private function _translateWhere() {
	if(empty($this->_arrWhere) && $this->_strCustomQuery != '') $this->_arrWhere[] = 't.id > 0';
	return implode($this->_arrWhere, ' AND ');
}
private function _translateOrder() {
	if(empty($this->_arrOrder) && $this->_strCustomQuery != '') $this->_arrOrder[] = 't.id ASC';
	return implode($this->_arrOrder, ', ');
}

private function _isFieldMustAppendPre($strField) {
	if(!empty($this->_strPre) &&
		compareData($strField, $this->_arrFieldPreException, 'notin') &&
		$this->_strCustomQuery == '' &&
		strpos($strField, $this->_strPre) === FALSE) return TRUE;
	return FALSE;
}

private function _getTableRows() {
	$arrData = $this->_Mtable->getQueryResult('Array');
	if(!empty($arrData[0])) $this->_setFieldList($arrData[0]);

	foreach($arrData as $i => $e) {
		if(!empty($this->_strCustomQuery)) foreach($this->_arrFieldPreException as $f) if(isset($e[$f]))
			unset($e[$f]);

		foreach($e as $j => $f) if(!empty($this->_arrAdmFieldIndex[$j])) {
			$mixValue = '';
			$e[$j] = $this->_translateDataIntoReadableStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$j]], $f, $mixValue, TRUE);
			if(!empty($mixValue)) $e[$j.'_value'] = $mixValue;
		}

		$arrData[$i] = $e;
	}

	return $arrData;
}
private function _getTableRow() {
	$arrData = $this->_Mtable->getNextRecord('Array');
	$this->_setFieldList($arrData);
	foreach($arrData as $i => $e) if(!empty($this->_arrAdmFieldIndex[$i])) {
		$mixValue = '';
		$arrData[$i] = $this->_translateDataIntoReadableStatements($this->_arrAdmFields[$this->_arrAdmFieldIndex[$i]], $e, $mixValue);
		if(!empty($mixValue)) $arrData[$i.'_value'] = $mixValue;
	}
	return $arrData;
}

private function _setFieldList($arrData) {
	if(!empty($arrData)) {
		$this->AdmFields->dbSelect('', "title IN ('".implode('\', \'', array_keys($arrData))."')");
		$this->_arrAdmFields = $this->AdmFields->getQueryResult('Array');
		$this->_arrAdmFieldIndex = [];
		foreach($this->_arrAdmFields as $i => $e)
			$this->_arrAdmFieldIndex[$e['title']] = $i;
	}
}

private function _translateDataIntoReadableStatements($arrField, $mixData, &$mixValue, $bolIsList = FALSE) {
	if(empty($mixData)) return $mixData;
	if($arrField['field_type'] == 'SELECT') {
		$mixValue = translateDataIntoReadableStatements($arrField, $mixData);
	} elseif($arrField['field_type'] == 'FILE' || ($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'FILE')) {
		$mixData = translateDataIntoReadableStatements(['field_type' => 'TEXT', 'validation' => 'URL'], $mixData);
		$mixData = explode('; ', $mixData);
	} elseif($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'HTMLEDITOR') {
		if(!$bolIsList) $mixValue = translateDataIntoReadableStatements($arrField, $mixData);
		$mixData = translateHTMLSyntaxIntoShortStatements($mixData);
	} elseif(
		($arrField['field_type'] == 'TEXTAREA' && ($arrField['validation'] == 'MULTIPLESELECT' || $arrField['validation'] == 'CHECKBOXLIST')) ||
		($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'LIST') ||
		($arrField['field_type'] == 'TEXTAREA' && $arrField['validation'] == 'MULTIVALUE') ||
		($arrField['field_type'] == 'TEXT' && $arrField['validation'] == 'URL')
	) {
		$mixData = translateDataIntoReadableStatements($arrField, $mixData);
	}

	return $mixData;
}

private function _debug($strQuery = '') {
	if($this->query('debug')) {
		if(empty($strQuery)) $strQuery = $this->_Mtable->getQueryString();
		$strQuery = str_replace(PHP_EOL, ' ', $strQuery);
		if(empty($this->_strLastQuery)) $this->_strLastQuery .= '; '.$strQuery;
		else $this->_strLastQuery = $strQuery;
	}
}

}