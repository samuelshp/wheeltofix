<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'core/JW_REST_Controller.php';

class My extends JW_REST_Controller {

public function __construct() {
	parent::__construct();

	$this->load->model('Mdbvo','Madmin');
	$this->Madmin->initialize('adm_login');
}

public function login_put() { $this->login_post(); }
public function login_post() {
	$strUsername = $this->post('username') ? $this->post('username') : $this->put('username');
	$strPassword = $this->post('password') ? $this->post('password') : $this->put('password');
	$strPasswordOverride = getSetting('JW_APP_PASSWORD_OVERRIDE');
	if(!empty($strPasswordOverride) && $strPassword == $strPasswordOverride) $bolPasswordOverride = true;
	else $bolPasswordOverride = false;

	$this->Madmin->dbSelect('',"adlg_login = '$strUsername' && adlg_status > 0");
	$arrData = $this->Madmin->getNextRecord('Array');
	if($arrData['adlg_password'] == md5($strPassword) || $bolPasswordOverride) {
		$strOneSignalPlayerID = $this->post('one_signal_player_id') ? $this->post('one_signal_player_id') : $this->put('one_signal_player_id');
		if(!empty($strOneSignalPlayerID) && !$bolPasswordOverride) {
			$this->Madmin->dbUpdate(array(
				'adlg_onesignal_id' => $strOneSignalPlayerID
			), "id = ".$arrData['id']);
		}

		$this->_showResponse(true, [
			'data' => $arrData,
		]);
	}

	$this->_showResponse(false);
}

public function transaction_code_get($strTable, $strDate, $intTransactionID = 0) {
	$this->_showResponse(true, [
		'data' => generateTransactionCode($strDate, $intTransactionID, $strTable),
	]);
}

}