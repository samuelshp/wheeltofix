<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'core/JW_REST_Controller.php';
/**
 * 
 */
class Helper extends JW_REST_Controller
{
	
	public function __construct()
	{
		parent::__construct();		
	}

	public function convertsatuan_get($strMode = 'display',$strSatuanType, $intProductID,$doubleQty, $strSatuanTypeTarget = '')
	{						
		if ($strMode == 'concluded') {
			$arrData = convertQuantity($doubleQty,$intProductID,'concluded',$strSatuanType);
		}

		if ($strMode == 'display') {
			$arrData = convertQuantity($doubleQty,$intProductID,'display',$strSatuanType);
		}

		if ($strMode == 'rebase') {
			$baseQty = convertQuantity($doubleQty,$intProductID,'concluded',$strSatuanType);
			$arrData = convertQuantity($baseQty,$intProductID,'display',$strSatuanTypeTarget);
		}

		$this->_showResponse(true,[
			'data' => $arrData
		]);
	}

	public function countstock_get($intProductID, $intWarehouseID)
	{
		$intStock = countStock($intProductID,$intWarehouseID);
		$this->_showResponse(true,[
			'data' => $intStock
		]);
	}

	public function pricedisplay_get($intPrice)
	{
		$this->_showResponse(true,[
			'data' => setPrice($intPrice)
		]);		
	}

	public function shortenFilename($strPath)
	{
		$this->_showResponse(true,[
			'data' => shortenFilename($strPath)
		]);
	}

}