<?php

class Inventory_ajax extends JW_Controller {

public function getProductStock($intProductID='0',$intWarehouse='0') {
    $this->load->model('Mproduct');
    ArrayToXml(array('Stock' => countStock($intProductID,$intWarehouse), 'Product' => $this->Mproduct->getItemByID($intProductID)));
}

public function getProductStockNew($strProductID = '0',$strPurchasedID = '0',$intWarehouse = '0') {
    # INIT
    $this->load->model('Mproduct');
    $arrIDProduct = explode('-',$strProductID);
    $arrIDPurchased = explode('-',$strPurchasedID);
    $arrStock = array();
    $i = 0;
    foreach($arrIDProduct as $e){
        if($e!='0'){
            $arrStock[] = array(
                'stock' => countStock($e,$intWarehouse),
                'id' => $arrIDPurchased[$i]
            );
            $i++;
        }
    }

    ArrayToXml(array('Stock' => $arrStock, 'Product' => $this->Mproduct->getItemByID($strProductID)));
}

public function getWarehouseComplete($txtData = '') {
    # INIT
    $this->load->model('Mwarehouse');

    ArrayToXml(array('Warehouse' => $this->Mwarehouse->getAllData($txtData)));
}

public function getAvailableProductCode($txtData = '') {
    # INIT
    $this->load->model('Mproduct');

    ArrayToXml(array('Product' => $this->Mproduct->getCodeAvailable($txtData)));
}

public function getComparisonUnits($intUnitID1,$intUnitID2) {
    # INIT
    $this->load->model('Munit');

    ArrayToXml(array('Unit' => $this->Munit->getComparisonUnits($intUnitID1,$intUnitID2)));
}

public function getConversionUnit($intUnitID1) {
    # INIT
    $this->load->model('Munit');

    ArrayToXml(array('Unit'=>$this->Munit->getConversion($intUnitID1)));
}

public function getUnitsByProductID($intID,$intWarehouse='0') {
    # INIT
    $this->load->model('Munit');
    $arrComp = $this->Munit->getUnitsForCalculation($intID);

    if($intWarehouse != '0') $arrData = array('Unit' => $arrComp,'Stock' => countStock($intID,$intWarehouse));
    else $arrData = array('Unit' => $arrComp);
    

    ArrayToXml($arrData);
}

}