<?php

class Subkontrak_ajax extends JW_Controller {

    public function getMaterialItems($intMaterialID)
    {
        $this->load->model('Mproduct');
        $material = $this->Mproduct->getItemByID($intMaterialID);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($material));
    }

}

?>