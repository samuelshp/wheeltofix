<?php
/**
 * 
 */
class Debt extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_CI =& get_instance();
		if($this->session->userdata('strAdminUserName') == '') redirect();
    
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		$this->load->model('Mkontrak');
		$this->load->model('Msupplier');
		$this->load->model('Mtransout');
		$this->load->model('Mtransoutdetail');
		$this->load->model('Mdebt');
		$this->load->model('Maccount');
		
	}

	public function pembayaran_hutang($type = '', $intID = 0)
	{		
		$this->_getMenuHelpContent(61,true,'adminpage');    

		if ($type == 'add') {			
	        $arrSupplier = $this->Msupplier->getAllData(); 
	        $arrCOA = $this->Maccount->getAllAccount(); #INI DIMASUKKAN KE MODEL transaction_out
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/add',
	            'strPageTitle' => "Pembayaran Hutang",
	            'strLink' => __FUNCTION__,
	            'arrSupplier' => $arrSupplier,
	            'intType' => 1,
	            'arrCOA' => $arrCOA
	        ),$this->admlinklist->getMenuPermission(61,1)));
		}

		if ($type == 'view') {
			$arrSupplier = $this->Msupplier->getAllData(); 
	        $arrCOA = $this->Maccount->getAllAccount();
	        $arrDebt = $this->Mtransout->getTransactionByID($intID);	     
	    	$arrDetail = $this->Mtransout->getDetailTransaction($intID);	    	
	    	
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/view',
	            'strPageTitle' => "Pembayaran Hutang",
	            'strLink' => __FUNCTION__,
	            'intType' => 1,
	            'arrSupplier' => $arrSupplier,	          
	            'arrCOA' => $arrCOA,
	            'arrDebt' => $arrDebt,
	            'arrDetail' => $arrDetail
	        ),$this->admlinklist->getMenuPermission(61,2)));
		}

		if ($type == 'browse') {

			if($this->input->post('subSearch') != '') {		
				$arrSearchData = array(
					'dateStart' => $this->input->post('txtSearchDate'),
					'dateEnd' => $this->input->post('txtSearchDate2'),
					'strSearchKey' => $this->input->post('txtSearchNo'),
					'strSearchOther' => $this->input->post('txtSearch')
				);				
		        $arrPembayaranHutang = $this->Mtransout->searchBy('PHU', $arrSearchData);		        
		        $strPage = '';
		        $strBrowseMode = '<a href="'.site_url('debt/'.__FUNCTION__.'/browse', NULL, FALSE).'">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPembayaranHutang) ? count($arrPembayaranHutang) : '0')." records).";		       
		    }else{
		    	$arrPagination['base_url'] = site_url("debt/pembayaran_hutang/browse?pagination=true", NULL, FALSE);
		        $arrPagination['total_rows'] = $this->Mtransout->getCount('PHU');
		        $arrPagination['per_page'] = $this->config->item('jw_item_count');		        
		        $this->pagination->initialize($arrPagination);
		        $strPage = $this->pagination->create_links();
		        $intPage = 0;
		        $strBrowseMode = '';
		        if($this->input->get('page') != '') $intPage = $this->input->get('page');
		        $arrPembayaranHutang = $this->Mtransout->getAllTransaction('PHU',$intPage,$arrPagination['per_page']);
		    }

	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'debt/browse',
	            'strPageTitle' => 'Pembayaran Hutang',
	            'strPage' => $strPage,
	            'strLink' => __FUNCTION__,
	            'strSearchDate' => $strSearchDate,
	            'strSearchDate2' => $strSearchDate2,
	            'strSearchKey' => $strSearchKey,
	            'intType' => 1,	            
	            'arrPembayaranHutang' => $arrPembayaranHutang
	        ),$this->admlinklist->getMenuPermission(61,2)));
	        
		}

		if ($type == '') {
			
			if ($this->input->post('smtMakeDebt') != '') {				
				
				$bolUpdate = false;				
				$this->load->model('Mdaftarpembayaranhutang');
				$intSupplierID = $this->input->post('intSupplier');

				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(
					'txou_code' => generateTransactionCode($this->input->post('txtDate'),'','pembayaran_hutang'),
					'txou_date' => $this->input->post('txtDate'),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => $this->input->post('intMethod'),
					'txou_ref_giro' => $this->input->post('txtRefGiro'),
					'txou_jatuhtempo' => $this->input->post('txtDueDate'),
					'txou_status_trans' => STATUS_WAITING_FOR_FINISHING
				);
			
				$intTransout = $this->Mtransout->addTransOut($arrDataTransaksi);				
									
				$arrDataDetail= array(
					'txod_txou_id' =>  $intTransout,
					'txod_ppn' => $this->input->post('intPPN'),
					'txod_pph' => $this->input->post('intPPH'),
					'txod_amount' => $this->input->post('intAmount'),
					'txod_totalbersih' => $this->input->post('intAmount'),
					'txod_ref_giro' => $this->input->post('txtRefGiro'),
					'txod_jatuhtempo' => $this->input->post('txtDueDate'),
					'txod_type' => 'DPH',
					'txod_ref_id' => $this->input->post('idDPH'),
					'txod_supp_id' => $this->input->post('intSupplier'),
					'txod_filter_by' => $this->input->post('chbxDibayar')
				);
				
				$bolInsertDetail = $this->Mtransoutdetail->add($arrDataDetail, true);

				$this->load->model('Mdaftarpembayaranhutangitem');
				
				if ($bolInsertDetail){
					#update Status menjadi 4 di DPH dan tabel terkait
					$this->load->model('Mtransoutdetail');
					$bolUpdate = $this->Mdaftarpembayaranhutangitem->updateStatusReferensi($arrDataDetail['txod_filter_by']);
					#update Amount Terbayar PI / Downpayment
					$bolUpdate = $this->Mdebt->updateTerbayar($arrDataDetail['txod_filter_by']);
				}

				$this->load->model('Mjournal');
				foreach ($this->input->post('idDPH') as $key => $intDPHid) {
					$detailDPH = $this->Mdaftarpembayaranhutangitem->getItemsByID($intDPHid);
					foreach ($detailDPH as $e) {
						if($e['dphi_reff_type'] == "PI") $this->Mjournal->insertHutangPIL($e['item_id']);
						else if($e['dphi_reff_type'] == "DP") $this->Mjournal->insertHutangUMS($e['item_id']);
						else if($e['dphi_reff_type'] == "NKS") $this->Mjournal->insertHutangNKSL($e['item_id']);
					}
				}
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();
				
				if ($bolUpdate){
					$this->session->set_flashdata('strMessage','Pembayaran Hutang berhasil disimpan');
					redirect('debt/pembayaran_hutang/browse');
				} else {
					$this->session->set_flashdata('strMessage','Pembayaran Hutang gagal disimpan');
					redirect('debt/pembayaran_hutang/add');		
				}
			}

			if ($this->input->post('smtUpdateDebt') != '') {
				$idTrans = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(					
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => $this->input->post('intMethod'),
					'txou_ref_giro' => $this->input->post('txtRefGiro'),
					'txou_jatuhtempo' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDueDate'))), "Y-m-d"),
				);								
				
				$intTransout = $this->Mtransout->updateTransOut($arrDataTransaksi, $idTrans);

				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();
				if ($intTransout){
					$this->session->set_flashdata('strMessage','Pembayaran Hutang berhasil diupdate');
					redirect('debt/pembayaran_hutang/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pembayaran Hutang gagal diupdate');
					redirect('debt/pembayaran_hutang/view/'.$idTrans);		
				}

			}

			if ($this->input->post('smtDeleteDebt') != '') {
				$this->load->model('Mdaftarpembayaranhutang');
				$idTrans = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$intDelete = $this->Mtransout->deleteTransout($idTrans);
				$intReverseTransactionBefore = $this->Mtransout->rollbackAfterDelete($idTrans);
				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();
				if ($intDelete){
					$this->session->set_flashdata('strMessage','Pembayaran Hutang berhasil dihapus');
					redirect('debt/pembayaran_hutang/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pembayaran Hutang gagal dihapus');
					redirect('debt/pembayaran_hutang/view/'.$idTrans);		
				}
			}

			if($this->input->post('subPrint') != '' || $this->input->get('print') != '' ) {	

				$intID = ($this->input->post('intID') != '') ? $this->input->post('intID') : $this->input->get('print');				
				$arrDebt = $this->Mtransout->getTransactionByID($intID);	     
	    		$arrDetail = $this->Mtransout->getDetailTransaction($intID);

				$this->load->model('Mdbvo','MAdminLogin');
			    $this->MAdminLogin->initialize('adm_login');

			    $this->MAdminLogin->dbSelect('','id =' . $arrDebt['cby']);
			    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
			    
				$this->load->view('sia_print',array(
		            'strPrintFile' => 'debt',
		            'strPageTitle' => "Pembayaran Hutang",
		            'arrDebt' => $arrDebt,					
					'arrDetail' => $arrDetail,
					'arrLogin' => $arrLogin,
					'intType' => 1,
					'arrDaftarPembayaranHutangItem' => null
		        ));		       
			}
		}
	}

	public function notakreditsupplier($type = '', $intID = 0)
	{		
		if ($type == "add") {

			$arrSupplier = $this->Msupplier->getAllData(); 
			$arrProject = $this->Mkontrak->getKontrak();			
			$arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');		
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/add',
	            'strPageTitle' => "Nota Kredit Supplier",
	            'strLink' => __FUNCTION__,
	            'arrSupplier' => $arrSupplier,
	            'arrProject' => $arrProject,
	            'intType' => 2,
	            'arrCOA' => $arrCOA
	        )));
		}

		if ($type == "view") {

			$arrSupplier = $this->Msupplier->getAllData(); 
	    	$arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');	
	        $arrDebt = $this->Mtransout->getTransactionByID($intID);	     	        
	    	$arrDetail = $this->Mtransout->getDetailTransaction($intID);
	    	$arrProject = $this->Mkontrak->getKontrak();
	    	$arrPurchaseInvoiceSupplier = $this->Mtransout->getPurchaseInvoiceBySupplier($arrDebt['supp_id']);		    	
	    	
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/view',
	            'strPageTitle' => "Nota Kredit Supplier",
	            'strLink' => __FUNCTION__,
	            'intType' => 2,
	            'arrSupplier' => $arrSupplier,	          
	            'arrCOA' => $arrCOA,
	            'arrDebt' => $arrDebt,
	            'arrProject' => $arrProject,
	            'arrPurchaseInvoiceSupplier' => $arrPurchaseInvoiceSupplier,
	            'arrDetail' => $arrDetail
	        )));
		}

		if ($type == "browse") {
			if($this->input->post('subSearch') != '') {						
				$arrSearchData = array(
					'dateStart' => $this->input->post('txtSearchDate'),
					'dateEnd' => $this->input->post('txtSearchDate2'),
					'strSearchKey' => $this->input->post('txtSearchNo'),
					'strSearchOther' => $this->input->post('txtSearch')
				);				
		        $arrTransaction = $this->Mtransout->searchBy('NKS', $arrSearchData);		        		       
		        $strPage = '';
		        $strBrowseMode = '<a href="'.site_url('debt/pengeluaran_lain/browse', NULL, FALSE).'">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrTransaction) ? count($arrTransaction) : '0')." records).";		       
		    }else{		 
				$arrPagination['base_url'] = site_url("debt/notakreditsupplier/browse?pagination=true", NULL, FALSE);
		        $arrPagination['total_rows'] = $this->Mtransout->getCount('NKS');
		        $arrPagination['per_page'] = $this->config->item('jw_item_count');
		        $arrPagination['uri_segment'] = 10;
		        $this->pagination->initialize($arrPagination);
		        $strPage = $this->pagination->create_links();
		        $strBrowseMode = '';
		        $intPage = 0;
		        if($this->input->get('page') != '') $intPage = $this->input->get('page');
		        $arrTransaction = $this->Mtransout->getAllTransaction('NKS', $intPage,$arrPagination['per_page']);
	        }
	        $this->load->view('sia', array(
	            'strViewFile' => 'debt/browse',
	            'strPageTitle' => 'Nota Kredit Supplier',
	            'strPage' => $strPage,
	            'strSearchDate' => $strSearchDate,
	            'strSearchDate2' => $strSearchDate2,
	            'strSearchKey' => $strSearchKey,
	            'intType' => 2,
	            'strLink' => __FUNCTION__,
	            'arrPembayaranHutang' => $arrTransaction
	        ));
		}

		if ($type == "") {

			if ($this->input->post('smtMakeDebt') != '') {					
								
				$intSupplierID = $this->input->post('intSupplier');
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(
					'txou_code' => generateTransactionCode($this->input->post('txtDate'),'','nota_kredit_supplier'),
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txou_description' => $this->input->post('txtDescription'),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => 0,
					'txou_status_trans' => STATUS_APPROVED
				);		

				$intTransout = $this->Mtransout->addTransOut($arrDataTransaksi);

				$arrDataDetail = array(
					'txod_txou_id' => $intTransout,
					'txod_ppn' => $this->input->post('intPPN'),
					'txod_pph' => $this->input->post('intPPH'),
					'txod_amount' => $this->input->post('intAmount'),
					'txod_totalbersih' => $this->input->post('intTotal'),
					'txod_sisa_bayar' => $this->input->post('intTotal'),
					'txod_ref_giro' => $this->input->post('txtRefGiro'),
					'txod_jatuhtempo' => $this->input->post('txtDueDate'),
					'txod_type' => 'NKS',
					'txod_ref_id' => $this->input->post('listProyek'),
					'txod_supp_id' => $this->input->post('intSupplier')
				);
				
				$this->Mtransoutdetail->add($arrDataDetail);
			
				$this->load->model('Mjournal');
				$this->Mjournal->insertHutangNKS($intTransout);
				// $this->Mjournal->postTransaction('nks',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
		        $this->_CI->db->trans_complete();
				// foreach ( as $key => $value){				
				// 	$this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);
				// }

				$arrDataLaporan = array(
					'nomormhsupplier' => $intSupplierID,
					'biaya' => 0,
					'jenis' => "NOTA_KREDIT_SUPPLIER",
					'tanggal' => $arrDataTransaksi['txou_date'],
					'transaksi_nomor' => $intTransout,
					'transaksi_kode' => $arrDataTransaksi['txou_code'],
					'transaksi_tanggal' => $arrDataTransaksi['txou_date'],
					'jatuh_tempo' => $arrDataDetail['txod_jatuhtempo'],
					'total' => $arrDataDetail['txod_amount'],					
					'keterangan' => $arrDataTransaksi['txou_description']					
				);

				// $this->Mtransout->copyToLaporan($arrDataLaporan);				
							
				redirect('debt/notakreditsupplier/browse');		
			
			}

			if ($this->input->post('smtUpdateDebt') != '') {

				$id = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$arrDataTransaksi = array(					
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txou_description' => $this->input->post('txtDescription'),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => 0,
				);

				$this->Mtransout->updateTransOut($arrDataTransaksi, $id);

				$arrDataDetail = array(
					'txod_id' => $this->input->post('intIDdetail'),
					'txod_ppn' => $this->input->post('intPPN'),
					'txod_pph' => $this->input->post('intPPH'),
					'txod_amount' => $this->input->post('intAmount'),
					'txod_totalbersih' =>$this->input->post('intTotal'),
					'txod_sisa_bayar' => $this->input->post('intTotal'),
					'txod_ref_giro' => $this->input->post('txtRefGiro'),
					'txod_jatuhtempo' => $this->input->post('txtDueDate'),
					'txod_type' => 'NKS',
					'txod_ref_id' => $this->input->post('listProyek'),
					'txod_supp_id' => $this->input->post('intSupplier')
				);
				
				$intUpdate = $this->Mtransoutdetail->update($arrDataDetail, $id);						

				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				// $this->Mjournal->postTransaction('nks',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();
				if ($intUpdate){
					$this->session->set_flashdata('strMessage','Nota Kredit Supplier berhasil diupdate');
					redirect('debt/notakreditsupplier/view/'.$id);
				}else{
					$this->session->set_flashdata('strMessage','Nota Kredit Supplier gagal diupdate');
					redirect('debt/notakreditsupplier/view/'.$id);		
				}
			}

			if ($this->input->post('smtDeleteDebt') != '') {
				$id = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$intDelete = $this->Mtransout->deleteTransout($id);
				$this->load->model('Mjournal');
				$this->Mjournal->resetRHLaporan();
				$this->_CI->db->trans_complete();
				if ($intDelete){
					$this->session->set_flashdata('strMessage','Nota Kredit Supplier berhasil dihapus');
					redirect('debt/notakreditsupplier/browse');
				}else{
					$this->session->set_flashdata('strMessage','Nota Kredit Supplier gagal dihapus');
					redirect('debt/notakreditsupplier/view/'.$id);		
				}
			}

			if($this->input->post('subPrint') != '' || $this->input->get('print') != '' ) {	

				$intID = ($this->input->post('intID') != '') ? $this->input->post('intID') : $this->input->get('print');				
				$arrDebt = $this->Mtransout->getTransactionByID($intID);	     
	    		$arrDetail = $this->Mtransout->getDetailTransaction($intID);
	    		
				$this->load->model('Mdbvo','MAdminLogin');
			    $this->MAdminLogin->initialize('adm_login');

			    $this->MAdminLogin->dbSelect('','id =' . $arrDebt['cby']);
			    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
			    
				$this->load->view('sia_print',array(
		            'strPrintFile' => 'debt',
		            'strPageTitle' => "Nota Kredit Supplier",
		            'arrDebt' => $arrDebt,					
					'arrDetail' => $arrDetail,
					'arrLogin' => $arrLogin,
					'intType' => 2,
					'arrDaftarPembayaranHutangItem' => null
		        ));		       
			}
		}
	}

	public function notadebetsupplier($type = '', $intID = 0)
	{
		if ($type == "add") {

			$arrSupplier = $this->Msupplier->getAllData(); 
			$arrCOA = $this->Maccount->getAllAccount();
			$arrProject = $this->Mkontrak->getKontrak();		
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/add',
	            'strPageTitle' => "Nota Debet Supplier",
	            'strLink' => __FUNCTION__,
	            'arrSupplier' => $arrSupplier,
	             'arrProject' => $arrProject,
	            'intType' => 2,
	            'arrCOA' => $arrCOA
	        )));
		}

		if ($type == "view") {
			$arrSupplier = $this->Msupplier->getAllData(); 
	        $arrCOA = $this->Maccount->getAllAccount(); #INI DIMASUKKAN KE MODEL transaction_out
	        $arrProject = $this->Mkontrak->getKontrak();
	        $arrDebt = $this->Mtransout->getTransactionByID($intID);	     
	    	$arrDetail = $this->Mtransout->getDetailTransaction($intID);
	    	$arrPurchaseInvoiceSupplier = $this->Mtransout->getPurchaseInvoiceBySupplier($arrDebt['supp_id']);	
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/view',
	            'strPageTitle' => "Nota Debet Supplier",
	            'strLink' => __FUNCTION__,
	            'intType' => 2,
	            'arrSupplier' => $arrSupplier,	          
	            'arrProject' => $arrProject,
	            'arrCOA' => $arrCOA,
	            'arrDebt' => $arrDebt,
	            'arrPurchaseInvoiceSupplier' => $arrPurchaseInvoiceSupplier,
	            'arrDetail' => $arrDetail
	        )));
		}

		if ($type == "browse") {
			$arrPagination['base_url'] = site_url("debt/notadebetsupplier/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mtransout->getCount('NDS');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 10;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrTransaction = $this->Mtransout->getAllTransaction('NDS',$intPage,$arrPagination['per_page']);
	        
	        $this->load->view('sia', array(
	            'strViewFile' => 'debt/browse',
	            'strPageTitle' => 'Nota Debet Supplier',
	            'strPage' => $strPage,
	            'intType' => 2,
	            'strLink' => __FUNCTION__,
	            'arrPembayaranHutang' => $arrTransaction
	        ));
		}

		if ($type == "") {

			if ($this->input->post('smtMakeDebt') != '') {	
								
				$intSupplierID = $this->input->post('intSupplier');
													
				$arrDataTransaksi = array(
					'txou_code' => generateTransactionCode($this->input->post('txtDate'),'','nota_debet_supplier'),
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-d-m"),
					'txou_description' => $this->input->post('txtDescription'),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => 0,
					'txou_status_trans' => STATUS_WAITING_FOR_FINISHING
				);				
				
				$intTransout = $this->Mtransout->addTransOut($arrDataTransaksi);

				$arrDataDetail = array(
					'txod_txou_id' => $intTransout,
					'txod_ppn' => str_replace(",", ".", str_replace(".", "", $this->input->post('intPPN'))),
					'txod_pph' => str_replace(",", ".", str_replace(".", "", $this->input->post('intPPH'))),
					'txod_amount' => str_replace(",", ".", str_replace(".", "", $this->input->post('intAmount'))),
					'txod_ref_giro' => $this->input->post('txtRefGiro'),
					'txod_jatuhtempo' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDueDate'))), "Y-d-m"),
					'txod_type' => 'NDS',
					'txod_ref_id' => $this->input->post('listProyek'),
					'txod_supp_id' => $this->input->post('intSupplier')
				);

				$this->Mtransoutdetail->add($arrDataDetail);

				// if ($this->db->trans_status() === FALSE)
				// {
				//         $this->db->trans_rollback();
				// }
				// else
				// {
				//         $this->db->trans_commit();
				// }
				// foreach ( as $key => $value){				
				// 	$this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);
				// }
				
				// update transaction id pada DPH yang terbayar (berdasarkan supplier)												
				redirect('debt/notadebetsupplier/browse');		
			}

			if ($this->input->post('smtUpdateDebt') != '') {	
				$intSupplierID = $this->input->post('intSupplier');
			}
		}
	}

	public function pengeluaran_lain($type = '', $intID = 0)
	{
		// $this->_getMenuHelpContent(247,true,'adminpage');    

		if ($type == "add") {
			 $arrSupplier = $this->Msupplier->getAllData(); 
			 $arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
			 $arrProject = $this->Mkontrak->getKontrak();
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/add',
	            'strPageTitle' => "Pengeluaran Lain-Lain",
	            'strLink' => __FUNCTION__,
	            'arrSupplier' => $arrSupplier,
	            'arrProject' => $arrProject,
	            'intType' => 3,
	            'arrCOA' => $arrCOA
	        )));
		}

		if ($type == "view") {
			$arrSupplier = $this->Msupplier->getAllData(); 
			$arrCOA = $this->Maccount->getAllAccount();
			$arrCOAdetail = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
			$arrProject = $this->Mkontrak->getKontrak();
	        $arrDebt = $this->Mtransout->getTransactionByID($intID);	     	        
	    	$arrDetail = $this->Mtransout->getDetailTransaction($intID);	    	
	    	$arrPurchaseInvoiceSupplier = $this->Mtransout->getPurchaseInvoiceBySupplier($arrDebt['supp_id']);	
	    	
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'debt/view',
	            'strPageTitle' => "Pengeluaran Lain-Lain",
	            'strLink' => __FUNCTION__,
	            'intType' => 3,
	            'arrSupplier' => $arrSupplier,	          
	            'arrCOA' => $arrCOA,
	            'arrCOAdetail' => $arrCOAdetail,
	            'arrDebt' => $arrDebt,
	            'arrProject' => $arrProject,
	            'arrPurchaseInvoiceSupplier' => $arrPurchaseInvoiceSupplier,
	            'arrDetail' => $arrDetail
	        )));
		}

		if ($type == "browse") {			
			$arrPagination['base_url'] = site_url("debt/pengeluaran_lain/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mtransout->getCount('PGL');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();	        
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrTransaction = $this->Mtransout->getAllTransaction('PGL', $intPage, $arrPagination['per_page']);		  

			if($this->input->post('subSearch') != '') {
				$arrSearchData = array(
					'dateStart' => $this->input->post('txtSearchDate'),
					'dateEnd' => $this->input->post('txtSearchDate2'),
					'strSearchKey' => $this->input->post('txtSearchNo'),
					'strSearchOther' => $this->input->post('txtSearch')
				);
		        $arrTransaction = $this->Mtransout->searchBy('PGL', $arrSearchData);
		        $strPage = '';
		        $strBrowseMode = '<a href="'.site_url('debt/pengeluaran_lain/browse', NULL, FALSE).'">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrTransaction) ? count($arrTransaction) : '0')." records).";		       
		    }
	        	
	        	$this->load->view('sia', array(
		            'strViewFile' => 'debt/browse',
		            'strPageTitle' => 'Pengeluaran Lain Lain',
		            'strPage' => $strPage,
		            'strLink' => __FUNCTION__,
		            'intType' => 3,
		            'strSearchDate' => $strSearchDate,
		            'strSearchDate2' => $strSearchDate2,
		            'strSearchKey' => $strSearchKey,
		            'arrPembayaranHutang' => $arrTransaction
		        ));
		}

		if ($type == "") {

			if ($this->input->post('smtMakeDebt') != '') {
				
				$this->load->model('Mdaftarpembayaranhutang');				
				
				$this->_CI->db->trans_start();

				$arrDataTransaksi = array(
					'txou_code' => generateTransactionCode($this->input->post('txtDate'),'','pengeluaran_lain_lain'),
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => $this->input->post('intMethod'),		
					'txou_ref_giro' => $this->input->post('txtRefGiro'),
					'txou_jatuhtempo' => str_replace("/", "-", $this->input->post('txtDueDate')),
					'txou_status_trans' => STATUS_FINISHED			
				);
				
				$intTransout = $this->Mtransout->addTransOut($arrDataTransaksi);				
				
				$this->load->model('Mtransoutdetail');
							
				$arrDataDetail = array(
					'txod_txou_id' => $intTransout,
					'txod_ppn' => $this->input->post('intPPN'),
					'txod_pph' => $this->input->post('intPPH'),
					'txod_amount' => $this->input->post('intAmount'),
					'txod_totalbersih' => $this->input->post('intAmount'),
					'txod_coa' => $this->input->post('intCOAdetail'),						
					'txod_type' => 'PGL',
					'txod_ref_id' => $this->input->post('listProyek'),
					'txod_description' => $this->input->post('keterangan')
				);
				
				$intTransoutDetail = $this->Mtransoutdetail->add($arrDataDetail);				
				// $this->Mdaftarpembayaranhutang->updateTrxOutID($arrDataDetail['txod_ref_id']);				
				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));			
				$this->_CI->db->trans_complete();				
								
				if ($intTransoutDetail){
					$this->session->set_flashdata('strMessage','Pengeluaran Lain berhasil disimpan');
					redirect('debt/pengeluaran_lain/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pengeluaran Lain gagal disimpan');
					redirect('debt/pengeluaran_lain/add');		
				}
			}

			if ($this->input->post('smtUpdateDebt') != '') {	
				$intID = $this->input->post('intID');
				$this->load->model('Mdaftarpembayaranhutang');				
				$this->_CI->db->trans_start();				
				$arrDataTransaksi = array(					
					'txou_date' => date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"),
					'txou_coa' => $this->input->post('intCOA'),
					'txou_metode' => $this->input->post('intMethod'),	
					'txou_ref_giro' => ($this->input->post('intMethod') == 3 ) ? $this->input->post('txtRefGiro') : null,
					'txou_jatuhtempo' => ($this->input->post('intMethod') == 3 ) ? str_replace("/", "-", $this->input->post('txtDueDate')) : null,
					'txou_status_trans' => STATUS_FINISHED
				);											
				
				$intTransout = $this->Mtransout->updateTransOut($arrDataTransaksi,$intID);	

				$this->load->model('Mtransoutdetail');
				
				$arrDataDetail = array(										
					'txod_id' => $this->input->post('intIDdetail'),
					'txod_coa' => $this->input->post('intCOAdetail'),
					'txod_amount' => $this->input->post('intAmount'),			
					'txod_totalbersih' => $this->input->post('intAmount'),					
					'txod_type' => 'PGL',
					'txod_ref_id' => $this->input->post('listProyek'),
					'txod_description' => $this->input->post('keterangan'),
					'txod_item_delete' => $this->input->post('deletedItem'),
				);
			
				$intUpdate = $this->Mtransoutdetail->update($arrDataDetail, $intID);

				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();				
				if ($intUpdate){
					$this->session->set_flashdata('strMessage','Pengeluaran Lain berhasil diupdate');
					redirect('debt/pengeluaran_lain/view/'.$intID);
				}else{
					$this->session->set_flashdata('strMessage','Pengeluaran Lain berhasil diupdate');
					redirect('debt/pengeluaran_lain/view/'.$intID);		
				}

			}

			if ($this->input->post('smtDeleteDebt')) {
				$idTrans = $this->input->post('intID');
				$this->_CI->db->trans_start();
				$intDelete = $this->Mtransout->deleteTransout($idTrans);
				
				$this->load->model('Mjournal');
				// $this->Mjournal->postTransaction('kasbankgiro_keluar',date_format(date_create(str_replace("/", "-", $this->input->post('txtDate'))), "Y-m-d"));
				$this->_CI->db->trans_complete();
				if ($intDelete){
					$this->session->set_flashdata('strMessage','Pengeluaran Lain berhasil dihapus');
					redirect('debt/pengeluaran_lain/browse');
				}else{
					$this->session->set_flashdata('strMessage','Pengeluaran Lain gagal dihapus');
					redirect('debt/pengeluaran_lain/view/'.$idTrans);		
				}
			}

			if ($this->input->post('btnSearch') != '') {
				
			}

		}
	}
}