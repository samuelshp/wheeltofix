<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)
- getDataForPurchase(intID)
- getBonusDataForPurchase(intID) //gak dipake
- getPurchaseOrderAutoComplete(txtData)
- getPurchaseOrderSupplierAutoComplete(txtData)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase_order extends JW_Controller {

	private $_CI;

	public function __construct() {
		parent::__construct();
		$this->_CI =& get_instance();
		if($this->session->userdata('strAdminUserName') == '') redirect();
		
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
		$this->load->model('Mpurchaseorder');
		$this->load->model('Mkontrak');
		$this->load->model('Msubkontrak');
		$this->load->model('Mpurchaseorderitem');
		$this->load->model('Mnotification');
	
		$this->_getMenuHelpContent(21,true,'adminpage');
	}
	
	public function index() {
		// WARNING! Don't change the following steps
		if($this->input->post('smtUpdatePurchase') != '' || $this->input->post('smtMakePurchase') != '') {
		}
	
		if($this->input->post('smtMakePurchase') != '') { // Make Purchase
			$this->_CI->db->trans_start();
			$arrPurchaseOrder = array(
				'txtNoPB' => $this->input->post('txtNoPB'),
				'txtDateBuat' => $this->input->post('txtDateBuat'),
				'txtDescription' => $this->input->post('txtaDescription'),
				'idKontrak' => $this->input->post('kontrakID'),
				'idSubKontrak' => $this->input->post('subKontrakID')
			);
			$intPurchaseID = $this->Mpurchaseorder->addPO('', $arrPurchaseOrder['txtDateBuat'], $arrPurchaseOrder['txtDescription'], $arrPurchaseOrder['idKontrak'], $arrPurchaseOrder['idSubKontrak'], $_SESSION['strAdminID']);
			$checkAvailableCC = $this->Mpurchaseorder->checkAvailableCostControl($this->input->post('subKontrakID'));
			if(empty($checkAvailableCC[0]['name'])){
				$updateStatusCC = $this->Mpurchaseorder->udpateStatusCC($intPurchaseID);
			}
			//yang lama -> $intPurchaseID = $this->Mpurchaseorder->add($arrPurchase['intSupplier'],$arrPurchase['intWarehouse'],$arrPurchase['strDescription'],$arrPurchase['intTax'],0,$arrPurchase['strDate'],$arrPurchase['strExpDate'],$arrPurchase['intDisc']);
			$no_pb_baru = generateTransactionCode($this->input->post('txtDateBuat'),'','purchase_order',TRUE);
			$this->Mpurchaseorder->updateKodePurchaseOrder($no_pb_baru, $intPurchaseID);
			$totalItem = $this->input->post('totalItem');
			for($i = 0; $i < $totalItem+1; $i++) {
				$title = $this->input->post('isiBahanText'.$i);//bahan
				$terpb_now = $this->input->post('isiTerPB'.$i);
				$id = $this->input->post('idProduct'.$i);//id product
				$qty = $this->input->post('inputQty'.$i);//yang di input dan masukkan ke ter PB juga di skm material
				$qtyTerima = $this->input->post('inputQtyTerima'.$i);
				$satuan = $this->input->post('isiSatuanText'.$i);//satuan dari PB
				$keterangan = $this->input->post('isiKeterangan'.$i);// keterangan
				$cdate = $this->input->post('txtDateBuat');//cdate
				$subkontrakID = $this->input->post('subKontrakID');//ganti int puch id
				$subKonPB = $this->input->post('isiSubKon'.$i);
				$noPB = $this->input->post('txtNoPB');
				if($qtyTerima == '' || $qtyTerima == null){
					$qtyTerima = 0;
				}
				if(!empty($id)){
					$this->Mpurchaseorderitem->addToPurchaseOrderItem($intPurchaseID, $id, $keterangan, $qty, $cdate, $qtyTerima);
					$jumlah_terima = $this->Mpurchaseorderitem->recalculateTerPb($subKonPB,$id);
					if($subKonPB != 0){
						$this->Mpurchaseorderitem->updateTerPB($jumlah_terima[0]['terpb_now'], $subKonPB, $id);
					}
				}
			}
			//Manggil nama kontrak
			$kontrak = $this->Mkontrak->getItemByID($this->input->post('kontrakID'));
			//Manggil nama subkontrak
			$subkontrak = $this->Msubkontrak->getSubkontrak($this->input->post('subKontrakID'));
			
			$id_subkon = $this->input->post('subKontrakID');

			//Code untuk kasih notif
			if(empty($checkAvailableCC[0]['name'])){
				$pm = $this->Mpurchaseorder->getAllPMBySubKontrak($id_subkon);
				foreach($pm as $data_pm){
					$data_notif_pm = array(
						"noti_user_id" => $data_pm['name'],
						"noti_title" => "$no_pb_baru telah dibuat.",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_pm);
				}
			}else{
				$cc = $this->Mpurchaseorder->getAllCCBySubKontrak($id_subkon);
				foreach($cc as $data_cc){
					$data_notif_cc = array(
						"noti_user_id" => $data_cc['name'],
						"noti_title" => "$no_pb_baru telah dibuat.",						
						"noti_status" => 2
					);
					$this->Mnotification->add($data_notif_cc);
				}
			}

			$this->_CI->db->trans_complete();
			$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseordermade'));
			redirect('purchase_order/view/'.$intPurchaseID);
		}

		//$this->Mpurchaserder->getAllNamaProyek

		if($_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 2 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 1){
			$this->load->model('Mpurchaseorder');
			$arrKontrak = $this->Mpurchaseorder->getAllKontrak();
		}
		else{
			$this->load->model('Mpurchaseorder');
			$arrKontrak = $this->Mpurchaseorder->getAllKontrak($_SESSION['strAdminID']);
		}

		$this->load->model('Mpurchaseorder');
		$arrProduct = $this->Mpurchaseorder->getAllProduct();
	
		// Load the views
		$this->load->view('sia',array(
			'strViewFile' => 'purchaseorder/add',
			'arrKontrak' => $arrKontrak,
			'arrProduct' => $arrProduct,
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addpurchaseorder')
		));
	}
	
	// To display, edit and delete purchase
	public function view($intID = 0) {
		// checkTeamPermission('Purchase_order',$intID);

		$statuscc = $this->input->post('status_cc');
		$statuspm = $this->input->post('status_pm');
		$statusnow = $this->input->post('status_pb');

		if($this->input->post('backPurchase') != ''){
			redirect('purchase_order/browse');
		}

		if($this->input->post('closePB') != ''){
			$prorCode = $this->input->post('prorCode');
			$description = "Yang close adalah ".$_SESSION['strAdminPriviledge'];
			$this->Mpurchaseorder->closePB($intID, $description);
			$this->session->set_flashdata('strMessage',''.$prorCode.' telah ditutup');
			redirect('purchase_order/browse');
		}
		
		if($this->input->post('approvePB') != ''){
			$purchaseID = $this->input->post('po_id');
			$ganti = 2;
			$this->Mpurchaseorder->editStatusByID($purchaseID, $ganti);
			$this->session->set_flashdata('strMessage','Updated status PB');
			redirect('purchase_order/browse');
		}

		if($this->input->post('disapprovePB') != ''){
			if($statuscc == -1 && $statuspm == 3){
				$this->session->set_flashdata('strMessage','PB sudah di approve oleh PM untuk proyek '.$kontrak_name);
				redirect('purchase_order/browse');
			}
			if($statuscc == 2 ){
				$purchaseID = $this->input->post('po_id');
				$ganti = 0;
				$this->Mpurchaseorder->editStatusByID($purchaseID, $ganti);
				$this->session->set_flashdata('strMessage','Updated status PB');
				redirect('purchase_order/browse');
			}
			else{
				$this->session->set_flashdata('strMessage','PB sudah di approve oleh CC untuk proyek '.$kontrak_name);
				redirect('purchase_order/browse');
			}
		}

		if($this->input->post('ccEdit') != ''){
			if($statusnow == 2){
				$this->_CI->db->trans_start();
				$prorCode = $this->input->post('prorCode');
				$kontrak_name = $this->input->post('kontrak_name');
				echo "<script>console.log($prorCode)</script>";
				$this->Mpurchaseorder->editCCStatus($prorCode);
				$this->session->set_flashdata('strMessage','Updated status cost control');

				$_SESSION['flash_data'] = $this->session->set_flashdata('strMessage','Menunggu persetujuan PM, PB dengan nomor '.$prorCode.' ');
				echo "<script>console.log(".$_SESSION['flash_data'].");</script>";
				$idpo = $this->input->post('po_id');
				$date = date('Y/m/d H:i:s');
				$this->load->model('Mpurchaseorder');
				$this->Mpurchaseorder->updateJamApprovalCC($idpo, $date);
				$this->Mpurchaseorder->updateOrangApprovalCC($idpo, $_SESSION['strAdminID']);
				$id_subkon = (int) $this->input->post('idsubkon');
				//Kirim notif untuk PM dan SM
				$pm = $this->Mpurchaseorder->getAllPMBySubKontrak($id_subkon);
				foreach($pm as $data_pm){
					$data_notif_pm = array(
						"noti_user_id" => $data_pm['name'],
						"noti_title" => "$prorCode telah disetujui oleh CC untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_pm);
				}
				$sm = $this->Mpurchaseorder->getAllSMBySubKontrak($id_subkon);
				foreach($sm as $data_sm){
					$data_notif_sm = array(
						"noti_user_id" => $data_sm['name'],
						"noti_title" => "$prorCode telah disetujui oleh CC untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_sm);
				}
				$this->_CI->db->trans_complete();
				redirect('purchase_order/browse');
			}
			else{
				$_SESSION['flash_data'] = $this->session->set_flashdata('strMessage','PB belum di approve');
				redirect('purchase_order/browse');
			}
		}

		if($this->input->post('pmEdit') != ''){
			if($statuscc == 3 || $statuscc == -1 && $statusnow == 2){
				$this->_CI->db->trans_start();
				$prorCode = $this->input->post('prorCode');
				$kontrak_name = $this->input->post('kontrak_name');
				echo "<script>console.log($prorCode)</script>";
				$this->Mpurchaseorder->editPMStatus($prorCode);
				$this->Mpurchaseorder->editProrStatus($prorCode);
				$this->session->set_flashdata('strMessage','Updated status PM');
				$_SESSION['flash_data'] = '';
				//Bawa untuk update terpb ke subkontrak_material
				//$this->Mpurchaseorderitem->updateTerPB($qty, $subKonPB, $id, $terpb_now);
				$jumlahPB = $this->input->post('jumlah_row3');
				for($i = 0; $i<$jumlahPB; $i++){
					$id = $this->input->post('idProduct'.$i);
					$proid = $this->input->post('idProiID'.$i);
					$qty = $this->input->post('jumlahInputan'.$proid);
					$terpb_now = $this->input->post('isiTerPB'.$i);
					$subKonPB = $this->input->post('isiSubKon'.$i);
					if(!empty($terpb_now)){
						// echo "Masuk sini".$proid." ".$qty." ".$terpb_now." ".$subKonPB." ".$id;
						// die();
						$this->load->model('Mpurchaseorderitem');
						$this->Mpurchaseorderitem->updateTerPB($qty, $subKonPB, $id, $terpb_now);
					}
				}
				$idpo = $this->input->post('po_id');
				$date = date('Y/m/d H:i:s');
				$this->load->model('Mpurchaseorder');
				$this->Mpurchaseorder->updateJamApprovalPM($idpo, $date);
				$this->Mpurchaseorder->updateOrangApprovalPM($idpo, $_SESSION['strAdminID']);
				$id_subkon = $this->input->post('idsubkon');
				//Dibawah untuk kirim notif ke SM
				$sm = $this->Mpurchaseorder->getAllSMBySubKontrak($id_subkon);
				foreach($sm as $data_sm){
					$data_notif_sm = array(
						"noti_user_id" => $data_sm['name'],
						"noti_title" => "$prorCode telah disetujui oleh PM untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_sm);
				}
				//Dibawah untuk kirim notif ke CC
				$cc = $this->Mpurchaseorder->getAllCCBySubKontrak($id_subkon);
				foreach($cc as $data_cc){
					$data_notif_cc = array(
						"noti_user_id" => $data_cc['name'],
						"noti_title" => "$prorCode telah disetujui oleh PM untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_cc);
				}
				//DIbawah untuk notifk ke Purchasing
				$purchasing = $this->Mpurchaseorder->getAllPurchasingBySubKontrak($id_subkon);
				foreach($purchasing as $data_purchasing){
					$data_notif_purchasing = array(
						"noti_user_id" => $data_purchasing['name'],
						"noti_title" => "$prorCode telah disetujui oleh PM untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_purchasing);
				}
				$this->_CI->db->trans_complete();
				redirect('purchase_order/browse');
			}
			else{
				if($statuscc == 2){
					$this->session->set_flashdata('strMessage','CC belum di approve');
					redirect('purchase_order/browse');
				}
				else{
					$this->session->set_flashdata('strMessage','PB belum di approve');
					redirect('purchase_order/browse');
				}
			}
		}

		if($this->input->post('disccEdit') != ''){
			// if($statuspm == 0){
			// 	$prorCode = $this->input->post('prorCode');
			// 	echo "<script>console.log($prorCode)</script>";
			// 	$this->Mpurchaseorder->uneditCCStatus($prorCode);
			// 	$this->Mpurchaseorder->editProrStatus2($prorCode);
			// 	$this->session->set_flashdata('strMessage','Updated status cost control');
			// 	redirect('purchase_order/browse');
			// }
			// else{
			// 	$this->session->set_flashdata('strMessage','PB sudah di approve oleh PM');
			// 	redirect('purchase_order/browse');
			// }
			$this->_CI->db->trans_start();
			$prorCode = $this->input->post('prorCode');
			$kontrak_name = $this->input->post('kontrak_name');
			$this->Mpurchaseorder->editProrStatus2($prorCode);
			$idpo = $this->input->post('po_id');
			$date = date('Y/m/d H:i:s');
			$this->load->model('Mpurchaseorder');
			$this->Mpurchaseorder->updateJamApprovalCC($idpo, $date);
			$this->Mpurchaseorder->updateOrangApprovalCC($idpo, $_SESSION['strAdminID']);
			$id_subkon = $this->input->post('idsubkon');
			//Notif untuk decline ke PM
			$pm = $this->Mpurchaseorder->getAllPMBySubKontrak($id_subkon);
			foreach($pm as $data_pm){
				$data_notif_pm = array(
					"noti_user_id" => $data_pm['name'],
					"noti_title" => "$prorCode telah ditolak oleh CC untuk proyek $kontrak_name",					
					"noti_status" => 2
				);	
				$this->Mnotification->add($data_notif_pm);
			}
			//Motif decline untuk ke CC
			$cc = $this->Mpurchaseorder->getAllCCBySubKontrak($id_subkon);
			foreach($cc as $data_cc){
				$data_notif_cc = array(
					"noti_user_id" => $data_cc['name'],
					"noti_title" => "$prorCode telah ditolak oleh CC untuk proyek $kontrak_name",					
					"noti_status" => 2
				);	
				$this->Mnotification->add($data_notif_cc);
			}
			$this->_CI->db->trans_complete();

			$this->session->set_flashdata('strMessage','PB telah di decline oleh CC');
			redirect('purchase_order/browse');
		}

		if($this->input->post('dispmEdit') != ''){
			$this->_CI->db->trans_start();
			$prorCode = $this->input->post('prorCode');
			$kontrak_name = $this->input->post('kontrak_name');
			$idnyapo = $this->input->post('po_id');
			echo "<script>console.log($prorCode)</script>";
			$hasil_return = $this->Mpurchaseorderitem->checkStatusPO($idnyapo);
			$id = $hasil_return[0]['id'];
			// if($id>0){
			// 	$this->session->set_flashdata('strMessage','PB sudah ada di OP');
			// 	redirect('purchase_order/browse');
			// }
			// else{
			//Bawah untuk update orang yang melakukan decline

			$id_subkon = $this->input->post('idsubkon');

			$idpo = $this->input->post('po_id');
			$date = date('Y/m/d H:i:s');
			$this->load->model('Mpurchaseorder');
			$this->Mpurchaseorder->updateJamApprovalPM($idpo, $date);
			$this->Mpurchaseorder->updateOrangApprovalPM($idpo, $_SESSION['strAdminID']);
			//Bawah ini update
			$this->Mpurchaseorder->uneditPMStatus($prorCode);
			$this->Mpurchaseorder->editProrStatus2($prorCode);
			//Bawah untuk CC
			$cc = $this->Mpurchaseorder->getAllCCBySubKontrak($id_subkon);
			foreach($cc as $data_cc){
				if(!empty($data_cc['name'])){
					$data_notif_cc = array(
						"noti_user_id" => $data_cc['name'],
						"noti_title" => "$prorCode telah ditolak oleh PM untuk proyek $kontrak_name",						
						"noti_status" => 2
					);	
					$this->Mnotification->add($data_notif_cc);
				}
			}
			//Bawah untuk SM
			$sm = $this->Mpurchaseorder->getAllSMBySubKontrak($id_subkon);
			foreach($sm as $data_sm){
				$data_notif_cc = array(
					"noti_user_id" => $data_sm['name'],
					"noti_title" => "$prorCode telah ditolak oleh PM untuk proyek $kontrak_name",					
					"noti_status" => 2
				);	
				$this->Mnotification->add($data_notif_cc);
			}
			$this->_CI->db->trans_complete();
			$this->session->set_flashdata('strMessage','PB telah di decline oleh PM');
			redirect('purchase_order/browse');
			//}
		}
	
		$this->load->model('Mpurchase');

	if($this->input->post('subSave') != '' && $intID != '') { # SAVE

		// Purchase
		$arrPurchaseData = $this->Mpurchaseorder->getItemByID($intID);
		if(compareData($arrPurchaseData['pror_status'],array(0))) {
			$this->_CI->db->trans_start();
			$this->Mpurchaseorder->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('dsc4'),$this->input->post('selStatus'),$this->input->post('txtTax'),$this->input->post('selEditable'));
			// Load purchase item
			$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetPurchasedItemsIDByID($intID);
			$arrPostQty1 = $this->input->post('txtItem1Qty');
			$arrPostQty2 = $this->input->post('txtItem2Qty');
			$arrPostQty3 = $this->input->post('txtItem3Qty');
			$arrPostPrice = $this->input->post('txtItemPrice');
			$arrPostDisc1 = $this->input->post('txtItem1Disc');
			$arrPostDisc2 = $this->input->post('txtItem2Disc');
			$arrPostDisc3 = $this->input->post('txtItem3Disc');

			$intTotalPrice = 0;
			$arrContainID = $this->input->post('idProiID');
			foreach($arrPurchaseItem as $e) {
				if(!empty($arrContainID)) {
					if (in_array($e['id'], $arrContainID)) {
						$this->Mpurchaseorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);

					} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
				} else $this->Mpurchaseorderitem->deleteByID($e['id']);
				
			}
			$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetPurchasedBonusItemsIDByID($intID);
			$arrPostQty1 = $this->input->post('txtItemBonus1Qty');
			$arrPostQty2 = $this->input->post('txtItemBonus2Qty');
			$arrPostQty3 = $this->input->post('txtItemBonus3Qty');

			$arrContainID = $this->input->post('idProiIDBonus');
			if(!empty($arrPurchaseItem)) foreach($arrPurchaseItem as $e) {
					
				if(!empty($arrContainID)) {
					if(in_array($e['id'], $arrContainID)) {
						$this->Mpurchaseorderitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
					} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
				} else $this->Mpurchaseorderitem->deleteByID($e['id']);
					
			}
			$totalItem=$this->input->post('totalItem');
			for($i = 0; $i < $totalItem; $i++) {
				$qty1Item = $this->input->post('qty1PriceEffect'.$i);
				$qty2Item = $this->input->post('qty2PriceEffect'.$i);
				$qty3Item = $this->input->post('qty3PriceEffect'.$i);
				$unit1Item = $this->input->post('sel1UnitID'.$i);
				$unit2Item = $this->input->post('sel2UnitID'.$i);
				$unit3Item = $this->input->post('sel3UnitID'.$i);
				$prcItem = $this->input->post('prcPriceEffect'.$i);
				$dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
				$dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
				$dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
				$prodItem = $this->input->post('prodItem'.$i);
				$probItem = $this->input->post('probItem'.$i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItem' . $i);
				if($strProductDescription != '' || !empty($strProductDescription)) {
					$this->Mpurchaseorderitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
				}
			}
			$totalItemBonus = $this->input->post('totalItemBonus');
			for($i = 0; $i < $totalItemBonus; $i++) {
				$qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
				$qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
				$qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
				$unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
				$unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
				$unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
				$prodItemBonus = $this->input->post('prodItemBonus'.$i);
				$probItemBonus = $this->input->post('probItemBonus'.$i);
				$strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
				$idItemBonus = $this->input->post('idItemBonus'.$i);
				if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
					$this->Mpurchaseorderitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1);
				}
			}

		} else $this->Mpurchaseorder->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));
		$this->_CI->db->trans_complete();
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderupdated');

	} else if($this->input->post('subDelete') != '' && $intID != '') {
		/* must use this, because item's trigger can't be activated in header trigger */
		// $this->Mpurchaseorderitem->deleteByPurchaseID($intID);
		$this->_CI->db->trans_start();
		$this->Mpurchaseorder->deleteByID($intID);

		$totalItem = $this->input->post('totalItemAwal');
		for($i=0;$i<$totalItem;$i++){
			$idsubkon = $this->input->post('isiSubKon'.$i);
			$idproduct = $this->input->post('idProduct'.$i);
			$jumlah_terima = $this->Mpurchaseorderitem->recalculateTerPb($idsubkon,$idproduct);
			$this->Mpurchaseorderitem->updateTerPB($jumlah_terima[0]['terpb_now'], $idsubkon, $idproduct);
		}
		$this->_CI->db->trans_complete();
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderdeleted'));
		redirect('purchase_order/browse');

	}
	$arrPurchaseData = $this->Mpurchaseorder->getItemByID($intID);
	$bolEditable = $this->Mpurchaseorder->checkEditablePB($_SESSION['strAdminID'],$arrPurchaseData['idSubKontrak']);
	if(!empty($arrPurchaseData)) {
		$arrPurchased = $this->Mpurchase->Purchase_Order_GetSJByPOID($intID);
		$arrPurchaseData['pror_rawstatus'] = $arrPurchaseData['pror_status'];
		$arrPurchaseData['pror_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrPurchaseData['pror_status']);

		// Load the purchase item
		// punya lama -> $arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetItemsByID($intID);
		$this->load->model('Mpurchaseorderitem');
		$arrPurchaseItem = $this->Mpurchaseorderitem->Purchase_Order_GetItemsByIDBaru($intID);
		$jumlah_row = count($arrPurchaseItem);
		//echo "<script>alert('".$arrPurchaseItem[0]['id']." ini id');</script>";
		$intPurchaseTotal = 0;
		// $this->load->model('Munit');
		// for($i = 0; $i < count($arrPurchaseItem); $i++) {
		// 	//$arrPurchaseItem[$i]['strName'] = $arrPurchaseItem[$i]['proi_description'].'|'.$arrPurchaseItem[$i]['proc_title'];
		// 	$arrPurchaseItem[$i]['strName'] = $arrPurchaseItem[$i]['proi_description'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit1']);
		// 	$intConv1 = $convTemp[0]['unit_conversion'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit2']);
		// 	$intConv2 = $convTemp[0]['unit_conversion'];
		// 	$convTemp = $this->Munit->getConversion($arrPurchaseItem[$i]['proi_unit3']);
		// 	$intConv3 = $convTemp[0]['unit_conversion'];
		// 	$arrPurchaseItem[$i]['proi_conv1'] = $intConv1;
		// 	$arrPurchaseItem[$i]['proi_conv2'] = $intConv2;
		// 	$arrPurchaseItem[$i]['proi_conv3'] = $intConv3;
        //     $intPurchaseTotal+=$arrPurchaseItem[$i]['proi_subtotal'];
		// }
		$arrPurchaseData['subtotal_discounted'] = (float) $intPurchaseTotal - (float) $arrPurchaseData['pror_discount'];
		$intPurchaseGrandTotal = (float)$arrPurchaseData['pror_grandtotal'];
        $arrPurchaseData['subtotal_taxed']=(float)$arrPurchaseData['pror_grandtotal']+((float)$arrPurchaseData['pror_grandtotal']*(float)$arrPurchaseData['pror_tax']/100);

		$arrPurchaseItemBonus = $this->Mpurchaseorderitem->Purchase_Order_GetBonusItemsByID($intID);
		if(!empty($arrPurchaseItemBonus)) {
			for($i = 0; $i < count($arrPurchaseItemBonus); $i++) {
				//$arrPurchaseItemBonus[$i]['strName']=$arrPurchaseItemBonus[$i]['proi_description'].'|'.$arrPurchaseItemBonus[$i]['proc_title'];
				$arrPurchaseItemBonus[$i]['strName'] = $arrPurchaseItemBonus[$i]['proi_description'];
			}
		}

	}

	$arrData = array(
		'intPurchaseID' => $intID,
		'intPurchaseTotal' => $intPurchaseTotal,
		'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
		'arrPurchaseItem' => $arrPurchaseItem,
		'jumlah_item' => count($arrPurchaseItem),
		'bolEditable' => $bolEditable,
		'arrPurchaseData' => $arrPurchaseData,
		'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
		'arrPurchased' => $arrPurchased,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
	);

	if($this->input->post('editPurchase') != ''){
		if($this->input->post('status_pb') == 1){
			$this->_CI->db->trans_start();
			$arrClose = $this->input->post('dataClose');
			if($arrClose != '' || $arrClose != NULL){
				$arrCloseData = explode(',', $arrClose);
				echo '<script language="javascript">';
				echo 'console.log('.$arrCloseData.')';
				echo '</script>';
				$this->Mpurchaseorder->updateCloseStatus($arrCloseData);
				redirect('purchase_order/view/'.$intID);
			}
			$jumlah_awal = $this->input->post('jumlah_row3');
			$jumlah_baru = $this->input->post('jumlah_baru');
			$jumlah_delete = $this->input->post('jumlah_delete');
			$po_id = $this->input->post('po_id');
			if($jumlah_delete == 0){
				if($jumlah_baru-$jumlah_awal > 0){
					for($i=0; $i<$jumlah_baru; $i++){
						$subKonPB = $this->input->post('isiSubKon'.$i);
						if($subKonPB != 0 && !empty($po_id)){//update jika barang sudah ada di PB
							$title = $this->input->post('isiBahanText'.$i);//bahan
							$terpb_now = $this->input->post('isiTerPB'.$i);
							$poi_id = $this->input->post('idProiID'.$i);
							$id = $this->input->post('idProduct'.$i);//id product
							$qty = $this->input->post('editQty'.$i);//yang di input dan masukkan ke ter PB juga di skm material
							$qtyTerima = $this->input->post('editQtyTerima'.$i);
							$satuan = $this->input->post('isiSatuanText'.$i);//satuan dari PB
							$keterangan = $this->input->post('editKeterangan'.$i);// keterangan
							$cdate = $this->input->post('todayDate');//cdate
							$subkontrakID = $this->input->post('subKontrakID');//ganti int puch id
							//$subKonPB = $this->input->post('isiSubKon'.$i);
							$noPB = $this->input->post('txtNoPB');
							$status = $this->input->post('status_tambah'.$i);
							$this->load->model('Mpurchaseorderitem');
							if(strcmp($status,"tidak") == 0){
								$this->Mpurchaseorderitem->updatePurchaseOrderItem($poi_id, $keterangan, $qty, $qtyTerima);
								if($subKonPB != 0 || $subKonPB != NULL){
									//$this->Mpurchaseorderitem->updateTerPB($qty, $subKonPB, $id, $terpb_now);
								}
							}
							else{
								if(!empty($po_id)){
									$this->Mpurchaseorderitem->addToPurchaseOrderItem($po_id, $id, $keterangan, $qty, $cdate, $qtyTerima);
									//$this->Mpurchaseorderitem->updateTerPB($qty, $subKonPB, $id, $terpb_now);
								}
								else{

								}
							}
						}
						else{//tambah barang bila tidak ada di PB tapi ditambahkan ke tabel
							if(!empty($po_id)){
								$title = $this->input->post('isiBahanText'.$i);//bahan
								$terpb_now = $this->input->post('isiTerPB'.$i);
								$id = $this->input->post('idProduct'.$i);//id product
								$qty = $this->input->post('editQty'.$i);//yang di input dan masukkan ke ter PB juga di skm material
								$qtyTerima = $this->input->post('editQtyTerima'.$i);
								$satuan = $this->input->post('isiSatuanText'.$i);//satuan dari PB
								$keterangan = $this->input->post('editKeterangan'.$i);// keterangan
								$cdate = date('Y/m/d');
								$subkontrakID = $this->input->post('subKontrakID');//ganti int puch id
								//$subKonPB = $this->input->post('isiSubKon'.$i);
								$noPB = $this->input->post('txtNoPB');
								if(!empty($id)){
									$this->load->model('Mpurchaseorderitem');
									$this->Mpurchaseorderitem->addToPurchaseOrderItem($po_id, $id, $keterangan, $qty, $cdate, $qtyTerima);
								}
							}
							else{
							}
							
						}
						$idsubkon = $this->input->post('idsubkon'.$i);
						$idproduct = $this->input->post('product_id_edit'.$i);
						$jumlah_terima = $this->Mpurchaseorderitem->recalculateTerPb($subKonPB,$id);
						$this->Mpurchaseorderitem->updateTerPB($jumlah_terima[0]['terpb_now'], $idsubkon, $idproduct);
					}
					$this->session->set_flashdata('strMessage','Berhasil melakukan penambahan data');
					redirect('purchase_order/view/'.$intID);
				}
				else{
					for($i=0; $i<$jumlah_awal; $i++){//bila update tanpa tambah tanda
						$title = $this->input->post('isiBahanText'.$i);//bahan
						$terpb_now = $this->input->post('isiTerPB'.$i);
						$poi_id = $this->input->post('idProiID'.$i);
						$id = $this->input->post('idProduct'.$i);//id product
						$qty = $this->input->post('editQty'.$i);//yang di input dan masukkan ke ter PB juga di skm material
						$qtyTerima = $this->input->post('editQtyTerima'.$i);
						$satuan = $this->input->post('isiSatuanText'.$i);//satuan dari PB
						$keterangan = $this->input->post('editKeterangan'.$i);// keterangan
						$cdate = $this->input->post('todayDate');//cdate
						$subkontrakID = $this->input->post('subKontrakID');//ganti int puch id
						$subKonPB = $this->input->post('isiSubKon'.$i);
						$noPB = $this->input->post('txtNoPB');
						// echo "HAHAHA".$qty," ".$keterangan." ".$poi_id;
						// die();
						$this->load->model('Mpurchaseorderitem');
						$this->Mpurchaseorderitem->updatePurchaseOrderItem($poi_id, $keterangan, $qty, $qtyTerima);
						if($subKonPB != 0 || $subKonPB != NULL){
							//$this->Mpurchaseorderitem->updateTerPB($qty, $subKonPB, $id, $terpb_now);
						}
						$idsubkon = $this->input->post('idsubkon'.$i);
						$idproduct = $this->input->post('product_id_edit'.$i);
						$jumlah_terima = $this->Mpurchaseorderitem->recalculateTerPb($subKonPB,$id);
						$this->Mpurchaseorderitem->updateTerPB($jumlah_terima[0]['terpb_now'], $idsubkon, $idproduct);
					}
					$this->session->set_flashdata('strMessage','Berhasil melakukan update');
					redirect('purchase_order/view/'.$intID);
				}
			}
			else{
				if($jumlah_delete == $jumlah_awal){
					$this->session->set_flashdata('strMessage','Data detail tidak boleh kosong');
					redirect('purchase_order/view/'.$intID);
				}
				else{
					$kumpulan_delete_id = $this->input->post('kumpulan_delete_id');
					$kumpulansId = explode(",", $kumpulan_delete_id);
					if(count($kumpulansId) > 1){//jika jumlah lebih dari satu
						for($i = 0; $i<count($kumpulansId); $i++){
							$id_po_item = $kumpulansId[$i];//id purchase order item
							$jumlah_inputan = $this->input->post('jumlahInputan'.$id_po_item.'');
							$product_id_delete = $this->input->post('idProduct2'.$id_po_item.'');
							$isi_subkontrak = $this->input->post('isiSubKon'.$i);
							//Ter PB di hapus dulu
							$terpb = $this->Mpurchaseorderitem->getTerPB($isi_subkontrak, $product_id_delete);
							if(empty($terpb)){
								$this->Mpurchaseorderitem->deleteByID($id_po_item);
							}
							else{
								$new_terpb = $terpb[0]['subkontrak_terpb']-$jumlah_inputan;
								$this->Mpurchaseorderitem->updateTerPBAfterDelete($new_terpb, $product_id_delete, $isi_subkontrak);
								$this->load->model('Mpurchaseorderitem');
								$this->Mpurchaseorderitem->deleteByID($id_po_item);
							}
							//Setelah itu update jumlah qty pada subkontrak_material
							//$this->Mpurchaseorderitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);
						}
					}
					else{
						$id_po_item = $kumpulansId[0];
						$jumlah_inputan = $this->input->post('jumlahInputan'.$id_po_item.'');
						$product_id_delete = $this->input->post('idProduct2'.$id_po_item.'');
						$isi_subkontrak = $this->input->post('idsubkon');

						$terpb = $this->Mpurchaseorderitem->getTerPB($isi_subkontrak, $product_id_delete);
						if(empty($terpb)){
							$this->Mpurchaseorderitem->deleteByID($id_po_item);
						}
						else{
							$new_terpb = $terpb[0]['subkontrak_terpb']-$jumlah_inputan;
							$this->Mpurchaseorderitem->updateTerPBAfterDelete($new_terpb, $product_id_delete, $isi_subkontrak);
							$this->load->model('Mpurchaseorderitem');
							$this->Mpurchaseorderitem->deleteByID($id_po_item);
							// echo $id_po_item." ".$jumlah_inputan." ".$product_id_delete." ".$isi_subkontrak;
							// die();
						}
						//Setelah itu update jumlah qty pada subkontrak_material
						//$this->Mpurchaseorderitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);
					}
					redirect('purchase_order/view/'.$intID);
				}
			}
			$this->_CI->db->trans_complete();
		}
	}

	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota

		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'purchaseorder',
		), $arrData));
	} else {
		# Load all other purchase data the user has ever made
		$arrPurchaseList = $this->Mpurchaseorder->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

		$this->load->view('sia',array_merge(array(
	        'strViewFile' => 'purchaseorder/view',
			'arrPurchaseList' => $arrPurchaseList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorderstatus')
	    ), $arrData, $this->admlinklist->getMenuPermission(21,$arrPurchaseData['pror_rawstatus'])));
	}
	
	}
	public function browse($intPage = 0) {
		if($this->input->post('subSearch') != '') {
			$noPB = $this->input->post('txtSearchNoPB');
			$tglProyek = $this->input->post('txtSearchTglProyek');			
			$arrPurchase = $this->Mpurchaseorder->searchBox($noPB, $tglProyek);
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('purchase_order/browse', NULL, FALSE).'">[Back]</a>';
			$this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";
		} else {
			$arrPagination['base_url'] = site_url("purchase_order/browse?pagination=true", NULL, FALSE);
			$arrPagination['total_rows'] = $this->Mpurchaseorder->getCount();
			$arrPagination['per_page'] = $this->config->item('jw_item_count');;
			$arrPagination['uri_segment'] = 3;
			$this->pagination->initialize($arrPagination);
			$strPage = $this->pagination->create_links();
	
			$strBrowseMode = '';
			if($this->input->get('page') != '') $intPage = $this->input->get('page');
			
			$arrPurchase = $this->Mpurchaseorder->getItemsBaru($intPage,$arrPagination['per_page']);
			$arrApprPO=array();
			array_push($arrApprPO, "");
			$this->load->model('Mpurchaseorderitem');
			//Untuk update status close
			for($i =0; $i<count($arrPurchase); $i++){
				$all_id = $arrPurchase[$i]['id'];
				$po_item = $this->Mpurchaseorderitem->getAllPurchaseOrderItem();
				
				// for($j = 0; $j<count($po_item); $j++){
				// 	if($po_item[$j]['subkontrak_terpb'] == $po_item[$j]['jumlah']){
				// 		$this->Mpurchaseorderitem->updateStatusClosed($po_item[$j]['id']);
				// 	}
				// 	if($po_item[$j]['no_pb'] == NULL){
				// 		$no_pb = '123';
				// 	}
				// 	else{
				// 		$no_pb = $po_item[$j]['no_pb'];
				// 	}
				// }
				if(!empty($all_id) || $all_id != NULL){
					$hasil_return = $this->Mpurchaseorderitem->checkStatusPO($all_id);
					$id = $hasil_return[0]['id'];
					//echo "<script>alert(".$id.");</script>";
					if($id>0){
						array_push($arrApprPO,"ada");
					}
					else{
						array_push($arrApprPO,"tidak");
					}
				}
			}
			//punya yang lama -> $arrPurchase = $this->Mpurchaseorder->getItems($intPage,$arrPagination['per_page']);
		}
	
		if(!empty($arrPurchase)) for($i = 0; $i < count($arrPurchase); $i++) {
			
			if($arrPurchase[$i]['approvalCC'] == 3){//approve by cost control
				$arrPurchase[$i]['approvalCC'] = '<span class="glyphicon glyphicon-ok"></span>';
			}
			else if($arrPurchase[$i]['approvalCC'] == -1){
				$arrPurchase[$i]['approvalCC'] = "Proyek tidak membutuhkan cost control";
			}
			else if($arrPurchase[$i]['approvalCC'] == 1){
				$arrPurchase[$i]['approvalCC'] = "<span class='glyphicon glyphicon-remove'></span>";
			}
			else{
				$arrPurchase[$i]['approvalCC'] = "Belum ada";
			}
			if($arrPurchase[$i]['approvalPM'] == 3){//approve by pm
				$arrPurchase[$i]['approvalPM'] = '<span class="glyphicon glyphicon-ok"></span>';
			}
			else if($arrPurchase[$i]['approvalPM'] == 1){
				$arrPurchase[$i]['approvalPM'] = "<span class='glyphicon glyphicon-remove'></span>";
			}
			else{
				$arrPurchase[$i]['approvalPM'] = "Belum ada";
			}
			$arrPurchase[$i] = array_merge($arrPurchase[$i],$this->admlinklist->getMenuPermission(21,$arrPurchase[$i]['pror_status']));
			// $arrPurchase[$i]['pror_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrPurchase[$i]['pror_status']);
			// $arrPurchase[$i]['pror_total'] = (float) $arrPurchase[$i]['pror_grandtotal']+((float) $arrPurchase[$i]['pror_grandtotal']*(float) $arrPurchase[$i]['pror_tax']/100);
		}
	
		$this->load->model('Mtinydbvo');
		$this->Mtinydbvo->initialize('invoice_status');
	
		$this->load->view('sia',array(
			'strViewFile' => 'purchaseorder/browse',
			'strPage' => $strPage,
			'strBrowseMode' => $strBrowseMode,
			'arrPurchase' => $arrPurchase,
			'arrAccPO' => $arrApprPO,
			'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
			'strSearchKey' => $this->input->post('txtSearchValue'),
			'strSearchDate' => $this->input->post('txtSearchDate'),
			'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
			'intSearchStatus' => $this->input->post('txtSearchStatus'),
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchaseorder')
		));
	}
	
	}

/* End of File */