<?php
/*
PUBLIC FUNCTION:
- index()

PRIVATE FUNCTION:
- __construct()
*/

class Main extends JW_Controller {

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect('adminpage');

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
}

public function index() {
    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('jwdefaultconfig');
    $intAdjustmentExp = $this->Mtinydbvo->getSingleData('adjustment_exp');
    $intPaymentExp = $this->Mtinydbvo->getSingleData('payment_exp');

    // Status adjustment selesai
    $this->load->model('Madjustment');
    $this->Madjustment->autoExpired($intAdjustmentExp);

    // Status payment selesai
    $this->load->model('Mpayment');
    $this->Mpayment->autoExpired($intPaymentExp);

    // Status repayment selesai
    $this->load->model('Mrepayment');
    $this->Mrepayment->autoExpired($intPaymentExp);

    // Status account adjustment selesai
    $this->load->model('Maccountadjustment');
    $this->Maccountadjustment->autoExpired($intAdjustmentExp);

    redirect('adminpage/main');
}

public function backup_database() {

    // Load the DB utility class
    $this->load->dbutil();

    // Backup your entire database and assign it to a variable
    $backup =& $this->dbutil->backup();

    // Load the file helper and write the file to your server
    // $this->load->helper('file');
    // write_file('/upload/backup/latest_backup.gz', $backup);

    // Load the download helper and send the file to your desktop
    $this->load->helper('download');
    force_download(url_title($this->config->item('jw_website_name')).'-'.date('Y_m_d').'_backup.gz', $backup); 
    
    // $this->session->set_flashdata('strMessage','Your backup download has been started');
    // redirect('adminpage');
}

public function migrate($version = '') {
    $this->load->library('migration');

    $bolSucceed = FALSE;
    if ($version != '') $bolSucceed = $this->migration->version($version);
    elseif ($version == 'latest') $bolSucceed = $this->migration->latest();
    else $bolSucceed = $this->migration->current();

    if($bolSucceed !== FALSE) echo 'Migration Succeed';
    else show_error($this->migration->error_string());
}

}

/* End of File */