<?php

class Permintaan_pembayaran extends JW_Controller {

private $_CI;

public function __construct() {
	parent::__construct();
	$this->_CI =& get_instance();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	//$this->load->model('Mdashboard');

	$this->_getMenuHelpContent(240,true,'adminpage');
	
	$this->load->model('Mperminbayar');
}

public function index($strMode = 'normal') {
    if($this->input->post("smtMakePerminBayar") != ''){        
        $kontrak_id = $this->input->post("kontrak_id");
        $subkontrak_id = $this->input->post("subkontrak_id");
        $owner_id = $this->input->post("owner_id");
        $kategori = $this->input->post("kategori_id");
        $txtDateBuat = $this->input->post("txtDateBuat");
        $pembuat = $this->session->userdata('strAdminID');
        $amount = str_replace(".","",$this->input->post("amount"));
        $date = date('Y/m/d H:i:s');
        $this->_CI->db->trans_start();
        $new_id = $this->Mperminbayar->add($kontrak_id, $subkontrak_id, $owner_id, $kategori, $txtDateBuat, $pembuat, $amount);

        $no_perminbayar_baru = generateTransactionCode($this->input->post('txtDateBuat'),'','permintaan_pembayaran',TRUE);
        $this->Mperminbayar->updatePerminBayarCode($no_perminbayar_baru, $new_id);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Permintaan pembayaran telah dibuat');
        redirect('permintaan_pembayaran/browse');
        
    }elseif ($this->input->post("updatePerbayar") != '') {

        $id = $this->input->post('perbayar_id');
        $kontrak_id = $this->input->post("kontrak_id");
        $subkontrak_id = $this->input->post("subkontrak_id");
        $owner_id = $this->input->post("owner_id");
        $kategori = $this->input->post("kategori_id");        
        $pembuat = $this->session->userdata('strAdminID');
        $amount = str_replace(".","",$this->input->post("amount"));
        
        $arrPerminBayar = array(
            'perbayar_ownerid' => $owner_id,
            'perbayar_kontrakid' => $kontrak_id,
            'perbayar_subkontrakid' => $subkontrak_id,
            'perbayar_amount' => $amount,
            'perbayar_kategori' => $kategori,
        );
        
        $intUpdate = $this->Mperminbayar->update($arrPerminBayar, $id);

        if($intUpdate){            
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-updatedone');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/view/'.$id);
        }else{
            $this->session->set_flashdata('strMessage', loadLanguage('adminmessage',$this->session->userdata('jw_language'), 'paymentrequest-updatefailed'));
            redirect('permintaan_pembayaran/view/'.$id);
        }
        
    }elseif ($this->input->post("deletePerbayar") != '') {
        $id = $this->input->post('perbayar_id');
        $intDelete = $this->Mperminbayar->delete($id);

        if($intDelete){
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-deletedone');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/browse');
        }else{
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-deletedone');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/browse');
        }
    }elseif($this->input->post("approvePerbayar") != ''){
        
        $id = $this->input->post('perbayar_id');
        $intApproval = $this->Mperminbayar->approve($id);

        if($intApproval){
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-approvaldone');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/view/'.$id);
        }else{
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-approvalfailed');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/view/'.$id);
        }

    }elseif($this->input->post("disapprovePerbayar") != ''){

        $id = $this->input->post('perbayar_id');
        $intDisapproval = $this->Mperminbayar->disapprove($id);

        if($intDisapproval){
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-disapprovaldone');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/view/'.$id);
        }else{
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'paymentrequest-disapprovalfailed');
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
            redirect('permintaan_pembayaran/view/'.$id);
        }
    }

	$arrOwner = $this->Mperminbayar->getAllOwner();

	$this->load->view('sia',array(
		'strViewFile' => 'permintaan_pembayaran/add',
		'arrOwner' => $arrOwner,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-addinvoice')
    ));
}

public function view($int){

    $arrPerminBayar = $this->Mperminbayar->getAllPerminBayarById($int);
    
    $arrOwner = $this->Mperminbayar->getAllOwner();
    $arrProyek = $this->Mperminbayar->getNamaProyekByOwner($arrPerminBayar['perbayar_ownerid'], 6);
    $arrPekerjaan = $this->Mperminbayar->getNamaPekerjaan($arrPerminBayar['perbayar_kontrakid']);    
    $arrKategori = $this->Mperminbayar->getNamaKategori($arrPerminBayar['perbayar_subkontrakid']);

    if(!empty($arrPerminBayar)){
        $arrBtnAllow = $this->admlinklist->getMenuPermission(240,0);
    }

    $strStatus = '';
    switch ($arrPerminBayar['approvalMgr']) {
        case '1':
            $strStatus = "Ditolak";
            break;
        case '3':
            $strStatus = "Diterima";
        break;                
    }

    $arrData = array(
        'arrBtnAllow' => $arrBtnAllow,
        'arrPerminBayar' => $arrPerminBayar,
        'arrOwner' => $arrOwner,
        'arrProyek' => $arrProyek,
        'arrPekerjaan' => $arrPekerjaan,
    );

    $this->load->view('sia', array_merge(array(
        'strViewFile' => 'permintaan_pembayaran/view',        
        'strStatus' => $strStatus,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'paymentrequest-paymentrequest'),
        'strPageTitle' => "Permintaan Pembayaran"
    ),$arrData,$this->admlinklist->getMenuPermission(240,0)) );
}

public function browse($intPage = 0){

	if($this->input->post('subSearch') != '') {
        $arrPerminBayar = $this->Mperminbayar->searchBy($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('invoice/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPerminBayar) ? count($arrPerminBayar) : '0')." records).";

    } else {
        $arrPagination['base_url'] = site_url("permintaan_pembayaran/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mperminbayar->getCount();
        $arrPagination['per_page'] = 10;
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrPerminBayar = $this->Mperminbayar->getAllPerminBayar($intPage,$arrPagination['per_page']);
    }

    $arrPPStatus = array(
        array('strKey' => 1,'strData' => "Ditolak"),
        array('strKey' => 2,'strData' => "Butuh Approval"),
        array('strKey' => 3,'strData' => "Diterima"),
    );
    

    $this->load->view('sia',array(
        'strViewFile' => 'permintaan_pembayaran/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
        'arrPerminBayar' => $arrPerminBayar,
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'arrPPStatus' => $arrPPStatus,
        'strPageTitle' => "Permintaan Pembayaran"
    ));
}

}

?>