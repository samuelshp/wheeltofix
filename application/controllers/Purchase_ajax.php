<?php

class Purchase_ajax extends JW_Controller {

public function getSupplierAutoComplete($txtData='') {
    $this->load->model('Msupplier');
    ArrayToXml(array('Supplier' => $this->Msupplier->getAllSuppliers($txtData)));
}

public function getPurchaseItemBySupplier($idSupp = ''){
    $this->load->model('Mpurchase');
    ArrayToXml(array('Purchase' => $this->Mpurchase->getAllPurchaseItemBySupplier($idSupp)));
}

public function getPurchaseItemBySupplier2($nama_supp= ''){
    $this->load->model('Mpurchase');
    ArrayToXml(array('Purchase' => $this->Mpurchase->getAllPurchaseItemBySupplier2($nama_Supp)));
}

public function getProductAutoComplete($txtData='', $txtUnselect='', $txtFilter='Product') {
    # INIT
    $this->load->model('Mproduct');
    $arrProduct=$this->Mproduct->getAllItems($txtData, $txtUnselect, $txtFilter);
    for($i=0;$i<count($arrProduct);$i++){
        $arrProduct[$i]['strName']=formatProductName("",$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
        $arrProduct[$i]['strNameWithPrice']=formatProductName("",$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['prci_price'],$arrProduct[$i]['prod_currency']));
    }

    $arrData = array('Product' => $arrProduct);

    if($txtFilter == 'Category') {
        $this->load->model('Mproductcategory');
        $arrData['Category'] = $this->Mproductcategory->getAllItems($txtData);
    } elseif($txtFilter == 'Brand') {
        $this->load->model('Mproductbrand');
        $arrData['Brand'] = $this->Mproductbrand->getAllItems($txtData);
    }

    ArrayToXml($arrData);
}

public function getProductAutoCompleteBySupplier($intSupplierID,$txtData='',$intInternal=0) {
    # INIT
    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getAllItemsPerSupplier($intSupplierID,$txtData,$intInternal);
    if (!empty($arrProduct)) {
        for($i = 0; $i < count($arrProduct); $i++) {
            $arrProduct[$i]['strName'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
            if(!empty($arrProduct[$i]['prod_rak'])) $arrProduct[$i]['prod_description'] =
                '<p>Rak: '.$arrProduct[$i]['prod_rak'].'</p>'.
                $arrProduct[$i]['prod_description'];
            $arrProduct[$i]['strNameWithPrice'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['prci_price'],$arrProduct[$i]['prod_currency']));
        }

        ArrayToXml(array('Product' => $arrProduct));
    }
}

public function getProductAutoCompletePerSupplierID($intSupplierID,$intOutletType,$intOutletMarketType = '1',$txtData = '') {
    # INIT
    $this->load->model('Mproduct');
    $arrProduct = $this->Mproduct->getAllItemsPerSupplierID($intSupplierID,$intOutletType,$txtData);

    if (!empty($arrProduct)) {
        for($i = 0; $i < count($arrProduct); $i++) {
            $arrProduct[$i]['strName'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
            if(!empty($arrProduct[$i]['prod_rak'])) $arrProduct[$i]['prod_description'] =
                '<p>Rak: '.$arrProduct[$i]['prod_rak'].'</p>'.
                $arrProduct[$i]['prod_description'];
            if ($intOutletMarketType == '1') { /* Modern Market */
                $arrProduct[$i]['strNameWithPrice'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['prod_pricemodern'],$arrProduct[$i]['prod_currency']));
            } else if ($intOutletMarketType == '0' || $intOutletMarketType == '2') { /*Traditional Market*/
                $arrProduct[$i]['strNameWithPrice'] = formatProductName('',$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['prod_price'],$arrProduct[$i]['prod_currency']));
            }
        }

        ArrayToXml(array('Product' => $arrProduct));
    }
}

public function getPurchaseItemById($idPurchase){
    $this->load->model('Mpurchaseitem');
    ArrayToXml(array('PurchaseItem' => $this->Mpurchaseitem->getAllPurchaseItemById($idPurchase)));
}

public function getPurchaseOrderAutoComplete($txtData='') {
    $this->load->model('Mpurchaseorder');
    ArrayToXml(array('Supplier' => $this->Mpurchaseorder->getDataAutoComplete($txtData)));
}

public function getPurchaseAutoComplete($txtData='') {
    $this->load->model('Mpurchase');
    ArrayToXml(array('Supplier' => $this->Mpurchase->getDataAutoComplete($txtData)));
}

public function getPurchaseIDBySupplier($idSupp){
    $this->load->model('Mpurchase');
    ArrayToXml(array('Purchase' => $this->Mpurchase->getPurchaseBySupplier($idSupp)));
}

public function getDataForPurchase($intID = 0) {//
    $this->load->model('Mpurchaseorderitem');
    $arrItem = $this->Mpurchaseorderitem->getItemsByPurchaseID($intID);
    $this->load->model('Munit');
    $arrUnit = array(); $i = 0;
    foreach($arrItem as $e) {
        $arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['proi_product_id']);
        $i++;
    }

    ArrayToXml(array('orderItem' => $arrItem,'Unit' => $arrUnit));
}

public function getDataForAcceptance($intID = 0) {
    $this->load->model('Mpurchaseitem');
    $arrItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
    $this->load->model('Munit');
    $arrUnit = array(); $i = 0;
    foreach($arrItem as $e) {
        $arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['prci_product_id']);
        $i++;
    }

    ArrayToXml(array('orderItem' => $arrItem,'Unit' => $arrUnit));
}

public function getBonusDataForPurchase($intID = 0) {
    $this->load->model('Mpurchaseorderitem');
    $arrItem = $this->Mpurchaseorderitem->getBonusItemsByPurchaseID($intID);
    $this->load->model('Munit');
    $arrUnit = array(); $i = 0;
    foreach($arrItem as $e) {
        $arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['proi_product_id']);
        $i++;
    }
    ArrayToXml(array('orderItem'=>$arrItem,'Unit'=>$arrUnit));
}

public function getBonusDataForAcceptance($intID = 0) {
    $this->load->model('Mpurchaseitem');
    $arrItem = $this->Mpurchaseitem->getBonusItemsByPurchaseID($intID);
    $this->load->model('Munit');
    $arrUnit = array(); $i = 0;
    foreach($arrItem as $e) {
        $arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['prci_product_id']);
        $i++;
    }
    ArrayToXml(array('orderItem'=>$arrItem,'Unit'=>$arrUnit));
}

public function getLastProductPrice() {
    $this->load->model('Macceptanceitem');

    $arrItems = $this->Macceptanceitem->getLastProductPrice($this->input->get('product_id'));
    foreach($arrItems as $i => $e) {
        $arrItems[$i]['strDate'] = formatDate2($e['cdate'], 'd F Y');
        $arrItems[$i]['strPrice'] = setPrice($e['acit_price']);
    }

    echo json_encode(array(
        'Items' => $arrItems,
    ));
}

public function getPBAutoCompleteList($txtData='') {
    $this->load->model('Mpurchaseorder');
    return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mpurchaseorder->getDataByProjectID($txtData)));
}

public function getPBAutoCompleteDetail($intPBID) {
    $this->load->model('Mpurchaseorder');
    return $this->output->set_content_type('application/json')->set_output(json_encode(
        array('detail' =>$this->Mpurchaseorder->getDataAutoComplete($intPBID))
    ));
}

public function getDataPurchaseById($intPBID){
    $this->load->model('Mpurchaseorderitem');
    return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mpurchaseorderitem->Purchase_Order_GetItemsByIDBaru($intPBID)));
}

public function getKPAutoComplete($intSupplierID,$intPBID) {
    $this->load->model('Mkontrakpembelian');
    $this->load->model('Mdownpayment');
    $dp = $this->Mdownpayment->getDPbySuppID($intSupplierID);
    foreach ($dp as $key => $e) {
        $dp[$key]['dpay_amount'] = setPrice($e['dpay_amount']);
    }
    return $this->output->set_content_type('application/json')->set_output(json_encode(
        array(
            'kp' => $this->Mkontrakpembelian->getDataAutoComplete($intSupplierID),
            'dp' => $dp
        )
    ));
}

public function getAddCostAutoComplete($txtData='') {
    $this->load->model('Mproduct');
    ArrayToXml(array('Product' => $this->Mproduct->getAllItems($txtData)));
}

public function getSupplierContactInfo($intSupplierID)
{
    $this->load->model('Msupplier');
    return $this->output->set_content_type('application/json')->set_output(json_encode($this->Msupplier->getItemByID($intSupplierID)));
}

}