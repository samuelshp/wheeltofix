<?php 

class Pembelian_asset extends JW_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		$this->_getMenuHelpContent(253,true,'adminpage');
		$this->load->model('Masset');		
	}

	public function index()
	{		
		if($this->input->post('smtMakePembelianAsset') != '') {	
									
			//Insert Item Asset				
			$arrItem = array();		
			// $strCode = generateTransactionCode($this->input->post('txtDateBuat'),'','asset',TRUE);
			$key = 0;
			for ($i=1; $i <= $this->input->post('intAmountQty')[$key] ; $i++) {
				$arrItem['strCode'] = '';
				$arrItem['txtDateBuat'] = str_replace("/", "-", $this->input->post('txtDateBuat'));
				$arrItem['intSupplier'] = $this->input->post('intSupplier');
				$arrItem['intCOAasset'] = $this->input->post('intCOAasset');
				$arrItem['intCOAakumulasi'] = $this->input->post('intCOAakumulasi');
				$arrItem['intCOAbeban'] = $this->input->post('intCOAbeban');
				$arrItem['strBarang'] = $this->input->post('strNamaBarang')[$key];
				$arrItem['intAmounQty'] = $this->input->post('intAmountQty')[$key];
				$arrItem['strSatuan'] = $this->input->post('strSatuan')[$key];
				$arrItem['intAmount'] = str_replace(",", ".", str_replace(".","",$this->input->post('intAmount')[$key]));
				$arrItem['intAmountDefault'] = str_replace(",", ".", str_replace(".","",$this->input->post('intAmountDefault')[$key]));
				$arrItem['intAmountFinal'] = str_replace(",", ".", str_replace(".","",$this->input->post('intAmountFinal')[$key]));
				$arrItem['intAmountPenyusutanSisa'] = str_replace(",", ".", str_replace(".","",$this->input->post('intAmountPenyusutanSisa')[$key]));
				$arrItem['intLama'] = str_replace(",", ".", str_replace(".","",$this->input->post('intLama')[$key]));
				$arrItem['strKeterangan'] = $this->input->post('keterangan');
				$arrItem['intPPN'] = str_replace(",", ".", str_replace(".","",$this->input->post('ppn')));
				$arrItem['intgrandTotal'] = str_replace(",", ".", str_replace(".","",$this->input->post('grandTotal')));
				$intInsert = $this->Masset->add('pembelian',$arrItem);
			}			

			$this->session->set_flashdata('strMessage','Pembelian Aset berhasil dibuat');
			redirect("pembelian_asset/browse","refresh");	
		}

		if ($this->input->post('smtDeletePembelianAsset') != '') {	
			
			$itemTerbayar = [];

			if(!is_array($this->input->post('itemTerbayar'))) array_push($itemTerbayar, $this->input->post('itemTerbayar'));			
			$dph_id = $this->input->post('dph_id');
			$intDPHdelete = $this->Mdaftarpembayaranhutang->delete($dph_id);
			
			if($intDPHdelete) {
				$this->session->set_flashdata('strMessage','Daftar Pembayaran Hutang berhasil dihapus');        
				redirect("daftar_pembayaran_hutang/browse","refresh");
			}else{
				$this->session->set_flashdata('strMessage','Daftar Pembayaran Hutang gagal dihapus');        
				redirect("daftar_pembayaran_hutang/view/".$intDPHdelete,"refresh");
			}			
		}	

		if($this->input->post('subPrint') != '' || $this->input->get('print') != '' ) {			

			$id = ($this->input->post('dph_id') != '') ? $this->input->post('dph_id') : $this->input->get('print');
			$this->load->model('Mdaftarpembayaranhutangitem');
			$arrDaftarPembayaranHutang = $this->Mdaftarpembayaranhutang->getDataByID($id);
			$arrDaftarPembayaranHutangItem = $this->Mdaftarpembayaranhutangitem->getItemsByID($id);

			foreach ($arrDaftarPembayaranHutangItem as $key => $value) {					
				$strProduct = " ";

				if ($value['dphi_reff_type'] !== "DP") $products[$key] = $this->Mdaftarpembayaranhutang->getProductList($value['dphi_reff_id']);		
				if($value['dphi_reff_type'] !== "PI") $arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = "-";		
				else $arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = $value['dummy_pph'];

				if(!empty($products[$key])){
					foreach ($products[$key] as $value) {
						$strProduct .= $value['prod_title']."<br/>";
					}
				}else{
					$strProduct .= $value['dpay_description'];
				}

				$arrDaftarPembayaranHutangItem[$key]['product'] = $strProduct;
			}

			$this->load->model('Mdbvo','MAdminLogin');
		    $this->MAdminLogin->initialize('adm_login');

		    $this->MAdminLogin->dbSelect('','id =' . $arrDaftarPembayaranHutang['cby']);
		    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
			
			$this->load->view('sia_print',array(
	            'strPrintFile' => 'daftarpembayaranhutang',
	            'arrDaftarPembayaranHutang' => $arrDaftarPembayaranHutang,
				'arrDaftarPembayaranHutangItem' => $arrDaftarPembayaranHutangItem,
				'arrLogin' => $arrLogin
	        ));
		}else{
		//untuk ngambil data no pi,supplier,barang,amount
		$this->load->model('Msupplier');
		$this->load->model('Maccount');
		$arrSupplier = $this->Msupplier->getAllData();	
		$arrCOA = $this->Maccount->getAllAccount();			
			
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'pembelian_asset/add',
			'arrSupplier' => $arrSupplier,
			'arrCOA' => $arrCOA,				
			'strPageTitle' => "Pembelian Asset",
		),$this->admlinklist->getMenuPermission(253,1)));
		}         

	}

	public function browse($intPage = 0)
	{
		if($this->input->post('subSearch') != '') {		
			$strSearchDate = str_replace("/", "-", $this->input->post('txtSearchDate'));
			$strSearchDate2 = str_replace("/", "-", $this->input->post('txtSearchDate2'));
			$strSearchKey = $this->input->post('txtSearchNoDPH');		        
	        $arrPembelianAsset = $this->Masset->searchBy($strSearchDate, $strSearchDate2, $strSearchKey, $this->input->post('txtSearchNoSupplier'));		       	        
	        $strPage = '';
	        $strBrowseMode = '<a href="'.site_url('daftar_pembayaran_hutang/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDaftarPembayaranHutang) ? count($arrDaftarPembayaranHutang) : '0')." records).";		       
		}else{
			$arrPagination['base_url'] = site_url("daftar_pembayaran_hutang/browse?pagination=true", NULL, FALSE);
			// $arrPagination['total_rows'] = $this->Mdaftarpembayaranhutang->getCount();
			// $arrPagination['per_page'] = $this->config->item('jw_item_count');
			// $arrPagination['uri_segment'] = 3;
			// $this->pagination->initialize($arrPagination);
			$strPage = null; #$this->pagination->create_links();

			$strBrowseMode = '';
			if($this->input->get('page') != '') $intPage = $this->input->get('page');
			$arrPembelianAsset = $this->Masset->getAllData();
		}		

		$this->load->view('sia',array_merge(array(
			'arrPembelianAsset' => $arrPembelianAsset,
			'strViewFile' => 'pembelian_asset/browse',
			// 'strSearchKey' => $this->input->post('txtSearchValue'),
			// 'strSearchDate' => $this->input->post('txtSearchDate'),
			// 'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
			// 'intSearchStatus' => $this->input->post('txtSearchStatus'),
			'strPage' => $strPage,
			'strPageTitle' => "Pembelian Asset"
		),$this->admlinklist->getMenuPermission(253,1)));

	}

	public function view($id)
	{
		$this->load->model('Msupplier');		
		$this->load->model('Maccount');
		$this->load->model('Mdaftarpembayaranhutangitem');
		$arrDaftarPembayaranHutang = $this->Masset->getItemByID($id);		
		$arrDaftarPembayaranHutangItem = null; #$this->Mdaftarpembayaranhutangitem->getItemsByID($id);	
		$arrSupplier = $this->Msupplier->getAllData();
		$arrCOA = $this->Maccount->getAllAccount();

		$this->load->view('sia', array_merge(array(
			'arrDaftarPembayaranHutang' => $arrDaftarPembayaranHutang,
			'arrDaftarPembayaranHutangItem' => $arrDaftarPembayaranHutangItem,
			'arrSupplier' => $arrSupplier,
			'arrCOA' => $arrCOA,	
			'strViewFile' => 'pembelian_asset/view',
			'strPageTitle' => 'Detail Pembelian Aset'
		),$this->admlinklist->getMenuPermission(253,2)));
	}


	
}