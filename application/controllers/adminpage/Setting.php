<?php

class Setting extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect('administrator');
}

public function index() {
	$this->load->model('Madmlinklist');
    $arrAvailability = array(
        'store' => $this->Madmlinklist->getAvailability(91),
        'owner' => $this->Madmlinklist->getAvailability(92),
        'meta' => FALSE,
        'theme' => FALSE,
        'file' => FALSE,
    );
    
    $arrData = array(
		'arrAvailability' => $arrAvailability,
        'strPageTitle' => 'Setting'
	);
    
    if($arrAvailability['store']) $arrData = array_merge($arrData,$this->_store(),array('strStoreInstruction' => $this->_arrData['strMenuInstruction']));
    if($arrAvailability['owner']) $arrData = array_merge($arrData,$this->_owner(),array('strOwnerInstruction' => $this->_arrData['strMenuInstruction']));
    if($arrAvailability['meta']) $arrData = array_merge($arrData,$this->_meta(),array('strMetaInstruction' => $this->_arrData['strMenuInstruction']));
    if($arrAvailability['theme']) $arrData = array_merge($arrData,$this->_theme(),array('strThemeInstruction' => $this->_arrData['strMenuInstruction']));
    if($arrAvailability['file']) $arrData = array_merge($arrData,$this->_file(),array('strFileInstruction' => $this->_arrData['strMenuInstruction']));
    
    if($this->input->post('smtProcessType') == 'Save') $arrData['strMessage'] = $this->lang->jw('done_setting');
    
	$this->load->view('adminpage',array_merge(array(
		'strViewFile' => 'setting/index'
	),$arrData));

}

public function owner() { redirect('setting#owner'); }
public function store() { redirect('setting#store'); }
public function theme() { redirect('setting#theme'); }
public function meta() { redirect('setting#meta'); }

/* PRIVATE FUNCTIONS */

private function _owner() {
    $this->_getMenuHelpContent(91,true,'');

	
	$this->load->model('Mtinydbvo','setting');
	$this->setting->initialize('jwsettingowner');
    
    $intDataCount = $this->setting->getSingleData('JW_OWNER_DATACOUNT');
	
	if($this->input->post('smtProcessType') == 'Save') {
		$this->setting->setSingleData('JW_OWNER_NAME',$this->input->post('JW_OWNER_NAME'));
		$this->setting->setSingleData('JW_OWNER_EMAIL',$this->input->post('JW_OWNER_EMAIL'));
		$this->setting->setSingleData('JW_OWNER_PHONE',$this->input->post('JW_OWNER_PHONE'));
		$this->setting->setSingleData('JW_OWNER_ADDRESS',$this->input->post('JW_OWNER_ADDRESS'));
		for ($i=1; $i <= $intDataCount; $i++) if($this->input->post('JW_OWNER_DATA'.$i) !== FALSE)
			$this->setting->setSingleData('JW_OWNER_DATA'.$i,
				translateDataIntoSafeStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->input->post('JW_OWNER_DATA'.$i))
			);
		$this->setting->saveData();
		$strMessage = $this->lang->jw('done_setting');

	} else $strMessage = '';
    
	$arrData = array(
		'intDataCount' => $intDataCount,
		'strOwnerName' => $this->setting->getSingleData('JW_OWNER_NAME'),
		'strOwnerEmail' => $this->setting->getSingleData('JW_OWNER_EMAIL'),
		'strOwnerPhone' => $this->setting->getSingleData('JW_OWNER_PHONE'),
		'strOwnerAddress' => $this->setting->getSingleData('JW_OWNER_ADDRESS')
	);

	for ($i=1; $i <= $intDataCount; $i++) {
		$strDataLabel = $this->setting->getSingleData('JW_OWNER_DATA'.$i,2);
		$strDataType = $this->setting->getSingleData('JW_OWNER_DATA'.$i,3);
		if(!empty($strDataLabel)) {
			$arrData['strOwnerData'.$i] = translateDataIntoReadableStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->setting->getSingleData('JW_OWNER_DATA'.$i));
			$arrData['strOwnerData'.$i.'Label'] = $strDataLabel;
			$arrData['strOwnerData'.$i.'Type'] = $strDataType;
		}
	}

    return $arrData;
}

private function _store() {
    $this->_getMenuHelpContent(92,true,'');
	
	$this->load->model('Mtinydbvo','settingstore');
	$this->settingstore->initialize('jwsettingstore');
	
	$this->load->model('Mtinydbvo','defconf');
	$this->defconf->initialize('jwdefaultconfig');
	
	if($this->input->post('smtProcessType') == 'Save') {
		$this->settingstore->setSingleData('JW_WEBSITE_NAME',$this->input->post('JW_WEBSITE_NAME'));
		$this->settingstore->setSingleData('JW_WEBSITE_MOTTO',$this->input->post('JW_WEBSITE_MOTTO'));
		$strWebsiteLogo = translateDataIntoSafeStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->input->post('JW_WEBSITE_LOGO'));
		$this->settingstore->setSingleData('JW_WEBSITE_LOGO',$strWebsiteLogo);
		$strWebsiteIcon = translateDataIntoSafeStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->input->post('JW_WEBSITE_ICON'));
		$this->settingstore->setSingleData('JW_WEBSITE_ICON',$strWebsiteIcon);
		$this->settingstore->setSingleData('JW_PRODUCT_COUNT',$this->input->post('JW_PRODUCT_COUNT'));
		$this->settingstore->setSingleData('JW_POST_COUNT',$this->input->post('JW_POST_COUNT'));
		$this->settingstore->setSingleData('JW_HOME_COUNT',$this->input->post('JW_HOME_COUNT'));
		$this->settingstore->setSingleData('JW_SIDEBAR_COUNT',$this->input->post('JW_SIDEBAR_COUNT'));
		$this->settingstore->saveData();
		$this->defconf->setSingleData('JW_LANGUAGE',$this->input->post('JW_LANGUAGE'));
		$this->defconf->saveData();
		$strMessage = $this->lang->jw('done_setting');

	} else $strMessage;
    
    return array(
		'strWebsiteName' => $this->settingstore->getSingleData('JW_WEBSITE_NAME'),
		'strWebsiteMotto' => $this->settingstore->getSingleData('JW_WEBSITE_MOTTO'),
		'strWebsiteLogo' => translateDataIntoReadableStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->settingstore->getSingleData('JW_WEBSITE_LOGO')),
		'strWebsiteIcon' => translateDataIntoReadableStatements(array('field_type' => 'TEXT','validation' => 'URL'),$this->settingstore->getSingleData('JW_WEBSITE_ICON')),
		'strLanguage' => translateDataIntoHTMLStatements(array(
			'title' => 'JW_LANGUAGE',
			'field_type' => 'SELECT',
			'description' => 'language',
			'allow_null' => 0,
			'validation' => 'RADIO'),
			$this->defconf->getSingleData('JW_LANGUAGE')),
		'intProductCount' => $this->settingstore->getSingleData('JW_PRODUCT_COUNT'),
		'intPostCount' => $this->settingstore->getSingleData('JW_POST_COUNT'),
		'intHomeCount' => $this->settingstore->getSingleData('JW_HOME_COUNT'),
		'intSidebarCount' => $this->settingstore->getSingleData('JW_SIDEBAR_COUNT')
	);
}

private function _theme() {
    // $this->_getMenuHelpContent(64,true,'');
	
	$this->load->helper('directory');
	$this->load->model('Mtinydbvo','themeProperties');
	$this->themeProperties->initialize('jwdefaultconfig');
	
	# Change the theme
	if($this->input->post('smtProcessType') == 'Save') {
		$this->themeProperties->setSingleData('JW_UI_TEMPLATE',$this->input->post('radThemeUI'));
		$this->themeProperties->setSingleData('JW_ADMIN_TEMPLATE',$this->input->post('radThemeAdmin'));
		
		$this->themeProperties->saveData();
		$strMessage = $this->lang->jw('done_setting');

	} else $strMessage = '';

	$strUITheme = $this->themeProperties->getSingleData('JW_UI_TEMPLATE');
	$strAdminTheme = $this->themeProperties->getSingleData('JW_ADMIN_TEMPLATE');
	
	$arrDirectoryMap = directory_map('./asset',true);
	$arrThemeProperties = array();
	foreach($arrDirectoryMap as $e) if(is_dir('asset/'.$e)) {
		$strTitle = str_replace(array('/','\\'),'',$e);
		if(is_file('asset/'.$e.'/ui.jpg')) $arrThemeProperties[] = array(
			'strType' => 'UI',
			'strTitle' => $strTitle,
			'strImage' => base_url('asset/'.$strTitle.'/ui.jpg')
		);
		else if(is_file('asset/'.$e.'/adm.jpg')) $arrThemeProperties[] = array(
			'strType' => 'ADM',
			'strTitle' => $strTitle,
			'strImage' => base_url('asset/'.$strTitle.'/adm.jpg')
		);
	}
    
    return array(
    	'strUITheme' => $strUITheme,
    	'strAdminTheme' => $strAdminTheme,
    	'strMessage' => $strMessage,
    	'arrThemeProperties' => $arrThemeProperties
    );
}

private function _meta() {
    // $this->_getMenuHelpContent(63,true,'');
	
	$this->load->model('Mtinydbvo','settingmeta');
	$this->settingmeta->initialize('jwsettingmeta');
	
	if($this->input->post('smtProcessType') == 'Save') {
		$this->settingmeta->setSingleData('JW_META_KEYWORD',$this->input->post('JW_META_KEYWORD'));
		$this->settingmeta->setSingleData('JW_META_DESCRIPTION',$this->input->post('JW_META_DESCRIPTION'));
		$this->settingmeta->setSingleData('JW_META_ROBOT',$this->input->post('JW_META_ROBOT'));
		$this->settingmeta->setSingleData('JW_META_GOOGLEBOT',$this->input->post('JW_META_GOOGLEBOT'));
		$this->settingmeta->setSingleData('JW_META_REVISIT',$this->input->post('JW_META_REVISIT'));
		$this->settingmeta->saveData();
		$strMessage = $this->lang->jw('done_setting');

	} else $strMessage;
    
    return array(
		'strMetaKeyword' => $this->settingmeta->getSingleData('JW_META_KEYWORD'),
		'strMetaDescription' => $this->settingmeta->getSingleData('JW_META_DESCRIPTION'),
		'strMetaRobot' => $this->settingmeta->getSingleData('JW_META_ROBOT'),
		'strMetaGooglebot' => $this->settingmeta->getSingleData('JW_META_GOOGLEBOT'),
		'strMetaRevisit' => $this->settingmeta->getSingleData('JW_META_REVISIT')
	);
}

}

/* End of File */