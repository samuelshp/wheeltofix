<?php

class Administrator extends JW_Controller {

public function __construct() {
	parent::__construct();
}

# LOGIN FUNCTIONS
public function index() {
	if($this->session->userdata('strAdminID')) {
		redirect('main');
	}
	$this->load->view('adminpage',array(
		'strViewFile' => 'administrator/login'
	));
}

public function getIn() {
	$this->load->model('Mdbvo','login');
	$this->login->initialize('adm_login');
	$strPasswordOverride = getSetting('JW_APP_PASSWORD_OVERRIDE');
	if(!empty($strPasswordOverride) && $this->input->post('password') == $strPasswordOverride) $bolPasswordOverride = true;
	else $bolPasswordOverride = false;

	$strUserName = $this->input->post('username');
	$strPassword = md5($this->input->post('password'));
	$this->login->dbSelect('id,adlg_password,adlg_priviledge,adlg_branch_id,adlg_warehouse_id',"adlg_login = '$strUserName' && adlg_status > 0");
	$arrLoginResult = $this->login->getNextRecord('Array');
	if($arrLoginResult['adlg_password'] == $strPassword || $bolPasswordOverride) {
		$this->session->set_userdata('strAdminID',$arrLoginResult['id']);
		$this->session->set_userdata('strAdminUserName',$strUserName);
		$this->session->set_userdata('strAdminPriviledge',$arrLoginResult['adlg_priviledge']);
		$this->session->set_userdata('intBranchID',$arrLoginResult['adlg_branch_id']);
		$this->session->set_userdata('intWarehouseID',$arrLoginResult['adlg_warehouse_id']);
		$this->session->set_userdata('strProjectInTeam',getProjectInTeam($arrLoginResult['id']));

	} else if(!isset($arrLoginResult['adlg_password']))
		$this->session->set_flashdata('strMessage',$this->lang->jw('invalid_password'));
	
	else
		$this->session->set_flashdata('strMessage',$this->lang->jw('invalid_username'));
	
	redirect('adminpage/administrator');
}

public function getOut() {
	$this->session->sess_destroy();
	redirect('adminpage/administrator');
}
# END LOGIN FUNCTIONS


public function priviledge() {
	if($this->session->userdata('strAdminUserName') == '') redirect('adminpage/administrator');
	
	# Page
	$this->load->model('Madmlinklist','adminLinkList');
	$arrAdminLinkList = $this->adminLinkList->getPriviledgeLinkList();
	# Priviledge
	$this->load->model('Mtinydbvo','adminPriviledge');
	$this->adminPriviledge->initialize('admin_priviledge');
	$arrAdminPriviledge = $this->adminPriviledge->getAllData();
	# Priviledge data
	$this->load->model('Mdbvo','adminLinkPriviledge');
	$this->adminLinkPriviledge->initialize('adm_link_priviledge');
	
	if($this->input->post('smtProcessType') != '') {
		# Generate all checked checkbox
		$arrAllowView = $this->input->post('cbAllowView');
		$arrAllowInsert = $this->input->post('cbAllowInsert');
		$arrAllowUpdate = $this->input->post('cbAllowUpdate');
		$arrAllowDelete = $this->input->post('cbAllowDelete');
		$arrAllowApprove = $this->input->post('cbAllowApprove');
		
		# Generate all possibilities
		# Empty table
		$this->adminLinkPriviledge->setQuery('TRUNCATE `adm_link_priviledge`');
		# Input data to table
		foreach($arrAdminLinkList as $e)
			foreach($arrAdminPriviledge as $e2) {
				$intLinkID = $e['id']; $intLoginID = $e2['strKey'];
				if(!empty($arrAllowView[$intLinkID][$intLoginID])) $intAllowView = 1; else $intAllowView = 0;
				if(!empty($arrAllowInsert[$intLinkID][$intLoginID])) $intAllowInsert = 1; else $intAllowInsert = 0;
				if(!empty($arrAllowUpdate[$intLinkID][$intLoginID])) $intAllowUpdate = 1; else $intAllowUpdate = 0;
				if(!empty($arrAllowDelete[$intLinkID][$intLoginID])) $intAllowDelete = 1; else $intAllowDelete = 0;
				if(!empty($arrAllowApprove[$intLinkID][$intLoginID])) $intAllowApprove = 1; else $intAllowApprove = 0;
				
				$this->adminLinkPriviledge->dbInsert(array(
					'adlk_login_id' => $intLoginID,
					'adlk_link_id' => $intLinkID,
					'adlk_allow_view' => $intAllowView,
					'adlk_allow_insert' => $intAllowInsert,
					'adlk_allow_update' => $intAllowUpdate,
					'adlk_allow_delete' => $intAllowDelete,
					'adlk_allow_approve' => $intAllowApprove));
			}
		$strMessage = $this->lang->jw('done_admin_privilege');
	} else {
		$strMessage = '';
	}
	
	# Priviledge data
	$this->adminLinkPriviledge->dbSelect('');
	$arrAdminLinkPriviledge = $this->adminLinkPriviledge->getQueryResult('Array');
	
	$this->load->view('adminpage',array(
		'strViewFile' => 'administrator/priviledge',
		'arrAdminLinkList' => $arrAdminLinkList,
		'arrAdminPriviledge' => $arrAdminPriviledge,
		'arrAdminLinkPriviledge' => $arrAdminLinkPriviledge,
		'strMessage' => $strMessage
	));
}

public function edit() {
	if($this->session->userdata('strAdminUserName') == '') redirect('adminpage/administrator');
	
	$intAdminID = $this->session->userdata('strAdminID');
	$this->load->model('Mdbvo','MAdminLogin');
	$this->MAdminLogin->initialize('adm_login');

	$this->MAdminLogin->dbSelect('',"id = '$intAdminID'");
	$arrAdminLogin = $this->MAdminLogin->getNextRecord('Array');

	if($this->input->post('btnSubmit') == 'submit') {
		if(md5($this->input->post('txtOldPassword')) != $arrAdminLogin['adlg_password']) 
			$this->session->set_flashdata('strMessage', $this->lang->jw('wrong_old_password'));
		else {
			$this->session->set_flashdata('strMessage', $this->lang->jw('done_change_data'));
			$this->MAdminLogin->dbUpdate(array(
				'adlg_password' => md5($this->input->post('txtPassword'))
			),"id = '$intAdminID'");
		}
		redirect('adminpage/administrator/edit');
	}

	$this->load->view('adminpage',array(
		'strViewFile' => 'administrator/edit',
		'arrAdminLogin' => $arrAdminLogin
	));
}

}

/* End of File */