<?php
/*
PUBLIC FUNCTION:
- index()
- browse()
- view(intProductID,intWarehouseID)
- print_barcode()
- print_opname()

PRIVATE FUNCTION:
- __construct()
*/

class Inventory extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->_getMenuHelpContent(121,true,'adminpage');
}

public function index() {$this->browse();}

public function browse($intPage = 0) {

	// Last stock update?
	$this->load->model('Mtinydbvo','defaultconfig');
	$this->defaultconfig->initialize('jwdefaultconfig');
	$strLastStockUpdate = $this->defaultconfig->getSingleData('JW_STOCK_UPDATE');
	
	$this->load->model('Mwarehouse'); $arrWarehouse = $this->Mwarehouse->getAllData();
	
	$this->load->model('Mproduct'); // Load product

	$bolCalibrate = FALSE;
	if($this->input->post('subSearch') != '') {
		if($this->input->post('filterType') == 'stocklimit_only') {
			$arrProduct = $this->Mproduct->getStockLimitItems();
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = 'Display Stock Limited Products <a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';

		} else {
			$arrProduct = $this->Mproduct->searchByTitle($this->input->post('txtSearchValue'));
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('inventory/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = "Search by Product Name with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
		}
    } else {
    	if($this->input->post('subCalibrate') != '' || $this->input->get('calibrate') != '') {
			$arrPagination['base_url'] = site_url("inventory/browse?pagination=true&calibrate=true", NULL, FALSE);
			$bolCalibrate = TRUE;
    	} else {
			$arrPagination['base_url'] = site_url("inventory/browse?pagination=true", NULL, FALSE);
    	}
		$arrPagination['total_rows'] = $this->Mproduct->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        if($bolCalibrate) {
        	$this->load->model('Mstock');
        	$this->Mstock->calibrate($intPage,$arrPagination['per_page']);
        }
		$arrProduct = $this->Mproduct->getItems($intPage,$arrPagination['per_page']);
	}

	if(!empty($arrProduct)) for($i = 0; $i < count($arrProduct); $i++)
		for($j = 0; $j < count($arrWarehouse); $j++) {
		$arrProduct[$i]['arrStock'][$arrWarehouse[$j]['id']] = countStock($arrProduct[$i]['id'],$arrWarehouse[$j]['id']);
	}

	if($this->input->get('done_calibrate') != '') {
		$this->_arrData['strMessage'] = 'Proses Kalibrasi Stok sudah selesai';
	}

	// Load the views
	$this->load->view('sia',array(
        'strViewFile' => 'inventory/browse',
		'strLastStockUpdate' => $strLastStockUpdate,
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrProduct' => $arrProduct,
		'arrWarehouse' => $arrWarehouse,
		'bolCalibrate' => $bolCalibrate,
        // 'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory')
        'strPageTitle' => 'Kartu Stok'
    ));
}

public function view($intProductID,$intWarehouseID) {
	// Last stock update?
	$this->load->model('Mtinydbvo','defaultconfig');
	$this->defaultconfig->initialize('jwdefaultconfig');
	$strLastStockUpdate = $this->defaultconfig->getSingleData('JW_STOCK_UPDATE');
	$intTotalStock = 0;
	
	// Load product
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getItemByID($intProductID);
	$this->load->model('Mstock'); $arrStock = $this->Mstock->getHistoryByProductID($intProductID,$intWarehouseID);
    $i=0;
    if(!empty($arrStock)) foreach($arrStock as $e) {
        //di sini kalok uda ada id e seng disimpen di transaction_history
        $arrStock[$i]['code'] = $this->Mstock->getDynamicCode($e['trhi_type'],$e['trhi_transaction_id']);
        $i++;
    }
	// Load warehouse
	$this->load->model('Mwarehouse'); $arrWarehouse = $this->Mwarehouse->getItemByID($intWarehouseID);
	
	// Load the views
	$this->load->view('sia',array(
        'strViewFile' => 'inventory/view',
		'arrProduct' => $arrProduct,
		'arrStock' => $arrStock,
		'arrWarehouse' => $arrWarehouse,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'inventory-inventory')
    ));
}

public function print_barcode($intProductID) {
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getItemByID($intProductID);
	$this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwdefaultconfig');

	$arrData = array(
		'arrProduct' => $arrProduct,
		'intProductID' => $intProductID,
	);

	// Load the views
	if($this->input->post('smtPrintBarcode') == 'Print')
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => $this->defconf->getSingleData('JW_PRINT_BARCODE_TYPE'),
			'intPrintNumber' => $this->input->post('txtPrintNumber'),
		), $arrData));
	else $this->load->view('sia',array_merge(array(
	        'strViewFile' => 'inventory/barcode',
			'strPageTitle' => 'Print Barcode',
	    ), $arrData));
}

public function print_tag($intProductID) {
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getItemByID($intProductID);
	$this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwdefaultconfig');

	$arrData = array(
		'arrProduct' => $arrProduct,
		'intProductID' => $intProductID,
	);

	// Load the views
	if($this->input->post('smtPrintPriceTag') == 'Print') 
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'inventorypricetag',
			'intPrintNumber' => $this->input->post('txtPrintNumber'),
		), $arrData));
	else $this->load->view('sia',array_merge(array(
	        'strViewFile' => 'inventory/pricetag',
			'strPageTitle' => 'Print Price Tag',
	    ), $arrData));
}

public function print_opname($intWarehouseID, $txtProduct = '') {
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getAllItems($txtProduct, '', 'In_ID');
	$this->load->model('Mwarehouse'); $arrWarehouse = $this->Mwarehouse->getItemByID($intWarehouseID);

	for($i=0;$i<count($arrProduct);$i++){
        $arrProduct[$i]['strName']=formatProductName("",$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title']);
        $arrProduct[$i]['strNameWithPrice']=formatProductName("",$arrProduct[$i]['prod_title'],$arrProduct[$i]['prob_title'],$arrProduct[$i]['proc_title'],setPrice($arrProduct[$i]['prci_price'],$arrProduct[$i]['prod_currency']));
    }

	$this->load->view('sia_print',array(
		'strPrintFile' => 'inventoryopnamelist',
		'arrProduct' => $arrProduct,
		'arrWarehouse' => $arrWarehouse,
	));
}

}

/* End of File */