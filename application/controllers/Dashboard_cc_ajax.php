<?php

class Dashboard_cc_ajax extends JW_Controller {

    public function getAllData($subkontrak_id){
        $this->load->model('Mdashboardcc');
        ArrayToXml(array('Keuangan' => $this->Mdashboardcc->getAllData($subkontrak_id)));
    }

    public function getAllDataByNamaBahan($id_bahan){
        $this->load->model('Mdashboardcc');
        ArrayToXml(array('KeuanganBahan' => $this->Mdashboardcc->getAllDataByNamaBahan($id_bahan)));
    }

    public function getNamaProyekByOwner($idOwner){
        $this->load->model('Mdashboardcc');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdashboardcc->getNamaProyekByOwner($idOwner)));
    }

    public function getNamaPekerjaanByIdProyek($idProyek){
        $this->load->model('Mdashboardcc');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdashboardcc->getNamaPekerjaanByIdProyek($idProyek)));
    }

    public function getProductByKontrakAndSubkontrak($kontrak,$subkontrak,$bahan = 0){
        $this->load->model('Mdashboardcc');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdashboardcc->getProductByKontrakAndSubkontrak($kontrak,$subkontrak,$bahan)));
    }

    public function getDetailProduct($idProduct,$subkontrak_id){
        $this->load->model('Mdashboardcc');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdashboardcc->getDetailProduct($idProduct,$subkontrak_id)));
    }

    public function getAllBahan(){
        $this->load->model('Mdashboardcc');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdashboardcc->getAllBahan()));
    }
}