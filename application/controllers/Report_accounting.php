<?php
/* 
PUBLIC FUNCTION:
- balance()
- ar_ap()
- akuntansiprofit($intPage = 0)
- akuntansilabarugi($intPage = 0)
- akuntansineraca($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_accounting extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mreportaccounting', 'MReport');
}

public function balance() {
	// $this->_getMenuHelpContent(81,true,'adminpage');
	
	$this->load->model('Mfinance');
	
	$intProfitLoss = 0;
	
	if($this->input->post('smtProcessType') != '') {
		// Set date
		$strFrom = formatDate2($this->input->post('txtDateFrom'),'Y-m-d');
		$strTo = formatDate2($this->input->post('txtDateTo'),'Y-m-d');
	
		$this->load->model('Mdbvo','d');
		$strDateNow = $strFrom; $arrChart = array();
		do {
			$arrChart[$strDateNow] = array(0 => 0, 1 => 0); // 0 = invoice, 1 = purchase
			$this->d->setQuery("SELECT DATE_ADD('$strDateNow',INTERVAL 1 DAY) as dn");
			if($this->d->getNumRows() > 0) { 
				$strDateNow = $this->d->getNextRecord('Object')->dn;
			} else $strDateNow = '';
		} while (!empty($strDateNow) && strtotime($strDateNow) <= strtotime($strTo));
		
		// Finance
		$arrFinance = $this->Mfinance->getItemsByDateRange($strFrom,$strTo);
		$i = 0;
		if(!empty($arrFinance)) foreach($arrFinance as $e) {
			$arrFinance[$i]['fina_rawtype'] = $arrFinance[$i]['fina_type'];

			switch($e['fina_type']) {
				case 1: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][0] = $e['fini_price']; break;
				case 2: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][1] = $e['fini_price']; $arrFinance[$i]['fini_price'] *= -1; break;
				case 3: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][0] = $e['fini_price']; break;
				case 4: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][1] = $e['fini_price']; $arrFinance[$i]['fini_price'] *= -1; break;
			}

			$intProfitLoss += $arrFinance[$i]['fini_price'];
			$arrFinance[$i]['fina_type'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'finance_type'),$arrFinance[$i]['fina_type']);

			$arrFinance[$i]['fini_to'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'finance_destination'),$arrFinance[$i]['fini_to']);
			$i++;
		}

	} else {
		$strFrom = ''; $strTo = ''; $arrChart = array(); $arrFinance = array();
	}

	$arrData = array(
		'strFrom' => $strFrom,
		'strTo' => $strTo,
		'arrFinance' => $arrFinance,
		'intProfitLoss' => $intProfitLoss
	);

	if($this->input->post('smtProcessType') == 'Print')
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'reportbalance',
		), $arrData));
	else $this->load->view('sia',array_merge(array(
        'strViewFile' => 'report_accounting/balance',
		'arrChart' => $arrChart,
    ), $arrData));

}

public function ar_ap() {
	// $this->_getMenuHelpContent(82,true,'adminpage');

	$this->load->model('Mfinance');

	$intProfitLoss = 0;
	$strType = $this->input->post('radType');

	// Finance
	$arrFinance = $this->Mfinance->getARAPItems($strType);
	$i = 0; 
	if(!empty($arrFinance)) foreach($arrFinance as $e) {
		$arrFinance[$i]['fina_rawtype'] = $arrFinance[$i]['fina_type'];

		switch($e['fina_type']) {
			case 1: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][0] = $e['fini_price']; break;
			case 2: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][1] = $e['fini_price']; $arrFinance[$i]['fini_price'] *= -1; break;
			case 3: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][0] = $e['fini_price']; break;
			case 4: $arrChart[formatDate2($e['fina_date'],'Y-m-d')][1] = $e['fini_price']; $arrFinance[$i]['fini_price'] *= -1; break;
		}
		
		$intProfitLoss += $arrFinance[$i]['fini_price'];
		
		$arrFinance[$i]['fina_type'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'finance_type'),$arrFinance[$i]['fina_type']);
		
		$arrFinance[$i]['fini_to'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'finance_destination'),$arrFinance[$i]['fini_to']);
		
		$i++;
	}
	
	// Jangan dibalik urutannya
	// Cari nilai finance yang belum diconvert jadi detail
	$arrFinance2 = $this->Mfinance->getLeftoverItems($strType);
	$arrFinanceTemp = $arrFinance;
	if(!empty($arrFinance2)) foreach($arrFinance2 as $e) {
		$e['fina_rawtype'] = $e['fina_type'];
		
		// Dikurangi dengan yang sudah selesai
		$arrFinanceTemp2 = $this->Mfinanceitem->getItemsByFinanceID($e['id']);
		if(!empty($arrFinanceTemp2)) foreach($arrFinanceTemp2 as $e2) if($e2['fini_status'] == 3) $e['fina_price'] -= $e2['fini_price'];
		
		// Hitung sisa
		switch($e['fina_rawtype']) {
			case 1: $e['fina_price'] *= 1; break;
			case 2: $e['fina_price'] *= -1; break;
			case 3: $e['fina_price'] *= 1; break;
			case 4: $e['fina_price'] *= -1; break;
		}

		// Dikurangi dengan yang masih mengambang
		if(!empty($arrFinanceTemp)) foreach($arrFinanceTemp as $e2) if($e2['id'] == $e['id']) $e['fina_price'] -= $e2['fini_price'];

		// Masukkan jika sisa ada
		if($e['fina_price'] != 0) {
			$e['fini_price'] = $e['fina_price']; $intProfitLoss += $e['fina_price'];

			$e['fini_to'] = ''; $e['fini_date'] = ''; $e['fini_duedate'] = '';
			$e['fini_title'] = loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-remaining');
			$e['fina_type'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'finance_type'),$e['fina_type']);

			$arrFinance[] = $e;
		}
	}

	$arrData = array(
		'arrFinance' => $arrFinance,
		'intProfitLoss' => $intProfitLoss,
	);

	if($this->input->post('smtProcessType') == 'Print') 
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'reportarap',
		), $arrData));
	else $this->load->view('sia',array_merge(array(
        'strViewFile' => 'report_accounting/arap',
		'strType' => $strType,
    ), $arrData));
}

// patricklipesik begin
public function akuntansiprofit($intPage = 0) {
	$this->_getMenuHelpContent(171,true,'adminpage');
	$cname = 'akuntansiprofit';
	
	// 1. persiapan variabel untuk menampung filter
	$strStart = $this->input->post('txtStart');
	if(empty($strStart))
		$strStart = date('Y/m/d');
	
	$strEnd = $this->input->post('txtEnd');
	if(empty($strEnd))
		$strEnd = date('Y/m/d');
	// 1. end
	
	// 2. ketika tombol print diklik
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		
		// 2a. persiapan variabel untuk menampung filter dengan cara GET
		$strStart = $this->input->get('start');
		if(empty($strStart))
			$strStart = date('Y/m/d');
		
		$strEnd = $this->input->get('end');
		if(empty($strEnd))
			$strEnd = date('Y/m/d');
		// 2a. end
		
		// 2b. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiProfit($strStart,$strEnd);
		$strPage = '';
		// 2b. end
		
	}
	// 2. end
	
	// 3. ketika tombol filter diklik
	else if($this->input->post('subSearch') != '') {
		
		// 3a. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiProfit($strStart,$strEnd);
		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_accounting/'.$cname, NULL, FALSE).'">[Back]</a>';
		// 3a. end
		
	}
	// 3. end
	
	// 4. ketika pertama kali menu dibuka
	else {
		
		// 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
		$arrPagination['base_url'] = site_url("report_accounting/".$cname."?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountAkuntansiProfit();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();
		
		$strBrowseMode = '';
		if($this->input->get('page') != '')
			$intPage = $this->input->get('page');
		// 4a. end
		
		// 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
		$arrItems = $this->MReport->getItemsAkuntansiProfit($intPage,$arrPagination['per_page']);
		// 4b. end
		
	}
	// 4.end

	if (!empty($arrItems)) {
		for ($i = 0; $i < count($arrItems); $i++) {
			$arrItems[$i]['nourut'] = $i + 1;
		}
	}

	$arrData = array(
		'strPage' => $strPage,
		'arrItems' => $arrItems,
		'strStart' => $strStart,
		'strEnd' => $strEnd,
		'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
	);

	// 5a. tampilan excel (views\sia_print\report..._export)
	if($this->input->get('excel') != '') {
		HTMLToExcel(
			$this->load->view('sia_print/report'.$cname.'_export', $arrData, true),
		$cname.'.xls');
	}
	// 5a. end
	
	// 5b. tampilan print (views\sia_print\report...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'report'.$cname,
		), $arrData));
	}
	// 5b. end
	
	// 5c. tampilan default menu (views\sia\report_accounting\...)
	else {
		$this->load->view('sia',array_merge(array(
	        'strViewFile' => 'report_accounting/'.$cname,
			'strBrowseMode' => $strBrowseMode,
			'strPageTitle' => 'Laporan',
	    ), $arrData));
	}
	// 5c. end
	
}

public function akuntansilabarugi($intPage = 0) {
	$this->_getMenuHelpContent(171,true,'adminpage');
	$cname = 'akuntansilabarugi';
	
	// 1. persiapan variabel untuk menampung filter
	$strPeriode = $this->input->post('txtPeriode');
	if(empty($strPeriode))
		$strPeriode = date('Y-m');
	// 1. end
	
	// 2. ketika tombol print diklik
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		
		// 2a. persiapan variabel untuk menampung filter dengan cara GET
		$strPeriode = $this->input->get('periode');
		if(empty($strPeriode))
			$strPeriode = date('Y-m');
		// 2a. end
		
		// 2b. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiLabaRugi($strPeriode,0);
		$strPage = '';
		// 2b. end
		
	}
	// 2. end
	
	// 3. ketika tombol filter diklik
	else if($this->input->post('subSearch') != '') {
		
		// 3a. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiLabaRugi($strPeriode,0);
		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_accounting/'.$cname, NULL, FALSE).'">[Back]</a>';
		// 3a. end
		
	}
	// 3. end
	
	// 4. ketika pertama kali menu dibuka
	else {
		
		// 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
		$arrPagination['base_url'] = site_url("report_accounting/".$cname."?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = 1000; // count($arrItems); // $this->MReport->getCountAkuntansiLabaRugi();
		$arrPagination['per_page'] = 1000; // $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();
		
		$strBrowseMode = '';
		if($this->input->get('page') != '')
			$intPage = $this->input->get('page');
		// 4a. end
		
		// 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
		$arrItems = $this->MReport->searchAkuntansiLabaRugi($strPeriode,0);; // getItemsAkuntansiLabaRugi($intPage,$arrPagination['per_page']);
		// 4b. end
		
	}
	// 4.end

	if (!empty($arrItems)) {
		for ($i = 0; $i < count($arrItems); $i++) {
			$arrItems[$i]['nourut'] = $i + 1;
		}
	}

	$arrData = array(
		'strPage' => $strPage,
		'arrItems' => $arrItems,
		'strStart' => $strStart,
		'strEnd' => $strEnd,
		'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
	);

	// 5a. tampilan excel (views\sia_print\report..._export)
	if($this->input->get('excel') != '') {
		HTMLToExcel(
			$this->load->view('sia_print/report'.$cname.'_export', $arrData, true),
		$cname.'.xls');
	}
	// 5a. end
	
	// 5b. tampilan print (views\sia_print\report...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'report'.$cname,
		), $arrData));
	}
	// 5b. end
	
	// 5c. tampilan default menu (views\sia\report_accounting\...)
	else {
		$this->load->view('sia',array_merge(array(
	        'strViewFile' => 'report_accounting/'.$cname,
			'strBrowseMode' => $strBrowseMode,
			'strPageTitle' => 'Laporan',
	    ), $arrData));
	}
	// 5c. end
	
}

public function akuntansineraca($intPage = 0) {
	$this->_getMenuHelpContent(171,true,'adminpage');
	$cname = 'akuntansineraca';
	
	// 1. persiapan variabel untuk menampung filter
	$strPeriode = $this->input->post('txtPeriode');
	if(empty($strPeriode))
		$strPeriode = date('Y-m');
	// 1. end
	
	// 2. ketika tombol print diklik
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		
		// 2a. persiapan variabel untuk menampung filter dengan cara GET
		$strPeriode = $this->input->get('periode');
		if(empty($strPeriode))
			$strPeriode = date('Y-m');
		// 2a. end
		
		// 2b. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiNeraca($strPeriode,0);
		$strPage = '';
		// 2b. end
		
	}
	// 2. end
	
	// 3. ketika tombol filter diklik
	else if($this->input->post('subSearch') != '') {
		
		// 3a. pemanggilan function model dengan dibatasi filter
		$arrItems = $this->MReport->searchAkuntansiNeraca($strPeriode,0);
		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_accounting/'.$cname, NULL, FALSE).'">[Back]</a>';
		// 3a. end
		
	}
	// 3. end
	
	// 4. ketika pertama kali menu dibuka
	else {
		
		// 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
		$arrPagination['base_url'] = site_url("report_accounting/".$cname."?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = 1000; // count($arrItems); // $this->MReport->getCountAkuntansiNeraca();
		$arrPagination['per_page'] = 1000; // $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();
		
		$strBrowseMode = '';
		if($this->input->get('page') != '')
			$intPage = $this->input->get('page');
		// 4a. end
		
		// 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
		$arrItems = $this->MReport->searchAkuntansiNeraca($strPeriode,0);; // getItemsAkuntansiNeraca($intPage,$arrPagination['per_page']);
		// 4b. end
		
	}
	// 4.end

	if (!empty($arrItems)) {
		for ($i = 0; $i < count($arrItems); $i++) {
			$arrItems[$i]['nourut'] = $i + 1;
		}
	}

	$arrData = array(
		'strPage' => $strPage,
		'arrItems' => $arrItems,
		'strStart' => $strStart,
		'strEnd' => $strEnd,
		'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
	);

	// 5a. tampilan excel (views\sia_print\report..._export)
	if($this->input->get('excel') != '') {
		HTMLToExcel(
			$this->load->view('sia_print/report'.$cname.'_export', $arrData, true),
		$cname.'.xls');
	}
	// 5a. end
	
	// 5b. tampilan print (views\sia_print\report...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
	if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
		$this->load->view('sia_print',array_merge(array(
			'strPrintFile' => 'report'.$cname,
		), $arrData));
	}
	// 5b. end
	
	// 5c. tampilan default menu (views\sia\report_accounting\...)
	else {
		$this->load->view('sia',array_merge(array(
	        'strViewFile' => 'report_accounting/'.$cname,
			'strBrowseMode' => $strBrowseMode,
			'strPageTitle' => 'Laporan',
	    ), $arrData));
	}
	// 5c. end
	
}
// patricklipesik end

}