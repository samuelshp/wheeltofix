<?php
/**
 * 
 */
class Postingtransaksi extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
        // Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		$this->_getMenuHelpContent(255,true,'adminpage');
		$this->load->model('Mjournal');		
	}

	public function index()
	{
		if ($this->input->post('btnPostingTransaksi') != '') {
			$arrData = array(
				'periode' => $this->input->post('periode'), 
				'tahun' => $this->input->post('tahun') 
			);
			
			$this->Mjournal->postingTransaksi($arrData);
			$this->_arrData['strMessage'] = "Transaksi-transaksi periode ".formatDate2($arrData['tahun']."-".$arrData['periode']."-01", "F")." tahun ".$arrData['tahun']." berhasil diposting";
			$this->session->set_flashdata('strMessage', $this->_arrData['strMessage']);
			redirect('postingtransaksi');			
		}		

		$this->load->view('sia', array_merge(array(
            'strViewFile' => 'posting_transaksi/add',
            'strPageTitle' => 'Input Periode Posting Transaksi',            
        ),$this->admlinklist->getMenuPermission(255,1)));
	}


}