<?php
/* 
PUBLIC FUNCTION:
- product_stock($intPage = 0)
- product_stock_kanvas($intPage = 0)
- sellout($intPage = 0)
- stockinvalue($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_stock extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mreportstock', 'MReport');
}

public function product_stock($intPage = 0) {
    $this->_getMenuHelpContent(122,true,'adminpage');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        if($this->input->get('supplier') != '') $arrStock = $this->MReport->searchStockBySupplier($this->input->get('supplier'));
            
        else $arrStock = $this->MReport->getItemsStock();
        
    } else if($this->input->post('subSearch') != '') {
		$arrStock = $this->MReport->searchStockBySupplier($this->input->post('txtSearchValue'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_stock/product_stock', NULL, FALSE).'">[Back]</a>';
		$this->_arrData['strKeyword'] = $this->input->post('txtSearchValue');
		$this->_arrData['strMessage'] = "Search by Supplier with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	
    } else {
		$arrPagination['base_url'] = site_url("report_stock/product_stock?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountStock();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		
		$arrStock = $this->MReport->getItemsStock($intPage,$arrPagination['per_page']);
        
	}

    if (!empty($arrStock)) {
        for ($i = 0; $i < count($arrStock); $i++) {
            if (array_key_exists('prod_conv1', $arrStock[$i]) && array_key_exists('prod_conv2', $arrStock[$i]) && array_key_exists('prod_conv3', $arrStock[$i])) {

                if ($arrStock[$i]['prod_conv1'] == 0) {
                    $arrStock[$i]['qty1'] = 0;
                    $modQty1 = 0;
                    $arrStock[$i]['pending1'] = 0;                        
                    $modPending1 = 0;
                } else {
                    $arrStock[$i]['qty1'] = (int) ($arrStock[$i]['qty'] / $arrStock[$i]['prod_conv1']);
                    $modQty1 = $arrStock[$i]['qty'] % $arrStock[$i]['prod_conv1'];
                    $arrStock[$i]['pending1'] = (int) ($arrStock[$i]['pending'] / $arrStock[$i]['prod_conv1']);
                    $modPending1 = $arrStock[$i]['pending'] % $arrStock[$i]['prod_conv1'];
                };

                if ($arrStock[$i]['prod_conv2'] == 0) {
                    $arrStock[$i]['qty2'] = 0;
                    $modQty2 = 0;
                    $arrStock[$i]['pending2'] = 0;
                    $modPending2 = 0;
                } else {
                    $arrStock[$i]['qty2'] = (int) ($modQty1 / $arrStock[$i]['prod_conv2']);
                    $modQty2 = $modQty1 % $arrStock[$i]['prod_conv2'];
                    $arrStock[$i]['pending2'] = (int) ($modPending1 / $arrStock[$i]['prod_conv2']);
                    $modPending2 = $modPending1 % $arrStock[$i]['prod_conv2'];
                };

                $arrStock[$i]['qty3'] = $modQty2;
                $arrStock[$i]['pending3'] = $modPending2;

            }
        }
    }

    $arrData = array(
        'arrStock' => $arrStock,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/stock_export', $arrData, true),
        'stock.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print 
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'stock',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_stock/stock',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
        ), $arrData));
    }
    
}

public function product_stock_kanvas($intPage = 0) {
    $this->_getMenuHelpContent(123,true,'adminpage');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        if($this->input->get('salesman') != '') $arrStock = $this->MReport->searchStockKanvasBySalesman($this->input->get('salesman'));
            
        else $arrStock = $this->MReport->getItemsStockKanvas();
        
    } else if($this->input->post('subSearch') != '') {
		$arrStock = $this->MReport->searchStockKanvasBySalesman($this->input->post('txtSearchValue'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_stock/product_stock_kanvas', NULL, FALSE).'">[Back]</a>';
		$this->_arrData['strKeyword'] = $this->input->post('txtSearchValue');
		$this->_arrData['strMessage'] = "Search by Salesman with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	
    } else {
		$arrPagination['base_url'] = site_url("report_stock/product_stock_kanvas?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountStockKanvas();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		
		$arrStock = $this->MReport->getItemsStockKanvas($intPage,$arrPagination['per_page']);
        
	}

    if (!empty($arrStock)) {
        for ($i = 0; $i < count($arrStock); $i++) {
            if (array_key_exists('prod_conv1', $arrStock[$i]) && array_key_exists('prod_conv2', $arrStock[$i]) && array_key_exists('prod_conv3', $arrStock[$i])) {

                if ($arrStock[$i]['prod_conv1'] == 0) {
                    $arrStock[$i]['qty1'] = 0;
                    $modQty1 = 0;
                    $arrStock[$i]['pending1'] = 0;                        
                    $modPending1 = 0;
                } else {
                    $arrStock[$i]['qty1'] = (int) ($arrStock[$i]['qty'] / $arrStock[$i]['prod_conv1']);
                    $modQty1 = $arrStock[$i]['qty'] % $arrStock[$i]['prod_conv1'];
                    $arrStock[$i]['pending1'] = (int) ($arrStock[$i]['pending'] / $arrStock[$i]['prod_conv1']);
                    $modPending1 = $arrStock[$i]['pending'] % $arrStock[$i]['prod_conv1'];
                };

                if ($arrStock[$i]['prod_conv2'] == 0) {
                    $arrStock[$i]['qty2'] = 0;
                    $modQty2 = 0;
                    $arrStock[$i]['pending2'] = 0;
                    $modPending2 = 0;
                } else {
                    $arrStock[$i]['qty2'] = (int) ($modQty1 / $arrStock[$i]['prod_conv2']);
                    $modQty2 = $modQty1 % $arrStock[$i]['prod_conv2'];
                    $arrStock[$i]['pending2'] = (int) ($modPending1 / $arrStock[$i]['prod_conv2']);
                    $modPending2 = $modPending1 % $arrStock[$i]['prod_conv2'];
                };

                $arrStock[$i]['qty3'] = $modQty2;
                $arrStock[$i]['pending3'] = $modPending2;

            }
        }
    }

    $arrData = array(
        'arrStock' => $arrStock,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/stockkanvas_export', $arrData, true),
        'stock_kanvas.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print 
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'stockkanvas',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_stock/stockkanvas',
            'strPage' => $strPage,
            'strBrowseMode' => $strBrowseMode,
        ), $arrData));
    }
    
}

public function sellout($intPage = 0) {
    $this->_getMenuHelpContent(125,true,'adminpage');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        if($this->input->get('supplier') != '') $arrStock = $this->MReport->searchSellOutBySupplier($this->input->get('supplier'));
            
        else $arrStock = $this->MReport->getItemsSellOut();
        
    } else if($this->input->post('subSearch') != '') {
		$arrStock = $this->MReport->searchSellOutBySupplier($this->input->post('txtSearchValue'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_stock/sellout', NULL, FALSE).'">[Back]</a>';
		$this->_arrData['strKeyword'] = $this->input->post('txtSearchValue');
		$this->_arrData['strMessage'] = "Search by Supplier with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	
    } else {
		$arrPagination['base_url'] = site_url("report_stock/sellout?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountSellOut();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		
		$arrStock = $this->MReport->getItemsSellOut($intPage,$arrPagination['per_page']);
        
	}

    if (!empty($arrStock)) {
        for ($i = 0; $i < count($arrStock); $i++) {
            if (array_key_exists('prod_conv1', $arrStock[$i]) && array_key_exists('prod_conv2', $arrStock[$i]) && array_key_exists('prod_conv3', $arrStock[$i])) {

                if ($arrStock[$i]['prod_conv1'] == 0) {
                    $arrStock[$i]['qty1'] = 0;
                    $modQty1 = 0;
                } else {
                    $arrStock[$i]['qty1'] = (int) ($arrStock[$i]['qty'] / $arrStock[$i]['prod_conv1']);
                    $modQty1 = $arrStock[$i]['qty'] % $arrStock[$i]['prod_conv1'];
                };

                if ($arrStock[$i]['prod_conv2'] == 0) {
                    $arrStock[$i]['qty2'] = 0;
                    $modQty2 = 0;
                } else {
                    $arrStock[$i]['qty2'] = (int) ($modQty1 / $arrStock[$i]['prod_conv2']);
                    $modQty2 = $modQty1 % $arrStock[$i]['prod_conv2'];
                };

                $arrStock[$i]['qty3'] = $modQty2;

            }
        }
    }

    $arrData = array(
        'arrStock' => $arrStock,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/sellout_export', $arrData, true),
        'sell_out.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') { //print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'sellout',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_stock/sellout',
            'strBrowseMode' => $strBrowseMode,
            'strPage' => $strPage,
        ), $arrData));
    }
}

public function stockinvalue($intPage = 0) {
    $this->_getMenuHelpContent(124,true,'adminpage');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        if($this->input->get('supplier') != '') $arrStock = $this->MReport->searchStockInValueBySupplier($this->input->get('supplier'));
        else $arrStock = $this->MReport->getItemsStockInValue();
    } else if($this->input->post('subSearch') != '') {
		$arrStock = $this->MReport->searchStockInValueBySupplier($this->input->post('txtSearchValue'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('report_stock/stockinvalue', NULL, FALSE).'">[Back]</a>';
		$this->_arrData['strKeyword'] = $this->input->post('txtSearchValue');
		$this->_arrData['strMessage'] = "Search by Supplier with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
    } else {
		$arrPagination['base_url'] = site_url("report_stock/stockinvalue?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->MReport->getCountStockInValue();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');
		$arrStock = $this->MReport->getItemsStockInValue($intPage,$arrPagination['per_page']);
	}

    if (!empty($arrStock)) {
        for ($i = 0; $i < count($arrStock); $i++) {
            if (array_key_exists('prod_conv1', $arrStock[$i]) && array_key_exists('prod_conv2', $arrStock[$i]) && array_key_exists('prod_conv3', $arrStock[$i])) {

                if ($arrStock[$i]['prod_conv1'] == 0) {
                    $arrStock[$i]['qty1'] = 0;
                    $modQty1 = 0;
                } else {
                    $arrStock[$i]['qty1'] = (int) ($arrStock[$i]['qty'] / $arrStock[$i]['prod_conv1']);
                    $modQty1 = $arrStock[$i]['qty'] % $arrStock[$i]['prod_conv1'];
                };

                if ($arrStock[$i]['prod_conv2'] == 0) {
                    $arrStock[$i]['qty2'] = 0;
                    $modQty2 = 0;
                } else {
                    $arrStock[$i]['qty2'] = (int) ($modQty1 / $arrStock[$i]['prod_conv2']);
                    $modQty2 = $modQty1 % $arrStock[$i]['prod_conv2'];
                };

                $arrStock[$i]['qty3'] = $modQty2;

            }
        }
    }

    $arrData = array(
        'arrStock' => $arrStock,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/stockinvalue_export', $arrData, true),
        'stock_in_value.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') { //print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'stockinvalue',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_stock/stockinvalue',
            'strBrowseMode' => $strBrowseMode,
            'strPage' => $strPage,
        ), $arrData));
    }
}

}