<?php
/* 
PUBLIC FUNCTION:
- price_list()

PRIVATE FUNCTION:
- __construct()
*/

class Report extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	$this->load->model('Mreport');
}

public function price_list() {
	$this->load->model('Mproduct'); $arrProduct = $this->Mproduct->getAllData();

	if($this->input->post('smtProcessType') == 'Print')
		$this->load->view('sia_print',array(
			'strPrintFile' => 'reportpricelist',
			'arrProduct' => $arrProduct
		));
	else $this->load->view('sia',array(
        'strViewFile' => 'report/pricelist',
		'arrProduct' => $arrProduct
    ));
}

public function kas() {
	$this->_getMenuHelpContent(259,true,'adminpage');
	$this->load->model('Maccount');
	$arrAccount = $this->Maccount->getAllDetailAccount();
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['arrAccount'] = $arrAccount;
		$arrData['setPrice'] = array('NOMINAL');
		$arrData['hidden'] = array('relasi_kode','urutan','grup', );
		$arrData['bold'] = array('SALDO AWAL','PENERIMAAN', 'TOTAL PENERIMAAN', 'PENGELUARAN', 'TOTAL PENGELUARAN', 'SALDO AKHIR');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		$result = $this->Maccount->getItemByCode($this->input->post('txtAccountStart'));
		
		$arrReport = $this->Mreport->reportKasBank($this->input->post('txtAccountStart'),$this->input->post('txtDateStart'),$this->input->post('txtDateEnd'));
		$arrData['strPageMode'] = 'Laporan Harian Kas / Bank';
		$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y')."<br />Akun : [". $this->input->post('txtAccountStart'). "] ". $result['acco_name'];
		$arrData['dataPrintFooter'] = '<div class="hidden">
                    <div class="col-xs-4 text-center">
                        Dibuat Oleh
                    </div>
                    <div class="col-xs-4 text-center">
                        Disetujui Oleh
                    </div>
                    <div class="col-xs-4 text-center">
                        Diterima Oleh
                    </div>
                </div>
                <div class="hidden" style="margin-top:100px;">
                    <div class="col-xs-4 text-center">
                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                    </div>
                    <div class="col-xs-4 text-center">
                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                    </div>
                    <div class="col-xs-4 text-center">
                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                    </div>
                </div>';

		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultkas',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Harian Kas / Bank'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/kas',
			'arrAccount' => $arrAccount,
			'strPageTitle' => "Laporan Harian Kas / Bank",
		));
	}
}

public function biayaproyek() {
	$this->_getMenuHelpContent(260,true,'adminpage');
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['setPrice'] = array('BAHAN POKOK','BAHAN BANTU','UPAH BORONG','SEWA','PAJAK','BIAYA LAIN','TOTAL BIAYA','TERMIN','SELISIH');
		$arrData['hidden'] = array('relasi_kode','urutan','grup');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		
		$arrReport = $this->Mreport->reportBiayaProyek($this->input->post('txtDateEnd'));
		$arrData['strPageMode'] = 'Laporan Biaya Proyek';
		$arrData['dataPrint'] = "Tanggal Akhir : ". formatDate2($this->input->post('txtDateEnd'),'d F Y');

		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultbiayaproyek',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Biaya Proyek'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/biayaproyek',
			'strPageTitle' => "Laporan Biaya Proyek",
		));
	}
}

public function biayapemasukanproyek() {
	$this->_getMenuHelpContent(262,true,'adminpage');
	$this->load->model('Mkontrak');
	$arrProject = $this->Mkontrak->getActiveItems(0,1000);
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['arrProject'] = $arrProject;
		$arrData['th'] = 'Data';
		$arrData['setPrice'] = array('BAHAN POKOK','BAHAN BANTU','UPAH','SEWA & SUB PEK.','PAJAK','BIAYA LAIN','TOTAL BIAYA','TERMIN','SELISIH');
		$arrData['hidden'] = array('Proyek','relasi_kode','urutan','grup');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';

		$arrData['strPageMode'] = 'Laporan Biaya & Pemasukan Proyek';
		$result = $this->Mkontrak->getItemByID($this->input->post('intProjectID'));

		$period = explode('-',$this->input->post('txtPeriode'));
		$arrReport = $this->Mreport->reportBiayaPemasukanProyek($this->input->post('intProjectID'),$period[1],$period[0]);
		$arrData['dataPrint'] = "<br />Proyek : ".$result['kont_name'];
		$arrData['dataPrint'] .= "Periode : " . formatDate2(formatDate2($this->input->post('txtPeriode').'-01 +1 month','Y-m-d').' -1 day','d F Y');

		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultbiayapemasukanproyek',
			'arrReport' => $arrReport,
			'arrData' => $arrData,
			'strPageTitle' => 'Laporan Biaya & Pemasukan Proyek'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/biayapemasukanproyek',
			'arrProject' => $arrProject,			
			'strPageTitle' => "Laporan Biaya & Pemasukan Proyek",
		));
	}
}

public function mutasibiaya() {
	$this->_getMenuHelpContent(263,true,'adminpage');
	$this->load->model('Maccount');
	$arrAccount = $this->Maccount->getAllDetailAccount();
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['arrAccount'] = $arrAccount;
		$arrData['setPrice'] = array('debet','kredit');
		$arrData['hidden'] = array('posisi','@sum_debet :=@sum_debet + debet','@sum_kredit :=@sum_kredit + kredit');
		$arrData['bold'] = array('');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		$result = $this->Maccount->getItemByID($this->input->post('txtAccountStart'));
		
		$arrReport = $this->Mreport->reportMutasiBiaya($this->input->post('txtAccountStart'),$this->input->post('txtDateStart'),$this->input->post('txtDateEnd'));
		$arrData['strPageMode'] = 'Laporan Mutasi Biaya';
		$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y')."<br />Akun : [". $result['acco_code']. "] ". $result['acco_name'];

		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultmutasibiaya',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Mutasi Biaya'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/mutasibiaya',
			'arrAccount' => $arrAccount,
			'strPageTitle' => "Laporan Mutasi Biaya",
		));
	}
}

public function hutang() {
    $this->_getMenuHelpContent(251,true,'adminpage');
	$this->load->model('Msupplier');
	$arrSupplier = $this->Msupplier->getAllData();
	$this->load->model('Mkontrak');
	$arrProject = $this->Mkontrak->getActiveItems(0,1000);
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['arrSupplier'] = $arrSupplier;
		$arrData['arrProject'] = $arrProject;
		$arrData['setPrice'] = array('saldo_awal','beli','bayar','saldo_akhir','sisa','debit','kredit','total');
		$arrData['hidden'] = array('relasi_kode','urutan','grup','@sa := @sa + saldo_awal','@beli := @beli + beli','@bayar := @bayar + bayar','@sakhir := @sakhir + saldo_akhir','@sisa := @sisa + sisa');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		$arrData['dataPrint'] = '';
		if($this->input->post('intMode') == 1) {
			$arrReport = $this->Mreport->reportHutangRekap($this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),$this->input->post('intSupplierID'),$this->input->post('intProjectID'));
			$arrData['strPageMode'] = 'Laporan Rekap Hutang';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intSupplierID') == 0) $arrData['dataPrint'] .= "<br />Supplier : All Supplier";
			else {
				$result = $this->Msupplier->getItemByID($this->input->post('intSupplierID'));
				$arrData['dataPrint'] .= "<br />Supplier : ".$result['supp_name'];
			}
			if($this->input->post('intProjectID') == 0) $arrData['dataPrint'] .= "<br />Proyek : All Project";
			else {
				$result = $this->Mkontrak->getItemByID($this->input->post('intProjectID'));
				$arrData['dataPrint'] .= "<br />Proyek : ".$result['kont_name'];
			}
		} else if($this->input->post('intMode') == 2) {
			$arrReport = $this->Mreport->reportHutangDetail($this->input->post('txtDateEnd'),$this->input->post('intSupplierID'),$this->input->post('intProjectID'));
			$arrData['th_subtotal'] = 'sisa';
			$arrData['th_separator'] = 'relasi_nama';
			$arrData['strPageMode'] = 'Laporan Detail Hutang';
			$arrData['dataPrint'] = "Tanggal Akhir : " . formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intSupplierID') == 0) $arrData['dataPrint'] .= "<br />Supplier : All Supplier";
			else {
				$result = $this->Msupplier->getItemByID($this->input->post('intSupplierID'));
				$arrData['dataPrint'] .= "<br />Supplier : ".$result['supp_name'];
			}
			if($this->input->post('intProjectID') == 0) $arrData['dataPrint'] .= "<br />Proyek : All Project";
			else {
				$result = $this->Mkontrak->getItemByID($this->input->post('intProjectID'));
				$arrData['dataPrint'] .= "<br />Proyek : ".$result['kont_name'];
			}
		} else {
			$arrReport = $this->Mreport->reportHutangKartu($this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),$this->input->post('intSupplierID'),$this->input->post('intProjectID'));
			if($this->input->post('intProjectID') != 0) array_push($arrData['hidden'], 'nama_proyek');
			$arrData['strPageMode'] = 'Laporan Kartu Hutang';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intSupplierID') == 0) $arrData['dataPrint'] .= "<br />Supplier : All Supplier";
			else {
				$result = $this->Msupplier->getItemByID($this->input->post('intSupplierID'));
				$arrData['dataPrint'] .= "<br />Supplier : ".$result['supp_name'];
			}
		}
		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resulthutang',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Hutang'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/hutang',
			'arrSupplier' => $arrSupplier,
			'arrProject' => $arrProject,
			'strPageTitle' => "Laporan Hutang",
		));
	}
}

public function piutang() {
    $this->_getMenuHelpContent(252,true,'adminpage');
	$this->load->model('Msupplier');
	$arrCustomer = $this->Mcustomer->getAllData();
	$this->load->model('Mkontrak');
	$arrProject = $this->Mkontrak->getActiveItems(0,1000);
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['arrCustomer'] = $arrCustomer;
		$arrData['arrProject'] = $arrProject;
		$arrData['setPrice'] = array('saldo_awal','jual','bayar','saldo_akhir','sisa','debit','kredit','total');
		$arrData['hidden'] = array('relasi_kode','urutan','grup','@sa := @sa + saldo_awal','@jual := @jual + jual','@bayar := @bayar + bayar','@sakhir := @sakhir + saldo_akhir','@sisa := @sisa + sisa');
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		$arrData['dataPrint'] = '';
		if($this->input->post('intMode') == 1) {
			$arrReport = $this->Mreport->reportPiutangRekap($this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),$this->input->post('intCustomerID'),$this->input->post('intProjectID'));
			$arrData['strPageMode'] = 'Laporan Rekap Piutang';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intCustomerID') == 0) $arrData['dataPrint'] .= "<br />Customer : All Customer";
			else {
				$result = $this->Mcustomer->getItemByID($this->input->post('intCustomerID'));
				$arrData['dataPrint'] .= "<br />Customer : ".$result['cust_name'];
			}
			if($this->input->post('intProjectID') == 0) $arrData['dataPrint'] .= "<br />Proyek : All Project";
			else {
				$result = $this->Mkontrak->getItemByID($this->input->post('intProjectID'));
				$arrData['dataPrint'] .= "<br />Proyek : ".$result['kont_name'];
			}
		} else if($this->input->post('intMode') == 2) {
			$arrReport = $this->Mreport->reportPiutangDetail($this->input->post('txtDateEnd'),$this->input->post('intCustomerID'),$this->input->post('intProjectID'));
			$arrData['th_subtotal'] = 'sisa';
			$arrData['th_separator'] = 'relasi_nama';
			$arrData['strPageMode'] = 'Laporan Detail Piutang';
			$arrData['dataPrint'] = "Tanggal  Akhir : ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intCustomerID') == 0) $arrData['dataPrint'] .= "<br />Customer : All Customer";
			else {
				$result = $this->Mcustomer->getItemByID($this->input->post('intCustomerID'));
				$arrData['dataPrint'] .= "<br />Customer : ".$result['cust_name'];
			}
			if($this->input->post('intProjectID') == 0) $arrData['dataPrint'] .= "<br />Proyek : All Project";
			else {
				$result = $this->Mkontrak->getItemByID($this->input->post('intProjectID'));
				$arrData['dataPrint'] .= "<br />Proyek : ".$result['kont_name'];
			}
		} else {
			$arrReport = $this->Mreport->reportPiutangKartu($this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),$this->input->post('intCustomerID'),$this->input->post('intProjectID'));
			if($this->input->post('intProjectID') != 0) array_push($arrData['hidden'], 'nama_proyek');
			$arrData['strPageMode'] = 'Laporan Kartu Piutang';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('intCustomerID') == 0) $arrData['dataPrint'] .= "<br />Customer : All Customer";
			else {
				$result = $this->Mcustomer->getItemByID($this->input->post('intCustomerID'));
				$arrData['dataPrint'] .= "<br />Customer : ".$result['cust_name'];
			}
		}
		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultpiutang',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Piutang'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/piutang',
			'arrCustomer' => $arrCustomer,
			'arrProject' => $arrProject,
			'strPageTitle' => "Laporan Piutang",
		));
	}
}

public function accounting() {
    $this->_getMenuHelpContent(254,true,'adminpage');
	$this->load->model('Maccount');
	$arrAccount = $this->Maccount->getAllDetailAccount();
	if($this->input->post('smtProcessType') == 'Generate') {
		$arrReport = array();
		$arrData = $this->input->post();
		$arrData['th'] = 'Data';
		$arrData['arrAccount'] = $arrAccount;

		$arrData['setPrice'] = array('debet','kredit','saldo','saldo_awal','saldo_akhir','debet_adj','kredit_adj','saldo_akhir_adj','nominalkiri','nominalkanan','totalrl');
		$arrData['hidden'] = array();
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';
		$arrData['dataPrint'] = '';

		$periode = explode('-',$this->input->post('txtPeriode'));
		if($this->input->post('intMode') == 1) {
			$arrReport = $this->Mreport->reportAccounting($this->input->post('txtAccountStart'),$this->input->post('txtAccountEnd'),$this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),0,0,$this->input->post('intMode'),'');
			$arrData['strPageMode'] = 'Laporan General Ledger';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y')."<br />No. Akun : ". $this->input->post('txtAccountStart') . " s/d " . $this->input->post('txtAccountEnd') ;
			$arrData['hidden'] = array('urutan','@rp_gl_nomoracc := z.nomoraccount');
		} else if($this->input->post('intMode') == 2) {
			$arrReport = $this->Mreport->reportAccounting('1','9999','','',$periode[1],$periode[0],$this->input->post('intMode'),'');
			$arrData['strPageMode'] = 'Laporan Laba Rugi';
			$arrData['dataPrint'] = "Periode : " . formatDate2(formatDate2($this->input->post('txtPeriode').'-01 +1 month','Y-m-d').' -1 day','d F Y');
			$arrData['hidden'] = array('lvl');
		} else if($this->input->post('intMode') == 3) {
			$arrReport = $this->Mreport->reportAccounting('1','9999','','',$periode[1],$periode[0],$this->input->post('intMode'),'');
			$arrData['strPageMode'] = 'Laporan Trial Balance';
			$arrData['dataPrint'] = "Periode : " . formatDate2(formatDate2($this->input->post('txtPeriode').'-01 +1 month','Y-m-d').' -1 day','d F Y');
			$arrData['hidden'] = array('@sum_awal := @sum_awal + saldo_awal','@sum_debet := @sum_debet + debet','@sum_kredit := @sum_kredit + kredit','@sum_akhir := @sum_akhir + saldo_akhir','@sum_debet_adj := @sum_debet_adj + debet_adj','@sum_kredit_adj := @sum_kredit_adj + kredit_adj','@sum_akhir_adj := @sum_akhir_adj + saldo_akhir_adj');
		} else if($this->input->post('intMode') == 4) {
			$arrReport = $this->Mreport->reportAccounting('1','9999',$this->input->post('txtDateStart'),$this->input->post('txtDateEnd'),0,0,$this->input->post('intMode'),$this->input->post('txtTransactionCode'));
			$arrData['strPageMode'] = 'Laporan Jurnal';
			$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y');
			if($this->input->post('txtTransactionCode') != '') $arrData['dataPrint'] .= "<br />Kode Transaksi : ". $this->input->post('txtTransactionCode');
			$arrData['hidden'] = array('urutan');
		} else {
			$arrReport = $this->Mreport->reportAccounting('1','9999','','',$periode[1],$periode[0],$this->input->post('intMode'),'');
			$arrData['strPageMode'] = 'Laporan Neraca';
			$arrData['dataPrint'] = "Periode : " . formatDate2(formatDate2($this->input->post('txtPeriode').'-01 +1 month','Y-m-d').' -1 day','d F Y');
		}
		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultaccounting',
			'arrReport' => $arrReport,
			'strPageTitle' => 'Laporan Accounting'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/accounting',
			'arrAccount' => $arrAccount,
			'strPageTitle' => "Laporan Accounting",
		));
	}
}

public function penerimaanpengeluaranproyek()
{
	$this->_getMenuHelpContent(259,true,'adminpage');
	$this->load->model('Maccount');
	$this->load->model('Mkontrak');	
	$arrProject = $this->Mkontrak->getActiveItems(0,1000);
	$arrAccount = $this->Maccount->getAllDetailAccount();
	if($this->input->post('smtProcessType') == 'Generate') {
		
		if ($this->input->post('strType') == 'rekap') {
			$arrReport = array();		
			$arrData['th'] = 'Data';		
			$arrData['setPrice'] = array('nilai_kontrak','prestasi_sebelum_(A)','%_sebelum_(A/DPP)', 'penerimaan_sebelum_(B)','%_sebelum_(b/dpp)','biaya_sebelum_(C)','%_sebelum_(c/dpp)','saldo_sebelum_(b-c)','prestasi_tahun_ini','%_tahun_ini_(a/dpp)','termin_yang_sudah_ditagihkan_(%)','penerimaan_tahun_ini(b)','%_(b/a)','biaya_tahun_ini','%_(c/a)','saldo_thn_ini','total_penerimaan','%_(b+b)/dpp','total_pengeluaran','%_(c+c)/dp','total_saldo');
			$arrData['hidden'] = array('id','total_nilai_kontrak','jumlah_prestasi_sebelum_tahun_ini','jumlah_penerimaan_sebelum_tahun_ini','jumlah_biaya_sebelum_tahun_ini','jumlah_sebelum_tahun_ini','jumlah_saldo_tahun_ini','jumlah_prestasi_tahun_ini','jumlah_penerimaan_tahun_ini','jumlah_biaya_tahun_ini','jumlah_saldo_tahun_ini','jumlah_total_penerimaan','jumlah_total_pengeluaran','jumlah_total_saldo');
			$arrData['bold'] = array('nilai_kontrak');
			$arrData['th_span'] = array(
				'tr_count' => 2,
				'fields' => [
					array(
						array('type'=>'row', 'count' => 2, 'field' => 'Kontrak'),
						array('type'=>'row', 'count' => 2, 'field' => 'Nilai_Kontrak'),
						array('type'=>'col', 'count' => 7, 'field' => date('Y',strtotime($this->input->post('txtDateEnd').'-1 year'))),
						array('type'=>'col', 'count' => 8, 'field' => date('Y',strtotime($this->input->post('txtDateEnd')))),
						array('type'=>'row', 'count' => 2, 'field' => 'Total_Penerimaan'),
						array('type'=>'row', 'count' => 2, 'field' => '%_(B+B)/DPP'),
						array('type'=>'row', 'count' => 2, 'field' => 'Total_Pengeluaran'),
						array('type'=>'row', 'count' => 2, 'field' => '%_(C+C)/DP'),
						array('type'=>'row', 'count' => 2, 'field' => 'Total_Saldo'),
					),
					array(
						array('field' => 'Prestasi (A)'),
						array('field' => '% (A/DPP)'),
						array('field' => 'Penerimaan (B)'),
						array('field' => '% (B/DPP)'),
						array('field' => 'Biaya (C)'),
						array('field' => '% (C/A)'),
						array('field' => 'Saldo (B-C)'),

						array('field' => 'Prestasi (A)'),
						array('field' => '% (A/DPP)'),
						array('field' => 'Termin Yang Sudah Ditagihkan'),
						array('field' => 'Penerimaan (B)'),
						array('field' => '% (B/DPP)'),
						array('field' => 'Biaya (C)'),
						array('field' => '% (C/A)'),
						array('field' => 'Saldo (B-C)'),
					)
				]
			);		
			$arrData['th_subtotal'] = '';
			$arrData['th_separator'] = 'nilai_kontrak';
			
			$arrReport = $this->Mreport->reportPenerimaanPengeluaranProyek('rekap',$this->input->post('txtDateEnd'));		
			$arrData['strPageMode'] = 'Laporan Penerimaan - Pengeluaran Proyek';
			$arrData['dataPrint'] = '';
			$arrData['dataPrintFooter'] = '<div class="hidden">
	                    <div class="col-xs-4 text-center">
	                        Dibuat Oleh
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        Disetujui Oleh
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        Diterima Oleh
	                    </div>
	                </div>
	                <div class="hidden" style="margin-top:100px;">
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                </div>';       
	        
			if(!empty($arrReport)) {
				$arrData['th'] = array();
				foreach (array_keys($arrReport[0]) as $key => $e) {
					if(!in_array($e, $arrData['hidden'])) {
						array_push($arrData['th'], $e);
					}
				}
			}
		}

		if ($this->input->post('strType') == 'detail') {			
			$arrReport = array();		
			$arrData['th'] = 'Data';		
			$arrData['setPrice'] = array('nominal');
			$arrData['hidden'] = array('urutan');
			$arrData['bold'] = array('kont_name');
			$arrData['th_span'] = array(
				'tr_count' => 1,
				'fields' => [					
					array(
						array('field' => 'Kontrak'),
						array('field' => 'Jenis'),
						array('field' => 'Tanggal'),
						array('field' => 'Kode'),
						array('field' => 'Nominal (Rp)')						
					)
				]
			);		
			$arrData['th_subtotal'] = '';
			$arrData['th_separator'] = 'nilai_kontrak';
			
			$arrReport = $this->Mreport->reportPenerimaanPengeluaranProyek('detail',$this->input->post('txtDateEnd'),$this->input->post('idKontrak'), $this->input->post('jenis'));		
			$arrData['strPageMode'] = 'Laporan Penerimaan - Pengeluaran Proyek';
			$arrData['dataPrint'] = '';
			$arrData['dataPrintFooter'] = '<div class="hidden">
	                    <div class="col-xs-4 text-center">
	                        Dibuat Oleh
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        Disetujui Oleh
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        Diterima Oleh
	                    </div>
	                </div>
	                <div class="hidden" style="margin-top:100px;">
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                    <div class="col-xs-4 text-center">
	                        (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
	                    </div>
	                </div>';       
	        
			if(!empty($arrReport)) {
				$arrData['th'] = array();
				foreach (array_keys($arrReport[0]) as $key => $e) {
					if(!in_array($e, $arrData['hidden'])) {
						array_push($arrData['th'], $e);
					}
				}
			}
		}

		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resultpenerimaanpengeluaranproyek',
			'arrReport' => $arrReport,
			'arrProject' => $arrProject,
			'idKontrak' => $this->input->post('idKontrak'),
			'jenis' => $this->input->post('jenis'),
			'strType' => $this->input->post('strType'),
			'strPageTitle' => "Laporan Penerimaan - Pengeluaran Proyek",
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/penerimaanpengeluaranproyek',
			'arrAccount' => $arrAccount,
			'arrProject' => $arrProject,
			'strPageTitle' => "Laporan Penerimaan - Pengeluaran Proyek",
		));
	}
}

public function hutangbelumdifakturkan()
{
	$this->_getMenuHelpContent(266,true,'adminpage');
	$this->load->model('Mkontrak');	
	$arrProject = $this->Mkontrak->getActiveItems(0,1000);
	if($this->input->post('smtProcessType') == 'Generate') {		
		$arrReport = array();
		$arrData = $this->input->post();		
		$arrData['th'] = 'Data';
		$arrData['setPrice'] = array('SALDO AWAL','debet','kredit','SALDO AKHIR','saldo');
		$arrData['hidden'] = array('@nomorkontrakbefore := nomorkontrak');		
		if ($this->input->post('intProjectID') !== "%") array_push($arrData['hidden'], 'kont_name');
		if ($this->input->post('intMode') == 1) array_push($arrData['hidden'], 'supp_name');		
		$arrData['th_subtotal'] = '';
		$arrData['th_separator'] = '';		
		$arrReport = $this->Mreport->reportHutangBelumDifakturkan($this->input->post('txtDateStart'), $this->input->post('txtDateEnd'), $this->input->post('intProjectID'), $this->input->post('intMode'));		
		// echo var_dump(array_keys($arrReport[0]));
		// exit();
		$arrData['strPageMode'] = 'Laporan Hutang Belum difakturkan';
		$arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('txtDateStart'),'d F Y')." s/d ". formatDate2($this->input->post('txtDateEnd'),'d F Y')."<br />Proyek : ".$arrReport[0]['kont_name'];
				
		if(!empty($arrReport)) {
			$arrData['th'] = array();
			foreach (array_keys($arrReport[0]) as $key => $e) {
				if(!in_array($e, $arrData['hidden'])) {
					array_push($arrData['th'], $e);
				}
			}
		}

		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'report/resulthutangbelumdifakturkan',
			'arrReport' => $arrReport,
			'arrProject' => $arrProject,
			'strStartDate' => $this->input->post('txtDateStart'),
			'strDateEnd' => $this->input->post('txtDateEnd'),
			'intProjectID' => $this->input->post('intProjectID'),
			'intMode' => $this->input->post('intMode'),
			'strPageTitle' => 'Laporan Hutang Belum difakturkan'
		),$arrData));
	} else {
		$this->load->view('sia',array(
		    'strViewFile' => 'report/hutangbelumdifakturkan',
			'arrProject' => $arrProject,
			'strPageTitle' => "Laporan Hutang Belum difakturkan",
		));
	}
}

}

/* End of File */