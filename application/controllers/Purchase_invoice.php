<?php
/*
PUBLIC FUNCTION:
- index(strMode)
- browse(intPage)
- view(intInvoiceID)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase_invoice extends JW_Controller {

private $_CI;

public function __construct() {
    parent::__construct();
    $this->_CI =& get_instance();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
    
    $this->load->model('Mpurchaseinvoice');
    $this->load->model('Mpurchaseinvoiceitem');     
    $this->load->model('Mdaftarpembayaranhutangitem');        
    $this->load->model('Macceptanceitem');
    $this->load->model('Mdownpayment');
    $this->load->model('Mpurchaseinvoicehistory');
    
    $this->_getMenuHelpContent(225,true,'adminpage');    
}

public function index($strMode = 'normal') {
    // WARNING! Don't change the following steps
    $bolKanvasAvlb = $this->admlinklist->getAvailability(54,$this->session->userdata('strAdminPriviledge'));

    // if($this->input->post('smtUpdateInvoice') != '' || $this->input->post('smtMakeInvoice') != '') { 
    // }

    if($this->input->post('smtMakeInvoice') != '') { // Make Invoice

        $dpp = $this->input->post('totalDpp');
        $nilaippn = $this->input->post('amountPPN');
        $beforePph = $dpp+$nilaippn;
        $nilaiPph = $dpp*($this->input->post('txtPPH')/100);

        $tambahan = 0;
        $tot_tambahan = $this->input->post('total_item_tambahan');

        if(!empty($this->input->post('total_item_tambahan'))){
            for($i = 0; $i < $tot_tambahan; $i++){
                if(!empty($this->input->post('amount_plus'.$i))){
                    $new_tot = $this->input->post('amount_plus'.$i);
                    // $new_tot = str_replace(['.',','],['','.'],$tot);
                    $tambahan = $tambahan+$new_tot;
                }
            }
        }

        $dpp = $dpp + $nilaippn - ($nilaiPph) + $tambahan;        

        $arrPurchaseInvoice = array(
            'idOrderPembelian' => $this->input->post('idOrderPembelian'),
            'idSupplier' => $this->input->post('idSupplier'),
            'cDate' => $this->input->post('txtDateBuat'),
            'codePurchaseInvoice' => generateTransactionCode($this->input->post('txtDateBuat'),'','purchase_invoice',TRUE),
            'idAcceptance' => $this->input->post('idAcceptance'),
            'intDiscount' => $this->input->post('dsc4'),
            'intPpn' => $this->input->post('txtPpn'),
            'intPph' => $this->input->post('txtPPH'),
            'cBy' =>  $this->session->userdata('strAdminID'),
            'intStatus' => 2,
            'intSubTotal' => $this->input->post('subTotalNoTax'),
            'intGrandTotalBeforePPH'=> $beforePph,
            'intGrandTotal'=> $dpp,
            'intFinalTotal'=> $this->input->post('amountTotalPembulatan'),
        );

        $ppn_dp = $this->input->post('total_ppn_dp');
        $pph_dp = $this->input->post('total_pph_dp');

        $dp = floatval($this->input->post('totalDownPayment'));
        if($dp < 0) $dp = 0;        

        $pembulatan = $this->input->post('amountPembulatan');
        if($pembulatan == null || $pembulatan == ''){
            $pembulatan = 0;
        }

        $idsubkon = $this->input->post('idsubkon');

        $this->_CI->db->trans_start();
        $intPurchaseInvoiceId = $this->Mpurchaseinvoice->addBaru($arrPurchaseInvoice['codePurchaseInvoice'],$arrPurchaseInvoice['idOrderPembelian'], $arrPurchaseInvoice['idSupplier'], $arrPurchaseInvoice['intDiscount'], '', $arrPurchaseInvoice['intPpn'], $arrPurchaseInvoice['cBy'], $arrPurchaseInvoice['cDate'], '','',0,$arrPurchaseInvoice['intGrandTotalBeforePPH'], $arrPurchaseInvoice['intGrandTotal'], $arrPurchaseInvoice['intFinalTotal'], $arrPurchaseInvoice['idAcceptance'], $arrPurchaseInvoice['intStatus'], $arrPurchaseInvoice['intPph'], $tambahan, $arrPurchaseInvoice['intSubTotal'], $dp, $pembulatan, $idsubkon, $ppn_dp, $pph_dp);

        $subTotalWithDisc = $this->input->post('subTotalWithDisc');
        $taxAmount = 0;
        if($arrPurchaseInvoice['intPpn'] != 0)
            $taxAmount = $this->input->post('subTotalWithDisc')/10;
        $hutangUsaha = $subTotalWithDisc+$taxAmount-$dp;
        
        $this->load->model('Mjournal');
        $this->Mjournal->insertHutangPI($intPurchaseInvoiceId);
        if($dp > 0) $this->Mjournal->insertHutangUMSPI($intPurchaseInvoiceId);
        // $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),256,'PI','Purchase_invoice',$intPurchaseInvoiceId,$subTotalWithDisc,'Subtotal(non ppn)');
        // if($arrPurchaseInvoice['intPpn'] != 0)
        //     $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),192,'PI','Purchase_invoice',$intPurchaseInvoiceId,$taxAmount,'ppn');
        // $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),188,'PI','Purchase_invoice',$intPurchaseInvoiceId,-$hutangUsaha,'Hutang usaha');
        // if($dp != 0)
        //     $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),171,'PI','Purchase_invoice',$intPurchaseInvoiceId,-$dp,'DP');

        //$this->Mjournal->insertHutang($arrPurchaseInvoice['idSupplier'],$intPurchaseInvoiceId,$arrPurchaseInvoice['codePurchaseInvoice'],'PI',$this->input->post('txtDateBuat'),$hutangUsaha);

        //echo $arrPurchaseInvoice['codePurchaseInvoice']." ".$arrPurchaseInvoice['idOrderPembelian']." ". $arrPurchaseInvoice['idSupplier']." ". $arrPurchaseInvoice['intDiscount']." ". ''." ". $arrPurchaseInvoice['intPpn']." ". $arrPurchaseInvoice['cBy']." ". $arrPurchaseInvoice['cDate']." ". ''." ".''." 0 ".$arrPurchaseInvoice['intGrandTotal']." ". $arrPurchaseInvoice['idAcceptance']." ". $arrPurchaseInvoice['intStatus']." ". $arrPurchaseInvoice['intPph']." ". $tambahan." ". $arrPurchaseInvoice['intSubTotal'];

        $i=0;$j=0;
        $totItem = (int) $this->input->post('totalItem');
        $pengkali=0;
        //Bawah add detailnya
        while($j<$totItem){
            $bahan = $this->input->post('bahan'.$j);
            $qty_bayar = (Float)$this->input->post('qtybayar'.$j);
            // $qty_bayar = (Float) str_replace(',','.',$this->input->post('qtybayar'.$j)) ;
            $harga_satuan = $this->input->post('hargasatuan'.$j);
            $sum_perbarang = $this->input->post('sum_barang'.$j);
            if($this->input->post('qtypengkali'.$j) == '' || $this->input->post('qtypengkali'.$j) == null){
                $qty_pengkali = 1;
            }
            else{
                $qty_pengkali = $this->input->post('qtypengkali'.$j);
            }
            $terpi = $this->input->post('terpi'.$j);
            $ai_id = $this->input->post('ai_id'.$j);
            $terpi_baru = $this->Mpurchaseinvoice->countAllPIItemByItemAndOPId($bahan,$arrPurchaseInvoice['idAcceptance']);
                        
            $this->Mpurchaseinvoiceitem->addBaru($intPurchaseInvoiceId, $ai_id, $bahan, '',$qty_bayar, $qty_pengkali, $harga_satuan, $sum_perbarang,$arrPurchaseInvoice['cBy'], $arrPurchaseInvoice['cDate']);
            $this->Mpurchaseinvoiceitem->updateTerPI($ai_id, $terpi_baru[0]['jumlah']);
            
            $i++;
            $j++;
        }

        //Bawah untuk add ke history dp jika ada
        $total_dp = (int)$this->input->post('jumlahDp');
        
        if($dp > 0){
            for($i=0;$i<$total_dp;$i++){
                $dipakai = $this->input->post('untuk_dp'.$i);
                $kp = ($this->input->post('id_kp'.$i) == 'undefined' ) ? 0 : $this->input->post('id_kp'.$i);
                $id_dp = $this->input->post('id_dp'.$i);
                $supplier = $this->input->post('idSupplier');
                $amount_awal = $this->input->post('amount_dp'.$i);

                if($amount_awal-$dipakai == 0){
                    $this->Mdownpayment->updateStatusToFinish($id_dp);
                }
                // $dipakai = str_replace(',','.',$dipakai);
                $this->Mpurchaseinvoice->addHistory($dipakai, $intPurchaseInvoiceId, $kp, $id_dp, $supplier);
            }
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Purchase invoice dengan kode '.$arrPurchaseInvoice['codePurchaseInvoice'].' berhasil dibuat');
        redirect('purchase_invoice/view/'.$intPurchaseInvoiceId);
        
        // $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoicemade'));
        // if(!empty($_POST['txtPayment'])) $this->session->set_flashdata('txtPayment', $_POST['txtPayment']);
        // if(!empty($_POST['txtPaymentChange'])) $this->session->set_flashdata('txtPaymentChange', $_POST['txtPaymentChange']);
        // if($strMode == 'cashier' || $this->input->post('smtMakeInvoice') == 'Make Cashier Invoice') redirect('purchaseinvoice/view/'.$intInvoiceID.'?print=true&process=cashier');
        // else redirect('purchaseinvoice/view/'.$intInvoiceID);
    
    }

    if($this->input->post('sementaraPurchase') != ''){
        $total = $this->input->post('totalInput');
        $tanggal = $this->input->post('tanggalInput');
        $supplier = $this->input->post('supplierInput');
        $kode = $this->input->post('kodeInput');

        $this->load->model('Mpurchaseinvoice');
        $this->_CI->db->trans_start();
        $id_supplier = $this->Mpurchaseinvoice->getSupplierByName($supplier);
        $this->Mpurchaseinvoice->addDataSementara($kode, $id_supplier[0]['id'], $total, $tanggal);
        $this->_CI->db->trans_start();
        $this->session->set_flashdata('strMessage','Purchase Invoice telah dibuat');
        redirect('purchaseinvoice/browse');
    }

    if($_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 2 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 1){
        $this->load->model('Macceptance');
        $arrBPB = $this->Macceptance->getAllBPBData();
    }
    else{
        $this->load->model('Macceptance');
        $arrBPB = $this->Macceptance->getAllBPBData($_SESSION['strAdminID']);
    }

    $this->load->model('Mpurchaseinvoice');
    $arrPlusCost = $this->Mpurchaseinvoice->getAllPlusCostData();

    $this->load->view('sia',array(
        'strViewFile' => 'purchaseinvoice/add',
        'arrBPB' => $arrBPB,
        'arrPlusCost' => $arrPlusCost,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchaseinvoice-add')
    ));

}

// To display, edit and delete invoice
public function view($intID = 0) {
    # INIT
    $this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwsettingowner');

    if($this->input->post('subSave') != '' && $intID != '') {
        
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoiceupdated');

    }
    if($this->input->post('subDelete') != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
        //$this->Mpurchaseinvoiceitem->deleteByInvoiceID($intID);
        $this->_CI->db->trans_start();
        $this->Mpurchaseinvoice->deleteByID($this->input->post('purchaseInvoiceId'));
        
        $this->Mpurchaseinvoicehistory->editStatusPinvHis($this->input->post('purchaseInvoiceId'));
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoicedeleted'));
        redirect('purchase_invoice/browse');
    }
    
    if($this->input->post('editPurchaseInvoiceItem') != ''){
        $jumlah_edit = $this->input->post('jumlah_row');
        $new_subtotal = 0;
        $this->_CI->db->trans_start();
        for($i = 0; $i<$jumlah_edit; $i++){
            $new_subtotal_item = 0;
            $pengkali_baru = $this->input->post('editPengkali'.$i);
            $harga_bayar = $this->input->post('harga_bayar'.$i);
            $bahan = $this->input->post('bahan'.$i);

            $idPI = $this->input->post('purchaseInvoiceId');
            $idItemPI = $this->input->post('id'.$i);
            $accId = $this->input->post('acceptanceId');
            $accItemId = $this->input->post('acceptanceItemId');

            //update ke terpi dan update di itemnya
            $new_qty = $this->input->post('qty'.$i);
            $new_pengkali = $this->input->post('editPengkali'.$i);
            $new_harga_bayar = $this->input->post('harga_bayar'.$i);

            $new_subtotal += $new_qty*$new_pengkali*$new_harga_bayar;
            $new_subtotal_item = $new_qty*$new_pengkali*$new_harga_bayar;

            $this->Mpurchaseinvoiceitem->updatePurchaseInvoiceItemQty($pengkali_baru,$idItemPI,$new_subtotal_item);

            // $jumlah_terpi_baru = $this->Mpurchaseinvoice->countAllPIItemByItemAndOPId($bahan,$accId);
            // $this->Macceptanceitem->updateTerPi($accItemId, $jumlah_terpi_baru[0]['jumlah']);//update terlebih dahulu

        }

        
        $tambahan = $this->input->post('tambahan');
        $diskon = $this->input->post('diskon');
        $dp = $this->input->post('dp');
        $tax = $this->input->post('tax');
        $pph = $this->input->post('pph');
        $pembulatan = $this->input->post('pembulatan');

        $dpp = ($new_subtotal-$diskon)-$dp;        
        $nilai_pph = ($dpp*$pph)/100;
        $nilai_ppn = ($dpp*$tax)/100;
        $grand_total = $dpp + $nilai_ppn - $nilai_pph + $tambahan;
        // $final_total = ((($nilai_ppn+$dpp)+$tambahan)-$nilai_pph)+$pembulatan;
        $final_total = $grand_total + $pembulatan;
        $before_pph = (($nilai_ppn+$dpp)+$tambahan)+$pembulatan;
        // $pembulatan = str_replace(',','.',$pembulatan);
        $this->Mpurchaseinvoice->updateDataKeuangan($new_subtotal, $final_total, $grand_total, $before_pph, $idPI, $pembulatan);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Purchase Invoice berhasil di update');
        redirect('purchase_invoice/view/'.$idPI);
        
    }

    if($this->input->post('saveEdit') != ''){

        $idPI = $this->input->post('purchaseInvoiceId');
        $this->_CI->db->trans_start();
        $this->Mpurchaseinvoice->editPIStatus($idPI);

        $date = date('Y/m/d H:i:s');
        $this->Mpurchaseinvoice->updateJamSave($idPI, $date);
        $this->Mpurchaseinvoice->updateOrangSave($idPI, $_SESSION['strAdminID']);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Updated status PI');
        redirect('purchase_invoice/browse');
    }

    if($this->input->post('unsaveEdit') != ''){
        $idPI = $this->input->post('purchaseInvoiceId');
        $date = date('Y/m/d H:i:s');
        $this->Mpurchaseinvoice->uneditSaveStatus($idPI, $date, $_SESSION['strAdminID']);

        $this->session->set_flashdata('strMessage','Updated status PI');
        redirect('purchase_invoice/browse');
    }

    $arrPurchasedInvoice = $this->Mpurchaseinvoice->getAllPurchaseInvoiceById($intID);
    $arrPurchasedInvoiceItem = $this->Mpurchaseinvoice->getAllPurchaseInvoiceItemById($arrPurchasedInvoice[0]['id']);
    $checkPiInDph = $this->Mdaftarpembayaranhutangitem->checkPiInDph($intID);
    $arrPurchasedInvoice['purchinvo_rawstatus'] = $arrPurchasedInvoice['pinv_status'];
    $bolEditable = $this->Mpurchaseinvoice->checkEditablePI($_SESSION['strAdminID'],$arrPurchasedInvoice[0]['idSubKontrak']);
    $arrHistory = $this->Mpurchaseinvoice->traceHistory($intID);
    $arrInvoiceData = $this->Mpurchaseinvoice->getItemByID($intID);
    if(!empty($arrInvoiceData)) {
        $arrInvoiceData['invo_rawstatus'] = $arrInvoiceData['invo_status'];
        $arrInvoiceData['invo_status'] = translateDataIntoHTMLStatements(
            array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
            $arrInvoiceData['invo_status']);
        $intID = $arrInvoiceData['id'];
        $temp=explode(' ',$arrInvoiceData['invo_date']);
        $arrInvoiceData['date']=$temp[0];

        // Load the invoice item
        $arrInvoiceItem = $this->Mpurchaseinvoiceitem->getItemsByInvoiceID($intID);
        $intInvoiceTotal = 0;
        $tambahan = array();
        $countertambahan = 0;
        $this->load->model('Munit');
        for($i = 0; $i < count($arrInvoiceItem); $i++) {
            $txtDiscount1 = $arrInvoiceItem[$i]['invi_discount1'];
            $txtDiscount2 = $arrInvoiceItem[$i]['invi_discount2'];
            $txtDiscount3 = $arrInvoiceItem[$i]['invi_discount3'];
            $intQty1 = $arrInvoiceItem[$i]['invi_quantity1'];
            $intQty2 = $arrInvoiceItem[$i]['invi_quantity2'];
            $intQty3 = $arrInvoiceItem[$i]['invi_quantity3'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceItem[$i]['invi_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrInvoiceItem[$i]['invi_price'];
            $intInvoiceTotalTemp = ((float) $intPrice * (float) $intQty1) + ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2) + ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * $intQty3);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount1 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount2 / 100);
            $intInvoiceTotalTemp = $intInvoiceTotalTemp - ($intInvoiceTotalTemp * $txtDiscount3 / 100);
            $intInvoiceTotal += $intInvoiceTotalTemp;
            $arrInvoiceItem[$i]['invi_subtotal'] = $intInvoiceTotalTemp;
            $arrInvoiceItem[$i]['invi_conv1'] = $intConv1;
            $arrInvoiceItem[$i]['invi_conv2'] = $intConv2;
            $arrInvoiceItem[$i]['invi_conv3'] = $intConv3;
            if(!empty($tambahan)) {
                $flag = 0;
                for($j = 0; $j < count($tambahan); $j++) {
                    if($tambahan[$j]['id'] == $arrInvoiceItem[$i]['invi_product_id']) {
                        $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $flag = 1;
                        break;
                    }
                }
                if($flag == 0) {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['invi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            } else {
                $tambahan[$countertambahan]['id'] = $arrInvoiceItem[$i]['invi_product_id'];
                $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                $countertambahan++;
            }
        }
        $pengurangan = 0;
        $arrKreditCustomer = $this->Mpurchaseinvoice->getCustomerCredit($arrInvoiceData['cust_id']);
        for($ii = 0; $ii < count($arrKreditCustomer); $ii++) {
            $pengurangan += (float) $arrKreditCustomer[$ii]['invo_grandtotal'];
        }
        if(is_numeric($pengurangan) && $pengurangan>0) {
            $arrInvoiceData['cust_limitkredit'] = (float)$arrInvoiceData['cust_limitkredit']-(float)$pengurangan+(float)$arrInvoiceData['invo_grandtotal'];
        }
        $InvoiceTotal = (float) $intInvoiceTotal - (float) $arrInvoiceData['invo_discount'];
        $arrInvoiceData['subtotal_discounted'] = $InvoiceTotal;
        $intInvoiceGrandTotal = (float) $InvoiceTotal + ($InvoiceTotal * (float) $arrInvoiceData['invo_tax'] / 100);
        $arrInvoiceItemBonus = $this->Mpurchaseinvoiceitem->getBonusItemsByInvoiceID($intID);
        if(!empty($arrInvoiceItemBonus)) {
            for($i = 0; $i < count($arrInvoiceItemBonus); $i++) {
                $arrInvoiceItemBonus[$i]['strName'] = $arrInvoiceItemBonus[$i]['invi_description'].'|'.$arrInvoiceItemBonus[$i]['proc_title'];
                $intQty1 = $arrInvoiceItemBonus[$i]['invi_quantity1'];
                $intQty2 = $arrInvoiceItemBonus[$i]['invi_quantity2'];
                $intQty3 = $arrInvoiceItemBonus[$i]['invi_quantity3'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrInvoiceItemBonus[$i]['invi_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrInvoiceItemBonus[$i]['invi_conv1'] = $intConv1;
                $arrInvoiceItemBonus[$i]['invi_conv2'] = $intConv2;
                $arrInvoiceItemBonus[$i]['invi_conv3'] = $intConv3;
                if(!empty($tambahan)) {
                    $flag = 0;
                    for($j = 0; $j < count($tambahan); $j++) {
                        if($tambahan[$j]['id'] == $arrInvoiceItemBonus[$i]['invi_product_id']) {
                            $tambahan[$j]['qty'] = $tambahan[$j]['qty'] + ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                            $flag = 1;
                            break;
                        }
                    }
                    if($flag == 0) {
                        $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['invi_product_id'];
                        $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                        $countertambahan++;
                    }
                } else {
                    $tambahan[$countertambahan]['id'] = $arrInvoiceItemBonus[$i]['invi_product_id'];
                    $tambahan[$countertambahan]['qty'] = ($intQty1 * $intConv1) + ($intQty2 * $intConv2) + ($intQty3 * $intConv3);
                    $countertambahan++;
                }
            }
        }
        $tambahanId = '';
        $tambahanQty = '';
        if($arrInvoiceData['invo_soid']!='0'){
            $this->load->model('Mpurchaseinvoiceorderitem');
            $masterStock=$this->Mpurchaseinvoiceorderitem->getItemsByInvoiceID($arrInvoiceData['invo_soid']);
            $masterStockBonus=$this->Mpurchaseinvoiceorderitem->getBonusItemsByInvoiceID($arrInvoiceData['invo_soid']);
        }
        for($i = 0;$i < count($tambahan); $i++) {
            $tambahanId = $tambahanId.$tambahan[$i]['id'].'-';
            $tambahanQty = $tambahanQty.$tambahan[$i]['qty'].'-';
            for($j = 0; $j < count($arrInvoiceItem); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItem[$j]['invi_product_id']) {
                    if($arrInvoiceData['invo_soid']=='0'){
                        $stock = countStock($arrInvoiceItem[$j]['invi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                        $arrInvoiceItem[$j]['invi_max1'] = ($stock / $arrInvoiceItem[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max1'] * $arrInvoiceItem[$j]['invi_conv1']);
                        $arrInvoiceItem[$j]['invi_max2'] = ($stock / $arrInvoiceItem[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max2'] * $arrInvoiceItem[$j]['invi_conv2']);
                        $arrInvoiceItem[$j]['invi_max3'] = ($stock / $arrInvoiceItem[$j]['invi_conv3']);
                        break;
                    }else{
                        //sini
                        $stock=0;
                        foreach($masterStock as $e) {
                            if($e['inoi_product_id']== $arrInvoiceItem[$j]['invi_product_id']){
                                $stock+=($e['inoi_quantity1']-$e['inoi_selled1'])*$arrInvoiceItem[$j]['invi_conv1'];
                                $stock+=($e['inoi_quantity2']-$e['inoi_selled2'])*$arrInvoiceItem[$j]['invi_conv2'];
                                $stock+=($e['inoi_quantity3']-$e['inoi_selled3'])*$arrInvoiceItem[$j]['invi_conv3'];
                                $stock+=$tambahan[$i]['qty'];
                            }
                        }
                        $arrInvoiceItem[$j]['invi_max1'] = ($stock / $arrInvoiceItem[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max1'] * $arrInvoiceItem[$j]['invi_conv1']);
                        $arrInvoiceItem[$j]['invi_max2'] = ($stock / $arrInvoiceItem[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItem[$j]['invi_max2'] * $arrInvoiceItem[$j]['invi_conv2']);
                        $arrInvoiceItem[$j]['invi_max3'] = ($stock / $arrInvoiceItem[$j]['invi_conv3']);
                    }

                }
            }
            for($j = 0;$j < count($arrInvoiceItemBonus); $j++) {
                if($tambahan[$i]['id'] == $arrInvoiceItemBonus[$j]['invi_product_id']) {
                    if($arrInvoiceData['invo_soid']=='0'){
                        $stock = countStock($arrInvoiceItemBonus[$j]['invi_product_id'],$arrInvoiceData['ware_id'])+$tambahan[$i]['qty'];
                        $arrInvoiceItemBonus[$j]['invi_max1'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max1'] * $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $arrInvoiceItemBonus[$j]['invi_max2'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max2'] * $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $arrInvoiceItemBonus[$j]['invi_max3'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv3']);
                        break;
                    }else{
                        //sini
                        $stock=0;
                        foreach($masterStockBonus as $e) {
                            if($e['inoi_product_id']== $arrInvoiceItemBonus[$j]['invi_product_id']){
                                $stock+=($e['inoi_quantity1']-$e['inoi_selled1'])*$arrInvoiceItemBonus[$j]['invi_conv1'];
                                $stock+=($e['inoi_quantity2']-$e['inoi_selled2'])*$arrInvoiceItemBonus[$j]['invi_conv2'];
                                $stock+=($e['inoi_quantity3']-$e['inoi_selled3'])*$arrInvoiceItemBonus[$j]['invi_conv3'];
                                $stock+=$tambahan[$i]['qty'];
                            }
                        }
                        $arrInvoiceItemBonus[$j]['invi_max1'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max1'] * $arrInvoiceItemBonus[$j]['invi_conv1']);
                        $arrInvoiceItemBonus[$j]['invi_max2'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $stock = $stock - ($arrInvoiceItemBonus[$j]['invi_max2'] * $arrInvoiceItemBonus[$j]['invi_conv2']);
                        $arrInvoiceItemBonus[$j]['invi_max3'] = ($stock / $arrInvoiceItemBonus[$j]['invi_conv3']);
                    }
                }
            }
        }
        $tambahanId = $tambahanId.'0';
        $tambahanQty = $tambahanQty.'0';
        $this->load->model('Mpotongan');
        $arrPotongan= $this->Mpotongan->getAllPotonganByInvoiceID($intID);

    } else {
        $arrInvoiceItem = array(); $arrInvoiceList = array(); $arrReturnItem = array(); $intInvoiceTotal = 0; $intInvoiceGrandTotal = 0;
    }

    $arrData = array(
        'arrInvoiceData' => $arrInvoiceData,
        'arrInvoiceItem' => $arrInvoiceItem,
        'intInvoiceID' => $intID,
        'bolEditable' => $bolEditable,
        'arrPurchasedInvoice' => $arrPurchasedInvoice[0],
        'arrPurchasedInvoiceItem' => $arrPurchasedInvoiceItem,
        'intInvoiceTotal' => $intInvoiceTotal,
        'arrInvoiceBonusItem' => $arrInvoiceItemBonus,
        'strTambahanId' => $tambahanId,
        'checkPiInDph' => $checkPiInDph,
        'strTambahanQty' => $tambahanQty,
        'intInvoiceGrandTotal' => $intInvoiceGrandTotal,
        'arrPotongan' => $arrPotongan,
        'arrHistory' => $arrHistory,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    $this->load->model('Mprintlog');

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->get('process') == 'cashier') && $intID != '') {
        if($this->input->get('process') == 'cashier') {
            if($this->session->flashdata('txtPayment') != '') $txtPayment = $this->session->flashdata('txtPayment');
            if($this->session->flashdata('txtPaymentChange') != '') $txtPaymentChange = $this->session->flashdata('txtPaymentChange');
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'),
                'dataPrint' => $this->Mpurchaseinvoice->getPrintDataByID($intID),
                'txtPayment' => !empty($txtPayment) ? $txtPayment : '',
                'txtPaymentChange' => !empty($txtPaymentChange) ? $txtPaymentChange : '',
            ), $arrData));
            $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'));

        } else {
            //$this->Mpurchaseinvoice->editAfterPrint($intID);
            $strLastTemplate = $this->Mprintlog->getLastTemplate('invoice', $intID);
            if(empty($strLastTemplate)) {
                if($this->input->post('subPrint') == 'Print + Surat Jalan') {
                    $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE').'_suratjalan';
                } else {
                    $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE');
                }
            }
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $strLastTemplate,
                'dataPrint' => $this->Mpurchaseinvoice->getPrintDataByID($intID),
            ), $arrData));
            $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $strLastTemplate);
        }

    }else if(($this->input->post('subPrintTax') != '') || ($this->input->get('print2') != '')) {//print e pajak
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoicetax',
            'dataPrint' => $this->Mpurchaseinvoice->getPrintDataByID($intID),
            'dateTax' => $date,
            'kodeTax' => $kode,
        ), $arrData));
        $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, 'invoicetax');

    } else {
        # Load all other invoice data the user has ever made
        $arrInvoiceList = $this->Mpurchaseinvoice->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'purchaseinvoice/view',
            'arrInvoiceList' => $arrInvoiceList,
            'arrPurchasedInvoice' => $arrPurchasedInvoice,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicestatus')
        ), $arrData, $this->admlinklist->getMenuPermission(43,$arrInvoiceData['invo_rawstatus'])));
    }
}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {
        $arrInvoice = $this->Mpurchaseinvoice->searchBy($this->input->post('txtSearchNoPI'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), -1);        
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('purchaseinvoice/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrInvoice) ? count($arrInvoice) : '0')." records).";
    } else {        
        $arrPagination['base_url'] = site_url("purchase_invoice/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpurchaseinvoice->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrInvoice = $this->Mpurchaseinvoice->getItemsBaru($intPage,$arrPagination['per_page']);
    }

    if($this->input->post('delPurchaseInvoice') != ''){
        $purchi_id = $this->input->post('delPurchaseInvoice');
        $acc_item_id = $this->input->post('accItemId');
        $acc_id = $this->input->post('accId'.$purchi_id.'');

        $count = $this->Macceptanceitem->getCount($acc_id);
        //Sewaktu delete update terpi semua jadi sesuai dengan yang di delete
        //Dibawah akan mengambil semua data jumlah barang dari purchase invoice
        $this->_CI->db->trans_start();
        for($i=0;$i<$count;$i++){
            $all_acit_id = $this->Macceptanceitem->getAllAccItem($acc_id);
            $qty_pi = $this->Mpurchaseinvoiceitem->getAllQtyPurchInv($purchi_id);
            $this->Mpurchaseinvoice->deleteByID($purchi_id);
            foreach($qty_pi as $pi){
                //Bawah fungsi delete dan update terPI
                $this->Mpurchaseinvoiceitem->deleteByID($pi['id']);
                //Bawah untuk update terpi
                if((float)$all_acit_id['acit_terpi'] > 0){
                    $new_terpi = (float)$all_acit_id['acit_terpi']-(float)$pi['pinvit_quantity'];
                    //$this->Mpurchaseinvoice->countAllPIItemByItemAndOPId($bahan,$acc_id);
                    $this->Macceptanceitem->updateTerPi($all_acit_id['id'], $new_terpi);
                }
            }
        }
        $this->_CI->db->trans_complete();
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'purchaseinvoice/browse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrInvoice' => $arrInvoice,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchNoPI'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-purchaseinvoice')
    ));
}

}

/* End of File */