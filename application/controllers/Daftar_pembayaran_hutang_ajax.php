<?php
/**
 * 
 */
class Daftar_pembayaran_hutang_ajax extends JW_Controller
{
		public function getOutstandingDataBySupp($intSuppID)
		{
			$this->load->model('Mdaftarpembayaranhutang');
			$arrOutstandingData = [];
			$arrPurchaseInvoiceData = $this->Mdaftarpembayaranhutang->getPurchaseInvoiceData($intSuppID);			
			$arrDebtData = $this->Mdaftarpembayaranhutang->getDebtData($intSuppID);			
			$arrDownpaymentData = $this->Mdaftarpembayaranhutang->getDownpaymentData($intSuppID);		
			
			if ($arrPurchaseInvoiceData) {

				#Markup Product List
				foreach ($arrPurchaseInvoiceData as $key => $value) {				
					$products[$key] = $this->Mdaftarpembayaranhutang->getProductList($value['id']);						
					$strProduct = "";
					if(!empty($products[$key])){
						foreach ($products[$key] as $value) {
							$strProduct .= $value['prod_title']."<br/>";
						}
					}
					// ((pinv_subtotal + (pinv_subtotal * pinv_tax/100)) + pinv_pembulatan) - ( pinv_subtotal * pinv_pph/100)
					$arrPurchaseInvoiceData[$key]['sisa'] = $arrPurchaseInvoiceData[$key]['pinv_sisa_bayar'];
					$arrPurchaseInvoiceData[$key]['product'] = $strProduct;										
					$arrPurchaseInvoiceData[$key]['date'] = formatDate2($arrPurchaseInvoiceData[$key]['cdate'],'d F Y');					
					$arrPurchaseInvoiceData[$key]['dpp'] = setPrice($arrPurchaseInvoiceData[$key]['pinv_grandtotal']);
					$arrPurchaseInvoiceData[$key]['ppn'] = $arrPurchaseInvoiceData[$key]['pinv_tax'];					
					$arrPurchaseInvoiceData[$key]['rencanabayar'] = $arrPurchaseInvoiceData[$key]['finaltotal'] - $arrPurchaseInvoiceData[$key]['pinv_terbayar'];					
					$arrPurchaseInvoiceData[$key]['raw_finaltotal'] = $arrPurchaseInvoiceData[$key]['pinv_finaltotal'];
					$arrPurchaseInvoiceData[$key]['raw_grandtotal'] = $arrPurchaseInvoiceData[$key]['pinv_grandtotal'];
					$arrPurchaseInvoiceData[$key]['raw_sisabayar'] = $arrPurchaseInvoiceData[$key]['pinv_finaltotal']-$arrPurchaseInvoiceData[$key]['pinv_terbayar'];
					$arrPurchaseInvoiceData[$key]['finaltotal'] = setPrice($arrPurchaseInvoiceData[$key]['pinv_finaltotal']); #sebelumnya pakai raw_total
					$arrPurchaseInvoiceData[$key]['grandtotal'] = setPrice($arrPurchaseInvoiceData[$key]['pinv_totalbeforepph']); #sebelumnya pakai raw_grandtotal
					$arrPurchaseInvoiceData[$key]['pinv_terbayar'] = setPrice($arrPurchaseInvoiceData[$key]['pinv_terbayar']);					
					$arrPurchaseInvoiceData[$key]['status'] = statusDPH($arrPurchaseInvoiceData[$key]['cdate']);

				}	

				foreach ($arrPurchaseInvoiceData as $key => $value) {						
					$arrOutstandingData[] = array(
						'id' => $value['id'],
			            'code' => $value['pinv_code'],
			            'supp_name' => $value['supp_name'],
			            'product' => $value['product'],
			            'type' => 'PI',
			            'dpp' => $value['dpp'],
			            'ppn' => $value['ppn'],
			            'grandtotal' => $value['grandtotal'],
			            'pph' => $value['pinv_pph'],
			            'total' => $value['finaltotal'],
			            'terbayar' => $value['pinv_terbayar'],
			            'sisa' => $value['sisa'],
			            'cdate' => formatDate2($value['cdate'],'d F Y'),
			            'rencanabayar' => $value['rencanabayar'],
			            'status' => $value['status'],
			            'raw_total' => $value['raw_sisabayar'] #sebelumnya pakai raw_finaltotal
					);
			 	}	
			}

			if (!empty($arrDebtData)) {
				foreach ($arrDebtData as $key => $value) {
					$arrOutstandingData[] = array(
						'id' => $value['id'],
			            'code' => $value['txou_code'],
			            'product' => "Nota Kredit Supplier",
			            'type' => 'NKS',
			            'dpp' => setPrice($value['txod_amount']),
			            'ppn' => $value['txod_ppn'],
			            'supp_name' => $value['supp_name'],
			            'grandtotal' => setPrice($value['txod_amount'] + ($value['txod_amount'] * $value['txod_ppn']/100)),
			            'pph' => $value['txod_pph'],
			            'total' => setPrice(($value['txod_amount'] + ($value['txod_amount'] * $value['txod_ppn']/100)) - ($value['txod_amount'] * $value['txod_pph']/100)),
			            'terbayar' => $value['txod_terbayar'],
			            'sisa' => $value['txod_sisa_bayar'],
			            'cdate' => formatDate2($value['txou_date'], "d F Y"),
			            'rencanabayar' => 0,
			            'status' => "-",
			            'raw_total' => ($value['txod_amount'] + ($value['txod_amount'] * $value['txod_ppn']/100)) - ($value['txod_amount'] * $value['txod_pph']/100)
					);
				}
			}
			
			if (!empty($arrDownpaymentData)){
				foreach ($arrDownpaymentData as $key => $value) {
					$arrOutstandingData[] = array(
						'id' => $value['id'],
			            'code' => $value['dpay_code'],
			            'product' => "Downpayment",
			            'type' => 'DP',
			            'supp_name' => $value['supp_name'],
			            'dpp' => setPrice($value['dpay_amount']),
			            'ppn' => 0,
			            'grandtotal' => setPrice($value['dpay_amount']),
			            'pph' => 0,
			            'total' => setPrice($value['dpay_amount']),
			            'terbayar' => setPrice($value['dpay_terbayar']),
			            'sisa' => $value['dpay_sisa_bayar'],
			            'cdate' => formatDate2($value['dpay_date'], "d F Y"),
			            'rencanabayar' => ($value['dpay_sisa_bayar']),
			            'status' => "-",
			            'raw_total' => $value['dpay_amount_final']-$value['dpay_terbayar']
					);
				}
			} 	

			return $this->output->set_content_type('application/json')
			->set_output(json_encode($arrOutstandingData));
		}
}