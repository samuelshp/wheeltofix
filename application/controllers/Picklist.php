<?php

class Picklist extends JW_Controller{

private $_CI;

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();
    $this->_CI =& get_instance();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mpicklist');
    $this->load->model('Minvoiceorder');
    $this->load->model('Mpicklistitem');

    $this->_getMenuHelpContent(263,true,'adminpage');
}

public function browse($intPage = 0) {
    if($this->input->post('plSearch') != '') {
        $strPage = '';
        $arrPickListData = $this->Mpicklist->getSearch($this->input->post('txtSOID'), $this->input->post('txtPickListCode'), $this->input->post('txtDate'), $intPage, 1000);

        $strBrowseMode = '<a href="'.site_url('picklist/browse', NULL, FALSE).'"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</button></a>';
        $this->_arrPickListData['strMessage'] = "Search result (".(!empty($arrPickListData) ? count($arrPickListData) : '0')." records).";
    } else {
        $arrPagination['base_url'] = site_url("picklist/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpicklist->getCountData();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['page_query_string'] = TRUE;
        $strBrowseMode = '';
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();    
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        $arrData = $this->Mpicklist->getSearch($this->input->post('txtSOID'), $this->input->post('txtPickListCode'), $this->input->post('txtDate'),$intPage, $arrPagination['per_page']);
        $arrPickListData = $this->Mpicklist->getAllPickList();
    }    

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'picklist/browse',
        'strPage' => $strPage,
        'arrPickList' => $arrPickListData,
        'strBrowseMode' => $strBrowseMode,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title')
    ), $this->admlinklist->getMenuPermission(263,$arrPickListData['pcls_status'])));
}

public function add() {
    if($this->input->post('smtMakePickList') != '') {
        $arrData = array(
            'intSOID' => $this->input->post('intSOID'),
            'intWarehouseID' => $this->input->post('intWarehouseID'),
            'intStatus' => 3,
            'txtDate' => $this->input->post('txtDate')
        );

        $this->_CI->db->trans_start();
        $intInsert = $this->Mpicklist->add($arrData, $this->input->post());
        
        $this->Mpicklistitem->add($intInsert, $this->input->post());
        $this->_CI->db->trans_complete();

        redirect('picklist/browse/');
    }

    $arrPickListData = $this->Mpicklist->getAllPickList();

    $this->load->view('sia',array(
        'strViewFile' => 'picklist/add',
        'arrPickList' => $arrPickListData,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-add')
    ));
}

public function view($intID) {
    $arrPickListData = $this->Mpicklist->getPickListDataByID($intID);

    if($this->input->post('subSave') != '') {    
        $intPickListID = $this->input->post('pcli_id');
        $intProductID = $this->input->post('idProduct');

        $this->_CI->db->trans_start();        
        $this->Mpicklistitem->update($intPickListID, $intProductID, $this->input->post());
        $this->_CI->db->trans_complete();
               
        redirect('picklist/view/'.$intID);
    }

    if($this->input->post('subDelete') != '') {
        $this->_CI->db->trans_start();        
        $this->Mpicklist->delete($intID);

        $this->_CI->db->trans_complete();

        redirect('picklist/browse/');
    }

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'picklist/view',
        'arrPickList' => $arrPickListData,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title')),$this->admlinklist->getMenuPermission(263,$arrPickListData[0]['pcls_status'])));       
}

}