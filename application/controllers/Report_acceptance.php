<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Report_Acceptance extends JW_Controller {

    public function __construct() {
    	parent::__construct();
    	if($this->session->userdata('strAdminUserName') == '') redirect();

    	// Generate the menu list
    	$this->load->model('Madmlinklist','admlinklist');
    	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

        $this->_getMenuHelpContent(243,true,'adminpage');
    }

    public function index() {
        // WARNING! Don't change the following steps
    	if($this->input->post('smtGenerateReportAcceptance') != '') {
            $this->load->model('Macceptance');
            $this->load->model('Macceptanceitem');
            $this->load->model('Mpurchaseitem');
            $this->load->model('Mkontrak');
            $this->load->model('Msupplier');

            $intKontrakID = $this->input->post('intKontrakID');
            $intSupplierID = $this->input->post('intSupplierID');
            $strDateStart = $this->input->post('txtDateStart');
            $strDateEnd = $this->input->post('txtDateEnd');
            if(!empty($intKontrakID)) {
                $arrKontrak = $this->Mkontrak->getItemByID($intKontrakID);
                $arrDataFilter['kontrak'] = $arrKontrak['kont_name'];
            }
            else $arrDataFilter['kontrak'] = '-';
            if(!empty($intSupplierID)) {
                $arrSupplier = $this->Msupplier->getItemByID($intSupplierID);
                $arrDataFilter['supplier'] = $arrSupplier['supp_name'];
            }
            else $arrDataFilter['supplier'] = '-';
            $arrDataFilter['dateStart'] = formatDate2($strDateStart);
            $arrDataFilter['dateEnd'] = formatDate2($strDateEnd);

            $arrAcceptance = $this->Macceptance->getReportAcceptance($intKontrakID,$intSupplierID,date('Y-m-d 00:00:00',strtotime($strDateStart)),date('Y-m-d 00:00:00',strtotime($strDateEnd)));
            foreach ($arrAcceptance as $key => $v) {
                $arrAcceptanceItem[$key] = $this->Macceptanceitem->getReportAcceptanceDetail($v['id']);
            }

            $arrData['strPageMode'] = 'Laporan BPB';
            $arrData['dataPrint'] = "Tanggal : ".formatDate2($this->input->post('strDateStart'),'d F Y')." sampai ". formatDate2($this->input->post('strDateEnd'),'d F Y');
            $arrData['dataPrint'] .= "<br />Supplier : ".$arrDataFilter['supplier'];
            $arrData['dataPrint'] .= "<br />Proyek : ".$arrDataFilter['kontrak'];

            $this->load->view('sia',array_merge(array(
                'strViewFile' => 'report_acceptance/result',
                'arrAcceptance' => $arrAcceptance,
                'arrAcceptanceItem' => $arrAcceptanceItem,
                'arrDataFilter' => $arrDataFilter,
                'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-browse')
            ),$arrData));
    	}
        redirect('report_acceptance/browse');
    }

    // To display, edit and delete purchase
    public function view($intID = 0) {
    	# INIT
    	$arrReturn = $this->session->userdata('arrReturn');

    	if($this->input->post('approval') != '' && $intID != '') {
            $this->Mpurchase->updateApproval($intID,$this->input->post('approval'),$this->session->userdata('strAdminID'));
            if($this->input->post('approval') == 1) {
                $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
                $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
                if($arrPurchaseData['prch_kontrak_pembelian_id'] != 0) {
                    $this->load->model('Mkontrakpembelian');
                    $this->Mkontrakpembelian->updateMinusPO($arrPurchaseData['prch_kontrak_pembelian_id'],$arrPurchaseItem[0]['prci_quantity_pb']);
                }
                $this->load->model('Mpurchaseorderitem');
                foreach ($arrPurchaseItem as $e) {
                    $result = $this->Mpurchaseitem->recalculateTotalByPOIID($e['prci_proi_id']);
                    $this->Mpurchaseorderitem->updateTerpurchase($poiID, $result[0]['total']); // update terpurchase PB
                }
                $this->session->set_flashdata('strMessage','OP ditolak oleh PM');
                redirect('purchase/browse');
            }

            $this->session->set_flashdata('strMessage','OP sudah diapprove oleh PM');
            redirect('purchase/browse');
        }
    	$arrPurchaseData = $this->Mpurchase->getItemByID($intID);
    	if(!empty($arrPurchaseData)) {
            // $arrArrived = $this->Macceptance->getArrivedByPurchaseID($intID);
    		$arrPurchaseData['prch_rawstatus'] = $arrPurchaseData['prch_status'];
    		$arrPurchaseData['prch_status'] = translateDataIntoHTMLStatements(
    			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'purchase_status', 'allow_null' => 0, 'validation' => ''),
    			$arrPurchaseData['approvalPM']);
    		
    		// Load the purchase item
    		$arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID2($intID);
    		
    	} else {
    		$arrPurchaseItem = array(); $arrPurchaseList = array();
    	}

        $this->load->model('Mdbvo','MAdminLogin');
        $this->MAdminLogin->initialize('adm_login');

        $this->MAdminLogin->dbSelect('','id =' . $arrPurchaseData['cby']);
        $arrLogin = $this->MAdminLogin->getNextRecord('Array');

        $this->MAdminLogin->initialize('adm_login');

        $this->MAdminLogin->dbSelect('','id =' . $arrPurchaseData['mby']);
        $arrEditedBy = $this->MAdminLogin->getNextRecord('Array');

        $arrData = array(
            'intPurchaseID' => $intID,
            'intPurchaseTotal' => $intPurchaseTotal,
            'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
            'arrPurchaseItem' => $arrPurchaseItem,
            'arrPurchaseData' => $arrPurchaseData,
            'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
            'arrLogin' => $arrLogin,
            'arrArrived' => $arrArrived,
            'numberTax' => $this->input->get('number'),
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        );

        if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
            //$dataPrint=$this->Mpurchase->getPrintDataByID($intID);
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => 'purchase',
            ), $arrData));
        } else {
            # Load all other purchase data the user has ever made
            $arrPurchaseList = $this->Mpurchase->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

            $this->load->view('sia',array_merge(array(
                'strViewFile' => 'purchase/view',
                'arrPurchaseList' => $arrPurchaseList,
                'arrUser' => $arrUser,
                'arrEditedBy' => $arrEditedBy,
                'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchasestatus')
            ), $arrData, $this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus'])));
        }
    }

    public function edit($intID) {
        $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
        if(!empty($arrPurchaseData)) {
            // $arrArrived = $this->Macceptance->getArrivedByPurchaseID($intID);
            $arrPurchaseData['prch_rawstatus'] = $arrPurchaseData['prch_status'];
            $arrPurchaseData['prch_status'] = translateDataIntoHTMLStatements(
                array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'purchase_status', 'allow_null' => 0, 'validation' => ''),
                $arrPurchaseData['approvalPM']);
            
            // Load the purchase item
            $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID2($intID);
            
        } else {
            $arrPurchaseItem = array();
        }

        $arrData = array(
            'intPurchaseID' => $intID,
            'arrPurchaseItem' => $arrPurchaseItem,
            'arrPurchaseData' => $arrPurchaseData,
            // 'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
            // 'arrArrived' => $arrArrived,
            'numberTax' => $this->input->get('number'),
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
        );
        $this->load->view('sia',array_merge(array(
                'strViewFile' => 'purchase/edit',
                // 'arrPurchaseList' => $arrPurchaseList,
                'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-editpurchase')
            ), $arrData, $this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus'])));
    }

    public function browse() {
        $this->load->model('Msupplier');
        $arrSupplier = $this->Msupplier->getAllSuppliers();
        $this->load->model('Mkontrak');
        $arrKontrak = $this->Mkontrak->getKontrak();

        $this->load->view('sia',array(
            'strViewFile' => 'report_acceptance/browse',
            'arrKontrak' => $arrKontrak,
            'arrSupplier' => $arrSupplier,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'reportacceptance-browse')
        ));
    }

}

/* End of File */