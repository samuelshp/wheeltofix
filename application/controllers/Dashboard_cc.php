<?php

class Dashboard_cc extends JW_Controller {

	public function __construct() {
		parent::__construct();
		
		if($this->session->userdata('strAdminUserName') == '') redirect();

		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->load->model('Mdashboardcc');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

		//$this->load->model('Mdashboard');

	    $this->_getMenuHelpContent(238,true,'adminpage');
	}

	public function index(){
		if($this->input->post('makeDashboardCC')){
			$this->load->model('Mkontrak');
			$arrKontrak = $this->Mkontrak->getKontrakByInProject($this->session->userdata('strProjectInTeam'));
			$this->load->model('Msubkontrak');
			$arrSubkontrak = $this->Msubkontrak->getSubkontrakByKontrakID($this->input->post('intKontrakID'));
			$this->load->model('Msubkontrakmaterial');
			$arrBahan = $this->Msubkontrakmaterial->getSubkontrakMaterialBySubkontrakID($this->input->post('intSubkontrakID'));
			$arrData = $this->Mdashboardcc->getCostControlDataByProject($this->input->post('intSubkontrakID'),$this->input->post('intProductID'));
			$arrNonMaterial = $this->Mdashboardcc->getCostControlNonMaterial($this->input->post('intSubkontrakID'),$this->input->post('intProductID'));
			$this->load->view('sia',array(
		        'strViewFile' => 'dashboard_cc/result',
		        'arrData' => $arrData,
		        'arrNonMaterial' => $arrNonMaterial,
				'arrKontrak' => $arrKontrak,
				'arrSubkontrak' => $arrSubkontrak,
				'arrBahan' => $arrBahan,
		        'intKontrakID' => $this->input->post('intKontrakID'),
		        'intSubkontrakID' => $this->input->post('intSubkontrakID'),
		        'intProductID' => $this->input->post('intProductID'),
		        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'dashboardcc-title')
		    ));
		} else redirect('dashboard_cc/browse');
	}

	public function browse() {
		$this->load->model('Mkontrak');
		$arrKontrak = $this->Mkontrak->getKontrakByInProject($this->session->userdata('strProjectInTeam'));

		$this->load->view('sia',array(
			'strViewFile' => 'dashboard_cc/browse',
			'arrKontrak' => $arrKontrak,
	        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'dashboardcc-title')
	    ));
	}

}
?>