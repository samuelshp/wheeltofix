<?php

class Downpayment_ajax extends JW_Controller {

public function getAllDpDataByIdSupplier($id) {
	$this->load->model('Mdownpayment');
	ArrayToXml(array('DownPayment' => $this->Mdownpayment->getAllDpDataByIdSupplier($id)));
}

public function getAllDpDataByIdDp($id) {
	$this->load->model('Mdownpayment');
	ArrayToXml(array('DownPayment' => $this->Mdownpayment->getAllDpDataByIdDp($id)));
}

public function getAllDpDataByIdSupplierAndIdKontrak($idKontrak, $idSupplier) {
	$this->load->model('Mdownpayment');
	return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdownpayment->getAllDpDataByIdSupplierAndIdKontrak($idKontrak, $idSupplier)));
}

public function countDp($id, $idDp){
	$this->load->model('Mdownpayment');
	ArrayToXml(array('TotalDipakai' => $this->Mdownpayment->countDp($id, $idDp)));
}

}