<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Downpayment extends JW_Controller {

private $_CI;

public function __construct() {
	parent::__construct();
    $this->_CI =& get_instance();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mdownpayment');
    $this->load->model('Mdaftarpembayaranhutangitem');

    $this->_getMenuHelpContent(237,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
    if($this->input->post('smtSaveDownpayment') != '') { // Save Kontrak Pembelian
        $tanggal = explode('/',$this->input->post('txtDate'));
        $strDPCode = 'DP/'.substr($tanggal[0], 2,2).'/'.$tanggal[1].'/';
        $lastCode = $this->Mdownpayment->getLastCode($strDPCode);
        $newCode = 1;
        if(!empty($lastCode))
        $newCode = $lastCode['code']+1;
        $strDPCode .= str_pad($newCode, 4 , '0', STR_PAD_LEFT);
        $arrDownpayment = array(
            'strDPCode' => $strDPCode,
            'strDate' => $this->input->post('txtDate'),
            'intKontrakID' => $this->input->post('intKontrakID'),
            'intSupplierID' => $this->input->post('intSupplierID'),
            'intKPID' => $this->input->post('intKPID'),
            'intAmount' => $this->input->post('txtAmount'),
            'intPpn' => $this->input->post('txtPpn'),
            'intAmountPpn' => $this->input->post('txtAmountPpn'),
            'intPph' => $this->input->post('txtPph'),
            'intAmountPph' => $this->input->post('txtAmountPph'),
            'intAmountFinal' => $this->input->post('txtAmountFinal'),
            'strDescription' => $this->input->post('txtDescription')
        );
        $this->Mdownpayment->add($arrDownpayment['strDPCode'],$arrDownpayment['strDate'],$arrDownpayment['intKontrakID'],$arrDownpayment['intSupplierID'],$arrDownpayment['intKPID'],$arrDownpayment['intAmount'],$arrDownpayment['intPph'],$arrDownpayment['intAmountPph'],$arrDownpayment['intPpn'],$arrDownpayment['intAmountPpn'],$arrDownpayment['intAmountFinal'],$arrDownpayment['strDescription']);

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'downpayment-save')."".$arrDownpayment['strDPCode'];
        redirect('downpayment/browse');
	}
    $this->load->model('Mkontrak');
    $arrKontrak = $this->Mkontrak->getKontrak();
    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'downpayment/add',
        'intField' => $intField,
        'arrKontrak' => $arrKontrak,
        'arrSupplier' => $arrSupplier,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-add')
    ));
}

// To display, edit and delete purchase
public function view($intID=0) {
	# INIT
    if($this->input->post('subSave') != '' ) { # SAVE

        $id = $this->input->post('idDp');
        $pph = $this->input->post('txtPph');
        $pph_awal = $this->input->post('pph_awal');
        $ppn = $this->input->post('txtPpn');
        $ppn_awal = $this->input->post('ppn_awal');
        $amount = $this->input->post('txtAmount');
        $amount_awal = $this->input->post('amount_awal');

        $new_amount = str_replace(['.',','],['','.'],$amount_awal);
        if($amount!=$amount_awal){
            $new_amount = str_replace(['.',','],['','.'],$amount);
        }
        $new_pph = str_replace(['.',','],['','.'],$pph);
        $new_ppn = str_replace(['.',','],['','.'],$ppn);
        
        $amount_pph = (($new_pph*$new_amount)/100);
        $amount_ppn = (($new_ppn*$new_amount)/100);

        $final_total = ($new_amount+$amount_ppn)-$amount_pph;        
        $this->_CI->db->trans_start();
        $this->Mdownpayment->editDp($id,$new_pph,$new_ppn,$new_amount,$amount_pph,$amount_ppn,$final_total);
        $this->Mdownpayment->updatePerson($id,$_SESSION['strAdminID']);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Uang Muka Supplier berhasil diubah');
        redirect('downpayment/view/'.$id);
        
        
		
	} else if($this->input->post('subDelete') != '') {
        $id = $this->input->post('idDp');
        $codeDp = $this->input->post('codeDp');
        $this->_CI->db->trans_start();
        $this->Mdownpayment->deleteDp($id);
        $this->Mdownpayment->updatePerson($id,$_SESSION['strAdminID']);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Uang Muka Supplier dengan kode '.$codeDp.' berhasil dihapus');
        redirect('downpayment/browse');
    }
    
    $arrDownpayment = $this->Mdownpayment->getAllDataByID($intID);
    
    $arrDownpayment2 = $this->Mdownpayment->countDp($arrDownpayment['dpay_supplier_id'],$intID);
    $availabilityDph = $this->Mdaftarpembayaranhutangitem->checkAvailabilityDp($intID);

    $editable = $this->Mdownpayment->checkEditableDp($intID);

    // echo print_r($arrDownpayment2[0])." ".$arrDownpayment['dpay_supplier_id']." ".$intID;
    // die();

    $this->load->view('sia',array(
        'strViewFile' => 'downpayment/view',
        'arrDownpayment' => $arrDownpayment,
        'arrDownpayment2' => $arrDownpayment2[0],
        'editable' => $editable,
        'availabilityDph' => $availabilityDph,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-view')
    ));

}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {     
        $arrSearchData = array(
            'dateStart' => $this->input->post('txtSearchDate'),
            'dateEnd' => $this->input->post('txtSearchDateTo'),
            'strSearchSupplier' => $this->input->post('txtSearchValue'),
            'intStatus' => $this->input->post('txtSearchStatus'),
            'txtSearchNoDP' => $this->input->post('txtSearchNoDP')
        );              
        $arrDownpayment = $this->Mdownpayment->searchBy($arrSearchData);
        if(!empty($arrDownpayment)) for($i = 0; $i < count($arrDownpayment); $i++) {
            $arrDownpayment[$i] = array_merge($arrDownpayment[$i],$this->admlinklist->getMenuPermission(237,$arrDownpayment[$i]['dpay_status']));
        }
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('downpayment/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDownpayment) ? count($arrDownpayment) : '0')." records).";             
    }else{
        $arrPagination['base_url'] = site_url("downpayment/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mdownpayment->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrDownpayment = $this->Mdownpayment->getAllDownpayment($intPage,$arrPagination['per_page']);
        if(!empty($arrDownpayment)) for($i = 0; $i < count($arrDownpayment); $i++) {
            $arrDownpayment[$i] = array_merge($arrDownpayment[$i],$this->admlinklist->getMenuPermission(237,$arrDownpayment[$i]['dpay_status']));
        }
    }
    
    $this->load->view('sia',array(
        'strViewFile' => 'downpayment/browse',
        'arrDownpayment' => $arrDownpayment,
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'strSearchDate' => $arrSearchData['dateStart'],
        'strSearchDateTo' => $arrSearchData['dateEnd'],
        'strSearchSupplier' => $arrSearchData['strSearchSupplier'],
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'downpayment-browse')
    ));
}

public function getKontrakPembelianBySuppID($intSupplierID) {
    $this->load->model('Mkontrakpembelian');
    $arrKontrakPembelian = $this->Mkontrakpembelian->getDataBySupplierID($intSupplierID);

    return $this->output->set_content_type('application/json')->set_output(json_encode($arrKontrakPembelian));
}


/* AUTOCOMPLETE */

}

/* End of File */