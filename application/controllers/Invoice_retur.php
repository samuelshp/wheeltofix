<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intInvoiceID)

PRIVATE FUNCTION:
- __construct()
*/

class Invoice_retur extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Minvoiceretur');
	$this->load->model('Minvoicereturitem');

    $this->_getMenuHelpContent(44,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps

	if($this->input->post('smtUpdateInvoice') != '' || $this->input->post('smtMakeInvoice') != '') {
	}

	if($this->input->post('smtMakeInvoice') != '') { // Make Invoice
		$this->load->model('Munit');
		$arrInvoiceRetur = array(
			'intCustID' => $this->input->post('customerID'),
			'intSalesmanID' => $this->input->post('salesmanID'),
			'intSuppID' => $this->input->post('supplierID'),
			'intWarehouse' => $this->input->post('WarehouseID'),
			'strDescription' => $this->input->post('txaDescription'),
			'strDate' => $this->input->post('txtDate')
		);

		$intInvoiceReturID = $this->Minvoiceretur->add($arrInvoiceRetur['intCustID'],$arrInvoiceRetur['intSalesmanID'],$arrInvoiceRetur['intSuppID'],$arrInvoiceRetur['intWarehouse'],$arrInvoiceRetur['strDescription'],2,$arrInvoiceRetur['strDate']);

		$totalItem = $this->input->post('totalItem');
        for($i = 0; $i < $totalItem; $i++) {
            $qty1Item = $this->input->post('qty1PriceEffect'.$i);
            $qty2Item = $this->input->post('qty2PriceEffect'.$i);
            $qty3Item = $this->input->post('qty3PriceEffect'.$i);
            $unit1Item = $this->input->post('sel1UnitID'.$i);
            $unit2Item = $this->input->post('sel2UnitID'.$i);
            $unit3Item = $this->input->post('sel3UnitID'.$i);
            $prcItem = $this->input->post('prcPriceEffect'.$i);
            $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
            $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
            $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
            $prodItem = $this->input->post('prodItem'.$i);
            $probItem = $this->input->post('probItem'.$i);
            $strProductDescription = formatProductName('',$prodItem,$probItem);
            $idItem = $this->input->post('idItem'.$i);
            if($strProductDescription != '' || !empty($strProductDescription)) {
                $this->Minvoicereturitem->add($intInvoiceReturID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
            }
        }
        $totalItemBonus = $this->input->post('totalItemBonus');
        for($i = 0; $i < $totalItemBonus; $i++) {
            $qty1Item = $this->input->post('qty1PriceEffectBonus'.$i);
            $qty2Item = $this->input->post('qty2PriceEffectBonus'.$i);
            $qty3Item = $this->input->post('qty3PriceEffectBonus'.$i);
            $unit1Item = $this->input->post('sel1BonusUnitID'.$i);
            $unit2Item = $this->input->post('sel2BonusUnitID'.$i);
            $unit3Item = $this->input->post('sel3BonusUnitID'.$i);
            $prodItem = $this->input->post('prodItemBonus'.$i);
            $probItem = $this->input->post('probItemBonus'.$i);
            $strProductDescription = formatProductName('',$prodItem,$probItem);
            $idItem = $this->input->post('idItemBonus'.$i);
            if($strProductDescription != '' || !empty($strProductDescription)) {
                $this->Minvoicereturitem->add($intInvoiceReturID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,0,0,0,0,1);
            }
        }
        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'return-returnmade'));
        redirect('invoice_retur/view/'.$intInvoiceReturID);
	}

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'invoiceretur/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturn')
    ));
}



// To display, edit and delete invoice
public function view($intID = '') {
	# INIT
	$this->load->model('Mtinydbvo','defconf'); $this->defconf->initialize('jwdefaultconfig');

	if($this->input->post('subSave') != '' && $intID != '') {
		// InvoiceRetur
		$arrInvoiceReturData = $this->Minvoiceretur->getItemByID($intID);

		if(compareData($arrInvoiceReturData['invr_status'],array(0))) {
			$this->Minvoiceretur->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'));

			// Load invoice item
			$arrInvoiceReturItem = $this->Minvoicereturitem->getItemsByInvoiceReturID($intID);
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');

            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
            foreach($arrInvoiceReturItem as $e) {
                // Search return quantity
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Minvoicereturitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);

                    } else $this->Minvoicereturitem->deleteByID($e['id']);

                } else $this->Minvoicereturitem->deleteByID($e['id']);
            }
            $totalItem = $this->input->post('totalItem');
            for($i = 0; $i < $totalItem; $i++) {
                $qty1Item = $this->input->post('qty1PriceEffect'.$i);
                $qty2Item = $this->input->post('qty2PriceEffect'.$i);
                $qty3Item = $this->input->post('qty3PriceEffect'.$i);
                $unit1Item = $this->input->post('sel1UnitID'.$i);
                $unit2Item = $this->input->post('sel2UnitID'.$i);
                $unit3Item = $this->input->post('sel3UnitID'.$i);
                $prcItem = $this->input->post('prcPriceEffect'.$i);
                $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
                $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
                $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
                $prodItem = $this->input->post('prodItem'.$i);
                $probItem = $this->input->post('probItem'.$i);
                $strProductDescription = formatProductName('',$prodItem,$probItem);
                $idItem = $this->input->post('idItem'.$i);
                if($strProductDescription != '' || !empty($strProductDescription)) {
                    $this->Minvoicereturitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0);
                }
            }

            $arrInvoiceReturItemBonus = $this->Minvoicereturitem->getItemsBonusByInvoiceReturID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');
            $arrContainID = $this->input->post('idProiIDBonus');
            foreach($arrInvoiceReturItemBonus as $e) {
                // Search return quantity
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Minvoicereturitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);

                    } else $this->Minvoicereturitem->deleteByID($e['id']);

                } else $this->Minvoicereturitem->deleteByID($e['id']);
            }
            $totalItemBonus = $this->input->post('totalItemBonus');
            for($i = 0; $i < $totalItemBonus; $i++) {
                $qty1Item = $this->input->post('qty1PriceEffectBonus'.$i);
                $qty2Item = $this->input->post('qty2PriceEffectBonus'.$i);
                $qty3Item = $this->input->post('qty3PriceEffectBonus'.$i);
                $unit1Item = $this->input->post('sel1BonusUnitID'.$i);
                $unit2Item = $this->input->post('sel2BonusUnitID'.$i);
                $unit3Item = $this->input->post('sel3BonusUnitID'.$i);
                $prodItem = $this->input->post('prodItemBonus'.$i);
                $probItem = $this->input->post('probItemBonus'.$i);
                $strProductDescription = formatProductName('',$prodItem,$probItem);
                $idItem = $this->input->post('idItemBonus'.$i);
                if($strProductDescription != '' || !empty($strProductDescription)) {
                    $this->Minvoicereturitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,0,0,0,0,1);
                }
            }
		} else $this->Minvoiceretur->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));

		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoiceupdated');//benakno

	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
		$this->Minvoicereturitem->deleteByInvoiceID($intID);
		$this->Minvoiceretur->deleteByID($intID);

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-invoicedeleted'));
		redirect('invoice_retur/browse');
	}

	$arrInvoiceReturData = $this->Minvoiceretur->getItemByID($intID);

	if(!empty($arrInvoiceReturData)) {
		$arrInvoiceReturData['invr_rawstatus'] = $arrInvoiceReturData['invr_status'];
		$arrInvoiceReturData['invr_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrInvoiceReturData['invr_status']);
		$intID = $arrInvoiceReturData['id'];

		// Load the invoice item
		$arrInvoiceReturItem = $this->Minvoicereturitem->getItemsByInvoiceReturID($intID);
        
		$intInvoiceReturTotal = 0;
		$this->load->model('Munit');
		for($i = 0; $i < count($arrInvoiceReturItem); $i++) {
			$txtDiscount1 = $arrInvoiceReturItem[$i]['inri_discount1'];
			$txtDiscount2 = $arrInvoiceReturItem[$i]['inri_discount2'];
			$txtDiscount3 = $arrInvoiceReturItem[$i]['inri_discount3'];
			$intQty1 = $arrInvoiceReturItem[$i]['inri_quantity1'];
			$intQty2 = $arrInvoiceReturItem[$i]['inri_quantity2'];
			$intQty3 = $arrInvoiceReturItem[$i]['inri_quantity3'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$i]['inri_unit1']);
			$intConv1 = $convTemp[0]['unit_conversion'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$i]['inri_unit2']);
			$intConv2 = $convTemp[0]['unit_conversion'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$i]['inri_unit3']);
			$intConv3 = $convTemp[0]['unit_conversion'];
			$intPrice = $arrInvoiceReturItem[$i]['inri_price'];
			$intInvoiceReturTotalTemp = ((float) $intPrice * (float) $intQty1) + ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2) + ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * $intQty3);
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount1 / 100);
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount2 / 100);
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount3 / 100);
			$intInvoiceReturTotal += $intInvoiceReturTotalTemp;
			$arrInvoiceReturItem[$i]['inri_subtotal'] = $intInvoiceReturTotalTemp;
			$arrInvoiceReturItem[$i]['inri_conv1'] = $intConv1;
			$arrInvoiceReturItem[$i]['inri_conv2'] = $intConv2;
			$arrInvoiceReturItem[$i]['inri_conv3'] = $intConv3;
		}
		$InvoiceReturTotal = (float) $intInvoiceReturTotal;

        $arrInvoiceReturItemBonus = $this->Minvoicereturitem->getItemsBonusByInvoiceReturID($intID);
        for($i = 0; $i < count($arrInvoiceReturItemBonus); $i++) {
            $intQty1 = $arrInvoiceReturItemBonus[$i]['inri_quantity1'];
            $intQty2 = $arrInvoiceReturItemBonus[$i]['inri_quantity2'];
            $intQty3 = $arrInvoiceReturItemBonus[$i]['inri_quantity3'];
            $convTemp = $this->Munit->getConversion($arrInvoiceReturItemBonus[$i]['inri_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceReturItemBonus[$i]['inri_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrInvoiceReturItemBonus[$i]['inri_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $arrInvoiceReturItemBonus[$i]['inri_conv1'] = $intConv1;
            $arrInvoiceReturItemBonus[$i]['inri_conv2'] = $intConv2;
            $arrInvoiceReturItemBonus[$i]['inri_conv3'] = $intConv3;
        }
	} else {
		$arrInvoiceReturItem = array(); $arrInvoiceReturItemBonus = array(); $arrInvoiceReturList = array(); $intInvoiceReturTotal = 0; $intInvoiceReturGrandTotal = 0;
	}

    $arrData = array(
        'arrInvoiceReturData' => $arrInvoiceReturData,
        'arrInvoiceReturItem' => $arrInvoiceReturItem,
        'arrInvoiceReturItemBonus' => $arrInvoiceReturItemBonus,
        'intInvoiceReturID' => $intID,
        'intInvoiceReturTotal' => $intInvoiceReturTotal,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '') && $intID != '') {
        //$this->Minvoice->editAfterPrint($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoiceretur',
            'dataPrint' => $this->Minvoiceretur->getPrintDataByID($intID),
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturnstatus')
        ), $arrData));

    }else {
        # Load all other invoice data the user has ever made
        $arrInvoiceReturList = $this->Minvoiceretur->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'invoiceretur/view',
            'arrInvoiceReturList' => $arrInvoiceReturList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturn')
        ), $arrData, $this->admlinklist->getMenuPermission(44,$arrInvoiceReturData['invr_rawstatus'])));
    }
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrInvoiceRetur = $this->Minvoiceretur->searchByCustomer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('invoice_retur/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Customer with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrInvoiceRetur) ? count($arrInvoiceRetur) : '0')." records).";
    } else {
		$arrPagination['base_url'] = site_url("invoice_retur/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Minvoiceretur->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrInvoiceRetur = $this->Minvoiceretur->getItems($intPage,$arrPagination['per_page']);
	}


	if(!empty($arrInvoiceRetur)) for($i = 0; $i < count($arrInvoiceRetur); $i++) {
		$arrInvoiceReturItem = $this->Minvoicereturitem->getItemsByInvoiceReturID($arrInvoiceRetur[$i]['id'],'COUNT_PRICE');
		$intInvoiceReturTotal = 0;
		$this->load->model('Munit');
		for($j = 0; $j < count($arrInvoiceReturItem); $j++) {
			$txtDiscount1 = $arrInvoiceReturItem[$j]['inri_discount1'];
			$txtDiscount2 = $arrInvoiceReturItem[$j]['inri_discount2'];
			$txtDiscount3 = $arrInvoiceReturItem[$j]['inri_discount3'];
			$intQty1 = $arrInvoiceReturItem[$j]['inri_quantity1'];
			$intQty2 = $arrInvoiceReturItem[$j]['inri_quantity2'];
			$intQty3 = $arrInvoiceReturItem[$j]['inri_quantity3'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$j]['inri_unit1']);
			$intConv1 = $convTemp[0]['unit_conversion'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$j]['inri_unit2']);
			$intConv2 = $convTemp[0]['unit_conversion'];
			$convTemp = $this->Munit->getConversion($arrInvoiceReturItem[$j]['inri_unit3']);
			$intConv3 = $convTemp[0]['unit_conversion'];
			$intPrice = $arrInvoiceReturItem[$j]['inri_price'];
			if((float) $intConv1 * (float) $intConv2 * (float) $intQty2 <= 0) $tmpTotalPriceConv2 = 0;
			else $tmpTotalPriceConv2 = ((float)$intPrice/(float)$intConv1*(float)$intConv2*(float)$intQty2);
			
			if((float) $intConv1 * (float) $intConv3 * (float) $intQty3 <= 0) $tmpTotalPriceConv3 = 0;
			else $tmpTotalPriceConv3 = ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);
			
			$intInvoiceReturTotalTemp = ((float) $intPrice * (float) $intQty1) + $tmpTotalPriceConv2 + $tmpTotalPriceConv3;
			
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount1 / 100);
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount2 / 100);
			$intInvoiceReturTotalTemp = $intInvoiceReturTotalTemp - ($intInvoiceReturTotalTemp * $txtDiscount3 / 100);
			$intInvoiceReturTotal+=$intInvoiceReturTotalTemp;
		}
		
		$arrInvoiceRetur[$i] = array_merge($arrInvoiceRetur[$i],$this->admlinklist->getMenuPermission(44,$arrInvoiceRetur[$i]['invr_status']));
		$arrInvoiceRetur[$i]['invr_rawstatus'] = $arrInvoiceRetur[$i]['invr_status'];
		$arrInvoiceRetur[$i]['invr_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrInvoiceRetur[$i]['invr_status']);
		$arrInvoiceRetur[$i]['invr_total'] = (float) $intInvoiceReturTotal;
	}

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'invoiceretur/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrInvoiceRetur' => $arrInvoiceRetur,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'return-invoicereturn')
    ));
}

}

/* End of File */