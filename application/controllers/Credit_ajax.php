<?php
/**
 * 
 */
class Credit_ajax extends JW_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mnotadebet");
	}

	public function getUMCList($intCustID)
	{		
		$this->load->model('Mcredit'); 
		$arrUMCData = $this->Mcredit->getUMCList($intCustID);

		if (!empty($arrUMCData)) {
			foreach ($arrUMCData as $key => $value) {
				$arrUMCData[$key]['date'] = formatDate2($value['date'], 'd F Y');
				$arrUMCData[$key]['amount'] = setPrice($value['amount']);
				$arrUMCData[$key]['raw_amount'] = $value['amount'];
			}
		}
		
        return $this->output->set_content_type('application/json')->set_output(json_encode($arrUMCData));
	}

	public function getInvoiceList($intCustID)
	{
		$this->load->model('Mcredit'); 
		$arrInvoiceData = $this->Mcredit->getInvoiceList($intCustID);

		if (!empty($arrInvoiceData)) {
			foreach ($arrInvoiceData as $key => $value) {
			 	// $arrInvoiceData[$key]['cdate'] = formatDate2($value['cdate'], 'd F Y');
				$arrInvoiceData[$key]['invo_grandtotal'] = setPrice($value['invo_grandtotal']);
				$arrInvoiceData[$key]['raw_grandtotal'] = $value['invo_grandtotal']-$value['invo_payment_value'];
			}
		}

		return $this->output->set_content_type('application/json')->set_output(json_encode($arrInvoiceData));
	}
	
	//Nota Debet
	public function getPurchaseInvoiceBySupplier($intSuppID)
	{
		return $this->output->set_content_type("application/json")
		->set_output(json_encode($this->Mnotadebet->getPurchaseInvoiceBySupplier($intSuppID)));
	}

	public function getPurchaseInvoiceDetail($intPIID)
	{
		return $this->output->set_content_type("application/json")
		->set_output(json_encode($this->Mnotadebet->getPurchaseInvoiceDetail($intPIID)));
	}
}