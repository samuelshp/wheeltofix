<?php

//Function
// - getKontrakData
// - getBahan
// - getSatuanPB

class Purchase_order_ajax extends JW_Controller {

public function getDataForPurchase($intID = 0) {
	$arrItem = $this->Mpurchasereturitem->getItemsByPurchaseID($intID);
	$this->load->model('Munit');
	$arrUnit = array();
	$i = 0;
	foreach($arrItem as $e) {
		$arrUnit['Unit'.$i] = $this->Munit->getUnitsForCalculation($e['prri_product_id']);
		$i++;
	}

	ArrayToXml(array('returItem' => $arrItem,'Unit' => $arrUnit));
}

public function getPurchaseReturAutoComplete($txtData = '') {
	ArrayToXml(array('Supplier' => $this->Mpurchaseretur->getDataAutoComplete($txtData)));
}
public function getPurchaseReturSupplierAutoComplete($txtData = '') {
    $this->load->model('Msupplier');

    ArrayToXml(array('Supplier' => $this->Msupplier->getAllSuppliers($txtData)));
}

public function kPrintLog($intID='0') {
    $this->load->model('Mprintlog');
    ArrayToXml(array('Kode' => $this->Mprintlog->getInvoiceNumber('purchase_retur',$intID)));
}

public function getOwnerData($namaOwner, $idKontrak = ''){
	$this->load->model('Mpurchaseorder');
    ArrayToXml(array('Owner' => $this->Mpurchaseorder->getAllOwner($namaOwner, $idKontrak)));
}

public function getNamaProyek($nama_proyek='', $id_pegawai_login=''){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('NamaProyek' => $this->Mpurchaseorder->getNamaProyek($nama_proyek, $id_pegawai_login)));
}

public function getNamaPekerjaan($kontak_id){
	// $nama_pekerjaan = $this->input->post('namaPekerjaan');
	// $id_pegawai_login = $this->input->post('adminID');
	// $kontrak_id = $this->input->post('idKontrak');
	$this->load->model('Mpurchaseorder');
	//ArrayToXml(array('NamaPekerjaan' => $this->Mpurchaseorder->getNamaPekerjaan($nama_pekerjaan, $id_pegawai_login, $kontrak_id)));
	return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mpurchaseorder->getNamaPekerjaan($kontak_id)));
}

public function getAllBahanAutoComplete(){
	$subkon_id = $this->input->post('idsubkon');
	$bahan = $this->input->post('bahan2');
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('Bahan' => $this->Mpurchaseorder->getAllBahanAutoComplete2($bahan, $subkon_id)));
}

public function getAllBahanAutoCompleteById(){
	$subkon_id = $this->input->post('idsubkon');
	$bahan = $this->input->post('bahanid');
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('Bahan' => $this->Mpurchaseorder->getAllBahanAutoCompleteById($bahan, $subkon_id)));
}

public function getAllBahanAC($bahan){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('Bahan' => $this->Mpurchaseorder->getAllBahanAC($bahan)));
}

public function checkBahanToSubkontrakMaterial($subkon_id2, $idprod){
	$this->load->model('Msubkontrakmaterial');
	ArrayToXml(array('Bahan' => $this->Msubkontrakmaterial->checkBahanToSubkontrakMaterial($subkon_id2, $idprod)));
}

public function getBahanByIdProduct($id_prod){
	$this->load->model('Mpurchaseorder');
	ArrayToXml(array('Bahan' => $this->Mpurchaseorder->getBahanByIdProduct($id_prod)));
}

}