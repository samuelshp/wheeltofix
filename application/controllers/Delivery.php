<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Delivery extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mdelivery');
    $this->load->model('Mdeliveryitem');

    $this->_getMenuHelpContent(51,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
	if($this->input->post('smtUpdateDelivery') != '' || $this->input->post('smtMakeDelivery') != '') {
	}

	if($this->input->post('smtMakeDelivery') != '') { // Make Delivery

        $arrDelivery = array(
            'intAtWarehouse' => $this->input->post('WarehouseID'),
            'intSalesman'=> $this->input->post('delivererID'),
            'strDescription' => $this->input->post('txaDescription'),
            'strEkspedition' => $this->input->post('txtEkspedition'),
            'strDateSent' => $this->input->post('txtDateSent'),
            'strDate'=> $this->input->post('txtDate')
        );
        $this->load->model('Minvoice');
        $intDeliveryID = $this->Mdelivery->add($arrDelivery['intAtWarehouse'],$arrDelivery['intSalesman'],$arrDelivery['strEkspedition'],$arrDelivery['strDescription'],$arrDelivery['strDateSent'],$arrDelivery['strDate']);
        $totalItem = $this->input->post('totalItem');
        for($i = 0; $i < $totalItem; $i++) {
            $invoiceID = $this->input->post('idSale'.$i);
            if($invoiceID > 0){
                $this->Mdeliveryitem->add($intDeliveryID,$invoiceID);
            }
        }
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliveryadded');
		redirect('delivery/view/'.$intDeliveryID);
	}

	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'delivery/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivery')
    ));
}

// To display, edit and delete delivery
public function view($intID = 0) {
	# INIT
	if($this->input->post('subSave') != '' && $intID != '') { # SAVE
		// Delivery
		$arrDeliveryData = $this->Mdelivery->getItemByID($intID);

		if(compareData($arrDeliveryData['deli_status'],array(0))) {
			$this->Mdelivery->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'),$this->input->post('selEditable'),$this->input->post('txtEkspedition'));
            $arrDeliveryItem = $this->Mdeliveryitem->getItemsByDeliveryID($intID);

            $arrContainID = $this->input->post('idDeit');
            foreach($arrDeliveryItem as $e) {
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {

                    } else $this->Mdeliveryitem->deleteByID($e['id']);

                } else $this->Mdeliveryitem->deleteByDeliveryID($intID);
            }

            $totalItem = $this->input->post('totalItem');

			for($i = 0; $i < $totalItem; $i++) {
                $invoiceID = $this->input->post('idSale'.$i);
                if($invoiceID > 0) $this->Mdeliveryitem->add($intID,$invoiceID);
            }

		} else $this->Mdelivery->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));
		if($this->input->post('selStatus')=='3'){
            $arrInvoice=$this->Mdeliveryitem->getInvoiceIDByDeliveryID($intID);
            $this->load->model('Minvoice');
            for($i = 0; $i < count($arrInvoice); $i++) {
                $this->Minvoice->editByID($arrInvoice[$i]['deit_invoice_id'],'','3');
            }
        }
		
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliveryupdated');
		
	} else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
		$this->Mdeliveryitem->deleteByDeliveryID($intID);
		$this->Mdelivery->deleteByID($intID);
		
		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliverydeleted'));
		redirect('delivery/browse');
		
	}

	$arrDeliveryData = $this->Mdelivery->getItemByID($intID);

	if(!empty($arrDeliveryData)) {
		$arrDeliveryData['deli_rawstatus'] = $arrDeliveryData['deli_status'];
		$arrDeliveryData['deli_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrDeliveryData['deli_status']);
        $arrDeliveryData['deli_seleditable'] = translateDataIntoHTMLStatements(
            array('title' => 'selEditable', 'field_type' => 'SELECT','description' => 'editable', 'allow_null' => 0, 'validation' => ''),
            $arrDeliveryData['deli_editable']);
		
		// Load the delivery item
        $arrItem = array();
		$arrDeliveryItem = $this->Mdeliveryitem->getItemsByDeliveryID($intID);
		$intDeliveryTotal = 0;
        $this->load->model('Minvoiceitem'); $this->load->model('Munit');
        $counter = 0;
		for($i = 0; $i < count($arrDeliveryItem); $i++) {
            $intDeliveryGrandTemp = 0;
            $arrDeliveryItem[$i]['invo_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrDeliveryItem[$i]['invo_status']);
            $arrDeliveryItemTemp = $this->Minvoiceitem->getItemsByInvoiceID($arrDeliveryItem[$i]['deit_invoice_id']);
            for($j = 0; $j < count($arrDeliveryItemTemp); $j++) {
                $flag = 0;
                $tempCounter=0;
                foreach ($arrItem as $e) {
                    if($e['id'] == $arrDeliveryItemTemp[$j]['product_id']){
                        $arrItem[$tempCounter]['qty1'] += $arrDeliveryItemTemp[$j]['invi_quantity1'];
                        $arrItem[$tempCounter]['qty2'] += $arrDeliveryItemTemp[$j]['invi_quantity2'];
                        $arrItem[$tempCounter]['qty3'] += $arrDeliveryItemTemp[$j]['invi_quantity3'];
                        $flag = 1;
                    }
                    $tempCounter++;
                }
                if($flag == 0) {
                    $arrItem[$counter]['name'] = "(".$arrDeliveryItemTemp[$j]['prod_code'].") ".formatProductName("",$arrDeliveryItemTemp[$j]['prod_title'],$arrDeliveryItemTemp[$j]['prob_title'],$arrDeliveryItemTemp[$j]['proc_title']);
                    $arrItem[$counter]['id'] = $arrDeliveryItemTemp[$j]['product_id'];
                    $arrItem[$counter]['qty1'] = $arrDeliveryItemTemp[$j]['invi_quantity1'];
                    $arrItem[$counter]['qty2'] = $arrDeliveryItemTemp[$j]['invi_quantity2'];
                    $arrItem[$counter]['qty3'] = $arrDeliveryItemTemp[$j]['invi_quantity3'];
                    $arrItem[$counter]['unit1'] = $arrDeliveryItemTemp[$j]['invi_unit1'];
                    $arrItem[$counter]['unit2'] = $arrDeliveryItemTemp[$j]['invi_unit2'];
                    $arrItem[$counter]['unit3'] = $arrDeliveryItemTemp[$j]['invi_unit3'];
                    $arrItem[$counter]['code'] = $arrDeliveryItemTemp[$j]['prod_code'];
                    $counter++;
                }
            }
            $arrDeliveryItemTemp = $this->Minvoiceitem->getBonusItemsByInvoiceID($arrDeliveryItem[$i]['deit_invoice_id']);
            for($j = 0; $j < count($arrDeliveryItemTemp); $j++) {
                $flag = 0;
                $tempCounter=0;
                foreach($arrItem as $e){
                    if($e['id'] == $arrDeliveryItemTemp[$j]['product_id']) {
                        $arrItem[$tempCounter]['qty1'] += $arrDeliveryItemTemp[$j]['invi_quantity1'];
                        $arrItem[$tempCounter]['qty2'] += $arrDeliveryItemTemp[$j]['invi_quantity2'];
                        $arrItem[$tempCounter]['qty3'] += $arrDeliveryItemTemp[$j]['invi_quantity3'];
                        $flag = 1;
                    }
                    $tempCounter++;
                }
                if($flag == 0) {
                    $arrItem[$counter]['name'] ="(".$arrDeliveryItemTemp[$j]['prod_code'].") ".formatProductName('',$arrDeliveryItemTemp[$j]['prod_title'],$arrDeliveryItemTemp[$j]['prob_title'],$arrDeliveryItemTemp[$j]['proc_title']);
                    $arrItem[$counter]['id'] = $arrDeliveryItemTemp[$j]['product_id'];
                    $arrItem[$counter]['qty1'] = $arrDeliveryItemTemp[$j]['invi_quantity1'];
                    $arrItem[$counter]['qty2'] = $arrDeliveryItemTemp[$j]['invi_quantity2'];
                    $arrItem[$counter]['qty3'] = $arrDeliveryItemTemp[$j]['invi_quantity3'];
                    $arrItem[$counter]['unit1'] = $arrDeliveryItemTemp[$j]['invi_unit1'];
                    $arrItem[$counter]['unit2'] = $arrDeliveryItemTemp[$j]['invi_unit2'];
                    $arrItem[$counter]['unit3'] = $arrDeliveryItemTemp[$j]['invi_unit3'];
                    $arrItem[$counter]['code'] = $arrDeliveryItemTemp[$j]['prod_code'];
                    $counter++;
                }
            }
            $arrDeliveryItem[$i]['deli_subtotal'] = $arrDeliveryItem[$i]['invo_grandtotal'];
            $intDeliveryTotal += $arrDeliveryItem[$i]['deli_subtotal'];
		}
		
	} else {
		$arrDeliveryItem = array(); $arrDeliveryList = array(); $arrReturnItem = array();
	}

    $arrData = array(
        'intDeliveryID' => $intID,
        'intDeliveryTotal' => $intDeliveryTotal,
        'arrDeliveryItem' => $arrDeliveryItem,
        'arrDeliveryData' => $arrDeliveryData,
        'arrItem' => $arrItem,
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'acceptance',
            'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
        ), $arrData));
    } else {
        # Load all other purchase data the user has ever made
        $arrDeliveryList = $this->Mdelivery->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'delivery/view',
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivery')
        ), $arrData, $this->admlinklist->getMenuPermission(51,$arrDeliveryData['deli_rawstatus'])));
    }
}

public function browse($intPage = 0) {
	if($this->input->post('subSearch') != '') {
		$arrDelivery = $this->Mdelivery->searchByDeliverer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

		$strPage = '';
		$strBrowseMode = '<a href="'.site_url('delivery/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDelivery) ? count($arrDelivery) : '0')." records).";
    } else {
		$arrPagination['base_url'] = site_url("delivery/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Mdelivery->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrDelivery = $this->Mdelivery->getItems($intPage,$arrPagination['per_page']);
	}
    
	if(!empty($arrDelivery)) for($i = 0; $i < count($arrDelivery); $i++) {
		$arrDeliveryItemID = $this->Mdeliveryitem->getInvoiceIDByDeliveryID($arrDelivery[$i]['id']);
		$intDeliveryTotal = 0;
        $this->load->model('Minvoice');
        
		$arrDelivery[$i] = array_merge($arrDelivery[$i],$this->admlinklist->getMenuPermission(51,$arrDelivery[$i]['deli_status']));
		$arrDelivery[$i]['deli_rawstatus'] = $arrDelivery[$i]['deli_status'];
		$arrDelivery[$i]['deli_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrDelivery[$i]['deli_status']);
        
        $this->load->model('Munit');
		for($j = 0; $j < count($arrDeliveryItemID); $j++) {
            //echo $arrDeliveryItemID[$j]['deit_invoice_id'];
            if($arrDeliveryItemID[$j]['deit_invoice_id']>0){
                $arrDeliveryItem = $this->Minvoice->getGrandTotalInvoiceByID($arrDeliveryItemID[$j]['deit_invoice_id']);

                $intDeliveryTotal += $arrDeliveryItem[0]['invo_grandtotal'];
            }
		}

		$arrDelivery[$i]['deli_total'] = $intDeliveryTotal;
	}

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'delivery/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrDelivery' => $arrDelivery,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-delivery')
    ));
}

public function back($intID = 0) {
    $this->_getMenuHelpContent(52,true,'adminpage');

    if($this->input->post('smtUpdateDelivery') != '' || $this->input->post('smtMakeDelivery') != '') {
    }
    //print_r($this->input->post());
    if($this->input->post('smtMakeDelivery') != '') { // Make Delivery
        $arrTime=$this->input->post('time');
        $selPayment=$this->input->post('selPayment');
        $txtDescription=$this->input->post('txaDescription');
        $intDeliveryID=$this->input->post('txtDeliveryID');
        $strDate=$this->input->post('txtDate');
        $strDateHour=$this->input->post('txtDateHour');
        $this->load->model('Mdeliveryback');
        $this->load->model('Mdeliverybackitem');
        $this->load->model('Minvoice');
        $arrDelivery=$this->Mdelivery->editByID3($intDeliveryID,$strDateHour,'3');
        $arrDeliveryBack=$this->Mdeliveryback->add($intDeliveryID,$strDate,$txtDescription,'3');
        foreach($selPayment as $key => $e)  {
            $arrDeliveryBackItem=$this->Mdeliverybackitem->add($arrDeliveryBack,$key,$selPayment[$key]);
            $status=0;
            if($selPayment[$key]=='1'){
                $status=3;
                $arrInvoice=$this->Minvoice->editByID3($key,$arrTime[$key],$selPayment[$key],$status);
            }elseif($selPayment[$key]=='2'){
                $status=3;
                $arrInvoice=$this->Minvoice->editByID3($key,$arrTime[$key],$selPayment[$key],$status);
            }elseif($selPayment[$key]=='3'){
                $status=2;
                $temp=$this->Mdeliveryitem->deleteByInvoiceID($key);
                $arrInvoice=$this->Minvoice->editByID4($key,$arrTime[$key],$status);
            }elseif($selPayment[$key]=='4'){
                $status=1;
                $temp=$this->Mdeliveryitem->deleteByInvoiceID($key);
                $arrInvoice=$this->Minvoice->editByID4($key,$arrTime[$key],$status);
            }
        }
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliverybackadded');
        redirect('delivery/backview/'.$arrDeliveryBack);
    }

    // Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'deliveryback/add',
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverystatus')
    ));
}

public function backbrowse($intPage = 0) {
    $this->_getMenuHelpContent(52,true,'adminpage');

    $this->load->model('Mdeliveryback');
    if($this->input->post('subSearch') != '') {
        $arrDelivery = $this->Mdeliveryback->searchByDeliverer($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('delivery/backbrowse', NULL, FALSE).'">[Back]</a>';, array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus')
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDelivery) ? count($arrDelivery) : '0')." records).";
    } else {
        $arrPagination['base_url'] = site_url("delivery/backbrowse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mdeliveryback->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrDelivery = $this->Mdeliveryback->getItems($intPage,$arrPagination['per_page']);
    }

    if(!empty($arrDelivery)) for($i = 0; $i < count($arrDelivery); $i++) {

        $arrDelivery[$i] = array_merge($arrDelivery[$i],$this->admlinklist->getMenuPermission(52,$arrDelivery[$i]['deba_status']));
        $arrDelivery[$i]['deba_rawstatus'] = $arrDelivery[$i]['deba_status'];
        $arrDelivery[$i]['deba_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrDelivery[$i]['deba_status']);
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'deliveryback/browse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrDelivery' => $arrDelivery,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverystatus')
    ));
}

public function backview($intID = 0) {
    $this->_getMenuHelpContent(52,true,'adminpage');

    # INIT

    if($this->input->post('subSave') != '' && $intID != '') { # SAVE

        // Delivery
        $this->load->model('Mdeliveryback');
        $this->load->model('Mdeliverybackitem');
        $arrDeliveryData = $this->Mdeliveryback->getItemByID($intID);

        if(compareData($arrDeliveryData['deba_status'],array(0))) {
            $this->Mdeliveryback->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('selStatus'));
            $arrTime=$this->input->post('time');
            $selPayment=$this->input->post('selPayment');
            if(!empty($selPayment)){
                foreach($selPayment as $key => $e)  {
                    $flag=$this->Mdeliverybackitem->getPaymentStatusByDeliveryBackAndItemID($intID,$key);
                    $arrDeliveryBackItem=$this->Mdeliverybackitem->editByID($intID,$key,$selPayment[$key]);
                    $status=0;
                    if($selPayment[$key]=='1'){
                        $status=3;
                    }elseif($selPayment[$key]=='2'){
                        $status=3;
                    }elseif($selPayment[$key]=='3'){
                        $status=2;
                        if($flag['prev_pay']<3){
                            $temp=$this->Mdeliveryitem->deleteByInvoiceID($key);
                        }
                    }elseif($selPayment[$key]=='4'){
                        $status=1;
                        if($flag['prev_pay']<3){
                            $temp=$this->Mdeliveryitem->deleteByInvoiceID($key);
                        }
                    }
                    $this->load->model('Minvoice');
                    // print_r($flag);
                    // echo $flag['prev_pay'];
                    if($flag['prev_pay']=='1' || $flag['prev_pay']=='2')
                    {
                        $arrInvoice=$this->Minvoice->editByID3($key,$arrTime[$key],$selPayment[$key],$status);
                    }
                }
            }

        } else $this->Mdeliveryback->editByID($intID,$this->input->post('selStatus'));

        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliverybackupdated');

    } else if($this->input->post('subDelete') != '' && $intID != '') {
        $this->Mdeliverybackitem->deleteByDebaID($intID);
        $this->Mdeliveryback->deleteByID($intID);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'delivery-deliverybackdeleted'));
        redirect('delivery/backbrowse');

    }
    $this->load->model('Mdeliveryback');
    $arrDeliveryData = $this->Mdeliveryback->getItemByID($intID);

    if(!empty($arrDeliveryData)) {
        $arrDeliveryData['deba_rawstatus'] = $arrDeliveryData['deba_status'];
        $arrDeliveryData['deba_status'] = translateDataIntoHTMLStatements(
            array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
            $arrDeliveryData['deba_status']);

        // Load the delivery item
        $arrItem = array();
        $this->load->model('Mdeliverybackitem');
        $arrDeliveryItem = $this->Mdeliverybackitem->getItemsByDeliveryBackID($intID);

    } else {
        $arrDeliveryItem = array(); $arrDeliveryList = array(); $arrReturnItem = array();
    }

    // Load all other purchase data the user has ever made
    $arrDeliveryList = $this->Mdeliveryback->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

    $this->load->view('sia',array_merge(array(
        'strViewFile' => 'deliveryback/view',
        'intDeliveryID' => $intID,
        'arrDeliveryList' => $arrDeliveryList,
        'arrDeliveryItem' => $arrDeliveryItem,
        'arrDeliveryData' => $arrDeliveryData,
        'arrItem' => $arrItem,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'delivery-deliverystatus')
    ), $this->admlinklist->getMenuPermission(52,$arrDeliveryData['deba_rawstatus'])));
}

}

/* End of File */