<?php
/**
 * 
 */
class Approvalgiro extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
    
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));	

		$this->load->model('Mgiro');
	}

	public function in($type = '', $intID = 0)
	{
		$this->_getMenuHelpContent(248,true,'adminpage');
		if ($type == 'add') {

			$arrDataGiro = $this->Mgiro->getAllData('in');			
			
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'approvalgiro/add',
	            'strPageTitle' => "Approval Giro Masuk",
	            'giroType' => __FUNCTION__,
	            'arrDataGiro' => $arrDataGiro
	        ),$this->admlinklist->getMenuPermission(248,1)));
		}
		if ($type == 'view') {
			$arrDataDetail = $this->Mgiro->getDetail(__FUNCTION__, $intID);		

			$this->load->view('sia',array_merge(array(
	            'strViewFile' => 'approvalgiro/view',
	            'strPageTitle' => "Approval Giro Keluar",
	            'giroType' => __FUNCTION__,
	            'arrDataDetail' => $arrDataDetail
	        ),$this->admlinklist->getMenuPermission(248,1)));
		}
		if ($type == 'browse') {
			$arrPagination['base_url'] = site_url("approvalgiro/in/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mgiro->getCount('PHU');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 10;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrApproved = $this->Mgiro->getAllApproved('in');	      
	        
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'approvalgiro/browse',
	            'strPageTitle' => 'Approval Giro Masuk',
	            'strPage' => $strPage,
	            'giroType' => __FUNCTION__,
	            'arrApproved' => $arrApproved
	        ),$this->admlinklist->getMenuPermission(248,1)));
		}

		if ($type == '') {
			
			if ($this->input->post('smtSaveApprovalGiro') != '') {

				$arrApproval = array(
					'approval_date' => $this->input->post('txtApprovalDate'),
					'approval_type' =>  $this->input->post('appGiro')
				);								

				$intUpdate = $this->Mgiro->approve($arrApproval);			

				if ($intUpdate) {
					$this->session->set_flashdata('strMessage','Giro berhasil diapprove');
					redirect('approvalgiro/in/browse');			
				}else{
					$this->session->set_flashdata('strMessage','Giro gagal diapprove');
					redirect('approvalgiro/in/add');		
				}
				
			}

			if ($this->input->post('smtUpdateApproval') != '') {
				
				$intID = $this->input->post('intID');				
				$arrApproval = array(
					'approval_date' => $this->input->post('txtDateApproval'),
					'approval_type' => $this->input->post('appGiro'),
				);

				$this->Mgiro->updateApproval($arrApproval);			

				$this->session->set_flashdata('strMessage','Approval Giro berhasil diperbaharui');
				redirect('approvalgiro/'.__FUNCTION__.'/view/'.$intID);

			}

		}
	}

	public function out($type='', $intID = 0)
	{
		$this->_getMenuHelpContent(249,true,'adminpage');
		if ($type == 'add') {

			$arrDataGiro = $this->Mgiro->getAllData('out');			
			
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'approvalgiro/add',
	            'strPageTitle' => "Approval Giro Keluar",
	            'giroType' => __FUNCTION__,
	            'arrDataGiro' => $arrDataGiro
	        ),$this->admlinklist->getMenuPermission(249,1)));
		}
		if ($type == 'view') {

			$arrDataDetail = $this->Mgiro->getDetail(__FUNCTION__, $intID);		

			$this->load->view('sia',array_merge(array(
	            'strViewFile' => 'approvalgiro/view',
	            'strPageTitle' => "Approval Giro Keluar",
	            'giroType' => __FUNCTION__,
	            'arrDataDetail' => $arrDataDetail
	        ),$this->admlinklist->getMenuPermission(249,1)));
		}

		if ($type == 'browse') {
			$arrPagination['base_url'] = site_url("approvalgiro/out/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mgiro->getCount('PHU');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 10;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrApproved = $this->Mgiro->getAllApproved('out');
	        
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'approvalgiro/browse',
	            'strPageTitle' => 'Approval Giro Keluar',
	            'strPage' => $strPage,
	            'giroType' => __FUNCTION__,
	            'arrApproved' => $arrApproved
	        ),$this->admlinklist->getMenuPermission(249,1)));
		}

		if ($type == '') {
			
			if ($this->input->post('smtSaveApprovalGiro') != ''){

				$arrApproval = array(
					'approval_date' => $this->input->post('txtApprovalDate'),
					'approval_type' =>  $this->input->post('appGiro')
				);

				$this->Mgiro->approve($arrApproval);

				$this->load->model('Mjournal');
				foreach ($this->input->post('appGiro') as $key => $value) {										
					$strApprovalDate = date_format(date_create(str_replace("/", "-", $this->input->post('txtApprovalDate')[$key])), "Y-m-d");
					$this->Mjournal->postTransaction('kasbankgiro_keluar',$strApprovalDate);
				}

				$this->session->set_flashdata('strMessage','Giro berhasil diapprove');
				redirect('approvalgiro/'.__FUNCTION__.'/browse');
			}

			if ($this->input->post('smtUpdateApproval') != '') {

				$intID = $this->input->post('intID');
				$arrApproval = array(
					'approval_date' => $this->input->post('txtDateApproval'),
					'approval_type' => $this->input->post('appGiro'),
				);

				$this->Mgiro->updateApproval($arrApproval);
				
				$this->load->model('Mjournal');
				$strApprovalDate = date_format(date_create(str_replace("/", "-", $this->input->post('txtApprovalDate'))), "Y-m-d");
				$this->Mjournal->postTransaction('kasbankgiro_keluar',$strApprovalDate);

				$this->session->set_flashdata('strMessage','Approval Giro berhasil diperbaharui');
				redirect('approvalgiro/'.__FUNCTION__.'/view/'.$intID);

			}

		}
	}
}