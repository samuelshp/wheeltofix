<?php
/*
PUBLIC FUNCTION:
- getTaxInvoiceByID(intID)
- getAllActiveInvoice(strKeyword)
- getAllActiveInvoicePerSalesman(intSalesmanID,strKeyword)
- getAllActiveInvoiceAndUnselect(strKeyword,strUnselect)
- getAllActiveInvoiceAndUnselectPerSalesman(intSalesmanID,strKeyword,strUnselect)
- getAllInvoice(strKeyword)
- getItems(intStartNo,intPerPage)
- getItemsByDate(strDate)
- getItemsByCustomerID(intID,strDate)
- getItemsByProductID(intProductID,intWarehouseID,strLastStockUpdate)
- getItemsForFinance()
- getAccountReceivableItems()
- getItemByID(intID)
- getPrintDataByID(intID)
- getUserHistory(intUserID,intPerPage)
- getCount()
- getDateMark(strFrom,strTo)
- getCustomers(strFrom,strTo)
- getStockSold(intProductID,intWarehouseID,strLastStockUpdate)
- getHeaderBySaleOrderID(intSaleOrderID)
- getQuantityBySaleOrderID(intSaleOrderID,intProductID)
- searchByCustomer(strCustomerName)
- add(strInvoiceSOID,intOutletID,intCustomerID,intSalesmanID,intSuppID,intWarehouseID,strDescription,intTax,intStatus, strDate,intDisc,intTipeBayar,strJatuhTempo,intJualKanvas)
- editByID(intID,strProgress,intStatus)
- editByID2(intID,strDescription,strProgress,intDiscount,intStatus,intTax)
- editStatusByID(intID,intStatus)
- editAfterPrint(intID)
- deleteByID(intID)
- getCustomerCredit(intID)
- getFakturBelumLunas()

PRIVATE FUNCTION:
- __construct()	
*/

class Minvoice extends JW_Model {

// Constructor
public function __construct() { 
	parent::__construct(); 
	$this->initialize('invoice');
}

public function getGrandTotalInvoiceByID($intID) {
    $this->setQuery(
'SELECT invo_grandtotal
FROM invoice AS i
WHERE id ='.$intID.'');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getTaxInvoiceByID($intID) {
    $this->setQuery(
'SELECT invo_tax
FROM invoice AS i
WHERE id ='.$intID.'');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllActiveInvoice($strKeyword = '') {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city, invo_payment, invo_jatuhtempo,
    invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (2)'.$strWhere.'
ORDER BY i.id ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
    
public function getAllActiveInvoicePerSalesman($intSalesmanID,$strKeyword = '') {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';
    
    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city, invo_payment, invo_jatuhtempo,
    invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (2)'.$strWhere.' AND i.id NOT IN(SELECT deit_invoice_id FROM delivery_item) AND invo_salesman_id = '.$intSalesmanID.'
ORDER BY i.id ASC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getInvoiceDataForPrintByArray($strArrayID){
    $this->setQuery(
'SELECT i.id,invo_code,cust_name,invo_status,i.cdate,cust_address,cust_city, invo_jatuhtempo, invo_arrive, invo_grandtotal,
    invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE  i.id IN('.$strArrayID.')
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllActiveInvoiceAndUnselect($strKeyword = '',$strUnselect='') {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city, invo_payment, invo_jatuhtempo,
    invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (2) AND i.id IN ('.$strUnselect.') '.$strWhere.'
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
    
public function getAllActiveInvoiceAndUnselectPerSalesman($intSalesmanID,$strKeyword = '',$strUnselect='') {
    if(!empty($strKeyword)) {
		$strKeyword = urldecode($strKeyword);
		$strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city, invo_payment, invo_jatuhtempo,
    invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE (invo_status IN (2)'.$strWhere.' AND i.id NOT IN(SELECT deit_invoice_id FROM delivery_item)) OR (invo_status IN (2) AND i.id IN ('.$strUnselect.') '.$strWhere.') AND invo_salesman_id = '.$intSalesmanID.'
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllInvoice($strKeyword = '') {
    if(!empty($strKeyword)) {
		$strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city, invo_payment, invo_jatuhtempo, invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (0,2)'.$strWhere.'
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllInvoiceByPayment($strKeyword = '') {
    if(!empty($strKeyword)) {
        $strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city,invo_grandtotal,cust_phone,sals_address,sals_phone,invo_jatuhtempo AS invo_date,s.id as sals_id
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (3)'.$strWhere.' AND i.id NOT IN
    (
        SELECT dbit_invoice_id
        FROM deliveryback_item
        WHERE dbit_paid = 1
    )
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getInvoiceForBilling($strKeyword = '') {
    if(!empty($strKeyword)) {
        $strWhere = " AND (invo_code LIKE '%$strKeyword%' OR cust_name LIKE '%$strKeyword%' OR sals_name LIKE '%$strKeyword%')";
    } else $strWhere = '';

    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city,invo_grandtotal,cust_phone,sals_address,sals_phone,invo_jatuhtempo AS invo_date,s.id as sals_id
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (3)'.$strWhere.' AND i.id NOT IN
(
    SELECT dbit_invoice_id
    FROM deliveryback_item
    WHERE dbit_paid = 1
)
ORDER BY i.id DESC');

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsForPayment($intStartNo,$intPerPage) {
    $this->setQuery(
'    SELECT id,invo_code,cust_name,sals_name,invo_status,cdate,cust_address,cust_city,invo_grandtotal,cust_phone,sals_address,sals_phone,invo_date,sals_id,tipe
    FROM(
            (
                SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city,invo_grandtotal,cust_phone,sals_address,sals_phone,invo_jatuhtempo AS invo_date,s.id as sals_id,"1" AS tipe
                FROM invoice AS i
                LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
                LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
                WHERE invo_status IN (3) AND i.id NOT IN
                    (
                        SELECT dbit_invoice_id
                        FROM deliveryback_item
                        WHERE dbit_paid = "1"
                    )
            )
            UNION
            (
                SELECT i.id,invr_code AS invo_code,cust_name,sals_name,invr_status AS invo_status,i.cdate,cust_address,cust_city,invr_grandtotal AS invo_grandtotal,cust_phone,sals_address,sals_phone,"-" AS invo_date,s.id as sals_id,"2" AS tipe
                FROM invoice_retur AS i
                LEFT JOIN jw_customer AS c ON invr_customer_id = c.id
                LEFT JOIN jw_salesman AS s ON invr_salesman_id = s.id
                WHERE invr_status IN (2)
            )
    ) AS t
    ORDER BY cdate DESC, id DESC LIMIT '.$intStartNo.', '.$intPerPage.'');
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItemsForBilling($intStartNo,$intPerPage) {
    $this->setQuery(
'SELECT i.id,invo_code,cust_name,sals_name,invo_status,i.cdate,cust_address,cust_city,invo_grandtotal,cust_phone,sals_address,sals_phone,invo_jatuhtempo AS invo_date,s.id as sals_id
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
WHERE invo_status IN (3) AND invo_jatuhtempo <= CAST(CURRENT_TIMESTAMP AS DATE)
AND i.id NOT IN
(
    SELECT dbit_invoice_id
    FROM deliveryback_item
    WHERE dbit_paid = "1"
)
ORDER BY i.cdate DESC, i.id DESC LIMIT '.$intStartNo.', '.$intPerPage.'');
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getItems($intStartNo,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, invo_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invo_code,invo_discount, invo_payment, invo_jatuhtempo, invo_jualkanvas,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp,invo_customer_name, invo_description, invo_payment_description, invo_payment
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
ORDER BY i.cdate DESC, i.id DESC LIMIT $intStartNo, $intPerPage");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByDate($strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, invo_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invo_code,invo_discount, invo_payment, invo_jatuhtempo, invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
WHERE DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByCustomerID($intID,$strDate) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, invo_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invo_code,invo_discount, invo_payment, invo_jatuhtempo, invo_jualkanvas
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
WHERE invo_customer_id = $intID AND DATE(i.cdate) = DATE('$strDate')
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsByProductID($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	
	$this->setQuery(
"SELECT ii.id, cust_name, cust_address, cust_city, i.cdate AS invo_date, invo_status, invi_quantity1
FROM invoice AS i
INNER JOIN invoice_item AS ii ON invi_invoice_id = i.id
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
WHERE invi_product_id = $intProductID AND invo_warehouse_id = $intWarehouseID AND i.cdate > '$strLastStockUpdate'
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemsForFinance() {
	$this->setQuery(
"SELECT i.id,cust_name AS name,i.cdate AS date
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
WHERE invo_status NOT IN (1,3) AND i.id NOT IN (
	SELECT fina_transaction_id FROM finance WHERE fina_type = 1
)
ORDER BY i.cdate DESC, i.id DESC");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getAccountReceivableItems() {
	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, cust_name, cust_address, cust_city, cust_phone, ware_name
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
WHERE invo_status IN (0,2)
ORDER BY i.cdate DESC, i.id DESC");
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getItemByID($intID) {
	if($intID == '') $strWhere = "ORDER BY i.invo_code DESC,i.cdate DESC LIMIT 0,1";
	else $strWhere = "
WHERE i.id = $intID
ORDER BY i.cdate DESC, i.id DESC";

    $this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, invo_description, invo_progress, invo_status, invo_locked, cust_name, cust_address, cust_city, cust_phone, ware_name, invo_code, invo_soid, sals_code, sals_name,invo_discount, invo_payment, invo_payment_description, invo_payment_value, invo_jatuhtempo, invo_customer_id as cust_id, invo_jatuhtempo,cust_outlettype,invo_supplier_id,w.id AS ware_id,cust_markettype,cust_internal,invo_jualkanvas,cust_limitkredit,invo_grandtotal,invo_arrive,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp,invo_soid,inor_code,invo_customer_name
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
LEFT JOIN jw_salesman AS sl on invo_salesman_id = sl.id
LEFT JOIN invoice_order AS so ON invo_soid = so.id
$strWhere");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Array');
	else return false;
}
    
public function getPrintDataByID($intID = 0) {
    if($intID == 0) $strWhere = "ORDER BY i.cdate ASC, i.id ASC DESC LIMIT 0,1";
    else $strWhere = "WHERE i.id = $intID";
	
	$this->setQuery(
"SELECT invo_jatuhtempo,invo_jualkanvas,invo_payment, sals_name,sals_code, ware_name, i.cdate AS invo_date, kota_title,kcmt_title
FROM invoice AS i
LEFT JOIN (
    SELECT cc.id,kota_title,kcmt_title
    FROM jw_customer AS cc
    LEFT JOIN jw_kota AS ck ON cc.cust_kota_id = ck.id
    LEFT JOIN jw_kecamatan AS ckc ON cc.cust_kecamatan_id = ckc.id
    ) AS c ON invo_customer_id = c.id
LEFT JOIN jw_salesman AS s ON invo_salesman_id = s.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
$strWhere");

    if($this->getNumRows() > 0) return $this->getNextRecord('Array');
    else return false;
}

public function getUserHistory($intUserID,$intPerPage) {
	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_status, cust_name, cust_address, cust_city,invo_code
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
WHERE (i.cby = $intUserID OR i.mby = $intUserID)
ORDER BY i.cdate DESC, i.id DESC LIMIT 0, $intPerPage");
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCount2() {

    $this->setQuery(
'    SELECT id,kode
    FROM(
            (
                SELECT i.id,invo_code AS kode
                FROM invoice AS i
                WHERE invo_status IN (3) AND i.id NOT IN
                    (
                        SELECT dbit_invoice_id
                        FROM deliveryback_item
                        WHERE dbit_paid = "1"
                    )
            )
            UNION
            (
                SELECT i.id,invr_code AS kode
                FROM invoice_retur AS i
                WHERE invr_status IN (2)
            )
    ) AS t
    ORDER BY id DESC');

    return $this->getNumRows();
}

public function getCountForBilling() {

    $this->setQuery(
'SELECT i.id
FROM invoice AS i
WHERE invo_status IN (3) AND invo_jatuhtempo <= CAST(CURRENT_TIMESTAMP AS DATE)
AND i.id NOT IN
(
SELECT dbit_invoice_id
FROM deliveryback_item
WHERE dbit_paid = 1
)
ORDER BY i.id ASC');

    return $this->getNumRows();
}

public function getCount() {
	$this->dbSelect('id');

	return $this->getNumRows();
}

public function getDateMark($strFrom,$strTo) {
	$this->dbSelect('DISTINCT DATE(cdate) as invo_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'cdate ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getCustomers($strFrom,$strTo) {
	$this->dbSelect('DISTINCT invo_customer_id,DATE(cdate) AS invo_date',"DATE(cdate) >= DATE('$strFrom') AND DATE(cdate) <= DATE('$strTo')",'invo_customer_id ASC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getStockSold($intProductID,$intWarehouseID,$strLastStockUpdate) {
	$strLastStockUpdate = formatDate2($strLastStockUpdate,'Y-m-d H:i:s');
	$intTotalStock = 0; 
	
	$this->setQuery(
"SELECT invi_quantity
FROM invoice AS i
INNER JOIN invoice_item AS ii ON invi_invoice_id = i.id
WHERE invi_product_id = $intProductID AND invo_warehouse_id = $intWarehouseID AND invo_status IN (2,3) AND i.cdate > '$strLastStockUpdate'");
	
	$arrData = $this->getQueryResult('Array');
	for($i = 0; $i < count($arrData); $i++) $intTotalStock += $arrData[$i]['invi_quantity'];
	
	return $intTotalStock;
}

public function getHeaderBySaleOrderID($intSaleOrderID) {
	$this->dbSelect('id,cdate AS invo_date',"invo_soid = $intSaleOrderID",'invo_date DESC');
	
	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function getQuantityBySaleOrderID($intSaleOrderID,$intProductID) {
	// Get invoice product quantity that involved with the sale order
	$this->setQuery(
"SELECT SUM(invi_quantity) AS sum_qty
FROM invoice AS i
INNER JOIN invoice_item AS ii ON invi_invoice_id = i.id
WHERE i.invo_soid = $intSaleOrderID AND invi_product_id = $intProductID");
	
	if($this->getNumRows() > 0) return $this->getNextRecord('Object')->sum_qty;
	else false;
}

public function searchByCustomer($strCustomerName, $arrSearchDate, $intStatus) {
    $strWhere = '';
    if(!empty($strCustomerName)) {
        $strCustomerName = urldecode($strCustomerName);
        $strWhere .= " AND (cust_name LIKE '%{$strCustomerName}%')";
    }
    if(!empty($arrSearchDate[0])) {
        if(!empty($arrSearchDate[1])) $strWhere .= " AND (i.cdate BETWEEN '{$arrSearchDate[0]}' AND '{$arrSearchDate[1]}')";
        else $strWhere .= " AND (i.cdate = '{$arrSearchDate[0]}')";
    }
    if($intStatus >= 0) $strWhere .= " AND (invo_status = {$intStatus})";

	$this->setQuery(
"SELECT i.id, i.cdate AS invo_date, invo_tax, invo_status, cust_name, cust_address, cust_city, cust_phone, ware_name, invo_code,invo_discount, invo_payment, invo_jatuhtempo, invo_jualkanvas,cust_nppkp,cust_namapkp,cust_alamatpkp,cust_npwp,invo_customer_name, invo_description, invo_payment_description, invo_payment
FROM invoice AS i
LEFT JOIN jw_customer AS c ON invo_customer_id = c.id
LEFT JOIN jw_warehouse AS w ON invo_warehouse_id = w.id
WHERE i.id > 0{$strWhere}
ORDER BY i.cdate DESC, i.id DESC");

	if($this->getNumRows() > 0) return $this->getQueryResult('Array');
	else return false;
}

public function add($strInvoiceSOID,$intOutletID,$intCustomerID,$intSalesmanID,$intSuppID,$intWarehouseID,$strDescription,$intTax,$intStatus,$strDate,$intDisc,$intTipeBayar,$strJatuhTempo,$intJualKanvas,$strCustName = '',$strCustomCode = '',$strPaymentValue = 0) {
    if($intTipeBayar == 1) $intStatus = 3;
    if(empty($intCustomerID)) $intCustomerID = 1;
    if(empty($strCustName)) {
        $this->setQuery("SELECT cust_name FROM jw_customer WHERE id = $intCustomerID");
        $arrCustomer = $this->getNextRecord('Array');
        $strCustName = $arrCustomer['cust_name'];
    }

    $arrData = array(
        'invo_soid' => !empty($strInvoiceSOID) ? $strInvoiceSOID : 0,
        'invo_code' => !empty($strCustomCode) ? $strCustomCode : '',
        'invo_outlet_id' => $intOutletID,
        'invo_jualkanvas' => $intJualKanvas,
        'invo_customer_id' => $intCustomerID,
        'invo_customer_name' => $strCustName,
        'invo_salesman_id' => $intSalesmanID,
        'invo_supplier_id' => $intSuppID,
        'invo_branch_id' => $this->session->userdata('intBranchID'),
        'invo_warehouse_id' => !empty($intWarehouseID) ? $intWarehouseID : $this->session->userdata('intWarehouseID'),
        'invo_description' => $strDescription,
        'invo_discount' => $intDisc,
        'invo_tax' => $intTax,
        'invo_payment' => $intTipeBayar,
        'invo_jatuhtempo' => $strJatuhTempo,
        'invo_status' => $intStatus,
        'cdate' => formatDate2(str_replace('/','-',$strDate),'Y-m-d H:i')
    );

    if($intTipeBayar == 3) {
        $arrData['invo_payment_description'] = '';
        if(!empty($_POST['txaPaymentName'])) $arrData['invo_payment_description'] .= $_POST['txaPaymentName'];
        if(!empty($_POST['txaPaymentDebitCard'])) $arrData['invo_payment_description'] .= PHP_EOL.$_POST['txaPaymentDebitCard'];
    } elseif($intTipeBayar == 4) {
        $arrData['invo_payment_description'] = '';
        if(!empty($_POST['txaPaymentName'])) $arrData['invo_payment_description'] .= $_POST['txaPaymentName'];
        if(!empty($_POST['txaPaymentTransferAccount'])) $arrData['invo_payment_description'] .= PHP_EOL.$_POST['txaPaymentTransferAccount'];
    }

    if(!empty($strPaymentValue)) $arrData['invo_payment_value'] = $strPaymentValue;

    $intInsertedID = $this->dbInsert($arrData);

    if(getTinyDbValue('jwdefaultconfig', 'JW_CODE_FORMAT') != 'trios' &&
        getTinyDbValue('jwdefaultconfig', 'JW_INVOICE_USE_CODE_FORMAT') == 'yes' &&
        empty($strCustomCode)) $this->dbUpdate(array(
            'invo_code' => generateTransactionCode(date('Y-m-d'), $intInsertedID, 'invoice', TRUE)
        ), 'id = '.$intInsertedID);
    return $intInsertedID;
}

public function editByID($intID,$strProgress='',$intStatus) {
    $arrUpdate = array('invo_status' => $intStatus);
    if(!empty($strProgress)) $arrUpdate['invo_progres'] = $strProgress;
	return $this->dbUpdate($arrUpdate,"id = $intID");
}



public function editByID2($intID,$strDescription,$strProgress,$intDiscount,$intStatus,$intTax) {
    return $this->dbUpdate(array(
            'invo_description' => $strDescription,
            'invo_progress' => $strProgress,
            'invo_discount' => $intDiscount,
            'invo_tax' => $intTax,
            'invo_status' => $intStatus),
        "id = $intID");
}

public function editByID3($intID,$dateArrive,$intPayment,$status) {
    return $this->dbUpdate(array(
            'invo_arrive' => formatDate2(str_replace('/','-',$dateArrive),'Y-m-d H:i'),
            'invo_status' => $status,
            'invo_payment' => $intPayment),
        "id = $intID");
}

public function editByID4($intID,$dateArrive,$status) {
    return $this->dbUpdate(array(
            'invo_arrive' => formatDate2(str_replace('/','-',$dateArrive),'Y-m-d H:i'),
            'invo_status' => $status),
        "id = $intID");
}

public function editStatusByID($intID,$intStatus) {
	return $this->dbUpdate(array(
		'invo_status' => $intStatus),"id = $intID");
}

public function editAfterPrint($intID) {
	return $this->dbUpdate(array(
		'invo_status' => '2'),"id = $intID AND invo_status = '3'");
}

public function deleteByID($intID) {
	return $this->dbDelete("id = $intID");
}

public function lockItemsByID($strIDs) {
    return $this->dbUpdate(array(
        'invo_status' => 3,
        'invo_locked' => 2
    ),"id IN ($strIDs)");
}

/*
public function getFakturBelumLunas() {
    $this->setQuery(
"SELECT invo_code, i.cdate as invoice_date, s.sals_code, s.sals_name,
	o.outl_title
FROM invoice i
	LEFT JOIN jw_salesman s ON (s.id = i.invo_salesman_id)
	LEFT JOIN jw_outlet o ON (o.id = i.invo_outlet_id)
ORDER BY i.cdate DESC");

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}
*/

public function getCustomerCredit($intID) {
    $this->setQuery(
"SELECT i.id,invo_grandtotal
FROM invoice AS i
WHERE invo_status IN (2,3) AND invo_customer_id  = $intID AND i.id");
//  NOT IN ( SELECT clam_sales_id FROM claim WHERE clam_type = 2 )

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function getAllAddCostItem($nama){
    $this->initialize('subkontrak_nonmaterial');
    $this->setQuery(
        "SELECT sknm.kategori, sknm.amount, sknm.id
        FROM subkontrak_nonmaterial as sknm
        WHERE sknm.kategori LIKE '%$nama%'"
    );

    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

public function addDataSementara($kode, $supplier, $total, $tanggal){
    echo $total;
    die();
    $this->initialize('purchase_invoice');
    return $this->dbInsert(array(
        'pinv_code' => $kode,
        'pinv_supplier_id' => $supplier,
        'pinv_grandtotal' => $total,
        'cdate' => $tanggal
    ));
}

public function getSupplierByName($nama_supplier){
    $this->initialize('jw_supplier');
    $this->setQuery(
    'SELECT id
    FROM jw_supplier
    WHERE supp_name = "'.$nama_supplier.'" ');
        
    if($this->getNumRows() > 0) return $this->getQueryResult('Array');
    else return false;
}

}

/* End of File */