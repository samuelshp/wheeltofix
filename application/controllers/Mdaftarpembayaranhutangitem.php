<?php
class Mdaftarpembayaranhutangitem extends JW_Model {

	// Constructor
	public function __construct() {
		parent::__construct();
		$this->initialize('daftar_pembayaran_hutang_item');
    }

    public function getItemsByID($id)
    {
        $this->setQuery(
            "SELECT *
            FROM (
                (select dph_item.id as item_id, dphi_dphu_id, dpay_code as code , dphi_reff_type, dphi_reff_id, dp.dpay_terbayar as dummy_pph, supp_name, dp.dpay_amount as total, dph_item.dphi_amount_rencanabayar, dp.dpay_description from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join downpayment as dp on dph_item.dphi_reff_id = dp.id
                left join jw_supplier as supp on dp.dpay_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'DP')
                UNION ALL
                (select dph_item.id as item_id, dphi_dphu_id, pinv_code as code, dphi_reff_type, dphi_reff_id, pi.pinv_pph as dummy_pph, supp_name, pi.pinv_grandtotal as total, dph_item.dphi_amount_rencanabayar, pi.pinv_description from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id
                left join purchase_invoice as pi on dph_item.dphi_reff_id = pi.id
                left join jw_supplier as supp on pi.pinv_supplier_id = supp.id
                where dph_item.dphi_reff_type = 'PI')
                UNION ALL
                (select 
                dph_item.id as item_id, dphi_dphu_id, txou_code as code, dphi_reff_type, dphi_reff_id, txod_pph as dummy_pph, supp_name, txod_totalbersih as total, dph_item.dphi_amount_rencanabayar, txod_description
                from daftar_pembayaran_hutang as dph 
                left join daftar_pembayaran_hutang_item as dph_item on dph.id = dph_item.dphi_dphu_id   
                left join transaction_out as txou on txou.id = dph_item.dphi_reff_id
                left join transaction_out_detail as txod on txou.id = txod.txod_txou_id
                left join jw_supplier as supp on txod.txod_supp_id = supp.id
                where dph_item.dphi_reff_type = 'NKS' OR dph_item.dphi_reff_type = 'NDS')
                ) tblUnion
            WHERE dphi_dphu_id = '$id'
            "
        );        

        if($this->getNumRows() > 0) return $this->getQueryResult('Array');
		else return false;
    }
    
    public function add($dphi_dphu_id, $dphi_reff_type, $dphi_pi_id, $dphi_amount_rencanabayar,$dphi_sisabayar, $dphi_twoweek_status)
    {
        return $this->dbInsert(array(
            'dphi_dphu_id'=> $dphi_dphu_id,
            'dphi_reff_type' => $dphi_reff_type,
            'dphi_reff_id' => $dphi_pi_id,             
            'dphi_amount_rencanabayar' => $dphi_amount_rencanabayar,
            'dphi_sisabayar' => $dphi_sisabayar,
            'dphi_2week_status' => $dphi_twoweek_status
        ));
    }

    public function delete($id)
    {
        //hitung ulang amount total
        $this->setQuery("
            SELECT (dphu.dphu_amount-pi.pinv_grandtotal) AS amount_baru , dph_item.dphi_dphu_id
            FROM daftar_pembayaran_hutang_item as dph_item         
            LEFT JOIN daftar_pembayaran_hutang as dphu ON dph_item.dphi_dphu_id = dphu.id
            LEFT JOIN purchase_invoice as pi ON dph_item.dphi_pi_id = pi.id
            WHERE dph_item.id = '".$id."'
        ");
        
        $record = $this->getNextRecord('Array');


        //update grandtotal baru
        $this->initialize('daftar_pembayaran_hutang');
        $this->dbUpdate(array(
            'dphu_amount' => $record['amount_baru'],            
        ),"id='".$record['dphi_dphu_id']."'");

        $this->initialize('daftar_pembayaran_hutang_item');
        return $this->dbDelete("id = '$id'");
        //4914250
    }

    public function deleteItem($intItemID)
    {
        return $this->dbDelete("id=$intItemID");
    }

    public function updateRencanaBayar($intRencanaBayar, $strID)
    {  
        return $this->dbUpdate(array('dphi_amount_rencanabayar' => $intRencanaBayar), "id = $strID");        
    }

    public function updateTerbayarPI($intPinvID, $amount, $type)
    {
        // $this->initialize('purchase_invoice');
        if ($type == "add") {
            $stmt = "pinv_terbayar + $amount";
        }else{
            $stmt = "pinv_terbayar - $amount";
        }
        return $this->setQuery("UPDATE purchase_invoice SET pinv_terbayar = ($stmt) WHERE id = $intPinvID");
       // return  $this->dbUpdate(array(
       //          'pinv_terbayar' => $amount
       //  ),"id=$intPinvID");
    }

    public function updateTerbayar($intDPHid)
    {
        $this->setQuery("SELECT dphi_reff_type, dphi_reff_id, dphi_amount_rencanabayar FROM daftar_pembayaran_hutang_item WHERE dphi_dphu_id = '$intDPHid' ");

        if($this->getNumRows() > 0) $result = $this->getQueryResult('Array');
        else $result = false;        

        foreach ($result as $item) {
            if ($item) {
                switch ($item['dphi_reff_type']) {
                    case 'PI': $strTableName = 'purchase_invoice'; $strFieldStatus = 'pinv_status'; break;
                    case 'NKS': $strTableName = 'transaction_out'; $strFieldStatus = 'txou_status_trans'; break;
                    case 'DP': $strTableName = 'downpayment'; $strFieldStatus = 'dpay_status'; break;
                }
                
                $return = $this->setQuery("UPDATE ".$strTableName." SET ".$strFieldStatus." = '4' WHERE id = '".$item['dphi_reff_id']."'");
                $return = $this->setQuery("UPDATE daftar_pembayaran_hutang SET dphu_status = 4 WHERE id = ".$intDPHid);

            } else {
                $return = false;
            }
        }

        $result = $return;
        return $result;
        
    }

    public function updateStatus($type, $intID)
    {
        switch ($type) {
            case 'PI':
                $strTableName = 'purchase_invoice'; $strField = 'pinv_status';
                break;
            case 'NKS':
                $strTableName = 'transaction_out'; $strField = 'txou_status_trans';
                break;            
        }

        return $this->setQuery("UPDATE $strTableName SET $strField = '3' WHERE id = '$intID'");
        
    }

    public function checkAvailabilityDp($intDp){
		$this->dbSelect('id,dphi_amount_rencanabayar',"dphi_reff_id = $intDp AND dphi_reff_type = 'DP' ");
		if($this->getNumRows() > 0) return $this->getNextRecord('Array');
		else return false;
	}


}