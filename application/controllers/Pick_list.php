<?php
/**
 * 
 */
class Pick_list extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->_CI =& get_instance();
		if($this->session->userdata('strAdminUserName') == '') redirect();
		
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
	
		$this->load->model('Mpick_list');
	
		$this->_getMenuHelpContent(41,true,'adminpage');
	}

	public function browse($intPage = 0)
	{	    
	    $this->load->view('sia',array_merge(array(
			'strViewFile' => 'pick_list/browse',
			'arrPicklist' => $this->Mpick_list->getAllPicklist(),
	        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title')
	    ),$this->admlinklist->getMenuPermission(41,2)));
    }
    
    public function add()
	{		
		$this->load->model('Mpick_list');
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'pick_list/add',			
			'strPageTitle' => loadlanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title'),
		)));
	}
	
	public function view($intPage)
	{
		$arrPicklist = $this->Mpick_list->getAllpicklist($intPage);
				
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'pick_list/view',		
			'arrPicklist' => $this->Mpick_list->getAllPicklist(),
			'strPageTitle' => loadlanguage('admindisplay',$this->session->userdata('jw_language'),'picklist-title'),
		),$this->admlinklist->getMenuPermission(41,$arrQuotation['quot_status'])));
	}
}