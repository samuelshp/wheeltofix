<?php

class Subkontrak extends JW_Controller {

private $_CI;

public function __construct() {
    parent::__construct();
    $this->_CI =& get_instance();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
    $this->load->model('Mkontrak');
    $this->load->model('Msubkontrak');

    $this->_getMenuHelpContent(217,true,'adminpage');

    $this->priv['canEditHeader']       = false;
    $this->priv['canViewTermin']       = false;
    $this->priv['canEditTermin']       = false;
    $this->priv['canViewTeamMaterial'] = false;
    $this->priv['canEditTeamMaterial'] = false;

    // Setup Privilege
    if(in_array($_SESSION['strAdminPriviledge'], [4, 5, 6])){
        $this->priv['canEditHeader'] = true;
        $this->priv['canViewTermin'] = true;
        $this->priv['canEditTermin'] = true;
        $this->priv['canViewTeamMaterial'] = true;
        $this->priv['canEditTeamMaterial'] = true;
    }
    if(in_array($_SESSION['strAdminPriviledge'], [5, 6, 11])){
        $this->priv['canViewTeamMaterial'] = true;
        $this->priv['canEditTeamMaterial'] = true;
    }
    if(in_array($_SESSION['strAdminPriviledge'], [7])){
        $this->priv['canViewTermin']       = true;
        $this->priv['canViewTeamMaterial'] = true;
    }
}

public function index($id = null)
{        
        $this->load->model('Mstaff');       
        $this->load->model('Mtinydbvo');
        $this->load->model('Mproduct');
        $this->Mtinydbvo->initialize('admin_priviledge');//awalnya jwsettingpic        
        $availableKontrak = $this->Mkontrak->getKontrak();
        $availableJabatan = $this->Mtinydbvo->getAllData();
        $availableTeamName = $this->Mstaff->all();        
        $availableBahan = $this->Mproduct->getAllItemWithSatuan();          
        
        if($this->input->post('smtMakeKontrak') != ''){
            $this->_CI->db->trans_start();
            $arrSubkontrak = array(
                'intKontrakID' => $this->input->post('proyek_id'),
                'strSubkontKode' => $this->input->post('subkont_kode'),
                'strJob' => $this->input->post('job'),
                'strSubkontDate' => $this->input->post('subkont_date'),
                'intJobValue' => str_replace(".","",$this->input->post('job-value')),            
                'intHPPTotal' => $this->input->post('hppTotal')
            );
           
            $intAddKontrak['general'] = $this->Msubkontrak->add($arrSubkontrak);                        

            //Mengambil data subkontrak yang telah diinsert.
            $subkontrak = $this->Msubkontrak->getActiveItems(null, $arrSubkontrak['strJob'], 0,1)[0];
            
            //Termin
            if(!empty($this->input->post('jenis')))
            {
                $this->load->model('Msubkontraktermin');
                $arrDataTermin = array(
                    'jenis' => $this->input->post('jenis'),
                    'termin_amount' => $this->input->post('termin_amount'),
                    'terbayar' => $this->input->post('terbayar'),
                    'terbayar_date' => $this->input->post('terbayar_date'),
                    'due_date' => $this->input->post('due_date')
                );                            
                $intAddKontrak['termin'] = $this->Msubkontraktermin->add($arrDataTermin, $subkontrak['id']);                
            }  

            if(!empty($this->input->post('name')))
            {
                $this->load->model('Msubkontrakteam');
                $arrDataTeam = array(
                    'jabatan_id' => $this->input->post('jabatan_id'),
                    'name' => $this->input->post('name'),                
                );
                $intAddKontrak['team'] = $this->Msubkontrakteam->add($arrDataTeam, $subkontrak['id']);                
            }

            if(!empty($this->input->post('material')))
            {
                $this->load->model('Msubkontrakmaterial');
                $arrDataMaterial = array(
                    'material' => $this->input->post('material'),
                    'qty' => $this->input->post('qty'),
                    'subkontrak_terpb' => $this->input->post('subkontrak_terpb'),
                    'keterangan' => $this->input->post('keterangan'),
                    'substitusi' => $this->input->post('substitusi'),
                    'hpp' => $this->input->post('hpp'),
                    'jumlah' => $this->input->post('jumlah')
                );
                $intAddKontrak['material'] = $this->Msubkontrakmaterial->add($arrDataMaterial, $subkontrak['id']);
            }

            if(!empty($this->input->post('kategori')))
            {
                $this->load->model('Msubkontraknonmaterial');                
                $arrDataKategori = array(
                    'kategori' => $this->input->post('kategori'),
                    'amount' => $this->input->post('amount')
                );
                $intAddKontrak['nonmaterial'] = $this->Msubkontraknonmaterial->add($arrDataKategori, $subkontrak['id']);            
            }         
            $this->_CI->db->trans_complete();
            if(!in_array(0, $intAddKontrak)){
                $this->session->set_flashdata('strMessage','Data kontrak has been saved.');
                redirect('subkontrak/view/'.$subkontrak['id'], 'refresh');
            }else{
                $this->session->set_flashdata('strMessage','Failed to save Data Kontrak');
                redirect('subkontrak/', 'refresh');
            }
        }
               
    $this->load->view('sia', array(
        'strViewFile'   => 'subkontrak/add',
        'strPageTitle'  => 'Tambah Kontrak',
        'priv'          => $this->priv,        
        'availableKontrak' => $availableKontrak,
        'availableJabatan' => $availableJabatan,
        'availableTeamName' => $availableTeamName,
        'availableBahan' => $availableBahan,        
    ));
}


public function browse() {
    $arrPagination['base_url'] = site_url("subkontrak/browse?pagination=true&txtSearchValue=".$this->input->get('txtSearchValue'), NULL, FALSE);
    $arrPagination['total_rows'] = $this->Msubkontrak->getCountActiveItems(0, $this->input->get('txtSearchValue'));
    $arrPagination['per_page'] = $this->config->item('jw_item_count');
    $arrPagination['page_query_string'] = TRUE;
    $this->pagination->initialize($arrPagination);
    $strPage = $this->pagination->create_links();
    if($this->input->get('page') != '') $intPage = $this->input->get('page');
    else $intPage = 0;

    $this->load->view('sia',array(
        'strViewFile'    => 'subkontrak/browse',
        'strPage'        => $strPage,
        'strPageTitle'   => 'Kontrak',
        'subkontrakList' => $this->Msubkontrak->getActiveItems(0, $this->input->get('txtSearchValue'),$intPage,$arrPagination['per_page']),
        'priv'           => $this->priv
    ));
}

public function view($id)
{
    // checkTeamPermission('Subkontrak',$id);
    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('admin_priviledge');//awalnya jwsettingpic

    if($this->input->post('btnCopyTeam') != ''){
        $team = $this->Msubkontrak->getCopyTeam($id);
        foreach ($team as $e) {
            $result = $this->Msubkontrak->checkTeam($id, $e['name'], $e['jabatan_id']);
            if($result == 0) $this->Msubkontrak->saveTeam($id, $e['name'], $e['jabatan_id']);
        }
    }

    $this->load->model('Mstaff');       
    $availableJabatan = $this->Mtinydbvo->getAllData();
    $availableTeamName = $this->Mstaff->all();
    $availableKontrak = $this->Mkontrak->getKontrak();
    $arrDataSubkontrak = $this->Msubkontrak->getSubkontrak($id);
    $arrDataSubkontrakTermin = $this->Msubkontrak->getSubkontrakTermin($id);
    $arrDataSubkontrakTeam = $this->Msubkontrak->getSubkontrakTeam($id);
    $arrDataMaterial = $this->Msubkontrak->getSubkontrakMaterial($id);
    $arrDataNonMaterial = $this->Msubkontrak->getSubkontrakNonMaterial($id);
    $availableSubstitusi = $this->Msubkontrak->getAvailableSubstitusi($id);    
    $intHPPSubkontrakMaterial = $this->Msubkontrak->getSubkontrakHPPMaterial($id);
    $intHPPSubkontrakNonMaterial = $this->Msubkontrak->getSubkontrakHPPNonMaterial($id);

    if($arrDataSubkontrak['hpp'] == ''){

    $intHPP = $intHPPSubkontrakMaterial['hpp_material'] + $intHPPSubkontrakNonMaterial['hpp_nonmaterial'];
    $arrDataSubkontrak['hpp'] = $intHPP;
    
    }
    
    $this->load->model('Mproduct');
    $availableBahan = $this->Mproduct->getAllItemWithSatuan();        

    if($this->input->post('btnUpdateSubkontrak') != ''){        
        $this->_CI->db->trans_start();
        //General Info
        $subkontrak = $this->Msubkontrak->getSubkontrak($id);        
        $intKontrakID       = $this->input->post('proyek_id');
        $strJob             = $this->input->post('job');
        $intJobValue        = str_replace(".","",$this->input->post('job-value'));
        $strSubkontDate     = $this->input->post('subkont-date');
        $strSubkontKode     = $this->input->post('subkont_kode');

        $intUpdate = $this->Msubkontrak->updateSubkontrak($intKontrakID, $strJob, $intJobValue, $strSubkontDate, $strSubkontKode, $id);

        //Termin & Rencana Pembayaran        
        $this->load->model('Msubkontraktermin');

        if(!empty($this->input->post('jenis'))){
            foreach($this->input->post('jenis') as $key => $value)
            { 
                $row_id            = isset($arrDataSubkontrakTermin[$key]['id']) ? $arrDataSubkontrakTermin[$key]['id'] : 'null' ; 
                $subkontrak_id = $subkontrak['subkont_id']; 
                $jenis         = $value;
                $intTerbayar      = str_replace(".","", $this->input->post('terbayar')[$key]);
                $strDateTerbayar = $this->input->post('terbayar_date')[$key];
                $intAmount        = str_replace(".","", $this->input->post('termin_amount')[$key]); 
                $strDueDate      = $this->input->post('due_date')[$key]; 
                $status        = 2;
                $mby           = $this->session->userdata('strAdminID');
                $intUpdate = $this->Msubkontraktermin->update($row_id,$subkontrak_id, $jenis,$intAmount,$intTerbayar,$strDateTerbayar, $strDueDate, $mby);
            }                                   
        }

        //Team
        $this->load->model('Msubkontrakteam');
        
        if(!empty($this->input->post('name'))){
            foreach($this->input->post('name') as $key => $item)
            {                      
                $row_id                = $this->input->post('team-rowID')[$key];
                $intSubkontrak_id      = $subkontrak['subkont_id']; 
                $intPegawaiID          = $this->input->post('name')[$key];
                $intJabatan_id         = $this->input->post('jabatan_id')[$key];
                $intStatus             = 2;
                $mby                = $this->session->userdata('strAdminID');            
                $intUpdate = $this->Msubkontrakteam->update($row_id,$intSubkontrak_id, $intPegawaiID, $intJabatan_id, $mby, $intStatus);             
            }
        }        
        
        //Material        
        $this->load->model('Msubkontrakmaterial');
        if (!empty($this->input->post('material'))) {
            foreach($this->input->post('material') as $key => $item)
            {                
                $row_id                     = $this->input->post('material-rowID')[$key];
                $intMaterialID              = $this->input->post('material')[$key];
                $intQty                     = str_replace(".","",$this->input->post('qty')[$key]);
                $doubleSubkontrakTerpb      = str_replace(".","",$this->input->post('subkontrak_terpb')[$key]);
                $strKeterangan              = $this->input->post('keterangan')[$key];
                $intSubstitusi              = $this->input->post('substitusi')[$key];
                if(!is_numeric($intSubstitusi)) $intSubstitusi = 0;
                $intHPPMaterial             = str_replace(".","",$this->input->post('hpp')[$key]);
                $intJumlahMaterial          = str_replace(".","",$this->input->post('jumlah')[$key]) ?  str_replace(".","",$this->input->post('jumlah')[$key]) : ($this->input->post('qty')[$key] * $this->input->post('hpp')[$key]);
                $intUpdate = $this->Msubkontrakmaterial->update($row_id,$intSubkontrak_id, $intMaterialID, $intQty, $strKeterangan, $intSubstitusi, $intHPPMaterial, $intJumlahMaterial, $doubleSubkontrakTerpb);                
                $material[$key] = $this->input->post('keterangan')[$key];
            }      
        }
                
   
        //Non-Material       
        $this->load->model('Msubkontraknonmaterial');
        if(!empty($this->input->post('kategori')))
        {
            foreach($this->input->post('kategori') as $key => $item)
            {                
                $row_id        = $this->input->post('nonmaterial-rowID')[$key];
                $intSubkontrak_id      = $subkontrak['subkont_id']; 
                $strKategori   = $this->input->post('kategori')[$key];
                $intAmount     = str_replace(".","",$this->input->post('amount')[$key]);
                $non[$key] = array($row_id, $strKategori, $intAmount);
                $intUpdate = $this->Msubkontraknonmaterial->update($row_id, $intSubkontrak_id, $strKategori, $intAmount);
            }
        }

        if($this->input->post('teamToDelete') != ''){
            $arrData = json_decode($this->input->post('teamToDelete'));
            foreach ($arrData as $key => $value) {
                if ($value->tipe == 'termin') {
                    $this->Msubkontraktermin->deleteItem($value->rowid);
                } elseif ($value->tipe == 'team') {
                    $this->Msubkontrakteam->deleteItem($value->rowid);
                } elseif($value->tipe == 'material') {
                    $this->Msubkontrakmaterial->deleteItem($value->rowid);
                } elseif($value->tipe == 'nonmaterial') {
                    $this->Msubkontraknonmaterial->deleteItem($value->rowid);
                }
            }            
        }        
        
        $this->_CI->db->trans_complete();
        if ($intUpdate) {
            $this->session->set_flashdata('strMessage','Data kontrak has been updated.');
            redirect('subkontrak/view/'.$id, 'refresh');
        }
    }    

    
    $this->load->view('sia',array(
        'strViewFile'      => 'subkontrak/view',
        'strPageTitle'     => 'Kontrak',
        'intSubkontrakID' => $id,
        'availableKontrak' => $availableKontrak,
        'availableJabatan' => $availableJabatan,
        'availableTeamName' => $availableTeamName,
        'availableBahan'   => $availableBahan,
        'availableSubstitusi' => $availableSubstitusi,
        'arrDataSubkontrak'=> $arrDataSubkontrak,
        'arrDataSubkontrakTermin' => $arrDataSubkontrakTermin, 
        'arrDataSubkontrakTeam' => $arrDataSubkontrakTeam,
        'arrDataMaterial' => $arrDataMaterial,
        'arrDataNonMaterial' => $arrDataNonMaterial,   
        'intHPPSubkontrakMaterial' => (!empty($intHPPSubkontrakMaterial['hpp_material']) ? $intHPPSubkontrakMaterial['hpp_material'] : 0), 
        'intHPPSubkontrakNonMaterial' => (!empty($intHPPSubkontrakNonMaterial['hpp_nonmaterial']) ? $intHPPSubkontrakNonMaterial['hpp_nonmaterial'] : 0), 
        'intJumlahTeam' => sizeof($arrDataSubkontrakTeam),
        'priv' => $this->priv,
    ));
}

}