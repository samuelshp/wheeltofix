<?php
/**
 * 
 */
class Jurnal extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));		
		$this->load->model('Mjournal');	
		$this->load->model('Mkontrak');
	}

	public function memorial($type = '', $intID = 0)
	{
		$this->_getMenuHelpContent(255,true,'adminpage');	
		if ($type == "add") {			
			$this->load->model("Maccount");
	        $arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
	        $arrProject = $this->Mkontrak->getKontrak();
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'jurnal/add',
	            'strPageTitle' => "Jurnal Memorial",
	            'strLink' => __FUNCTION__,	            
	            'intType' => 1,
	            'arrCOA' => $arrCOA,
	            'arrProject' => $arrProject
	        ),$this->admlinklist->getMenuPermission(255,1)));
		}

		if($type == "view") {			
			$this->load->model("Maccount");
	        $arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
	        $arrProject = $this->Mkontrak->getKontrak();	        
	        $arrJournal = $this->Mjournal->getByID($intID);
			$this->load->view('sia',array_merge(array(
	            'strViewFile' => 'jurnal/view',
	            'strPageTitle' => "Jurnal Memorial",
	            'strLink' => __FUNCTION__,	            
	            'intType' => 1,
	            'arrCOA' => $arrCOA,
	            'arrProject' => $arrProject,
	            'arrJournal' => $arrJournal
	        ),$this->admlinklist->getMenuPermission(255,1)));
		}

		if ($type == "browse"){		
			$arrPagination['base_url'] = site_url("jurnal/memorial/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mjournal->getCount('JM');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();	        
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrJournal = $this->Mjournal->getAllJournal('JM', $intPage,$arrPagination['per_page']);

	        if ($this->input->post('subSearch') != '') {

	        	$arrDataSearch = array(
	        		'jrnl_date' => $this->input->post('txtSearchTgl'),
	        		'jrnl_code' => $this->input->post('txtSearchNo')
	        	);

	        	$arrJournal = $this->Mjournal->searchBy('JM',$arrDataSearch);
		        $strPage = '';

		        $strBrowseMode = '<a href="'.site_url('jurnal/memorial/browse', NULL, FALSE).'" class="btn btn-link">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrJournal) ? count($arrJournal) : '0')." records).";
	        }	       
  
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'jurnal/browse',
	            'strPageTitle' => 'Jurnal Memorial',
	            'strPage' => $strPage,
	            'strBrowseMode' => $strBrowseMode,
	            'intType' => 1,	
	            'strLink' => __FUNCTION__,
	            'arrJournal' => $arrJournal
	        ),$this->admlinklist->getMenuPermission(255,1)));
		}

		if ($type == "") {
			
			if ($this->input->post('btnSaveJurnal') != '') {
				
				$arrJurnal = array(
					'jrnl_date' => $this->input->post('txtDateBuat'),
					'jrnl_acco_id' => $this->input->post('intCOA'),
					'jrnl_ref_id' => $this->input->post('strProyek'),
					'jrnl_keterangan' => $this->input->post('strKeterangan'),
					'jrnl_debet' => $this->input->post('intAmountDebet'),
					'jrnl_kredit' => $this->input->post('intAmountCredit')
				);			

				$intInsertMemorial = $this->Mjournal->insertJournalMemorial($arrJurnal);

				if ($intInsertMemorial) {
					redirect('jurnal/memorial/browse');
				}else{
					redirect('jurnal/memorial/add');
				}
				
			}

			if ($this->input->post('smtUpdateJurnal') != '') {				
				$intID = $this->input->post('intID');
				$arrJurnal = array(
					'jrnl_date' => $this->input->post('txtDateBuat'),
					'jrnl_acco_id' => $this->input->post('intCOA'),
					'jrnl_ref_id' => $this->input->post('strProyek'),
					'jrnl_keterangan' => $this->input->post('strKeterangan'),
					'jrnl_debet' => $this->input->post('intAmountDebet'),
					'jrnl_kredit' => $this->input->post('intAmountCredit')
				);	

				$intUpdate = $this->Mjournal->update($arrJurnal,$intID);				

				if ($intUpdate) {
					redirect('jurnal/'.__FUNCTION__.'/view/'.$intID);
				}else{
					redirect('jurnal/'.__FUNCTION__.'/view/'.$intID);
				}
			}

			if ($this->input->post('smtDeleteJurnal') != '') {

				$strCode = $this->input->post('strCode');				
				$intDeleteMemorial = $this->Mjournal->deleteJournalMemorial($strCode);

				if ($intDeleteMemorial) {
					redirect('jurnal/memorial/browse');
				}else{
					redirect('jurnal/memorial/view/'.$this->input->post('intID'));
				}
			}

		}
	}

	public function penyesuaian($type = '', $intID = 0)
	{
		$this->_getMenuHelpContent(256,true,'adminpage');	
		if ($type == "add") {			
			$this->load->model("Maccount");
	        $arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');
	    
	        $this->load->view('sia',array_merge(array(
	            'strViewFile' => 'jurnal/add',
	            'strPageTitle' => "Jurnal Penyesuaian",
	            'strLink' => __FUNCTION__,	            
	            'intType' => 1,
	            'arrCOA' => $arrCOA
	        ),$this->admlinklist->getMenuPermission(256,1)));
		}

		if($type == "view") {			
			$this->load->model("Maccount");
	        $arrCOA = $this->Maccount->getAllAccount('',' AND acco_detail = 2');   
	        $arrJournal = $this->Mjournal->getByID($intID);
			$this->load->view('sia',array_merge(array(
	            'strViewFile' => 'jurnal/view',
	            'strPageTitle' => "Jurnal Penyesuaian",
	            'strLink' => __FUNCTION__,	            
	            'intType' => 1,
	            'arrCOA' => $arrCOA,
	            'arrJournal' => $arrJournal
	        ),$this->admlinklist->getMenuPermission(256,1)));
		}

		if ($type == "browse"){		
			$arrPagination['base_url'] = site_url("jurnal/penyesuaian/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mjournal->getCount('JP');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();	        
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrJournal = $this->Mjournal->getAllJournal('JP', $intPage,$arrPagination['per_page']);

	        if ($this->input->post('subSearch') != '') {

	        	$arrDataSearch = array(
	        		'jrnl_date' => $this->input->post('txtSearchTgl'),
	        		'jrnl_code' => $this->input->post('txtSearchNo')
	        	);

	        	$arrJournal = $this->Mjournal->searchBy('JP',$arrDataSearch);
		        $strPage = '';

		        $strBrowseMode = '<a href="'.site_url('jurnal/penyesuaian/browse', NULL, FALSE).'" class="btn btn-link">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrJournal) ? count($arrJournal) : '0')." records).";
	        }	       
  
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'jurnal/browse',
	            'strPageTitle' => 'Jurnal Penyesuaian',
	            'strPage' => $strPage,
	            'strBrowseMode' => $strBrowseMode,
	            'intType' => 1,	
	            'strLink' => __FUNCTION__,
	            'arrJournal' => $arrJournal
	        ),$this->admlinklist->getMenuPermission(256,1)));
		}

		if ($type == "") {
			
			if ($this->input->post('btnSaveJurnal') != '') {
				
				$arrJurnal = array(
					'jrnl_date' => $this->input->post('txtDateBuat'),
					'jrnl_acco_id' => $this->input->post('intCOA'),
					'jrnl_ref_id' => $this->input->post('strProyek'),
					'jrnl_keterangan' => $this->input->post('strKeterangan'),
					'jrnl_debet' => $this->input->post('intAmountDebet'),
					'jrnl_kredit' => $this->input->post('intAmountCredit')
				);			

				$intInsertPenyesuaian = $this->Mjournal->insertJournalPenyesuaian($arrJurnal);

				if ($intInsertPenyesuaian){
					redirect('jurnal/penyesuaian/browse');
				}else{
					redirect('jurnal/penyesuaian/add');
				}
				
			}

			if ($this->input->post('smtUpdateJurnal') != '') {				
				$intID = $this->input->post('intID');
				$arrJurnal = array(
					'jrnl_date' => $this->input->post('txtDateBuat'),
					'jrnl_acco_id' => $this->input->post('intCOA'),
					'jrnl_ref_id' => $this->input->post('strProyek'),
					'jrnl_keterangan' => $this->input->post('strKeterangan'),
					'jrnl_debet' => $this->input->post('intAmountDebet'),
					'jrnl_kredit' => $this->input->post('intAmountCredit')
				);			

				$intUpdate = $this->Mjournal->update($arrJurnal,$intID);

				if ($intUpdate) {
					redirect('jurnal/'.__FUNCTION__.'/view/'.$intID);
				}else{
					redirect('jurnal/'.__FUNCTION__.'/view/'.$intID);
				}
			}

			if ($this->input->post('smtDeleteJurnal') != '') {

				$strCode = $this->input->post('strCode');				
				$intDeleteMemorial = $this->Mjournal->deleteJournalMemorial($strCode);

				if ($intDeleteMemorial) {
					redirect('jurnal/penyesuaian/browse');
				}else{
					redirect('jurnal/penyesuaian/view/'.$this->input->post('intID'));
				}
			}

		}
	}

	public function persediaanakhirtahun($type = '', $intID = 0)
	{		
		if ($type == 'add') {
			$this->load->model('Mkontrak');
    		$arrProject = $this->Mkontrak->getActiveItems(0,1000);			
			$this->load->view('sia',array_merge(
				array(
				'strViewFile' => 'jurnal/persediaan_akhir_tahun/add',
	            'strPageTitle' => "Jurnal Persediaan Akhir Tahun",
	            'strLink' => __FUNCTION__,	            
	            'intType' => 1,
	            'arrProject' => $arrProject
			),$this->admlinklist->getMenuPermission(265,1)));
		}

		if ($type == 'view') {
			$arrJournal = $this->Mjournal->getByProjectID($intID);
			$this->load->view('sia',array_merge(
				array(
				'strViewFile' => 'jurnal/persediaan_akhir_tahun/view',
	            'strPageTitle' => "Detail Jurnal Persediaan Akhir Tahun",
	            'strLink' => __FUNCTION__,
	            'intType' => 1,
	            'arrJournal' => $arrJournal
			),$this->admlinklist->getMenuPermission(265,1)));
		}

		if ($type == 'browse') {
			$arrPagination['base_url'] = site_url("jurnal/memorial/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mjournal->getCount('JM');
	        $arrPagination['per_page'] = $this->config->item('jw_item_count');
	        $arrPagination['uri_segment'] = 3;
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();	        
	        $intPage = 0;
	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');
	        $arrJournal = $this->Mjournal->getAllJournal('PERSEDIAAN AKHIR TAHUN', $intPage,$arrPagination['per_page']);

	        if ($this->input->post('subSearch') != '') {

	        	$arrDataSearch = array(
	        		'jrnl_date' => $this->input->post('txtSearchTgl'),
	        		'jrnl_code' => $this->input->post('txtSearchNo')
	        	);

	        	$arrJournal = $this->Mjournal->searchBy('PERSEDIAAN AKHIR TAHUN',$arrDataSearch);
		        $strPage = '';

		        $strBrowseMode = '<a href="'.site_url('jurnal/'.__FUNCTION__.'/browse', NULL, FALSE).'" class="btn btn-link">[Back]</a>';
		        $this->_arrData['strMessage'] = "Search result (".(!empty($arrJournal) ? count($arrJournal) : '0')." records).";
	        }	       
  
	        $this->load->view('sia', array_merge(array(
	            'strViewFile' => 'jurnal/persediaan_akhir_tahun/browse',
	            'strPageTitle' => 'Jurnal Persediaan Akhir Tahun',
	            'strPage' => $strPage,
	            'strBrowseMode' => $strBrowseMode,
	            'intType' => 1,	
	            'strLink' => __FUNCTION__,
	            'arrJournal' => $arrJournal
	        ),$this->admlinklist->getMenuPermission(265,1)));
		}

		if ($type == '') {
			
			if ($this->input->post('btnSaveJurnal') != '') {
				
				$arrPostData = array(
					'jrnl_date' => $this->input->post('txtDateBuat'),
					'strProyek' => $this->input->post('strProyek'),
					'intAmount' => $this->input->post('intAmount')
				);

				$intInsertPersediaan = $this->Mjournal->insertJournalPersediaanAkhirTahun($arrPostData);

				if ($intInsertPersediaan){
					redirect('jurnal/penyesuaian/browse');
				}else{
					redirect('jurnal/penyesuaian/add');
				}
			}

		}
	}
}