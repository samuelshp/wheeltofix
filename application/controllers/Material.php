<?php 

use App\Models\{Ebahan};
if(!class_exists('Ebahan')) require_once(APPPATH.'models/Ebahan.php');
use App\Models\DBFacadesWrapper as DB;

class Material extends JW_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    }

    public function get($id)
    {
        $material = Ebahan::with('satuan_bayar')->where('id', $id)->first();

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($material));
    }

}