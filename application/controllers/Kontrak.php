<?php

class Kontrak extends JW_Controller {

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
    $this->load->model('Mkontrak');
    $this->load->model('Msubkontrak');
    $this->load->model('Mcustomer');

    $this->_getMenuHelpContent(216,true,'adminpage');
}

public function browse() {

    $arrPagination['base_url'] = site_url("kontrak/browse?pagination=true", NULL, FALSE);
    $arrPagination['total_rows'] = $this->Mkontrak->getCountActiveItems();
    $arrPagination['per_page'] = $this->config->item('jw_item_count');
    $arrPagination['page_query_string'] = TRUE;
    $this->pagination->initialize($arrPagination);
    $strPage = $this->pagination->create_links();
    if($this->input->get('page') != '') $intPage = $this->input->get('page');
    else $intPage = 0;

    $this->load->view('sia',array(
        'strViewFile'  => 'kontrak/browse',
        'kontrakList'  => $this->Mkontrak->getActiveItems($intPage,$arrPagination['per_page']),
        'strPage' => $strPage,
        'strPageTitle' => 'Proyek',
    ));

}

public function index() {$this->view();}

public function view($id = 0) {
    
    $arrData = array();

    if($this->input->post('subSave') == 'save') {
        
        $arrKontrak = array(
            'kont_name' => $this->input->post('kont_name'),
            'owner_id' => $this->input->post('owner_id'),
            'kont_date' => str_replace("/", "-", $this->input->post('kont_date'))
        );

        if($id = $this->Mkontrak->add($arrKontrak)) {
            $this->session->set_flashdata('strMessage','Data proyek has been created successfully.');
            redirect('kontrak/view/'.$id);
        } else {
            $this->session->set_flashdata('strMessage','Please re-check your data.');
            redirect('kontrak');
        }

    } elseif($this->input->post('subSave') == 'edit') {

        $arrKontrak = array(
            'kont_name' => $this->input->post('kont_name'),
            'owner_id' => $this->input->post('owner_id'),
            'kont_date' => str_replace("/", "-", $this->input->post('kont_date')),
        );

        if($this->Mkontrak->edit($id, $arrKontrak)) {
            $this->session->set_flashdata('strMessage','Data proyek has been updated.');
        }

        redirect('kontrak/view/'.$id);

    } elseif($this->input->post('subDelete') == 'delete') {

        if($this->Mkontrak->dbDelete("id = $id")) {
            $this->session->set_flashdata('strMessage', 'Data succesfully deleted');
            redirect('kontrak/browse');
        }
        
    }

    if($id > 0) {
        $subkontrak = $this->Msubkontrak->getActiveItems($id);
        $terbayar = 0;
        foreach($subkontrak as $e) {
            $terbayar += $e['subkontrak_terpb'];
        }

        $arrData = array(
            'object'         => $this->Mkontrak->getItemByID($id),
            'kontrakVal'     => $this->Msubkontrak->getCountActiveItems($id),
            'subkontrak'     => $subkontrak,
            'terbayar'       => $terbayar,
        );
    }

    $this->load->view('sia', array_merge($arrData, array(
        'strViewFile'    => 'kontrak/view',
        'id'             => $id,
        'availableOwner' => $this->Mcustomer->getAllData(),
        'strPageTitle'   => 'Proyek',
    ))); 
}

}