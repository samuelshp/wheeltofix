<?php
/**
 * 
 */
class Warehouse extends JW_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();
    
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		$this->load->model('Mwarehouse');		
	}

	public function add()
	{
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'warehouse/add',			
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')
		),$this->admlinklist->getMenuPermission(84,1)));
	}

	public function view($intID = 0)
	{
		$arrWarehouse = $this->Mwarehouse->getItemByID($intID);
		$arrInventories = $this->Mwarehouse->getInventoriesInWarehouse($intID);
		
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'warehouse/view',
			'arrWarehouse' => $arrWarehouse,
			'arrInventories' => $arrInventories,
			'intID' => $intID,
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')
		),$this->admlinklist->getMenuPermission(84,1)));
	}

	public function browse($intPage = 0)
	{
		if($this->input->post('subSearch') != '') {					
			$strSearchKey = $this->input->post('txtSearchValue');			
	       	$arrWarehouse = $this->Mwarehouse->getAllData($strSearchKey);		       	

	        $strPage = '';
	        $strBrowseMode = '<a class="btn btn-link" href="'.site_url('warehouse/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = "Search result (".(!empty($arrWarehouse) ? count($arrWarehouse) : '0')." records).";		       
		}else{
			$arrPagination['base_url'] = site_url("warehouse/browse?pagination=true", NULL, FALSE);
	        $arrPagination['total_rows'] = $this->Mwarehouse->getCount();
	        $arrPagination['per_page'] = 1;
	        
	        $this->pagination->initialize($arrPagination);
	        $strPage = $this->pagination->create_links();

	        $strBrowseMode = '';
	        if($this->input->get('page') != '') $intPage = $this->input->get('page');

			$arrWarehouse = $this->Mwarehouse->getAllData(null,$intPage, $arrPagination['per_page']);
		}

		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'warehouse/browse',
			'arrWarehouse' => $arrWarehouse,
			'strPage' => $strPage,
			'strBrowseMode' => $strBrowseMode,		
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'warehouse-title')
		),$this->admlinklist->getMenuPermission(84,1)));
	}

	public function index()
	{
		if ($this->input->post('smtAddWarehouse') != '') {
			$arrData = array(
				'ware_name' => $this->input->post('ware_name'),
				'ware_stock_warning' => $this->input->post('ware_stock_warn'),
				'ware_status' => $this->input->post('ware_status')
			);

			$intInsert = $this->Mwarehouse->add($arrData);

			if ($intInsert) {
				$this->session->set_flashdata('strMessage','Data Gudang berhasil ditambahkan');
				redirect("warehouse/browse/","refresh");
			}else{
				$this->session->set_flashdata('strMessage','Data Gudang gagal ditambahkan');
				redirect("warehouse/add");
			}

		}
		if ($this->input->post('smtUpdateWarehouse') != '') {			
			$intID = $this->input->post('intID');
			$arrData = array(
				'ware_name' => $this->input->post('ware_name'),
				'ware_stock_warning' => $this->input->post('ware_stock_warn'),
				'ware_status' => $this->input->post('ware_status')
			);
			
			$intUpdate = $this->Mwarehouse->update($arrData, $intID);

			if ($intUpdate) {
				$this->session->set_flashdata('strMessage','Data Gudang berhasil diperbaharui');				
			}else{
				$this->session->set_flashdata('strMessage','Data Gudang gagal diperbaharui');
			}

			redirect("warehouse/view/".$intID,"refresh");
		}
		if ($this->input->post('smtDeleteWarehouse') != '') {			
			$intID = $this->input->post('intID');
			$intDelete = $this->Mwarehouse->delete($intID);

			if ($intDelete) {
				$this->session->set_flashdata('strMessage','Data Gudang berhasil dihapus');				
			}else{
				$this->session->set_flashdata('strMessage','Data Gudang gagal dihapus');
			}

			redirect("warehouse/browse","refresh");
		}
	}
}