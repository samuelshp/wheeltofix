<?php
/**
 * Created by PhpStorm.
 * User: Chun
 * Date: 9/23/14
 * Time: 9:21 AM
 */
class Payroll extends JW_Controller {
public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->_getMenuHelpContent(65,true,'adminpage');
}

public function index() {
    $this->load->view('sia',array(
        'strViewFile' => 'payroll/index',
    ));
}

function do_upload()
{
    $config['upload_path'] = 'upload/excel';
    $config['allowed_types'] = 'xls|xlsx';
    $config['max_size']	= '1000';

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('userfile'))
    {
        $error = array('error' => $this->upload->display_errors());
    }
    else
    {
        $upload_data = $this->upload->data();

        echo "<h3>Your file was successfully uploaded!</h3>" ;

        echo "<ul>" ;
        foreach($upload_data as $item => $value) {
            echo '<li>',$item,' : ',$value,'</li>';
        }
        echo "</ul>" ;

        echo "<p>== Data read file excel ==================================== //</p>" ;

        // Load the spreadsheet reader library
        $this->load->library('excel_reader');

        // Set output Encoding.
        $this->excel_reader->setOutputEncoding('CP1251');

        $file =$config['upload_path'].'/'.$upload_data['file_name'];
        
        $this->excel_reader->read($file);

        // Sheet 1
        $data = $this->excel_reader->sheets[0];

        for ($i = 1; $i <= $data['numRows']; $i++) {
            for ($j = 1; $j <= $data['numCols']; $j++) {
                if(empty($data['cells'][$i][$j])){

                }else{
                    echo "\"".$data['cells'][$i][$j]."\",";
                }
            }
            echo "<br />";
        }

    }
}

}