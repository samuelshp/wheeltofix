
<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Acceptance extends JW_Controller {

private $_CI;

public function __construct() {
	parent::__construct();
    $this->_CI =& get_instance();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Macceptance');
    $this->load->model('Mkontrak');
    $this->load->model('Macceptanceitem');
    $this->load->model('Mpurchaseorder');
    $this->load->model('Mpurchaseinvoice');
    $this->load->model('Mpurchaseitem');
    $this->load->model('Msubkontrakmaterial');
    $this->load->model('Msubkontrakteam');
    $this->load->model('Mnotification');

    $this->_getMenuHelpContent(23, true, 'adminpage');
}

public function index() {
    if($this->input->post('smtMakeAcceptance') != '') { // Make Acceptance
        $this->load->model('Munit');

        $this->_CI->db->trans_begin();
        $intAcceptanceID = 0;
        $days=$this->input->post('txtJatuhTempo');
        $date=$this->input->post('txtDate');
        $date = strtotime("+".$days." days", strtotime($date));
        $jatuhtempo = date("Y-m-d", $date);
        $jatuhtempo = str_replace("-","/",$jatuhtempo);
        $int_purchase2=0;
        $totalItem = $this->input->post('totalItem');
        $intTotalPrice = 0;
        for($i = 0; $i < $totalItem; $i++){
            //$intPurchaseID = $this->input->post('PurchaseID');

            if ($this->input->post('idsubkon'.$i) != ''){            
                $intSuppID = $this->input->post('supplierID');
                $intWarehouse = $this->input->post('WarehouseID');
                $strEkspedition = $this->input->post('txtEkspedition');
                $strDescription = $this->input->post('txaDescription');
                $intTax= $this->input->post('txtTax');
                $intDiscount= $this->input->post('dsc4');
                $strDate= $this->input->post('txtDate');
                $intTipeBayar = $this->input->post('intTipeBayar');
                $strJatuhTempo = $jatuhtempo;
                $id_subkon = $this->input->post('idsubkon'.$i);
                $strCustomCode = $this->input->post('txtCustomCode');
                $int_purchase = $this->input->post('purchase_id'.$i);
                $int_supplier = $this->input->post('supplier_item'.$i);
                $intTotalPrice += $this->input->post('qty1ItemBayarX'.$i)*$this->input->post('price'.$i);
                // $c_date = $this->input->post('tanggal_hari_ini');
                $c_date = date("Y-m-d H:i:s");
                if($int_supplier == NULL){
                    $int_supplier = 0;
                }              
                $this->load->model('Macceptance');
                if($int_purchase != $int_purchase2){
                    $intAcceptanceID = $this->Macceptance->add($int_purchase,$int_supplier,$intWarehouse,$strDescription,$intTax=0,$intDiscount=0,2,$this->input->post('txtDateBuat'),$intTipeBayar="Tunai",$strJatuhTempo,$int_acc_code, $_SESSION['strAdminID'], $id_subkon);
                    $kode_acceptance = generateTransactionCode($this->input->post('txtDateBuat'),'','acceptance',TRUE);
                    $this->Macceptance->updateKodeAcceptance($kode_acceptance, $intAcceptanceID);
                    $int_purchase2 = $int_purchase;                    
                    // Tampung data untuk notif
                    $sm2[] = $this->Mkontrak->getKontrakByPurchaseId($int_purchase);                    
                }
                continue;
            }
        }

        $this->load->model('Mjournal');
        // $this->Mjournal->postTransaction('acceptance',$this->input->post('txtDateBuat'));
        // $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),168,'BPB','Acceptance',$intAcceptanceID,$intTotalPrice,'');
        // $this->Mjournal->insertJournal($this->input->post('txtDateBuat'),256,'BPB','Acceptance',$intAcceptanceID,-$intTotalPrice,'');

 
        $arrAcceptance = array(
            'intPurchaseID' => $this->input->post('PurchaseID'),
            'intSuppID'=> $this->input->post('supplierID'),
            'intWarehouse' => $this->input->post('WarehouseID'),
            'strEkspedition' => $this->input->post('txtEkspedition'),
            'strDescription' => $this->input->post('txaDescription'),
            'intTax'=> $this->input->post('txtTax'),
            'intDiscount'=> $this->input->post('dsc4'),
            'strDate'=> $this->input->post('txtDate'),
            'intTipeBayar' => $this->input->post('intTipeBayar'),
            'strJatuhTempo' => $jatuhtempo,
            'strCustomCode' => $this->input->post('txtCustomCode'),
        );
        
        //$this->load->model('Mpurchaseitem');
        for($i = 0; $i < $totalItem; $i++) {
            if ($this->input->post('idsubkon'.$i) != '') {                
                $nomor_po = $this->input->post('nomor_po'.$i);
                $name_bahan = $this->input->post('nameItemX'.$i);
                $terterima = $this->input->post('jumlah_terterima'.$i);
                $satuan = $this->input->post('satuan_po'.$i);
                $jumlah = $this->input->post('jumlah_po_fill'.$i);
                $jumlah_tetap = $this->input->post('jumlah_awal'.$i);
                $jumlah_penerimaan = $this->input->post('qty1ItemX'.$i);
                $jumlah_pb = $this->input->post('qty1ItemTerimaX'.$i);
                $jumlah_bayar = $this->input->post('qty1ItemBayarX'.$i);
                $satuan_penerimaan = $this->input->post('inputan_satuan'.$i);
                $keterangan_penerimaan = $this->input->post('inputan_keterangan'.$i);
                $purchase_id_lol = $this->input->post('purchase_id_input'.$i);
                $purchase_item_id = $this->input->post('purchase_item_id'.$i);
                $id_subkon = $this->input->post('idsubkon'.$i);
                $exist = $this->input->post('existItem'.$i);
                $prodId = $this->input->post('product_id'.$i);
                $strProductDescription = 'Something';
                $idItem = $this->input->post('idItem'.$i);
                if($prodId == NULL){
                    $prodId = 0;
                }
                //update jumlah barang pada PO
               //Bawah untuk nambah ke acceptance item
                $this->load->model('Macceptanceitem');
                // echo $jumlah_penerimaan."ha";
                if($jumlah_bayar != ''){
                    $this->Macceptanceitem->add($intAcceptanceID,$prodId,$keterangan_penerimaan,$jumlah_penerimaan,$purchase_id_lol, $lokasi_penerimaan="Surabaya", $jumlah_pb, $purchase_item_id, $jumlah_bayar);
                    $new_terterima = $this->Macceptanceitem->recalculateTerterima($purchase_id_lol, $prodId, $purchase_item_id); 
                    // echo var_dump($new_terterima);
                    // exit();                   
                    if ($new_terterima !== FALSE) {                       
                        $this->load->model('Macceptanceitem');
                        $this->Msubkontrakmaterial->updateTerBpb($id_subkon,$prodId,$new_terterima['jumlah2']);
                        $this->Mpurchaseitem->UpdateTerterima($purchase_item_id, $new_terterima['jumlah'], $new_terterima['jumlah2']);
                    }else $return = false;                    
                }

            continue;
            }
        }
        if($arrAcceptance['intPurchaseID']>0){
            $different=0;
            $arrCompare=$this->Mpurchaseitem->getItemsByPurchaseID($arrAcceptance['intPurchaseID']);
            if(!empty($arrCompare)) {
                for($i = 0; $i < count($arrCompare); $i++) {
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit1']);
                    $intConv1 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit2']);
                    $intConv2 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit3']);
                    $intConv3 = $convTemp[0]['unit_conversion'];
                    $quantity=(int)$arrCompare[$i]['prci_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['prci_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['prci_quantity3']*(int)$intConv3;
                    $purchased=(int)$arrCompare[$i]['prci_arrived1']*(int)$intConv1+(int)$arrCompare[$i]['prci_arrived2']*(int)$intConv2+(int)$arrCompare[$i]['prci_arrived3']*(int)$intConv3;
                    $different+=(int)$quantity-(int)$purchased;
                }
            }
            $arrCompare=$this->Mpurchaseitem->getBonusItemsByPurchaseID($arrAcceptance['intPurchaseID']);
            if(!empty($arrCompare)) {
                for($i = 0; $i < count($arrCompare); $i++) {
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit1']);
                    $intConv1 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit2']);
                    $intConv2 = $convTemp[0]['unit_conversion'];
                    $convTemp = $this->Munit->getConversion($arrCompare[$i]['prci_unit3']);
                    $intConv3 = $convTemp[0]['unit_conversion'];
                    $quantity=(int)$arrCompare[$i]['prci_quantity1']*(int)$intConv1+(int)$arrCompare[$i]['prci_quantity2']*(int)$intConv2+(int)$arrCompare[$i]['prci_quantity3']*(int)$intConv3;
                    $purchased=(int)$arrCompare[$i]['prci_arrived1']*(int)$intConv1+(int)$arrCompare[$i]['prci_arrived2']*(int)$intConv2+(int)$arrCompare[$i]['prci_arrived3']*(int)$intConv3;
                    $different+=(int)$quantity-(int)$purchased;
                }
            }
            if($different==0){
                //sini ubah status order jadi 4
                $this->load->model('Mpurchase');
                $this->Mpurchase->editStatusByID($arrAcceptance['intPurchaseID'],4);
            }
        }        
      
        if ($this->db->trans_status() === FALSE || $return === FALSE){
            $this->db->trans_rollback();            
            $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptancefailed'));
            redirect('acceptance');
        } else {

            /* Send Notif pindah kesini */
            foreach ($sm2 as $key => $value) {
                $id_subkon =$sm2[$key][0]['id'];
                //Bawah untuk PM
                $pm = $this->Mpurchaseorder->getAllPMBySubKontrak($id_subkon);
                foreach($pm as $data_pm){
                    $data_notif_pm = array(
                        "noti_user_id" => $data_pm['name'],
                        "noti_title" => "$kode_acceptance telah dibuat oleh logistik untuk pekerjaan ".$sm2[$key][0]['job']."",
                        "noti_content" => "Supplier : ".$sm2[$key][0]['supp_name'].""
                    );  
                    $this->Mnotification->add($data_notif_pm);
                }
                //Bawah untuk SM
                $sm = $this->Mpurchaseorder->getAllSMBySubKontrak($id_subkon);
                foreach($sm as $data_sm){
                    $data_notif_sm = array(
                        "noti_user_id" => $data_sm['name'],
                        "noti_title" => "$kode_acceptance telah dibuat oleh logistik untuk pekerjaan ".$sm2[$key][0]['job']." supplier ".$sm2[$key][0]['supp_name']."",
                    );  
                    $this->Mnotification->add($data_notif_sm);
                }
                //Dibawah untuk kirim notif ke CC
                $cc = $this->Mpurchaseorder->getAllCCBySubKontrak($id_subkon);
                foreach($cc as $data_cc){
                    $data_notif_cc = array(
                        "noti_user_id" => $data_cc['name'],
                        "noti_title" => "$kode_acceptance telah dibuat oleh logistik untuk pekerjaan ".$sm2[$key][0]['job']." supplier ".$sm2[$key][0]['supp_name']."",
                    );  
                    $this->Mnotification->add($data_notif_cc);
                }
                //DIbawah untuk notifk ke Purchasing
                $purchasing = $this->Mpurchaseorder->getAllPurchasingBySubKontrak($id_subkon);
                foreach($purchasing as $data_purchasing){
                    $data_notif_purchasing = array(
                        "noti_user_id" => $data_purchasing['name'],
                        "noti_title" => "$kode_acceptance telah dibuat oleh logistik untuk pekerjaan ".$sm2[$key][0]['job']." supplier ".$sm2[$key][0]['supp_name']."",
                    );  
                    $this->Mnotification->add($data_notif_purchasing);
                }
                //Bawah untuk logistik
                $logistik = $this->Mpurchaseorder->getAllLogistikBySubKontrak($id_subkon);
                foreach($logistik as $data_logistik){
                    $data_notif_logistik = array(
                        "noti_user_id" => $data_logistik['name'],
                        "noti_title" => "$kode_acceptance telah dibuat oleh logistik untuk pekerjaan ".$sm2[$key][0]['job']." supplier ".$sm2[$key][0]['supp_name']."",
                    );  
                    $this->Mnotification->add($data_notif_logistik);
                }
            }
            /*End of Send Notif pindah kesini */

            $this->db->trans_commit();
            $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptancemade');
            redirect('acceptance/view/'.$intAcceptanceID);
        }        
    }
    if($this->input->post('subSearchSupply') != '') {
        //$arrAcceptance = $this->Macceptance->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));
        //Tambah pagination disini
        
        $this->load->model('Mpurchaseitem');
        $arrAcceptance = $this->Mpurchaseitem->getAllPurchaseItemBySupplier($this->input->post('txtSearchSupplier'));
        $strPage = '';
		$strBrowseMode = '<a href="'.site_url('acceptance', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrAcceptance) ? count($arrAcceptance) : '0')." records).";
        $this->load->model('Mtinydbvo');
        $this->Mtinydbvo->initialize('invoice_status');
        $this->load->view('sia',array(
            'strViewFile' => 'acceptance/add',
            'arrDataPO' => $arrAcceptance,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
        ));

    }
    
    //Untuk bikin pagination
    // $this->load->model('Mpurchaseitem');
    // $arrPagination['base_url'] = site_url("acceptance/add?pagination=true", NULL, FALSE);
    // $arrPagination['total_rows'] = $this->Mpurchaseitem->getCount();
    // echo "a";
    // $arrPagination['per_page'] = $this->config->item('jw_item_count');
    // $arrPagination['page_query_string'] = true;
    // $this->pagination->initialize($arrPagination);
    // $strPage = $this->pagination->create_links();

    // $strBrowseMode = '';
    // if($this->input->get('page') != '' ) $intPage = $this->input->get('page');
    // else{
    //     $intPage=0;
    // }

    if($_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 2 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 1){
        $this->load->model('Mpurchaseitem');
        $arrDataPO = $this->Mpurchaseitem->getAllPurchaseItem();
    }
    else{
        $this->load->model('Mpurchaseitem');
        $arrDataPO = $this->Mpurchaseitem->getAllPurchaseItem(-1,-1, $_SESSION['strAdminID']);
    }
    $jumlahData = sizeof($arrDataPO);
    $cek = $arrDataPO[0]['id'];
    //echo "<script>alert($cek);</script>";
	// Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/add',
        'strPage' => $strPage,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addacceptance'),
        'arrDataPO' => $arrDataPO,
        'jumlahData' => $jumlahData
    ));
    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/add',
        'strPage' => $strPage,
        'arrDataPO' => $arrDataPO,
        'jumlahData' => $jumlahData
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
    // checkTeamPermission('Acceptance',$intID);
	# INIT
    $arrReturn = $this->session->userdata('arrReturn');

    if($this->input->post('unApproveSM') != ''){
        $accept_id = $this->input->post('acceptance_id');
        $this->_CI->db->trans_start();
        $date = date('Y/m/d H:i:s');
        $this->Macceptance->uneditSaveStatus($accept_id, $date, $_SESSION['strAdminID']);

        //Bawah untuk notifikasi

        $int_purchase = $this->input->post('PurchaseID');

        $sm = $this->Mkontrak->getKontrakByPurchaseId($int_purchase);
        $id_subkon =$sm[0]['id'];
        //Bawah untuk logistik
        $logistik = $this->Mpurchaseorder->getAllLogistikBySubKontrak($id_subkon);
        foreach($logistik as $data_logistik){
            $data_notif_logistik = array(
                "noti_user_id" => $data_logistik['name'],
                "noti_title" => "$kode_acceptance telah ditolak oleh SM untuk pekerjaan ".$sm[0]['job']." supplier ".$sm[0]['supp_name']."",                
            );	
            $this->Mnotification->add($data_notif_logistik);
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Updated status BPB');
        redirect('acceptance/browse');
    }

    if($this->input->post('approveSM') != ''){
        $accept_id = $this->input->post('acceptance_id');
        $this->_CI->db->trans_start();
        $this->Macceptance->editAccStatus($accept_id);

        $date = date('Y/m/d H:i:s');
        $this->load->model('Macceptance');
        $this->Macceptance->updateJamSave($accept_id, $date);
        $this->Macceptance->updateOrangSave($accept_id, $_SESSION['strAdminID']);

        //Bawah untuk notification

        $int_purchase = $this->input->post('PurchaseID');

        $sm = $this->Mkontrak->getKontrakByPurchaseId($int_purchase);
        $id_subkon =$sm[0]['id'];
        //Bawah untuk PM
        $pm = $this->Mpurchaseorder->getAllPMBySubKontrak($id_subkon);
        foreach($pm as $data_pm){
            $data_notif_pm = array(
                "noti_user_id" => $data_pm['name'],
                "noti_title" => "$kode_acceptance telah disetujui oleh SM untuk pekerjaan ".$sm[0]['job']."",                
            );	
            $this->Mnotification->add($data_notif_pm);
        }
        //DIbawah untuk notifk ke Purchasing
        $purchasing = $this->Mpurchaseorder->getAllPurchasingBySubKontrak($id_subkon);
        foreach($purchasing as $data_purchasing){
            $data_notif_purchasing = array(
                "noti_user_id" => $data_purchasing['name'],
                "noti_title" => "$kode_acceptance telah disetujui oleh SM untuk pekerjaan ".$sm[0]['job']." supplier ".$sm[0]['supp_name']."",                
            );	
            $this->Mnotification->add($data_notif_purchasing);
        }
        //Bawah untuk logistik
        $logistik = $this->Mpurchaseorder->getAllLogistikBySubKontrak($id_subkon);
        foreach($logistik as $data_logistik){
            $data_notif_logistik = array(
                "noti_user_id" => $data_logistik['name'],
                "noti_title" => "$kode_acceptance telah disetujui oleh SM untuk pekerjaan ".$sm[0]['job']." supplier ".$sm[0]['supp_name']."",                
            );	
            $this->Mnotification->add($data_notif_logistik);
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Updated status BPB');
        redirect('acceptance/browse');
    }
    
    if($this->input->post('saveEdit') != ''){

        $accept_id = $this->input->post('acceptance_id');
        $this->Macceptance->editPMStatus($accept_id);
        $this->Macceptance->editAccStatus($accept_id);

        $date = date('Y/m/d H:i:s');
        $this->load->model('Macceptance');
        $this->Macceptance->updateJamSave($accept_id, $date);
        $this->Macceptance->updateOrangSave($accept_id, $_SESSION['strAdminID']);
        $this->session->set_flashdata('strMessage','Updated status BPB');
        redirect('acceptance/browse');
    }

    if($this->input->post('unsaveEdit') != ''){
        $accept_id = $this->input->post('acceptance_id');
        $date = date('Y/m/d H:i:s');
        $this->Macceptance->uneditSaveStatus($accept_id, $date, $_SESSION['strAdminID']);

        $this->session->set_flashdata('strMessage','Updated status BPB');
        redirect('acceptance/browse');
    }

	if($this->input->post('subSave') != '' && $intID != '') { # SAVE
		// Acceptance
		$arrAcceptanceData = $this->Macceptance->getItemByID($intID);
        $intPurchaseID = $this->input->post('PurchaseID');
        $this->load->model('Macceptanceitem');
        $this->_CI->db->trans_start();
		if(compareData($arrAcceptanceData['acce_status'],array(0))) {
			    $this->Macceptance->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('txtProgress'),$this->input->post('selStatus'),$this->input->post('dsc4'),$this->input->post('txtTax'));
			// Load purchase item
			$arrAcceptanceItem = $this->Macceptanceitem->getItemsByAcceptanceID($intID);
            //Seharusnya dari purchaseitemubEdit
            $arrPostQty1 = $this->input->post('txtItem1Qty');
            $arrPostQty2 = $this->input->post('txtItem2Qty');
            $arrPostQty3 = $this->input->post('txtItem3Qty');
            $arrAwal1 = $this->input->post('awal1Item');
            $arrAwal2 = $this->input->post('awal2Item');
            $arrAwal3 = $this->input->post('awal3Item');
            $arrPostPrice = $this->input->post('txtItemPrice');
            $arrPostDisc1 = $this->input->post('txtItem1Disc');
            $arrPostDisc2 = $this->input->post('txtItem2Disc');
            $arrPostDisc3 = $this->input->post('txtItem3Disc');
            $arrIdItem = $this->input->post('idItem');
            $intTotalPrice = 0;
            $arrContainID = $this->input->post('idProiID');
			foreach($arrAcceptanceItem as $e) {
				// Search return quantity
				
                if(!empty($arrContainID)) {
                    if (in_array($e['id'], $arrContainID)) {
                        $this->Macceptanceitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],$arrPostPrice[$e['id']],$arrPostDisc1[$e['id']],$arrPostDisc2[$e['id']],$arrPostDisc3[$e['id']]);
                        if($intPurchaseID>0){
                            $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                            $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                            $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                            if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                $this->Macceptanceitem->UpdateArrivedItemView($intPurchaseID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,0);
                            }
                        }
                    } else $this->Macceptanceitem->deleteByID($e['id']);
					
                } else $this->Macceptanceitem->deleteByID($e['id']);
                
			}
            $arrAcceptanceItem = $this->Macceptanceitem->getBonusItemsByAcceptanceID($intID);
            $arrPostQty1 = $this->input->post('txtItemBonus1Qty');
            $arrPostQty2 = $this->input->post('txtItemBonus2Qty');
            $arrPostQty3 = $this->input->post('txtItemBonus3Qty');
            $arrAwal1 = $this->input->post('awal1ItemBonus');
            $arrAwal2 = $this->input->post('awal2ItemBonus');
            $arrAwal3 = $this->input->post('awal3ItemBonus');
            $arrIdItem = $this->input->post('idItemBonus');
            $arrContainID = $this->input->post('idProiIDBonus');
            if(!empty($arrAcceptanceItem)){
                foreach($arrAcceptanceItem as $e) {
                    // Search return quantity
                    
                    if(!empty($arrContainID)) {
                        if (in_array($e['id'], $arrContainID)) {
                            $this->Macceptanceitem->editByID($e['id'],$arrPostQty1[$e['id']],$arrPostQty2[$e['id']],$arrPostQty3[$e['id']],0,0,0,0);
                            if($intPurchaseID>0){
                                $temp1= (int)$arrPostQty1[$e['id']]-(int)$arrAwal1[$e['id']];
                                $temp2= (int)$arrPostQty2[$e['id']]-(int)$arrAwal2[$e['id']];
                                $temp3= (int)$arrPostQty3[$e['id']]-(int)$arrAwal3[$e['id']];
                                if($arrAwal1[$e['id']]!=$arrPostQty1[$e['id']] || $arrAwal2[$e['id']]!=$arrPostQty2[$e['id']] || $arrAwal3[$e['id']]!=$arrPostQty3[$e['id']] ){
                                    $this->Macceptanceitem->UpdateArrivedItemView($intPurchaseID,$arrIdItem[$e['id']],$temp1,$temp2,$temp3,1);
                                }
                            }
                        } else $this->Macceptanceitem->deleteByID($e['id']);
						
                    } else $this->Macceptanceitem->deleteByID($e['id']);
                    
                }
            }
            if($intPurchaseID==0){
                $totalItem = $this->input->post('totalItem');
                for($i = 0; $i < $totalItem; $i++) {
                    $qty1Item = $this->input->post('qty1PriceEffect'.$i);
                    $qty2Item = $this->input->post('qty2PriceEffect'.$i);
                    $qty3Item = $this->input->post('qty3PriceEffect'.$i);
                    $unit1Item = $this->input->post('sel1UnitID'.$i);
                    $unit2Item = $this->input->post('sel2UnitID'.$i);
                    $unit3Item = $this->input->post('sel3UnitID'.$i);
                    $prcItem = $this->input->post('prcPriceEffect'.$i);
                    $dsc1Item = $this->input->post('dsc1PriceEffect'.$i);
                    $dsc2Item = $this->input->post('dsc2PriceEffect'.$i);
                    $dsc3Item = $this->input->post('dsc3PriceEffect'.$i);
                    $prodItem = $this->input->post('prodItem'.$i);
                    $probItem = $this->input->post('probItem'.$i);
                    $strProductDescription = formatProductName('',$prodItem,$probItem);
                    $idItem = $this->input->post('idItem'.$i);
                    if($strProductDescription != '' || !empty($strProductDescription)) {
                        $this->load->model('Macceptanceitem');
                        $this->Macceptanceitem->add($intID,$idItem,$strProductDescription,$qty1Item,$qty2Item,$qty3Item,$unit1Item,$unit2Item,$unit3Item,$prcItem,$dsc1Item,$dsc2Item,$dsc3Item,0,1);
                    }
                }
                // $totalItemBonus = $this->input->post('totalItemBonus');
                // for($i = 0; $i < $totalItemBonus; $i++) {
                //     $qty1ItemBonus = $this->input->post('qty1PriceEffectBonus'.$i);
                //     $qty2ItemBonus = $this->input->post('qty2PriceEffectBonus'.$i);
                //     $qty3ItemBonus = $this->input->post('qty3PriceEffectBonus'.$i);
                //     $unit1ItemBonus = $this->input->post('sel1UnitBonusID'.$i);
                //     $unit2ItemBonus = $this->input->post('sel2UnitBonusID'.$i);
                //     $unit3ItemBonus = $this->input->post('sel3UnitBonusID'.$i);
                //     $prodItemBonus = $this->input->post('prodItemBonus'.$i);
                //     $probItemBonus = $this->input->post('probItemBonus'.$i);
                //     $strBonusProductDescription = formatProductName('',$prodItemBonus,$probItemBonus);
                //     $idItemBonus = $this->input->post('idItemBonus'.$i);
                //     if($strBonusProductDescription != '' || !empty($strBonusProductDescription)) {
                //         $this->Macceptanceitem->add($intID,$idItemBonus,$strBonusProductDescription,$qty1ItemBonus,$qty2ItemBonus,$qty3ItemBonus,$unit1ItemBonus,$unit2ItemBonus,$unit3ItemBonus,0,0,0,0,1,1);
                //     }
                // }
            }

			// Edit finance data
			//$this->load->model('Mfinance'); $this->Mfinance->editAutomatic(2,$intID,$intTotalPrice);
			
		} else $this->Macceptance->editByID($intID,$this->input->post('txtProgress'),$this->input->post('selStatus'));
		
		$this->_CI->db->trans_complete();
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-acceptanceupdated');
		
    } 
    
    if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
        $idAcceptance = $this->input->post('idAcceptance');
        $acce_code = $this->input->post('codeAcceptance');
        
        $this->load->model('Mpurchaseinvoice');
        $check = $this->Mpurchaseinvoice->checkForBPBInPI($idAcceptance);
        if($check[0]['ada']>0){
            $this->session->set_flashdata('strMessage','Penerimaan barang dengan kode '.$acce_code.' telah ada di Purchase Invoice');
            redirect('acceptance/browse');
        }
        else{
            $status = $this->Macceptance->checkStatusForBPB($idAcceptance);
            $this->load->model('Macceptanceitem');
            $this->_CI->db->trans_start();
            $arrAccItem = $this->Macceptanceitem->getItemsByAcceptanceID($idAcceptance);
            //echo "<script>alert(".count($arrAccItem).");</script>";
            $this->load->model('Macceptance');//Delete terlebih dahulu 
            $this->Macceptance->deleteByID($idAcceptance);
            //$this->Macceptance->deleteByID($idAcceptance);

            if (count($arrAccItem) > 0) {
                for($i=0;$i<count($arrAccItem);$i++){
                    $prod_id = $arrAccItem[$i]['product_id_baru'];//ambil product id yang mau di delete
                    $purchase_id = $arrAccItem[$i]['acit_purchase_id'];//amvil purchase id yang mau di delete
                    $this->load->model('Mpurchaseitem');
                    $id_subkon = $this->input->post('idsubkon['.$i.']');
                    $purchase_item_id_delete = $this->Mpurchaseitem->getIdOfPurchaseItem($prod_id, $purchase_id);//ambil purchase item id
                    $this->load->model('Macceptanceitem');
                    //$this->Macceptanceitem->deleteByAcitID($arrAccItem[$i]['id']);//delete acceptance item berdasarkan id
                    $sum_terterima = $this->Macceptanceitem->recalculateTerterima($purchase_id,$prod_id,$purchase_item_id_delete[0]['id']);//calculate jumlah item untuk di update ke terterima
                    if($sum_terterima['jumlah'] == null){
                        $sum_terterima['jumlah'] = 0;
                    }
                    if($sum_terterima['jumlah2'] == null){
                        $sum_terterima['jumlah2'] = 0;
                    }
                    // echo $purchase_item_id_delete[0]['id']." ".$sum_terterima['jumlah']." ".$sum_terterima['jumlah2'];
                    // die();
                    $this->Msubkontrakmaterial->updateTerBpb($id_subkon,$prod_id,$sum_terterima['jumlah2']);
                    $this->Mpurchaseitem->UpdateTerterima($purchase_item_id_delete[0]['id'], $sum_terterima['jumlah'], $sum_terterima['jumlah2']);//update jumlah terterima berdasarkan id purchasenya
                }    
            }

            $this->_CI->db->trans_complete();
            $this->session->set_flashdata('strMessage','Penerimaan barang dengan kode '.$acce_code.' telah berhasil dihapus');
            redirect('acceptance/browse');
        }

		$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderdeleted'));
		redirect('acceptance/browse');
        
        //Alurnya, ambil data dulu, dari setiap detail acceptance, kemudian data dikembalikan
        //Setelah itu, data di hapus
		
    }

    $arrAcceptanceData = $this->Macceptance->getItemByID($intID);
    $bolEditable = $this->Msubkontrakteam->checkEditableBPB($_SESSION['strAdminID'], $arrAcceptanceData['idSubKontrak']); 
    if($_SESSION['strAdminID'] == 2){
        $bolEditable = 1;
    }  
	if(!empty($arrAcceptanceData)) {        
		$arrAcceptanceData['acce_rawstatus'] = $arrAcceptanceData['acce_status'];
		$arrAcceptanceData['acce_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'invoice_status', 'allow_null' => 0, 'validation' => ''),
			$arrAcceptanceData['acce_status']);
		
        // Load the purchase item
        $this->load->model('Macceptanceitem');
        $arrTampilan = array();
        $arrAcceptanceItem = $this->Macceptanceitem->getItemsByAcceptanceID($intID);
        //get count
        $jumlah_row_baru = $this->Macceptanceitem->getSumRow($intID);
        for($j=0;$j<$jumlah_row_baru[0]['jml_row'];$j++){
            $arr_temp = $this->Macceptanceitem->getDataPurchase($intID, $arrAcceptanceItem[$j]['purchase_id_baru'], $arrAcceptanceItem[$j]['product_id_baru']);
            if($arr_temp[0]['jml_purch_now'] == null || $arr_temp[0]['jml_purch_now'] < 0){
                $arr_temp[0]['jml_purch_now'] = 0;
            }
            array_push($arrTampilan, $arr_temp[0]['jml_purch_now']);
        }
        $test = count($arrAcceptanceItem[0]);
        echo "<script>console.log($test);</script>";
		$intAcceptanceTotal = 0;
        $this->load->model('Munit');
        $this->load->model('Macceptanceitem');
		for($i = 0; $i < count($arrAcceptanceItem); $i++) {
            if($arrAcceptanceData['acce_purchase_id']>0){
                $arrAcceptanceItem[$i]['exist'] = $arrAcceptanceItem[$i]['acit_exist'];
                $arrAcceptanceItem[$i]['maxData'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit1'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit2'] = '';
                $arrAcceptanceItem[$i]['maxData']['unit3'] = '';
                if($arrAcceptanceItem[$i]['exist']=='1') {
                    $arrAcceptanceItem[$i]['maxData'] = $this->Macceptanceitem->getMaxItemsByAcceptedItemIDAndProdID($arrAcceptanceData['acce_purchase_id'],$arrAcceptanceItem[$i]['product_id']);
                    $arrAcceptanceItem[$i]['maxData']['unit1'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit1']);
                    $arrAcceptanceItem[$i]['maxData']['unit2'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit2']);
                    $arrAcceptanceItem[$i]['maxData']['unit3'] = $this->Munit->getName($arrAcceptanceItem[$i]['maxData']['prci_unit3']);
                }
            }
            $arrAcceptanceItem[$i]['strName'] = $arrAcceptanceItem[$i]['acit_description'].'|'.$arrAcceptanceItem[$i]['proc_title'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrAcceptanceItem[$i]['acit_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intAcceptanceTotal += $arrAcceptanceItem[$i]['acit_subtotal'];
            $arrAcceptanceItem[$i]['acit_conv1'] = $intConv1;
            $arrAcceptanceItem[$i]['acit_conv2'] = $intConv2;
            $arrAcceptanceItem[$i]['acit_conv3'] = $intConv3;
		}

        $arrAcceptanceData['subtotal_discounted'] = (float) $arrAcceptanceData['acce_grandtotal'];
        $intAcceptanceGrandTotal = (float) $arrAcceptanceData['subtotal_discounted'] + ($arrAcceptanceData['subtotal_discounted'] * (float) $arrAcceptanceData['acce_tax'] / 100);

        $arrAcceptanceItemBonus = $this->Macceptanceitem->getBonusItemsByAcceptanceID($intID);
        if(!empty($arrAcceptanceItemBonus)) {
            for($i = 0; $i < count($arrAcceptanceItemBonus); $i++) {
                if($arrAcceptanceData['acce_purchase_id']>0){
                    $arrAcceptanceItemBonus[$i]['exist'] = $arrAcceptanceItemBonus[$i]['acit_exist'];
                    $arrAcceptanceItemBonus[$i]['maxData'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit1'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit2'] = '';
                    $arrAcceptanceItemBonus[$i]['maxData']['unit3'] = '';
                    if($arrAcceptanceItemBonus[$i]['exist'] == '1') {
                        $arrAcceptanceItemBonus[$i]['maxData'] = $this->Macceptanceitem->getMaxBonusItemsByAcceptedItemIDAndProdID($arrAcceptanceData['acce_purchase_id'],$arrAcceptanceItemBonus[$i]['product_id']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit1'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit1']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit2'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit2']);
                        $arrAcceptanceItemBonus[$i]['maxData']['unit3'] = $this->Munit->getName($arrAcceptanceItemBonus[$i]['maxData']['prci_unit3']);
                    }
                }
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit1']);
                $intConv1 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit2']);
                $intConv2 = $convTemp[0]['unit_conversion'];
                $convTemp = $this->Munit->getConversion($arrAcceptanceItemBonus[$i]['acit_unit3']);
                $intConv3 = $convTemp[0]['unit_conversion'];
                $arrAcceptanceItemBonus[$i]['strName'] = $arrAcceptanceItemBonus[$i]['acit_description'].'|'.$arrAcceptanceItemBonus[$i]['proc_title'];
                $arrAcceptanceItemBonus[$i]['acit_conv1'] = $intConv1;
                $arrAcceptanceItemBonus[$i]['acit_conv2'] = $intConv2;
                $arrAcceptanceItemBonus[$i]['acit_conv3'] = $intConv3;
            }
        }
        $arrAcceptanceList = $this->Macceptance->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));
	} else {
		$arrAcceptanceItem = array(); $arrAcceptanceList = array();
    }

    if($this->input->post('editAcceptanceItem') != ''){
        $pengecekan_lama;
            // $jumlah_delete = $this->input->post('jumlah_delete');
            // $jumlah_item = $this->input->post('total_edit_item');
            // $accept_id = $this->input->post('acceptance_id');
            // echo "<script>console.log('$jumlah_item hahhaha');</script>";
            // //echo "<script>console.log('$kumpulansId[0] hahaha');</script>";
            // $kumpulanId = $this->input->post('kumpulan_id');
            // if($jumlah_delete == 0){
            //     if($jumlah_item > 1){
            //         //$kumpulansId = explode(",", $kumpulanId);
            //         for($i=0; $i<$jumlah_item; $i++){
            //             $jumlah_terterima = $this->input->post('jumlah_terterima['.$i.']');
            //             $keterangan_terima_baru = $this->input->post('keterangan_terima['.$i.']');
            //             $qty_inputan = $this->input->post('jumlah_diterima['.$i.']');
            //             $prod_id = $this->input->post('purchase_id_edit['.$i.']');
            //             $purch_id = $this->input->post('product_id_edit['.$i.']');
            //             $purchi_id = $this->input->post('purchase_item_id_edit['.$i.']');
            //             $ai_id = $this->input->post('ai_id['.$i.']');
            //             $this->load->model('Mpurchaseitem');
            //             $this->Macceptanceitem->editItemsById($qty_inputan, $keterangan_terima_baru, $ai_id);
            //             //$this->Macceptanceitem->editItem($purchi_id, $qty_inputan);
            //             // $this->load->model('Mpurchaseitem');
            //             // $qty_baru = $qty_inputan-$jumlah_terima_baru;
            //             // $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_inputan);
            //             //echo "<script>alert('$keterangan_terima_baru');</script>";
            //         }
            //         redirect('acceptance/view/'.$accept_id);
            //     }
            //     else if($jumlah_item == 1){
            //         //echo "<script>console.log('Masuk yang satu');</script>";
            //         $jumlah_terterima = $this->input->post('jumlah_terterima[0]');
            //         //echo "<script>alert('a');</script>";
            //         $keterangan_terima_baru = $this->input->post('keterangan_terima[0]');
            //         $qty_inputan = $this->input->post('jumlah_diterima[0]');
            //         $prod_id = $this->input->post('purchase_id_edit[0]');
            //         $purch_id = $this->input->post('product_id_edit[0]');
            //         $purchi_id = $this->input->post('purchase_item_id_edit[0]');
            //         $ai_id = $this->input->post('ai_id[0]');
            //         // echo "Saya masuk sama dengan 0 ".$jumlah_terima_baru." ".$keterangan_terima_baru." ".$qty_inputan." ".$prod_id." ".$purch_id." ".$purchi_id;
            //         // die();
            //         $this->Macceptanceitem->editItemsById($jumlah_terima_baru, $keterangan_terima_baru, $qty_inputan, $ai_id);
            //         //$this->Macceptanceitem->editItem($purchi_id, $qty_inputan);
            //         //$this->load->model('Mpurchaseitem');
            //         // $qty_baru = $qty_inputan-$jumlah_terima_baru;
            //         // $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_inputan);
            //         redirect('acceptance/view/'.$accept_id);
            //     }
            //     else{}
            // }
            // else{
            //     $jumlah_awal = $this->input->post('total_edit_item');
            //     if($jumlah_delete == $jumlah_awal){
            //         $this->session->set_flashdata('strMessage','Data detail tidak boleh kosong');
            //         redirect('acceptance/view/'.$accept_id);
            //     }
            //     else{
            //         if($jumlah_delete > 1){//jika jumlah lebih dari satu
            //             for($i = 0; $i<$jumlah_delete; $i++){
            //                 $id_acce_item = $this->input->post('ai_id['.$i.']');
            //                 $jumlah_terima = $this->input->post('jumlah_terterima['.$i.']');
            //                 $jumlah_dari_purchase = $this->input->post('jumlah_diterima['.$i.']');
            //                 $acc_id = $this->input->post('acceptance_id['.$i.']');

            //                 $purchase_item_id_delete = $this->input->post('purchase_item_id_edit['.$i.']');
            //                 $product_id_delete = $this->input->post('product_id_edit['.$i.']');

            //                 $jumlah_baru_delete = $jumlah_terima + $jumlah_dari_purchase;

            //                 $this->Macceptanceitem->deleteByID($id_acce_item);
            //                 //$this->Macceptanceitem->updateDataPO($jumlah_dari_purchase, $purchase_item_id_delete, $product_id_delete);
            //             }
            //         }
            //         else{
            //             $id_acce_item = $this->input->post('ai_id[0]');
            //             $jumlah_terterima = $this->input->post('jumlah_terterima[0]');
            //             $jumlah_terima = $this->input->post('jumlah_terima[0]');
            //             $jumlah_dari_purchase = $this->input->post('jumlah_diterima[0]');
            //             $acc_id = $this->input->post('acceptance_id[0]');

            //             $purchase_item_id_delete = $this->input->post('purchase_item_id_edit[0]');
            //             $product_id_delete = $this->input->post('product_id_edit[0]');

            //             $jumlah_baru_delete = $jumlah_terima + $jumlah_dari_purchase;
            //             //echo "<script>alert(".$id_acc.");</script>";
            //             $this->Macceptanceitem->deleteByID($id_acce_item);
            //             //$this->Macceptanceitem->updateDataPO($jumlah_baru_delete, $purchase_item_id_delete, $product_id_delete);
            //         }
            //         redirect('acceptance/view/'.$accept_id);
            //     }
            // }

        if($this->input->post('raw_status') != 4){
            $jumlah_item = $this->input->post('total_edit_item');
            $accept_id = $this->input->post('acceptance_id');
            if($jumlah_item == 1){
                $jumlah_terterima = $this->input->post('jumlah_terterima[0]');
                $keterangan_terima_baru = $this->input->post('keterangan_terima[0]');
                $qty_pb = $this->input->post('jumlah_pb[0]');
                $qty_inputan = $this->input->post('jumlah_diterima[0]');
                $qty_bayar = $this->input->post('jumlah_bayar[0]');
                $qty_awal_input = $this->input->post('qty_inputan[0]');
                //cadangan -> $qty_inputan = $this->input->post('jumlah_diterima_view[0]');
                $purch_id = $this->input->post('purchase_id_edit[0]');
                $prod_id = $this->input->post('product_id_edit[0]');
                $id_subkon = $this->input->post('idsubkon[0]');
                $purchi_id = $this->input->post('purchase_item_id_edit[0]');
                $ai_id = $this->input->post('ai_id[0]');

                if(strpos($qty_inputan,',') !== false){
                    $qty_inputan_baru = str_replace(['.',','],['','.'],$qty_inputan);
                }
                else{
                    $qty_inputan_baru = $qty_inputan;
                }
                if(strpos($qty_bayar,',') !== false){
                    $qty_bayar_baru = str_replace(['.',','],['','.'],$qty_bayar);
                }
                else{
                    $qty_bayar_baru = $qty_bayar;
                }
                if(strpos($qty_pb,',') !== false){
                    $qty_pb_baru = str_replace(['.',','],['','.'],$qty_pb);
                }
                else{
                    $qty_pb_baru = $qty_pb;
                }
                $this->_CI->db->trans_start();
                if ($this->input->post('raw_status') == 1) $this->Macceptance->updateStatus($accept_id, STATUS_APPROVED);
                $qty_baru = $qty_inputan+$jumlah_terterima;
                //if($this->input->post('awal1Item[0]') != $qty_inputan){
                    if($qty_inputan > $qty_awal_input){
                        $qty_baru = ($qty_inputan-$qty_awal_input)+$jumlah_terterima;
                        $this->load->model('Mpurchaseitem');
                        // $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_baru); tidak perlu karena re calculate untuk update terterimanya
                        // $qty_inputan_baru = str_replace(['.',','],['','.'],$qty_inputan);
                        $this->Macceptanceitem->updateJumlahBarang($ai_id, $qty_inputan_baru, $qty_bayar_baru, $qty_pb_baru);
                    }
                    else{
                        $qty_baru = $jumlah_terterima-($qty_awal_input-$qty_inputan);
                        $this->load->model('Mpurchaseitem');
                        // $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_baru); tidak perlu karena re calculate untuk update terterimanya
                        // $qty_inputan_baru = str_replace(['.',','],['','.'],$qty_inputan);
                        // echo "Masuk 2"." ".$qty_inputan_baru." ".$qty_bayar;
                        // die();
                        $this->Macceptanceitem->updateJumlahBarang($ai_id, $qty_inputan_baru, $qty_bayar_baru, $qty_pb_baru);
                    }
                $this->_CI->db->trans_complete();
                // }
                $qty_calculate = $this->Macceptanceitem->recalculateTerterima($purch_id,$prod_id, $purchi_id);//calculate jumlah item untuk di update ke terterima
                if($qty_calculate['jumlah'] == null){
                    $qty_calculate['jumlah'] = 0;
                }
                if($qty_calculate['jumlah2'] == null){
                    $qty_calculate['jumlah2'] = 0;
                }
                $this->_CI->db->trans_start();
                $this->Msubkontrakmaterial->updateTerBpb($id_subkon,$prod_id,$qty_calculate['jumlah2']);
                $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_calculate['jumlah'],$qty_calculate['jumlah2']);
                $this->_CI->db->trans_complete();
            }
            else{
                for($i = 0; $i<$jumlah_item; $i++){
                    $jumlah_terterima = $this->input->post('jumlah_terterima['.$i.']');
                    $keterangan_terima_baru = $this->input->post('keterangan_terima['.$i.']');
                    $qty_pb = $this->input->post('jumlah_pb['.$i.']');
                    $qty_inputan = $this->input->post('jumlah_diterima['.$i.']');
                    $qty_bayar = $this->input->post('jumlah_bayar['.$i.']');
                    $qty_awal_input = $this->input->post('qty_inputan['.$i.']');
                    $purch_id = $this->input->post('purchase_id_edit['.$i.']');
                    $id_subkon = $this->input->post('idsubkon['.$i.']');
                    $prod_id = $this->input->post('product_id_edit['.$i.']');
                    $purchi_id = $this->input->post('purchase_item_id_edit['.$i.']');
                    $ai_id = $this->input->post('ai_id['.$i.']');

                    if(strpos($qty_inputan,',') !== false){
                        $qty_inputan_baru = str_replace(['.',','],['','.'],$qty_inputan);
                    }
                    else{
                        $qty_inputan_baru = $qty_inputan;
                    }
                    if(strpos($qty_bayar,',') !== false){
                        $qty_bayar_baru = str_replace(['.',','],['','.'],$qty_bayar);
                    }
                    else{
                        $qty_bayar_baru = $qty_bayar;
                    }
                    if(strpos($qty_pb,',') !== false){
                        $qty_pb_baru = str_replace(['.',','],['','.'],$qty_pb);
                    }
                    else{
                        $qty_pb_baru = $qty_pb;
                    }

                    $this->_CI->db->trans_start();
                    if ($this->input->post('raw_status') == 1) $this->Macceptance->updateStatus($accept_id, STATUS_APPROVED);
                    if($qty_inputan > $qty_awal_input){
                        $this->Macceptanceitem->updateJumlahBarang($ai_id, $qty_inputan_baru, $qty_bayar_baru, $qty_pb_baru);
                    }
                    else{
                        $this->Macceptanceitem->updateJumlahBarang($ai_id, $qty_inputan_baru, $qty_bayar_baru, $qty_pb_baru);
                    }                    
                // }
                    $qty_calculate = $this->Macceptanceitem->recalculateTerterima($purch_id,$prod_id, $purchi_id);//calculate jumlah item untuk di update ke terterima
                    if($qty_calculate['jumlah'] == null){
                        $qty_calculate['jumlah'] = 0;
                    }
                    if($qty_calculate['jumlah2'] == null){
                        $qty_calculate['jumlah2'] = 0;
                    }
                    $this->Msubkontrakmaterial->updateTerBpb($id_subkon,$prod_id,$qty_calculate['jumlah2']);
                    $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_calculate['jumlah'],$qty_calculate['jumlah2']);
                    $this->_CI->db->trans_complete();
                }
            }
            $this->session->set_flashdata('strMessage','BPB telah berhasil di edit');
            redirect('acceptance/view/'.$accept_id);
        }

    } 

    // Untuk PM
    if($this->input->post('pmEdit') != ''){
        $jumlah_item = $this->input->post('total_edit_item');
        $accept_id = $this->input->post('acceptance_id');
        $this->_CI->db->trans_start();
        $this->Macceptance->editPMStatus($accept_id);
        $this->Macceptance->editAccStatus($accept_id);

        // if($jumlah_item == 1){
        //     $jumlah_terterima = $this->input->post('jumlah_terterima[0]');
        //     $keterangan_terima_baru = $this->input->post('keterangan_terima[0]');
        //     $qty_inputan = $this->input->post('jumlah_diterima[0]');
        //     //cadangan -> $qty_inputan = $this->input->post('jumlah_diterima_view[0]');
        //     $prod_id = $this->input->post('purchase_id_edit[0]');
        //     $purch_id = $this->input->post('product_id_edit[0]');
        //     $purchi_id = $this->input->post('purchase_item_id_edit[0]');
        //     $ai_id = $this->input->post('ai_id[0]');

        //     $qty_baru = $qty_inputan+$jumlah_terterima;
        //     $this->load->model('Mpurchaseitem');
        //     $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_baru);
        // }
        // else{
        //     for($i = 0; $i<$jumlah_item; $i++){
        //         $jumlah_terterima = $this->input->post('jumlah_terterima['.$i.']');
        //         $keterangan_terima_baru = $this->input->post('keterangan_terima['.$i.']');
        //         $qty_inputan = $this->input->post('jumlah_diterima['.$i.']');
        //         $prod_id = $this->input->post('purchase_id_edit['.$i.']');
        //         $purch_id = $this->input->post('product_id_edit['.$i.']');
        //         $purchi_id = $this->input->post('purchase_item_id_edit['.$i.']');
        //         $ai_id = $this->input->post('ai_id['.$i.']');

        //         $qty_baru = $qty_inputan+$jumlah_terterima;
        //         $this->load->model('Mpurchaseitem');
        //         $this->Mpurchaseitem->UpdateTerterima($purchi_id, $qty_baru);
        //     }
        // }

        $date = date('Y/m/d H:i:s');
        $this->load->model('Macceptance');
        $this->Macceptance->updateJamApprovalPM($accept_id, $date);
        $this->Macceptance->updateOrangApprovalPM($accept_id, $_SESSION['strAdminID']);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','Updated status PM');
        redirect('acceptance/browse');
    }

    if($this->input->post('unpmEdit') != ''){
        $accept_id = $this->input->post('acceptance_id');
        $this->Macceptance->uneditPMStatus($accept_id);

        $this->session->set_flashdata('strMessage','Updated status PM');
        redirect('purchase_order/browse');
    }

    $this->load->model('Mdbvo','MAdminLogin');
    $this->MAdminLogin->initialize('adm_login');

    $inPi = 0;

    $check = $this->Mpurchaseinvoice->checkForBPBInPI($intID);
    if($check[0]['ada']>0){
        $inPi = 1;
    }

    $this->MAdminLogin->dbSelect('','id =' . $arrAcceptanceData['cby_bpb']);
    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
	// Load all other purchase data the user has ever made

    $arrData = array(
        'intAcceptanceID' => $intID,
        'intAcceptanceTotal' => $intAcceptanceTotal,        
        'intAcceptanceGrandTotal' => $intAcceptanceGrandTotal,
        'arrAcceptanceItem' => $arrAcceptanceItem,
        'raw_status' => $arrAcceptanceData['acce_rawstatus'],
        'bolEditable' => $bolEditable,
        'inPi' => $inPi,
        'arrAcceptanceData' => $arrAcceptanceData,
        'arrAcceptanceBonusItem' => $arrAcceptanceItemBonus,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
        'arrLogin' => $arrLogin,        
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        //$dataPrint=$this->Macceptance->getPrintDataByID($intID);
        // print_r($arrData);
        // exit();
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'acceptance',
        ), $arrData));        
    }elseif($this->input->get('print2') != '') {//print e pajak
        //$dataPrint=$this->Macceptance->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'acceptancetax',
        ),$arrData));
    }else{                
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'acceptance/view',
            'arrTampilan' => $arrTampilan,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancedata'),
        ), $arrData,$this->admlinklist->getMenuPermission(23,$arrAcceptanceData['acce_rawstatus'])));
    }
}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {
        if(!empty($this->input->post('txtSearchNamaProyek'))){
            $arrAcceptance = $this->Macceptance->searchByNamaProyek($this->input->post('txtSearchNamaProyek'));
        }
        else if(!empty($this->input->post('txtSearchKodeAcc'))){
            
            $arrAcceptance = $this->Macceptance->searchByIdAcc($this->input->post('txtSearchKodeAcc'));
        }
        else{
            $arrAcceptance = $this->Macceptance->searchBySupplier($this->input->post('txtSearchValue'), array($this->input->post('txtSearchDate'), $this->input->post('txtSearchDateTo')), $this->input->post('txtSearchStatus'));
            $strPage = '';
        }
		$strBrowseMode = '<a href="'.site_url('acceptance/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrAcceptance) ? count($arrAcceptance) : '0')." records).";
        
    } else {
        $arrPagination['base_url'] = site_url("acceptance/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Macceptance->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 10;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();
		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrAcceptance = $this->Macceptance->getItemsBaru($intPage,$arrPagination['per_page']);
        //Harus ambil purchase invoice
    }

    if($this->input->post('delAcceptance') != ''){

        // $this->Macceptance->deleteByID($intID);

		// $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseorderdeleted'));
		// redirect('purchase_order/browse');        
        $idAcceptance = $this->input->post('delAcceptance');
        $acce_code = $this->input->post('acceptanceCode');
        //Alurnya, ambil data dulu, dari setiap detail acceptance, kemudian data dikembalikan
        //Setelah itu, data di hapus
        $this->load->model('Mpurchaseinvoice');
        $check = $this->Mpurchaseinvoice->checkForBPBInPI($idAcceptance);
        if($check[0]['ada']>0){
            $this->session->set_flashdata('strMessage','Penerimaan barang dengan kode '.$acce_code.' telah ada di Purchase Invoice');
            redirect('acceptance/browse');
        }
        else{
            $this->_CI->db->trans_start();
            $status = $this->Macceptance->checkStatusForBPB($idAcceptance);
            $this->load->model('Macceptanceitem');
            $arrAccItem = $this->Macceptanceitem->getItemsByAcceptanceID($idAcceptance);
            //echo "<script>alert(".count($arrAccItem).");</script>";
            $this->load->model('Macceptance');//Delete terlebih dahulu 
            $this->Macceptance->deleteByID($idAcceptance);
            for($i=0;$i<count($arrAccItem);$i++){
                $prod_id = $arrAccItem[$i]['product_id_baru'];//ambil product id yang mau di delete
                $purchase_id = $arrAccItem[$i]['acit_purchase_id'];//amvil purchase id yang mau di delete
                $this->load->model('Mpurchaseitem');
                $purchase_item_id_delete = $this->Mpurchaseitem->getIdOfPurchaseItem($prod_id, $purchase_id);//ambil purchase item id
                $this->load->model('Macceptanceitem');
                $this->Macceptanceitem->deleteByAcitID($arrAccItem[$i]['id']);//delete acceptance item berdasarkan id
                $sum_terterima = $this->Macceptance->countAllBPBItemByItemAndOPId($prod_id, $purchase_id);//calculate jumlah item untuk di update ke terterima
                if($sum_terterima[0]['jumlah'] == null){
                    $sum_terterima[0]['jumlah'] = 0;
                }
                $this->Mpurchaseitem->updatePurchaseItemAfterDelAcc($sum_terterima[0]['jumlah'], $purchase_item_id_delete[0]['id']);//update jumlah terterima berdasarkan id purchasenya
            }
            $this->_CI->db->trans_complete();
            $this->session->set_flashdata('strMessage','Penerimaan barang dengan kode '.$acce_code.' telah berhasil dihapus');
            redirect('acceptance/browse');
        }
        //Check apakah BPB sudah dibuat PI, jika belum, maka bisa dihapus, jika tidak
        //Tidak bisa dihapus
        
    }

	if(!empty($arrAcceptance)) for($i = 0; $i < count($arrAcceptance); $i++) {

		$arrAcceptance[$i] = array_merge($arrAcceptance[$i],$this->admlinklist->getMenuPermission(23,$arrAcceptance[$i]['acce_status']));
		// $arrAcceptance[$i]['acce_code'] = $arrAcceptance[$i]['acce_status'];
		// $arrAcceptance[$i]['cdate'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrAcceptance[$i]['acce_status']);
        // $arrAcceptance[$i]['idKontrak'] =(float)$arrAcceptance[$i]['acce_grandtotal']+((float)$arrAcceptance[$i]['acce_grandtotal']*(float)$arrAcceptance[$i]['acce_tax']/100);
    }
    
    // if($this->input->post('delete_from_pb') != ''){
    //     echo "<script>console.log('Sesuatu');</script>";
    // }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'acceptance/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrAcceptance' => $arrAcceptance,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-acceptancebrowse')
    ));
}

}

/* End of File */