<?php
/*
PUBLIC FUNCTION:
- index(strMode)
- browse(intPage)
- view(intInvoiceID)

PRIVATE FUNCTION:
- __construct()
*/

class Invoice extends JW_Controller {

private $_CI;

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();
    $this->_CI =& get_instance();
    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Minvoice');
    $this->load->model('Minvoiceitem');

    $this->_getMenuHelpContent(43,true,'adminpage');
}

public function index($strMode = 'normal') {
    // WARNING! Don't change the following steps
    $bolKanvasAvlb = $this->admlinklist->getAvailability(54,$this->session->userdata('strAdminPriviledge'));

    if($this->input->post('smtUpdateInvoice') != '' || $this->input->post('smtMakeInvoice') != '') { 
    }

    if($this->input->post('smtMakeInvoice') != '') { // Make Invoice

        $arrInvoice = array(
            'strCode' => generateTransactionCode($this->input->post('txtDate'),'','invoice',TRUE),
            'txtDate' => $this->input->post('txtDate'),
            'intKontrakID' => $this->input->post('intKontrakID'),
            'intSubkontrakID' => $this->input->post('intSubkontrakID'),
            'intSubkontrakTerminID' => $this->input->post('intSubkontrakTerminID'),
            'txtPrice' => $this->input->post('txtPrice')
        );
        $this->_CI->db->trans_start();
        $intInvoiceID = $this->Minvoice->add2($arrInvoice);
        $this->load->model('Msubkontraktermin');
        $this->Msubkontraktermin->updateDuedate($arrInvoice['intSubkontrakTerminID'],$arrInvoice['txtDate']);

        $this->load->model('Mkontrak');
        $kontrak = $this->Mkontrak->getItemByID($arrInvoice['intKontrakID']);

        $taxAmount = $arrInvoice['txtPrice']/10;
        $dp = $arrInvoice['txtPrice']-$taxAmount;

        $this->load->model('Mjournal');
        $this->Mjournal->insertPiutangUMC($intInvoiceID);
        // $this->Mjournal->postTransaction('invoice',$this->input->post('txtDate'));
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage',loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicemade'));
        redirect('invoice/view/'.$intInvoiceID);
    
    }
    $this->load->model('Mkontrak');
    $arrKontrak = $this->Mkontrak->getActiveItems(0,1000);

    $this->load->view('sia',array(
        'strViewFile' => 'invoice/add',
        'arrKontrak' => $arrKontrak,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-addkuitansi')
    ));
}

// To display, edit and delete invoice
public function view($intID = '') {
    # INIT

    if($this->input->post('smtUpdateInvoice') != '' && $intID != '') {
        // Invoice        
        $arrPostData = array(
            'txtDate' => $this->input->post('txtDate')            
        );
        $this->load->model('Mjournal');
        $this->Minvoice->update($arrPostData, $intID);        
        $this->Mjournal->resetRHLaporan();
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoiceupdated');

    } else if($this->input->post('subDelete') != '' && $intID != '') {
        /* must use this, because item's trigger can't be activated in header trigger */
        $this->_CI->db->trans_start();
        $this->Minvoiceitem->deleteByInvoiceID($intID);
        $this->Minvoice->deleteByID($intID);
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'invoice-invoicedeleted'));
        redirect('invoice/browse');
    }

    $arrInvoiceData = $this->Minvoice->getItemByID($intID);
    
    $arrData = array(
        'arrInvoiceData' => $arrInvoiceData,
        'intInvoiceID' => $intID,
    );

    $this->load->model('Mprintlog');

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->get('process') == 'cashier') && $intID != '') {
        if($this->input->get('process') == 'cashier') {
            if($this->session->flashdata('txtPayment') != '') $txtPayment = $this->session->flashdata('txtPayment');
            if($this->session->flashdata('txtPaymentChange') != '') $txtPaymentChange = $this->session->flashdata('txtPaymentChange');
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'),
                'dataPrint' => $this->Minvoice->getPrintDataByID($intID),
                'txtPayment' => !empty($txtPayment) ? $txtPayment : '',
                'txtPaymentChange' => !empty($txtPaymentChange) ? $txtPaymentChange : '',
            ), $arrData));
            $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $this->defconf->getSingleData('JW_PRINT_CASHIER_TYPE'));

        } else {                    
            //$this->Minvoice->editAfterPrint($intID);
            // $strLastTemplate = $this->Mprintlog->getLastTemplate('invoice', $intID);
            // if(empty($strLastTemplate)) {
            //     if($this->input->post('subPrint') == 'Print + Surat Jalan') {
            //         $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE').'_suratjalan';
            //     } else {
            //         $strLastTemplate = $this->defconf->getSingleData('JW_PRINT_INVOICE_TYPE');
            //     }
            // }
            $this->load->view('sia_print',array_merge(array(
                'strPrintFile' => 'invoice',
            ), $arrData));
            // $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, $strLastTemplate);
        }

    }else if(($this->input->post('subPrintTax') != '') || ($this->input->get('print2') != '')) {//print e pajak
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'invoicetax',
            'dataPrint' => $this->Minvoice->getPrintDataByID($intID),
            'dateTax' => $date,
            'kodeTax' => $kode,
        ), $arrData));
        $idPrintLog = $this->Mprintlog->add($arrInvoiceData['invo_code'], 'invoice', $intID, 'invoicetax');

    } else {        
        # Load all other invoice data the user has ever made
        $arrInvoiceList = $this->Minvoice->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));        
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'invoice/view',
            'arrInvoiceList' => $arrInvoiceList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-invoicestatus')
        ), $arrData, $this->admlinklist->getMenuPermission(43,$arrInvoiceData['invo_status'])));
    }
}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {
        $arrInvoice = $this->Minvoice->searchFilter($this->input->post('txtSearchNoInvoice'),$this->input->post('txtSearchProject'));

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('invoice/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrInvoice) ? count($arrInvoice) : '0')." records).";
    } else {
        $arrPagination['base_url'] = site_url("invoice/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Minvoice->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrInvoice = $this->Minvoice->getItems($intPage,$arrPagination['per_page']);
    }

    if(!empty($arrInvoice)) for($i = 0; $i < count($arrInvoice); $i++) {
        $arrInvoice[$i] = array_merge($arrInvoice[$i],$this->admlinklist->getMenuPermission(43,$arrInvoice[$i]['invo_status']));
        $arrInvoice[$i]['invo_rawstatus'] = $arrInvoice[$i]['invo_status'];
        $arrInvoice[$i]['invo_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrInvoice[$i]['invo_status']);

    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'invoice/browse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrInvoice' => $arrInvoice,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'invoice-kuitansi')
    ));
}

}

/* End of File */