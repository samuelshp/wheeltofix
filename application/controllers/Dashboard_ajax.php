<?php

class Dashboard_ajax extends JW_Controller {
    public function checkAvailable($intLapangan=0,$strDateFrom = '',$strDateUntil = '') {
    	$strDateFrom = str_replace('_',' ',$strDateFrom);
        $strDateUntil = str_replace('_',' ',$strDateUntil);
        $this->load->model('Mbooking');
        $arrItem = $this->Mbooking->checkAvailable($intLapangan,$strDateFrom,$strDateUntil);
        ArrayToXml(array('available' => $arrItem));
    }
}