<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)


PRIVATE FUNCTION:
- __construct()
*/

class Pemakaian extends JW_Controller {

	private $_CI;

	public function __construct() {
		parent::__construct();
		$this->_CI =& get_instance();
		if($this->session->userdata('strAdminUserName') == '') redirect();
		
		// Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		
		// Get Model
		$this->load->model('Mkontrak');
		$this->load->model('Mpemakaian');
		$this->load->model('Mpemakaianitem');

		// ID adm_link_list, true, 'adminpage'
		$this->_getMenuHelpContent(34,true,'adminpage');
	}
	
	public function index() {
		// WARNING! Don't change the following steps
		if($this->input->post('smtMakeUsage') != '') { // Make Usage
			//echo "<pre>";
			//echo var_dump($this->input->post());
			//echo "</pre>";
    		//exit();

			$this->_CI->db->trans_start();
			
			$arrUsage = array(
				'kontrak_id' => $this->input->post('idkontrak'),
				'subkontrak_id' => $this->input->post('idsubkon'),
				'warehouse_id' => $this->input->post('idwarehouse')
			);

			$intUsageID = $this->Mpemakaian->add($arrUsage);
			
			$intAddItem = $this->Mpemakaianitem->addItem($intUsageID,$this->input->post());

			$this->_CI->db->trans_complete();
			$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'pemakaian-pemakaianmade'));
			
			redirect('pemakaian/view/'.$intUsageID);
		}

		if($this->input->post('smtEditUsage') != '') { // Update Usage
			//echo "<pre>";
			//echo var_dump($this->input->post());
			//echo "</pre>";
    		//exit();

			$intUsageID = $this->input->post('id_pemakaian');	
			
			$intUpdate = $this->Mpemakaianitem->updateItem($intUsageID,$this->input->post());		

			redirect('pemakaian/view/'.$intUsageID);
		}

		if($this->input->post('smtDeleteUsage') != ''){
			$intUsageID = $this->input->post('id_pemakaian');		
			$intDelete = $this->Mpemakaian->delete($intUsageID);

			if ($intDelete) {
				redirect('pemakaian/browse/');
			}else{
				redirect('pemakaian/view/'.$intUsageID);	
			}
		}


		if($_SESSION['strAdminPriviledge'] == 5 || $_SESSION['strAdminPriviledge'] == 2 || $_SESSION['strAdminPriviledge'] == 6 || $_SESSION['strAdminPriviledge'] == 1){
			$arrKontrak = $this->Mpemakaian->getAllKontrak();
		}
		else{
			$arrKontrak = $this->Mpemakaian->getAllKontrak($_SESSION['strAdminID']);
		}

		// Load the views
		$this->load->view('sia',array(
			'strViewFile' => 'pemakaian/add',
			'arrKontrak' => $arrKontrak,
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-addpemakaian')
		));
	}

	public function browse($intPage = 0) {
		if($this->input->post('subSearch') != '') {
			$proyek = $this->input->post('txtSearchProyek');
			$tglProyek = $this->input->post('txtSearchTglProyek');
			$arrUsage = $this->Mpemakaian->searchBox($proyek, $tglProyek);
			$strPage = '';
			$strBrowseMode = '<a href="'.site_url('pemakaian/browse', NULL, FALSE).'">[Back]</a>';
			$this->_arrData['strMessage'] = "Search result (".(!empty($arrUsage) ? count($arrUsage) : '0')." records).";
		} else {
			// Basic Data URL, Total, Page
			$arrPagination['base_url'] = site_url("pemakaian/browse?pagination=true", NULL, FALSE);
			$arrPagination['total_rows'] = $this->Mpemakaian->getCount();
			$arrPagination['per_page'] = $this->config->item('jw_item_count');;
			$arrPagination['uri_segment'] = 3;
			$this->pagination->initialize($arrPagination);
			$strPage = $this->pagination->create_links();
	
			$strBrowseMode = '';
			if($this->input->get('page') != '') $intPage = $this->input->get('page');

			// Get Main Data
			$arrUsage = $this->Mpemakaian->getItems($intPage,$arrPagination['per_page']);
		}

		$this->load->view('sia',array(
			'strViewFile' => 'pemakaian/browse',
			'strPage' => $strPage,
			'strBrowseMode' => $strBrowseMode,
			'strSearchKey' => $this->input->post('txtSearchProyek'),
			'strSearchDate' => $this->input->post('txtSearchTglProyek'),
			'arrUsage' => $arrUsage,
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaian')
		));
	}

	public function view($intPage)
	{
		$arrParent = $this->Mpemakaian->getItem($intPage);
		$arrItem = $this->Mpemakaianitem->getItem($intPage);
				
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'pemakaian/view',		
			'arrParent' => $arrParent,	
			'arrItem' => $arrItem,
			'strPageTitle' => loadlanguage('admindisplay',$this->session->userdata('jw_language'),'pemakaian-pemakaian'),
		),$this->admlinklist->getMenuPermission(34,$arrUsage['status'])));
	}
}
/* End of File */