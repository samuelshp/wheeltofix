<?php

class Acceptance_ajax extends JW_Controller {

public function getAllPOData() {
	$this->load->model('Macceptance');
	ArrayToXml(array('PurchaseOrder' => $this->Macceptance->getAllPOData()));
}

public function searchBySupplier(){
	$this->load->model('Macceptance');
	ArrayToXml(array('PurchaseOrder' => $this->Macceptance->searchBySupplier()));
}

public function getHeaderBPB($bpb){
	$this->load->model('Macceptance');
	return $this->output->set_content_type('application/json')->set_output(json_encode($this->Macceptance->getHeaderBPB($bpb)));
}

public function getDetailBPB($bpb){
	$this->load->model('Macceptance');
	return $this->output->set_content_type('application/json')->set_output(json_encode($this->Macceptance->getDetailBPB($txtData)));
}

public function getDetailBPBByID($id){
	$this->load->model('Macceptance');
	ArrayToXml(array('DetailTerima' => $this->Macceptance->getDetailBPBByID($id)));
}

}