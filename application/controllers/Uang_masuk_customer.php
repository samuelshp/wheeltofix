<?php 

class Uang_masuk_customer extends JW_Controller
{

    private $_CI;
    public function __construct()
    {
        parent::__construct();
        $this->_CI =& get_instance();
        if($this->session->userdata('strAdminUserName') == '') redirect();
    
        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

        $this->load->model('Mtransin');
        $this->load->model('Mtransindetail');

    }

    public function index($intID = 0)
    {      
        if ($this->input->post('smtMakeUangMasukCustomer') != '') {
            $this->_CI->db->trans_start();
            $arrData = array(
                'txin_code' => generateTransactionCode($this->input->post('txtDate'),'','uang_masuk_customer'),
                'txin_date' => $this->input->post('txtDate'),
                'txin_cust_id' => $this->input->post('intCustomer'),
                'txin_acco_id' => $this->input->post('intCOA'),
                'txin_metode' => $this->input->post('intMethod'),
                'txin_jatuhtempo' => $this->input->post('txtDueDate'),                            
            );

            $intAddTrans = $this->Mtransin->addTransIn($arrData);

            $arrDataDetail = array(
                'txid_txin_id' => $intAddTrans,           
                'txid_amount' => str_replace(",", ".", str_replace(".", "", $this->input->post('txtAmount'))),
                'txid_type' =>"UMC",
                'txid_ref_id' => $intAddTrans,
                'txid_ref_giro' => $this->input->post('txtRefGiro'),
                'txid_jatuhtempo' => $this->input->post('txtDueDate'),

            );

            $this->Mtransindetail->add($arrDataDetail);
            $this->_CI->db->trans_complete();
            if ($intAddTrans > 0) {
                redirect('uang_masuk_customer/browse', 'refresh');
            }else{
                redirect('uang_masuk_customer/add','refresh');
            }
        }

        if ($this->input->post('smtUpdateUangMasukCustomer') != '') {
            $this->_CI->db->trans_start();
            $arrData = array(               
                'txin_date' => $this->input->post('txtDate'),
                'txin_cust_id' => $this->input->post('intCustomer'),
                'txin_acco_id' => $this->input->post('intCOA'),
                'txin_metode' => $this->input->post('intMethod'),                
                'txin_jatuhtempo' => $this->input->post('txtDueDate'),                            
            );

            $intUpdateTrans = $this->Mtransin->updateTransIn($arrData, $intID);

            $arrDataDetail = array(                
                'txid_amount' => str_replace(",", ".", str_replace(".", "", $this->input->post('txtAmount'))),
                'txid_ref_giro' => ($arrData['txin_metode'] == 3) ? $this->input->post('txtRefGiro') : null,
                'txid_jatuhtempo' => ($arrData['txin_metode'] == 3) ? $this->input->post('txtDueDate') : null,

            );
            
            $this->Mtransindetail->update($arrDataDetail, $intID);
            $this->_CI->db->trans_complete();
            if ($intUpdateTrans > 0) {
                redirect('uang_masuk_customer/view/'.$intID, 'refresh');
            }else{
                redirect('uang_masuk_customer/view/'.$intID,'refresh');
            }
        }

        if ($this->input->post('smtDeleteUangMasukCustomer') != '') {
            $this->_CI->db->trans_start();
            $this->Mtransin->deleteTransIn($intID);             
            $intDetail = $this->Mtransindetail->delete($intID);
            $this->_CI->db->trans_complete();
           if($intDetail > 0){
                redirect('uang_masuk_customer/browse', 'refresh');
           }else{
                redirect('uang_masuk_customer/view/'.$intID,'refresh');
           }

        }
    }

    public function add()
    {        
        $this->_getMenuHelpContent(235,true,'adminpage');       
        
        $this->load->model('Maccount');
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'uang_masuk_customer/add',
            'strPageTitle' => 'Uang Masuk Customer',
            'arrCOA' => $this->Maccount->getAllAccount(),
            'arrCustomer' => $this->Mcustomer->getAllCustomer()
        ),$this->admlinklist->getMenuPermission(235,1)));
    }

    public function view($intID)
    {       
         $this->_getMenuHelpContent(235,true,'adminpage');       
        
        $this->load->model('Maccount');
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'uang_masuk_customer/view',
            'strPageTitle' => 'Uang Masuk Customer',
            'intID' => $intID,
            'arrCOA' => $this->Maccount->getAllAccount(),
            'arrCustomer' => $this->Mcustomer->getAllCustomer(),
            'arrUangMasukDetail' => $this->Mtransin->getDetail($intID)
        ),$this->admlinklist->getMenuPermission(235,1)));
    }

    public function browse($intPage = '0')
    {
        $this->_getMenuHelpContent(235,true,'adminpage');

        $arrPagination['base_url'] = site_url("uang_masuk_customer/?pagination=true", NULL, FALSE);
        // $arrPagination['total_rows'] = count(getListOfData("UMC"));
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $arrUangMasuk = $this->Mtransin->getListOfData("UMC");

        $strBrowseMode = '';
        $this->load->view('sia',array(
            'strViewFile' => 'uang_masuk_customer/browse',
            'strPage' => $strPage,
            'strPageTitle' => 'Uang Masuk Customer',
            'strBrowseMode' => $strBrowseMode,
            'arrUangMasuk' => $arrUangMasuk,
        ));
    }

 
}