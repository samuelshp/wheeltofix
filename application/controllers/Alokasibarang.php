<?php

class Alokasibarang extends JW_Controller {

public function __construct() {
	parent::__construct();
	
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	//$this->load->model('Mdashboard');

	$this->load->model('Malokasibarang');

    $this->_getMenuHelpContent(223,true,'adminpage');
}

public function index(){
	if ($this->input->post('smtMakeAlokasi') != '') {
		
		$intInsert = $this->Malokasibarang->add($this->input->post());		

		if ($intInsert) {
			$this->session->set_flashdata('strMessage','Alokasi Barang berhasil disimpan');
        	redirect('alokasibarang/browse');
		}else{
			$this->session->set_flashdata('strMessage','Alokasi Barang gagal disimpan');
        	redirect('alokasibarang/browse');
		}
		
	}

	if ($this->input->post('smtUpdateAlokasi') != '') {

		$intSubkontrakID = $this->input->post('intSubkontrakID');
		$intUpdate = $this->Malokasibarang->update($this->input->post(), $intSubkontrakID);

		if ($intUpdate) $this->session->set_flashdata('strMessage','Alokasi Barang berhasil diupdate');
		else $this->session->set_flashdata('strMessage','Alokasi Barang gagal diupdate');

		redirect('alokasibarang/view/'.$intSubkontrakID);
	}
}

public function add()
{
	$this->load->model('Mkontrak');

	$arrDataProyek = $this->Mkontrak->getKontrakByCC($this->session->userdata('strAdminID'));

	$this->load->view('sia',array(
		'strViewFile' => 'alokasibarang/add',
		'arrDataProyek' => $arrDataProyek,
		'strPageTitle' => "Tambah Alokasi Barang Pembelian"
	));
}

public function view($intSubkontrakID = 0)
{	
	$arrDataAlokasi = $this->Malokasibarang->getDetailBySubkontrak($intSubkontrakID);
	$arrPurchase = $this->Malokasibarang->getAlokasiDataPurchased($intSubkontrakID);
	$arrPengeluaran = $this->Malokasibarang->getAlokasiDataPengeluaran($intSubkontrakID);

	$arrSubkontrakNonMaterial = $this->Malokasibarang->getSubkontrakNonMaterial($intSubkontrakID);
	
	$this->load->view('sia',array(
		'strViewFile' => 'alokasibarang/view',
		'arrDataAlokasi' => $arrDataAlokasi,
		'arrPurchase' => $arrPurchase,
		'arrPengeluaran' => $arrPengeluaran,
		'arrSubkontrakNonMaterial' => $arrSubkontrakNonMaterial,
		'strPageTitle' => "Detail Alokasi Barang Pembelian"
	));
}

public function browse($intPage = 0){

	$dataAlokasi = $this->Malokasibarang->getAllData($this->session->userdata('strAdminID'));

	$this->load->view('sia',array(
		'strViewFile' => 'alokasibarang/browse',
		'intPage' => $intPage,
		'dataAlokasi' => $dataAlokasi,
        'strPageTitle' => 'Alokasi barang'
	));
}

}

?>