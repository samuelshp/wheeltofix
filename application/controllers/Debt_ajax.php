<?php
/**
 * 
 */
class Debt_ajax extends JW_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mnotadebet");
	}
	public function getDPH($intSuppID)
	{
		$this->load->model('Mdebt');        
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mdebt->getDetailDPH($intSuppID)));
	}
	
	//Nota Debet
	public function getPurchaseInvoiceBySupplier($intSuppID)
	{
		return $this->output->set_content_type("application/json")
		->set_output(json_encode($this->Mnotadebet->getPurchaseInvoiceBySupplier($intSuppID)));
	}

	public function getPurchaseInvoiceDetail($intPIID)
	{
		return $this->output->set_content_type("application/json")
		->set_output(json_encode($this->Mnotadebet->getPurchaseInvoiceDetail($intPIID)));
	}

	public function getProjectBySupplier($intSuppID)
	{
		$this->Mnotadebet->getProjectBySupplier($intSuppID);
	}

	public function getAccount($intMethod)
	{		
		$this->load->model('Maccount');
		if ($intMethod == 1) $strWhere = " AND acco_kas = 2";
		elseif($intMethod == 2) $strWhere = " AND acco_bank = 2";
		else $strWhere = '';		
		return $this->output->set_content_type("application/json")
		->set_output(json_encode($this->Maccount->getAllAccount('',$strWhere)));		
	}
}