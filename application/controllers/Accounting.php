<?php
/*
PUBLIC FUNCTION:
- adjustmentbrowse()
- adjustment()
- posting()
- akuntansigeneralledger($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Accounting extends JW_Controller {

public function __construct() {
    parent::__construct();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->_getMenuHelpContent(74,true,'adminpage');
}

public function adjustmentbrowse($intID = 0) {
    $this->_getMenuHelpContent(72,true,'adminpage');
    $this->load->model('Maccountadjustment');
    $this->load->model('Maccountadjustmentitem');
    if($this->input->post('subSave') != '' && $intID != '') {
        $status=$this->Maccountadjustment->getStatusAdjustmentByID($intID);
        if(compareData($status['paym_status'],array(0))) {
            $description=$this->input->post('txtDescription'.$intID);
            $this->Maccountadjustment->editByID2($intID,$description,$this->input->post('selStatus'));
        }else{
            $this->Maccountadjustment->editByID($intID,$this->input->post('selStatus'));
        }
        redirect('accounting/adjustmentbrowse');
    } else if($this->input->post('subDelete') != '' && $intID != '') {
        $this->Maccountadjustmentitem->deleteByHeaderID($intID);
        $this->Maccountadjustment->deleteByID($intID);

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'adjustment-adjustmentdeleted'));
        redirect('accounting/adjustmentbrowse');
    }

    if($this->input->post('subSearch') != '') {
        $arrBilling = $this->Maccountadjustment->getAllAdjustment($this->input->post('txtSearchValue'));
        $i=0;
        if(!empty($arrBilling)){
            foreach($arrBilling as $e){
                $arrBilling[$i]['debit']=$this->Maccountadjustmentitem->getAllAdjustmentItemDebit($e['id']);
                $arrBilling[$i]['kredit']=$this->Maccountadjustmentitem->getAllAdjustmentItemKredit($e['id']);
                $i++;
             }
        }
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('accounting/adjustmentbrowse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search by Code with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";

    } else {
        $arrPagination['base_url'] = site_url("accounting/adjustmentbrowse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Maccountadjustment->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');
        $arrBilling =  $this->Maccountadjustment->getAllAdjustment();
        $i=0;
        if(!empty($arrBilling)){
            foreach($arrBilling as $e){
                $arrBilling[$i]['debit']=$this->Maccountadjustmentitem->getAllAdjustmentItemDebit($e['id']);
                $arrBilling[$i]['kredit']=$this->Maccountadjustmentitem->getAllAdjustmentItemKredit($e['id']);
                $i++;
            }
        }
    }
    $i=0;
    if(!empty($arrBilling)){
        foreach($arrBilling as $e){
            $arrBilling[$i]=array_merge($arrBilling[$i],$this->admlinklist->getMenuPermission(72,$arrBilling[$i]['acad_status']));
            $i++;
        }
    }

    $this->load->view('sia',array(
        'strViewFile' => 'accounting/adjustmentbrowse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrBilling' => $arrBilling
    ));
}

public function adjustment($intPage = 0) {
    $this->_getMenuHelpContent(72,true,'adminpage');
    $this->load->model('Maccountadjustment');
    $this->load->model('Maccountadjustmenttemplate');
    $arrTemplateSetting = false;
    if($this->input->post('smtMakeAdjustment') != '') { // Make Purchase
        $totalDebit=$this->input->post('totalDebit');
        $totalKredit=$this->input->post('totalKredit');
        $this->load->model('Maccountadjustmentitem');
        $txtDescription = $this->input->post('txaDescription');
        $txtDate = $this->input->post('txtDate');
        $intAdjustmentID=$this->Maccountadjustment->add($txtDescription,$txtDate,2);
        if($this->input->post('cbSaveTemplate') == 1) $arrTemplate = array('debit' => '', 'kredit' => '');
        else $arrTemplate = false;

        for($i=0;$i<$totalDebit;$i++){
            $name=$this->input->post('isDebitX'.$i);

            if($name!='0'){
                $subtotal=$this->input->post('totalDebitX'.$i);
                $id=$this->input->post('idDebitX'.$i);
                if(!empty($id)) { # ada kemungkinan id kosong karena dihapus
                    $intAdjustmentItemID=$this->Maccountadjustmentitem->add($intAdjustmentID,$id,1,$subtotal);
                    if(!empty($arrTemplate)) $arrTemplate['debit'][] = $id;
                }
            }
        }

        for($i=0;$i<$totalKredit;$i++){
            $name=$this->input->post('isKreditX'.$i);

            if($name!='0'){
                $subtotal=$this->input->post('totalKreditX'.$i);
                $id=$this->input->post('idKreditX'.$i);
                if(!empty($id)) { # ada kemungkinan id kosong karena dihapus
                    $intAdjustmentItemID=$this->Maccountadjustmentitem->add($intAdjustmentID,$id,-1,$subtotal);
                    if(!empty($arrTemplate)) $arrTemplate['kredit'][] = $id;
                }
            }
        }

        if(!empty($arrTemplate)) {
            $this->Maccountadjustmenttemplate->add($this->input->post('txtTemplateName'), $arrTemplate);
        }

        $this->session->set_flashdata('strMessage',loadLanguage('adminmessage',$this->session->userdata('jw_language'),'finance-financemade'));
        redirect('accounting/adjustmentbrowse');
    } else if($this->input->post('smtUseTemplate') != '') {
        $arrTemplateSetting = $this->Maccountadjustmenttemplate->getItemByID($this->input->post('selTemplate'));
        $arrTemplateSetting = $arrTemplateSetting['acte_setting'];
    } else if($this->input->post('smtDeleteTemplate') != '') {
        $this->Maccountadjustmenttemplate->delete($this->input->post('selTemplate'));
        $this->session->set_flashdata('strMessage', 'Template sudah dihapus');
        redirect('accounting/adjustment');
    }

    $arrAkun=$this->Maccountadjustment->getAllPerkiraan();
    $arrTemplate = $this->Maccountadjustmenttemplate->getAllItems();

    $this->load->view('sia',array(
        'strViewFile' => 'accounting/adjustmentadd',
        'arrAkun' => $arrAkun,
        'arrTemplate' => $arrTemplate,
        'arrTemplateSetting' => $arrTemplateSetting,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'finance-adjustmentadd')
    ));
}

public function posting() {
    $strMessage = '';
    $strPeriod = $this->input->post('period');
    if(isset($strPeriod) && !empty($strPeriod)) {
        $this->load->model('Mposting');
        $result = $this->Mposting->callFunctionPost($strPeriod);
        
        if($result['status'] === FALSE) $this->session->set_flashdata('strMessage', $result['error_message']);
        else $this->session->set_flashdata('strMessage', $result['success_message']);
    
    } else $this->session->set_flashdata('strMessage', 'Wrong input, please try again');

    redirect('accounting/generalledger');
}

public function generalledger($intPage = 0) {
    $this->load->model('Mreportaccounting', 'MReport');
    
    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strAccountID = $this->input->post('accountID');
    $strAccountCode = $this->input->post('accountCode');
    $strAccountName = $this->input->post('accountName');
    
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $this->load->model('Maccount');
        $account = $this->Maccount->getItemByID($this->input->get('accountid'));
        if($this->input->get('accountid') != '')
            $arrItems = $this->MReport->searchAkuntansiGeneralLedger($account['acco_code'],$this->input->get('start'),$this->input->get('end'),0);
        else
            $arrItems = '';
        
        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strAccountID = $account['id'];
        $strAccountCode = $account['acco_code'];
        $strAccountName = $account['acco_name'];
        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('accountID') != '')
            $arrItems = $this->MReport->searchAkuntansiGeneralLedger($this->input->post('accountCode'),$this->input->post('txtStart'),$this->input->post('txtEnd'),0);
        else
            $arrItems = '';
        
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('accounting/generalledger', NULL, FALSE).'">[Back]</a>';
    } else {
        $arrPagination['base_url'] = site_url('accounting/generalledger?pagination=true', NULL, FALSE);
        $arrPagination['total_rows'] = 0;
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        
        $strBrowseMode = '';
        if($this->input->get('page') != '')
            $intPage = $this->input->get('page');
        $arrItems = array();
    }
    
    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strAccountID' => $strAccountID,
        'strAccountCode' => $strAccountCode,
        'strAccountName' => $strAccountName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    if($this->input->get('excel') != '') {
        HTMLToExcel($this->load->view($this->config->item('jw_style').'_print/akuntansigeneralledger_export',$arrData,true),'akuntasi_general_ledger_by_account.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'akuntansigeneralledger',
        ),$arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'accounting/generalledger',
        ), $arrData));
    }
}

}

/* End of File */