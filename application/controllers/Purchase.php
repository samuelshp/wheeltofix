<?php
/*
PUBLIC FUNCTION:
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Purchase extends JW_Controller {

private $_CI;

public function __construct() {
    parent::__construct();
    $this->_CI =& get_instance();
    if($this->session->userdata('strAdminUserName') == '') redirect();

    // Generate the menu list
    $this->load->model('Madmlinklist','admlinklist');
    $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mpurchase');
    $this->load->model('Mpurchaseitem');
    $this->load->model('Mnotification');

    $this->_getMenuHelpContent(22,true,'adminpage');
}

public function index() {
    // WARNING! Don't change the following steps
    if($this->input->post('smtUpdatePurchase') != '') {
        $this->load->model('Mpurchaseorderitem');
        $this->_CI->db->trans_start();
        $arrPurchase = array(
            'intPurchaseID' => $this->input->post('intPurchaseID'),
            'strDate'=> $this->input->post('txtDate'),
            'strDescription'=> $this->input->post('txtDescription'),
            'intSubTotal'=> $this->input->post('subTotalNoTax'),
            'intDiscount'=> $this->input->post('dsc4'),
            'intTax'=> $this->input->post('txtTax'),
            'intCostAdd'=> $this->input->post('subTotalCost'),
            'intGrandTotal'=> $this->input->post('grandTotal'),
            'intPPH'=> $this->input->post('amountPPH'),
        );
        $this->Mpurchase->update($arrPurchase['intPurchaseID'],$arrPurchase['strDate'],$arrPurchase['strDescription'],$arrPurchase['intSubTotal'],$arrPurchase['intDiscount'],$arrPurchase['intTax'],$arrPurchase['intCostAdd'],$arrPurchase['intGrandTotal'],$arrPurchase['intPPH']);

        $totalItem = $this->input->post('totalItem');
        $po_id = $this->input->post('po_id');
        $i = 0; $j = 0;
        while($i < $totalItem) {
            if($this->input->post('productID'.$j)!='') {
                $id = $this->input->post('productID'.$j);
                $poiID = $this->input->post('poiID'.$j);
                $piID = $this->input->post('piID'.$j);
                $qtyItem = $this->input->post('qtyItem'.$j);
                $qtyBayar = $this->input->post('qtyBayar'.$j);
                $hargaBayar = $this->input->post('hargaBayar'.$j);
                $po_id = $this->input->post('po_id');
                $this->Mpurchaseitem->update($piID,$qtyItem,$qtyBayar,$hargaBayar);
                $result = $this->Mpurchaseitem->recalculateTotalByPOIID($poiID, $id, $po_id);
                if($result[0]['total'] == null || $result[0]['total'] == '' ){
                    $result[0]['total'] = 0;
                }
                if($result[0]['total2'] == null || $result[0]['total2'] == ''){
                    $result[0]['total2'] = 0;
                }
                $this->Mpurchaseorderitem->updateTerpurchase($poiID, $result[0]['total'],$result[0]['total2'], $po_id); // update terpurchase PB
                // if($kp != '0') {
                //     $this->Mkontrakpembelian->updatePO($kp,$qtyBayar);
                // }
                $i++;
            }
            $j++;
        }
        $this->_CI->db->trans_complete();
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchaseupdated');
        redirect('purchase/view/'.$arrPurchase['intPurchaseID']);
    }
    else if($this->input->post('smtMakePurchase') != '') { // Make Purchase
        $kp = $this->input->post('kpID');
        if($kp == '') $kp = '0';
        $this->_CI->db->trans_start();
        $arrPurchase = array(
            'strPurchaseCode' => generateTransactionCode($this->input->post('txtDate'),'','purchase',TRUE),
            'strDate'=> $this->input->post('txtDate'),
            'strDescription'=> $this->input->post('txtDescription'),
            'intPBID' => $this->input->post('selPB'),
            'intSupplierID' => $this->input->post('supplierID'),
            'intKPID' => $kp,
            'intSubTotal'=> $this->input->post('subTotalNoTax'),
            'intDiscount'=> $this->input->post('dsc4'),
            'intTax'=> $this->input->post('txtTax'),
            'intCostAdd'=> $this->input->post('subTotalCost'),
            'intGrandTotal'=> $this->input->post('grandTotal'),
            'intPPH'=> $this->input->post('amountPPH'),
            'idSubKontrak' => $this->input->post('idsubkon')
        );

        $intPurchaseID = $this->Mpurchase->add($arrPurchase['strPurchaseCode'],$arrPurchase['strDate'],$arrPurchase['strDescription'],$arrPurchase['intPBID'],$arrPurchase['intSupplierID'],$arrPurchase['intKPID'],$arrPurchase['intSubTotal'],$arrPurchase['intDiscount'],$arrPurchase['intTax'],$arrPurchase['intCostAdd'],$arrPurchase['intGrandTotal'],$arrPurchase['intPPH'], $arrPurchase['idSubKontrak']);
        $totalItem = $this->input->post('totalItem');
        $i = 0; $j = 0;
        $this->load->model('Mpurchaseitem');
        $this->load->model('Mpurchaseorderitem');
        $this->load->model('Mkontrakpembelian');
        while($i < $totalItem) {
            if($this->input->post('productID'.$j)!='') {
                $id = $this->input->post('productID'.$j);
                $poiID = $this->input->post('poiID'.$j);
                $qtyItem = $this->input->post('qtyItem'.$j);//data untuk PO
                $qtyTerima = $this->input->post('qtyItemTerima'.$j);
                $qtyBayar = $this->input->post('qtyBayar'.$j);//data untuk PO
                $hargaBayar = $this->input->post('hargaBayar'.$j);//data untuk PO
                $po_id = $this->input->post('po_id');
                $this->Mpurchaseitem->add($intPurchaseID,$id,$poiID,$qtyItem,$qtyBayar,0,0,0,0,0,$hargaBayar,0,0,0,0,0,0,$qtyTerima);
                $result = $this->Mpurchaseitem->recalculateTotalByPOIID($poiID, $id);
                if($result[0]['total'] == null || $result[0]['total'] == '' ){
                    $result[0]['total'] = 0;
                }
                if($result[0]['total2'] == null || $result[0]['total2'] == ''){
                    $result[0]['total2'] = 0;
                }
                $this->Mpurchaseorderitem->updateTerpurchase($poiID, $result[0]['total'],$result[0]['total2'],$po_id); // update terpurchase PB
                if($kp != '0') $this->Mkontrakpembelian->updatePO($kp,$qtyBayar);
                $i++;
            }
            $j++;
        }
        
        $arrPurchaseData = $this->Mpurchase->getItemByID($intPurchaseID);
        $arrUserLogistik = $this->Mpurchase->getUserByJabatan($intPurchaseID,3); // Logistik
        $arrUserPM = $this->Mpurchase->getUserByJabatan($intPurchaseID,9); // PM
        $arrUserCC = $this->Mpurchase->getUserByJabatan($intPurchaseID,11); // CC

        if(!empty($arrUserLogistik)){
            foreach($arrUserLogistik as $e){
                $dataNotificationOP = array(
                    "noti_user_id" => $e['id'],
                    "noti_title" => $arrPurchaseData['prch_code'] . " telah dibuat untuk proyek " . $e['kont_name'],                     
                    "noti_status" => 2
                );
                $this->Mnotification->add($dataNotificationOP);
            }
        }
        if(!empty($arrUserPM)){
            foreach($arrUserPM as $e){
                $dataNotificationOP = array(
                    "noti_user_id" => $e['id'],
                    "noti_title" => $arrPurchaseData['prch_code'] . " telah dibuat untuk proyek " . $e['kont_name'],                    
                    "noti_status" => 2
                );
                $this->Mnotification->add($dataNotificationOP);
            }
        }
        if(!empty($arrUserCC)){
            foreach($arrUserCC as $e){
                $dataNotificationOP = array(
                    "noti_user_id" => $e['id'],
                    "noti_title" => $arrPurchaseData['prch_code'] . " telah dibuat untuk proyek " . $e['kont_name'],                    
                    "noti_status" => 2
                );
                $this->Mnotification->add($dataNotificationOP);
            }
        }
        $this->_CI->db->trans_complete();
        $this->_arrData['strMessage'] = loadLanguage('adminmessage',$this->session->userdata('jw_language'),'purchase-purchasemade');
        redirect('purchase/view/'.$intPurchaseID);
    }
    $this->load->model('Mcustomer');
    $arrOwner = $this->Mcustomer->getAllCustomer();

    $this->load->model('Msupplier');
    $arrSupplier = $this->Msupplier->getAllSuppliers();

    $this->load->model('Mpurchaseorder');
    $arrKontrak = $this->Mpurchaseorder->getAllKontrak();

    // Load the views
    $this->load->view('sia',array(
        'strViewFile' => 'purchase/add',
        'arrOwner' => $arrOwner,
        'arrSupplier' => $arrSupplier,
        'arrKontrak'  => $arrKontrak,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-addpurchase')
    ));
}

// To display, edit and delete purchase
public function view($intID = 0) {
    # INIT
    // checkTeamPermission('Purchase',$intID);
    $arrReturn = $this->session->userdata('arrReturn');

    if($this->input->post('approval') != '' && $intID != '') {
        $this->_CI->db->trans_start();
        $this->Mpurchase->updateApproval($intID,$this->input->post('approval'),$this->session->userdata('strAdminID'));
        if($this->input->post('approval') == 1) {
            $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
            $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
            $po_id = $this->input->post('po_id');
            if($arrPurchaseData['prch_kontrak_pembelian_id'] != 0) {
                $this->load->model('Mkontrakpembelian');
                $this->Mkontrakpembelian->updateMinusPO($arrPurchaseData['prch_kontrak_pembelian_id'],$arrPurchaseItem[0]['prci_quantity_pb']);
            }
            $this->load->model('Mpurchaseorderitem');
            foreach ($arrPurchaseItem as $e) {
                $result = $this->Mpurchaseitem->recalculateTotalByPOIID($e['prci_proi_id'], $e['prci_product_id']);
                if($result[0]['total'] == null || $result[0]['total'] == ''){
                    $result[0]['total'] = 0;
                }
                if($result[0]['total2'] == null || $result[0]['total2'] == ''){
                    $result[0]['total2'] = 0;
                }
                $this->Mpurchaseorderitem->updateTerpurchase($poiID, $result[0]['total'],$result[0]['total2'],$po_id); // update terpurchase PB
            }
            $this->session->set_flashdata('strMessage','OP ditolak oleh PM');
            redirect('purchase/browse');
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','OP sudah diapprove oleh PM');
        redirect('purchase/browse');
    }

    if($this->input->post('subDelete') != ''){
        $intID = $this->input->post('p_id');
        $this->_CI->db->trans_start();
        $this->Mpurchase->editStatusByID($intID,STATUS_DELETED);
        $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
        $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
        if($arrPurchaseData['prch_kontrak_pembelian_id'] != 0) {
            $this->load->model('Mkontrakpembelian');
            $this->Mkontrakpembelian->updateMinusPO($arrPurchaseData['prch_kontrak_pembelian_id'],$arrPurchaseItem[0]['prci_quantity_pb']);
        }
        $this->load->model('Mpurchaseorderitem');
        foreach ($arrPurchaseItem as $e) {
            $result = $this->Mpurchaseitem->recalculateTotalByPOIID($e['prci_proi_id'], $e['prci_product_id']);
            if($result[0]['total'] == null || $result[0]['total'] == ''){
                $result[0]['total'] = 0;
            }
            if($result[0]['total2'] == null || $result[0]['total2'] == ''){
                $result[0]['total2'] = 0;
            }
            $this->Mpurchaseorderitem->updateTerpurchase($e['prci_proi_id'], $result[0]['total'], $result[0]['total2'], $arrPurchaseData['purchase_order_id'] ); // update terpurchase PB
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','OP dengan kode ' . $arrPurchaseData['prch_code'] . ' telah dihapus');
        redirect('purchase/browse');
    }

    if($this->input->post('close') != ''){
        $purchID = $this->input->post('p_id');        
        $item = $this->Mpurchase->getItemByID($purchID);
        $closed = $this->Mpurchase->close($purchID, $this->session->userdata('strAdminUserName'));       
        $this->session->set_flashdata('strMessage',''.$item['prch_code'].' telah ditutup');
        redirect('purchase/browse');
    }

    $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
    if(!empty($arrPurchaseData)){
        // $arrArrived = $this->Macceptance->getArrivedByPurchaseID($intID);
        $arrPurchaseData['prch_rawstatus'] = $arrPurchaseData['prch_status'];
        $arrPurchaseData['prch_status'] = translateDataIntoHTMLStatements(
            array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'purchase_status', 'allow_null' => 0, 'validation' => ''),
            $arrPurchaseData['approvalPM']);
        
        // Load the purchase item
        $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID2($intID);
        
    } else {
        $arrPurchaseItem = array(); $arrPurchaseList = array();
    }

    $this->load->model('Mdbvo','MAdminLogin');
    $this->MAdminLogin->initialize('adm_login');

    $this->MAdminLogin->dbSelect('','id =' . $arrPurchaseData['cby']);
    $arrLogin = $this->MAdminLogin->getNextRecord('Array');

    $this->MAdminLogin->initialize('adm_login');

    $this->MAdminLogin->dbSelect('','id =' . $arrPurchaseData['mby']);
    $arrEditedBy = $this->MAdminLogin->getNextRecord('Array');

    $arrData = array(
        'intPurchaseID' => $intID,
        'intPurchaseTotal' => $intPurchaseTotal,
        'intPurchaseGrandTotal' => $intPurchaseGrandTotal,
        'arrPurchaseItem' => $arrPurchaseItem,
        'arrPurchaseData' => $arrPurchaseData,
        'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
        'arrLogin' => $arrLogin,
        'arrArrived' => $arrArrived,
        'numberTax' => $this->input->get('number'),
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print e nota
        //$dataPrint=$this->Mpurchase->getPrintDataByID($intID);
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchase',
        ), $arrData));
    } else {
        # Load all other purchase data the user has ever made
        $arrPurchaseList = $this->Mpurchase->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'purchase/view',
            'arrPurchaseList' => $arrPurchaseList,
            'arrUser' => $arrUser,
            'arrEditedBy' => $arrEditedBy,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchasestatus')
        ), $arrData, $this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus'])));
    }
}

public function edit($intID) {
    $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
    if(!empty($arrPurchaseData)) {
        // $arrArrived = $this->Macceptance->getArrivedByPurchaseID($intID);
        $arrPurchaseData['prch_rawstatus'] = $arrPurchaseData['prch_status'];
        $arrPurchaseData['prch_status'] = translateDataIntoHTMLStatements(
            array('title' => 'selStatus', 'field_type' => 'SELECT','description' => 'purchase_status', 'allow_null' => 0, 'validation' => ''),
            $arrPurchaseData['approvalPM']);
        
        // Load the purchase item
        $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID2($intID);
        
    } else {
        $arrPurchaseItem = array();
    }

    $arrData = array(
        'intPurchaseID' => $intID,
        'arrPurchaseItem' => $arrPurchaseItem,
        'arrPurchaseData' => $arrPurchaseData,
        // 'arrPurchaseBonusItem' => $arrPurchaseItemBonus,
        // 'arrArrived' => $arrArrived,
        'numberTax' => $this->input->get('number'),
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );
    $this->load->view('sia',array_merge(array(
            'strViewFile' => 'purchase/edit',
            // 'arrPurchaseList' => $arrPurchaseList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-editpurchase')
        ), $arrData, $this->admlinklist->getMenuPermission(22,$arrPurchaseData['prch_rawstatus'])));
}

public function browse($intPage = 0) {
    if($this->input->post('subSearch') != '') {
        $arrPurchase = $this->Mpurchase->searchBySupplier($this->input->post('txtSearchProyek'), $this->input->post('txtSearchNoOp'), $this->input->post('txtSearchNoSupplier'));
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('purchase/browse', NULL, FALSE).'">[Back]</a>';
        $this->_arrData['strMessage'] = "Search result (".(!empty($arrPurchase) ? count($arrPurchase) : '0')." records).";

    } else {
        $arrPagination['base_url'] = site_url("purchase/browse?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->Mpurchase->getCount();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrPurchase = $this->Mpurchase->getItems($intPage,$arrPagination['per_page']);
    }
    
    if($this->input->post('delPurchase') != ''){
        $intID = $this->input->post('delPurchase');
        $this->_CI->db->trans_start();
        $this->Mpurchase->editStatusByID($intID,1);
        $arrPurchaseData = $this->Mpurchase->getItemByID($intID);
        $arrPurchaseItem = $this->Mpurchaseitem->getItemsByPurchaseID($intID);
        if($arrPurchaseData['prch_kontrak_pembelian_id'] != 0) {
            $this->load->model('Mkontrakpembelian');
            $this->Mkontrakpembelian->updateMinusPO($arrPurchaseData['prch_kontrak_pembelian_id'],$arrPurchaseItem[0]['prci_quantity_pb']);
        }
        $this->load->model('Mpurchaseorderitem');
        foreach ($arrPurchaseItem as $e) {
            $result = $this->Mpurchaseitem->recalculateTotalByPOIID($e['prci_proi_id'], $e['prci_product_id']);
            if($result[0]['total'] == null || $result[0]['total'] == ''){
                $result[0]['total'] = 0;
            }
            if($result[0]['total2'] == null || $result[0]['total2'] == ''){
                $result[0]['total2'] = 0;
            }
            $this->Mpurchaseorderitem->updateTerpurchase($e['prci_proi_id'], $result[0]['total'], $result[0]['total2'], $arrPurchaseData['purchase_order_id'] ); // update terpurchase PB
        }
        $this->_CI->db->trans_complete();
        $this->session->set_flashdata('strMessage','OP dengan kode ' . $arrPurchaseData['prch_code'] . ' telah dihapus');
        redirect('purchase/browse');
    }

    if(!empty($arrPurchase)) for($i = 0; $i < count($arrPurchase); $i++) {
 //        $status = '';
 //        if ($arrPurchase[$i]['prch_status'] == 4) $status = 'PO sudah terBPB';
 //        else if($arrPurchase[$i]['approvalPM'] == 1) $status = 'PO Ditolak PM';
 //        else if($arrPurchase[$i]['approvalPM'] == 2) $status = 'Menunggu Persetujuan PM';
 //        else if($arrPurchase[$i]['approvalPM'] == 3) $status = 'PO Disetujui PM';

        $arrPurchase[$i] = array_merge($arrPurchase[$i],$this->admlinklist->getMenuPermission(22,$arrPurchase[$i]['prch_status']));
        // $arrPurchase[$i]['prch_rawstatus'] = $arrPurchase[$i]['prch_status'];
        // $arrPurchase[$i]['prch_status'] = $status;
        $arrPurchase[$i]['prch_total'] = $arrPurchase[$i]['prch_grandtotal'];
    }

    $this->load->model('Mtinydbvo');
    $this->Mtinydbvo->initialize('invoice_status');

    $this->load->view('sia',array(
        'strViewFile' => 'purchase/browse',
        'strPage' => $strPage,
        'strBrowseMode' => $strBrowseMode,
        'arrPurchase' => $arrPurchase,
        'arrInvoiceStatus' => $this->Mtinydbvo->getAllData(),
        'strSearchKey' => $this->input->post('txtSearchValue'),
        'strSearchDate' => $this->input->post('txtSearchDate'),
        'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
        'intSearchStatus' => $this->input->post('txtSearchStatus'),
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'purchase-purchase')
    ));
}

}

/* End of File */