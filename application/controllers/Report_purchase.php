<?php

/* 
PUBLIC FUNCTION:
- detailbeliperbarangbysupplier($intPage = 0)
- detailbelipersupplierbybarang($intPage = 0)
- rekapbelipersupplierbybarang($intPage = 0)

- pembelianrekap($intPage = 0)
- purchasepersupplier($intPage = 0)

PRIVATE FUNCTION:
- __construct()
*/

class Report_purchase extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    $this->load->model('Mreportpurchase', 'MReport');
}

public function detailbeliperbarangbysupplier($intPage = 0) {
    $this->_getMenuHelpContent(112,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strProductID = $this->input->post('productID');
    $strProductCode = $this->input->post('productCode');
    $strProductName = $this->input->post('productName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('productid') != '')
            $arrItems = $this->MReport->searchDetailBeliPerBarangBySupplier($this->input->get('productid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strProductID = $this->input->get('productid');

        $this->load->model('Mproduct');
        $product = $this->Mproduct->getItemByID($strProductID);
        $product['strName'] = formatProductName('',$product['prod_title'],$product['prob_title'],$product['proc_title']);


        $strProductCode = $product['prod_code'];
        $strProductName = $product['strName'];

        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('productID') != '')
			$arrItems = $this->MReport->searchDetailBeliPerBarangBySupplier($this->input->post('productID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_purchase/detailbeliperbarangbysupplier', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_purchase/detailbeliperbarangbysupplier?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailBeliPerBarangBySupplier();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailBeliPerBarangBySupplier($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['prci_price'] - ($arrItems[$i]['prci_price'] * $arrItems[$i]['prci_discount1']);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['prci_discount2']);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['prci_discount3']);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strProductID' => $strProductID,
        'strProductCode' => $strProductCode,
        'strProductName' => $strProductName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo']
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailbeliperbarangbysupplier_export', $arrData, true),
        'detail_beli_per_barang_by_supplier.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailbeliperbarangbysupplier',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_purchase/detailbeliperbarangbysupplier',
            'strIncludeJsFile' => 'report_purchase/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function detailbelipersupplierbybarang($intPage = 0) {
    $this->_getMenuHelpContent(114,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSupplierID = $this->input->post('supplierID');
    $strSupplierCode = $this->input->post('supplierCode');
    $strSupplierName = $this->input->post('supplierName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->post('subExcel') != '' || $this->input->get('excel') != '') {

        if($this->input->get('suppid') != '')
            $arrItems = $this->MReport->searchDetailBeliPerSupplierByBarang($this->input->get('suppid'),$this->input->get('start'),$this->input->get('end'));
        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSupplierID = $this->input->get('suppid');

        $this->load->model('Msupplier');
        $supplier = $this->Msupplier->getItemByID($strSupplierID);

        $strSupplierCode = $supplier['supp_code'];
        $strSupplierName = $supplier['supp_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('supplierID') != '')
			$arrItems = $this->MReport->searchDetailBeliPerSupplierByBarang($this->input->post('supplierID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		
		else $arrItems = '';
		
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_purchase/detailbelipersupplierbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_purchase/detailbelipersupplierbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountDetailBeliPerSupplierByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsDetailBeliPerSupplierByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;

            $arrItems[$i]['netprice'] = $arrItems[$i]['prci_price'] - ($arrItems[$i]['prci_price'] * $arrItems[$i]['prci_discount1']);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['prci_discount2']);
            $arrItems[$i]['netprice'] = $arrItems[$i]['netprice'] - ($arrItems[$i]['netprice'] * $arrItems[$i]['prci_discount3']);
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSupplierID' => $strSupplierID,
        'strSupplierCode' => $strSupplierCode,
        'strSupplierName' => $strSupplierName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/detailbelipersupplierbybarang_export', $arrData, true),
        'detail_beli_per_supplier_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'detailbelipersupplierbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/detailbelipersupplierbybarang',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

public function rekapbelipersupplierbybarang($intPage = 0) {
    $this->_getMenuHelpContent(94,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSupplierID = $this->input->post('supplierID');
    $strSupplierCode = $this->input->post('supplierCode');
    $strSupplierName = $this->input->post('supplierName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {

        if($this->input->get('suppid') != '')
            $arrItems = $this->MReport->searchRekapBeliPerSupplierByBarang($this->input->get('suppid'),$this->input->get('start'),$this->input->get('end'));

        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSupplierID = $this->input->get('suppid');

        $this->load->model('Msupplier');
        $supplier = $this->Msupplier->getItemByID($strSupplierID);

        $strSupplierCode = $supplier['supp_code'];
        $strSupplierName = $supplier['supp_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('supplierID') != '')
			$arrItems = $this->MReport->searchRekapBeliPerSupplierByBarang($this->input->post('supplierID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
		else $arrItems = '';

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_purchase/rekapbelipersupplierbybarang', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_purchase/rekapbelipersupplierbybarang?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountRekapBeliPerSupplierByBarang();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsRekapBeliPerSupplierByBarang($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSupplierID' => $strSupplierID,
        'strSupplierCode' => $strSupplierCode,
        'strSupplierName' => $strSupplierName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/rekapbelipersupplierbybarang_export', $arrData, true),
        'rekap_beli_per_supplier_by_barang.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'rekapbelipersupplierbybarang',
        ), $arrData));
    } else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_invoice/rekapbelipersupplierbybarang',
            'strIncludeJsFile' => 'report_invoice/detailjualperbarangbycustomer',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }

}

public function pembelianrekap($intPage = 0) {
    $this->_getMenuHelpContent(111,true,'adminpage');

    $cname = 'pembelianrekap';

    // 1. persiapan variabel untuk menampung filter
    $strStart = $this->input->post('txtStart');
    if(empty($strStart))
        $strStart = date('Y/m/d');

    $strEnd = $this->input->post('txtEnd');
    if(empty($strEnd))
        $strEnd = date('Y/m/d');

    $strSupplierID = $this->input->post('supplierID');
    if(empty($strSupplierID))
        $strSupplierID = '';
    $strSupplierCode = $this->input->post('supplierCode');
    if(empty($strSupplierCode))
        $strSupplierCode = '';
    $strSupplierName = $this->input->post('supplierName');
    if(empty($strSupplierName))
        $strSupplierName = '';
    // 1. end
    
    // 2. ketika tombol print diklik
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        
        // 2a. persiapan variabel untuk menampung filter dengan cara GET
        $strStart = $this->input->get('start');
        if(empty($strStart))
            $strStart = date('Y/m/d');
        
        $strEnd = $this->input->get('end');
        if(empty($strEnd))
            $strEnd = date('Y/m/d');
        
        if($this->input->get('supplierid') != '')
        {
            $strSupplierID = $this->input->get('supplierid');
            
            $this->load->model('Msupplier');
            $user = $this->Msupplier->getItemByID($strSupplierID);
            $strSupplierCode = $user['supp_code'];
            $strSupplierName = $user['supp_name'];
        }
        else
        {
            $strSupplierID = '';
            $strSupplierCode = '';
            $strSupplierName = '';
        }
        // 2a. end
        
        // 2b. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchPembelianRekap($strStart,$strEnd,$strSupplierName);
        $strPage = '';
        // 2b. end
        
    }
    // 2. end
    
    // 3. ketika tombol filter diklik
    else if($this->input->post('subSearch') != '') {
        
        // 3a. pemanggilan function model dengan dibatasi filter
        $arrItems = $this->MReport->searchPembelianRekap($strStart,$strEnd,$strSupplierName);
        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_purchase/'.$cname, NULL, FALSE).'">[Back]</a>';
        // 3a. end
        
    }
    // 3. end
    
    // 4. ketika pertama kali menu dibuka
    else {
        
        // 4a. persiapan paginasi & pemanggilan function model untuk menghitung total semua data
        $arrPagination['base_url'] = site_url("report_purchase/".$cname."?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountPembelianRekap();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();
        
        $strBrowseMode = '';
        if($this->input->get('page') != '')
            $intPage = $this->input->get('page');
        // 4a. end
        
        // 4b. pemanggilan function model dengan dibatasi paginasi yang sedang aktif diakses
        $arrItems = $this->MReport->getItemsPembelianRekap($intPage,$arrPagination['per_page']);
        // 4b. end
        
    }
    // 4.end
    
    if (!empty($arrItems)) {
        for ($i = 0; $i < count($arrItems); $i++) {
            $arrItems[$i]['nourut'] = $i + 1;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSupplierID' => $strSupplierID,
        'strSupplierCode' => $strSupplierCode,
        'strSupplierName' => $strSupplierName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // 5a. tampilan excel (views\jwshop1\print\vreport..._export)
    if($this->input->get('excel') != '') {
        HTMLToExcel(
            $this->load->view($this->config->item('jw_style').'_print/report'.$cname.'_export', $arrData, true),
        $cname.'.xls');
    }
    // 5a. end

    // 5b. tampilan print (views\jwshop1\print\vreport...) >> cukup dibuatkan file-nya, otomatis akan digenerate dari versi export excel
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'report'.$cname,
        ), $arrData));
    }
    // 5b. end

    // 5c. tampilan default menu (views\jwshop1\vreport...)
    else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_purchase/'.$cname,
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
    // 5c. end
}

public function purchasepersupplier($intPage = 0) {
    $this->_getMenuHelpContent(113,true,'adminpage');

    $strStart = $this->input->post('txtStart');
    $strEnd = $this->input->post('txtEnd');
    $strSupplierID = $this->input->post('supplierID');
    $strSupplierCode = $this->input->post('supplierCode');
    $strSupplierName = $this->input->post('supplierName');

    if($this->input->post('subPrint') != '' || $this->input->get('print') != '' || $this->input->post('subExcel') != '' || $this->input->get('excel') != '') {

        if($this->input->get('suppid') != '')
            $arrItems = $this->MReport->searchPurchasePerSupplierBySupplier($this->input->get('suppid'),$this->input->get('start'),$this->input->get('end'));
        else $arrItems = '';

        $strStart = $this->input->get('start');
        $strEnd = $this->input->get('end');
        $strSupplierID = $this->input->get('suppid');

        $this->load->model('Msupplier');
        $supplier = $this->Msupplier->getItemByID($strSupplierID);

        $strSupplierCode = $supplier['supp_code'];
        $strSupplierName = $supplier['supp_name'];


        $strPage = '';
    } else if($this->input->post('subSearch') != '') {
        if($this->input->post('supplierID') != '')
            $arrItems = $this->MReport->searchPurchasePerSupplierBySupplier($this->input->post('supplierID'),$this->input->post('txtStart'),$this->input->post('txtEnd'));
        else $arrItems = '';

        $strPage = '';
        $strBrowseMode = '<a href="'.site_url('report_purchase/purchasepersupplier', NULL, FALSE).'">[Back]</a>';
    } else {

        $arrPagination['base_url'] = site_url("report_purchase/purchasepersupplier?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = $this->MReport->getCountPurchasePerSupplierBySupplier();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $strBrowseMode = '';
        if($this->input->get('page') != '') $intPage = $this->input->get('page');

        $arrItems = $this->MReport->getItemsPurchasePerSupplierBySupplier($intPage,$arrPagination['per_page']);
    }

    if (!empty($arrItems)) {
        $this->load->model('Munit');
        for($i = 0; $i < count($arrItems); $i++) {
            $txtDiscount1 = $arrItems[$i]['prci_discount1'];
            $txtDiscount2 = $arrItems[$i]['prci_discount2'];
            $txtDiscount3 = $arrItems[$i]['prci_discount3'];
            $intQty1 = $arrItems[$i]['prci_quantity1'];
            $intQty2 = $arrItems[$i]['prci_quantity2'];
            $intQty3 = $arrItems[$i]['prci_quantity3'];
            $convTemp = $this->Munit->getConversion($arrItems[$i]['prci_unit1']);
            $intConv1 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrItems[$i]['prci_unit2']);
            $intConv2 = $convTemp[0]['unit_conversion'];
            $convTemp = $this->Munit->getConversion($arrItems[$i]['prci_unit3']);
            $intConv3 = $convTemp[0]['unit_conversion'];
            $intPrice = $arrItems[$i]['prci_price'];
            $intPurchaseTotalTemp=0;

            if($intConv1 > 0) $intPurchaseTotalTemp = ((float) $intPrice * (float) $intQty1);
            if($intConv2 > 0) $intPurchaseTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv2 * (float) $intQty2);
            if($intConv3 > 0) $intPurchaseTotalTemp += ((float) $intPrice / (float) $intConv1 * (float) $intConv3 * (float) $intQty3);

            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount1 / 100);
            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount2 / 100);
            $intPurchaseTotalTemp = $intPurchaseTotalTemp - ($intPurchaseTotalTemp * $txtDiscount3 / 100);

            $arrItems[$i]['prci_subtotal'] = $intPurchaseTotalTemp;
            $arrItems[$i]['prci_conv1'] = $intConv1;
            $arrItems[$i]['prci_conv2'] = $intConv2;
            $arrItems[$i]['prci_conv3'] = $intConv3;
        }
    }

    $arrData = array(
        'strPage' => $strPage,
        'arrItems' => $arrItems,
        'strStart' => $strStart,
        'strEnd' => $strEnd,
        'strSupplierID' => $strSupplierID,
        'strSupplierCode' => $strSupplierCode,
        'strSupplierName' => $strSupplierName,
        'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    // Load the views
    if($this->input->get('excel') != '') { //print
        HTMLToExcel(
            $this->load->view('sia_print/purchasepersupplier_export', $arrData, true),
        'purchase_per_supplier.xls');
    }
    if($this->input->post('subPrint') != '' || $this->input->get('print') != '') {//print
        $this->load->view('sia_print',array_merge(array(
            'strPrintFile' => 'purchasepersupplier',
        ), $arrData));
    }else {
        $this->load->view('sia',array_merge(array(
            'strViewFile' => 'report_purchase/purchasepersupplier',
            'strBrowseMode' => $strBrowseMode,
            'strPageTitle' => 'Laporan'
        ), $arrData));
    }
}

}