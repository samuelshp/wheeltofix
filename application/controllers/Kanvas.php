<?php
/*
PUBLIC FUNCTION: 
- index()
- browse(intPage)
- view(intID)

PRIVATE FUNCTION:
- __construct()
*/

class Kanvas extends JW_Controller {

public function __construct() {
	parent::__construct();
	if($this->session->userdata('strAdminUserName') == '') redirect();

	// Generate the menu list
	$this->load->model('Madmlinklist','admlinklist');
	$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

	$this->load->model('Mkanvas');
	$this->load->model('Mkanvasitem');

	$this->_getMenuHelpContent(47,true,'adminpage');
}

public function index() {

	if($this->input->post('smtUpdateKanvas') != '' || $this->input->post('smtMakeKanvas') != '') {

		if($this->input->post('smtMakeKanvas') != '') {
			$this->load->model('Munit');
			$arrKanvas = array(
				'intWarehouseID' => $this->input->post('warehouseID'),
				'intSalesmanID' => $this->input->post('salesmanID'),
				'strLoadingDate' => $this->input->post('txtLoadingDate'),
				'strDischargeDate' => $this->input->post('txtDischarge'),
				'strDate' => $this->input->post('txtDate'),
				'strDescription' => $this->input->post('txaDescription')
			);

			/*
			 * function add($intWarehouseID,$intSalesmanID,$strLoadingDate,$strDischargeDate,$intStatus,$strDescription,
			 *              $strDate)
			 */
			$intKanvasID = $this->Mkanvas->add($arrKanvas['intWarehouseID'],
											   $arrKanvas['intSalesmanID'],
											   $arrKanvas['strLoadingDate'],
											   $arrKanvas['strDischargeDate'],
											   2,
											   $arrKanvas['strDescription'],
											   $arrKanvas['strDate']);

			$totalItemSisa = $this->input->post('totalItemSisa');
			for ($i = 0; $i < $totalItemSisa; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 0;
				$qty1Item=$this->input->post('qty1ItemSisaX' . $i);
				$qty2Item=$this->input->post('qty2ItemSisaX' . $i);
				$qty3Item=$this->input->post('qty3ItemSisaX' . $i);
				$unit1Item=$this->input->post('sel1UnitSisaID' . $i);
				$unit2Item=$this->input->post('sel2UnitSisaID' . $i);
				$unit3Item=$this->input->post('sel3UnitSisaID' . $i);
				$prodItem = $this->input->post('prodItemSisa' . $i);
				$probItem = $this->input->post('probItemSisa' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemSisa' . $i);

				$this->Mkanvasitem->add($intKanvasID,
										$idItem,
										$strProductDescription,
										$intDay,
										$intRecordType,
										$qty1Item,
										$qty2Item,
										$qty3Item,
										$unit1Item,
										$unit2Item,
										$unit3Item);
			}

			$totalItemJual = $this->input->post('totalItemJual');
			for ($i = 0; $i < $totalItemJual; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 1;
				$qty1Item=$this->input->post('qty1ItemJualX' . $i);
				$qty2Item=$this->input->post('qty2ItemJualX' . $i);
				$qty3Item=$this->input->post('qty3ItemJualX' . $i);
				$unit1Item=$this->input->post('sel1UnitJualID' . $i);
				$unit2Item=$this->input->post('sel2UnitJualID' . $i);
				$unit3Item=$this->input->post('sel3UnitJualID' . $i);
				$prodItem = $this->input->post('prodItemJual' . $i);
				$probItem = $this->input->post('probItemJual' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemJual' . $i);

				$this->Mkanvasitem->add($intKanvasID,
					$idItem,
					$strProductDescription,
					$intDay,
					$intRecordType,
					$qty1Item,
					$qty2Item,
					$qty3Item,
					$unit1Item,
					$unit2Item,
					$unit3Item);
			}

			$totalItemTurun = $this->input->post('totalItemTurun');
			for ($i = 0; $i < $totalItemTurun; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 2;
				$qty1Item=$this->input->post('qty1ItemTurunX' . $i);
				$qty2Item=$this->input->post('qty2ItemTurunX' . $i);
				$qty3Item=$this->input->post('qty3ItemTurunX' . $i);
				$unit1Item=$this->input->post('sel1UnitTurunID' . $i);
				$unit2Item=$this->input->post('sel2UnitTurunID' . $i);
				$unit3Item=$this->input->post('sel3UnitTurunID' . $i);
				$prodItem = $this->input->post('prodItemTurun' . $i);
				$probItem = $this->input->post('probItemTurun' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemTurun' . $i);

				$this->Mkanvasitem->add($intKanvasID,
					$idItem,
					$strProductDescription,
					$intDay,
					$intRecordType,
					$qty1Item,
					$qty2Item,
					$qty3Item,
					$unit1Item,
					$unit2Item,
					$unit3Item);
			}

			$totalItemRetur = $this->input->post('totalItemRetur');
			for ($i = 0; $i < $totalItemRetur; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 3;
				$qty1Item=$this->input->post('qty1ItemReturX' . $i);
				$qty2Item=$this->input->post('qty2ItemReturX' . $i);
				$qty3Item=$this->input->post('qty3ItemReturX' . $i);
				$unit1Item=$this->input->post('sel1UnitReturID' . $i);
				$unit2Item=$this->input->post('sel2UnitReturID' . $i);
				$unit3Item=$this->input->post('sel3UnitReturID' . $i);
				$prodItem = $this->input->post('prodItemRetur' . $i);
				$probItem = $this->input->post('probItemRetur' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemRetur' . $i);

				$this->Mkanvasitem->add($intKanvasID,
					$idItem,
					$strProductDescription,
					$intDay,
					$intRecordType,
					$qty1Item,
					$qty2Item,
					$qty3Item,
					$unit1Item,
					$unit2Item,
					$unit3Item);
			}

			$this->session->set_flashdata('strMessage',loadLanguage('adminmessage',
				$this->session->userdata('jw_language'),'kanvas-kanvasmade'));

			redirect('kanvas/view/'.$intKanvasID);
		}

	}
	else {
        $this->load->view('sia',array(
	        'strViewFile' => 'kanvas/',
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-addkanvas')
	    ));
	}
}

public function browse($intPage = 0) {
	if ($this->input->post('subSearch') != '') {
		$arrKanvas = $this->Mkanvas->searchBySalesman($this->input->post('txtSearchValue'));

		$strPage = '';

		$this->_arrData['strMessage'] = "Search by Salesman with keyword <i>'".$this->input->post('txtSearchValue')."'</i>.";
	} else {
		$arrPagination['base_url'] = site_url("kanvas/browse?pagination=true", NULL, FALSE);
		$arrPagination['total_rows'] = $this->Mkanvas->getCount();
		$arrPagination['per_page'] = $this->config->item('jw_item_count');
		$arrPagination['uri_segment'] = 3;
		$this->pagination->initialize($arrPagination);
		$strPage = $this->pagination->create_links();

		$strBrowseMode = '';
		if($this->input->get('page') != '') $intPage = $this->input->get('page');

		$arrKanvas = $this->Mkanvas->getItems($intPage,$arrPagination['per_page']);
	}

	if (!empty($arrKanvas)) {
		for ($i = 0; $i < count($arrKanvas); $i++) {
			$arrKanvas[$i] = array_merge($arrKanvas[$i],$this->admlinklist->getMenuPermission(47,$arrKanvas[$i]['kanv_status']));
			$arrKanvas[$i]['kanv_rawstatus'] = $arrKanvas[$i]['kanv_status'];
			$arrKanvas[$i]['kanv_status'] = translateDataIntoReadableStatements(array('field_type' => 'SELECT', 'description' => 'invoice_status'),$arrKanvas[$i]['kanv_status']);//tiny dp invoice_status
		}
	}

	$this->load->view('sia',array(
        'strViewFile' => 'kanvas/browse',
		'strPage' => $strPage,
		'strBrowseMode' => $strBrowseMode,
		'arrKanvas' => $arrKanvas,
        'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvas')
    ));
}

public function view($intID = 0) {
	$this->load->model('Mkanvas');
	if ($this->input->post('subSave') != '' && $intID != '') {

		$arrKanvasData = $this->Mkanvas->getItemByID($intID);
		if (compareData($arrKanvasData['kanv_status'],array(0,2))) {
			$this->Mkanvas->editByID2($intID,$this->input->post('txaDescription'),$this->input->post('selStatus'),
				$this->input->post('txtDischargeDate'));

			/*
			 * Simpan perubahan muatan sisa
			 */
			$intDay = $this->input->post('intHari');
			$arrKanvasItemSisa = $this->Mkanvasitem->getItemsByKanvasIDByDayForEditing($intID,0,$intDay);
			if (!empty($arrKanvasItemSisa)) {

				$arrPostQty1 = $this->input->post('txtItemSisa1Qty');
				$arrPostQty2 = $this->input->post('txtItemSisa2Qty');
				$arrPostQty3 = $this->input->post('txtItemSisa3Qty');

                $arrContainID = $this->input->post('idKaviSisaID');                                                
                
				foreach ($arrKanvasItemSisa as $e) {
					if (!empty($arrContainID)) {
						if (in_array($e['id'], $arrContainID)) {							
                                if ($e['saldo'] == -1) {
								$this->Mkanvasitem->editByID($e['id'],
															 $intDay,
															 0,
															 $arrPostQty1[$e['id']],
															 $arrPostQty2[$e['id']],
															 $arrPostQty3[$e['id']]);
                                }
						} else {
                            if ($e['id'] >= 0) {
							    $this->Mkanvasitem->deleteByID($e['id']);
                            }
						}
					} else {
                        if ($e['id'] >= 0) {
						    $this->Mkanvasitem->deleteByID($e['id']);
                        }
					}
				}
			}
			$totalItemSisa = $this->input->post('totalNewItemSisa');

			for ($i = 0; $i < $totalItemSisa; $i++) {
                /*
                 * Record yang merupakan saldo yang berasal dari stok hari sebelumnnya, tidak
                 * perlu disimpan.
                 */
                $asSaldo = $this->input->post('asSaldoX' . $i);
                /*trigger_error('asSaldo : '.$asSaldo);*/
                /*if (empty($asSaldo)) {
                    trigger_error('empty : '.$asSaldo.' ;i :'. $i);
                } else {
                    trigger_error('asSaldo : '.$asSaldo.' ;i :'. $i);
                }*/
                
//                if (!empty($asSaldo)) {
                    if (($asSaldo == 0) || ($asSaldo == '0')) {

                        $intDay = $this->input->post('intHari');
                        $intRecordType = 0;

                        $qty1Item=$this->input->post('qty1ItemSisaX' . $i);
                        $qty2Item=$this->input->post('qty2ItemSisaX' . $i);
                        $qty3Item=$this->input->post('qty3ItemSisaX' . $i);
                        $unit1Item=$this->input->post('sel1UnitSisaID' . $i);
                        $unit2Item=$this->input->post('sel2UnitSisaID' . $i);
                        $unit3Item=$this->input->post('sel3UnitSisaID' . $i);
                        $prodItem = $this->input->post('prodItemSisa' . $i);
                        $probItem = $this->input->post('probItemSisa' . $i);
                        $strProductDescription = formatProductName('',$prodItem,$probItem);
                        $idItem = $this->input->post('idItemSisa' . $i);


                        if ($idItem > 0) {
                            $this->Mkanvasitem->add($intID,
                                $idItem,
                                $strProductDescription,
                                $intDay,
                                $intRecordType,
                                $qty1Item,
                                $qty2Item,
                                $qty3Item,
                                $unit1Item,
                                $unit2Item,
                                $unit3Item);
                        }
                    }
//                }
			}

			/*
			 * Simpan perubahan muatan jual
			 */
			$intDay = $this->input->post('intHari');
			$arrKanvasItemJual = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID,1,$intDay);
			if (!empty($arrKanvasItemJual)) {

				$arrPostQty1 = $this->input->post('txtItemJual1Qty');
				$arrPostQty2 = $this->input->post('txtItemJual2Qty');
				$arrPostQty3 = $this->input->post('txtItemJual3Qty');

				$arrContainID = $this->input->post('idKaviJualID');
				foreach ($arrKanvasItemJual as $e) {
					if (!empty($arrContainID)) {
						if (in_array($e['id'], $arrContainID)) {
							$this->Mkanvasitem->editByID($e['id'],
								$intDay,
								1,
								$arrPostQty1[$e['id']],
								$arrPostQty2[$e['id']],
								$arrPostQty3[$e['id']]);
						} else {
							$this->Mkanvasitem->deleteByID($e['id']);
						}
					} else {
						//$this->Mkanvasitem->deleteByID($e['id']);
					}
				}
			}
			$totalItemJual = $this->input->post('totalItemJual');
			for ($i = 0; $i < $totalItemJual; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 1;
				$qty1Item=$this->input->post('qty1ItemJualX' . $i);
				$qty2Item=$this->input->post('qty2ItemJualX' . $i);
				$qty3Item=$this->input->post('qty3ItemJualX' . $i);
				$unit1Item=$this->input->post('sel1UnitJualID' . $i);
				$unit2Item=$this->input->post('sel2UnitJualID' . $i);
				$unit3Item=$this->input->post('sel3UnitJualID' . $i);
				$prodItem = $this->input->post('prodItemJual' . $i);
				$probItem = $this->input->post('probItemJual' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemJual' . $i);

				if ($idItem > 0) {
					$this->Mkanvasitem->add($intID,
						$idItem,
						$strProductDescription,
						$intDay,
						$intRecordType,
						$qty1Item,
						$qty2Item,
						$qty3Item,
						$unit1Item,
						$unit2Item,
						$unit3Item);
				}
			}

			/*
			 * Simpan perubahan muatan turun
			 */
			$intDay = $this->input->post('intHari');
			$arrKanvasItemTurun = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID,2,$intDay);
			if (!empty($arrKanvasItemTurun)) {

				$arrPostQty1 = $this->input->post('txtItemTurun1Qty');
				$arrPostQty2 = $this->input->post('txtItemTurun2Qty');
				$arrPostQty3 = $this->input->post('txtItemTurun3Qty');

				$arrContainID = $this->input->post('idKaviTurunID');
				foreach ($arrKanvasItemTurun as $e) {
					if (!empty($arrContainID)) {
						if (in_array($e['id'], $arrContainID)) {
							$this->Mkanvasitem->editByID($e['id'],
								$intDay,
								2,
								$arrPostQty1[$e['id']],
								$arrPostQty2[$e['id']],
								$arrPostQty3[$e['id']]);
						} else {
							$this->Mkanvasitem->deleteByID($e['id']);
						}
					} else {
						$this->Mkanvasitem->deleteByID($e['id']);
					}
				}
			}
			$totalItemTurun = $this->input->post('totalItemTurun');
			for ($i = 0; $i < $totalItemTurun; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 2;
				$qty1Item=$this->input->post('qty1ItemTurunX' . $i);
				$qty2Item=$this->input->post('qty2ItemTurunX' . $i);
				$qty3Item=$this->input->post('qty3ItemTurunX' . $i);
				$unit1Item=$this->input->post('sel1UnitTurunID' . $i);
				$unit2Item=$this->input->post('sel2UnitTurunID' . $i);
				$unit3Item=$this->input->post('sel3UnitTurunID' . $i);
				$prodItem = $this->input->post('prodItemTurun' . $i);
				$probItem = $this->input->post('probItemTurun' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemTurun' . $i);

				if ($idItem > 0) {
					$this->Mkanvasitem->add($intID,
						$idItem,
						$strProductDescription,
						$intDay,
						$intRecordType,
						$qty1Item,
						$qty2Item,
						$qty3Item,
						$unit1Item,
						$unit2Item,
						$unit3Item);
				}
			}

			/*
			 * Simpan perubahan muatan retur
			 */
			$intDay = $this->input->post('intHari');
			$arrKanvasItemRetur = $this->Mkanvasitem->getItemsByKanvasID($intID,3);
			if (!empty($arrKanvasItemRetur)) {

				$arrPostQty1 = $this->input->post('txtItemRetur1Qty');
				$arrPostQty2 = $this->input->post('txtItemRetur2Qty');
				$arrPostQty3 = $this->input->post('txtItemRetur3Qty');

				$arrContainID = $this->input->post('idKaviReturID');
				foreach ($arrKanvasItemRetur as $e) {
					if (!empty($arrContainID)) {
						if (in_array($e['id'], $arrContainID)) {
							$this->Mkanvasitem->editByID($e['id'],
								$intDay,
								3,
								$arrPostQty1[$e['id']],
								$arrPostQty2[$e['id']],
								$arrPostQty3[$e['id']]);
						} else {
							$this->Mkanvasitem->deleteByID($e['id']);
						}
					} else {
						$this->Mkanvasitem->deleteByID($e['id']);
					}
				}
			}
			$totalItemRetur = $this->input->post('totalItemRetur');
			for ($i = 0; $i < $totalItemRetur; $i++) {
				$intDay = $this->input->post('intHari');
				$intRecordType = 3;
				$qty1Item=$this->input->post('qty1ItemReturX' . $i);
				$qty2Item=$this->input->post('qty2ItemReturX' . $i);
				$qty3Item=$this->input->post('qty3ItemReturX' . $i);
				$unit1Item=$this->input->post('sel1UnitReturID' . $i);
				$unit2Item=$this->input->post('sel2UnitReturID' . $i);
				$unit3Item=$this->input->post('sel3UnitReturID' . $i);
				$prodItem = $this->input->post('prodItemRetur' . $i);
				$probItem = $this->input->post('probItemRetur' . $i);
				$strProductDescription = formatProductName('',$prodItem,$probItem);
				$idItem = $this->input->post('idItemRetur' . $i);

				if ($idItem > 0) {
					$this->Mkanvasitem->add($intID,
						$idItem,
						$strProductDescription,
						$intDay,
						$intRecordType,
						$qty1Item,
						$qty2Item,
						$qty3Item,
						$unit1Item,
						$unit2Item,
						$unit3Item);
				}
			}

		}
		else {
			$this->Mkanvas->editByID($intID, $this->input->post('selStatus'),$this->input->post('txtDischargeDate'));
		}

		//TODO update lang untuk terjemahan kanvas-kanvasupdated
		$this->_arrData['strMessage'] = loadLanguage('adminmessage',
													 $this->session->userdata('jw_language'),
													 'kanvas-kanvasupdated');
	} else if ($this->input->post('subDelete') != '' && $intID != '') {
		$this->Mkanvas->deleteByID($intID);

		//TODO update lang untuk terjemahan kanvas-kanvasdeleted
		$this->session->set_flashdata('strMessage',
			loadLanguage('adminmessage',$this->session->userdata('jw_language'),'kanvas-kanvasdeleted'));
		redirect('kanvas/browse');
	}

	$arrKanvasData = $this->Mkanvas->getItemByID($intID);
	$intKanvasDay = $this->Mkanvas->getLastDay($intID);

	if (!empty($arrKanvasData)) {
		$arrKanvasData['kanv_day'] = $intKanvasDay;

		$arrKanvasData['kanv_rawstatus'] = $arrKanvasData['kanv_status'];
		$arrKanvasData['kanv_status'] = translateDataIntoHTMLStatements(
			array('title' => 'selStatus',
				  'field_type' => 'SELECT',
				  'description' => 'invoice_status',
				  'allow_null' => 0, 'validation' => ''), $arrKanvasData['kanv_status']);

		$this->load->model('Munit');

		/*ambil semua data item dari kanvas yg terpilih*/
		$arrKanvasItemSisa = $this->Mkanvasitem->getItemsByKanvasIDByDayForEditing($intID, 0, $intKanvasDay);

		if (!empty($arrKanvasItemSisa))
		for($i = 0; $i < count($arrKanvasItemSisa); $i++) {
			$arrKanvasItemSisa[$i]['strName']=$arrKanvasItemSisa[$i]['kavi_description'].' | '.$arrKanvasItemSisa[$i]['proc_title'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemSisa[$i]['kavi_unit1']);
			$intConv1=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemSisa[$i]['kavi_unit2']);
			$intConv2=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemSisa[$i]['kavi_unit3']);
			$intConv3=$convTemp[0]['unit_conversion'];

			$arrKanvasItemSisa[$i]['kavi_conv1']=$intConv1;
			$arrKanvasItemSisa[$i]['kavi_conv2']=$intConv2;
			$arrKanvasItemSisa[$i]['kavi_conv3']=$intConv3;
            
            if ($arrKanvasItemSisa[$i]['saldo'] > -1) {
                
                if (array_key_exists('conv1', $arrKanvasItemSisa[$i]) && array_key_exists('conv2', $arrKanvasItemSisa[$i]) && array_key_exists('conv3', $arrKanvasItemSisa[$i])) {
                    if ($arrKanvasItemSisa[$i]['conv1'] == 0) {
                        $arrKanvasItemSisa[$i]['kavi_quantity1'] = 0;
                        $modQty1 = 0;
                    } else {
                        $arrKanvasItemSisa[$i]['kavi_quantity1'] = (int) ($arrKanvasItemSisa[$i]['saldo'] / $arrKanvasItemSisa[$i]['conv1']);
                        $modQty1 = $arrKanvasItemSisa[$i]['saldo'] % $arrKanvasItemSisa[$i]['conv1'];
                    };

                    if ($arrKanvasItemSisa[$i]['conv2'] == 0 ) {
                        $arrKanvasItemSisa[$i]['kavi_quantity2'] = 0;
                        $modQty2 = 0;
                    } else {
                        $arrKanvasItemSisa[$i]['kavi_quantity2'] = (int) ($modQty1 / $arrKanvasItemSisa[$i]['conv2']);
                        $modQty2 = $modQty1 % $arrKanvasItemSisa[$i]['conv2'];
                    }

                    $arrKanvasItemSisa[$i]['kavi_quantity3'] = $modQty2;
                }
                
            }
		};


		$arrKanvasItemJual = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID, 1, $intKanvasDay);
		if (!empty($arrKanvasItemJual))
		for($i = 0; $i < count($arrKanvasItemJual); $i++) {
			$arrKanvasItemJual[$i]['strName']=$arrKanvasItemJual[$i]['kavi_description'].' | '.$arrKanvasItemJual[$i]['proc_title'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemJual[$i]['kavi_unit1']);
			$intConv1=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemJual[$i]['kavi_unit2']);
			$intConv2=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemJual[$i]['kavi_unit3']);
			$intConv3=$convTemp[0]['unit_conversion'];

			$arrKanvasItemJual[$i]['kavi_conv1']=$intConv1;
			$arrKanvasItemJual[$i]['kavi_conv2']=$intConv2;
			$arrKanvasItemJual[$i]['kavi_conv3']=$intConv3;
		}


		$arrKanvasItemTurun = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID, 2, $intKanvasDay);
		if (!empty($arrKanvasItemTurun))
		for($i = 0; $i < count($arrKanvasItemTurun); $i++) {
			$arrKanvasItemTurun[$i]['strName']=$arrKanvasItemTurun[$i]['kavi_description'].' | '.$arrKanvasItemTurun[$i]['proc_title'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemTurun[$i]['kavi_unit1']);
			$intConv1=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemTurun[$i]['kavi_unit2']);
			$intConv2=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemTurun[$i]['kavi_unit3']);
			$intConv3=$convTemp[0]['unit_conversion'];

			$arrKanvasItemTurun[$i]['kavi_conv1']=$intConv1;
			$arrKanvasItemTurun[$i]['kavi_conv2']=$intConv2;
			$arrKanvasItemTurun[$i]['kavi_conv3']=$intConv3;
		};

		$arrKanvasItemRetur = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID, 3, $intKanvasDay);
		if (!empty($arrKanvasItemRetur))
		for($i = 0; $i < count($arrKanvasItemRetur); $i++) {
			$arrKanvasItemRetur[$i]['strName']=$arrKanvasItemRetur[$i]['kavi_description'].' | '.$arrKanvasItemRetur[$i]['proc_title'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemRetur[$i]['kavi_unit1']);
			$intConv1=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemRetur[$i]['kavi_unit2']);
			$intConv2=$convTemp[0]['unit_conversion'];
			$convTemp=$this->Munit->getConversion($arrKanvasItemRetur[$i]['kavi_unit3']);
			$intConv3=$convTemp[0]['unit_conversion'];

			$arrKanvasItemRetur[$i]['kavi_conv1']=$intConv1;
			$arrKanvasItemRetur[$i]['kavi_conv2']=$intConv2;
			$arrKanvasItemRetur[$i]['kavi_conv3']=$intConv3;
		};

	}
    
    $intDay = $this->input->post('intHari');
    $arrKanvasItemPrintout = $this->getPrintDataByID($intID,0,$intDay);
    $arrKanvasData['curr_loading_date'] = $arrKanvasData['kanv_loading_date'];

    $dateLoading = strtotime("+".$intDay." days", strtotime($arrKanvasData['kanv_loading_date']));

    $arrKanvasData['curr_loading_date'] = date("Y-m-d", $dateLoading);
    $arrKanvasData['curr_loading_date'] = str_replace("-","/",$arrKanvasData['curr_loading_date']);

    $arrData = array(
        'intKanvasID' => $intID,
        'arrKanvasItemSisa' => $arrKanvasItemSisa,
        'arrKanvasItemJual' => $arrKanvasItemJual,
        'arrKanvasItemTurun' => $arrKanvasItemTurun,
        'arrKanvasItemRetur' => $arrKanvasItemRetur,
        'arrKanvasData' => $arrKanvasData,
		'arrCompanyInfo' => $this->_arrData['arrCompanyInfo'],
    );

    if(($this->input->post('subPrint') != '' || $this->input->get('print') != '') && $intID != '') {
        $this->load->view('sia_print',array_merge(array(
        	'strPrintFile' => 'kanvas',
        ), $arrData));
    } else {
        $arrKanvasList = $this->Mkanvas->getUserHistory($this->session->userdata('strAdminID'),$this->config->item('jw_item_count'));

        $this->load->view('sia',array_merge(array(
	        'strViewFile' => 'kanvas/view',
            'arrKanvasList' => $arrKanvasList,
            'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'kanvas-kanvasstatus')
	    ), $arrData, $this->admlinklist->getMenuPermission(47,$arrKanvasData['kanv_rawstatus'])));
    }
}

public function getPrintDataByID($intID,$intRecType,$intDay) {

    $arrItems = $this->Mkanvasitem->getItemsByKanvasIDByDay($intID,$intRecType,$intDay);        

	if (!empty($arrItems)) {
		for ($i = 0; $i < count($arrItems); $i++) {

            $arrItems[$i]['strName']=$arrItems[$i]['kavi_description'].' | '.$arrItems[$i]['proc_title'];

            if ($arrItems[$i]['saldo'] > -1) {
                if (array_key_exists('conv1', $arrItems[$i]) && array_key_exists('conv2', $arrItems[$i]) && array_key_exists('conv3', $arrItems[$i])) {
                    if ($arrItems[$i]['conv1'] == 0) {
                        $arrItems[$i]['kavi_quantity1'] = 0;
                        $modQty1 = 0;
                    } else {
                        $arrItems[$i]['kavi_quantity1'] = (int) ($arrItems[$i]['saldo'] / $arrItems[$i]['conv1']);
                        $modQty1 = $arrItems[$i]['saldo'] % $arrItems[$i]['conv1'];
                    };

                    if ($arrItems[$i]['conv2'] == 0 ) {
                        $arrItems[$i]['kavi_quantity2'] = 0;
                        $modQty2 = 0;
                    } else {
                        $arrItems[$i]['kavi_quantity2'] = (int) ($modQty1 / $arrItems[$i]['conv2']);
                        $modQty2 = $modQty1 % $arrItems[$i]['conv2'];
                    }

                    $arrItems[$i]['kavi_quantity3'] = $modQty2;
                }
			}
		};
    }

    return $arrItems;

}

}