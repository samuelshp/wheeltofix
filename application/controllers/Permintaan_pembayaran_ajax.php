<?php

class Permintaan_pembayaran_ajax extends JW_Controller {

    public function getNamaProyekByOwner($idowner='', $id_pegawai_login=''){
        $this->load->model('Mperminbayar');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mperminbayar->getNamaProyekByOwner($idowner, $id_pegawai_login)));
    }

    public function getNamaPekerjaan($kontrak_id){
        $this->load->model('Mperminbayar');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mperminbayar->getNamaPekerjaan($kontrak_id)));
    }

    public function getKategori($id_subkontrak){
        $this->load->model('Mperminbayar');
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->Mperminbayar->getNamaKategori($id_subkontrak)));
    }
}