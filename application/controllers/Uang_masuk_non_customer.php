<?php 

use App\Models\{Euangmasuknoncustomer, Eaccount};
if(!class_exists('Euangmasuknoncustomer')) require_once(APPPATH.'models/Euangmasuknoncustomer.php');
if(!class_exists('Eaccount')) require_once(APPPATH.'models/Eaccount.php');
use App\Models\DBFacadesWrapper as DB;

class Uang_masuk_non_customer extends JW_Controller
{
    public function __construrct()
    {
        parent::__construct();
        if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
        $this->load->model('Madmlinklist','admlinklist');
        $this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));

    }

    public function index()
    {
        $this->_getMenuHelpContent(236,true,'adminpage');

        $arrPagination['base_url'] = site_url("uang_masuk_non_customer/?pagination=true", NULL, FALSE);
        $arrPagination['total_rows'] = Euangmasuknoncustomer::count();
        $arrPagination['per_page'] = $this->config->item('jw_item_count');
        $arrPagination['uri_segment'] = 3;
        $this->pagination->initialize($arrPagination);
        $strPage = $this->pagination->create_links();

        $umncList = Euangmasuknoncustomer::where('status', 0)->get();
        // dd($umncList);
        $strBrowseMode = '';

        $this->load->view('sia',array(
            'strViewFile' => 'uang_masuk_non_customer/index',
            'strPage' => $strPage,
            'strPageTitle' => 'Uang Masuk Non Customer',
            'strBrowseMode' => $strBrowseMode,
            'umncList' => $umncList,
        ));
    }

    public function action($action, $id = null)
    {
        $data = [];
        $availableAccount  = Eaccount::all();

        // page action hanya membutuhkan object(singular) jika tidak menggunakan revisi/versi
        if($action == 'add')
        {
            $object  = null;
        }
        elseif($action == 'edit')
        {
            $object = Euangmasuknoncustomer::with('account')
                ->where('id', $id)
                ->first();
        }
        
        $data = array_merge($data, [
            'strViewFile'       => 'uang_masuk_non_customer/action',
            'strPageTitle'      => 'Uang Masuk Non Customer',
            'formAction'        => site_url('uang_masuk_non_customer/'.$action),
            'availableAccount'  => $availableAccount,
            'object'            => $object,
        ]);
        
        $this->load->view('sia', $data); 

    }

    public function add()
    {
        generateRepopulate();


        $uangMasukNonCustomer              = new Euangmasuknoncustomer;
        $uangMasukNonCustomer->kode        = $this->input->post('kode');
        $uangMasukNonCustomer->coa_id      = $this->input->post('coa_id');
        $uangMasukNonCustomer->type        = $this->input->post('type');
        $uangMasukNonCustomer->no_giro     = $this->input->post('no_giro');
        $uangMasukNonCustomer->tanggal_jt  = $this->input->post('tanggal_jt');
        $uangMasukNonCustomer->amount      = $this->input->post('amount');
        $uangMasukNonCustomer->status      = 0;
        $uangMasukNonCustomer->cdate       = date("Y-m-d H:i:s");
        $uangMasukNonCustomer->save();

        $this->session->set_flashdata('strMessage','Data Uang Masuk Non Customer has been created successfully.');
        redirect('uang_masuk_non_customer/', 'refresh');

    }

    public function edit()
    {
        generateRepopulate();

        $uangMasukNonCustomer              = Euangmasuknoncustomer::where('id', $this->input->post('id'))->first();
        $uangMasukNonCustomer->kode        = $this->input->post('kode');
        $uangMasukNonCustomer->coa_id      = $this->input->post('coa_id');
        $uangMasukNonCustomer->type        = $this->input->post('type');
        $uangMasukNonCustomer->no_giro     = $this->input->post('no_giro');
        $uangMasukNonCustomer->tanggal_jt  = $this->input->post('tanggal_jt');
        $uangMasukNonCustomer->amount      = $this->input->post('amount');
        $uangMasukNonCustomer->status      = 0;
        $uangMasukNonCustomer->mdate       = date("Y-m-d H:i:s");
        $uangMasukNonCustomer->save();

        $this->session->set_flashdata('strMessage','Data Uang Masuk Non Customer has been updated.');
        redirect('uang_masuk_non_customer/', 'refresh');

    }

    public function  delete()
    {
        $data = $this->cfg;

        try
        {
            $id   = $this->input->post('id');
            $object = Euangmasuknoncustomer::where('id', $id)->update(['status' => 2]);



            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode([
                        'status' => 'ok',
                        'text' => 'Data succesfully deleted',
                        'type' => 'success'
                    ])
                );

        }
        catch (\Exception $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
        catch (QueryException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        } 
        catch (\PDOException $e)
        {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                        'status' => 'error',
                        'text' => $e->getCode() .' - '. $e->getMessage(),
                        'type' => 'danger'
                    ])
                );
        }
    }
}