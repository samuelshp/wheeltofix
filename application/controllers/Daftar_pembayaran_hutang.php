<?php 

class Daftar_pembayaran_hutang extends JW_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('strAdminUserName') == '') redirect();

        // Generate the menu list
		$this->load->model('Madmlinklist','admlinklist');
		$this->_arrData['arrLinkList'] = $this->admlinklist->get($this->session->userdata('strAdminPriviledge'));
		$this->_getMenuHelpContent(244,true,'adminpage');
		$this->load->model('Mdaftarpembayaranhutang');
		$this->load->model('Mdaftarpembayaranhutangitem');	
	}

	public function index()
	{		
		if($this->input->post('smtMakeDaftarPembayaranHutang') != '') {		
			
			$arrDaftarPembayaranHutang = array(
				'dphu_date' => $this->input->post('txtDateBuat'),
				'dphu_amount' => $this->input->post('amount'),
			);								
			
			$intDPPID = $this->Mdaftarpembayaranhutang->addDPH($arrDaftarPembayaranHutang);

			$dphi_dphu_id = $this->Mdaftarpembayaranhutang->getDataByCode($arrDaftarPembayaranHutang['dphu_code'])['id'];

			//Insert Item Daftar Pembayaran Hutang
			foreach ($this->input->post('chbxDibayar') as $key => $strIDwithAmount){				
				$item = explode("-",$strIDwithAmount);
				$no_pi = $item[0];
				$amount_rencanabayar = ($item[4] == 1) ? ($item[1]+$item[3]) : $item[1];
				$type = $item[2];
				$dphi_twoweek_status = 0;
				$sisa = $item[3];
				$intLunas = $item[4];				
				$intDPHItem = $this->Mdaftarpembayaranhutangitem->add($intDPPID, $type, $no_pi, $amount_rencanabayar, $sisa, $dphi_twoweek_status,$intLunas);
				$intUpdateStatus = $this->Mdaftarpembayaranhutangitem->updateStatus($type, $no_pi);				
			}			
			
			$this->session->set_flashdata('strMessage','Pembayaran Hutang berhasil dibuat');
			redirect("daftar_pembayaran_hutang/browse","refresh");	
		}

		if ($this->input->post('smtUpdateDaftarPembayaranHutang') != '') {

			$dph_id = $this->input->post('dph_id');
			$strTglEdit = str_replace("/", "-", $this->input->post('tglEdit'));
			$this->Mdaftarpembayaranhutang->updateTglBuat($strTglEdit, $dph_id);

			$intAmount = 0;
			foreach($this->input->post('amountRencanaBayar') as $key => $item){
				$itemToDelete['amount_rencanabayar'] = str_replace(",", ".", str_replace(".","",$item));
				$itemToDelete['amount_sisabayar'] = $this->input->post('sisa_bayar')[$key];
				$itemToDelete['id'] = $this->input->post('dphi_id')[$key];
				$itemToDelete['lunas'] = $this->input->post('chbxLunas')[$key];
				$this->Mdaftarpembayaranhutangitem->updateRencanaBayar($itemToDelete,$itemToDelete['id']);
				$intAmount += $itemToDelete['amount_rencanabayar'];
			}

			$this->Mdaftarpembayaranhutang->updateAmount($intAmount,$dph_id);

			$this->session->set_flashdata('strMessage','Daftar Pembayaran Hutang berhasil diperbaharui');
			redirect("daftar_pembayaran_hutang/view/".$this->input->post('dph_id'),"refresh");			
		}

		if ($this->input->post('smtDeleteDaftarPembayaranHutang') != '') {	
			
			$itemTerbayar = [];

			if(!is_array($this->input->post('itemTerbayar'))) array_push($itemTerbayar, $this->input->post('itemTerbayar'));			
			$dph_id = $this->input->post('dph_id');
			$intDPHdelete = $this->Mdaftarpembayaranhutang->delete($dph_id);
			
			if($intDPHdelete) {
				$this->session->set_flashdata('strMessage','Daftar Pembayaran Hutang berhasil dihapus');        
				redirect("daftar_pembayaran_hutang/browse","refresh");
			}else{
				$this->session->set_flashdata('strMessage','Daftar Pembayaran Hutang gagal dihapus');        
				redirect("daftar_pembayaran_hutang/view/".$intDPHdelete,"refresh");
			}			
		}

		if ($this->input->post('btnDeleteItem') != '') {
			$id = $this->input->post('dph_id');			
			$intAmount = 0;
			foreach ($this->input->post('idToDelete') as $key => $item) {
				if (!empty($item)) {
					$itemDelete = $this->Mdaftarpembayaranhutangitem->deleteItem($item); 				
				}else{
					$intAmount += (Double) str_replace(",", ".", str_replace(".","",$this->input->post('amountRencanaBayar')[$key]));
				}
			}

			$this->Mdaftarpembayaranhutang->updateAmount($intAmount, $id);

			if($intAmount == 0) $this->Mdaftarpembayaranhutang->delete($id);
			
			$this->session->set_flashdata('strMessage','Item berhasil dihapus');        
			redirect("daftar_pembayaran_hutang/view/".$id,"refresh");
		}

		if($this->input->post('subPrint') != '' || $this->input->get('print') != '' ) {			

			$id = ($this->input->post('dph_id') != '') ? $this->input->post('dph_id') : $this->input->get('print');
			$this->load->model('Mdaftarpembayaranhutangitem');
			$arrDaftarPembayaranHutang = $this->Mdaftarpembayaranhutang->getDataByID($id);
			$arrDaftarPembayaranHutangItem = $this->Mdaftarpembayaranhutangitem->getItemsByID($id);

			foreach ($arrDaftarPembayaranHutangItem as $key => $value) {					
				$strProduct = " ";

				if ($value['dphi_reff_type'] !== "DP") $products[$key] = $this->Mdaftarpembayaranhutang->getProductList($value['dphi_reff_id']);		
				if($value['dphi_reff_type'] !== "PI") $arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = "-";		
				else $arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = $value['dummy_pph'];

				if(!empty($products[$key])){
					foreach ($products[$key] as $value) {
						$strProduct .= $value['prod_title']."<br/>";
					}
				}else{
					$strProduct .= $value['dpay_description'];
				}

				$arrDaftarPembayaranHutangItem[$key]['product'] = $strProduct;
			}

			$this->load->model('Mdbvo','MAdminLogin');
		    $this->MAdminLogin->initialize('adm_login');

		    $this->MAdminLogin->dbSelect('','id =' . $arrDaftarPembayaranHutang['cby']);
		    $arrLogin = $this->MAdminLogin->getNextRecord('Array');
			
			$this->load->view('sia_print',array(
	            'strPrintFile' => 'daftarpembayaranhutang',
	            'arrDaftarPembayaranHutang' => $arrDaftarPembayaranHutang,
				'arrDaftarPembayaranHutangItem' => $arrDaftarPembayaranHutangItem,
				'arrLogin' => $arrLogin
	        ));
		}else{
		//untuk ngambil data no pi,supplier,barang,amount
		$this->load->model('Msupplier');
		$arrSupplier = $this->Msupplier->getAllData();				
			
		$this->load->view('sia',array_merge(array(
			'strViewFile' => 'daftar_pembayaran_hutang/add',
			'arrSupplier' => $arrSupplier,				
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'daftarpembayaranhutang-daftarpembayaranhutang')
		),$this->admlinklist->getMenuPermission(244,1)));
		}         


	}

	public function browse($intPage = 0)
	{
		if($this->input->post('subSearch') != '') {		
			$strSearchDate = str_replace("/", "-", $this->input->post('txtSearchDate'));
			$strSearchDate2 = str_replace("/", "-", $this->input->post('txtSearchDate2'));
			$strSearchKey = $this->input->post('txtSearchNoDPH');		        
	        $arrDaftarPembayaranHutang = $this->Mdaftarpembayaranhutang->searchBy($strSearchDate, $strSearchDate2, $strSearchKey, $this->input->post('txtSearchNoSupplier'));		       	        
	        $strPage = '';
	        $strBrowseMode = '<a href="'.site_url('daftar_pembayaran_hutang/browse', NULL, FALSE).'">[Back]</a>';
	        $this->_arrData['strMessage'] = "Search result (".(!empty($arrDaftarPembayaranHutang) ? count($arrDaftarPembayaranHutang) : '0')." records).";		       
		}else{
			$arrPagination['base_url'] = site_url("daftar_pembayaran_hutang/browse?pagination=true", NULL, FALSE);
			$arrPagination['total_rows'] = $this->Mdaftarpembayaranhutang->getCount();
			$arrPagination['per_page'] = $this->config->item('jw_item_count');
			$arrPagination['uri_segment'] = 3;
			$this->pagination->initialize($arrPagination);
			$strPage = $this->pagination->create_links();

			$strBrowseMode = '';
			if($this->input->get('page') != '') $intPage = $this->input->get('page');
			$arrDaftarPembayaranHutang = $this->Mdaftarpembayaranhutang->getAllData($intPage, $arrPagination['per_page']);
		}		

		$this->load->view('sia',array_merge(array(
			'arrDaftarPembayaranHutang' => $arrDaftarPembayaranHutang,
			'strViewFile' => 'daftar_pembayaran_hutang/browse',
			'strSearchKey' => $this->input->post('txtSearchValue'),
			'strSearchDate' => $this->input->post('txtSearchDate'),
			'strSearchDateTo' => $this->input->post('txtSearchDateTo'),
			'intSearchStatus' => $this->input->post('txtSearchStatus'),
			'strPage' => $strPage,
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'daftarpembayaranhutang-daftarpembayaranhutang')
		),$this->admlinklist->getMenuPermission(244,1)));

	}

	public function view($id)
	{
		$this->load->model('Mdaftarpembayaranhutangitem');
		$arrDaftarPembayaranHutang = $this->Mdaftarpembayaranhutang->getDataByID($id);		
		$arrDaftarPembayaranHutangItem = $this->Mdaftarpembayaranhutangitem->getItemsByID($id);
		
		foreach ($arrDaftarPembayaranHutangItem as $key => $value) {					
			$strProduct = " ";

			if ($value['dphi_reff_type'] !== "DP") $products[$key] = $this->Mdaftarpembayaranhutang->getProductList($value['dphi_reff_id']);		
			if($value['dphi_reff_type'] !== "PI" && $value['dphi_reff_type'] !== "NKS" && $value['dphi_reff_type'] !== "NDS"){ 
				$arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = "-"; 
				$arrDaftarPembayaranHutangItem[$key]['total_after_pph'] = $arrDaftarPembayaranHutangItem[$key]['total'];
			}else{
				$arrDaftarPembayaranHutangItem[$key]['pinv_pph'] = $value['dummy_pph'];
				$arrDaftarPembayaranHutangItem[$key]['total_after_pph'] = $arrDaftarPembayaranHutangItem[$key]['total'] - ($arrDaftarPembayaranHutangItem[$key]['total']*$arrDaftarPembayaranHutangItem[$key]['pinv_pph']/100);
			}

			if(!empty($products[$key])){
				foreach ($products[$key] as $value) {
					$strProduct .= $value['prod_title']."<br/>";
				}
			}else{
				$strProduct .= $value['dpay_description'];
			}

			$arrDaftarPembayaranHutangItem[$key]['product'] = $strProduct;

		}

		$this->load->view('sia', array_merge(array(
			'arrDaftarPembayaranHutang' => $arrDaftarPembayaranHutang,
			'arrDaftarPembayaranHutangItem' => $arrDaftarPembayaranHutangItem,
			'strViewFile' => 'daftar_pembayaran_hutang/view',
			'strPageTitle' => loadLanguage('admindisplay',$this->session->userdata('jw_language'),'daftarpembayaranhutang-daftarpembayaranhutang')
		),$this->admlinklist->getMenuPermission(244,2)));
	}


	
}