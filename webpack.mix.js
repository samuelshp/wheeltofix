const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'asset/compiled/js')
   .sass('resources/sass/app.scss', 'asset/compiled/css')
   .autoload({
        jquery: ['$', 'window.jQuery']
    });
