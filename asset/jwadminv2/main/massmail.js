$("#InStRuCtIoN").popover({ html: true, placement: 'bottom' });
$(".InStRuCtIoN").popover({ html: true, placement: 'right' });

$("#selTo").chosen({
    single_backstroke_delete: false,
    no_results_text: "Oops, nothing found!"
}).change(function() {
    var i = 0;
    var l = $("#selTo > option").length;
    $("#txaTo").text('');
    $("#selTo > option:selected").each(function() {
        if(i == 0) $("#txaTo").text(this.value);
        else $("#txaTo").append(', ' + this.value);
        i++;
    });
    if(i == l) $("#cbSendCheckAll").prop('checked',true);
    else $("#cbSendCheckAll").prop('checked',false);
});

$("#cbSendCheckAll").change(function() {
    if($(this).is(":checked")) {
        $("#selTo > option").prop('selected',true);
    } else {
        $("#selTo > option").prop('selected',false);
    }
    $("#selTo").change();
    $("#selTo").trigger("chosen:updated");
});

$("#subSend").click(function() {
    $("#frmSendMail").validate();
});

/* Tiny MCE */
tinymce.init({
    selector: "#txaContent",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker responsivefilemanager"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
    toolbar2: "",
    /*content_css : "/mycontent.css",*/
    image_advtab: true,
    relative_urls: false,
    convert_urls : false,
    remove_script_host : false,
    height: 400,
    resize: "both",
    extended_valid_elements : "iframe[src|title|width|height|allowfullscreen|frameborder|class|style|alt|align|name]",
    external_filemanager_path:"<?=base_url('asset/responsivefilemanager')?>/",
    filemanager_title:"File Manager" ,
    external_plugins: { "filemanager" : "<?=base_url('asset/responsivefilemanager/plugin.min.js')?>"}
});