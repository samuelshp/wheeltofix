$(".InStRuCtIoN").popover({ html: true, placement: 'right' });
$(".btnFileBrowse").fancybox({ 'width': '100%', 'height': '100%', 'margin': 15, 'type': 'iframe', 'autoScale': false });

$("#frmTable").validate({
	rules:{},    
	messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
	errorClass: "input-group-addon error",
	errorElement: "label",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
		$(element).parents('.control-group').addClass('success');
	}
});
    
$(window).on('hashchange',function() { 
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    else $('.nav-tabs li:first-of-type a').tab('show');
}).trigger('hashchange');
$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    window.location.hash = $(this).attr('href');
});