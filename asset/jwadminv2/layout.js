$('#container').addClass("enabled");
$('.form-control').addClass("input-sm");
$('.btn').addClass("btn-sm");

if($.cookie('sidebar-hidden') == 'true') {
	$('#wrapper').addClass('sidebar-hidden');
	$('.navbar-toggle i').removeClass('fa-minus').addClass('fa-plus');
}

$('.navbar-toggle').click(function() {
	if($(window).width() > 768) {
		$('#wrapper').toggleClass('sidebar-hidden');
		if($('#wrapper').hasClass('sidebar-hidden')) {
			$.cookie('sidebar-hidden', 'true');
			$('.navbar-toggle i').removeClass('fa-minus').addClass('fa-plus');
		} else {
			$.cookie('sidebar-hidden', 'false');
			$('.navbar-toggle i').removeClass('fa-plus').addClass('fa-minus');
		}
	}
});