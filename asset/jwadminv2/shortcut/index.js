$(".sortable").disableSelection().sortable({
    placeholder: "sortable-item",
    opacity: 0.6,
    update: function(event, ui) {
        var info = $(this).sortable("serialize");
        $.ajax({
            type: "POST",
            url: "<?=site_url('shortcut/reorder')?>",
            data: info,
            context: document.body,
            success: function(response){
                if(response == 'reorder_success') location.assign("<?=site_url('shortcut?msg=reorder_success')?>");
    			else location.assign("<?=site_url('shortcut?msg=reorder_failed')?>");
            }
        });
    }
});