$(".jwDateTime").datetimepicker({dateFormat: "yy/mm/dd"});
$("#InStRuCtIoN").popover({ html: true, placement: 'bottom' });
$(".InStRuCtIoN").popover({ html: true, placement: 'right' });

function checkCbDate() {
    var e = $('input[name=cbDate]');
    var bolEnabled = !e.is(':checked');
    e.closest('div').siblings('.jwDateTime').attr('disabled',bolEnabled);
}

checkCbDate();
$('input[name=cbDate]').change(function() {
    checkCbDate();
});

$('input[name=radType]').change(function() {
    $('.divQuestion, .divQuestion + p.spacer, .divQuestion + p.spacer + hr').remove();
    $('#hidQNo').val(0);
    $('#bAddQuestion').data('qno',1);
    $('#bAddQuestion').click();
    if($(this).val() == 1) $('#bAddQuestion').show();
    else if($(this).val() == 2) $('#bAddQuestion').hide();
});

$('#bAddQuestion').click(function() {
    var qNo = $(this).data('qno');
    $(this).data('qno',qNo + 1);
    $('#hidQNo').val(parseInt($('#hidQNo').val()) + 1);

    if($(this).data('qorder') && $(this).data('qorder') != '') {
        var intID = $(this).data('qid'); $(this).data('qid','');
        var strTitle = $(this).data('qtitle'); $(this).data('qtitle','');
        var intQType = $(this).data('qtype'); $(this).data('qtype','');
        if(intQType == 1) var strQType1 = ' checked="checked"'; else var strQType1 = '';
        if(intQType == 2) var strQType2 = ' checked="checked"'; else var strQType2 = '';
        if(intQType == 3) {
            var strQType3 = ' checked="checked"';
            var strHTMLAnswer = '';
        } else {
            var strQType3 = '';
            var strHTMLAnswer = 
        '<button type="button" class="btn btn-sm btn-default bAddAnswer" data-qno="' + qNo + '" data-ano="1"><i class="fa fa-plus-circle"></i></button>';
        }
        var intQOrder = $(this).data('qorder'); $(this).data('qorder','');
        var bolClickAddAnswerButton = false;

    } else {
        var intID = '0';
        var strTitle = '';
        var strQType1 = ' checked="checked"', strQType2 = '', strQType3 = '';
        var strHTMLAnswer = 
        '<button type="button" class="btn btn-sm btn-default bAddAnswer" data-value="" data-qno="' + qNo + '" data-ano="1"><i class="fa fa-plus-circle"></i></button>';
        var intQOrder = '1';
        var bolClickAddAnswerButton = true;
    }

    $(this).closest('div').before(
'<div class="form-group divQuestion" id="divQuestion' + intQOrder + '">' + 
    '<div class="col-sm-4 col-md-2 control-label">' + 
        'Question ' + qNo + 
        '<i class="fa fa-info-circle InStRuCtIoN" rel="popover" data-content="Masukkan pertanyaan ' + qNo + '"></i>' + 
    '</div>' + 
    '<div class="col-sm-8 col-md-10 divQTitle">' + 
       '<input type="text" name="txtQ' + qNo + '" value="' + strTitle + '" class="form-control input-sm required" placeholder="Pertanyaan ' + qNo + '" />' + 
        '<input type="hidden" name="hidQID' + qNo + '" value="' + intID + '" />' + 
        '<input type="hidden" name="hidDeleted' + qNo + '" class="hidDeleted" value="0" />' + 
    '</div>' + 
    '<p class="spacer">&nbsp;</p>' + 
    '<div class="col-sm-4 col-md-2 control-label">' + 
        'Answer Type' + 
    '</div>' + 
    '<div class="col-sm-8 col-md-10 radio">' + 
       '<label class="radio-inline"><input type="radio" name="radQType' + qNo + '" value="1" class="qType"' + strQType1 + ' data-qno="' + qNo + '" /> Radio</label>' + 
       '<label class="radio-inline"><input type="radio" name="radQType' + qNo + '" value="2" class="qType"' + strQType2 + ' data-qno="' + qNo + '" /> Checkbox</label>' + 
       '<label class="radio-inline"><input type="radio" name="radQType' + qNo + '" value="3" class="qType"' + strQType3 + ' /> Text</label>' + 
    '</div>' + 
    '<p class="spacer">&nbsp;</p>' + 
    '<div class="col-sm-4 col-md-2 control-label">' + 
        'Answer' + 
    '</div>' + 
    '<div class="col-sm-8 col-md-10 row divAnswer"><div class="col-xs-12 col-md-6 col-lg-4">' + 
        strHTMLAnswer + 
    '</div></div>' + 
    '<p class="spacer">&nbsp;</p>' + 
    '<div><span class="btn btn-danger pull-right btnClose"><i class="fa fa-trash-o"></i></span></div>' +
'</div>' + 
'<p class="spacer">&nbsp;</p>' + 
'<hr />');

    if(bolClickAddAnswerButton)
        $(this).closest('div').prev().prev().prev('.divQuestion').children('.divAnswer').children('div').children('.bAddAnswer').click();

});

$('#frmQuestionnaire').validate({
    rules:{},    
    messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
    errorClass: "input-group-addon error",
    errorElement: "label",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});

$('#frmQuestionnaire').on('click','.bAddAnswer',function() {
    var qNo = $(this).data('qno');
    var aNo = $(this).data('ano');
    $(this).data('ano',aNo + 1);

    if($(this).data('value') != '') var strAnswerValue = $(this).data('value');
    else strAnswerValue = '';

    $(this).before(
'<input type="text" name="txtA' + qNo + '-' + aNo + '" value="' + strAnswerValue + '" class="form-control input-sm required" placeholder="Jawaban pertanyaan ' + qNo + '" />' + 
'<p class="spacer">&nbsp;</p>'
    );
}).on('change','.qType',function() {
    var qNo = $(this).data('qno');
    if($(this).val() == '3') /* Text */
        $(this).closest('div').siblings('.divAnswer').children('div').empty();
    else 
        $(this).closest('div').siblings('.divAnswer').children('div').html(
'<input type="text" name="txtA' + qNo + '-1" value="" class="form-control input-sm required" placeholder="Jawaban pertanyaan ' + qNo + '" />' + 
'<p class="spacer">&nbsp;</p>' + 
'<button type="button" class="btn btn-sm btn-default bAddAnswer" data-qno="' + qNo + '" data-ano="1"><i class="fa fa-plus-circle"></i></button>'
    );
}).on('click','.divQuestion .btnClose',function() {
    $(this).closest('.divQuestion').hide('slow');
    $(this).closest('.divQuestion').next('p.spacer').hide('slow');
    $(this).closest('.divQuestion').next('p.spacer').next('hr').hide('slow');
    $(this).closest('div').siblings('.divQTitle').children('.hidDeleted').val('1');
});<?php 

if($strMode == 'add'): ?>  
    if($('#bAddQuestion').data('qno') == 1) $('#bAddQuestion').click();<?php 
elseif(!empty($arrPolling['arrItem'])): 
$jsPollingItem = json_encode($arrPolling['arrItem']); ?>  
    var arrPollingItem = <?=$jsPollingItem?>;
    $.each(arrPollingItem, function(i,v) {
        $('#bAddQuestion').data('qid',v['id']);
        $('#bAddQuestion').data('qtype',v['poli_type']);
        $('#bAddQuestion').data('qtitle',v['poli_title']);
        $('#bAddQuestion').data('qorder',v['poli_order']);
        $('#bAddQuestion').click();
        $.each(v['poli_option'],function(i2,v2) {
            $('#divQuestion' + v['poli_order'] + ' .bAddAnswer').data('value',v2);
            $('#divQuestion' + v['poli_order'] + ' .bAddAnswer').click();
        });
    });<?php 
endif; ?>  