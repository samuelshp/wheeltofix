$("#InStRuCtIoN").popover({ html: true, placement: 'bottom' });
$(".InStRuCtIoN").popover({ html: true, placement: 'right' });
$(".jwDateTime").datetimepicker({dateFormat: "yy/mm/dd"});
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".jwTime").datetimepicker({ dateFormat: "yy/mm/dd", timeOnly: true });
$('[data-toggle="tooltip"]').tooltip();
$('[target$=blank]').removeAttr('target');

$('.table-responsive td:not(.action)').each(function(index) {
    $(this).html($(this).html().replace(/[,.;]/g, function(match) {
        return match + '&#8203;';
    }));
});

$('#btnCheckAll').click(function() {
    $('#cbCheckAll').click();
});

$("#cbCheckAll").click(function() {
    if($(this).prop('checked') == true) $(".table-responsive .cb input[type='checkbox']").prop('checked',true);
    else $(".table-responsive .cb input[type='checkbox']").prop('checked',false);
});

$(".table-responsive tbody td:not(.cb, .action)").click(function() {
    var url = $(this).parents('tr').data('url');
    if(url) window.location.href = url;
    else $(this).siblings(".cb").children("input[type='checkbox']").click();
    $(this).children(".cb > input[type='checkbox']").click();
});

 $(".table-responsive .cb input[type='checkbox']").click(function() {
    var bolCheckAll = true;
    $(".table-responsive tbody .cb").each(function() {
        if($(this).children("input[type='checkbox']").prop('checked') == false) {bolCheckAll = false;}
    });
    if(bolCheckAll) $("#cbCheckAll").prop('checked',true);
    else $("#cbCheckAll").prop('checked',false);
 });

$(".filterFields").keydown(function(e) {
    var code = e.charCode || e.keyCode || e.which;
    if(code == 13) {
        e.preventDefault();
        $("button[name='subSearch']").click();
    }
});

$("button[name='smtProcessType']").click(function() {
    if($(".table-responsive .cb input[type='checkbox']:checked").length <= 0) {
        alert("Please select some data to be processed!");
       return false;
    }
    
    if($(this).val() == 'Delete') {
        var bValid = confirm("Are you sure?\nIf you delete it, the data can't be retrieved again.\nIf you just want to hide the data, you can set the STATUS into INACTIVE by editing it's STATUS.");
        if(bValid == false) return false;    
    }
    
    if($(this).val() == 'Edit' || $(this).val() == 'Delete') $("#frmTable").removeAttr('target');
});

$("button[name='subSearch']").click(function(e) {
    $("#frmTable").attr('method', 'get').attr('action', '<?=site_url('table/browse/'.$arrTableListData['id'])?>');
});

$(".dropdown-toolbar").change(function() {
    $("#frmTable").attr('method', 'get').attr('action', '<?=site_url('table/browse/'.$arrTableListData['id'])?>').submit();
});

$("button[name='subPrint'][value='Excel']").click(function() {
    $("#frmTable table").table2excel({
        exclude: ".no-excel",
        name: "<?=$strPageTitle?>",
        filename: "<?=$strPageTitle?>-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
});

$("button[name='subPrint'][value='Print']").click(function() {
    window.print();
});