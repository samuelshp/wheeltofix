$(".form-group .digits, .form-group .number, .form-group .currency, .form-group .float, .form-group .half").parent().addClass('col-sm-5 col-md-4 col-lg-3');

$("#InStRuCtIoN").popover({ html: true, placement: 'bottom' });
$(".InStRuCtIoN").popover({ html: true, placement: 'right' });
$(".jwDateTime").datetimepicker({dateFormat: "yy/mm/dd"});
$(".jwDate").datetimepicker({ dateFormat: "yy/mm/dd", showTimepicker: false });
$(".jwTime").datetimepicker({ dateFormat: "yy/mm/dd", timeOnly: true });
$(".currency").number(true,2,'.',',');
$("select").chosen({
    disable_search_threshold: 20,
    single_backstroke_delete: false,
    no_results_text: "Oops, nothing found!"
});

<?php if($this->input->get('dependant') != ''): ?>  
$('select[name="<?=$this->input->get('dependant')?>"]').val('<?=$this->input->get('filter_'.$this->input->get('dependant'))?>').trigger('chosen:updated');
<?php endif; ?>  

$(".btnFileBrowse").fancybox({ 'width': '95%', 'type': 'iframe', 'autoSize': false });
$(".btnFileAdd").click(function () { checkFileList($(this).attr('id')); }).click();
$(document).on('click', ".btnFileRemove", function() { removeFileList($(this).data('targetid'),$(this).attr('value')); });
$(".multipleSelect").change(function() { checkMultipleSelectItems($(this).attr('id')); });
$(".cblAll").change(function() {
    if($(this).prop("checked")) var strClass = ".cbl:not(:checked)";
    else var strClass = ".cbl:checked";
    $(this).parents().eq(2).find(strClass).click(); 
});
$(".cbl").change(function() { checkCheckBoxListItems($(this).data("targetid")); });

$(".imgList").sortable().on( "sortupdate", function(event,ui) {
    var id = $(this).attr('id').replace('imglist-','');
    var data = $(this).sortable('toArray', {attribute: 'data-src'});
    var gallString = '';
    
    $.each(data, function(k,v) {
       if(gallString == '') gallString = v;
       else gallString = gallString + '; ' + v;
    }); 
    
    $('#' + id).val(gallString);
    checkFileList(id);
});

$('.jwAutoComplete').each(function(i, el) {
    el = $(el);
    el.autocomplete({
        minLength:3,
        delay:500,
        source: function(request,response) {
            $.ajax({
                url: el.data('link'),
                data: {strKey: el.val()},
                beforeSend: function() {
                    el.siblings('.input-group-addon').html('<img src="<?=base_url('asset/ajax_loader.gif')?>" width="15px" border="0px" />');
                    
                },
                success: function(data) {
                    var xmlDoc = $.parseXML(data);
                    var xml = $(xmlDoc);
                    var display=[];
                    $.map(xml.find('item'),function(val,i){
                        var strKey = $(val).find('strKey').text();
                        var strData = $(val).find('strData').text();
                        display.push({ label:strData, value:strData, id:strKey });
                    });
                    response(display);
                },
                complete: function() {
                    el.siblings('.input-group-addon').html('');
                }
            });
        },
        select: function(event, ui) {
            if(el.data('saveas') == 'id') addAutoCompleteItems(el.data('targetid'),ui.item.id);
            else addAutoCompleteItems(el.data('targetid'),ui.item.value);
        }
    });
});

$('.jwTableDescriptionList tfoot .form-control').attr('disabled', true).addClass('hidden');
$('.jwTableDescriptionList tfoot .btn-primary').click(function() {
    var html = $(this).closest('tfoot').html();
    $(this).closest('.jwTableDescriptionList').find('tbody').append(html);
    var $addedRow = $(this).closest('.jwTableDescriptionList').find('tbody .added');
    $addedRow.find('.action').html('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></button>');
    $addedRow.find('.form-control').attr('disabled', false).removeClass('hidden');
    $addedRow.removeClass('added');
});
$('.jwTableDescriptionList').on('click', 'tbody .btn-danger', function() {
    $(this).closest('tr').remove();
});

$("#frmTable").validate({
    rules:{<?php
$i = 0;
if(!empty($arrPasswordID)) foreach($arrPasswordID as $e) { ?>  
        "pwdReType<?=$e?>": {equalTo: "#<?=$e?>"}<?php
    if($i++ < count($arrPasswordID) - 1) echo ',
        ';
} ?>},    
    messages:{},
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });
        $.each(errorList, function (index, error) {
            var $element = $(error.element);
            $element.tooltip("destroy").data("title", error.message).addClass("error").tooltip();
            $element.closest('.form-group').addClass('has-error');
        });
    },
    errorClass: "input-group-addon error",
    errorElement: "label",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
        $(element).parents('.control-group').addClass('success');
    }
});

tinymce.init({
    selector: "textarea.jwHTMLEditor",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image imagetools charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker responsivefilemanager importcss"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
    toolbar2: "",
    importcss_append: true,<?php
if(is_file('asset/'.$this->config->item('jw_ui_style').'/tinymce.css')): ?>  
    content_css: "<?=base_url('asset/'.$this->config->item('jw_ui_style').'/tinymce.css')?>",<?php
endif; ?>  
    image_advtab: true,
    relative_urls: false,
    convert_urls : false,
    remove_script_host : false,
    height: 400,
    resize: "both",
    valid_elements : "*[*]",
    extended_valid_elements : "iframe[src|title|width|height|allowfullscreen|frameborder|class|style|alt|align|name]",
    external_filemanager_path:"<?=base_url('asset/responsivefilemanager')?>/",
    filemanager_title:"File Manager" ,
    external_plugins: { "filemanager" : "<?=base_url('asset/responsivefilemanager/plugin.min.js')?>"}
});