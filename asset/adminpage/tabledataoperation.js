// Check (;) occurence, if keypressed, add space
function checkSemicolon(id,e) {
	var keynum = 0;
	var keychar = '';
	if(window.event) { // IE
		keynum = window.event.keyCode;
		if(keynum == 186) keychar = ";";
	} else if(e.which) { // Netscape/Firefox/Opera
		keynum = e.which;
		if(keynum == 59) keychar = ";";
	}
	//keychar = String.fromCharCode(keynum);
	if(keychar == ";")
		document.getElementById(id).value = document.getElementById(id).value + " ";
}

// ----------------------- MULTIPLE SELECT ----------------------

function checkMultipleSelectItems(selectBoxID) {
	id = selectBoxID.replace('-list','');
    var selectedText = '';
    $("#" + selectBoxID).children("option:selected").each(function() {
        if(selectedText == '') selectedText = $(this).val();
        else selectedText = selectedText + '; ' +  $(this).val();
    });
    
    $("#" + id).val(selectedText);
}

// ----------------------- CHECKBOX LIST -----------------------

// Change the hidden checkbox list value
function checkCheckBoxListItems(id) {
    var strSelectedText = '';
    var count = 0;
    $("#" + id).parents().eq(1).find(".cbl:checked").each(function(i) {
        if(strSelectedText == '') strSelectedText = $(this).val();
        else strSelectedText = strSelectedText + '; ' + $(this).val();
        count++;
    });
    
    $("#" + id).val(strSelectedText);
    $("#checkboxNum" + id).val(count);
}

// ---------------------- AUTOCOMPLETE -----------------------
function addAutoCompleteItems(id,v) {
    if($("#" + id).text() == '') $("#" + id).text(v);
    else $("#" + id).text($("#" + id).text() + '; ' + v);
}

// ----------------------- FILE LIST -------------------------
function checkFileList(id) {
	id = id.replace('inpbut-','');
    
	// If any new addition to file list, add it to file list string
	if($("#inptext-" + id).val() != '') {
        if($("#" + id).val() == '') $("#" + id).val($("#inptext-" + id).val());
		else $("#" + id).val($("#" + id).val() + '; ' + $("#inptext-" + id).val());
	    
		$("#inptext-" + id).val('');
	}
    
	// Get all the file list string and display it
	var fileList = $("#" + id).val();
	var arrFileList = fileList.split('; ');
	var htmlFileList = '';
	for(i = 0; i < arrFileList.length; i++) if(arrFileList[i] != '') htmlFileList = htmlFileList + 
'<span class="img" data-src="' + arrFileList[i] + '">' + 
    '<a class="label label-danger btnFileRemove" data-targetid="' + id + '" value="' + i + '"><i class="fa fa-times"></i></a>' +
    '<img src="' + arrFileList[i] + '" alt="' + arrFileList[i] + '" class="imgData" />' + 
'</span>';

	if(fileList == '') var c = 0;
    else var c = i;
	
    $("#imglist-" + id).html(htmlFileList);
	
	toggleAddFile(id, Number(c));
}

function removeFileList(id, index) {
	// Get all the file list string and display it
	var fileList = $("#" + id).val();
	var arrFileList = fileList.split('; ');
	var htmlFileList = '';
	
	arrFileList.splice(index,1);
    
	fileList = '';
	for(i = 0; i < arrFileList.length; i++) {
        htmlFileList = htmlFileList + 
'<span class="img">' + 
    '<a class="label label-danger btnFileRemove" data-targetid="' + id + '" value="' + i + '"><i class="fa fa-times"></i></a>' +
    '<img src="' + arrFileList[i] + '" class="imgData" />' + 
'</span>';

		if(fileList == '') fileList = arrFileList[i];
		else fileList = fileList + arrFileList[i];
		if(i < arrFileList.length - 1) fileList = fileList + '; ';
	}
	var c = i;
	
    $("#imglist-" + id).html(htmlFileList);
    $("#" + id).val(fileList);
	
	toggleAddFile(id, c);
}

function toggleAddFile(id, fileCount) {
	var c = $("#countfile-" + id).val();
	// Initialize
    $("#inptext-" + id).css("display","inline");
    $("#inpbut-" + id).css("display","inline");
	// Check add button
	if(c != '' && !isNaN(c) && fileCount >= c) {
        $("#inptext-" + id).css("display","none");
        $("#inpbut-" + id).css("display","none");
	} 
}