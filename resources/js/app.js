
window.DataTable    = require('datatables.net-bs');
window.Responsive   = require('datatables.net-responsive-bs');
window.select2      = require('select2');
window.Sortable     = require('sortablejs');
window.swal         = require('sweetalert');

back = () => window.history.back();

$(function(){
    window.jwTable = $('.data-table').DataTable({responsive: true, "autoWidth": false});
    $('.select2').select2();
});